# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Content

#### Added

- Added Inventory Quantity fields to the Sales/Purchase facts [DEV-1173](https://hillstar.atlassian.net/browse/DEV-1173)
- Added Status and Type to the Sales/Purchase Order [DEV-1183]
- fixed sortByColumn issue that caused the job to fail [DEV-1203]

#### Changed

- Now replaces the dynamic sql in delta load procedure with a delta Function. [DEV-1115](https://hillstar.atlassian.net/browse/DEV-1115)
- During incremental load, includes updated records during an insert. [DEV-1086](https://hillstar.atlassian.net/browse/DEV-1086)
- **BREAKING** Tabular: Updated Tabular structure, renamed technical names of all date and calculated dates, measures to adhere to the coding standards [DEV-1167]
- **BREAKING** Reports: Updated reports to adhere to the new Tabular structure [DEV-1167]

### Ui & Framework

### Added

- Now logging task_executions, a detail level within a component execution for dwh and stage processing [DEV-1098], [DEV-1133]
- Adding a new component from the Birds application [DEV-1081]
- Change metadata of a component [DEV-1029]
    - Add/Update validation metadata required for executing rowcount validation
- Deploy components (Complete deploy, deploy metadata) [DEV-476]
- Process component [DEV-434]

### Changed

- Improved performance of the report logging procedure, and changed the default behaviour [DEV-1154]

### Removed

- userid fields from logging tables [DEV-1134]
- duplicate log messages that are now replaced with task_executions
- procedure validation.add_all_metadata, now moved to the content [DEV-1124]

## [1.1] - 2018-07-13

Minor release with additional content Payables, Receivables, Production

### Content

#### Added

- Add Production Module
- Added Settlement records to the VendorTransaction and CustomerTransaction tables
- Tabular: Added measures to calculate the different Vendor/Customer Transaction vs Settlements

#### Changed

- DWH: : Payables & Reveivables renamed to VendorTransaction and CustomerTransaction
- Tabular: Payables & Reveivables renamed to VendorTransaction and CustomerTransaction
- Report: Payables & Receivables included the new measures introduced with

### UI & Framework

#### Changed

- made several dwh/repl/stage process procedures for processing consistent in its use, can now execute procedure with a an db_object instead of just an id.

#### Removed

- removed dwh.process_full_single_component, now use dwh.process_component instead

## [1.0] - 2018-01-31

Initial version of Birds. Supports AX2012R3 and D365FO PU11+. Includes base modules for Sales, Purchase, Inventory, Payables, Recievables 
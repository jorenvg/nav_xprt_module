EXEC dbo.drop_object 'delta.SalesInvoiceFunction', 'F' ;
GO
CREATE FUNCTION delta.SalesInvoiceFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
              , inventory_transactions AS (
             SELECT IT.INVENTTRANSID
                  , IT.INVOICEID
                  , IT.ITEMID
                  , IT.ACCOUNTINGCURRENCY
                  , IT.DataConnectionID
                  , IT.CompanyID
                  , IT.PARTITION
                  , -SUM(IT.AMOUNT) AS Amount
               FROM help.InventoryTransaction AS IT
              GROUP BY IT.INVENTTRANSID
                     , IT.INVOICEID
                     , IT.ITEMID
                     , IT.ACCOUNTINGCURRENCY
                     , IT.DataConnectionID
                     , IT.CompanyID
                     , IT.PARTITION
           )
           SELECT            CIT.stage_id                                        AS StageId
                           , @execution_flag                                     AS ExecutionFlag
                           , @component_execution_id                             AS ComponentExecutionId
                           , CIT.RECID                                           AS SalesInvoiceLineRecId
                           , CIT.PARTITION                                       AS SalesInvoiceLinePartition
                           , CIT.data_connection_id                              AS DataConnectionID
                           , CIJ.company_id                                      AS CompanyId
                           , CIJ.DOCUMENTDATE                                    AS DocumentDate
                           , CIT.INVOICEDATE                                     AS InvoiceDate
                           , CIT.DLVDATE                                         AS ShipDate
                           , ST.RECEIPTDATEREQUESTED                             AS ReceiptRequestedDate
                           , ST.SHIPPINGDATEREQUESTED                            AS ShippingRequestedDate
                           , ST.RECEIPTDATECONFIRMED                             AS ReceiptConfirmedDate
                           , ST.SHIPPINGDATECONFIRMED                            AS ShippingConfirmedDate
                           , COALESCE(NULLIF(ID.WMSLOCATIONID, ''), 'N/A')       AS WMSlocationCode
                           , COALESCE(NULLIF(ID.INVENTLOCATIONID, ''), 'N/A')    AS LocationCode
                           , COALESCE(NULLIF(ID.CONFIGID, ''), 'N/A')            AS ProductConfigCode
                           , COALESCE(NULLIF(ID.INVENTCOLORID, ''), 'N/A')       AS ProductColorCode
                           , COALESCE(NULLIF(ID.INVENTSIZEID, ''), 'N/A')        AS ProductInventSizeCode
                           , COALESCE(NULLIF(ID.INVENTSTYLEID, ''), 'N/A')       AS ProductStyleCode
                           , COALESCE(NULLIF(ID.INVENTBATCHID, ''), 'N/A')       AS TrackingBatchCode
                           , COALESCE(NULLIF(ID.INVENTSERIALID, ''), 'N/A')      AS TrackingSerialCode
                           , CIT.DATAAREAID                                      AS CompanyCode
                           , COALESCE(CIJ.ORDERACCOUNT, ST.CUSTACCOUNT)          AS CustomerAccountCode
                           , COALESCE(CIJ.INVOICEACCOUNT, ST.INVOICEACCOUNT)     AS InvoiceAccountCode
                           , CIT.DELIVERYPOSTALADDRESS                           AS DeliveryAddressCode
                           , CIJ.INVOICEPOSTALADDRESS                            AS InvoiceAddressCode
                           , CIT.ITEMID                                          AS ItemCode
                           , COALESCE(CIJ.WORKERSALESTAKER, ST.WORKERSALESTAKER) AS SalesTakerCode
                           , ST.WORKERSALESRESPONSIBLE                           AS SalesResponsibleCode
                           , CIT.CURRENCYCODE                                    AS CurrencyCode
                           , CIT.DEFAULTDIMENSION                                AS DefaultDimensionRecId
                           , CIJ.LEDGERVOUCHER                                   AS VoucherCode
                           , CIT.REASONREFRECID                                  AS ReasonCode
                           , COALESCE(CIJ.DLVTERM, ST.DLVTERM)                   AS DeliveryTermCode
                           , COALESCE(CIJ.DLVMODE, ST.DLVMODE)                   AS DeliveryModeCode
                           , COALESCE(CIJ.PAYMENT, ST.PAYMENT)                   AS PaymentTermCode
                           , ST.PAYMMODE                                         AS PaymentModeCode
                           , CIJ.SALESTYPE                                       AS SalesOrderTypeCode
                           , COALESCE(CIJ.SALESORIGINID, ST.SALESORIGINID)       AS SalesOriginCode
                           , COALESCE(CIT.SALESGROUP, ST.SALESGROUP)             AS SalesGroupCode
                           , CIT.SALESID                                         AS SalesOrderCode
                           , ST.SALESPOOLID                                      AS SalesPoolCode
                           , CIT.INVOICEID                                       AS SalesInvoice
                           , CIT.NUMBERSEQUENCEGROUP                             AS NumberSequenceGroup
                           , CIT.SALESUNIT                                       AS SalesUnitOfMeasureCode
                           , CIT.TAXGROUP                                        AS SalesTaxGroupCode
                           , CIT.TAXITEMGROUP                                    AS SalesTaxItemGroupCode
                           , CIT.QTY                                             AS InvoicedQuantity
                           , CIT.INVENTQTY                                       AS InventoryQuantity
                           , CIT.QtyPhysical                                     AS DeliveredWithoutPackingSlipQuantity
                           , CIT.SALESPRICE                                      AS SalesPrice
                           , CIT.SALESPRICE * ex.CrossRate                       AS SalesPrice_RCY
                           , CIT.LINEDISC                                        AS Discount /* Discount per UOM */
                           , CIT.LINEDISC * ex.CrossRate                         AS Discount_RCY /* Discount per UOM in reporting currency)*/
                           , CIT.LINEPERCENT                                     AS DiscountPercent
                           , CIT.MULTILNDISC                                     AS MultiLineDiscount
                           , CIT.MULTILNDISC * ex.CrossRate                      AS MultiLineDiscount_RCY
                           , CIT.MULTILNPERCENT                                  AS MultiLineDiscountPercent
                           , CIT.LINEAMOUNT                                      AS NetAmount
                           , CIT.LINEAMOUNT * ex.CrossRate                       AS NetAmount_RCY
                           , CIT.LINEAMOUNTMST                                   AS NetAmountMST
                           , COALESCE(IT.Amount, 0)                              AS COGSMST
                           , COALESCE(IT.Amount, 0) * ex2.CrossRate              AS COGS_RCY
                           , CIT.LINEAMOUNTTAX                                   AS TaxAmount
                           , CIT.LINEAMOUNTTAX * ex.CrossRate                    AS TaxAmount_RCY
                           , CIT.LINEAMOUNTTAXMST                                AS TaxAmountMST
                           , CIT.LINEAMOUNT                                      AS AmountExclTax
                           , CIT.LINEAMOUNTMST                                   AS AmountExclTaxMST

                           , CASE WHEN CIT.LINEDISC <> 0 AND CIT.LINEDISC IS NOT NULL
                                  THEN CIT.QTY * CIT.LINEDISC
                                  ELSE 
                                      CASE WHEN CIT.LINEPERCENT <> 0 AND CIT.LINEPERCENT IS NOT NULL
                                           THEN ((CIT.QTY * CIT.SALESPRICE) * CIT.LINEPERCENT / 100 )
                                           ELSE 0
                                      END
                             END                                                  AS DiscountAmount
                           , CASE WHEN CIT.LINEDISC <> 0 AND CIT.LINEDISC IS NOT NULL
                                  THEN CIT.QTY * CIT.LINEDISC * ex.CrossRate
                                  ELSE 
                                      CASE WHEN CIT.LINEPERCENT <> 0 AND CIT.LINEPERCENT IS NOT NULL
                                           THEN ((CIT.QTY * CIT.SALESPRICE) * CIT.LINEPERCENT / 100 ) * ex.CrossRate
                                           ELSE 0
                                      END
                             END                                                    AS DiscountAmount_RCY                            

            FROM             stage_ax.CUSTINVOICETRANS AS CIT
            INNER JOIN       stage_ax.CUSTINVOICEJOUR  AS CIJ ON CIT.INVOICEID           = CIJ.INVOICEID
                                                             AND CIT.SALESID             = CIJ.SALESID
                                                             AND CIT.INVOICEDATE         = CIJ.INVOICEDATE
                                                             AND CIT.NUMBERSEQUENCEGROUP = CIJ.NUMBERSEQUENCEGROUP
                                                             AND CIT.PARTITION           = CIJ.PARTITION
                                                             AND CIT.data_connection_id  = CIJ.data_connection_id
                                                             AND CIT.company_id          = CIJ.company_id
             LEFT OUTER JOIN stage_ax.INVENTDIM        AS ID ON CIT.INVENTDIMID          = ID.INVENTDIMID
                                                            AND CIT.PARTITION            = ID.PARTITION
                                                            AND CIT.data_connection_id   = ID.data_connection_id
                                                            AND CIT.company_id           = ID.company_id
             LEFT OUTER JOIN stage_ax.SALESTABLE       AS ST ON CIJ.SALESID              = ST.SALESID
                                                            AND CIJ.PARTITION            = ST.PARTITION
                                                            AND CIJ.data_connection_id   = ST.data_connection_id
                                                            AND CIJ.company_id           = ST.company_id
             LEFT JOIN       inventory_transactions    AS IT ON CIT.INVENTTRANSID        = IT.INVENTTRANSID
                                                            AND CIT.INVOICEID            = IT.INVOICEID
                                                            AND CIT.PARTITION            = IT.PARTITION
                                                            AND CIT.company_id           = IT.CompanyID
                                                            AND CIT.data_connection_id   = IT.DataConnectionID
             LEFT JOIN       reporting_exchange_rates  AS ex ON ex.FromCurrencyCode      = CIT.CURRENCYCODE
                                                            AND ex.DataConnectionID      = CIT.data_connection_id
                                                            AND ex.Partition             = CIT.PARTITION
                                                            AND CIT.INVOICEDATE BETWEEN ex.ValidFrom AND ex.ValidTo
             LEFT JOIN       reporting_exchange_rates  AS ex2 ON ex2.FromCurrencyCode    = IT.ACCOUNTINGCURRENCY
                                                             AND ex2.DataConnectionID    = IT.DataConnectionID
                                                             AND ex2.Partition           = IT.PARTITION
                                                             AND CIT.INVOICEDATE BETWEEN ex2.ValidFrom AND ex2.ValidTo
            WHERE
             /** FULL LOAD **/
                             ( @load_type         = 0
                           AND CIT.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                       = 1
                           AND @execution_flag                  = 'N'
                           AND CIT.execution_flag               = @execution_flag
                           AND CIT.execution_timestamp          > @last_processed_timestamp)
               OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( CIT.execution_flag           = @execution_flag
                             AND CIT.execution_timestamp      > @last_processed_timestamp)
                            OR ( ID.execution_flag            = @execution_flag
                             AND ID.execution_timestamp       > @last_processed_timestamp)
                            OR ( ST.execution_flag          = @execution_flag
                             AND ST.execution_timestamp     > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                       = 1
                           AND @execution_flag                  = 'D'
                           AND CIT.execution_flag               = @execution_flag
                           AND CIT.execution_timestamp          > @last_processed_timestamp)) ;
GO

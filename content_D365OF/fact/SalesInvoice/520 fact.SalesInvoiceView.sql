EXEC dbo.drop_object @object = N'fact.SalesInvoiceView', @type = N'V' ;
GO
CREATE VIEW fact.SalesInvoiceView
AS
  SELECT
    --	/***BK***/
                    fsi.StageId                             AS StageID
                  , fsi.ExecutionFlag                       AS ExecutionFlag
                  , fsi.SalesInvoiceLineRecId               AS SalesInvoiceLineRecID
                  , fsi.SalesInvoiceLinePartition           AS SalesInvoiceLinePartition
                  , fsi.CompanyId                           AS CompanyID
                  , fsi.DataConnectionID                    AS DataConnectionID

                  --	 /***Dimensions*/
                  --	 --Dates
                  , COALESCE(dd.DateID, 0)                  AS DocumentDateID
                  , COALESCE(id.DateID, 0)                  AS InvoiceDateID
                  , COALESCE(sd.DateID, 0)                  AS DeliveryDateID
                  , COALESCE(rrd.DateID, 0)                 AS ReceiptRequestedDateID
                  , COALESCE(srd.DateID, 0)                 AS ShippingRequestedDateID
                  , COALESCE(rcd.DateID, 0)                 AS ReceiptConfirmedDateID
                  , COALESCE(scd.DateID, 0)                 AS ShippingConfirmedDateID
                  -- inventdimension                              
                  , COALESCE(wl.WMSLocationID, 0)           AS WMSlocationID
                  , COALESCE(l.LocationID, 0)               AS LocationID
                  , COALESCE(p.ProductID, 0)                AS ProductID
                  , COALESCE(t.TrackingID, 0)               AS TrackingID
                  --	--employee                                  
                  , COALESCE(stEmp.EmployeeID, 0)           AS SalesTakerID
                  , COALESCE(srEmp.EmployeeID, 0)           AS SalesResponsibleID
                  --	 --customer                                 
                  , COALESCE(cAcc.CustomerID, 0)            AS CustomerAccountID
                  , COALESCE(iAcc.CustomerID, 0)            AS InvoiceAccountID
                  --	-- address                                  
                  , COALESCE(dAdr.AddressID, 0)             AS DeliveryAddressID
                  , COALESCE(iAdr.AddressID, 0)             AS InvoiceAddressID
                  --	-- item                                     
                  , COALESCE(i.ItemID, 0)                   AS ItemID
                  --	 -- currency                                
                  , COALESCE(cy.CurrencyID, 0)              AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)             AS CurrencyReportID
                  --	 -- financialdimension                      
                  , COALESCE(fd.FinancialDimensionsID, 0)   AS FinancialDimensionsID
                  --	 -- reason code                             
                  , COALESCE(jv.VoucherID, 0)               AS [VoucherID]
                  , COALESCE(r.ReasonID, 0)                 AS ReasonID
                  , COALESCE(sot.SalesOrderTypeID, 0)       AS OrderTypeID
                  , COALESCE(orign.SalesOriginID, 0)        AS SalesOriginID
                  , COALESCE(sg.SalesGroupID, 0)            AS SalesGroupID
                  , COALESCE(modlv.ModeOfDeliveryID, 0)     AS ModeOfDeliveryID
                  , COALESCE(dt.DeliveryTermsID, 0)         AS DeliveryTermsID
                  , COALESCE(pt.PaymentTermsID, 0)          AS PaymentTermsID
                  , COALESCE(mop.MethodOfPaymentID, 0)      AS [MethodOfPaymentID]
                  , COALESCE(sp.SalesPoolID, 0)             AS [SalesPoolID]
                  , COALESCE(so.SalesOrderID, 0)            AS SalesOrderID
                  , COALESCE(uom.UnitOfMeasureID, 0)        AS SalesUnitID
                  , COALESCE(sInv.SalesInvoiceID, 0)        AS SalesInvoiceID

                  --	/***Measures */                             
                  , fsi.InvoicedQuantity                    AS InvoicedQuantity
                  , fsi.InventoryQuantity                   AS InventoryQuantity
                  , fsi.DeliveredWithoutPackingSlipQuantity AS DeliveredWithoutPackingSlipQuantity
                  , fsi.SalesPrice                          AS SalesPrice
                  , fsi.SalesPrice_RCY                      AS SalesPrice_RCY
                  , fsi.Discount                            AS Discount
                  , fsi.Discount_RCY                        AS Discount_RCY
                  , fsi.DiscountPercent                     AS DiscountPercent
                  , fsi.MultiLineDiscount                   AS MultilineDiscount
                  , fsi.MultiLineDiscount_RCY               AS MultilineDiscount_RCY
                  , fsi.MultiLineDiscountPercent            AS MultilineDiscountPercent
                  , fsi.NetAmount                           AS NetAmount
                  , fsi.NetAmount_RCY                       AS NetAmount_RCY
                  , fsi.NetAmountMST                        AS NetAmountMST
                  , fsi.COGSMST                             AS COGSMST
                  , fsi.COGS_RCY                            AS COGS_RCY
                  , fsi.TaxAmount                           AS TaxAmount
                  , fsi.TaxAmount_RCY                       AS TaxAmount_RCY
                  , fsi.TaxAmountMST                        AS TaxAmountMST
                  , fsi.AmountExclTax                       AS AmountExclTax
                  , fsi.AmountExclTaxMST                    AS AmountExclTaxMST
                  , fsi.DiscountAmount                      AS DiscountAmount
                  , fsi.DiscountAmount_RCY                  AS DiscountAmount_RCY
 
    FROM            delta.SalesInvoice      AS fsi
    LEFT JOIN       dim.Date                AS dd ON dd.Date                        = fsi.DocumentDate
    LEFT JOIN       dim.Date                AS id ON id.Date                        = fsi.InvoiceDate
    LEFT JOIN       dim.Date                AS sd ON sd.Date                        = fsi.ShipDate
    LEFT JOIN       dim.Date                AS rrd ON rrd.Date                      = fsi.ReceiptRequestedDate
    LEFT JOIN       dim.Date                AS srd ON srd.Date                      = fsi.ShippingRequestedDate
    LEFT JOIN       dim.Date                AS rcd ON rcd.Date                      = fsi.ReceiptConfirmedDate
    LEFT JOIN       dim.Date                AS scd ON scd.Date                      = fsi.ShippingConfirmedDate
    LEFT JOIN       dim.WMSLocation         AS wl ON wl.WMSLocationCode             = fsi.WMSlocationCode
                                                 AND wl.LocationCode                = fsi.LocationCode
                                                 AND wl.Partition                   = fsi.SalesInvoiceLinePartition
                                                 AND wl.DataConnectionID            = fsi.DataConnectionID
                                                 AND wl.CompanyID                   = fsi.CompanyId
    LEFT JOIN       dim.Location            AS l ON l.LocationCode                  = fsi.LocationCode
                                                AND l.Partition                     = fsi.SalesInvoiceLinePartition
                                                AND l.DataConnectionID              = fsi.DataConnectionID
                                                AND l.CompanyID                     = fsi.CompanyId
    LEFT JOIN       dim.Product             AS p ON p.InventSize                    = fsi.ProductInventSizeCode
                                                AND p.InventColor                   = fsi.ProductColorCode
                                                AND p.InventStyle                   = fsi.ProductStyleCode
                                                AND p.InventConfiguration           = fsi.ProductConfigCode
                                                AND p.Partition                     = fsi.SalesInvoiceLinePartition
                                                AND p.DataConnectionID              = fsi.DataConnectionID
                                                AND p.CompanyID                     = fsi.CompanyId
    LEFT JOIN       dim.Tracking            AS t ON t.InventBatchCode               = fsi.TrackingBatchCode
                                                AND t.InventSerialCode              = fsi.TrackingSerialCode
                                                AND t.Partition                     = fsi.SalesInvoiceLinePartition
                                                AND t.DataConnectionID              = fsi.DataConnectionID
                                                AND t.CompanyID                     = fsi.CompanyId
    LEFT JOIN       dim.Customer            AS cAcc ON cAcc.CustomerAccountNo       = fsi.CustomerAccountCode
                                                   AND cAcc.Partition               = fsi.SalesInvoiceLinePartition
                                                   AND cAcc.DataConnectionID        = fsi.DataConnectionID
                                                   AND cAcc.CompanyID               = fsi.CompanyId
    LEFT JOIN       dim.Customer            AS iAcc ON iAcc.CustomerAccountNo       = fsi.InvoiceAccountCode
                                                   AND iAcc.Partition               = fsi.SalesInvoiceLinePartition
                                                   AND iAcc.DataConnectionID        = fsi.DataConnectionID
                                                   AND iAcc.CompanyID               = fsi.CompanyId
    LEFT JOIN       dim.Address             AS dAdr ON dAdr.AddressRecID            = fsi.DeliveryAddressCode
                                                   AND dAdr.Partition               = fsi.SalesInvoiceLinePartition
                                                   AND dAdr.DataConnectionID        = fsi.DataConnectionID
    LEFT JOIN       dim.Address             AS iAdr ON iAdr.AddressRecID            = fsi.DeliveryAddressCode
                                                   AND iAdr.Partition               = fsi.SalesInvoiceLinePartition
                                                   AND iAdr.DataConnectionID        = fsi.DataConnectionID
    LEFT JOIN       dim.Item                AS i ON i.ItemNumber                    = fsi.ItemCode
                                                AND i.Partition                     = fsi.SalesInvoiceLinePartition
                                                AND i.DataConnectionID              = fsi.DataConnectionID
                                                AND i.CompanyID                     = fsi.CompanyId
    LEFT JOIN       dim.FinancialDimensions AS fd ON fd.DefaultDimension            = fsi.DefaultDimensionRecId
                                                 AND fd.Partition                   = fsi.SalesInvoiceLinePartition
                                                 AND fd.DataConnectionID            = fsi.DataConnectionID
    LEFT JOIN       dim.PaymentTerms        AS pt ON pt.PaymentTermsCode            = fsi.PaymentTermCode
                                                 AND pt.Partition                   = fsi.SalesInvoiceLinePartition
                                                 AND pt.DataConnectionID            = fsi.DataConnectionID
                                                 AND pt.CompanyID                   = fsi.CompanyId
    LEFT JOIN       dim.ModeOfDelivery      AS modlv ON modlv.ModeOfDeliveryCode    = fsi.DeliveryModeCode
                                                    AND modlv.Partition             = fsi.SalesInvoiceLinePartition
                                                    AND modlv.DataConnectionID      = fsi.DataConnectionID
                                                    AND modlv.CompanyID             = fsi.CompanyId
    LEFT JOIN       dim.DeliveryTerms       AS dt ON dt.DeliveryTermsCode           = fsi.DeliveryTermCode
                                                 AND dt.Partition                   = fsi.SalesInvoiceLinePartition
                                                 AND dt.DataConnectionID            = fsi.DataConnectionID
                                                 AND dt.CompanyID                   = fsi.CompanyId
    LEFT JOIN       dim.SalesGroup          AS sg ON sg.SalesGroupCode              = fsi.SalesGroupCode
                                                 AND sg.Partition                   = fsi.SalesInvoiceLinePartition
                                                 AND sg.DataConnectionID            = fsi.DataConnectionID
                                                 AND sg.CompanyID                   = fsi.CompanyId
    LEFT JOIN       dim.SalesOrigin         AS orign ON orign.SalesOriginCode       = fsi.SalesOriginCode
                                                    AND orign.DataConnectionID      = fsi.DataConnectionID
                                                    AND orign.CompanyID             = fsi.CompanyId
    LEFT JOIN       dim.SalesOrder          AS so ON so.SalesOrder                  = fsi.SalesOrderCode
                                                 AND so.Partition                   = fsi.SalesInvoiceLinePartition
                                                 AND so.DataConnectionID            = fsi.DataConnectionID
                                                 AND so.CompanyID                   = fsi.CompanyId
    LEFT JOIN       dim.SalesOrderType      AS sot ON sot.SalesOrderTypeCode        = fsi.SalesOrderTypeCode
                                                  AND sot.DataConnectionID          = fsi.DataConnectionID
    LEFT JOIN       dim.SalesInvoice        AS sInv ON sInv.SalesInvoice            = fsi.SalesInvoice
                                                   AND sInv.CompanyID               = fsi.CompanyId
                                                   AND sInv.Partition               = fsi.SalesInvoiceLinePartition
                                                   AND sInv.DataConnectionID        = fsi.DataConnectionID
    LEFT JOIN       dim.Currency            AS cy ON cy.Code                        = fsi.CurrencyCode
                                                 AND cy.Partition                   = fsi.SalesInvoiceLinePartition
                                                 AND cy.DataConnectionID            = fsi.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport      AS rcy ON fsi.SalesInvoiceLinePartition = rcy.Partition
    LEFT JOIN       dim.Reason              AS r ON r.ReasonRefRecId                = fsi.ReasonCode
                                                AND r.DataConnectionID              = fsi.DataConnectionID
                                                AND r.CompanyID                     = fsi.CompanyId
    LEFT JOIN       dim.Employee            AS stEmp ON stEmp.EmployeeRecID         = fsi.SalesTakerCode
                                                    AND stEmp.Partition             = fsi.SalesInvoiceLinePartition
                                                    AND stEmp.DataConnectionID      = fsi.DataConnectionID
    LEFT JOIN       dim.Employee            AS srEmp ON srEmp.EmployeeRecID         = fsi.SalesResponsibleCode
                                                    AND srEmp.Partition             = fsi.SalesInvoiceLinePartition
                                                    AND srEmp.DataConnectionID      = fsi.DataConnectionID
    LEFT JOIN       dim.UnitOfMeasure       AS uom ON uom.UnitOfMeasureCode         = fsi.SalesUnitOfMeasureCode
                                                  AND uom.Partition                 = fsi.SalesInvoiceLinePartition
                                                  AND uom.DataConnectionID          = fsi.DataConnectionID
    LEFT JOIN       dim.SalesPool           AS sp ON sp.SalesPoolCode               = fsi.SalesPoolCode
                                                 AND sp.Partition                   = fsi.SalesInvoiceLinePartition
                                                 AND sp.DataConnectionID            = fsi.DataConnectionID
                                                 AND sp.CompanyID                   = fsi.CompanyId
    LEFT JOIN       dim.MethodOfPayment     AS mop ON mop.MethodOfPaymentCode       = fsi.PaymentModeCode
                                                  AND mop.Partition                 = fsi.SalesInvoiceLinePartition
                                                  AND mop.DataConnectionID          = fsi.DataConnectionID
                                                  AND mop.CompanyID                 = fsi.CompanyId
    LEFT JOIN       dim.Voucher             AS jv ON jv.Voucher                     = fsi.VoucherCode
                                                 AND jv.CompanyId                   = fsi.CompanyId
                                                 AND jv.Partition                   = fsi.SalesInvoiceLinePartition
                                                 AND jv.DataConnectionID            = fsi.DataConnectionID ;



GO


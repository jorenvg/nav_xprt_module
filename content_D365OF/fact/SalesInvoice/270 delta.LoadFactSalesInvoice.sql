/****************************************************************************************************
    Functionality:  Loads the delta for SalesInvoice, either incrementally or with a full load based on the @load_type.
			Returns the @inserted, @updated, @deleted records that have been processed on the delta table.
    Created by:          Date:           	
    Date            Changed by      Ticket/Change       Description

*****************************************************************************************************/
EXEC dbo.Drop_Object @object = N'delta.LoadFactSalesInvoice' -- nvarchar(128)
                   , @type = N'P'                            -- nchar(2)
                   , @debug = 0 ;                            -- int
GO

CREATE PROCEDURE delta.LoadFactSalesInvoice
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @execution_flag NCHAR(1) = N'' ;
  DECLARE @table_name sysname = N'SalesInvoice' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  DECLARE @last_processed_timestamp BINARY(8) = (SELECT meta.get_last_processed_timestamp(@fact_table_identifier)) ;

  --truncate delta table
  EXECUTE dbo.truncate_table @delta_table_identifier ;

  IF @load_type = 1 --incremental
  BEGIN
    /****************************************************************************************************
  Functionality:  Incrementally inserts all new records for the SalesInvoice

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'N' ;

    INSERT INTO delta.SalesInvoice (
      StageID
    , ExecutionFlag
    , ComponentExecutionId
    , SalesInvoiceLineRecId
    , SalesInvoiceLinePartition
    , DataConnectionID
    , CompanyId
    , DocumentDate
    , InvoiceDate
    , ShipDate
    , ReceiptRequestedDate
    , ShippingRequestedDate
    , ReceiptConfirmedDate
    , ShippingConfirmedDate
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , CompanyCode
    , CustomerAccountCode
    , InvoiceAccountCode
    , DeliveryAddressCode
    , InvoiceAddressCode
    , ItemCode
    , SalesTakerCode
    , SalesResponsibleCode
    , CurrencyCode
    , DefaultDimensionRecId
    , VoucherCode
    , ReasonCode
    , DeliveryTermCode
    , DeliveryModeCode
    , PaymentTermCode
    , PaymentModeCode
    , SalesOrderTypeCode
    , SalesOriginCode
    , SalesGroupCode
    , SalesOrderCode
    , SalesPoolCode
    , SalesInvoice
    , NumberSequenceGroup
    , SalesUnitOfMeasureCode
    , SalesTaxGroupCode
    , SalesTaxItemGroupCode
    , InvoicedQuantity
    , InventoryQuantity
    , DeliveredWithoutPackingSlipQuantity
    , SalesPrice
    , SalesPrice_RCY
    , Discount
    , Discount_RCY
    , DiscountPercent
    , MultiLineDiscount
    , MultiLineDiscount_RCY
    , MultiLineDiscountPercent
    , NetAmount
    , NetAmount_RCY
    , NetAmountMST
    , COGSMST
    , COGS_RCY
    , TaxAmount
    , TaxAmount_RCY
    , TaxAmountMST
    , AmountExclTax
    , AmountExclTaxMST
    , DiscountAmount
    , DiscountAmount_RCY
    )
    SELECT StageID
         , ExecutionFlag
         , ComponentExecutionId
         , SalesInvoiceLineRecId
         , SalesInvoiceLinePartition
         , DataConnectionID
         , CompanyId
         , DocumentDate
         , InvoiceDate
         , ShipDate
         , ReceiptRequestedDate
         , ShippingRequestedDate
         , ReceiptConfirmedDate
         , ShippingConfirmedDate
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , CompanyCode
         , CustomerAccountCode
         , InvoiceAccountCode
         , DeliveryAddressCode
         , InvoiceAddressCode
         , ItemCode
         , SalesTakerCode
         , SalesResponsibleCode
         , CurrencyCode
         , DefaultDimensionRecId
         , VoucherCode
         , ReasonCode
         , DeliveryTermCode
         , DeliveryModeCode
         , PaymentTermCode
         , PaymentModeCode
         , SalesOrderTypeCode
         , SalesOriginCode
         , SalesGroupCode
         , SalesOrderCode
         , SalesPoolCode
         , SalesInvoice
         , NumberSequenceGroup
         , SalesUnitOfMeasureCode
         , SalesTaxGroupCode
         , SalesTaxItemGroupCode
         , InvoicedQuantity
         , InventoryQuantity
         , DeliveredWithoutPackingSlipQuantity
         , SalesPrice
         , SalesPrice_RCY
         , Discount
         , Discount_RCY
         , DiscountPercent
         , MultiLineDiscount
         , MultiLineDiscount_RCY
         , MultiLineDiscountPercent
         , NetAmount
         , NetAmount_RCY
         , NetAmountMST
         , COGSMST
         , COGS_RCY
         , TaxAmount
         , TaxAmount_RCY
         , TaxAmountMST
         , AmountExclTax
         , AmountExclTaxMST
         , DiscountAmount
         , DiscountAmount_RCY
      FROM delta.SalesInvoiceFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @Inserted = @@RowCount ;
    /****************************************************************************************************
  Functionality:  Incrementally inserts all updated records for the SalesInvoice

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'U' ;

    INSERT INTO delta.SalesInvoice (
      StageID
    , ExecutionFlag
    , ComponentExecutionId
    , SalesInvoiceLineRecId
    , SalesInvoiceLinePartition
    , DataConnectionID
    , CompanyId
    , DocumentDate
    , InvoiceDate
    , ShipDate
    , ReceiptRequestedDate
    , ShippingRequestedDate
    , ReceiptConfirmedDate
    , ShippingConfirmedDate
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , CompanyCode
    , CustomerAccountCode
    , InvoiceAccountCode
    , DeliveryAddressCode
    , InvoiceAddressCode
    , ItemCode
    , SalesTakerCode
    , SalesResponsibleCode
    , CurrencyCode
    , DefaultDimensionRecId
    , VoucherCode
    , ReasonCode
    , DeliveryTermCode
    , DeliveryModeCode
    , PaymentTermCode
    , PaymentModeCode
    , SalesOrderTypeCode
    , SalesOriginCode
    , SalesGroupCode
    , SalesOrderCode
    , SalesPoolCode
    , SalesInvoice
    , NumberSequenceGroup
    , SalesUnitOfMeasureCode
    , SalesTaxGroupCode
    , SalesTaxItemGroupCode
    , InvoicedQuantity
    , InventoryQuantity
    , DeliveredWithoutPackingSlipQuantity
    , SalesPrice
    , SalesPrice_RCY
    , Discount
    , Discount_RCY
    , DiscountPercent
    , MultiLineDiscount
    , MultiLineDiscount_RCY
    , MultiLineDiscountPercent
    , NetAmount
    , NetAmount_RCY
    , NetAmountMST
    , COGSMST
    , COGS_RCY
    , TaxAmount
    , TaxAmount_RCY
    , TaxAmountMST
    , AmountExclTax
    , AmountExclTaxMST
    , DiscountAmount
    , DiscountAmount_RCY
    )
    SELECT StageID
         , ExecutionFlag
         , ComponentExecutionId
         , SalesInvoiceLineRecId
         , SalesInvoiceLinePartition
         , DataConnectionID
         , CompanyId
         , DocumentDate
         , InvoiceDate
         , ShipDate
         , ReceiptRequestedDate
         , ShippingRequestedDate
         , ReceiptConfirmedDate
         , ShippingConfirmedDate
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , CompanyCode
         , CustomerAccountCode
         , InvoiceAccountCode
         , DeliveryAddressCode
         , InvoiceAddressCode
         , ItemCode
         , SalesTakerCode
         , SalesResponsibleCode
         , CurrencyCode
         , DefaultDimensionRecId
         , VoucherCode
         , ReasonCode
         , DeliveryTermCode
         , DeliveryModeCode
         , PaymentTermCode
         , PaymentModeCode
         , SalesOrderTypeCode
         , SalesOriginCode
         , SalesGroupCode
         , SalesOrderCode
         , SalesPoolCode
         , SalesInvoice
         , NumberSequenceGroup
         , SalesUnitOfMeasureCode
         , SalesTaxGroupCode
         , SalesTaxItemGroupCode
         , InvoicedQuantity
         , InventoryQuantity
         , DeliveredWithoutPackingSlipQuantity
         , SalesPrice
         , SalesPrice_RCY
         , Discount
         , Discount_RCY
         , DiscountPercent
         , MultiLineDiscount
         , MultiLineDiscount_RCY
         , MultiLineDiscountPercent
         , NetAmount
         , NetAmount_RCY
         , NetAmountMST
         , COGSMST
         , COGS_RCY
         , TaxAmount
         , TaxAmount_RCY
         , TaxAmountMST
         , AmountExclTax
         , AmountExclTaxMST
         , DiscountAmount
         , DiscountAmount_RCY

      FROM delta.SalesInvoiceFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @Inserted = @@RowCount ;
    /****************************************************************************************************
  Functionality:  Incrementally inserts all deleted records for SalesInvoice

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'D' ;

    INSERT INTO delta.SalesInvoice (
      StageID
    , ExecutionFlag
    , ComponentExecutionId
    , SalesInvoiceLineRecId
    , SalesInvoiceLinePartition
    , DataConnectionID
    , CompanyId
    , DocumentDate
    , InvoiceDate
    , ShipDate
    , ReceiptRequestedDate
    , ShippingRequestedDate
    , ReceiptConfirmedDate
    , ShippingConfirmedDate
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , CompanyCode
    , CustomerAccountCode
    , InvoiceAccountCode
    , DeliveryAddressCode
    , InvoiceAddressCode
    , ItemCode
    , SalesTakerCode
    , SalesResponsibleCode
    , CurrencyCode
    , DefaultDimensionRecId
    , VoucherCode
    , ReasonCode
    , DeliveryTermCode
    , DeliveryModeCode
    , PaymentTermCode
    , PaymentModeCode
    , SalesOrderTypeCode
    , SalesOriginCode
    , SalesGroupCode
    , SalesOrderCode
    , SalesPoolCode
    , SalesInvoice
    , NumberSequenceGroup
    , SalesUnitOfMeasureCode
    , SalesTaxGroupCode
    , SalesTaxItemGroupCode
    , InvoicedQuantity
    , InventoryQuantity
    , DeliveredWithoutPackingSlipQuantity
    , SalesPrice
    , SalesPrice_RCY
    , Discount
    , Discount_RCY
    , DiscountPercent
    , MultiLineDiscount
    , MultiLineDiscount_RCY
    , MultiLineDiscountPercent
    , NetAmount
    , NetAmount_RCY
    , NetAmountMST
    , COGSMST
    , COGS_RCY
    , TaxAmount
    , TaxAmount_RCY
    , TaxAmountMST
    , AmountExclTax
    , AmountExclTaxMST
    , DiscountAmount
    , DiscountAmount_RCY
    )
    SELECT StageID
         , ExecutionFlag
         , ComponentExecutionId
         , SalesInvoiceLineRecId
         , SalesInvoiceLinePartition
         , DataConnectionID
         , CompanyId
         , DocumentDate
         , InvoiceDate
         , ShipDate
         , ReceiptRequestedDate
         , ShippingRequestedDate
         , ReceiptConfirmedDate
         , ShippingConfirmedDate
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , CompanyCode
         , CustomerAccountCode
         , InvoiceAccountCode
         , DeliveryAddressCode
         , InvoiceAddressCode
         , ItemCode
         , SalesTakerCode
         , SalesResponsibleCode
         , CurrencyCode
         , DefaultDimensionRecId
         , VoucherCode
         , ReasonCode
         , DeliveryTermCode
         , DeliveryModeCode
         , PaymentTermCode
         , PaymentModeCode
         , SalesOrderTypeCode
         , SalesOriginCode
         , SalesGroupCode
         , SalesOrderCode
         , SalesPoolCode
         , SalesInvoice
         , NumberSequenceGroup
         , SalesUnitOfMeasureCode
         , SalesTaxGroupCode
         , SalesTaxItemGroupCode
         , InvoicedQuantity
         , InventoryQuantity
         , DeliveredWithoutPackingSlipQuantity
         , SalesPrice
         , SalesPrice_RCY
         , Discount
         , Discount_RCY
         , DiscountPercent
         , MultiLineDiscount
         , MultiLineDiscount_RCY
         , MultiLineDiscountPercent
         , NetAmount
         , NetAmount_RCY
         , NetAmountMST
         , COGSMST
         , COGS_RCY
         , TaxAmount
         , TaxAmount_RCY
         , TaxAmountMST
         , AmountExclTax
         , AmountExclTaxMST
         , DiscountAmount
         , DiscountAmount_RCY
      FROM delta.SalesInvoiceFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @Inserted = @@RowCount ;
  END ;
  /****************************************************************************************************
    Functionality:  During a full load with insert all records that have not been deleted. 

    Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
  IF @load_type = 0 --full load
  BEGIN
    SET @deleted = 0 ;
    SET @updated = 0 ;
    SET @execution_flag = N'N' ;

    INSERT INTO delta.SalesInvoice (
      StageID
    , ExecutionFlag
    , ComponentExecutionId
    , SalesInvoiceLineRecId
    , SalesInvoiceLinePartition
    , DataConnectionID
    , CompanyId
    , DocumentDate
    , InvoiceDate
    , ShipDate
    , ReceiptRequestedDate
    , ShippingRequestedDate
    , ReceiptConfirmedDate
    , ShippingConfirmedDate
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , CompanyCode
    , CustomerAccountCode
    , InvoiceAccountCode
    , DeliveryAddressCode
    , InvoiceAddressCode
    , ItemCode
    , SalesTakerCode
    , SalesResponsibleCode
    , CurrencyCode
    , DefaultDimensionRecId
    , VoucherCode
    , ReasonCode
    , DeliveryTermCode
    , DeliveryModeCode
    , PaymentTermCode
    , PaymentModeCode
    , SalesOrderTypeCode
    , SalesOriginCode
    , SalesGroupCode
    , SalesOrderCode
    , SalesPoolCode
    , SalesInvoice
    , NumberSequenceGroup
    , SalesUnitOfMeasureCode
    , SalesTaxGroupCode
    , SalesTaxItemGroupCode
    , InvoicedQuantity
    , InventoryQuantity
    , DeliveredWithoutPackingSlipQuantity
    , SalesPrice
    , SalesPrice_RCY
    , Discount
    , Discount_RCY
    , DiscountPercent
    , MultiLineDiscount
    , MultiLineDiscount_RCY
    , MultiLineDiscountPercent
    , NetAmount
    , NetAmount_RCY
    , NetAmountMST
    , COGSMST
    , COGS_RCY
    , TaxAmount
    , TaxAmount_RCY
    , TaxAmountMST
    , AmountExclTax
    , AmountExclTaxMST
    , DiscountAmount
    , DiscountAmount_RCY

    )
    SELECT StageID
         , ExecutionFlag
         , ComponentExecutionId
         , SalesInvoiceLineRecId
         , SalesInvoiceLinePartition
         , DataConnectionID
         , CompanyId
         , DocumentDate
         , InvoiceDate
         , ShipDate
         , ReceiptRequestedDate
         , ShippingRequestedDate
         , ReceiptConfirmedDate
         , ShippingConfirmedDate
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , CompanyCode
         , CustomerAccountCode
         , InvoiceAccountCode
         , DeliveryAddressCode
         , InvoiceAddressCode
         , ItemCode
         , SalesTakerCode
         , SalesResponsibleCode
         , CurrencyCode
         , DefaultDimensionRecId
         , VoucherCode
         , ReasonCode
         , DeliveryTermCode
         , DeliveryModeCode
         , PaymentTermCode
         , PaymentModeCode
         , SalesOrderTypeCode
         , SalesOriginCode
         , SalesGroupCode
         , SalesOrderCode
         , SalesPoolCode
         , SalesInvoice
         , NumberSequenceGroup
         , SalesUnitOfMeasureCode
         , SalesTaxGroupCode
         , SalesTaxItemGroupCode
         , InvoicedQuantity
         , InventoryQuantity
         , DeliveredWithoutPackingSlipQuantity
         , SalesPrice
         , SalesPrice_RCY
         , Discount
         , Discount_RCY
         , DiscountPercent
         , MultiLineDiscount
         , MultiLineDiscount_RCY
         , MultiLineDiscountPercent
         , NetAmount
         , NetAmount_RCY
         , NetAmountMST
         , COGSMST
         , COGS_RCY
         , TaxAmount
         , TaxAmount_RCY
         , TaxAmountMST
         , AmountExclTax
         , AmountExclTaxMST 
         , DiscountAmount
         , DiscountAmount_RCY
      FROM delta.SalesInvoiceFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @Inserted = @@RowCount ;
  END ;
END ;


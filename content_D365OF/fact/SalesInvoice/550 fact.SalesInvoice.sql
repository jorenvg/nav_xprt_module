EXEC dbo.drop_object 'fact.SalesInvoice', 'T' ;
GO
CREATE TABLE [fact].[SalesInvoice]
(
  [SalesInvoiceLineRecID]               [BIGINT]          NULL
, SalesInvoiceLinePartition             BIGINT            NULL
, [CompanyID]                           [INT]             NOT NULL
, [DataConnectionID]                    [INT]             NOT NULL
, [ComponentExecutionID]                INT               NOT NULL
, [StageID]                             INT               NOT NULL
, [DocumentDateID]                      [INT]             NOT NULL
, [InvoiceDateID]                       [INT]             NOT NULL
, [DeliveryDateID]                      [INT]             NOT NULL
, ReceiptRequestedDateID                [INT]             NOT NULL
, ShippingRequestedDateID               [INT]             NOT NULL
, ReceiptConfirmedDateID                [INT]             NOT NULL
, ShippingConfirmedDateID               [INT]             NOT NULL
, [WMSlocationID]                       [INT]             NOT NULL
, [LocationID]                          [INT]             NOT NULL
, [ProductID]                           [INT]             NOT NULL
, [TrackingID]                          [INT]             NOT NULL
, [SalesTakerID]                        [INT]             NOT NULL
, [SalesResponsibleID]                  [INT]             NOT NULL
, [CustomerAccountID]                   [INT]             NOT NULL
, [InvoiceAccountID]                    [INT]             NOT NULL
, [DeliveryAddressID]                   [INT]             NOT NULL
, [InvoiceAddressID]                    [INT]             NOT NULL
, [ItemID]                              [INT]             NOT NULL
, CurrencyTransactionID                 INT               NOT NULL
, CurrencyReportID                      INT               NOT NULL
, [FinancialDimensionsID]               [INT]             NOT NULL
, [VoucherID]                           [INT]             NOT NULL
, [ReasonID]                            [INT]             NOT NULL
, [OrderTypeID]                         [INT]             NOT NULL
, [SalesOriginID]                       [INT]             NOT NULL
, [SalesGroupID]                        [INT]             NOT NULL
, [ModeOfDeliveryID]                    [INT]             NOT NULL
, MethodOfPaymentID                     INT               NOT NULL
, [DeliveryTermsID]                     [INT]             NOT NULL
, [PaymentTermsID]                      [INT]             NOT NULL
, [SalesPoolID]                         INT               NOT NULL
, [SalesOrderID]                        [INT]             NOT NULL
, [SalesUnitID]                         [INT]             NOT NULL
, [SalesInvoiceID]                      INT               NOT NULL
, [InvoicedQuantity]                    DECIMAL(19,4)     NULL
, [InventoryQuantity]                   DECIMAL(19,4)     NULL
, [DeliveredWithoutPackingSlipQuantity] DECIMAL(19,4)     NULL
, [SalesPrice]                          DECIMAL(19,4)     NULL
, [SalesPrice_RCY]                      DECIMAL(19,4)     NULL
, [Discount]                            DECIMAL(19,4)     NULL
, [Discount_RCY]                        DECIMAL(19,4)     NULL
, [DiscountPercent]                     DECIMAL(19,4)     NULL
, [MultilineDiscount]                   DECIMAL(19,4)     NULL
, [MultilineDiscount_RCY]               DECIMAL(19,4)     NULL
, [MultilineDiscountPercent]            DECIMAL(19,4)     NULL
, [NetAmount]                           DECIMAL(19,4)     NULL
, [NetAmount_RCY]                       DECIMAL(19,4)     NULL
, [NetAmountMST]                        DECIMAL(19,4)     NULL
, [COGSMST]                             DECIMAL(19,4)     NULL
, [COGS_RCY]                            DECIMAL(19,4)     NULL
, [TaxAmount]                           DECIMAL(19,4)     NULL
, [TaxAmount_RCY]                       DECIMAL(19,4)     NULL
, [TaxAmountMST]                        DECIMAL(19,4)     NULL
, [AmountExclTax]                       DECIMAL(19,4)     NULL
, [AmountExclTaxMST]                    DECIMAL(19,4)     NULL
, [DiscountAmount]                      DECIMAL(19,4)     NULL
, [DiscountAmount_RCY]                  DECIMAL(19,4)     NULL
) ;

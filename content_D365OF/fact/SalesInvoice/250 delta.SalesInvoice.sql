EXEC dbo.drop_object 'delta.SalesInvoice', 'T' ;
GO
CREATE TABLE [delta].[SalesInvoice]
(
  StageId                               BIGINT            NOT NULL
, ExecutionFlag                         NCHAR(1)          NOT NULL
, [ComponentExecutionID]                INT               NOT NULL
, [DataConnectionID]                    [INT]             NOT NULL
, [CompanyId]                           [INT]             NOT NULL
, [SalesInvoiceLineRecId]               [BIGINT]          NULL
, SalesInvoiceLinePartition             BIGINT            NULL
, [DocumentDate]                        [DATETIME]        NULL
, [InvoiceDate]                         [DATETIME]        NULL
, [ShipDate]                            [DATETIME]        NULL
, [ReceiptRequestedDate]                [DATETIME]        NULL
, [ShippingRequestedDate]               [DATETIME]        NULL
, [ReceiptConfirmedDate]                [DATETIME]        NULL
, [ShippingConfirmedDate]               [DATETIME]        NULL
, [WMSlocationCode]                     [NVARCHAR](100)   NULL
, [LocationCode]                        [NVARCHAR](100)   NULL
, [ProductConfigCode]                   [NVARCHAR](100)   NULL
, [ProductColorCode]                    [NVARCHAR](100)   NULL
, [ProductInventSizeCode]               [NVARCHAR](100)   NULL
, [ProductStyleCode]                    [NVARCHAR](100)   NULL
, [TrackingBatchCode]                   [NVARCHAR](100)   NULL
, [TrackingSerialCode]                  [NVARCHAR](100)   NULL
, [CompanyCode]                         [NVARCHAR](10)    NULL
, [CustomerAccountCode]                 [NVARCHAR](100)   NULL
, [InvoiceAccountCode]                  [NVARCHAR](100)   NULL
, [DeliveryAddressCode]                 [BIGINT]          NULL
, [InvoiceAddressCode]                  [BIGINT]          NULL
, [ItemCode]                            [NVARCHAR](100)   NULL
, [SalesTakerCode]                      [BIGINT]          NULL
, [SalesResponsibleCode]                [BIGINT]          NULL
, [CurrencyCode]                        [NVARCHAR](10)    NULL
, [DefaultDimensionRecId]               [BIGINT]          NULL
, [VoucherCode]                         [NVARCHAR](100)   NULL
, [ReasonCode]                          [BIGINT]          NULL
, [DeliveryTermCode]                    [NVARCHAR](100)   NULL
, [DeliveryModeCode]                    [NVARCHAR](100)   NULL
, [PaymentTermCode]                     [NVARCHAR](100)   NULL
, [PaymentModeCode]                     [NVARCHAR](100)   NULL
, [SalesOrderTypeCode]                  [INT]             NULL
, [SalesOriginCode]                     [NVARCHAR](100)   NULL
, [SalesGroupCode]                      [NVARCHAR](100)   NULL
, [SalesOrderCode]                      [NVARCHAR](100)   NULL
, [SalesPoolCode]                       [NVARCHAR](100)   NULL
, [SalesInvoice]                        [NVARCHAR](100)   NULL
, [NumberSequenceGroup]                 [NVARCHAR](10)    NULL
, [SalesUnitOfMeasureCode]              [NVARCHAR](100)   NULL
, [SalesTaxGroupCode]                   [NVARCHAR](100)   NULL
, [SalesTaxItemGroupCode]               [NVARCHAR](100)   NULL
, [InvoicedQuantity]                    DECIMAL(19,4)     NULL
, [InventoryQuantity]                   DECIMAL(19,4)     NULL
, [DeliveredWithoutPackingSlipQuantity] DECIMAL(19,4)     NULL
, [SalesPrice]                          DECIMAL(19,4)     NULL
, [SalesPrice_RCY]                      DECIMAL(19,4)     NULL
, [Discount]                            DECIMAL(19,4)     NULL
, [Discount_RCY]                        DECIMAL(19,4)     NULL
, [DiscountPercent]                     DECIMAL(19,4)     NULL
, [MultiLineDiscount]                   DECIMAL(19,4)     NULL
, [MultiLineDiscount_RCY]               DECIMAL(19,4)     NULL
, [MultiLineDiscountPercent]            DECIMAL(19,4)     NULL
, [NetAmount]                           DECIMAL(19,4)     NULL
, [NetAmount_RCY]                       DECIMAL(19,4)     NULL
, [NetAmountMST]                        DECIMAL(19,4)     NULL
, [COGSMST]                             DECIMAL(19,4)     NULL
, [COGS_RCY]                            DECIMAL(19,4)     NULL
, [TaxAmount]                           DECIMAL(19,4)     NULL
, [TaxAmount_RCY]                       DECIMAL(19,4)     NULL
, [TaxAmountMST]                        DECIMAL(19,4)     NULL
, [AmountExclTax]                       DECIMAL(19,4)     NULL
, [AmountExclTaxMST]                    DECIMAL(19,4)     NULL
, [DiscountAmount]                      DECIMAL(19,4)     NULL
, [DiscountAmount_RCY]                  DECIMAL(19,4)     NULL
) ;
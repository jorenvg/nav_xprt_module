EXEC dbo.drop_object @object = N'fact.LoadSalesInvoice' -- nvarchar(128)
                   , @type = N'P'                       -- nchar(2)
                   , @debug = 0 ;                       -- int
GO

CREATE PROCEDURE fact.LoadSalesInvoice
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  DECLARE @table_name sysname = N'SalesInvoice' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.SalesInvoice ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO fact.SalesInvoice (
      SalesInvoiceLineRecID
    , SalesInvoiceLinePartition
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , StageID
    , DocumentDateID
    , InvoiceDateID
    , DeliveryDateID
    , ReceiptRequestedDateID
    , ShippingRequestedDateID
    , ReceiptConfirmedDateID
    , ShippingConfirmedDateID
    , WMSlocationID
    , LocationID
    , ProductID
    , TrackingID
    , SalesTakerID
    , SalesResponsibleID
    , CustomerAccountID
    , InvoiceAccountID
    , DeliveryAddressID
    , InvoiceAddressID
    , ItemID
    , CurrencyTransactionID
    , CurrencyReportID
    , FinancialDimensionsID
    , VoucherID
    , ReasonID
    , OrderTypeID
    , SalesOriginID
    , SalesGroupID
    , ModeOfDeliveryID
    , MethodOfPaymentID
    , DeliveryTermsID
    , PaymentTermsID
    , SalesPoolID
    , SalesOrderID
    , SalesUnitID
    , SalesInvoiceID
    , InvoicedQuantity
    , InventoryQuantity
    , DeliveredWithoutPackingSlipQuantity
    , SalesPrice
    , SalesPrice_RCY
    , Discount
    , Discount_RCY
    , DiscountPercent
    , MultilineDiscount
    , MultilineDiscount_RCY
    , MultilineDiscountPercent
    , NetAmount
    , NetAmount_RCY
    , NetAmountMST
    , COGSMST
    , COGS_RCY
    , TaxAmount
    , TaxAmount_RCY
    , TaxAmountMST
    , AmountExclTax
    , AmountExclTaxMST
    , DiscountAmount
    , DiscountAmount_RCY
    )
    SELECT vw.SalesInvoiceLineRecID
         , vw.SalesInvoiceLinePartition
         , vw.CompanyID
         , vw.DataConnectionID
         , @component_execution_id
         , vw.StageID
         , vw.DocumentDateID
         , vw.InvoiceDateID
         , vw.DeliveryDateID
         , vw.ReceiptRequestedDateID
         , vw.ShippingRequestedDateID
         , vw.ReceiptConfirmedDateID
         , vw.ShippingConfirmedDateID
         , vw.WMSlocationID
         , vw.LocationID
         , vw.ProductID
         , vw.TrackingID
         , vw.SalesTakerID
         , vw.SalesResponsibleID
         , vw.CustomerAccountID
         , vw.InvoiceAccountID
         , vw.DeliveryAddressID
         , vw.InvoiceAddressID
         , vw.ItemID
         , vw.CurrencyTransactionID
         , vw.CurrencyReportID
         , vw.FinancialDimensionsID
         , vw.VoucherID
         , vw.ReasonID
         , vw.OrderTypeID
         , vw.SalesOriginID
         , vw.SalesGroupID
         , vw.ModeOfDeliveryID
         , vw.MethodOfPaymentID
         , vw.DeliveryTermsID
         , vw.PaymentTermsID
         , vw.SalesPoolID
         , vw.SalesOrderID
         , vw.SalesUnitID
         , vw.SalesInvoiceID
         , vw.InvoicedQuantity
         , vw.InventoryQuantity
         , vw.DeliveredWithoutPackingSlipQuantity
         , vw.SalesPrice
         , vw.SalesPrice_RCY
         , vw.Discount
         , vw.Discount_RCY
         , vw.DiscountPercent
         , vw.MultilineDiscount
         , vw.MultilineDiscount_RCY
         , vw.MultilineDiscountPercent
         , vw.NetAmount
         , vw.NetAmount_RCY
         , vw.NetAmountMST
         , vw.COGSMST
         , vw.COGS_RCY
         , vw.TaxAmount
         , vw.TaxAmount_RCY
         , vw.TaxAmountMST
         , vw.AmountExclTax
         , vw.AmountExclTaxMST
         , vw.DiscountAmount
         , vw.DiscountAmount_RCY
      FROM fact.SalesInvoiceView AS vw ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.SalesInvoice
         SET      SalesInvoiceLineRecID = vw.SalesInvoiceLineRecID
                , SalesInvoiceLinePartition = vw.SalesInvoiceLinePartition
                , CompanyID = vw.CompanyID
                , DataConnectionID = vw.DataConnectionID
                , ComponentExecutionID = @component_execution_id
                , StageID = vw.StageID
                , DocumentDateID = vw.DocumentDateID
                , InvoiceDateID = vw.InvoiceDateID
                , DeliveryDateID = vw.DeliveryDateID
                , ReceiptRequestedDateID = vw.ReceiptRequestedDateID
                , ShippingRequestedDateID = vw.ShippingRequestedDateID
                , ReceiptConfirmedDateID = vw.ReceiptConfirmedDateID
                , ShippingConfirmedDateID = vw.ShippingConfirmedDateID
                , WMSlocationID = vw.WMSlocationID
                , LocationID = vw.LocationID
                , ProductID = vw.ProductID
                , TrackingID = vw.TrackingID
                , SalesTakerID = vw.SalesTakerID
                , SalesResponsibleID = vw.SalesResponsibleID
                , CustomerAccountID = vw.CustomerAccountID
                , InvoiceAccountID = vw.InvoiceAccountID
                , DeliveryAddressID = vw.DeliveryAddressID
                , InvoiceAddressID = vw.InvoiceAddressID
                , ItemID = vw.ItemID
                , CurrencyTransactionID = vw.CurrencyTransactionID
                , CurrencyReportID = vw.CurrencyReportID
                , FinancialDimensionsID = vw.FinancialDimensionsID
                , VoucherID = vw.VoucherID
                , ReasonID = vw.ReasonID
                , OrderTypeID = vw.OrderTypeID
                , SalesOriginID = vw.SalesOriginID
                , SalesGroupID = vw.SalesGroupID
                , ModeOfDeliveryID = vw.ModeOfDeliveryID
                , MethodOfPaymentID = vw.MethodOfPaymentID
                , DeliveryTermsID = vw.DeliveryTermsID
                , PaymentTermsID = vw.PaymentTermsID
                , SalesPoolID = vw.SalesPoolID
                , SalesOrderID = vw.SalesOrderID
                , SalesUnitID = vw.SalesUnitID
                , SalesInvoiceID = vw.SalesInvoiceID
                , InvoicedQuantity = vw.InvoicedQuantity
                , InventoryQuantity = vw.InventoryQuantity
                , DeliveredWithoutPackingSlipQuantity = vw.DeliveredWithoutPackingSlipQuantity
                , SalesPrice = vw.SalesPrice
                , SalesPrice_RCY = vw.SalesPrice_RCY
                , Discount = vw.Discount
                , Discount_RCY = vw.Discount_RCY
                , DiscountPercent = vw.DiscountPercent
                , MultilineDiscount = vw.MultilineDiscount
                , MultilineDiscount_RCY = vw.MultilineDiscount_RCY
                , MultilineDiscountPercent = vw.MultilineDiscountPercent
                , NetAmount = vw.NetAmount
                , NetAmount_RCY = vw.NetAmount_RCY
                , NetAmountMST = vw.NetAmountMST
                , COGSMST = vw.COGSMST
                , COGS_RCY = vw.COGS_RCY
                , TaxAmount = vw.TaxAmount
                , TaxAmount_RCY = vw.TaxAmount_RCY
                , TaxAmountMST = vw.TaxAmountMST
                , AmountExclTax = vw.AmountExclTax
                , AmountExclTaxMST = vw.AmountExclTaxMST
                , DiscountAmount = vw.DiscountAmount
                , DiscountAmount_RCY = vw.DiscountAmount_RCY
        FROM      fact.SalesInvoice     AS fct
       INNER JOIN fact.SalesInvoiceView AS vw ON fct.StageID = vw.StageID
       WHERE      vw.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.SalesInvoice (
        SalesInvoiceLineRecID
      , SalesInvoiceLinePartition
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , StageID
      , DocumentDateID
      , InvoiceDateID
      , DeliveryDateID
      , ReceiptRequestedDateID
      , ShippingRequestedDateID
      , ReceiptConfirmedDateID
      , ShippingConfirmedDateID
      , WMSlocationID
      , LocationID
      , ProductID
      , TrackingID
      , SalesTakerID
      , SalesResponsibleID
      , CustomerAccountID
      , InvoiceAccountID
      , DeliveryAddressID
      , InvoiceAddressID
      , ItemID
      , CurrencyTransactionID
      , CurrencyReportID
      , FinancialDimensionsID
      , VoucherID
      , ReasonID
      , OrderTypeID
      , SalesOriginID
      , SalesGroupID
      , ModeOfDeliveryID
      , MethodOfPaymentID
      , DeliveryTermsID
      , PaymentTermsID
      , SalesPoolID
      , SalesOrderID
      , SalesUnitID
      , SalesInvoiceID
      , InvoicedQuantity
      , InventoryQuantity
      , DeliveredWithoutPackingSlipQuantity
      , SalesPrice
      , SalesPrice_RCY
      , Discount
      , Discount_RCY
      , DiscountPercent
      , MultilineDiscount
      , MultilineDiscount_RCY
      , MultilineDiscountPercent
      , NetAmount
      , NetAmount_RCY
      , NetAmountMST
      , COGSMST
      , COGS_RCY
      , TaxAmount
      , TaxAmount_RCY
      , TaxAmountMST
      , AmountExclTax
      , AmountExclTaxMST
      , DiscountAmount
      , DiscountAmount_RCY
      )
      SELECT vw.SalesInvoiceLineRecID
           , vw.SalesInvoiceLinePartition
           , vw.CompanyID
           , vw.DataConnectionID
           , @component_execution_id
           , vw.StageID
           , vw.DocumentDateID
           , vw.InvoiceDateID
           , vw.DeliveryDateID
           , vw.ReceiptRequestedDateID
           , vw.ShippingRequestedDateID
           , vw.ReceiptConfirmedDateID
           , vw.ShippingConfirmedDateID
           , vw.WMSlocationID
           , vw.LocationID
           , vw.ProductID
           , vw.TrackingID
           , vw.SalesTakerID
           , vw.SalesResponsibleID
           , vw.CustomerAccountID
           , vw.InvoiceAccountID
           , vw.DeliveryAddressID
           , vw.InvoiceAddressID
           , vw.ItemID
           , vw.CurrencyTransactionID
           , vw.CurrencyReportID
           , vw.FinancialDimensionsID
           , vw.VoucherID
           , vw.ReasonID
           , vw.OrderTypeID
           , vw.SalesOriginID
           , vw.SalesGroupID
           , vw.ModeOfDeliveryID
           , vw.MethodOfPaymentID
           , vw.DeliveryTermsID
           , vw.PaymentTermsID
           , vw.SalesPoolID
           , vw.SalesOrderID
           , vw.SalesUnitID
           , vw.SalesInvoiceID
           , vw.InvoicedQuantity
           , vw.InventoryQuantity
           , vw.DeliveredWithoutPackingSlipQuantity
           , vw.SalesPrice
           , vw.SalesPrice_RCY
           , vw.Discount
           , vw.Discount_RCY
           , vw.DiscountPercent
           , vw.MultilineDiscount
           , vw.MultilineDiscount_RCY
           , vw.MultilineDiscountPercent
           , vw.NetAmount
           , vw.NetAmount_RCY
           , vw.NetAmountMST
           , vw.COGSMST
           , vw.COGS_RCY
           , vw.TaxAmount
           , vw.TaxAmount_RCY
           , vw.TaxAmountMST
           , vw.AmountExclTax
           , vw.AmountExclTaxMST
           , vw.DiscountAmount
           , vw.DiscountAmount_RCY
        FROM fact.SalesInvoiceView AS vw
       WHERE vw.ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.SalesInvoice AS F WHERE F.StageID = vw.StageID) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.SalesInvoice
       WHERE EXISTS ( SELECT 1
                        FROM fact.SalesInvoiceView AS vw
                       WHERE vw.ExecutionFlag = 'D'
                         AND vw.StageID       = fact.SalesInvoice.StageID) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
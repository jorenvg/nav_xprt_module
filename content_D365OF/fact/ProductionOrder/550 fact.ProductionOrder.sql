EXEC dbo.drop_object 'fact.ProductionOrder', 'T' ;
GO
CREATE TABLE [fact].[ProductionOrder]
(
  [StageId]                  [BIGINT]   NOT NULL
, [ExecutionFlag]            [NCHAR](1) NOT NULL
, [ComponentExecutionID]     [INT]      NOT NULL
, [Partition]                [BIGINT]   NULL
, [DataConnectionID]         [INT]      NOT NULL
, [RecID]                    [BIGINT]   NULL
, [CompanyID]                [INT]      NOT NULL
, [CreatedDateID]            [INT]      NULL
, [EstimatedDateID]          [INT]      NULL
, [ScheduledDateID]          [INT]      NULL
, [StartedDateID]            [INT]      NULL
, [ReportedAsFinishedDateID] [INT]      NULL
, [EndedDateID]              [INT]      NULL
, [DeliveryDateID]           [INT]      NULL
, [ProductionOrderID]        [INT]      NULL
, [ItemID]                   [INT]      NULL
, [ProductID]                [INT]      NULL
, [TrackingID]               [INT]      NULL
, [LocationID]               [INT]      NULL
, [WMSlocationID]            [INT]      NULL
, [IsDelayed]                [BIT]      NULL
, [IsOnTime]                 [BIT]      NULL
, [NumberOfDaysDelayed]      [INT]      NULL
, [LeadtimeInMinutes]        [BIGINT]   NULL
, [PlannedLeadtimeInMinutes] [BIGINT]   NULL
) ;

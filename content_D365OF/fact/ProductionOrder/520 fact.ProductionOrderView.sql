EXEC Dbo.Drop_Object @Object = N'fact.ProductionOrderView', @Type = N'V' ;
GO
CREATE VIEW fact.ProductionOrderView
AS
  SELECT            StageID                                   = DPO.StageID
                  , ExecutionFlag                             = DPO.ExecutionFlag
                  --	/***BK***/
                  , DPO.Partition
                  , DPO.CompanyId
                  , DPO.RecId
                  , DPO.DataConnectionId
                  , DPO.ComponentExecutionId
                  --Dates
                  , COALESCE(CD.DateId, 0)                    AS CreatedDateID
                  , COALESCE(EST.DateId, 0)                   AS EstimatedDateID
                  , COALESCE(SD.DateId, 0)                    AS ScheduledDateID
                  , COALESCE(STD.DateId, 0)                   AS StartedDateID
                  , COALESCE(RPD.DateId, 0)                   AS ReportedAsFinishedDateID
                  , COALESCE(ED.DateId, 0)                    AS EndedDateID
                  , COALESCE(DLV.DateID, 0)                   AS DeliveryDateID
                  --Production
                  , COALESCE(PO.ProductionOrderID, 0)         AS ProductionOrderID
                  , COALESCE(I.ItemID, 0)                     AS ItemID
                  --Invent Dim Dimensions
                  , COALESCE(PROD.ProductId, 0)               AS ProductID
                  , COALESCE(T.TrackingId, 0)                 AS TrackingID
                  , COALESCE(LOC.LocationID, 0)               AS LocationID
                  , COALESCE(WL.WMSlocationID, 0)             AS WMSlocationID
                  --Measures
                  , COALESCE(DPO.Isdelayed, 0)                AS IsDelayed
                  , COALESCE(DPO.IsOnTime, 0)                 AS IsOnTime
                  , COALESCE(DPO.NumberOfDaysDelayed, 0)      AS NumberOfDaysDelayed
                  , COALESCE(DPO.LeadtimeInMinutes, 0)        AS LeadTimeInMinutes
                  , COALESCE(DPO.PlannedLeadtimeInMinutes, 0) AS PlannedLeadTimeInMinutes
    -- select * 
    FROM            Delta.ProductionOrder AS DPO
    LEFT OUTER JOIN Dim.Date              AS CD ON CD.Date                    = DPO.CreatedDate
    LEFT OUTER JOIN Dim.Date              AS EST ON EST.Date                  = DPO.EstimatedDate
    LEFT OUTER JOIN Dim.Date              AS SD ON SD.Date                    = DPO.ScheduledDate
    LEFT OUTER JOIN Dim.Date              AS STD ON STD.Date                  = DPO.StartedDate
    LEFT OUTER JOIN Dim.Date              AS RPD ON RPD.Date                  = DPO.ReportAsFinishedDate
    LEFT OUTER JOIN Dim.Date              AS ED ON ED.Date                    = DPO.EndedDate
    LEFT OUTER JOIN Dim.Date              AS DLV ON DLV.Date                  = DPO.DeliveryDate
    LEFT OUTER JOIN Dim.ProductionOrder   AS PO ON PO.ProductionOrder         = DPO.ProductionOrder
                                               AND PO.Partition               = DPO.Partition
                                               AND PO.DataConnectionId        = DPO.DataConnectionId
                                               AND PO.CompanyId               = DPO.CompanyId
    LEFT OUTER JOIN Dim.Item              AS I ON I.ItemNumber                = DPO.Item
                                              AND I.Partition                 = DPO.Partition
                                              AND I.DataConnectionId          = DPO.DataConnectionId
                                              AND I.CompanyId                 = DPO.CompanyId
    LEFT OUTER JOIN Dim.Product           AS PROD ON PROD.InventSize          = DPO.ProductInventSizeCode
                                                 AND PROD.InventColor         = DPO.ProductColorCode
                                                 AND PROD.InventsTyle         = DPO.ProductsTyleCode
                                                 AND PROD.InventConfiguration = DPO.ProductConFigCode
                                                 AND PROD.CompanyId           = DPO.CompanyId
                                                 AND PROD.Partition           = DPO.Partition
                                                 AND PROD.DataConnectionId    = DPO.DataConnectionId
    LEFT OUTER JOIN Dim.Tracking          AS T ON T.InventBatchCode           = DPO.TrackingBatchCode
                                              AND T.InventSerialCode          = DPO.TrackingSerialCode
                                              AND T.CompanyId                 = DPO.CompanyId
                                              AND T.Partition                 = DPO.Partition
                                              AND T.DataConnectionId          = DPO.DataConnectionId
    LEFT OUTER JOIN Dim.Location          AS LOC ON LOC.LocationCode          = DPO.LocationCode
                                                AND LOC.CompanyId             = DPO.CompanyId
                                                AND LOC.Partition             = DPO.Partition
                                                AND LOC.DataConnectionId      = DPO.DataConnectionId
    LEFT OUTER JOIN Dim.Wmslocation       AS WL ON WL.Wmslocationcode         = DPO.Wmslocationcode
                                               AND WL.LocationCode            = DPO.LocationCode
                                               AND WL.CompanyId               = DPO.CompanyId
                                               AND WL.Partition               = DPO.Partition
                                               AND WL.DataConnectionId        = DPO.DataConnectionId ;

GO


EXEC dbo.drop_object 'delta.ProductionOrderFunction', 'F' ;
GO
CREATE FUNCTION delta.ProductionOrderFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN SELECT            PT.Stage_Id                                      AS StageId
                         , @Execution_Flag                                  AS ExecutionFlag
                         , @Component_Execution_Id                          AS ComponentExecutionId
                         , PT.Partition                                     AS Partition
                         , PT.Data_Connection_Id                            AS DataConnectionID
                         , PT.Company_Id                                    AS CompanyId
                         , PT.RecId                                         AS RecId
                         , CAST(PT.createdDateTime AS DATE)                 AS CreatedDate
                         , PT.CalcDate                                      AS EstimatedDate
                         , PT.SchedDate                                     AS ScheduledDate
                         , PT.StupDate                                      AS StartedDate
                         , PT.FinishedDate                                  AS ReportAsFinishedDate
                         , PT.RealDate                                      AS EndedDate
                         , PT.DlvDate                                       AS DeliveryDate
                         , PT.ProdID                                        AS ProductionOrder
                         , PT.ItemID                                        AS ItemCode
                         , COALESCE(NULLIF(ID.CONFIGID, ''), 'N/A')         AS ProductConfigCode
                         , COALESCE(NULLIF(ID.INVENTCOLORID, ''), 'N/A')    AS ProductColorCode
                         , COALESCE(NULLIF(ID.INVENTSIZEID, ''), 'N/A')     AS ProductInventSizeCode
                         , COALESCE(NULLIF(ID.INVENTSTYLEID, ''), 'N/A')    AS ProductStyleCode
                         , COALESCE(NULLIF(ID.INVENTBATCHID, ''), 'N/A')    AS TrackingBatchCode
                         , COALESCE(NULLIF(ID.INVENTSERIALID, ''), 'N/A')   AS TrackingSerialCode
                         , COALESCE(NULLIF(ID.INVENTLOCATIONID, ''), 'N/A') AS LocationCode
                         , COALESCE(NULLIF(ID.WMSLOCATIONID, ''), 'N/A')    AS WMSlocationCode
                         , CAST((CASE
                                   WHEN PT.PRODSTATUS NOT IN ( 5, 7 )
                                    AND PT.DLVDATE < GETUTCDATE() THEN 1    -- is not finished and dvldate before today 
                                   WHEN PT.PRODSTATUS IN ( 5, 7 )
                                    AND PT.DLVDATE < PT.FINISHEDDATE THEN 1 -- is finished and finished date after dlvdate  
                                   ELSE 0
                                 END) AS INT)                               AS IsDelayed
                         , CAST((CASE
                                   WHEN PT.PRODSTATUS NOT IN ( 5, 7 )
                                    AND PT.DLVDATE < GETUTCDATE() THEN 0    -- is not finished and dvldate before today 
                                   WHEN PT.PRODSTATUS IN ( 5, 7 )
                                    AND PT.DLVDATE < PT.FINISHEDDATE THEN 0 -- is finished and finished date after dlvdate  
                                   ELSE 1
                                 END) AS INT)                               AS IsOnTime
                         , CAST((CASE
                                   WHEN PT.PRODSTATUS NOT IN ( 5, 7 ) THEN CASE
                                                                             WHEN PT.DLVDATE = { TS '1900-01-01 00:00:00.000' }
                                                                               OR DATEDIFF(D, PT.DLVDATE, GETUTCDATE()) < 0 THEN NULL -- cannot calculate days delayed 
                                                                             ELSE DATEDIFF(D, PT.DLVDATE, GETUTCDATE())
                                                                           END
                                   ELSE CASE
                                          WHEN PT.DLVDATE = { TS '1900-01-01 00:00:00.000' }
                                            OR PT.FINISHEDDATE = { TS '1900-01-01 00:00:00.000' }
                                            OR DATEDIFF(D, PT.DLVDATE, PT.FINISHEDDATE) < 0 THEN NULL -- cannot calculate days delayed, dvldate or finished date is unknown 
                                          ELSE DATEDIFF(D, PT.DLVDATE, PT.FINISHEDDATE)
                                        END
                                 END) AS INT)                               AS NumberOfDaysDelayed
                         , CAST((CASE
                                   WHEN ( (MIN(DATEADD(MINUTE, PRT.FROMTIME, PRT.DATEWIP)) = { TS '1900-01-01 00:00:00.000' })
                                       OR (MAX(DATEADD(MINUTE, PRT.TOTIME, PRT.DATEWIP)) = { TS '1900-01-01 00:00:00.000' }))
                                     OR (DATEDIFF(MINUTE, MIN(DATEADD(MINUTE, PRT.FROMTIME, PRT.DATEWIP)), MAX(DATEADD(MINUTE, PRT.TOTIME, PRT.DATEWIP))) < 0) THEN NULL
                                   ELSE DATEDIFF(MINUTE, MIN(DATEADD(MINUTE, PRT.FROMTIME, PRT.DATEWIP)), MAX(DATEADD(MINUTE, PRT.TOTIME, PRT.DATEWIP)))
                                 END) AS BIGINT)                            AS LeadTimeInMinutes
                         , CAST((CASE
                                   WHEN ( (MIN(DATEADD(MINUTE, PR.FROMTIME, PR.FROMDATE)) = { TS '1900-01-01 00:00:00.000' })
                                       OR (MAX(DATEADD(MINUTE, PR.TOTIME, PR.TODATE)) = { TS '1900-01-01 00:00:00.000' }))
                                     OR (DATEDIFF(MINUTE, MIN(DATEADD(S, PR.FROMTIME, PR.FROMDATE)), MAX(DATEADD(MINUTE, PR.TOTIME, PR.TODATE))) < 0) THEN NULL
                                   ELSE DATEDIFF(MINUTE, MIN(DATEADD(S, PR.FROMTIME, PR.FROMDATE)), MAX(DATEADD(MINUTE, PR.TOTIME, PR.TODATE)))
                                 END) AS BIGINT)                            AS PlannedLeadtimeInMinutes
           FROM            Stage_AX.PRODTABLE      AS PT
           LEFT OUTER JOIN stage_ax.PRODROUTETRANS AS PRT ON PRT.TRANSREFTYPE = 0
                                                         AND PT.PRODID        = PRT.TRANSREFID
                                                         AND PT.DATAAREAID    = PRT.DATAAREAID
                                                         AND PT.PARTITION     = PRT.PARTITION
           LEFT OUTER JOIN stage_ax.PRODROUTE      AS PR ON PT.PRODID         = PR.PRODID
                                                        AND PT.DATAAREAID     = PR.DATAAREAID
                                                        AND PT.PARTITION      = PR.PARTITION
                                                        AND PT.Company_ID     = PR.Company_id
           LEFT OUTER JOIN Stage_Ax.InventDim      AS ID ON PT.InventDimId    = ID.InventDimID
                                                        AND PT.DataAreaId     = ID.DataAreaId
                                                        AND PT.Partition      = ID.Partition
                                                        AND PT.Company_Id     = ID.Company_Id
          WHERE
           /** FULL LOAD **/
                           ( @load_type        = 0
                         AND PT.execution_flag <> 'D')
             OR
             /** INCREMENTAL, NEW RECORDS **/
                           ( @load_type                      = 1
                         AND @execution_flag                 = 'N'
                         AND PT.execution_flag               = @execution_flag
                         AND PT.execution_timestamp          > @last_processed_timestamp)
             OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                      = 1
                         AND @execution_flag                 = 'U'
                         AND ( ( PT.execution_flag           = @execution_flag
                             AND PT.execution_timestamp      > @last_processed_timestamp)
                            OR ( PR.execution_flag           = @execution_flag
                             AND PR.execution_timestamp      > @last_processed_timestamp)
                            OR ( ID.execution_flag           = @execution_flag
                             AND ID.execution_timestamp      > @last_processed_timestamp)))
             OR
             /** INCREMENTAL, DELETED RECORDS **/
                           ( @load_type                      = 1
                         AND @execution_flag                 = 'D'
                         AND PT.execution_flag               = @execution_flag
                         AND PT.execution_timestamp          > @last_processed_timestamp)
          GROUP BY PT.Stage_id
                 , PT.Partition
                 , PT.Data_Connection_Id
                 , PT.RecId
                 , PT.Company_Id
                 , PT.PRODID
                 , PT.ItemId
                 , PT.CreatedDateTime
                 , PT.CalcDate
                 , PT.SchedDate
                 , PT.StupDate
                 , PT.FinishedDate
                 , PT.RealDate
                 , PT.DlvDate
                 , ID.INVENTLOCATIONID
                 , ID.INVENTBATCHID
                 , ID.INVENTSERIALID
                 , ID.WMSLOCATIONID
                 , ID.CONFIGID
                 , ID.INVENTCOLORID
                 , ID.INVENTSIZEID
                 , ID.INVENTSTYLEID
                 , PT.ProdStatus ;
GO


EXEC dbo.drop_object 'delta.PurchaseInvoice', 'T' ;
GO
CREATE TABLE [delta].[PurchaseInvoice]
(
  StageId                               BIGINT            NOT NULL
, ExecutionFlag                         NCHAR(1)          NOT NULL
, ComponentExecutionId                  INT               NOT NULL
, [PurchaseInvoiceLineRecId]            [BIGINT]          NULL
, [PurchaseInvoiceLinePartition]        BIGINT            NULL
, [DataConnectionID]                    [INT]             NOT NULL
, [CompanyId]                           [INT]             NOT NULL
, [DocumentDate]                        [DATETIME]        NULL
, [InvoiceDate]                         [DATETIME]        NULL
, [ShipDate]                            [DATETIME]        NULL
, [WMSlocationCode]                     [NVARCHAR](100)   NULL
, [LocationCode]                        [NVARCHAR](100)   NULL
, [ProductConfigCode]                   [NVARCHAR](100)   NULL
, [ProductColorCode]                    [NVARCHAR](100)   NULL
, [ProductInventSizeCode]               [NVARCHAR](100)   NULL
, [ProductStyleCode]                    [NVARCHAR](100)   NULL
, [TrackingBatchCode]                   [NVARCHAR](100)   NULL
, [TrackingSerialCode]                  [NVARCHAR](100)   NULL
, [CompanyCode]                         [NVARCHAR](10)    NULL
, [VendorAccountCode]                   [NVARCHAR](100)   NULL
, [InvoiceAccountCode]                  [NVARCHAR](100)   NULL
, [DeliveryAddressCode]                 [BIGINT]          NULL
, [ItemCode]                            [NVARCHAR](100)   NULL
, [PurchasePlacerCode]                  [BIGINT]          NULL
, [PurchaseRequesterCode]               [BIGINT]          NULL
, [CurrencyCode]                        [NVARCHAR](10)    NULL
, [DefaultDimensionRecId]               [BIGINT]          NULL
, [VoucherCode]                         [NVARCHAR](100)   NULL
, [DeliveryTermCode]                    [NVARCHAR](100)   NULL
, [DeliveryModeCode]                    [NVARCHAR](100)   NULL
, [PaymentModeCode]                     [NVARCHAR](100)   NULL
, [PaymentTermCode]                     [NVARCHAR](100)   NULL
, [PurchaseOrderTypeCode]               [INT]             NULL
, [PurchasePoolCode]                    [NVARCHAR](100)   NULL
, [PurchaseInvoice]                     [NVARCHAR](255)   NULL
, [PurchaseOrderCode]                   [NVARCHAR](20)    NULL
, [NumberSequenceGroup]                 [NVARCHAR](10)    NULL
, [InternalInvoiceCode]                 [NVARCHAR](20)    NULL
, [PurchaseUnitOfMeasureCode]           [NVARCHAR](100)   NULL
, [InventoryQuantity]                   DECIMAL(19,4)     NULL
, [DeliveredWithoutPackingSlipQuantity] DECIMAL(19,4)     NULL
, [InvoicedQuantity]                    DECIMAL(19,4)     NULL
, [PurchasePrice]                       DECIMAL(19,4)     NULL
, [PurchasePrice_RCY]                   DECIMAL(19,4)     NULL
, [Discount]                            DECIMAL(19,4)     NULL
, [Discount_RCY]                        DECIMAL(19,4)     NULL
, [DiscountPercent]                     DECIMAL(19,4)     NULL
, [MultiLineDiscount]                   DECIMAL(19,4)     NULL
, [MultiLineDiscount_RCY]               DECIMAL(19,4)     NULL
, [MultiLineDiscountPercent]            DECIMAL(19,4)     NULL
, [NetAmount]                           DECIMAL(19,4)     NULL
, [NetAmount_RCY]                       DECIMAL(19,4)     NULL
, [NetAmountMST]                        DECIMAL(19,4)     NULL
, [TaxAmount_RCY]                       DECIMAL(19,4)     NULL
, [TaxAmount]                           DECIMAL(19,4)     NULL
, [AmountExclTax]                       DECIMAL(19,4)     NULL
, [AmountExclTaxMST]                    DECIMAL(19,4)     NULL
, [DiscountAmount]                      DECIMAL(19,4)     NULL
, [DiscountAmount_RCY]                  DECIMAL(19,4)     NULL
) ;                                     
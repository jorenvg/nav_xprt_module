EXEC dbo.drop_object 'delta.PurchaseInvoiceFunction', 'F' ;
GO
CREATE FUNCTION delta.PurchaseInvoiceFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            VIT.stage_id                                     AS StageID
                           , @execution_flag                                  AS ExecutionFlag
                           , @component_execution_id                          AS ComponentExecutionID
                           , VIT.RECID                                        AS PurchaseInvoiceLineRecId
                           , VIT.PARTITION                                    AS PurchaseInvoiceLinePartition
                           , VIT.data_connection_id                           AS DataConnectionID
                           , VIJ.company_id                                   AS CompanyId
                           , VIJ.DOCUMENTDATE                                 AS DocumentDate
                           , VIT.INVOICEDATE                                  AS InvoiceDate
                           , PT.DELIVERYDATE                                  AS ShipDate
                           , COALESCE(NULLIF(ID.WMSLOCATIONID, ''), 'N/A')    AS WMSlocationCode
                           , COALESCE(NULLIF(ID.INVENTLOCATIONID, ''), 'N/A') AS LocationCode
                           , COALESCE(NULLIF(ID.CONFIGID, ''), 'N/A')         AS ProductConfigCode
                           , COALESCE(NULLIF(ID.INVENTCOLORID, ''), 'N/A')    AS ProductColorCode
                           , COALESCE(NULLIF(ID.INVENTSIZEID, ''), 'N/A')     AS ProductInventSizeCode
                           , COALESCE(NULLIF(ID.INVENTSTYLEID, ''), 'N/A')    AS ProductStyleCode
                           , COALESCE(NULLIF(ID.INVENTBATCHID, ''), 'N/A')    AS TrackingBatchCode
                           , COALESCE(NULLIF(ID.INVENTSERIALID, ''), 'N/A')   AS TrackingSerialCode
                           , VIT.DATAAREAID                                   AS CompanyCode
                           , COALESCE(VIJ.ORDERACCOUNT, PT.ORDERACCOUNT)      AS VendorAccountCode
                           , COALESCE(VIJ.INVOICEACCOUNT, PT.INVOICEACCOUNT)  AS InvoiceAccountCode
                           , VIT.DELIVERYPOSTALADDRESS                        AS DeliveryAddressCode
                           , VIT.ITEMID                                       AS ItemCode
                           , PT.WORKERPURCHPLACER                             AS PurchasePlacerCode
                           , PT.REQUESTER                                     AS PurchaseRequesterCode
                           , VIT.CURRENCYCODE                                 AS CurrencyCode
                           , VIT.DEFAULTDIMENSION                             AS DefaultDimensionRecId
                           , VIJ.LEDGERVOUCHER                                AS VoucherCode
                           , COALESCE(VIJ.DLVTERM, PT.DLVTERM)                AS DeliveryTermCode
                           , COALESCE(VIJ.DLVMODE, PT.DLVMODE)                AS DeliveryModeCode
                           , COALESCE(VIJ.PAYMENT, PT.PAYMENT)                AS PaymentTermCode
                           , PT.PAYMMODE                                      AS PaymentModeCode
                           , COALESCE(VIJ.PURCHASETYPE, PT.PURCHASETYPE)      AS PurchaseOrderTypeCode
                           , PT.PURCHPOOLID                                   AS PurchasePoolCode
                           , VIT.INVOICEID                                    AS PurchaseInvoice
                           , VIT.PURCHID                                      AS PurchaseOrderCode
                           , VIT.NUMBERSEQUENCEGROUP                          AS NumberSequenceGroup
                           , VIT.INTERNALINVOICEID                            AS InternalInvoiceCode
                           , VIT.PURCHUNIT                                    AS PurchaseUnitOfMeasureCode
                           /***Measures***/
                           , VIT.INVENTQTY                                    AS InventoryQuantity
                           , VIT.QtyPhysical                                  AS DeliveredWithoutPackingSlipQuantity
                           , VIT.QTY                                          AS InvoicedQuantity
                           , VIT.PURCHPRICE                                   AS PurchasePrice
                           , VIT.PURCHPRICE * ex.CrossRate                    AS PurchasePrice_RCY
                           , VIT.LINEDISC                                     AS Discount
                           , VIT.LINEDISC * ex.CrossRate                      AS Discount_RCY
                           , VIT.LINEPERCENT                                  AS DiscountPercent
                           , VIT.MULTILNDISC                                  AS MultiLineDiscount
                           , VIT.MULTILNDISC * ex.CrossRate                   AS MultiLineDiscount_RCY
                           , VIT.MULTILNPERCENT                               AS MultiLineDiscountPercent
                           , VIT.LINEAMOUNT                                   AS NetAmount
                           , VIT.LINEAMOUNT * ex.CrossRate                    AS NetAmount_RCY
                           , VIT.LINEAMOUNTMST                                AS NetAmountMST
                           , VIT.TAXAMOUNT                                    AS TaxAmount
                           , VIT.TAXAMOUNT * ex.CrossRate                     AS TaxAmount_RCY
                           , VIT.LINEAMOUNT                                   AS AmountExclTax
                           , VIT.LINEAMOUNTMST                                AS AmountExclTaxMST
                           , CASE WHEN VIT.LINEDISC <> 0 AND VIT.LINEDISC IS NOT NULL
                                  THEN VIT.QTY * VIT.LINEDISC
                                  ELSE 
                                      CASE WHEN VIT.LINEPERCENT <> 0 AND VIT.LINEPERCENT IS NOT NULL
                                           THEN ((VIT.QTY * VIT.PURCHPRICE) * VIT.LINEPERCENT / 100 )
                                           ELSE 0
                                      END
                             END                                              AS DiscountAmount

                           , CASE WHEN VIT.LINEDISC <> 0 AND VIT.LINEDISC IS NOT NULL
                                  THEN VIT.QTY * VIT.LINEDISC * ex.CrossRate
                                  ELSE 
                                      CASE WHEN VIT.LINEPERCENT <> 0 AND VIT.LINEPERCENT IS NOT NULL
                                           THEN ((VIT.QTY * VIT.PURCHPRICE) * VIT.LINEPERCENT / 100 ) * ex.CrossRate
                                           ELSE 0
                                      END
                             END                                              AS DiscountAmount_RCY 

             FROM            stage_ax.VENDINVOICETRANS AS VIT
            INNER JOIN       stage_ax.VENDINVOICEJOUR  VIJ ON VIT.INVOICEID           = VIJ.INVOICEID
                                                          AND VIT.PURCHID             = VIJ.PURCHID
                                                          AND VIT.DATAAREAID          = VIJ.DATAAREAID
                                                          AND VIT.INVOICEDATE         = VIJ.INVOICEDATE
                                                          AND VIT.NUMBERSEQUENCEGROUP = VIJ.NUMBERSEQUENCEGROUP
                                                          AND VIT.INTERNALINVOICEID   = VIJ.INTERNALINVOICEID
                                                          AND VIT.PARTITION           = VIJ.PARTITION
                                                          AND VIT.data_connection_id  = VIJ.data_connection_id
                                                          AND VIT.company_id          = VIJ.company_id
             LEFT OUTER JOIN stage_ax.INVENTDIM        ID ON VIT.INVENTDIMID          = ID.INVENTDIMID
                                                         AND VIT.DATAAREAID           = ID.DATAAREAID
                                                         AND VIT.PARTITION            = ID.PARTITION
                                                         AND VIT.data_connection_id   = ID.data_connection_id
                                                         AND VIT.company_id           = ID.company_id
             LEFT OUTER JOIN stage_ax.PURCHTABLE       PT ON VIJ.PURCHID              = PT.PURCHID
                                                         AND VIJ.DATAAREAID           = PT.DATAAREAID
                                                         AND VIJ.PARTITION            = PT.PARTITION
                                                         AND VIJ.data_connection_id   = PT.data_connection_id
                                                         AND VIJ.company_id           = PT.company_id
             LEFT JOIN       reporting_exchange_rates  ex ON ex.FromCurrencyCode      = VIT.CURRENCYCODE
                                                         AND ex.DataConnectionID      = VIT.data_connection_id
                                                         AND VIT.INVOICEDATE BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE
             /** FULL LOAD **/
                             ( @load_type         = 0
                           AND VIT.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                       = 1
                           AND @execution_flag                  = 'N'
                           AND VIT.execution_flag               = @execution_flag
                           AND VIT.execution_timestamp          > @last_processed_timestamp)
               OR
               /** INCREMENTAL, UPDATED RECORDS **/
                             ( @load_type                       = 1
                           AND @execution_flag                  = 'U'
                           AND ( ( VIT.execution_flag           = @execution_flag
                               AND VIT.execution_timestamp      > @last_processed_timestamp)
                              OR ( ID.execution_flag            = @execution_flag
                               AND ID.execution_timestamp       > @last_processed_timestamp)
                              OR ( PT.execution_flag            = @execution_flag
                               AND PT.execution_timestamp       > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                       = 1
                           AND @execution_flag                  = 'D'
                           AND VIT.execution_flag               = @execution_flag
                           AND VIT.execution_timestamp          > @last_processed_timestamp)) ;
GO


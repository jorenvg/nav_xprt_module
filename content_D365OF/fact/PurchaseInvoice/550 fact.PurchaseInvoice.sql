EXEC dbo.drop_object 'fact.PurchaseInvoice', 'T' ;
GO
CREATE TABLE [fact].[PurchaseInvoice]
(
  StageID                               BIGINT            NOT NULL
, ComponentExecutionID                  INT               NOT NULL
, [CompanyID]                           [INT]             NOT NULL
, [DataConnectionID]                    [INT]             NOT NULL
, [PurchaseInvoiceLineRecID]            [BIGINT]          NOT NULL
, PurchaseInvoiceLinePartition          BIGINT            NOT NULL
, [DocumentDateID]                      [INT]             NOT NULL
, [InvoiceDateID]                       [INT]             NOT NULL
, [PurchaseInvoiceID]                   INT               NOT NULL
, [DeliveryDateID]                      INT               NOT NULL
, [WMSlocationID]                       [INT]             NOT NULL
, [LocationID]                          [INT]             NOT NULL
, [ProductID]                           [INT]             NOT NULL
, [TrackingID]                          [INT]             NOT NULL
, [VendorAccountID]                     [INT]             NOT NULL
, [InvoiceAccountID]                    [INT]             NOT NULL
, [DeliveryAddressID]                   [INT]             NOT NULL
, [ItemID]                              [INT]             NOT NULL
, PurchasePlacerID                      INT               NOT NULL
, PurchaseRequesterID                   INT               NOT NULL
, CurrencyTransactionID                 INT               NOT NULL
, CurrencyReportID                      INT               NOT NULL
, [FinancialDimensionsID]               [INT]             NOT NULL
, [VoucherID]                           [INT]             NOT NULL
, [PurchaseTypeID]                      [INT]             NOT NULL
, [ModeOfDeliveryID]                    [INT]             NOT NULL
, [DeliveryTermsID]                     [INT]             NOT NULL
, MethodOfPaymentID                     [INT]             NOT NULL
, [PaymentTermsID]                      [INT]             NOT NULL
, [PurchaseOrderID]                     [INT]             NOT NULL
, [PurchaseUnitID]                      [INT]             NOT NULL
, PurchasePoolID                        [INT]             NOT NULL
, [InventoryQuantity]                   DECIMAL(19,4)     NULL
, [DeliveredWithoutPackingSlipQuantity] DECIMAL(19,4)     NULL
, [InvoicedQuantity]                    DECIMAL(19,4)     NULL
, [PurchasePrice]                       DECIMAL(19,4)     NULL
, [PurchasePrice_RCY]                   DECIMAL(19,4)     NULL
, [Discount]                            DECIMAL(19,4)     NULL
, [Discount_RCY]                        DECIMAL(19,4)     NULL
, [DiscountPercent]                     DECIMAL(19,4)     NULL
, [MultilineDiscount]                   DECIMAL(19,4)     NULL
, [MultilineDiscount_RCY]               DECIMAL(19,4)     NULL
, [MultilineDiscountPercent]            DECIMAL(19,4)     NULL
, [NetAmount]                           DECIMAL(19,4)     NULL
, [NetAmount_RCY]                       DECIMAL(19,4)     NULL
, [NetAmountMST]                        DECIMAL(19,4)     NULL
, [TaxAmount]                           DECIMAL(19,4)     NULL
, [TaxAmount_RCY]                       DECIMAL(19,4)     NULL
, [AmountExclTax]                       DECIMAL(19,4)     NULL
, [AmountExclTaxMST]                    DECIMAL(19,4)     NULL
, [DiscountAmount]                      DECIMAL(19,4)     NULL
, [DiscountAmount_RCY]                  DECIMAL(19,4)     NULL
) ;

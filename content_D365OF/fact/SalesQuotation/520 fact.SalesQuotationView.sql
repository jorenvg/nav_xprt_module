EXEC dbo.drop_object @object = N'fact.SalesQuotationView', @type = N'V' ;
GO
CREATE VIEW fact.SalesQuotationView
AS
  SELECT            QL.StageId
                  , QL.ExecutionFlag
                  --	/***BK***/
                  , QL.InventTransCode
                  , QL.DataAreaCode
                  , QL.Partition
                  , QL.CompanyID
                  , QL.DataConnectionID

                  --	 /***Dimensions*/
                  --	 --Dates
                  , COALESCE(cd.DateID, 0)                AS CreatedDateID
                  , COALESCE(srd.DateID, 0)               AS ShippingRequestedDateID
                  , COALESCE(qed.DateID, 0)               AS QuotationExpirationDateID
                  , COALESCE(qdsd.DateID, 0)              AS QuotationDocumentSendDateID
                  , COALESCE(qddd.DateID, 0)              AS QuotationDocumentDueDateID
                  --	--employee
                  , COALESCE(stEmp.EmployeeID, 0)         AS SalesTakerID
                  , COALESCE(srEmp.EmployeeID, 0)         AS SalesResponsibleID
                  --	 --customer
                  , COALESCE(cAcc.CustomerID, 0)          AS CustomerID
                  --	-- address
                  , COALESCE(dAdr.AddressID, 0)           AS AddressID
                  --	-- item
                  , COALESCE(i.ItemID, 0)                 AS ItemID
                  , COALESCE(pci.ProjectCategoryID, 0)    AS ProjectCategoryID
                  --	 -- currency
                  , COALESCE(cy.CurrencyID, 0)            AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)           AS CurrencyReportID
                  --	 -- financialdimension
                  , COALESCE(fd.FinancialDimensionsID, 0) AS FinancialDimensionsID
                  --	 -- reason code
                  , COALESCE(sOrg.SalesOriginID, 0)       AS SalesOriginID
                  , COALESCE(sg.SalesGroupID, 0)          AS SalesGroupID
                  , COALESCE(modlv.ModeOfDeliveryID, 0)   AS ModeOfDeliveryID
                  , COALESCE(dt.DeliveryTermsID, 0)       AS DeliveryTermsID
                  , COALESCE(dtp.DeliveryTypeID, 0)       AS DeliveryTypeID
                  , COALESCE(mop.MethodOfPaymentID, 0)    AS MethodOfPaymentID
                  , COALESCE(pt.PaymentTermsID, 0)        AS PaymentTermsID
                  , COALESCE(SQ.SalesQuotationID, 0)      AS SalesQuotationID
                  , COALESCE(sTax.TaxID, 0)               AS SalesTaxID

                  --	/***Measures */
                  , QL.SalesQuantity                      AS SalesQuantity
                  , QL.PriceUnit                          AS PriceUnit
                  , QL.PriceUnit_RCY                      AS PriceUnit_RCY
                  , QL.CostPrice                          AS CostPrice
                  , QL.CostPrice_RCY                      AS CostPrice_RCY
                  , QL.QuotationPrice                     AS QuotationPrice
                  , QL.QuotationPrice_RCY                 AS QuotationPrice_RCY
                  , QL.Discount                           AS Discount
                  , QL.Discount_RCY                       AS Discount_RCY
                  , QL.DiscountPercent                    AS DiscountPercent
                  , QL.MultilineDiscount                  AS MultilineDiscount
                  , QL.MultilineDiscount_RCY              AS MultilineDiscount_RCY
                  , QL.MultilineDiscountPercent           AS MultilineDiscountPercent
                  , QL.SalesCharges                       AS SalesCharges
                  , QL.SalesCharges_RCY                   AS SalesCharges_RCY
                  , QL.QuotationAmount                    AS QuotationAmount
                  , QL.QuotationAmount_RCY                AS QuotationAmount_RCY

    --SELECT COUNT(*)  -- 1642
    FROM            delta.SalesQuotation    AS QL
    LEFT JOIN       dim.Date                AS cd ON cd.Date                     = QL.CreatedDate
    LEFT JOIN       dim.Date                AS srd ON srd.Date                   = QL.ShippingRequestedDate
    LEFT JOIN       dim.Date                AS qed ON qed.Date                   = QL.QuotationExpirationDate
    LEFT JOIN       dim.Date                AS qdsd ON qdsd.Date                 = QL.QuotationDocumentSendDate
    LEFT JOIN       dim.Date                AS qddd ON qddd.Date                 = QL.QuotationDocumentDueDate
    LEFT JOIN       dim.Customer            AS cAcc ON cAcc.CustomerAccountNo    = QL.CustomerAccountCode
                                                   AND cAcc.Partition            = QL.Partition
                                                   AND cAcc.DataConnectionID     = QL.DataConnectionID
                                                   AND cAcc.CompanyID            = QL.CompanyID
    LEFT JOIN       dim.Address             AS dAdr ON dAdr.AddressRecID         = QL.DeliveryAddressCode
                                                   AND dAdr.Partition            = QL.Partition
                                                   AND dAdr.DataConnectionID     = QL.DataConnectionID
    LEFT JOIN       dim.Item                AS i ON i.ItemNumber                 = QL.ItemCode
                                                AND i.Partition                  = QL.Partition
                                                AND i.DataConnectionID           = QL.DataConnectionID
                                                AND i.CompanyID                  = QL.CompanyID
    LEFT JOIN       dim.FinancialDimensions AS fd ON fd.DefaultDimension         = QL.FinancialDimensionCode
                                                 AND fd.Partition                = QL.Partition
                                                 AND fd.DataConnectionID         = QL.DataConnectionID
    LEFT JOIN       dim.MethodOfPayment     AS mop ON mop.MethodOfPaymentCode    = QL.PaymentModeCode
                                                  AND mop.Partition              = QL.Partition
                                                  AND mop.DataConnectionID       = QL.DataConnectionID
                                                  AND mop.CompanyID              = QL.CompanyID
    LEFT JOIN       dim.PaymentTerms        AS pt ON pt.PaymentTermsCode         = QL.PaymentTermsCode
                                                 AND pt.Partition                = QL.Partition
                                                 AND pt.DataConnectionID         = QL.DataConnectionID
                                                 AND pt.CompanyID                = QL.CompanyID
    LEFT JOIN       dim.ModeOfDelivery      AS modlv ON modlv.ModeOfDeliveryCode = QL.DeliveryModeCode
                                                    AND modlv.Partition          = QL.Partition
                                                    AND modlv.DataConnectionID   = QL.DataConnectionID
                                                    AND modlv.CompanyID          = QL.CompanyID
    LEFT JOIN       dim.DeliveryTerms       AS dt ON dt.DeliveryTermsCode        = QL.DeliveryTermsCode
                                                 AND dt.Partition                = QL.Partition
                                                 AND dt.DataConnectionID         = QL.DataConnectionID
                                                 AND dt.CompanyID                = QL.CompanyID
    LEFT JOIN       dim.DeliveryType        AS dtp ON dtp.DeliveryTypeCode       = QL.DeliveryTypeCode
                                                  AND dtp.DataConnectionID       = QL.DataConnectionID
    LEFT JOIN       dim.SalesGroup          AS sg ON sg.SalesGroupCode           = QL.SalesGroupCode
                                                 AND sg.Partition                = QL.Partition
                                                 AND sg.DataConnectionID         = QL.DataConnectionID
                                                 AND sg.CompanyID                = QL.CompanyID
    LEFT JOIN       dim.SalesOrigin         AS sOrg ON sOrg.SalesOriginCode      = QL.SalesOriginCode
                                                   AND sOrg.DataConnectionID     = QL.DataConnectionID
                                                   AND sOrg.CompanyID            = QL.CompanyID
    LEFT JOIN       dim.SalesQuotation      AS SQ ON SQ.SalesQuotationCode       = QL.QuotationCode
                                                 AND SQ.Partition                = QL.Partition
                                                 AND SQ.DataConnectionID         = QL.DataConnectionID
                                                 AND SQ.CompanyID                = QL.CompanyID
    LEFT JOIN       dim.Tax                 AS sTax ON sTax.TaxItemGroupCode     = QL.SalesTaxItemGroupCode
                                                   AND sTax.TaxGroupCode         = QL.SalesTaxGroupCode
                                                   AND sTax.Partition            = QL.Partition
                                                   AND sTax.DataConnectionID     = QL.DataConnectionID
                                                   AND sTax.CompanyID            = QL.CompanyID
    LEFT JOIN       dim.Currency            AS cy ON cy.Code                     = QL.CurrencyCode
                                                 AND cy.Partition                = QL.Partition
                                                 AND cy.DataConnectionID         = QL.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport      AS rcy ON rcy.Partition              = QL.Partition
    LEFT JOIN       dim.Employee            AS stEmp ON stEmp.EmployeeRecID      = QL.SalesTakerCode
                                                    AND stEmp.Partition          = QL.Partition
                                                    AND stEmp.DataConnectionID   = QL.DataConnectionID
    LEFT JOIN       dim.Employee            AS srEmp ON srEmp.EmployeeRecID      = QL.SalesResponsibleCode
                                                    AND srEmp.Partition          = QL.Partition
                                                    AND srEmp.DataConnectionID   = QL.DataConnectionID
    LEFT JOIN       dim.ProjectCategory     AS pci ON pci.CategoryCode           = QL.ProjectCategoryCode
                                                  AND pci.Partition              = QL.Partition
                                                  AND pci.DataConnectionID       = QL.DataConnectionID
                                                  AND pci.CompanyID              = QL.CompanyID ;
GO



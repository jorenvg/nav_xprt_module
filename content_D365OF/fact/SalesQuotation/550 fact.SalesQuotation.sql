EXEC dbo.drop_object 'fact.SalesQuotation', 'T' ;
GO
CREATE TABLE [fact].[SalesQuotation]
(
  [StageID]                     BIGINT            NOT NULL
, [InventTransCode]             [NVARCHAR](100)   NULL
, [DataAreaCode]                [NVARCHAR](100)   NULL
, [Partition]                   [BIGINT]          NULL
, [CompanyID]                   [INT]             NOT NULL
, [DataConnectionID]            [INT]             NOT NULL
, [ComponentExecutionID]        INT               NOT NULL
, [CreatedDateID]               [INT]             NOT NULL
, [ShippingRequestedDateID]     [INT]             NOT NULL
, [QuotationExpirationDateID]   [INT]             NOT NULL
, [QuotationDocumentSenddateID] [INT]             NOT NULL
, [QuotationDocumentDueDateID]  [INT]             NOT NULL
, [SalesTakerID]                [INT]             NOT NULL
, [SalesResponsibleID]          [INT]             NOT NULL
, [CustomerID]                  [INT]             NOT NULL
, [AddressID]                   [INT]             NOT NULL
, [ItemID]                      [INT]             NOT NULL
, [ProjectCategoryID]           [INT]             NOT NULL
, [CurrencyTransactionID]       [INT]             NOT NULL
, [CurrencyReportID]            [INT]             NOT NULL
, [FinancialDimensionsID]       [INT]             NOT NULL
, [SalesOriginID]               [INT]             NOT NULL
, [SalesGroupID]                [INT]             NOT NULL
, [ModeOfDeliveryID]            [INT]             NOT NULL
, [DeliveryTermsID]             [INT]             NOT NULL
, [DeliveryTypeID]              [INT]             NOT NULL
, [MethodOfPaymentID]           [INT]             NOT NULL
, [PaymentTermsID]              [INT]             NOT NULL
, [SalesQuotationID]            [INT]             NOT NULL
, [SalesTaxID]                  [INT]             NOT NULL
, [SalesQuantity]               DECIMAL(19,4)     NULL
, [PriceUnit]                   DECIMAL(19,4)     NULL
, [PriceUnit_RCY]               DECIMAL(19,4)     NULL
, [CostPrice]                   DECIMAL(19,4)     NULL
, [CostPrice_RCY]               DECIMAL(19,4)     NULL
, [QuotationPrice]              DECIMAL(19,4)     NULL
, [QuotationPrice_RCY]          DECIMAL(19,4)     NULL
, [Discount]                    DECIMAL(19,4)     NULL
, [Discount_RCY]                DECIMAL(19,4)     NULL
, [DiscountPercent]             DECIMAL(19,4)     NULL
, [MultilineDiscount]           DECIMAL(19,4)     NULL
, [MultilineDiscount_RCY]       DECIMAL(19,4)     NULL
, [MultilineDiscountPercent]    DECIMAL(19,4)     NULL
, [SalesCharges]                DECIMAL(19,4)     NULL
, [SalesCharges_RCY]            DECIMAL(19,4)     NULL
, [QuotationAmount]             DECIMAL(19,4)     NULL
, [QuotationAmount_RCY]         DECIMAL(19,4)     NULL
) ;

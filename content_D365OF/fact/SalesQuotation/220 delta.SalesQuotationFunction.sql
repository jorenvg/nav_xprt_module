EXEC dbo.drop_object 'delta.SalesQuotationFunction', 'F' ;
GO
CREATE FUNCTION delta.SalesQuotationFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            QL.stage_id                                                                                              AS StageID
                           , @execution_flag                                                                                          AS ExecutionFlag
                           , QL.INVENTTRANSID                                                                                         AS InventTransCode
                           , QL.DATAAREAID                                                                                            AS DataAreaCode
                           , QL.PARTITION                                                                                             AS Partition
                           , QL.company_id                                                                                            AS CompanyID
                           , QL.data_connection_id                                                                                    AS DataConnectionID
                           , @component_execution_id                                                                                  AS ComponentExecutionID
                           , CAST(QL.CREATEDDATETIME AS DATE)                                                                         AS CreatedDate
                           , COALESCE(NULLIF(QL.SHIPPINGDATEREQUESTED, '1900-01-01'), NULLIF(QH.SHIPPINGDATEREQUESTED, '1900-01-01')) AS ShippingRequestedDate
                           , CAST(QH.QUOTATIONEXPIRYDATE AS DATE)                                                                     AS QuotationExpirationDate
                           , CAST(CQJ.QUOTATIONDATE AS DATE)                                                                          AS QuotationDocumentSendDate
                           , CAST(CQJ.RESPITEDATE AS DATE)                                                                            AS QuotationDocumentDueDate
                           , QH.WORKERSALESTAKER                                                                                      AS SalesTakerCode
                           , QH.WORKERSALESRESPONSIBLE                                                                                AS SalesResponsibleCode
                           , QH.CUSTACCOUNT                                                                                           AS CustomerAccountCode
                           , QL.DATAAREAID                                                                                            AS CompanyCode
                           , QL.ITEMID                                                                                                AS ItemCode
                           , QL.PROJCATEGORYID                                                                                        AS ProjectCategoryCode
                           , QL.CURRENCYCODE                                                                                          AS CurrencyCode
                           , QL.DEFAULTDIMENSION                                                                                      AS FinancialDimensionCode
                           , QL.DELIVERYPOSTALADDRESS                                                                                 AS DeliveryAddressCode
                           , QL.QUOTATIONSTATUS                                                                                       AS QuotationStatusCode
                           , QL.QUOTATIONTYPE                                                                                         AS QuotationTypeCode
                           , QH.SALESORIGINID                                                                                         AS SalesOriginCode
                           , QH.SALESPOOLID                                                                                           AS SalesPoolCode
                           , QH.SALESGROUP                                                                                            AS SalesGroupCode
                           , COALESCE(NULLIF(QL.DLVMODE, ''), QH.DLVMODE)                                                             AS DeliveryModeCode
                           , COALESCE(NULLIF(QL.DLVTERM, ''), QH.DLVTERM)                                                             AS DeliveryTermsCode
                           , QH.PAYMMODE                                                                                              AS PaymentModeCode
                           , QH.PAYMENT                                                                                               AS PaymentTermsCode
                           , QL.QUOTATIONID                                                                                           AS QuotationCode
                           , QL.SALESUNIT                                                                                             AS SalesUnitCode
                           , QL.TAXGROUP                                                                                              AS SalesTaxGroupCode
                           , QL.TAXITEMGROUP                                                                                          AS SalesTaxItemGroupCode
                           , QL.SALESQTY                                                                                              AS SalesQuantity
                           , QL.PRICEUNIT                                                                                             AS PriceUnit
                           , QL.PRICEUNIT * ex.CrossRate                                                                              AS PriceUnit_RCY
                           , QL.COSTPRICE                                                                                             AS CostPrice
                           , QL.COSTPRICE * ex.CrossRate                                                                              AS CostPrice_RCY
                           , QL.SALESPRICE                                                                                            AS QuotationPrice
                           , QL.SALESPRICE * ex.CrossRate                                                                             AS QuotationPrice_RCY
                           , QL.LINEDISC                                                                                              AS Discount
                           , QL.LINEDISC * ex.CrossRate                                                                               AS Discount_RCY
                           , QL.LINEPERCENT                                                                                           AS DiscountPercent
                           , QL.MULTILNDISC                                                                                           AS MultilineDiscount
                           , QL.MULTILNDISC * ex.CrossRate                                                                            AS MultilineDiscount_RCY
                           , QL.MULTILNPERCENT                                                                                        AS MultilineDiscountPercent
                           , QL.SALESMARKUP                                                                                           AS SalesCharges
                           , QL.SALESMARKUP * ex.CrossRate                                                                            AS SalesCharges_RCY
                           , QL.LINEAMOUNT                                                                                            AS QuotationAmount
                           , QL.LINEAMOUNT * ex.CrossRate                                                                             AS QuotationAmount_RCY
             FROM            stage_ax.SALESQUOTATIONLINE  AS QL
             LEFT OUTER JOIN stage_ax.SALESQUOTATIONTABLE AS QH ON QL.QUOTATIONID         = QH.QUOTATIONID
                                                               AND QL.DATAAREAID          = QH.DATAAREAID
                                                               AND QL.PARTITION           = QH.PARTITION
                                                               AND QL.data_connection_id  = QH.data_connection_id
                                                               AND QL.company_id          = QH.company_id
             LEFT JOIN       reporting_exchange_rates     AS ex ON ex.FromCurrencyCode    = QL.CURRENCYCODE
                                                               AND ex.DataConnectionID    = QL.data_connection_id
                                                               AND QL.CREATEDDATETIME BETWEEN ex.ValidFrom AND ex.ValidTo
             LEFT OUTER JOIN stage_ax.CUSTQUOTATIONJOUR   AS CQJ ON QL.DATAAREAID         = CQJ.DATAAREAID
                                                                AND QL.PARTITION          = CQJ.PARTITION
                                                                AND QL.data_connection_id = CQJ.data_connection_id
                                                                AND QL.QUOTATIONID        = CQJ.QUOTATIONID
            WHERE
             /** FULL LOAD **/
                             ( @load_type        = 0
                           AND QL.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'N'
                           AND QL.execution_flag               = @execution_flag
                           AND QL.execution_timestamp          > @last_processed_timestamp)
               OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( QL.execution_flag           = @execution_flag
                             AND QL.execution_timestamp      > @last_processed_timestamp)
                            OR ( QH.execution_flag            = @execution_flag
                             AND QH.execution_timestamp       > @last_processed_timestamp)
                            OR ( CQJ.execution_flag          = @execution_flag
                             AND CQJ.execution_timestamp     > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'D'
                           AND QL.execution_flag               = @execution_flag
                           AND QL.execution_timestamp          > @last_processed_timestamp)) ;
GO

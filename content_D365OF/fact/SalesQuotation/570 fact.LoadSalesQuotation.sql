EXEC dbo.drop_object @object = N'fact.LoadSalesQuotation' -- nvarchar(128)
                   , @type = N'P'                         -- nchar(2)
                   , @debug = 0 ;                         -- int
GO

CREATE PROCEDURE fact.LoadSalesQuotation
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  DECLARE @table_name sysname = N'SalesQuotation' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.SalesQuotation ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO fact.SalesQuotation (
      InventTransCode
    , DataAreaCode
    , Partition
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , CreatedDateID
    , ShippingRequestedDateID
    , QuotationExpirationDateID
    , QuotationDocumentSenddateID
    , QuotationDocumentDueDateID
    , SalesTakerID
    , SalesResponsibleID
    , CustomerID
    , AddressID
    , ItemID
    , ProjectCategoryID
    , CurrencyTransactionID
    , CurrencyReportID
    , FinancialDimensionsID
    , SalesOriginID
    , SalesGroupID
    , ModeOfDeliveryID
    , DeliveryTermsID
    , DeliveryTypeID
    , MethodOfPaymentID
    , PaymentTermsID
    , SalesQuotationID
    , SalesTaxID
    , SalesQuantity
    , PriceUnit
    , PriceUnit_RCY
    , CostPrice
    , CostPrice_RCY
    , QuotationPrice
    , QuotationPrice_RCY
    , Discount
    , Discount_RCY
    , DiscountPercent
    , MultilineDiscount
    , MultilineDiscount_RCY
    , MultilineDiscountPercent
    , SalesCharges
    , SalesCharges_RCY
    , QuotationAmount
    , QuotationAmount_RCY
    , StageID
    )
    SELECT InventTransCode
         , DataAreaCode
         , Partition
         , CompanyID
         , DataConnectionID
         , @component_execution_id
         , CreatedDateID
         , ShippingRequestedDateID
         , QuotationExpirationDateID
         , QuotationDocumentSenddateID
         , QuotationDocumentDueDateID
         , SalesTakerID
         , SalesResponsibleID
         , CustomerID
         , AddressID
         , ItemID
         , ProjectCategoryID
         , CurrencyTransactionID
         , CurrencyReportID
         , FinancialDimensionsID
         , SalesOriginID
         , SalesGroupID
         , ModeOfDeliveryID
         , DeliveryTermsID
         , DeliveryTypeID
         , MethodOfPaymentID
         , PaymentTermsID
         , SalesQuotationID
         , SalesTaxID
         , SalesQuantity
         , PriceUnit
         , PriceUnit_RCY
         , CostPrice
         , CostPrice_RCY
         , QuotationPrice
         , QuotationPrice_RCY
         , Discount
         , Discount_RCY
         , DiscountPercent
         , MultilineDiscount
         , MultilineDiscount_RCY
         , MultilineDiscountPercent
         , SalesCharges
         , SalesCharges_RCY
         , QuotationAmount
         , QuotationAmount_RCY
         , StageId
      FROM fact.SalesQuotationView ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.SalesQuotation
         SET      InventTransCode = QV.InventTransCode
                , DataAreaCode = QV.DataAreaCode
                , Partition = QV.Partition
                , CompanyID = QV.CompanyID
                , DataConnectionID = QV.DataConnectionID
                , ComponentExecutionID = @component_execution_id
                , CreatedDateID = QV.CreatedDateID
                , ShippingRequestedDateID = QV.ShippingRequestedDateID
                , QuotationExpirationDateID = QV.QuotationExpirationDateID
                , QuotationDocumentSenddateID = QV.QuotationDocumentSenddateID
                , QuotationDocumentDueDateID = QV.QuotationDocumentDueDateID
                , SalesTakerID = QV.SalesTakerID
                , SalesResponsibleID = QV.SalesResponsibleID
                , CustomerID = QV.CustomerID
                , AddressID = QV.AddressID
                , ItemID = QV.ItemID
                , ProjectCategoryID = QV.ProjectCategoryID
                , CurrencyTransactionID = QV.CurrencyTransactionID
                , CurrencyReportID = QV.CurrencyReportID
                , FinancialDimensionsID = QV.FinancialDimensionsID
                , SalesOriginID = QV.SalesOriginID
                , SalesGroupID = QV.SalesGroupID
                , ModeOfDeliveryID = QV.ModeOfDeliveryID
                , DeliveryTermsID = QV.DeliveryTermsID
                , DeliveryTypeID = QV.DeliveryTypeID
                , MethodOfPaymentID = QV.MethodOfPaymentID
                , PaymentTermsID = QV.PaymentTermsID
                , SalesQuotationID = QV.SalesQuotationID
                , SalesTaxID = QV.SalesTaxID
                , SalesQuantity = QV.SalesQuantity
                , PriceUnit = QV.PriceUnit
                , CostPrice = QV.CostPrice
                , CostPrice_RCY = QV.CostPrice_RCY
                , QuotationPrice = QV.QuotationPrice
                , QuotationPrice_RCY = QV.QuotationPrice_RCY
                , Discount = QV.Discount
                , Discount_RCY = QV.Discount_RCY
                , DiscountPercent = QV.DiscountPercent
                , MultilineDiscount = QV.MultilineDiscount
                , MultilineDiscount_RCY = QV.MultilineDiscount_RCY
                , MultilineDiscountPercent = QV.MultilineDiscountPercent
                , SalesCharges = QV.SalesCharges
                , SalesCharges_RCY = QV.SalesCharges_RCY
                , QuotationAmount = QV.QuotationAmount
                , QuotationAmount_RCY = QV.QuotationAmount_RCY
        FROM      fact.SalesQuotation     AS QV
       INNER JOIN fact.SalesQuotationView AS V ON V.StageId = QV.StageID
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.SalesQuotation (
        InventTransCode
      , DataAreaCode
      , Partition
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , CreatedDateID
      , ShippingRequestedDateID
      , QuotationExpirationDateID
      , QuotationDocumentSenddateID
      , QuotationDocumentDueDateID
      , SalesTakerID
      , SalesResponsibleID
      , CustomerID
      , AddressID
      , ItemID
      , ProjectCategoryID
      , CurrencyTransactionID
      , CurrencyReportID
      , FinancialDimensionsID
      , SalesOriginID
      , SalesGroupID
      , ModeOfDeliveryID
      , DeliveryTermsID
      , DeliveryTypeID
      , MethodOfPaymentID
      , PaymentTermsID
      , SalesQuotationID
      , SalesTaxID
      , SalesQuantity
      , PriceUnit
      , PriceUnit_RCY
      , CostPrice
      , CostPrice_RCY
      , QuotationPrice
      , QuotationPrice_RCY
      , Discount
      , Discount_RCY
      , DiscountPercent
      , MultilineDiscount
      , MultilineDiscount_RCY
      , MultilineDiscountPercent
      , SalesCharges
      , SalesCharges_RCY
      , QuotationAmount
      , QuotationAmount_RCY
      , StageID
      )
      SELECT QV.InventTransCode
           , QV.DataAreaCode
           , QV.Partition
           , QV.CompanyID
           , QV.DataConnectionID
           , @component_execution_id
           , QV.CreatedDateID
           , QV.ShippingRequestedDateID
           , QV.QuotationExpirationDateID
           , QV.QuotationDocumentSendDateID
           , QV.QuotationDocumentDueDateID
           , QV.SalesTakerID
           , QV.SalesResponsibleID
           , QV.CustomerID
           , QV.AddressID
           , QV.ItemID
           , QV.ProjectCategoryID
           , QV.CurrencyTransactionID
           , QV.CurrencyReportID
           , QV.FinancialDimensionsID
           , QV.SalesOriginID
           , QV.SalesGroupID
           , QV.ModeOfDeliveryID
           , QV.DeliveryTermsID
           , QV.DeliveryTypeID
           , QV.MethodOfPaymentID
           , QV.PaymentTermsID
           , QV.SalesQuotationID
           , QV.SalesTaxID
           , QV.SalesQuantity
           , QV.PriceUnit
           , QV.PriceUnit_RCY
           , QV.CostPrice
           , QV.CostPrice_RCY
           , QV.QuotationPrice
           , QV.QuotationPrice_RCY
           , QV.Discount
           , QV.Discount_RCY
           , QV.DiscountPercent
           , QV.MultilineDiscount
           , QV.MultilineDiscount_RCY
           , QV.MultilineDiscountPercent
           , QV.SalesCharges
           , QV.SalesCharges_RCY
           , QV.QuotationAmount
           , QV.QuotationAmount_RCY
           , QV.StageId
        FROM fact.SalesQuotationView AS QV
       WHERE QV.ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.SalesQuotation AS F WHERE F.StageID = QV.StageId) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.SalesQuotation
       WHERE EXISTS ( SELECT 1
                        FROM fact.SalesQuotationView
                       WHERE ExecutionFlag                   = 'D'
                         AND [fact].[SalesQuotation].StageID = StageId) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
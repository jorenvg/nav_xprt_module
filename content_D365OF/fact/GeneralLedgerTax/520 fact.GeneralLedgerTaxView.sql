EXEC dbo.drop_object 'fact.GeneralLedgerTaxView', 'V' ;
GO

CREATE VIEW fact.GeneralLedgerTaxView
AS
  SELECT            glt.StageId
                  , glt.ExecutionFlag
                  , glt.DataConnectionID
                  , glt.CompanyID
                  , glt.Partition
                  , COALESCE(pd.DateID, 0)              AS PostingDateID
                  , COALESCE(jv.VoucherID, 0)           AS VoucherID
                  , COALESCE(tcy.CurrencyID, 0)         AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)         AS CurrencyReportID
                  , COALESCE(tbcy.CurrencyID, 0)        AS TaxBaseTaxCurrencyID
                  , COALESCE(td.SalesTaxDirectionID, 0) AS SalesTaxDirectionID
                  , COALESCE(ta.TaxAuthorityID, 0)      AS TaxAuthorityID
                  , COALESCE(te.TaxExemptID, 0)         AS TaxExemptID
                  , COALESCE(st.TaxID, 0)               AS TaxID
                  , glt.TaxBaseAccountingAmount
                  , glt.TaxBaseTransactionAmount
                  , glt.TaxBaseTransactionAmount_RCY
                  , glt.TaxBaseAmount
                  , glt.TaxBaseAmount_RCY
                  , glt.TaxAccountingAmount
                  , glt.TaxTransactionAmount
                  , glt.TaxTransactionAmount_RCY
                  , glt.TaxAmount
                  , glt.TaxAmount_RCY
    FROM            delta.GeneralLedgerTax AS glt
    LEFT OUTER JOIN dim.Date               AS pd ON glt.PostingDate          = pd.Date
    LEFT OUTER JOIN dim.Voucher            AS jv ON glt.Voucher              = jv.Voucher
                                                AND glt.CompanyID            = jv.CompanyID
                                                AND glt.DataConnectionID     = jv.DataConnectionID
                                                AND glt.Partition            = jv.Partition
    LEFT OUTER JOIN dim.Currency           AS tcy ON glt.TaxCurrency         = tcy.Code
                                                 AND glt.DataConnectionID    = tcy.DataConnectionID
                                                 AND glt.Partition           = tcy.Partition
    LEFT OUTER JOIN dim.Currency           AS tbcy ON glt.TaxBaseTaxCurrency = tbcy.Code
                                                  AND glt.DataConnectionID   = tbcy.DataConnectionID
                                                  AND glt.Partition          = tbcy.Partition
    LEFT OUTER JOIN dim.SalesTaxDirection  AS td ON glt.TaxDirection         = td.SalesTaxDirectionCode
                                                AND glt.DataConnectionID     = td.DataConnectionID
    LEFT OUTER JOIN dim.TaxAuthority       AS ta ON glt.TaxAuthority         = ta.TaxAuthorityCode
                                                AND glt.CompanyID            = ta.CompanyID
                                                AND glt.DataConnectionID     = ta.DataConnectionID
                                                AND glt.Partition            = ta.Partition
    LEFT OUTER JOIN dim.TaxExempt          AS te ON glt.ExemptCode           = te.TaxExemptCode
                                                AND glt.CompanyID            = te.CompanyID
                                                AND glt.DataConnectionID     = te.DataConnectionID
                                                AND glt.Partition            = te.Partition
    LEFT OUTER JOIN dim.Tax                AS st ON glt.TaxCode              = st.TaxCode
                                                AND glt.TaxGroup             = st.TaxGroupCode
                                                AND glt.TaxItemGroup         = st.TaxItemGroupCode
                                                AND glt.CompanyID            = st.CompanyID
                                                AND glt.DataConnectionID     = st.DataConnectionID
                                                AND glt.Partition            = st.Partition
    LEFT OUTER JOIN dim.CurrencyReport     AS rcy ON glt.Partition           = rcy.Partition ;
GO


EXEC dbo.drop_object @object = N'fact.LoadGeneralLedgerTax' -- nvarchar(128)
                   , @type = N'P'                           -- nchar(2)
                   , @debug = 0 ;                           -- int
GO

CREATE PROCEDURE fact.LoadGeneralLedgerTax
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  DECLARE @table_name sysname = N'GeneralLedgerTax' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.GeneralLedgerTax ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO [fact].[GeneralLedgerTax] (
      StageId
    , ComponentExecutionId
    , DataConnectionID
    , CompanyID
    , Partition
    , PostingDateID
    , VoucherID
    , CurrencyTransactionID
    , CurrencyReportID
    , TaxBaseTaxCurrencyID
    , SalesTaxDirectionID
    , TaxAuthorityID
    , TaxExemptID
    , TaxID
    , TaxBaseAccountingAmount
    , TaxBaseTransactionAmount
    , TaxBaseTransactionAmount_RCY
    , TaxBaseAmount
    , TaxBaseAmount_RCY
    , TaxAccountingAmount
    , TaxTransactionAmount
    , TaxTransactionAmount_RCY
    , TaxAmount
    , TaxAmount_RCY
    )
    SELECT StageId
         , @component_execution_id
         , DataConnectionID
         , CompanyID
         , Partition
         , PostingDateID
         , VoucherID
         , CurrencyTransactionID
         , CurrencyReportID
         , TaxBaseTaxCurrencyID
         , SalesTaxDirectionID
         , TaxAuthorityID
         , TaxExemptID
         , TaxID
         , TaxBaseAccountingAmount
         , TaxBaseTransactionAmount
         , TaxBaseTransactionAmount_RCY
         , TaxBaseAmount
         , TaxBaseAmount_RCY
         , TaxAccountingAmount
         , TaxTransactionAmount
         , TaxTransactionAmount_RCY
         , TaxAmount
         , TaxAmount_RCY
      FROM [fact].[GeneralLedgerTaxView] ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      [fact].[GeneralLedgerTax]
         SET      ComponentExecutionId = @component_execution_id
                , DataConnectionID = V.DataConnectionID
                , CompanyID = V.CompanyID
                , Partition = V.Partition
                , PostingDateID = V.PostingDateID
                , VoucherID = V.VoucherID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , TaxBaseTaxCurrencyID = V.TaxBaseTaxCurrencyID
                , SalesTaxDirectionID = V.SalesTaxDirectionID
                , TaxAuthorityID = V.TaxAuthorityID
                , TaxExemptID = V.TaxExemptID
                , TaxID = V.TaxID
                , TaxBaseAccountingAmount = V.TaxBaseAccountingAmount
                , TaxBaseTransactionAmount = V.TaxBaseTransactionAmount
                , TaxBaseAmount = V.TaxBaseAmount
                , TaxAccountingAmount = V.TaxAccountingAmount
                , TaxTransactionAmount = V.TaxTransactionAmount
                , TaxAmount = V.TaxAmount
        FROM      [fact].[GeneralLedgerTax]     F
       INNER JOIN [fact].[GeneralLedgerTaxView] AS V ON F.StageId = V.StageId
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO [fact].[GeneralLedgerTax] (
        StageId
      , ComponentExecutionId
      , DataConnectionID
      , CompanyID
      , Partition
      , PostingDateID
      , VoucherID
      , CurrencyTransactionID
      , CurrencyReportID
      , TaxBaseTaxCurrencyID
      , SalesTaxDirectionID
      , TaxAuthorityID
      , TaxExemptID
      , TaxID
      , TaxBaseAccountingAmount
      , TaxBaseTransactionAmount
      , TaxBaseTransactionAmount_RCY
      , TaxBaseAmount
      , TaxBaseAmount_RCY
      , TaxAccountingAmount
      , TaxTransactionAmount
      , TaxTransactionAmount_RCY
      , TaxAmount
      , TaxAmount_RCY
      )
      SELECT StageId
           , @component_execution_id
           , DataConnectionID
           , CompanyID
           , Partition
           , PostingDateID
           , VoucherID
           , CurrencyTransactionID
           , CurrencyReportID
           , TaxBaseTaxCurrencyID
           , SalesTaxDirectionID
           , TaxAuthorityID
           , TaxExemptID
           , TaxID
           , TaxBaseAccountingAmount
           , TaxBaseTransactionAmount
           , TaxBaseTransactionAmount_RCY
           , TaxBaseAmount
           , TaxBaseAmount_RCY
           , TaxAccountingAmount
           , TaxTransactionAmount
           , TaxTransactionAmount_RCY
           , TaxAmount
           , TaxAmount_RCY
        FROM [fact].[GeneralLedgerTaxView] V
       WHERE ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.GeneralLedgerTax F WHERE F.StageId = V.StageId) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM [fact].[GeneralLedgerTax]
       WHERE EXISTS ( SELECT 1
                        FROM [fact].[GeneralLedgerTaxView] V
                       WHERE ExecutionFlag = 'D'
                         AND V.StageId     = [fact].[GeneralLedgerTax].StageId) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
/****************************************************************************************************
    Functionality:  Loads the delta for the GeneralLegderTax, either incrementally or with a full load based on the @load_type.
			Returns the @inserted, @updated, @deleted records that have been processed on the delta table.
    Created by:          Date:           	
    Date            Changed by      Ticket/Change       Description

*****************************************************************************************************/

EXEC dbo.drop_object @object = N'delta.LoadFactGeneralLedgerTax' -- nvarchar(128)
                   , @type = N'P'                                -- nchar(2)
                   , @debug = 0 ;                                -- int
GO

CREATE PROCEDURE delta.LoadFactGeneralLedgerTax
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @debug                  BIT     = 0
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @execution_flag NCHAR(1) = N'' ;
  DECLARE @table_name sysname = N'GeneralLedgerTax' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  /*debugging*/
  --DECLARE @debug BIT = 1
  --   DECLARE @deleted INT = -1
  --   , @updated INT = -1
  --   , @inserted INT = -1

  --get last processed
  DECLARE @last_processed_timestamp BINARY(8) = (SELECT meta.get_last_processed_timestamp(@fact_table_identifier)) ;

  --truncate delta table
  EXECUTE dbo.truncate_table @delta_table_identifier ;

  IF @load_type = 1 --incremental
  BEGIN
    /****************************************************************************************************
  Functionality:  Incrementally inserts all new records for the GeneralLedgerTax

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'N' ;

    INSERT INTO delta.GeneralLedgerTax (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , DataConnectionID
    , CompanyID
    , Partition
    , PostingDate
    , Voucher
    , TaxCurrency
    , TaxBaseTaxCurrency
    , TaxDirection
    , TaxAuthority
    , ExemptCode
    , TaxCode
    , TaxGroup
    , TaxItemGroup
    , TaxBaseAccountingAmount
    , TaxBaseTransactionAmount
    , TaxBaseTransactionAmount_RCY
    , TaxBaseAmount
    , TaxBaseAmount_RCY
    , TaxAccountingAmount
    , TaxTransactionAmount
    , TaxTransactionAmount_RCY
    , TaxAmount
    , TaxAmount_RCY
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , DataConnectionID
         , CompanyID
         , Partition
         , PostingDate
         , Voucher
         , TaxCurrency
         , TaxBaseTaxCurrency
         , TaxDirection
         , TaxAuthority
         , ExemptCode
         , TaxCode
         , TaxGroup
         , TaxItemGroup
         , TaxBaseAccountingAmount
         , TaxBaseTransactionAmount
         , TaxBaseTransactionAmount_RCY
         , TaxBaseAmount
         , TaxBaseAmount_RCY
         , TaxAccountingAmount
         , TaxTransactionAmount
         , TaxTransactionAmount_RCY
         , TaxAmount
         , TaxAmount_RCY
      FROM delta.GeneralLegderTaxFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @inserted = @@ROWCOUNT ;

    /****************************************************************************************************
  Functionality:  Incrementally inserts all updated records for the GeneralLedgerTax

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'U' ;
    INSERT INTO delta.GeneralLedgerTax (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , DataConnectionID
    , CompanyID
    , Partition
    , PostingDate
    , Voucher
    , TaxCurrency
    , TaxBaseTaxCurrency
    , TaxDirection
    , TaxAuthority
    , ExemptCode
    , TaxCode
    , TaxGroup
    , TaxItemGroup
    , TaxBaseAccountingAmount
    , TaxBaseTransactionAmount
    , TaxBaseTransactionAmount_RCY
    , TaxBaseAmount
    , TaxBaseAmount_RCY
    , TaxAccountingAmount
    , TaxTransactionAmount
    , TaxTransactionAmount_RCY
    , TaxAmount
    , TaxAmount_RCY
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , DataConnectionID
         , CompanyID
         , Partition
         , PostingDate
         , Voucher
         , TaxCurrency
         , TaxBaseTaxCurrency
         , TaxDirection
         , TaxAuthority
         , ExemptCode
         , TaxCode
         , TaxGroup
         , TaxItemGroup
         , TaxBaseAccountingAmount
         , TaxBaseTransactionAmount
         , TaxBaseTransactionAmount_RCY
         , TaxBaseAmount
         , TaxBaseAmount_RCY
         , TaxAccountingAmount
         , TaxTransactionAmount
         , TaxTransactionAmount_RCY
         , TaxAmount
         , TaxAmount_RCY
      FROM delta.GeneralLegderTaxFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @inserted = @@ROWCOUNT ;

    /****************************************************************************************************
  Functionality:  Incrementally inserts all deleted records for the GeneralLedgerBudget

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'D' ;
    INSERT INTO delta.GeneralLedgerTax (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , DataConnectionID
    , CompanyID
    , Partition
    , PostingDate
    , Voucher
    , TaxCurrency
    , TaxBaseTaxCurrency
    , TaxDirection
    , TaxAuthority
    , ExemptCode
    , TaxCode
    , TaxGroup
    , TaxItemGroup
    , TaxBaseAccountingAmount
    , TaxBaseTransactionAmount
    , TaxBaseTransactionAmount_RCY
    , TaxBaseAmount
    , TaxBaseAmount_RCY
    , TaxAccountingAmount
    , TaxTransactionAmount
    , TaxTransactionAmount_RCY
    , TaxAmount
    , TaxAmount_RCY
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , DataConnectionID
         , CompanyID
         , Partition
         , PostingDate
         , Voucher
         , TaxCurrency
         , TaxBaseTaxCurrency
         , TaxDirection
         , TaxAuthority
         , ExemptCode
         , TaxCode
         , TaxGroup
         , TaxItemGroup
         , TaxBaseAccountingAmount
         , TaxBaseTransactionAmount
         , TaxBaseTransactionAmount_RCY
         , TaxBaseAmount
         , TaxBaseAmount_RCY
         , TaxAccountingAmount
         , TaxTransactionAmount
         , TaxTransactionAmount_RCY
         , TaxAmount
         , TaxAmount_RCY
      FROM delta.GeneralLegderTaxFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @inserted = @@ROWCOUNT ;

  END ;

  /****************************************************************************************************
    Functionality:  During a full load with insert all records that have not been deleted. 

    Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
  IF @load_type = 0 --full load
  BEGIN
    SET @deleted = 0 ;
    SET @updated = 0 ;
    SET @execution_flag = N'N' ;

    INSERT INTO delta.GeneralLedgerTax (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , DataConnectionID
    , CompanyID
    , Partition
    , PostingDate
    , Voucher
    , TaxCurrency
    , TaxBaseTaxCurrency
    , TaxDirection
    , TaxAuthority
    , ExemptCode
    , TaxCode
    , TaxGroup
    , TaxItemGroup
    , TaxBaseAccountingAmount
    , TaxBaseTransactionAmount
    , TaxBaseTransactionAmount_RCY
    , TaxBaseAmount
    , TaxBaseAmount_RCY
    , TaxAccountingAmount
    , TaxTransactionAmount
    , TaxTransactionAmount_RCY
    , TaxAmount
    , TaxAmount_RCY
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , DataConnectionID
         , CompanyID
         , Partition
         , PostingDate
         , Voucher
         , TaxCurrency
         , TaxBaseTaxCurrency
         , TaxDirection
         , TaxAuthority
         , ExemptCode
         , TaxCode
         , TaxGroup
         , TaxItemGroup
         , TaxBaseAccountingAmount
         , TaxBaseTransactionAmount
         , TaxBaseTransactionAmount_RCY
         , TaxBaseAmount
         , TaxBaseAmount_RCY
         , TaxAccountingAmount
         , TaxTransactionAmount
         , TaxTransactionAmount_RCY
         , TaxAmount
         , TaxAmount_RCY
      FROM delta.GeneralLegderTaxFunction(@last_processed_timestamp, 'N', 0, @component_execution_id) ; --Full Load
    SELECT @inserted = @@ROWCOUNT ;
  END ;
END ;
EXEC dbo.drop_object 'delta.SalesMarkup', 'T' ;
GO
CREATE TABLE [delta].[SalesMarkup]
(
  StageId                      BIGINT            NOT NULL
, ExecutionFlag                NCHAR(1)          NOT NULL
, ComponentExecutionId         INT               NOT NULL
, SalesMarkupRecID             BIGINT            NULL
, SalesMarkupPartition         BIGINT            NULL
, CompanyID                    [INT]             NOT NULL
, DataConnectionID             [INT]             NOT NULL
, [OriginTableCode]            [INT]             NULL
, [OriginTable]                [NVARCHAR](100)   NULL
, [OriginTransRecID]           [BIGINT]          NULL
, [SalesOrderCode]             [NVARCHAR](100)   NULL
, [SalesInvoice]               [NVARCHAR](100)   NULL
, InvoiceDate                  [DATETIME]        NULL
, NumberSequenceGroup          [NVARCHAR](10)    NULL
, SalesPackingSlipCode         [NVARCHAR](100)   NULL
, TransactionDate              [DATETIME]        NULL
, DocumentDate                 [DATETIME]        NULL
, DeliveryDate                 [DATETIME]        NULL
, InvoiceAddressCode           [BIGINT]          NULL
, DeliveryAddressCode          [BIGINT]          NULL
, CustomerAccountCode          [NVARCHAR](100)   NULL
, InvoiceAccountCode           [NVARCHAR](100)   NULL
, ReceiptRequestedDate         [DATETIME]        NULL
, ShippingRequestedDate        [DATETIME]        NULL
, ReceiptConfirmedDate         [DATETIME]        NULL
, ShippingConfirmedDate        [DATETIME]        NULL
, SalesTakerCode               [BIGINT]          NULL
, SalesResponsibleCode         [BIGINT]          NULL
, VoucherCode                  [NVARCHAR](100)   NULL
, SalesOrderTypeCode           [NVARCHAR](100)   NULL
, DeliveryTermCode             [NVARCHAR](100)   NULL
, DeliveryModeCode             [NVARCHAR](100)   NULL
, PaymentTermCode              [NVARCHAR](100)   NULL
, PaymentModeCode              [NVARCHAR](100)   NULL
, SalesOriginCode              [NVARCHAR](100)   NULL
, SalesGroupCode               [NVARCHAR](100)   NULL
, SalesPoolCode                [NVARCHAR](100)   NULL
, [CurrencyCode]               [NVARCHAR](10)    NULL
, [MarkupCode]                 [NVARCHAR](100)   NULL
, [ModuleType]                 [INT]             NULL
, [TaxGroupCode]               [NVARCHAR](100)   NULL
, [TaxItemGroupCode]           [NVARCHAR](100)   NULL
, [MarkUpCalculatedAmount]     DECIMAL(19,4)     NULL
, [MarkUpCalculatedAmount_RCY] DECIMAL(19,4)     NULL
, [MarkUpPostedAmount]         DECIMAL(19,4)     NULL
, [MarkUpPostedAmount_RCY]     DECIMAL(19,4)     NULL
, [MarkupTaxAmount]            DECIMAL(19,4)     NULL
, [MarkupTaxAmount_RCY]        DECIMAL(19,4)     NULL
) ;
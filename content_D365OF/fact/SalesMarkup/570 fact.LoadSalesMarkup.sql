EXEC dbo.drop_object @object = N'fact.LoadSalesMarkup' -- nvarchar(128)
                   , @type = N'P'                      -- nchar(2)
                   , @debug = 0 ;                      -- int
GO

CREATE PROCEDURE fact.LoadSalesMarkup
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @table_name sysname = N'SalesMarkup' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.SalesMarkup ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO fact.SalesMarkup (
      StageID
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , SalesMarkupRecID
    , SalesMarkupPartition
    , TransactionDateID
    , DocumentDateID
    , DeliveryDateID
    , ReceiptRequestedDateID
    , ShippingRequestedDateID
    , ReceiptConfirmedDateID
    , ShippingConfirmedDateID
    , InvoiceAddressID
    , DeliveryAddressID
    , CustomerAccountID
    , InvoiceAccountID
    , SalesTakerID
    , SalesResponsibleID
    , VoucherID
    , SalesOrderTypeID
    , DeliveryTermID
    , ModeOfDeliveryID
    , PaymentTermsID
    , MethodOfPaymentID
    , SalesOriginID
    , SalesGroupID
    , SalesPoolID
    , CurrencyTransactionID
    , CurrencyReportID
    , SalesMarkupOriginID
    , MarkupTypeID
    , SalesOrderID
    , SalesInvoiceID
    , SalesPackingSlipID
    , MarkUpCalculatedAmount
    , MarkUpCalculatedAmount_RCY
    , MarkUpPostedAmount
    , MarkUpPostedAmount_RCY
    , MarkupTaxAmount
    , MarkupTaxAmount_RCY
    )
    SELECT StageID
         , CompanyID
         , DataConnectionID
         , @component_execution_id
         , SalesMarkupRecID
         , SalesMarkupPartition
         , TransactionDateID
         , DocumentDateID
         , DeliveryDateID
         , ReceiptRequestedDateID
         , ShippingRequestedDateID
         , ReceiptConfirmedDateID
         , ShippingConfirmedDateID
         , InvoiceAddressID
         , DeliveryAddressID
         , CustomerAccountID
         , InvoiceAccountID
         , SalesTakerID
         , SalesResponsibleID
         , VoucherID
         , SalesOrderTypeID
         , DeliveryTermID
         , ModeOfDeliveryID
         , PaymentTermsID
         , MethodOfPaymentID
         , SalesOriginID
         , SalesGroupID
         , SalesPoolID
         , CurrencyTransactionID
         , CurrencyReportID
         , SalesMarkupOriginID
         , MarkupTypeID
         , SalesOrderID
         , SalesInvoiceID
         , SalesPackingSlipID
         , MarkUpCalculatedAmount
         , MarkUpCalculatedAmount_RCY
         , MarkUpPostedAmount
         , MarkUpPostedAmount_RCY
         , MarkupTaxAmount
         , MarkupTaxAmount_RCY
      FROM fact.SalesMarkupView ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.SalesMarkup
         SET      CompanyID = V.CompanyID
                , DataConnectionID = V.DataConnectionID
                , ComponentExecutionID = @component_execution_id
                , SalesMarkupRecID = V.SalesMarkupRecID
                , SalesMarkupPartition = V.SalesMarkupPartition
                , TransactionDateID = V.TransactionDateID
                , DocumentDateID = V.DocumentDateID
                , DeliveryDateID = V.DeliveryDateID
                , ReceiptRequestedDateID = V.ReceiptRequestedDateID
                , ShippingRequestedDateID = V.ShippingRequestedDateID
                , ReceiptConfirmedDateID = V.ReceiptConfirmedDateID
                , ShippingConfirmedDateID = V.ShippingConfirmedDateID
                , InvoiceAddressID = V.InvoiceAddressID
                , DeliveryAddressID = V.DeliveryAddressID
                , CustomerAccountID = V.CustomerAccountID
                , InvoiceAccountID = V.InvoiceAccountID
                , SalesTakerID = V.SalesTakerID
                , SalesResponsibleID = V.SalesResponsibleID
                , VoucherID = V.VoucherID
                , SalesOrderTypeID = V.SalesOrderTypeID
                , DeliveryTermID = V.DeliveryTermID
                , ModeOfDeliveryID = V.ModeOfDeliveryID
                , PaymentTermsID = V.PaymentTermsID
                , MethodOfPaymentID = V.MethodOfPaymentID
                , SalesOriginID = V.SalesOriginID
                , SalesGroupID = V.SalesGroupID
                , SalesPoolID = V.SalesPoolID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , SalesMarkupOriginID = V.SalesMarkupOriginID
                , MarkupTypeID = V.MarkupTypeID
                , SalesOrderID = V.SalesOrderID
                , SalesInvoiceID = V.SalesInvoiceID
                , SalesPackingSlipID = V.SalesPackingSlipID
                , MarkUpCalculatedAmount = V.MarkUpCalculatedAmount
                , MarkUpCalculatedAmount_RCY = V.MarkUpCalculatedAmount_RCY
                , MarkUpPostedAmount = V.MarkUpPostedAmount
                , MarkUpPostedAmount_RCY = V.MarkUpPostedAmount_RCY
                , MarkupTaxAmount = V.MarkupTaxAmount
                , MarkupTaxAmount_RCY = V.MarkupTaxAmount_RCY
        FROM      fact.SalesMarkup     F
       INNER JOIN fact.SalesMarkupView AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.SalesMarkup (
        StageID
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , SalesMarkupRecID
      , SalesMarkupPartition
      , TransactionDateID
      , DocumentDateID
      , DeliveryDateID
      , ReceiptRequestedDateID
      , ShippingRequestedDateID
      , ReceiptConfirmedDateID
      , ShippingConfirmedDateID
      , InvoiceAddressID
      , DeliveryAddressID
      , CustomerAccountID
      , InvoiceAccountID
      , SalesTakerID
      , SalesResponsibleID
      , VoucherID
      , SalesOrderTypeID
      , DeliveryTermID
      , ModeOfDeliveryID
      , PaymentTermsID
      , MethodOfPaymentID
      , SalesOriginID
      , SalesGroupID
      , SalesPoolID
      , CurrencyTransactionID
      , CurrencyReportID
      , SalesMarkupOriginID
      , MarkupTypeID
      , SalesOrderID
      , SalesInvoiceID
      , SalesPackingSlipID
      , MarkUpCalculatedAmount
      , MarkUpCalculatedAmount_RCY
      , MarkUpPostedAmount
      , MarkUpPostedAmount_RCY
      , MarkupTaxAmount
      , MarkupTaxAmount_RCY
      )
      SELECT V.StageID
           , V.CompanyID
           , V.DataConnectionID
           , @component_execution_id
           , V.SalesMarkupRecID
           , V.SalesMarkupPartition
           , V.TransactionDateID
           , V.DocumentDateID
           , V.DeliveryDateID
           , V.ReceiptRequestedDateID
           , V.ShippingRequestedDateID
           , V.ReceiptConfirmedDateID
           , V.ShippingConfirmedDateID
           , V.InvoiceAddressID
           , V.DeliveryAddressID
           , V.CustomerAccountID
           , V.InvoiceAccountID
           , V.SalesTakerID
           , V.SalesResponsibleID
           , V.VoucherID
           , V.SalesOrderTypeID
           , V.DeliveryTermID
           , V.ModeOfDeliveryID
           , V.PaymentTermsID
           , V.MethodOfPaymentID
           , V.SalesOriginID
           , V.SalesGroupID
           , V.SalesPoolID
           , V.CurrencyTransactionID
           , V.CurrencyReportID
           , V.SalesMarkupOriginID
           , V.MarkupTypeID
           , V.SalesOrderID
           , V.SalesInvoiceID
           , V.SalesPackingSlipID
           , V.MarkUpCalculatedAmount
           , V.MarkUpCalculatedAmount_RCY
           , V.MarkUpPostedAmount
           , V.MarkUpPostedAmount_RCY
           , V.MarkupTaxAmount
           , V.MarkupTaxAmount_RCY
        FROM fact.SalesMarkupView V
       WHERE V.ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.SalesMarkup F WHERE F.StageID = V.StageID) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.SalesMarkup
       WHERE EXISTS ( SELECT 1
                        FROM fact.SalesMarkupView V
                       WHERE V.ExecutionFlag = 'D'
                         AND V.StageID       = [fact].[SalesMarkup].StageID) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
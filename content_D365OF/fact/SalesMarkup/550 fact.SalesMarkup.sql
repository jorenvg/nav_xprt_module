EXEC dbo.drop_object 'fact.SalesMarkup', 'T' ;
GO
CREATE TABLE [fact].[SalesMarkup]
(
  StageID                      BIGINT            NOT NULL
, [CompanyID]                  [INT]             NOT NULL
, [DataConnectionID]           [INT]             NOT NULL
, [ComponentExecutionID]       INT               NOT NULL
, [SalesMarkupRecID]           [BIGINT]          NULL
, [SalesMarkupPartition]       [BIGINT]          NULL
, [TransactionDateID]          [INT]             NOT NULL
, DocumentDateID               [INT]             NOT NULL
, DeliveryDateID               [INT]             NOT NULL
, ReceiptRequestedDateID       [INT]             NOT NULL
, ShippingRequestedDateID      [INT]             NOT NULL
, ReceiptConfirmedDateID       [INT]             NOT NULL
, ShippingConfirmedDateID      [INT]             NOT NULL
, InvoiceAddressID             [INT]             NOT NULL
, DeliveryAddressID            [INT]             NOT NULL
, CustomerAccountID            [INT]             NOT NULL
, InvoiceAccountID             [INT]             NOT NULL
, SalesTakerID                 [INT]             NOT NULL
, SalesResponsibleID           [INT]             NOT NULL
, VoucherID                    [INT]             NOT NULL
, SalesOrderTypeID             [INT]             NOT NULL
, DeliveryTermID               [INT]             NOT NULL
, ModeOfDeliveryID             [INT]             NOT NULL
, PaymentTermsID               [INT]             NOT NULL
, MethodOfPaymentID            [INT]             NOT NULL
, SalesOriginID                [INT]             NOT NULL
, SalesGroupID                 [INT]             NOT NULL
, SalesPoolID                  [INT]             NOT NULL
, CurrencyTransactionID        INT               NOT NULL
, CurrencyReportID             INT               NOT NULL
, [SalesMarkupOriginID]        [INT]             NOT NULL
, [MarkupTypeID]               [INT]             NOT NULL
, [SalesOrderID]               [INT]             NOT NULL
, [SalesInvoiceID]             [INT]             NOT NULL
, [SalesPackingSlipID]         [INT]             NOT NULL
, [MarkUpCalculatedAmount]     DECIMAL(19,4)     NULL
, [MarkUpCalculatedAmount_RCY] DECIMAL(19,4)     NULL
, [MarkUpPostedAmount]         DECIMAL(19,4)     NULL
, [MarkUpPostedAmount_RCY]     DECIMAL(19,4)     NULL
, [MarkupTaxAmount]            DECIMAL(19,4)     NULL
, [MarkupTaxAmount_RCY]        DECIMAL(19,4)     NULL
) ;

/****************************************************************************************************
    Functionality:  Loads the delta for the GeneralLegderBudget, either incrementally or with a full load based on the @load_type.
			Returns the @inserted, @updated, @deleted records that have been processed on the delta table.
    Created by:          Date:           	
    Date            Changed by      Ticket/Change       Description

*****************************************************************************************************/

EXEC dbo.drop_object @object = N'delta.LoadFactGeneralLedgerBudget' -- nvarchar(128)
                   , @type = N'P'                                   -- nchar(2)
                   , @debug = 0 ;                                   -- int
GO

CREATE PROCEDURE delta.LoadFactGeneralLedgerBudget
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @execution_flag NCHAR(1) = N'' ;
  DECLARE @table_name sysname = N'GeneralLedgerBudget' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  /*debugging*/
  --DECLARE @debug BIT = 1
  --   DECLARE @deleted INT = -1
  --   , @updated INT = -1
  --   , @inserted INT = -1

  --get last processed
  DECLARE @last_processed_timestamp BINARY(8) = (SELECT meta.get_last_processed_timestamp(@fact_table_identifier)) ;

  --truncate delta table
  EXECUTE dbo.truncate_table @delta_table_identifier ;

  IF @load_type = 1 --incremental
  BEGIN
    /****************************************************************************************************
  Functionality:  Incrementally inserts all new records for the GeneralLedgerBudget

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'N' ;

    INSERT INTO delta.GeneralLedgerBudget (
      [StageId]
    , [ExecutionFlag]
    , [ComponentExecutionId]
    , [DataConnectionID]
    , [CompanyID]
    , [Partition]
    , [LedgerDimension]
    , [PostingDate]
    , [Voucher]
    , [BudgetModel]
    , [BudgetSubModel]
    , [BudgetCode]
    , [BudgetPlanHeaderDocumentNumber]
    , [LedgerChartofAccounts]
    , [Currency]
    , [MainAccountID]
    , [IsCredit]
    , [Balance]
    , [TransactionAmount]
    , [TransactionAmount_RCY]
    , [AccountingAmount]
    )
    SELECT [StageId]
         , @execution_flag
         , @component_execution_id
         , [DataConnectionID]
         , [CompanyID]
         , [Partition]
         , [LedgerDimension]
         , [PostingDate]
         , [Voucher]
         , [BudgetModel]
         , [BudgetSubModel]
         , [BudgetCode]
         , [BudgetPlanHeaderDocumentNumber]
         , [LedgerChartofAccounts]
         , [Currency]
         , [MainAccountID]
         , [IsCredit]
         , [Balance]
         , [TransactionAmount]
         , [TransactionAmount_RCY]
         , [AccountingAmount]
      FROM delta.GeneralLegderBudgetFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @inserted = @@ROWCOUNT ;

    /****************************************************************************************************
  Functionality:  Incrementally inserts all updated records for the GeneralLedgerBudget

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'U' ;
    INSERT INTO delta.GeneralLedgerBudget (
      [StageId]
    , [ExecutionFlag]
    , [ComponentExecutionId]
    , [DataConnectionID]
    , [CompanyID]
    , [Partition]
    , [LedgerDimension]
    , [PostingDate]
    , [Voucher]
    , [BudgetModel]
    , [BudgetSubModel]
    , [BudgetCode]
    , [BudgetPlanHeaderDocumentNumber]
    , [LedgerChartofAccounts]
    , [Currency]
    , [MainAccountID]
    , [IsCredit]
    , [Balance]
    , [TransactionAmount]
    , [TransactionAmount_RCY]
    , [AccountingAmount]
    )
    SELECT [StageId]
         , @execution_flag
         , @component_execution_id
         , [DataConnectionID]
         , [CompanyID]
         , [Partition]
         , [LedgerDimension]
         , [PostingDate]
         , [Voucher]
         , [BudgetModel]
         , [BudgetSubModel]
         , [BudgetCode]
         , [BudgetPlanHeaderDocumentNumber]
         , [LedgerChartofAccounts]
         , [Currency]
         , [MainAccountID]
         , [IsCredit]
         , [Balance]
         , [TransactionAmount]
         , [TransactionAmount_RCY]
         , [AccountingAmount]
      FROM delta.GeneralLegderBudgetFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @updated = @@ROWCOUNT ;

    /****************************************************************************************************
  Functionality:  Incrementally inserts all deleted records for the GeneralLedgerBudget

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'D' ;

    INSERT INTO delta.GeneralLedgerBudget (
      [StageId]
    , [ExecutionFlag]
    , [ComponentExecutionId]
    , [DataConnectionID]
    , [CompanyID]
    , [Partition]
    , [LedgerDimension]
    , [PostingDate]
    , [Voucher]
    , [BudgetModel]
    , [BudgetSubModel]
    , [BudgetCode]
    , [BudgetPlanHeaderDocumentNumber]
    , [LedgerChartofAccounts]
    , [Currency]
    , [MainAccountID]
    , [IsCredit]
    , [Balance]
    , [TransactionAmount]
    , [TransactionAmount_RCY]
    , [AccountingAmount]
    )
    SELECT [StageId]
         , @execution_flag
         , @component_execution_id
         , [DataConnectionID]
         , [CompanyID]
         , [Partition]
         , [LedgerDimension]
         , [PostingDate]
         , [Voucher]
         , [BudgetModel]
         , [BudgetSubModel]
         , [BudgetCode]
         , [BudgetPlanHeaderDocumentNumber]
         , [LedgerChartofAccounts]
         , [Currency]
         , [MainAccountID]
         , [IsCredit]
         , [Balance]
         , [TransactionAmount]
         , [TransactionAmount_RCY]
         , [AccountingAmount]
      FROM delta.GeneralLegderBudgetFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @deleted = @@ROWCOUNT ;

  END ;

  /****************************************************************************************************
    Functionality:  During a full load with insert all records that have not been deleted. 

    Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
  IF @load_type = 0 --full load
  BEGIN
    SET @deleted = 0 ;
    SET @updated = 0 ;
    SET @execution_flag = N'N' ;

    INSERT INTO delta.GeneralLedgerBudget (
      [StageId]
    , [ExecutionFlag]
    , [ComponentExecutionId]
    , [DataConnectionID]
    , [CompanyID]
    , [Partition]
    , [LedgerDimension]
    , [PostingDate]
    , [Voucher]
    , [BudgetModel]
    , [BudgetSubModel]
    , [BudgetCode]
    , [BudgetPlanHeaderDocumentNumber]
    , [LedgerChartofAccounts]
    , [Currency]
    , [MainAccountID]
    , [IsCredit]
    , [Balance]
    , [TransactionAmount]
    , [TransactionAmount_RCY]
    , [AccountingAmount]
    )
    SELECT [StageId]
         , @execution_flag
         , @component_execution_id
         , [DataConnectionID]
         , [CompanyID]
         , [Partition]
         , [LedgerDimension]
         , [PostingDate]
         , [Voucher]
         , [BudgetModel]
         , [BudgetSubModel]
         , [BudgetCode]
         , [BudgetPlanHeaderDocumentNumber]
         , [LedgerChartofAccounts]
         , [Currency]
         , [MainAccountID]
         , [IsCredit]
         , [Balance]
         , [TransactionAmount]
         , [TransactionAmount_RCY]
         , [AccountingAmount]
      FROM delta.GeneralLegderBudgetFunction(@last_processed_timestamp, 'N', 0, @component_execution_id) ; -- FULL LOAD

    SELECT @inserted = @@ROWCOUNT ;
  END ;
END ;

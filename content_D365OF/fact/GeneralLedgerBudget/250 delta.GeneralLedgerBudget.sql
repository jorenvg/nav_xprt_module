EXEC dbo.drop_object 'delta.GeneralLedgerBudget', 'T' ;
GO
CREATE TABLE delta.GeneralLedgerBudget
(
  StageId                        BIGINT          NOT NULL
, ExecutionFlag                  NCHAR(1)        NOT NULL
, ComponentExecutionId           INT             NOT NULL
, DataConnectionID               INT             NOT NULL
, CompanyID                      INT             NOT NULL
, Partition                      BIGINT          NULL
, LedgerDimension                BIGINT          NULL
, PostingDate                    DATETIME        NULL
, Voucher                        NVARCHAR(100)   NULL
, BudgetModel                    NVARCHAR(100)   NULL
, BudgetSubModel                 NVARCHAR(100)   NULL
, BudgetCode                     NVARCHAR(100)   NULL
, BudgetPlanHeaderDocumentNumber NVARCHAR(100)   NULL
, LedgerChartofAccounts          BIGINT          NULL
, Currency                       NVARCHAR(100)   NULL
, MainAccountID                  NVARCHAR(100)   NULL
, IsCredit                       INT             NULL
, Balance                        INT             NULL
, TransactionAmount              DECIMAL(19,4)   NULL
, TransactionAmount_RCY          DECIMAL(19,4)   NULL
, AccountingAmount               DECIMAL(19,4)   NULL
) ;                              
GO
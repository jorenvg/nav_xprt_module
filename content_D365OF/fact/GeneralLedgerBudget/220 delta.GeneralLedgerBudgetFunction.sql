EXEC dbo.drop_object 'delta.GeneralLegderBudgetFunction', 'F' ;
GO
CREATE FUNCTION delta.GeneralLegderBudgetFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            BTL.stage_id                                                 AS StageId
                           , @execution_flag                                              AS ExecutionFlag
                           , @component_execution_id                                      AS ComponentExecutionId
                           , BTL.data_connection_id                                       AS DataConnectionID
                           , BTL.company_id                                               AS CompanyID
                           , BTL.PARTITION                                                AS Partition
                           , BTL.LEDGERDIMENSION                                          AS LedgerDimension
                           , BTL.DATE                                                     AS PostingDate
                           , BTH.TRANSACTIONNUMBER                                        AS Voucher
                           , BTH.BUDGETMODELID                                            AS BudgetModel
                           , BTH.BUDGETSUBMODELID                                         AS BudgetSubModel
                           , BTH.BUDGETTRANSACTIONCODE                                    AS BudgetCode
                           , bph.DOCUMENTNUMBER                                           AS BudgetPlanHeaderDocumentNumber
                           , L.CHARTOFACCOUNTS                                            AS LedgerChartofAccounts
                           , BTL.TRANSACTIONCURRENCY                                      AS Currency
                           , MA.MAINACCOUNTID                                             AS MainAccountID
                           , CASE WHEN BTL.ACCOUNTINGCURRENCYAMOUNT < 0 THEN 1 ELSE 0 END AS IsCredit --ISCREDIT
                           , CASE WHEN MA.TYPE IN ( 0, 1, 2 ) THEN 0 ELSE 1 END           AS Balance
                           , BTL.TRANSACTIONCURRENCYAMOUNT                                AS TransactionAmount
                           , BTL.TRANSACTIONCURRENCYAMOUNT * ex.CrossRate                 AS TransactionAmount_RCY
                           , BTL.ACCOUNTINGCURRENCYAMOUNT                                 AS AccountingAmount
             FROM            stage_ax.BUDGETTRANSACTIONLINE                   AS BTL
            INNER JOIN       stage_ax.BUDGETTRANSACTIONHEADER                 AS BTH ON BTL.BUDGETTRANSACTIONHEADER = BTH.RECID
                                                                                    AND BTL.company_id              = BTH.company_id
                                                                                    AND BTL.data_connection_id      = BTH.data_connection_id
                                                                                    AND BTL.PARTITION               = BTH.PARTITION
             LEFT OUTER JOIN stage_ax.DIMENSIONATTRIBUTEVALUECOMBINATION      AS DAVC ON BTL.LEDGERDIMENSION        = DAVC.RECID
                                                                                     AND BTL.data_connection_id     = DAVC.data_connection_id
                                                                                     AND BTL.PARTITION              = DAVC.PARTITION
             LEFT OUTER JOIN stage_ax.MAINACCOUNT                             AS MA ON DAVC.MAINACCOUNT             = MA.RECID
                                                                                   AND BTL.data_connection_id       = MA.data_connection_id
                                                                                   AND BTL.PARTITION                = MA.PARTITION
             LEFT OUTER JOIN stage_ax.LEDGER                                  AS L ON BTH.PRIMARYLEDGER             = L.RECID
                                                                                  AND BTL.data_connection_id        = L.data_connection_id
                                                                                  AND BTL.PARTITION                 = L.PARTITION
             LEFT OUTER JOIN stage_ax.BUDGETPLANLINEBUDGETTRANSACTIONLINELINK AS ll ON BTL.RECID                    = ll.BUDGETTRANSACTIONLINE
                                                                                   AND BTL.PARTITION                = ll.PARTITION
             LEFT OUTER JOIN stage_ax.BUDGETPLANLINE                          AS bpl ON ll.BUDGETPLANLINE           = bpl.RECID
                                                                                    AND ll.PARTITION                = bpl.PARTITION
             LEFT OUTER JOIN stage_ax.BUDGETPLANHEADER                        AS bph ON bpl.BUDGETPLANHEADER        = bph.RECID
                                                                                    AND bpl.PARTITION               = bph.PARTITION
             LEFT JOIN       reporting_exchange_rates                         AS ex ON BTL.TRANSACTIONCURRENCY      = ex.FromCurrencyCode
                                                                                   AND BTL.data_connection_id       = ex.DataConnectionID
                                                                                   AND BTL.DATE BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE
             /** FULL LOAD **/
                             ( @load_type         = 0
                           AND BTL.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                       = 1
                           AND @execution_flag                  = 'N'
                           AND BTL.execution_flag               = @execution_flag
                           AND BTL.execution_timestamp          > @last_processed_timestamp)
               OR
               /** INCREMENTAL, UPDATED RECORDS **/
                             ( @load_type                       = 1
                           AND @execution_flag                  = 'U'
                           AND ( ( BTL.execution_flag           = @execution_flag
                               AND BTL.execution_timestamp      > @last_processed_timestamp)
                              OR ( BTH.execution_flag           = @execution_flag
                               AND BTH.execution_timestamp      > @last_processed_timestamp)
                              OR ( DAVC.execution_flag          = @execution_flag
                               AND DAVC.execution_timestamp     > @last_processed_timestamp)
                              OR ( MA.execution_flag            = @execution_flag
                               AND MA.execution_timestamp       > @last_processed_timestamp)
                              OR ( L.execution_flag             = @execution_flag
                               AND L.execution_timestamp        > @last_processed_timestamp)
                              OR ( ll.execution_flag            = @execution_flag
                               AND ll.execution_timestamp       > @last_processed_timestamp)
                              OR ( bpl.execution_flag           = @execution_flag
                               AND bpl.execution_timestamp      > @last_processed_timestamp)
                              OR ( bph.execution_flag           = @execution_flag
                               AND bph.execution_timestamp      > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                       = 1
                           AND @execution_flag                  = 'D'
                           AND BTL.execution_flag               = @execution_flag
                           AND BTL.execution_timestamp          > @last_processed_timestamp)) ;

GO
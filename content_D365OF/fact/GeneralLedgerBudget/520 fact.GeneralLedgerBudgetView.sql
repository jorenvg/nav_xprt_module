/***
  Currently the JournalDimensionID is never filled with a value. This issue DEV-783 has been created (https://hillstar.atlassian.net/browse/DEV-783)
***/
EXEC dbo.drop_object 'fact.GeneralLedgerBudgetView', 'V' ;
GO

CREATE VIEW fact.GeneralLedgerBudgetView
AS
  SELECT            glb.StageId
                  , glb.ExecutionFlag
                  , glb.DataConnectionID
                  , glb.CompanyID
                  , glb.Partition
                  , COALESCE(fd.FinancialDimensionsID, 0) AS FinancialDimensionsID
                  , COALESCE(pd.DateID, 0)                AS PostingDateID
                  , COALESCE(jv.VoucherID, 0)             AS VoucherID
                  , COALESCE(bm.BudgetModelID, 0)         AS BudgetModelID
                  , COALESCE(BC.BudgetCodeID, 0)          AS BudgetCodeID
                  , COALESCE(BP.BudgetPlanID, 0)          AS BudgetPlanID
                  , COALESCE(coa.ChartOfAccountsID, 0)    AS ChartOfAccountsID
                  , COALESCE(cy.CurrencyID, 0)            AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)           AS CurrencyReportID
                  , COALESCE(glb.IsCredit, 0)             AS IsCredit
                  , COALESCE(glb.Balance, 0)              AS Balance
                  , glb.TransactionAmount
                  , glb.TransactionAmount_RCY
                  , glb.AccountingAmount
    FROM            delta.GeneralLedgerBudget AS glb
    LEFT OUTER JOIN dim.FinancialDimensions   AS fd ON glb.LedgerDimension        = fd.LedgerDimension
                                                   AND glb.DataConnectionID       = fd.DataConnectionID
    LEFT OUTER JOIN dim.Date                  AS pd ON glb.PostingDate            = pd.Date
    LEFT OUTER JOIN dim.Voucher               AS jv ON glb.Voucher                = jv.Voucher
                                                   AND glb.CompanyID              = jv.CompanyID
                                                   AND glb.DataConnectionID       = jv.DataConnectionID
                                                   AND glb.Partition              = jv.Partition
    LEFT OUTER JOIN dim.BudgetModel           AS bm ON glb.BudgetModel            = bm.BudgetModelCode
                                                   AND glb.BudgetSubModel         = bm.BudgetSubmodelCode
                                                   AND glb.CompanyID              = bm.CompanyID
                                                   AND glb.DataConnectionID       = bm.DataConnectionID
    LEFT OUTER JOIN dim.ChartOfAccounts       AS coa ON glb.LedgerChartofAccounts = coa.ChartOfAccountsCode
                                                    AND glb.MainAccountID         = coa.MainAccountCode
                                                    AND glb.DataConnectionID      = coa.DataConnectionID
                                                    AND glb.Partition             = coa.Partition
    LEFT OUTER JOIN dim.Currency              AS cy ON glb.Currency               = cy.Code
                                                   AND glb.DataConnectionID       = cy.DataConnectionID
                                                   AND glb.Partition              = cy.Partition
    LEFT OUTER JOIN dim.BudgetCode            AS BC ON BC.BudgetCode              = glb.BudgetCode
                                                   AND glb.CompanyID              = BC.CompanyID
                                                   AND glb.DataConnectionID       = BC.DataConnectionID
                                                   AND BC.Partition               = glb.Partition
    LEFT OUTER JOIN dim.BudgetPlan            AS BP ON BP.BudgetPlanCode          = glb.BudgetPlanHeaderDocumentNumber
                                                   AND glb.DataConnectionID       = BP.DataConnectionID
                                                   AND BP.Partition               = glb.Partition
    LEFT OUTER JOIN dim.CurrencyReport        AS rcy ON glb.Partition             = rcy.Partition ;
GO


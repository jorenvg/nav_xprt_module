EXEC dbo.drop_object 'fact.GeneralLedgerBudget', 'T' ;
GO

CREATE TABLE fact.GeneralLedgerBudget
(
  DataConnectionID      INT             NOT NULL
, CompanyID             INT             NOT NULL
, ComponentExecutionID  INT             NOT NULL
, Partition             BIGINT          NULL
, FinancialDimensionsID INT             NOT NULL
, VoucherID             INT             NOT NULL
, PostingDateID         INT             NOT NULL
, BudgetModelID         INT             NOT NULL
, BudgetCodeID          INT             NOT NULL
, BudgetPlanID          INT             NOT NULL
, ChartOfAccountsID     INT             NOT NULL
, CurrencyTransactionID INT             NOT NULL
, CurrencyReportID      INT             NOT NULL
, IsCredit              INT             NULL
, Balance               INT             NULL
, TransactionAmount     DECIMAL(19,4)   NULL
, TransactionAmount_RCY DECIMAL(19,4)   NULL
, AccountingAmount      DECIMAL(19,4)   NULL
, StageId               BIGINT
) ;
GO
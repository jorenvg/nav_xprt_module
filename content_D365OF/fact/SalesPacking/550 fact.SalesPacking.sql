EXEC dbo.drop_object 'fact.SalesPacking', 'T' ;
GO
CREATE TABLE [fact].[SalesPacking]
(
  StageID                      BIGINT          NOT NULL
, [CompanyID]                  [INT]           NOT NULL
, [DataConnectionID]           [INT]           NOT NULL
, [ComponentExecutionID]       INT             NOT NULL
, [SalesPackingTransRecID]     [BIGINT]        NULL
, [SalesPackingTransPartition] [BIGINT]        NULL
, [DocumentDateID]             [INT]           NOT NULL
, [ShippingRequestedDateID]    [INT]           NOT NULL
, [ShippingConfirmedDateID]    [INT]           NOT NULL
, [ReceiptRequestedDateID]     [INT]           NOT NULL
, [ReceiptConfirmedDateID]     [INT]           NOT NULL
, [DeliveryDateID]             [INT]           NOT NULL
, [WMSlocationID]              [INT]           NOT NULL
, [LocationID]                 [INT]           NOT NULL
, [ProductID]                  [INT]           NOT NULL
, [TrackingID]                 [INT]           NOT NULL
, [SalesTakerID]               [INT]           NOT NULL
, [SalesResponsibleID]         INT             NOT NULL
, [CustomerAccountID]          [INT]           NOT NULL
, [InvoiceAccountID]           [INT]           NOT NULL
, [DeliveryAddressID]          [INT]           NOT NULL
, [InvoiceAddressID]           [INT]           NOT NULL
, [ItemID]                     [INT]           NOT NULL
, CurrencyTransactionID        INT             NOT NULL
, CurrencyReportID             INT             NOT NULL
, [FinancialDimensionsID]      [INT]           NOT NULL
, [OrderTypeID]                [INT]           NOT NULL
, [SalesOriginID]              [INT]           NOT NULL
, [SalesPoolID]                [INT]           NOT NULL
, [ModeOfDeliveryID]           [INT]           NOT NULL
, [DeliveryTermsID]            [INT]           NOT NULL
, [MethodOfPaymentID]          [INT]           NOT NULL
, [PaymentTermsID]             [INT]           NOT NULL
, [VoucherID]                  [INT]           NOT NULL
, [SalesOrderID]               [INT]           NOT NULL
, [SalesGroupID]               INT             NOT NULL
, [SalesUnitID]                [INT]           NOT NULL
, [SalesPackingSlipID]         [INT]           NOT NULL
, [OrderedQuantity]            DECIMAL(19,4)   NULL
, [RemainingQuantity]          DECIMAL(19,4)   NULL
, [DeliveredQuantity]          DECIMAL(19,4)   NULL
, [InventoryQuantity]          DECIMAL(19,4)   NULL
, [InventoryQuantityOpen]      DECIMAL(19,4)   NULL
, [Value]                      DECIMAL(19,4)   NULL
, [Value_RCY]                  DECIMAL(19,4)   NULL
, [ValueMST]                   DECIMAL(19,4)   NULL
) ;

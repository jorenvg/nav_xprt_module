EXEC dbo.drop_object @object = N'fact.LoadSalesPacking' -- nvarchar(128)
                   , @type = N'P'                       -- nchar(2)
                   , @debug = 0 ;                       -- int
GO

CREATE PROCEDURE fact.LoadSalesPacking
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
-- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @table_name sysname = N'SalesPacking' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.SalesPacking ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO fact.SalesPacking (
      StageID
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , SalesPackingTransRecID
    , SalesPackingTransPartition
    , DocumentDateID
    , ShippingRequestedDateID
    , ShippingConfirmedDateID
    , ReceiptRequestedDateID
    , ReceiptConfirmedDateID
    , DeliveryDateID
    , WMSlocationID
    , LocationID
    , ProductID
    , TrackingID
    , SalesTakerID
    , SalesResponsibleID
    , CustomerAccountID
    , InvoiceAccountID
    , DeliveryAddressID
    , InvoiceAddressID
    , ItemID
    , CurrencyTransactionID
    , CurrencyReportID
    , FinancialDimensionsID
    , OrderTypeID
    , SalesOriginID
    , SalesPoolID
    , ModeOfDeliveryID
    , DeliveryTermsID
    , MethodOfPaymentID
    , PaymentTermsID
    , VoucherID
    , SalesOrderID
    , SalesGroupID
    , SalesUnitID
    , SalesPackingSlipID
    , OrderedQuantity
    , RemainingQuantity
    , DeliveredQuantity
    , InventoryQuantity
    , InventoryQuantityOpen
    , Value
    , Value_RCY
    , ValueMST
    )
    SELECT StageID
         , CompanyID
         , DataConnectionID
         , @component_execution_id
         , SalesPackingTransRecID
         , SalesPackingTransPartition
         , DocumentDateID
         , ShippingRequestedDateID
         , ShippingConfirmedDateID
         , ReceiptRequestedDateID
         , ReceiptConfirmedDateID
         , DeliveryDateID
         , WMSlocationID
         , LocationID
         , ProductID
         , TrackingID
         , SalesTakerID
         , SalesResponsibleID
         , CustomerAccountID
         , InvoiceAccountID
         , DeliveryAddressID
         , InvoiceAddressID
         , ItemID
         , CurrencyTransactionID
         , CurrencyReportID
         , FinancialDimensionsID
         , OrderTypeID
         , SalesOriginID
         , SalesPoolID
         , ModeOfDeliveryID
         , DeliveryTermsID
         , MethodOfPaymentID
         , PaymentTermsID
         , VoucherID
         , SalesOrderID
         , SalesGroupID
         , SalesUnitID
         , SalesPackingSlipID
         , OrderedQuantity
         , RemainingQuantity
         , DeliveredQuantity
         , InventoryQuantity
         , InventoryQuantityOpen
         , Value
         , Value_RCY
         , ValueMST
      FROM fact.SalesPackingView ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.SalesPacking
         SET      CompanyID = V.CompanyID
                , DataConnectionID = V.DataConnectionID
                , ComponentExecutionID = @component_execution_id
                , SalesPackingTransRecID = V.SalesPackingTransRecID
                , SalesPackingTransPartition = V.SalesPackingTransPartition
                , DocumentDateID = V.DocumentDateID
                , ShippingRequestedDateID = V.ShippingRequestedDateID
                , ShippingConfirmedDateID = V.ShippingConfirmedDateID
                , ReceiptRequestedDateID = V.ReceiptRequestedDateID
                , ReceiptConfirmedDateID = V.ReceiptConfirmedDateID
                , DeliveryDateID = V.DeliveryDateID
                , WMSlocationID = V.WMSlocationID
                , LocationID = V.LocationID
                , ProductID = V.ProductID
                , TrackingID = V.TrackingID
                , SalesTakerID = V.SalesTakerID
                , SalesResponsibleID = V.SalesResponsibleID
                , CustomerAccountID = V.CustomerAccountID
                , InvoiceAccountID = V.InvoiceAccountID
                , DeliveryAddressID = V.DeliveryAddressID
                , InvoiceAddressID = V.InvoiceAddressID
                , ItemID = V.ItemID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , FinancialDimensionsID = V.FinancialDimensionsID
                , OrderTypeID = V.OrderTypeID
                , SalesOriginID = V.SalesOriginID
                , SalesPoolID = V.SalesPoolID
                , ModeOfDeliveryID = V.ModeOfDeliveryID
                , DeliveryTermsID = V.DeliveryTermsID
                , MethodOfPaymentID = V.MethodOfPaymentID
                , PaymentTermsID = V.PaymentTermsID
                , VoucherID = V.VoucherID
                , SalesOrderID = V.SalesOrderID
                , SalesGroupID = V.SalesGroupID
                , SalesUnitID = V.SalesUnitID
                , SalesPackingSlipID = V.SalesPackingSlipID
                , OrderedQuantity = V.OrderedQuantity
                , RemainingQuantity = V.RemainingQuantity
                , DeliveredQuantity = V.DeliveredQuantity
                , InventoryQuantity = V.InventoryQuantity
                , InventoryQuantityOpen = V.InventoryQuantityOpen
                , Value = V.Value
                , ValueMST = V.ValueMST
        FROM      fact.SalesPacking     F
       INNER JOIN fact.SalesPackingView AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.SalesPacking (
        StageID
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , SalesPackingTransRecID
      , SalesPackingTransPartition
      , DocumentDateID
      , ShippingRequestedDateID
      , ShippingConfirmedDateID
      , ReceiptRequestedDateID
      , ReceiptConfirmedDateID
      , DeliveryDateID
      , WMSlocationID
      , LocationID
      , ProductID
      , TrackingID
      , SalesTakerID
      , SalesResponsibleID
      , CustomerAccountID
      , InvoiceAccountID
      , DeliveryAddressID
      , InvoiceAddressID
      , ItemID
      , CurrencyTransactionID
      , CurrencyReportID
      , FinancialDimensionsID
      , OrderTypeID
      , SalesOriginID
      , SalesPoolID
      , ModeOfDeliveryID
      , DeliveryTermsID
      , MethodOfPaymentID
      , PaymentTermsID
      , VoucherID
      , SalesOrderID
      , SalesGroupID
      , SalesUnitID
      , SalesPackingSlipID
      , OrderedQuantity
      , RemainingQuantity
      , DeliveredQuantity
      , InventoryQuantity
      , InventoryQuantityOpen
      , Value
      , Value_RCY
      , ValueMST
      )
      SELECT V.StageID
           , V.CompanyID
           , V.DataConnectionID
           , @component_execution_id
           , V.SalesPackingTransRecID
           , V.SalesPackingTransPartition
           , V.DocumentDateID
           , V.ShippingRequestedDateID
           , V.ShippingConfirmedDateID
           , V.ReceiptRequestedDateID
           , V.ReceiptConfirmedDateID
           , V.DeliveryDateID
           , V.WMSlocationID
           , V.LocationID
           , V.ProductID
           , V.TrackingID
           , V.SalesTakerID
           , V.SalesResponsibleID
           , V.CustomerAccountID
           , V.InvoiceAccountID
           , V.DeliveryAddressID
           , V.InvoiceAddressID
           , V.ItemID
           , V.CurrencyTransactionID
           , V.CurrencyReportID
           , V.FinancialDimensionsID
           , V.OrderTypeID
           , V.SalesOriginID
           , V.SalesPoolID
           , V.ModeOfDeliveryID
           , V.DeliveryTermsID
           , V.MethodOfPaymentID
           , V.PaymentTermsID
           , V.VoucherID
           , V.SalesOrderID
           , V.SalesGroupID
           , V.SalesUnitID
           , V.SalesPackingSlipID
           , V.OrderedQuantity
           , V.RemainingQuantity
           , V.DeliveredQuantity
           , InventoryQuantity
           , InventoryQuantityOpen
           , V.Value
           , V.Value_RCY
           , V.ValueMST
        FROM fact.SalesPackingView V
       WHERE V.ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.SalesPacking F WHERE F.StageID = V.StageID) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.SalesPacking
       WHERE EXISTS ( SELECT 1
                        FROM fact.SalesPackingView V
                       WHERE V.ExecutionFlag = 'D'
                         AND V.StageID       = [fact].[SalesPacking].StageID) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
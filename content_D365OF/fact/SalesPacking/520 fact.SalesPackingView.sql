/***
  Currently the CurrencyID is always 0, the reason is because the source field(CUSTPACKINGSLIPTRANS.CURRENCYCODE) is always ''.
  The question is if the Currency dimension is valid. An issue has been created: https://hillstar.atlassian.net/browse/DEV-786  
***/

EXEC dbo.drop_object @object = N'fact.SalesPackingView', @type = N'V' ;
GO
CREATE VIEW fact.SalesPackingView
AS
  SELECT            sp.StageId                            AS StageID
                  , sp.ExecutionFlag                      AS ExecutionFlag
                  , sp.SalesPackingTransRecId
                  , sp.SalesPackingTransPartition
                  , sp.CompanyID
                  , sp.DataConnectionID

                  --	 /***Dimensions*/
                  --	 --Dates
                  , COALESCE(cd.DateID, 0)                AS DocumentDateID
                  , COALESCE(rrd.DateID, 0)               AS ReceiptRequestedDateID
                  , COALESCE(srd.DateID, 0)               AS ShippingRequestedDateID
                  , COALESCE(rcd.DateID, 0)               AS ReceiptConfirmedDateID
                  , COALESCE(scd.DateID, 0)               AS ShippingConfirmedDateID
                  , COALESCE(dd.DateID, 0)                AS DeliveryDateID
                  -- inventdimension
                  , COALESCE(wl.WMSLocationID, 0)         AS WMSlocationID
                  , COALESCE(l.LocationID, 0)             AS LocationID
                  , COALESCE(p.ProductID, 0)              AS ProductID
                  , COALESCE(t.TrackingID, 0)             AS TrackingID
                  --	--employee
                  , COALESCE(sTkr.EmployeeID, 0)          AS SalesTakerID
                  , COALESCE(sres.EmployeeID, 0)          AS SalesResponsibleID
                  --	 --customer
                  , COALESCE(cAcc.CustomerID, 0)          AS CustomerAccountID
                  , COALESCE(iAcc.CustomerID, 0)          AS InvoiceAccountID
                  --	-- address
                  , COALESCE(dAdr.AddressID, 0)           AS DeliveryAddressID
                  , COALESCE(iAdr.AddressID, 0)           AS InvoiceAddressID
                  --	-- item
                  , COALESCE(i.ItemID, 0)                 AS ItemID
                  --	 -- currency
                  , COALESCE(cy.CurrencyID, 0)            AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)           AS CurrencyReportID
                  --	 -- financialdimension
                  , COALESCE(fd.FinancialDimensionsID, 0) AS FinancialDimensionsID
                  , COALESCE(sot.SalesOrderTypeID, 0)     AS OrderTypeID
                  , COALESCE(orign.SalesOriginID, 0)      AS SalesOriginID
                  , COALESCE(sp1.SalesPoolID, 0)          AS SalesPoolID
                  , COALESCE(modlv.ModeOfDeliveryID, 0)   AS ModeOfDeliveryID
                  , COALESCE(dt.DeliveryTermsID, 0)       AS DeliveryTermsID
                  , COALESCE(mop.MethodOfPaymentID, 0)    AS MethodOfPaymentID
                  , COALESCE(pt.PaymentTermsID, 0)        AS PaymentTermsID
                  , COALESCE(jv.VoucherID, 0)             AS VoucherID
                  , COALESCE(so.SalesOrderID, 0)          AS SalesOrderID
                  , COALESCE(sg.SalesGroupID, 0)          AS SalesGroupID
                  , COALESCE(uom.UnitOfMeasureID, 0)      AS SalesUnitID
                  , COALESCE(sps.SalesPackingSlipID, 0)   AS SalesPackingSlipID

                  /***Measures***/
                  , sp.OrderedQuantity                    AS OrderedQuantity
                  , sp.RemainingQuantity                  AS RemainingQuantity
                  , sp.DeliveredQuantity                  AS DeliveredQuantity
                  , sp.InventoryQuantity                  AS InventoryQuantity
                  , sp.InventoryQuantityOpen              AS InventoryQuantityOpen
                  , sp.Value                              AS Value
                  , sp.Value_RCY                          AS Value_RCY
                  , sp.ValueMST                           AS ValueMST


    --SELECT COUNT(*)  -- 
    FROM            delta.SalesPacking      AS sp
    LEFT JOIN       dim.Date                AS cd ON cd.Date                        = sp.DocumentDate
    LEFT JOIN       dim.Date                AS srd ON srd.Date                      = sp.ShippingRequestedDate
    LEFT JOIN       dim.Date                AS scd ON scd.Date                      = sp.ShippingConfirmedDate
    LEFT JOIN       dim.Date                AS rrd ON rrd.Date                      = sp.ReceiptRequestedDate
    LEFT JOIN       dim.Date                AS rcd ON rcd.Date                      = sp.ReceiptConfirmedDate
    LEFT JOIN       dim.Date                AS dd ON dd.Date                        = sp.DeliveryDate
    LEFT JOIN       dim.WMSLocation         AS wl ON wl.WMSLocationCode             = sp.WMSlocationCode
                                                 AND wl.LocationCode                = sp.LocationCode
                                                 AND wl.Partition                   = sp.SalesPackingTransPartition
                                                 AND wl.DataConnectionID            = sp.DataConnectionID
                                                 AND wl.CompanyID                   = sp.CompanyID
    LEFT JOIN       dim.Location            AS l ON l.LocationCode                  = sp.LocationCode
                                                AND l.Partition                     = sp.SalesPackingTransPartition
                                                AND l.DataConnectionID              = sp.DataConnectionID
                                                AND l.CompanyID                     = sp.CompanyID
    LEFT JOIN       dim.Product             AS p ON p.InventSize                    = sp.ProductInventSizeCode
                                                AND p.InventColor                   = sp.ProductColorCode
                                                AND p.InventStyle                   = sp.ProductStyleCode
                                                AND p.InventConfiguration           = sp.ProductConfigCode
                                                AND p.Partition                     = sp.SalesPackingTransPartition
                                                AND p.DataConnectionID              = sp.DataConnectionID
                                                AND p.CompanyID                     = sp.CompanyID
    LEFT JOIN       dim.Tracking            AS t ON t.InventBatchCode               = sp.TrackingBatchCode
                                                AND t.InventSerialCode              = sp.TrackingSerialCode
                                                AND t.Partition                     = sp.SalesPackingTransPartition
                                                AND t.DataConnectionID              = sp.DataConnectionID
                                                AND t.CompanyID                     = sp.CompanyID
    LEFT JOIN       dim.Customer            AS cAcc ON cAcc.CustomerAccountNo       = sp.CustomerAccountCode
                                                   AND cAcc.Partition               = sp.SalesPackingTransPartition
                                                   AND cAcc.DataConnectionID        = sp.DataConnectionID
                                                   AND cAcc.CompanyID               = sp.CompanyID
    LEFT JOIN       dim.Customer            AS iAcc ON iAcc.CustomerAccountNo       = sp.InvoiceAccountCode
                                                   AND iAcc.Partition               = sp.SalesPackingTransPartition
                                                   AND iAcc.DataConnectionID        = sp.DataConnectionID
                                                   AND iAcc.CompanyID               = sp.CompanyID
    LEFT JOIN       dim.Address             AS dAdr ON dAdr.AddressRecID            = sp.DeliveryAddressCode
                                                   AND dAdr.Partition               = sp.SalesPackingTransPartition
                                                   AND dAdr.DataConnectionID        = sp.DataConnectionID
    LEFT JOIN       dim.Address             AS iAdr ON iAdr.AddressRecID            = sp.DeliveryAddressCode
                                                   AND iAdr.Partition               = sp.SalesPackingTransPartition
                                                   AND iAdr.DataConnectionID        = sp.DataConnectionID
    LEFT JOIN       dim.Item                AS i ON i.ItemNumber                    = sp.ItemCode
                                                AND i.Partition                     = sp.SalesPackingTransPartition
                                                AND i.DataConnectionID              = sp.DataConnectionID
                                                AND i.CompanyID                     = sp.CompanyID
    LEFT JOIN       dim.FinancialDimensions AS fd ON fd.DefaultDimension            = sp.FinancialDimensionCode
                                                 AND fd.Partition                   = sp.SalesPackingTransPartition
                                                 AND fd.DataConnectionID            = sp.DataConnectionID
    LEFT JOIN       dim.ModeOfDelivery      AS modlv ON modlv.ModeOfDeliveryCode    = sp.DeliveryModeCode
                                                    AND modlv.Partition             = sp.SalesPackingTransPartition
                                                    AND modlv.DataConnectionID      = sp.DataConnectionID
                                                    AND modlv.CompanyID             = sp.CompanyID
    LEFT JOIN       dim.DeliveryTerms       AS dt ON dt.DeliveryTermsCode           = sp.DeliveryTermsCode
                                                 AND dt.Partition                   = sp.SalesPackingTransPartition
                                                 AND dt.DataConnectionID            = sp.DataConnectionID
                                                 AND dt.CompanyID                   = sp.CompanyID
    LEFT JOIN       dim.SalesOrder          AS so ON so.SalesOrder                  = sp.SalesCode
                                                 AND so.Partition                   = sp.SalesPackingTransPartition
                                                 AND so.DataConnectionID            = sp.DataConnectionID
                                                 AND so.CompanyID                   = sp.CompanyID
    LEFT JOIN       dim.SalesOrderType      AS sot ON sot.SalesOrderTypeCode        = sp.OrderTypeCode
                                                  AND sot.DataConnectionID          = sp.DataConnectionID
    LEFT JOIN       dim.SalesGroup          AS sg ON sg.SalesGroupCode              = sp.SalesGroupCode
                                                 AND sg.Partition                   = sp.SalesPackingTransPartition
                                                 AND sg.DataConnectionID            = sp.DataConnectionID
                                                 AND sg.CompanyID                   = sp.CompanyID
    LEFT JOIN       dim.SalesPackingSlip    AS sps ON sps.SalesPackingSlip          = sp.SalesPackingSlip
                                                  AND sps.CompanyID                 = sp.CompanyID
                                                  AND sps.Partition                 = sp.SalesPackingTransPartition
                                                  AND sps.DataConnectionID          = sp.DataConnectionID
    LEFT JOIN       dim.Employee            AS sTkr ON sTkr.EmployeeRecID           = sp.SalesTakerCode
                                                   AND sTkr.Partition               = sp.SalesPackingTransPartition
                                                   AND sTkr.DataConnectionID        = sp.DataConnectionID
    LEFT JOIN       dim.Employee            AS sres ON sres.EmployeeRecID           = sp.SalesResponsibleCode
                                                   AND sres.Partition               = sp.SalesPackingTransPartition
                                                   AND sres.DataConnectionID        = sp.DataConnectionID
    LEFT JOIN       dim.Currency            AS cy ON cy.Code                        = sp.CurrencyCode
                                                 AND cy.Partition                   = sp.SalesPackingTransPartition
                                                 AND cy.DataConnectionID            = sp.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport      AS rcy ON sp.SalesPackingTransPartition = rcy.Partition
    LEFT JOIN       dim.UnitOfMeasure       AS uom ON uom.UnitOfMeasureCode         = sp.SalesUnitCode
                                                  AND uom.Partition                 = sp.SalesPackingTransPartition
                                                  AND uom.DataConnectionID          = sp.DataConnectionID
    LEFT JOIN       dim.Voucher             AS jv ON jv.Voucher                     = sp.VoucherCode
                                                 AND jv.CompanyID                   = sp.CompanyID
                                                 AND jv.Partition                   = sp.SalesPackingTransPartition
                                                 AND jv.DataConnectionID            = sp.DataConnectionID
    LEFT JOIN       dim.SalesOrigin         AS orign ON orign.SalesOriginCode       = sp.SalesOriginCode
                                                    AND orign.DataConnectionID      = sp.DataConnectionID
                                                    AND orign.CompanyID             = sp.CompanyID
    LEFT JOIN       dim.SalesPool           AS sp1 ON sp1.SalesPoolCode             = sp.SalesPoolCode
                                                  AND sp1.Partition                 = sp.SalesPackingTransPartition
                                                  AND sp1.DataConnectionID          = sp.DataConnectionID
                                                  AND sp1.CompanyID                 = sp.CompanyID
    LEFT JOIN       dim.MethodOfPayment     AS mop ON mop.MethodOfPaymentCode       = sp.PaymentModeCode
                                                  AND mop.Partition                 = sp.SalesPackingTransPartition
                                                  AND mop.DataConnectionID          = sp.DataConnectionID
                                                  AND mop.CompanyID                 = sp.CompanyID
    LEFT JOIN       dim.PaymentTerms        AS pt ON pt.PaymentTermsCode            = sp.PaymentTermsCode
                                                 AND pt.Partition                   = sp.SalesPackingTransPartition
                                                 AND pt.DataConnectionID            = sp.DataConnectionID
                                                 AND pt.CompanyID                   = sp.CompanyID ;
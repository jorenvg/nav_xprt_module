EXEC dbo.drop_object 'delta.SalesPacking', 'T' ;
GO
CREATE TABLE [delta].[SalesPacking]
(
  StageId                    [BIGINT]          NOT NULL
, ExecutionFlag              [NCHAR](1)        NOT NULL
, ComponentExecutionId       [INT]             NOT NULL
, SalesPackingTransRecId     [BIGINT]          NULL
, SalesPackingTransPartition [BIGINT]          NULL
, [CompanyID]                [INT]             NOT NULL
, [DataConnectionID]         [INT]             NOT NULL
, [DocumentDate]             [DATE]            NULL
, ReceiptRequestedDate       [DATE]            NULL
, ShippingRequestedDate      [DATE]            NULL
, ShippingConfirmedDate      [DATE]            NULL
, ReceiptConfirmedDate       [DATE]            NULL
, [DeliveryDate]             [DATETIME]        NULL
, [WMSlocationCode]          [NVARCHAR](100)   NULL
, [LocationCode]             [NVARCHAR](100)   NULL
, [ProductConfigCode]        [NVARCHAR](100)   NULL
, [ProductColorCode]         [NVARCHAR](100)   NULL
, [ProductInventSizeCode]    [NVARCHAR](100)   NULL
, [ProductStyleCode]         [NVARCHAR](100)   NULL
, [TrackingBatchCode]        [NVARCHAR](100)   NULL
, [TrackingSerialCode]       [NVARCHAR](100)   NULL
, [SalesTakerCode]           [BIGINT]          NULL
, [SalesResponsibleCode]     [BIGINT]          NULL
, [CustomerAccountCode]      [NVARCHAR](100)   NULL
, [InvoiceAccountCode]       [NVARCHAR](100)   NULL
, [DeliveryAddressCode]      [BIGINT]          NULL
, [InvoiceAddressCode]       [BIGINT]          NULL
, [CompanyCode]              [NVARCHAR](10)    NULL
, [ItemCode]                 [NVARCHAR](100)   NULL
, [CurrencyCode]             [NVARCHAR](10)    NULL
, [FinancialDimensionCode]   [BIGINT]          NULL
, [OrderTypeCode]            [INT]             NULL
, [SalesOriginCode]          [NVARCHAR](100)   NULL
, [SalesPoolCode]            [NVARCHAR](100)   NULL
, [DeliveryModeCode]         [NVARCHAR](100)   NULL
, [DeliveryTermsCode]        [NVARCHAR](100)   NULL
, [PaymentModeCode]          [NVARCHAR](100)   NULL
, [PaymentTermsCode]         [NVARCHAR](100)   NULL
, [SalesCode]                [NVARCHAR](100)   NULL
, [SalesGroupCode]           [NVARCHAR](100)   NULL
, [SalesPackingSlip]         [NVARCHAR](100)   NULL
, [SalesUnitCode]            [NVARCHAR](100)   NULL
, [VoucherCode]              [NVARCHAR](100)   NULL
, [OrderedQuantity]          DECIMAL(19,4)     NULL
, [RemainingQuantity]        DECIMAL(19,4)     NULL
, [DeliveredQuantity]        DECIMAL(19,4)     NULL
, [InventoryQuantity]        DECIMAL(19,4)     NULL
, [InventoryQuantityOpen]    DECIMAL(19,4)     NULL
, [Value]                    DECIMAL(19,4)     NULL
, [Value_RCY]                DECIMAL(19,4)     NULL
, [ValueMST]                 DECIMAL(19,4)     NULL
) ;

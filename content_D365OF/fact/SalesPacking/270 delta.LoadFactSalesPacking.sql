/****************************************************************************************************
    Functionality:  Loads the delta for SalesPacking, either incrementally or with a full load based on the @load_type.
			Returns the @inserted, @updated, @deleted records that have been processed on the delta table.
    Created by:          Date:           	
    Date            Changed by      Ticket/Change       Description

*****************************************************************************************************/
EXEC dbo.Drop_Object @object = N'delta.LoadFactSalesPacking' -- nvarchar(128)
                   , @type = N'P'                            -- nchar(2)
                   , @debug = 0 ;                            -- int
GO

CREATE PROCEDURE delta.LoadFactSalesPacking
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @execution_flag NCHAR(1) = N'' ;
  DECLARE @table_name sysname = N'SalesPacking' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  DECLARE @last_processed_timestamp BINARY(8) = (SELECT meta.get_last_processed_timestamp(@fact_table_identifier)) ;

  --truncate delta table
  EXECUTE dbo.truncate_table @delta_table_identifier ;

  IF @load_type = 1 --incremental
  BEGIN
    /****************************************************************************************************
  Functionality:  Incrementally inserts all new records for the SalesPacking

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'N' ;

    INSERT INTO delta.SalesPacking (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , SalesPackingTransRecId
    , SalesPackingTransPartition
    , CompanyID
    , DataConnectionID
    , DocumentDate
    , ReceiptRequestedDate
    , ShippingRequestedDate
    , ShippingConfirmedDate
    , ReceiptConfirmedDate
    , DeliveryDate
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , SalesTakerCode
    , SalesResponsibleCode
    , CustomerAccountCode
    , InvoiceAccountCode
    , DeliveryAddressCode
    , InvoiceAddressCode
    , CompanyCode
    , ItemCode
    , CurrencyCode
    , FinancialDimensionCode
    , OrderTypeCode
    , SalesOriginCode
    , SalesPoolCode
    , DeliveryModeCode
    , DeliveryTermsCode
    , PaymentModeCode
    , PaymentTermsCode
    , SalesCode
    , SalesGroupCode
    , SalesPackingSlip
    , SalesUnitCode
    , VoucherCode
    , OrderedQuantity
    , RemainingQuantity
    , DeliveredQuantity
    , InventoryQuantity
    , InventoryQuantityOpen
    , Value
    , Value_RCY
    , ValueMST
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , SalesPackingTransRecId
         , SalesPackingTransPartition
         , CompanyID
         , DataConnectionID
         , DocumentDate
         , ReceiptRequestedDate
         , ShippingRequestedDate
         , ShippingConfirmedDate
         , ReceiptConfirmedDate
         , DeliveryDate
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , SalesTakerCode
         , SalesResponsibleCode
         , CustomerAccountCode
         , InvoiceAccountCode
         , DeliveryAddressCode
         , InvoiceAddressCode
         , CompanyCode
         , ItemCode
         , CurrencyCode
         , FinancialDimensionCode
         , OrderTypeCode
         , SalesOriginCode
         , SalesPoolCode
         , DeliveryModeCode
         , DeliveryTermsCode
         , PaymentModeCode
         , PaymentTermsCode
         , SalesCode
         , SalesGroupCode
         , SalesPackingSlip
         , SalesUnitCode
         , VoucherCode
         , OrderedQuantity
         , RemainingQuantity
         , DeliveredQuantity
         , InventoryQuantity
         , InventoryQuantityOpen
         , Value
         , Value_RCY
         , ValueMST
      FROM Delta.SalesPackingFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @inserted = @@RowCount ;

    /****************************************************************************************************
  Functionality:  Incrementally inserts all updated records for the SalesPacking

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'U' ;

    INSERT INTO delta.SalesPacking (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , SalesPackingTransRecId
    , SalesPackingTransPartition
    , CompanyID
    , DataConnectionID
    , DocumentDate
    , ReceiptRequestedDate
    , ShippingRequestedDate
    , ShippingConfirmedDate
    , ReceiptConfirmedDate
    , DeliveryDate
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , SalesTakerCode
    , SalesResponsibleCode
    , CustomerAccountCode
    , InvoiceAccountCode
    , DeliveryAddressCode
    , InvoiceAddressCode
    , CompanyCode
    , ItemCode
    , CurrencyCode
    , FinancialDimensionCode
    , OrderTypeCode
    , SalesOriginCode
    , SalesPoolCode
    , DeliveryModeCode
    , DeliveryTermsCode
    , PaymentModeCode
    , PaymentTermsCode
    , SalesCode
    , SalesGroupCode
    , SalesPackingSlip
    , SalesUnitCode
    , VoucherCode
    , OrderedQuantity
    , RemainingQuantity
    , DeliveredQuantity
    , InventoryQuantity
    , InventoryQuantityOpen
    , Value
    , Value_RCY
    , ValueMST
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , SalesPackingTransRecId
         , SalesPackingTransPartition
         , CompanyID
         , DataConnectionID
         , DocumentDate
         , ReceiptRequestedDate
         , ShippingRequestedDate
         , ShippingConfirmedDate
         , ReceiptConfirmedDate
         , DeliveryDate
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , SalesTakerCode
         , SalesResponsibleCode
         , CustomerAccountCode
         , InvoiceAccountCode
         , DeliveryAddressCode
         , InvoiceAddressCode
         , CompanyCode
         , ItemCode
         , CurrencyCode
         , FinancialDimensionCode
         , OrderTypeCode
         , SalesOriginCode
         , SalesPoolCode
         , DeliveryModeCode
         , DeliveryTermsCode
         , PaymentModeCode
         , PaymentTermsCode
         , SalesCode
         , SalesGroupCode
         , SalesPackingSlip
         , SalesUnitCode
         , VoucherCode
         , OrderedQuantity
         , RemainingQuantity
         , DeliveredQuantity
         , InventoryQuantity
         , InventoryQuantityOpen
         , Value
         , Value_RCY
         , ValueMST
      FROM Delta.SalesPackingFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @inserted = @@RowCount ;

    /****************************************************************************************************
  Functionality:  Incrementally inserts all deleted records for SalesPacking

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'D' ;

    INSERT INTO delta.SalesPacking (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , SalesPackingTransRecId
    , SalesPackingTransPartition
    , CompanyID
    , DataConnectionID
    , DocumentDate
    , ReceiptRequestedDate
    , ShippingRequestedDate
    , ShippingConfirmedDate
    , ReceiptConfirmedDate
    , DeliveryDate
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , SalesTakerCode
    , SalesResponsibleCode
    , CustomerAccountCode
    , InvoiceAccountCode
    , DeliveryAddressCode
    , InvoiceAddressCode
    , CompanyCode
    , ItemCode
    , CurrencyCode
    , FinancialDimensionCode
    , OrderTypeCode
    , SalesOriginCode
    , SalesPoolCode
    , DeliveryModeCode
    , DeliveryTermsCode
    , PaymentModeCode
    , PaymentTermsCode
    , SalesCode
    , SalesGroupCode
    , SalesPackingSlip
    , SalesUnitCode
    , VoucherCode
    , OrderedQuantity
    , RemainingQuantity
    , DeliveredQuantity
    , InventoryQuantity
    , InventoryQuantityOpen
    , Value
    , Value_RCY
    , ValueMST
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , SalesPackingTransRecId
         , SalesPackingTransPartition
         , CompanyID
         , DataConnectionID
         , DocumentDate
         , ReceiptRequestedDate
         , ShippingRequestedDate
         , ShippingConfirmedDate
         , ReceiptConfirmedDate
         , DeliveryDate
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , SalesTakerCode
         , SalesResponsibleCode
         , CustomerAccountCode
         , InvoiceAccountCode
         , DeliveryAddressCode
         , InvoiceAddressCode
         , CompanyCode
         , ItemCode
         , CurrencyCode
         , FinancialDimensionCode
         , OrderTypeCode
         , SalesOriginCode
         , SalesPoolCode
         , DeliveryModeCode
         , DeliveryTermsCode
         , PaymentModeCode
         , PaymentTermsCode
         , SalesCode
         , SalesGroupCode
         , SalesPackingSlip
         , SalesUnitCode
         , VoucherCode
         , OrderedQuantity
         , RemainingQuantity
         , DeliveredQuantity
         , InventoryQuantity
         , InventoryQuantityOpen
         , Value
         , Value_RCY
         , ValueMST
      FROM Delta.SalesPackingFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @inserted = @@RowCount ;
  END ;
  /****************************************************************************************************
    Functionality:  During a full load with insert all records that have not been deleted. 

    Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/

  IF @load_type = 0 --full load
  BEGIN
    SET @deleted = 0 ;
    SET @updated = 0 ;
    SET @execution_flag = N'N' ;

    INSERT INTO delta.SalesPacking (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , SalesPackingTransRecId
    , SalesPackingTransPartition
    , CompanyID
    , DataConnectionID
    , DocumentDate
    , ReceiptRequestedDate
    , ShippingRequestedDate
    , ShippingConfirmedDate
    , ReceiptConfirmedDate
    , DeliveryDate
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , SalesTakerCode
    , SalesResponsibleCode
    , CustomerAccountCode
    , InvoiceAccountCode
    , DeliveryAddressCode
    , InvoiceAddressCode
    , CompanyCode
    , ItemCode
    , CurrencyCode
    , FinancialDimensionCode
    , OrderTypeCode
    , SalesOriginCode
    , SalesPoolCode
    , DeliveryModeCode
    , DeliveryTermsCode
    , PaymentModeCode
    , PaymentTermsCode
    , SalesCode
    , SalesGroupCode
    , SalesPackingSlip
    , SalesUnitCode
    , VoucherCode
    , OrderedQuantity
    , RemainingQuantity
    , DeliveredQuantity
    , InventoryQuantity
    , InventoryQuantityOpen
    , Value
    , Value_RCY
    , ValueMST
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , SalesPackingTransRecId
         , SalesPackingTransPartition
         , CompanyID
         , DataConnectionID
         , DocumentDate
         , ReceiptRequestedDate
         , ShippingRequestedDate
         , ShippingConfirmedDate
         , ReceiptConfirmedDate
         , DeliveryDate
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , SalesTakerCode
         , SalesResponsibleCode
         , CustomerAccountCode
         , InvoiceAccountCode
         , DeliveryAddressCode
         , InvoiceAddressCode
         , CompanyCode
         , ItemCode
         , CurrencyCode
         , FinancialDimensionCode
         , OrderTypeCode
         , SalesOriginCode
         , SalesPoolCode
         , DeliveryModeCode
         , DeliveryTermsCode
         , PaymentModeCode
         , PaymentTermsCode
         , SalesCode
         , SalesGroupCode
         , SalesPackingSlip
         , SalesUnitCode
         , VoucherCode
         , OrderedQuantity
         , RemainingQuantity
         , DeliveredQuantity
         , InventoryQuantity
         , InventoryQuantityOpen
         , Value
         , Value_RCY
         , ValueMST
      FROM Delta.SalesPackingFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @inserted = @@RowCount ;
  END ;
END ;

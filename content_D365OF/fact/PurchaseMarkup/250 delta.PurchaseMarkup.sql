EXEC dbo.drop_object 'delta.PurchaseMarkup', 'T' ;
GO
CREATE TABLE [delta].[PurchaseMarkup]
(
  StageId                      BIGINT            NOT NULL
, ExecutionFlag                NCHAR(1)          NOT NULL
, CompanyID                    [INT]             NOT NULL
, DataConnectionID             [INT]             NOT NULL
, ComponentExecutionId         INT               NOT NULL
, PurchaseMarkupRecID          BIGINT            NULL
, PurchaseMarkupPartition      BIGINT            NULL
, [OriginTableCode]            [INT]             NULL
, [OriginTable]                [NVARCHAR](128)   NULL
, [OriginTransRecID]           [BIGINT]          NULL
, [PurchaseOrderCode]          [NVARCHAR](30)    NULL
, [PurchaseInvoice]            [NVARCHAR](30)    NULL
, [InvoiceDate]                [DATETIME]        NULL
, [NumberSequenceGroup]        [NVARCHAR](10)    NULL
, [InternalInvoiceCode]        [NVARCHAR](20)    NULL
, [PurchasePackingSlipCode]    [NVARCHAR](50)    NULL
, [TransactionDate]            [DATETIME]        NULL
, DocumentDate                 [DATETIME]        NULL
, DeliveryDate                 [DATETIME]        NULL
, DeliveryAddressCode          BIGINT            NULL
, VendorAccountCode            [NVARCHAR](30)    NULL
, InvoiceAccountCode           [NVARCHAR](30)    NULL
, PurchasePlacerCode           BIGINT            NULL
, PurchaseRequesterCode        BIGINT            NULL
, VoucherCode                  [NVARCHAR](30)    NULL
, PurchaseOrderTypeCode        [NVARCHAR](30)    NULL
, DeliveryTermCode             [NVARCHAR](30)    NULL
, DeliveryModeCode             [NVARCHAR](30)    NULL
, PaymentTermCode              [NVARCHAR](30)    NULL
, PaymentModeCode              [NVARCHAR](30)    NULL
, PurchasePoolCode             [NVARCHAR](30)    NULL
, [CurrencyCode]               [NVARCHAR](10)    NULL
, [MarkupCode]                 [NVARCHAR](30)    NULL
, [ModuleType]                 [INT]             NULL
, [TaxGroupCode]               [NVARCHAR](30)    NULL
, [TaxItemGroupCode]           [NVARCHAR](30)    NULL
, [MarkUpCalculatedAmount]     DECIMAL(19,4)     NULL
, [MarkUpCalculatedAmount_RCY] DECIMAL(19,4)     NULL
, [MarkUpPostedAmount]         DECIMAL(19,4)     NULL
, [MarkUpPostedAmount_RCY]     DECIMAL(19,4)     NULL
, [MarkupTaxAmount]            DECIMAL(19,4)     NULL
, [MarkupTaxAmount_RCY]        DECIMAL(19,4)     NULL
) ;

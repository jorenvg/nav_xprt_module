EXEC dbo.drop_object 'delta.PurchaseMarkupFunction', 'F' ;
GO
CREATE FUNCTION delta.PurchaseMarkupFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = -1
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
              , markup_transactions AS (
             SELECT      mt.stage_id
                       , mt.component_execution_id
                       , mt.execution_timestamp
                       , mt.data_connection_id
                       , mt.execution_flag
                       , mt.company_id
                       , mt.TRANSRECID
                       , mt.MARKUPCODE
                       , mt.CURRENCYCODE
                       , mt.VALUE
                       , mt.CALCULATEDAMOUNT
                       , mt.TAXAMOUNT
                       , mt.POSTED
                       , mt.TRANSDATE
                       , mt.TAXGROUP
                       , mt.TAXITEMGROUP
                       , mt.MARKUPCATEGORY
                       , mt.MODULETYPE
                       , mt.PARTITION
                       , mt.RECID
                       , SQLD.TABLEID
                       , SQLD.SQLNAME
                       , CASE
                           WHEN SQLD.SQLNAME = 'PurchLine' THEN pl.PURCHID
                           WHEN SQLD.SQLNAME = 'PurchTable' THEN pt.PURCHID
                           WHEN SQLD.SQLNAME = 'VENDPackingSlipTrans' THEN vpst.ORIGPURCHID
                           WHEN SQLD.SQLNAME = 'VENDPackingSlipJour' THEN vpsj.PURCHID
                           WHEN SQLD.SQLNAME = 'VENDInvoiceTrans' THEN vit.PURCHID
                           WHEN SQLD.SQLNAME = 'VENDInvoiceJour' THEN vij.INVOICEID
                           ELSE NULL
                         END AS PurchaseOrderCode
                       , CASE
                           WHEN SQLD.SQLNAME = 'VENDInvoiceTrans' THEN vit.INVOICEID
                           WHEN SQLD.SQLNAME = 'VENDInvoiceJour' THEN vij.INVOICEID
                           ELSE NULL
                         END AS PurchaseInvoice
                       , CASE
                           WHEN SQLD.SQLNAME = 'VENDPackingSlipTrans' THEN vpst.PACKINGSLIPID
                           WHEN SQLD.SQLNAME = 'VENDPackingSlipJour' THEN vpsj.PACKINGSLIPID
                           ELSE NULL
                         END AS PurchasePackingSlipCode
               FROM      stage_ax.MARKUPTRANS          AS mt
               JOIN      stage_ax.SQLDICTIONARY        AS SQLD ON mt.TRANSTABLEID         = SQLD.TABLEID
                                                              AND SQLD.FIELDID            = 0
                                                              AND mt.data_connection_id   = SQLD.data_connection_id
               LEFT JOIN stage_ax.VENDPACKINGSLIPTRANS AS vpst ON vpst.RECID              = mt.TRANSRECID
                                                              AND vpst.PARTITION          = mt.PARTITION
                                                              AND vpst.data_connection_id = mt.data_connection_id
                                                              AND vpst.company_id         = mt.company_id
               LEFT JOIN stage_ax.VENDPACKINGSLIPJOUR  AS vpsj ON vpsj.RECID              = mt.TRANSRECID
                                                              AND vpsj.PARTITION          = mt.PARTITION
                                                              AND vpsj.data_connection_id = mt.data_connection_id
                                                              AND vpsj.company_id         = mt.company_id
               LEFT JOIN stage_ax.VENDINVOICETRANS     AS vit ON vit.RECID                = mt.TRANSRECID
                                                             AND vit.PARTITION            = mt.PARTITION
                                                             AND vit.data_connection_id   = mt.data_connection_id
                                                             AND vit.company_id           = mt.company_id
               LEFT JOIN stage_ax.VENDINVOICEJOUR      AS vij ON vij.RECID                = mt.TRANSRECID
                                                             AND vij.PARTITION            = mt.PARTITION
                                                             AND vij.data_connection_id   = mt.data_connection_id
                                                             AND vij.company_id           = mt.company_id
               LEFT JOIN stage_ax.PURCHTABLE           AS pt ON pt.RECID                  = mt.TRANSRECID
                                                            AND pt.PARTITION              = mt.PARTITION
                                                            AND pt.data_connection_id     = mt.data_connection_id
                                                            AND pt.company_id             = mt.company_id
               LEFT JOIN stage_ax.PURCHLINE            AS pl ON pl.RECID                  = mt.TRANSRECID
                                                            AND pl.PARTITION              = mt.PARTITION
                                                            AND pl.data_connection_id     = mt.data_connection_id
                                                            AND pl.company_id             = mt.company_id
              WHERE      SQLD.SQLNAME IN ( 'PurchLine', 'PurchTable', 'VENDPackingSlipTrans', 'VENDPackingSlipJour', 'VENDInvoiceTrans', 'VENDInvoiceJour' )
           )
           SELECT      mt.stage_id                                     AS StageId
                     , @execution_flag                                 AS ExecutionFlag
                     , mt.company_id                                   AS CompanyID
                     , mt.data_connection_id                           AS DataConnectionID
                     , @component_execution_id                         AS ComponentExecutionId
                     , mt.RECID                                        AS PurchaseMarkupRecID
                     , mt.PARTITION                                    AS PurchaseMarkupPartition
                     , mt.TABLEID                                      AS OriginTableCode
                     , mt.SQLNAME                                      AS OriginTable
                     , mt.TRANSRECID                                   AS OriginTransRecID
                     , mt.PurchaseOrderCode                            AS PurchaseOrderCode
                     , mt.PurchaseInvoice                              AS PurchaseInvoice
                     , VIJ.INVOICEDATE                                 AS InvoiceDate
                     , VIJ.NUMBERSEQUENCEGROUP                         AS NumberSequenceGroup
                     , VIJ.INTERNALINVOICEID                           AS InternalInvoiceCode
                     , mt.PurchasePackingSlipCode                      AS PurchasePackingSlipCode
                     , CAST(mt.TRANSDATE AS DATE)                      AS TransactionDate
                     , CAST(VIJ.DOCUMENTDATE AS DATE)                  AS DocumentDate
                     , VPSJ.DELIVERYDATE                               AS DeliveryDate
                     , VPSJ.DELIVERYPOSTALADDRESS                      AS DeliveryAddressCode
                     , COALESCE(VIJ.ORDERACCOUNT, PT.ORDERACCOUNT)     AS VendorAccountCode
                     , COALESCE(VIJ.INVOICEACCOUNT, PT.INVOICEACCOUNT) AS InvoiceAccountCode
                     , PT.WORKERPURCHPLACER                            AS PurchasePlacerCode
                     , PT.REQUESTER                                    AS PurchaseRequesterCode
                     , VIJ.LEDGERVOUCHER                               AS VoucherCode
                     , COALESCE(VIJ.PURCHASETYPE, VPSJ.PURCHASETYPE)   AS PurchaseOrderTypeCode
                     , COALESCE(PT.DLVTERM, VIJ.DLVTERM)               AS DeliveryTermCode
                     , COALESCE(PT.DLVMODE, VIJ.DLVMODE)               AS DeliveryModeCode
                     , PT.PAYMENT                                      AS PaymentTermCode
                     , PT.PAYMMODE                                     AS PaymentModeCode
                     , PT.PURCHPOOLID                                  AS PurchasePoolCode
                     , mt.CURRENCYCODE                                 AS CurrencyCode
                     , mt.MARKUPCODE                                   AS MarkupCode
                     , mt.MODULETYPE                                   AS ModuleType
                     , mt.TAXGROUP                                     AS TaxGroupCode
                     , mt.TAXITEMGROUP                                 AS TaxItemGroupCode
                     , mt.CALCULATEDAMOUNT                             AS MarkUpCalculatedAmount
                     , mt.CALCULATEDAMOUNT * ex.CrossRate              AS MarkUpCalculatedAmount_RCY
                     , mt.POSTED                                       AS MarkUpPostedAmount
                     , mt.POSTED * ex.CrossRate                        AS MarkUpPostedAmount_RCY
                     , mt.TAXAMOUNT                                    AS MarkupTaxAmount
                     , mt.TAXAMOUNT * ex.CrossRate                     AS MarkupTaxAmount_RCY
             FROM      markup_transactions          AS mt
             LEFT JOIN stage_ax.PURCHTABLE          AS PT ON PT.PURCHID              = mt.PurchaseOrderCode
                                                         AND mt.data_connection_id   = PT.data_connection_id
                                                         AND mt.company_id           = PT.company_id
                                                         AND mt.PARTITION            = PT.PARTITION
             LEFT JOIN stage_ax.VENDINVOICEJOUR     AS VIJ ON VIJ.INVOICEID          = mt.PurchaseInvoice
                                                          AND mt.data_connection_id  = VIJ.data_connection_id
                                                          AND mt.company_id          = VIJ.company_id
                                                          AND mt.PARTITION           = VIJ.PARTITION
             LEFT JOIN stage_ax.VENDPACKINGSLIPJOUR AS VPSJ ON VPSJ.PACKINGSLIPID    = mt.PurchasePackingSlipCode
                                                           AND mt.data_connection_id = VPSJ.data_connection_id
                                                           AND mt.company_id         = VPSJ.company_id
                                                           AND mt.PARTITION          = VPSJ.PARTITION
             LEFT JOIN reporting_exchange_rates     AS ex ON ex.FromCurrencyCode     = mt.CURRENCYCODE
                                                         AND ex.DataConnectionID     = mt.data_connection_id
                                                         AND mt.TRANSDATE BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE
             /** FULL LOAD **/
                       ( @load_type         = 0
                     AND mt.execution_flag  <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                       ( @load_type                 = 1
                     AND @execution_flag            = 'N'
                     AND mt.execution_flag          = @execution_flag
                     AND mt.execution_timestamp     > @last_processed_timestamp)
               OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( MT.execution_flag           = @execution_flag
                             AND MT.execution_timestamp      > @last_processed_timestamp)
                            OR ( PT.execution_flag            = @execution_flag
                             AND PT.execution_timestamp       > @last_processed_timestamp)
                            OR ( VIJ.execution_flag          = @execution_flag
                             AND VIJ.execution_timestamp     > @last_processed_timestamp)
                            OR ( VPSJ.execution_flag           = @execution_flag
                             AND VPSJ.execution_timestamp      > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                       ( @load_type                 = 1
                     AND @execution_flag            = 'D'
                     AND mt.execution_flag          = @execution_flag
                     AND mt.execution_timestamp     > @last_processed_timestamp)) ;
GO


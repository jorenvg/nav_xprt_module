-- ################################################################################ --
-- #####                     Table: help.ExchangeRates	                          ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'fact.ExchangeRates', 'T' ;

CREATE TABLE fact.ExchangeRates
(
  -- System
  DataConnectionID     INT             NOT NULL
, ExecutionTimestamp   ROWVERSION      NOT NULL
, ComponentExecutionID INT             NOT NULL
-- Dimensions
, FromCurrencyId       INT             NOT NULL
, ToCurrencyId         INT             NOT NULL
, DateId               INT             NOT NULL
-- Other Fields
, Partition            BIGINT          NOT NULL
, RecId                BIGINT          NOT NULL
-- Measures
, ExchangeRate         DECIMAL(19, 4)  NULL
) ;

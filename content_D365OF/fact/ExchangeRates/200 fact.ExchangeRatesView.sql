EXEC dbo.drop_object @object = N'fact.ExchangeRatesView' -- nvarchar(128)
                   , @type = N'V'                        -- nchar(2)
                   , @debug = 0 ;

GO

CREATE VIEW fact.ExchangeRatesView
AS
  SELECT      CrossRate
            , COALESCE(from_c.CurrencyID, 0) AS FromCurrencyID
            , COALESCE(to_c.CurrencyID, 0)   AS ToCurrencyID
            , COALESCE(d.DateID, 0)          AS DateID
            , er.Partition
            , er.RecId
            , er.DataConnectionID
    FROM      help.ExchangeRates AS er
    LEFT JOIN dim.Currency       AS from_c ON from_c.Code = er.FromCurrencyCode
    LEFT JOIN dim.Currency       AS to_c ON to_c.Code = er.ToCurrencyCode
   CROSS JOIN dim.Date           d
   WHERE      ( (d.Date <= er.VALIDTO)
            AND (d.Date >= er.VALIDFROM)) ;



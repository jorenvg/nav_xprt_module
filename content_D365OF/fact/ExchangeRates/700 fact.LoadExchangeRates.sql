
/************************************************************************************
*** ExchangeRates Load procedure, 
*** Supports FULL_LOAD only because the base is a help table that supports full load only
*************************************************************************************/
EXEC dbo.drop_object @object = N'fact.LoadExchangeRates' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE fact.LoadExchangeRates
  @component_execution_id INT     = -1
, @load_type              TINYINT = 0
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  IF @load_type = 1
  BEGIN
    DECLARE @proc sysname = dbo.get_proc_full_name(@@PROCID) ;
    EXEC [audit].[log_warning_unsupported_load_type] @procedure = @proc
                                                   , @load_type = @load_type
                                                   , @component_execution_id = @component_execution_id ;
  END ;

  SELECT @deleted = COUNT(1) FROM fact.ExchangeRates ;
  EXEC dbo.truncate_table 'fact.ExchangeRates' ;

  INSERT INTO fact.ExchangeRates WITH (TABLOCK) (
    ExchangeRate
  , FromCurrencyId
  , ToCurrencyId
  , DateId
  , Partition
  , RecId

  -- System
  , DataConnectionID
  , ComponentExecutionID
  )
  SELECT CrossRate
       , FromCurrencyId
       , ToCurrencyId
       , DateId
       , Partition
       , RecId
       , DataConnectionID
       , ComponentExecutionID = COALESCE(@component_execution_id, -1)
    FROM fact.ExchangeRatesView ;

  SELECT @inserted = @@ROWCOUNT ;

  INSERT INTO fact.ExchangeRates WITH (TABLOCK) (
    ExchangeRate
  , FromCurrencyId
  , ToCurrencyId
  , DateId
  , Partition
  , RecId

  -- System
  , DataConnectionID
  , ComponentExecutionID
  )
  SELECT CrossRate
       , FromCurrencyId
       , ToCurrencyId
       , DateId
       , Partition
       , RecId
       , DataConnectionID
       , ComponentExecutionID = COALESCE(@component_execution_id, -1)
    FROM fact.ExchangeRatesInvalidDateView ;

  SELECT @inserted = @inserted + @@ROWCOUNT ;

END ;

GO

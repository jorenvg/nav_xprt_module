EXEC dbo.drop_object @object = N'fact.VendorTransactionTransView', @type = N'V' ;
GO
CREATE VIEW fact.VendorTransactionTransView
AS
  SELECT            VTT.StageId                              AS StageID
                  , VTT.ExecutionFlag                        AS ExecutionFlag
                  /***BK***/
                  , VTT.VendorTransactionRecId               AS VendorTransactionRecId
                  , VTT.VendorTransactionPartition           AS VendorTransactionPartition
                  , VTT.CompanyID                            AS CompanyID
                  , VTT.DataConnectionID                     AS DataConnectionID
                  , VTT.TransactionSource                    AS TransactionSource

                  /***Dimensions*/
                  , COALESCE(YN.YesNoID, 0)                  AS IsInvoiceID
                  , COALESCE(YN2.YesNoID, 0)                 AS IsCreditID
                  --Dates
                  , COALESCE(DP.DateID, 0)                   AS PostingDateID
                  , COALESCE(DD.DateID, 0)                   AS DueDateID
                  , COALESCE(DS.DateID, 0)                   AS LastSettleDateID
                  , COALESCE(DC.DateID, 0)                   AS ClosedDateID
                  , COALESCE(APP.EmployeeID, 0)              AS ApproverID
                  , COALESCE(cy.CurrencyID, 0)               AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)              AS CurrencyReportID
                  , COALESCE(LTT.LedgerTransactionTypeID, 0) AS LedgerTransactionTypeID
                  , COALESCE(SI.PurchaseInvoiceID, 0)        AS PurchaseInvoiceID
                  , COALESCE(CI.VendorID, 0)                 AS InvoiceAccountID
                  , COALESCE(VC.VoucherID, 0)                AS VoucherID
                  , COALESCE(FD.FinancialDimensionsID, 0)    AS FinancialDimensionsID

                  /***Measures*/
                  , VTT.Amount                               AS Amount
                  , VTT.Amount_RCY                           AS Amount_RCY
                  , VTT.AmountMST                            AS AmountMST

    --SELECT COUNT(*)
    FROM            delta.VendorTransactionTrans AS VTT
    LEFT JOIN       dim.Date                     AS DP ON DP.Date                        = VTT.PostingDate
    LEFT JOIN       dim.Date                     AS DD ON DD.Date                        = VTT.DueDate
    LEFT JOIN       dim.Date                     AS DS ON DS.Date                        = VTT.LastSettleDate
    LEFT JOIN       dim.Date                     AS DC ON DC.Date                        = VTT.ClosedDate
    LEFT JOIN       dim.Currency                 AS cy ON cy.Code                        = VTT.CurrencyCode
                                                      AND cy.Partition                   = VTT.VendorTransactionPartition
                                                      AND cy.DataConnectionID            = VTT.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport           AS rcy ON rcy.Partition                 = VTT.VendorTransactionPartition
    LEFT JOIN       dim.Employee                 AS APP ON APP.EmployeeRecID             = VTT.Approver
                                                       AND APP.Partition                 = VTT.VendorTransactionPartition
                                                       AND APP.DataConnectionID          = VTT.DataConnectionID
    LEFT JOIN       dim.LedgerTransactionType    AS LTT ON LTT.LedgerTransactionTypeCode = VTT.TransType
                                                       AND LTT.DataConnectionID          = VTT.DataConnectionID
    LEFT JOIN       dim.PurchaseInvoice          AS SI ON SI.PurchaseInvoice             = VTT.PurchaseInvoice
                                                      AND SI.Partition                   = VTT.VendorTransactionPartition
                                                      AND SI.DataConnectionID            = VTT.DataConnectionID
                                                      AND SI.CompanyID                   = VTT.CompanyID
    LEFT JOIN       dim.Vendor                   AS CI ON CI.VendorAccountNo             = VTT.InvoiceAccount
                                                      AND CI.Partition                   = VTT.VendorTransactionPartition
                                                      AND CI.CompanyID                   = VTT.CompanyID
                                                      AND CI.DataConnectionID            = VTT.DataConnectionID
    LEFT JOIN       dim.Voucher                  AS VC ON VC.Voucher                     = VTT.Voucher
                                                      AND VC.Partition                   = VTT.VendorTransactionPartition
                                                      AND VC.CompanyID                   = VTT.CompanyID
                                                      AND VC.DataConnectionID            = VTT.DataConnectionID
    LEFT JOIN       dim.FinancialDimensions      AS FD ON FD.DefaultDimension            = VTT.DefaultDimension
                                                      AND FD.Partition                   = VTT.VendorTransactionPartition
                                                      AND FD.DataConnectionID            = VTT.DataConnectionID
    LEFT JOIN       dim.YesNo                    AS YN ON YN.YesNoCode                   = VTT.IsInvoice
    LEFT JOIN       dim.YesNo                    AS YN2 ON YN2.YesNoCode                 = VTT.IsCredit ;



GO


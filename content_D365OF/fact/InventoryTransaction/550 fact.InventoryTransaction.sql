EXEC dbo.drop_object 'fact.InventoryTransaction', 'T' ;
GO

CREATE TABLE [fact].[InventoryTransaction]
(
  [ComponentExecutionID]   [INT]            NOT NULL
, [CompanyId]              [INT]            NOT NULL
, [DataConnectionID]       [INT]            NOT NULL
, [IncrementalDate]        [DATETIME]       NOT NULL
, [TransactionDateID]      [INT]            NOT NULL
, [WMSlocationID]          [INT]            NOT NULL
, [LocationID]             [INT]            NOT NULL
, [ProductID]              [INT]            NOT NULL
, [TrackingID]             [INT]            NOT NULL
, [InventoryReferenceID]   [INT]            NOT NULL
, [InventoryPostingTypeID] [INT]            NOT NULL
, [StatusReceiptID]        [INT]            NOT NULL
, [StatusIssueID]          [INT]            NOT NULL
, [PostingTypeID]          [INT]            NOT NULL
, [PostingTypeOffsetID]    [INT]            NOT NULL
, [PostedID]               [INT]            NOT NULL
, [VoucherID]              [INT]            NOT NULL
, [ItemID]                 [INT]            NOT NULL
, [FinancialDimensionsID]  [INT]            NOT NULL
, [CurrencyID]             [INT]            NOT NULL
, [SalesInvoiceID]         [INT]            NOT NULL
, [Quantity]               DECIMAL(19,4)    NULL
, [Amount]                 DECIMAL(19,4)    NULL
, [Amount_RCY]             DECIMAL(19,4)    NULL
, [IssueQuantity]          DECIMAL(19,4)    NULL
, [IssueAmount]            DECIMAL(19,4)    NULL
, [IssueAmount_RCY]        DECIMAL(19,4)    NULL
, [ReceiptQuantity]        DECIMAL(19,4)    NULL
, [ReceiptAmount]          DECIMAL(19,4)    NULL
, [ReceiptAmount_RCY]      DECIMAL(19,4)    NULL
) ;
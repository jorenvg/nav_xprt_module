EXEC dbo.drop_object @object = N'fact.InventoryTransactionView', @type = N'V' ;
GO
CREATE VIEW fact.InventoryTransactionView
AS
  WITH reporting_exchange_rates AS (
    SELECT er.CrossRate
         , er.ExchangeRate1
         , er.ExchangeRate2
         , er.ExchangeRateType
         , er.FromCurrencyCode
         , er.ToCurrencyCode
         , er.ValidFrom
         , er.ValidTo
         , er.Partition
         , er.DataConnectionID
      FROM help.ExchangeRates    AS er
      JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
  )
  SELECT      IT.CompanyID                               AS CompanyID
            , IT.DataConnectionID                        AS DataConnectionID
            , IT.INCREMENTALDATE                         AS IncrementalDate
            -- Dates
            , COALESCE(DP.DateID, 0)                     AS TransactionDateID

            -- Inventdimension
            , COALESCE(WL.WMSLocationID, 0)              AS WMSlocationID
            , COALESCE(L.LocationID, 0)                  AS LocationID
            , COALESCE(P.ProductID, 0)                   AS ProductID
            , COALESCE(T.TrackingID, 0)                  AS TrackingID
            , COALESCE(IR.InventoryReferenceID, 0)       AS InventoryReferenceID
            , COALESCE(IPT.InventoryPostingTypeID, 0)    AS InventoryPostingTypeID
            , COALESCE(SR.StatusReceiptID, 0)            AS StatusReceiptID
            , COALESCE(SI.StatusIssueID, 0)              AS StatusIssueID
            , COALESCE(PT.PostingTypeID, 0)              AS PostingTypeID
            , COALESCE(PTO.PostingTypeID, 0)             AS PostingTypeOffsetID
            , COALESCE(POS.PostedID, 0)                  AS PostedID
            , COALESCE(JV.VoucherID, 0)                  AS VoucherID
            , COALESCE(ITEM.ItemID, 0)                   AS ItemID
            , COALESCE(FD.FinancialDimensionsID, 0)      AS FinancialDimensionsID
            , COALESCE(curr.CurrencyID, 0)               AS CurrencyID
            , COALESCE(SINV.SalesInvoiceID, 0)           AS SalesInvoiceID
            , IT.AMOUNT                                  AS Amount
            , IT.AMOUNT * ISNULL(ex.CrossRate, 1)        AS Amount_RCY
            , IT.QUANTITY                                AS Quantity
            , IT.ISSUEQUANTITY                           AS IssueQuantity
            , IT.ISSUEAMOUNT                             AS IssueAmount
            , IT.ISSUEAMOUNT * ISNULL(ex.CrossRate, 1)   AS IssueAmount_RCY
            , IT.RECEIPTQUANTITY                         AS ReceiptQuantity
            , IT.RECEIPTAMOUNT                           AS ReceiptAmount
            , IT.RECEIPTAMOUNT * ISNULL(ex.CrossRate, 1) AS ReceiptAmount_RCY
    FROM      help.InventoryTransaction AS IT
    LEFT JOIN dim.Date                  AS DP ON DP.Date                       = IT.TRANSDATE
    LEFT JOIN dim.FinancialDimensions   AS FD ON FD.DefaultDimension           = IT.DEFAULTDIMENSION
                                             AND FD.Partition                  = IT.PARTITION
                                             AND FD.DataConnectionID           = IT.DataConnectionID
    LEFT JOIN dim.WMSLocation           AS WL ON WL.WMSLocationCode            = IT.WMSlocationCode
                                             AND WL.LocationCode               = IT.LocationCode
                                             AND WL.Partition                  = IT.PARTITION
                                             AND WL.DataConnectionID           = IT.DataConnectionID
                                             AND WL.CompanyID                  = IT.CompanyID
    LEFT JOIN dim.Location              AS L ON L.LocationCode                 = IT.LocationCode
                                            AND L.Partition                    = IT.PARTITION
                                            AND L.DataConnectionID             = IT.DataConnectionID
                                            AND L.CompanyID                    = IT.CompanyID
    LEFT JOIN dim.Product               AS P ON P.InventSize                   = IT.INVENTSIZEID
                                            AND P.InventColor                  = IT.INVENTCOLORID
                                            AND P.InventStyle                  = IT.INVENTSTYLEID
                                            AND P.InventConfiguration          = IT.CONFIGID
                                            AND P.Partition                    = IT.PARTITION
                                            AND P.CompanyID                    = IT.CompanyID
                                            AND P.DataConnectionID             = IT.DataConnectionID
                                            AND P.CompanyID                    = IT.CompanyID
    LEFT JOIN dim.Tracking              AS T ON T.InventBatchCode              = IT.TrackingBatchCode
                                            AND T.InventSerialCode             = IT.TrackingSerialCode
                                            AND T.Partition                    = IT.PARTITION
                                            AND T.CompanyID                    = IT.CompanyID
                                            AND T.DataConnectionID             = IT.DataConnectionID
    LEFT JOIN dim.InventoryReference    AS IR ON IR.Recid                      = IT.INVENTTRANSORIGIN
                                             AND IR.Partition                  = IT.PARTITION
                                             AND IR.DataConnectionID           = IT.DataConnectionID
    LEFT JOIN dim.StatusIssue           AS SI ON SI.StatusIssueCode            = IT.STATUSISSUE
                                             AND SI.DataConnectionID           = IT.DataConnectionID
    LEFT JOIN dim.StatusReceipt         AS SR ON SR.StatusReceiptCode          = IT.STATUSRECEIPT
                                             AND SR.DataConnectionID           = IT.DataConnectionID
    LEFT JOIN dim.PostingType           AS PT ON PT.PostingTypeCode            = IT.POSTINGTYPE
                                             AND PT.DataConnectionID           = IT.DataConnectionID
    LEFT JOIN dim.PostingType           AS PTO ON PTO.PostingTypeCode          = IT.POSTINGTYPEOFFSET
                                              AND PTO.DataConnectionID         = IT.DataConnectionID
    LEFT JOIN dim.Item                  AS ITEM ON ITEM.ItemNumber             = IT.ITEMID
                                               AND ITEM.CompanyID              = IT.CompanyID
                                               AND ITEM.DataConnectionID       = IT.DataConnectionID
                                               AND ITEM.Partition              = IT.PARTITION
    LEFT JOIN dim.Voucher               AS JV ON JV.VOUCHER                    = IT.VOUCHER
                                             AND JV.CompanyID                  = IT.CompanyID
                                             AND JV.PARTITION                  = IT.PARTITION
                                             AND JV.DataConnectionID           = IT.DataConnectionID
    LEFT JOIN dim.Posted                AS POS ON POS.PostedCode               = IT.ISPOSTED
                                              AND POS.DataConnectionID         = IT.DataConnectionID
    LEFT JOIN dim.InventoryPostingType  AS IPT ON IPT.InventoryPostingTypeCode = IT.INVENTTRANSPOSTINGTYPE
                                              AND IPT.DataConnectionID         = IT.DataConnectionID
    LEFT JOIN dim.Currency              AS curr ON curr.Code                   = IT.CURRENCYCODE
                                               AND curr.Partition              = IT.PARTITION
                                               AND curr.DataConnectionID       = IT.DataConnectionID
    LEFT JOIN dim.SalesInvoice          AS SINV ON SINV.SalesInvoice           = IT.INVOICEID
                                               AND SINV.CompanyID              = IT.CompanyID
                                               AND SINV.Partition              = IT.PARTITION
                                               AND SINV.DataConnectionID       = IT.DataConnectionID
    LEFT JOIN reporting_exchange_rates  AS ex ON IT.ACCOUNTINGCURRENCY         = ex.FromCurrencyCode
                                             AND IT.DataConnectionID           = ex.DataConnectionID
                                             AND IT.TRANSDATE BETWEEN ex.ValidFrom AND ex.ValidTo ;
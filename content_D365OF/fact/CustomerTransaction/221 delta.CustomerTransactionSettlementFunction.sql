/****************************************************************************************************
  Functionality: Selects the changed vendor settlements for use of the vendor transactions fact table.
  Created by:    JvL	Date:	2018/11/20
  Date 		    Changed by 	Ticket/Change 	Description
  2018/11/20 	JvL 		    DEV-1396 		    changed Voucher to include the OFFSETTRANSVOUCHER instead of using the voucher of the transaction
*****************************************************************************************************/

EXEC dbo.drop_object 'delta.CustomerTransactionSettlementFunction', 'F' ;
GO
CREATE FUNCTION delta.CustomerTransactionSettlementFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            CS.stage_id                                        AS StageId
                           , @execution_flag                                    AS ExecutionFlag
                           , CT.DATA_CONNECTION_ID                              AS DataConnectionID
                           , CT.COMPANY_ID                                      AS CompanyID
                           , @component_execution_id                            AS ComponentExecutionID
                           , CT.RECID                                           AS CustomerTransactionRecID
                           , CT.PARTITION                                       AS CustomerTransactionPartition
                           , 'Settlement'                                       AS TransactionSource
                           , '0'                                                AS IsInvoice
                           , CASE WHEN CT.SETTLEAMOUNTMST > 0 THEN 1 ELSE 0 END AS IsCredit
                           , CT.TRANSTYPE                                       AS TransType
                           , cs.OFFSETTRANSVOUCHER                              AS Voucher
                           , CT.INVOICE                                         AS SalesInvoice
                           , CT.TRANSDATE                                       AS InvoiceDate
                           , CS.TRANSDATE                                       AS PostingDate
                           , CT.DUEDATE                                         AS DueDate
                           , CT.LASTSETTLEDATE                                  AS LastSettleDate
                           , CT.Closed                                          AS ClosedDate
                           , CT.ACCOUNTNUM                                      AS InvoiceAccount
                           , CT.ORDERACCOUNT                                    AS OrderAccount
                           , CS.DEFAULTDIMENSION                                AS DefaultDimension
                           , CT.APPROVER                                        AS Approver
                           , CT.CurrencyCode                                    AS CurrencyCode
                           , CS.SETTLEAMOUNTCUR * -1                            AS Amount
                           , CS.SETTLEAMOUNTCUR * EX.CrossRate * -1             AS Amount_RCY
                           , CS.SETTLEAMOUNTMST * -1                            AS AmountMST
             FROM            stage_ax.CUSTSETTLEMENT  AS CS
            INNER JOIN       stage_ax.CUSTTRANS       AS CT ON CS.TRANSRECID         = CT.recid
                                                           AND CT.transtype          <> 9 
                                                           -- Filters the ExchAdjustment settlements. This has been removed from VendorTransactions.
                                                           AND CS.data_connection_id = CT.data_connection_id
             LEFT OUTER JOIN reporting_exchange_rates AS EX ON EX.FromCurrencyCode   = CT.CURRENCYCODE
                                                           AND EX.DataConnectionID   = CT.data_connection_id
                                                           AND CS.TRANSDATE BETWEEN EX.ValidFrom AND EX.ValidTo
            WHERE
             /** FULL LOAD **/
                             ( @load_type        = 0
                           AND CS.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'N'
                           AND CS.execution_flag               = @execution_flag
                           AND CS.execution_timestamp          > @last_processed_timestamp)
               OR
               /** INCREMENTAL, UPDATED RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'U'
                           AND ( ( CS.execution_flag           = @execution_flag
                               AND CS.execution_timestamp      > @last_processed_timestamp)
                              OR ( CT.execution_flag           = @execution_flag
                               AND CT.execution_timestamp      > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'D'
                           AND CS.execution_flag               = @execution_flag
                           AND CS.execution_timestamp          > @last_processed_timestamp)) ;

GO
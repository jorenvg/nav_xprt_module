EXEC dbo.drop_object @object = N'fact.CustomerTransactionSettlementView', @type = N'V' ;
GO
CREATE VIEW fact.CustomerTransactionSettlementView
AS
  SELECT            CTS.StageId                              AS StageID
                  , CTS.ExecutionFlag                        AS ExecutionFlag
                  /***BK***/
                  , CTS.CustomerTransactionRecId             AS CustomerTransactionRecId
                  , CTS.CustomerTransactionPartition         AS CustomerTransactionPartition
                  , CTS.CompanyID                            AS CompanyID
                  , CTS.DataConnectionID                     AS DataConnectionID
                  , CTS.TransactionSource                    AS TransactionSource

                  /***Dimensions*/
                  --Dates
                  , COALESCE(YN.YesNoID, 0)                  AS IsInvoiceID
                  , COALESCE(YN2.YesNoID, 0)                 AS IsCreditID
                  , COALESCE(DP.DateID, 0)                   AS PostingDateID
                  , COALESCE(DD.DateID, 0)                   AS DueDateID
                  , COALESCE(DS.DateID, 0)                   AS LastSettleDateID
                  , COALESCE(DC.DateID, 0)                   AS ClosedDateID
                  , COALESCE(APP.EmployeeID, 0)              AS ApproverID
                  , COALESCE(cy.CurrencyID, 0)               AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)              AS CurrencyReportID
                  , COALESCE(LTT.LedgerTransactionTypeID, 0) AS LedgerTransactionTypeID
                  , COALESCE(SI.SalesInvoiceID, 0)           AS SalesInvoiceID
                  , COALESCE(CI.CustomerID, 0)               AS InvoiceAccountID
                  , COALESCE(CO.CustomerID, 0)               AS OrderAccountID
                  , COALESCE(VC.VoucherID, 0)                AS VoucherID
                  , COALESCE(FD.FinancialDimensionsID, 0)    AS FinancialDimensionsID

                  /***Measures*/
                  , CTS.Amount                               AS Amount
                  , CTS.Amount_RCY                           AS Amount_RCY
                  , CTS.AmountMST                            AS AmountMST
    FROM            delta.CustomerTransactionSettlement AS CTS
    LEFT JOIN       dim.Date                            AS DP ON DP.Date                        = CTS.PostingDate
    LEFT JOIN       dim.Date                            AS DD ON DD.Date                        = CTS.DueDate
    LEFT JOIN       dim.Date                            AS DS ON DS.Date                        = CTS.LastSettleDate
    LEFT JOIN       dim.Date                            AS DC ON DC.Date                        = CTS.ClosedDate
    LEFT JOIN       dim.Currency                        AS cy ON cy.Code                        = CTS.CurrencyCode
                                                             AND cy.Partition                   = CTS.CustomerTransactionPartition
                                                             AND cy.DataConnectionID            = CTS.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport                  AS rcy ON rcy.Partition                 = CTS.CustomerTransactionPartition
    LEFT JOIN       dim.Employee                        AS APP ON APP.EmployeeRecID             = CTS.Approver
                                                              AND APP.Partition                 = CTS.CustomerTransactionPartition
                                                              AND APP.DataConnectionID          = CTS.DataConnectionID
    LEFT JOIN       dim.LedgerTransactionType           AS LTT ON LTT.LedgerTransactionTypeCode = CTS.TransType
                                                              AND LTT.DataConnectionID          = CTS.DataConnectionID
    LEFT JOIN       dim.SalesInvoice                    AS SI ON SI.SalesInvoice                = CTS.SalesInvoice
                                                             AND SI.Partition                   = CTS.CustomerTransactionPartition
                                                             AND SI.DataConnectionID            = CTS.DataConnectionID
                                                             AND SI.CompanyID                   = CTS.CompanyID
    LEFT JOIN       dim.Customer                        AS CI ON CI.CustomerAccountNo           = CTS.InvoiceAccount
                                                             AND CI.Partition                   = CTS.CustomerTransactionPartition
                                                             AND CI.CompanyID                   = CTS.CompanyID
                                                             AND CI.DataConnectionID            = CTS.DataConnectionID
    LEFT JOIN       dim.Customer                        AS CO ON CO.CustomerAccountNo           = CTS.OrderAccount
                                                             AND CO.Partition                   = CTS.CustomerTransactionPartition
                                                             AND CO.DataConnectionID            = CTS.DataConnectionID
                                                             AND CO.CompanyID                   = CTS.CompanyID
    LEFT JOIN       dim.Voucher                         AS VC ON VC.Voucher                     = CTS.Voucher
                                                             AND VC.Partition                   = CTS.CustomerTransactionPartition
                                                             AND VC.CompanyID                   = CTS.CompanyID
                                                             AND VC.DataConnectionID            = CTS.DataConnectionID
    LEFT JOIN       dim.FinancialDimensions             AS FD ON FD.DefaultDimension            = CTS.DefaultDimension
                                                             AND FD.Partition                   = CTS.CustomerTransactionPartition
                                                             AND FD.DataConnectionID            = CTS.DataConnectionID
    LEFT JOIN       dim.YesNo                           AS YN ON YN.YesNoCode                   = CTS.IsInvoice
    LEFT JOIN       dim.YesNo                           AS YN2 ON YN2.YesNoCode                 = CTS.IsCredit ;



GO


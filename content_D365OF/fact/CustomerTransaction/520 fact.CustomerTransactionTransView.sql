EXEC dbo.drop_object @object = N'fact.CustomerTransactionTransView', @type = N'V' ;
GO
CREATE VIEW fact.CustomerTransactionTransView
AS
  SELECT            CTT.StageId                              AS StageID
                  , CTT.ExecutionFlag                        AS ExecutionFlag

                  /***BK***/
                  , CTT.CustomerTransactionRecId             AS CustomerTransactionRecId
                  , CTT.CustomerTransactionPartition         AS CustomerTransactionPartition
                  , CTT.CompanyID                            AS CompanyID
                  , CTT.DataConnectionID                     AS DataConnectionID
                  , CTT.TransactionSource                    AS TransactionSource

                  /***Dimensions*/
                  , COALESCE(YN.YesNoID, 0)                  AS IsInvoiceID
                  , COALESCE(YN2.YesNoID, 0)                 AS IsCreditID
                  , COALESCE(DP.DateID, 0)                   AS PostingDateID
                  , COALESCE(DD.DateID, 0)                   AS DueDateID
                  , COALESCE(DS.DateID, 0)                   AS LastSettleDateID
                  , COALESCE(DC.DateID, 0)                   AS ClosedDateID
                  , COALESCE(APP.EmployeeID, 0)              AS ApproverID
                  , COALESCE(cy.CurrencyID, 0)               AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)              AS CurrencyReportID
                  , COALESCE(LTT.LedgerTransactionTypeID, 0) AS LedgerTransactionTypeID
                  , COALESCE(SI.SalesInvoiceID, 0)           AS SalesInvoiceID
                  , COALESCE(CI.CustomerID, 0)               AS InvoiceAccountID
                  , COALESCE(CO.CustomerID, 0)               AS OrderAccountID
                  , COALESCE(VC.VoucherID, 0)                AS VoucherID
                  , COALESCE(FD.FinancialDimensionsID, 0)    AS FinancialDimensionsID

                  /***Measures*/
                  , CTT.Amount                               AS Amount
                  , CTT.Amount_RCY                           AS Amount_RCY
                  , CTT.AmountMST                            AS AmountMST

    --SELECT COUNT(*)
    FROM            delta.CustomerTransactionTrans AS CTT
    LEFT JOIN       dim.Date                       AS DP ON DP.Date                        = CTT.PostingDate
    LEFT JOIN       dim.Date                       AS DD ON DD.Date                        = CTT.DueDate
    LEFT JOIN       dim.Date                       AS DS ON DS.Date                        = CTT.LastSettleDate
    LEFT JOIN       dim.Date                       AS DC ON DC.Date                        = CTT.ClosedDate
    LEFT JOIN       dim.Currency                   AS cy ON cy.Code                        = CTT.CurrencyCode
                                                        AND cy.Partition                   = CTT.CustomerTransactionPartition
                                                        AND cy.DataConnectionID            = CTT.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport             AS rcy ON rcy.Partition                 = CTT.CustomerTransactionPartition
    LEFT JOIN       dim.Employee                   AS APP ON APP.EmployeeRecID             = CTT.Approver
                                                         AND APP.Partition                 = CTT.CustomerTransactionPartition
                                                         AND APP.DataConnectionID          = CTT.DataConnectionID
    LEFT JOIN       dim.LedgerTransactionType      AS LTT ON LTT.LedgerTransactionTypeCode = CTT.TransType
                                                         AND LTT.DataConnectionID          = CTT.DataConnectionID
    LEFT JOIN       dim.SalesInvoice               AS SI ON SI.SalesInvoice                = CTT.SalesInvoice
                                                        AND SI.Partition                   = CTT.CustomerTransactionPartition
                                                        AND SI.DataConnectionID            = CTT.DataConnectionID
                                                        AND SI.CompanyID                   = CTT.CompanyID
    LEFT JOIN       dim.Customer                   AS CI ON CI.CustomerAccountNo           = CTT.InvoiceAccount
                                                        AND CI.Partition                   = CTT.CustomerTransactionPartition
                                                        AND CI.CompanyID                   = CTT.CompanyID
                                                        AND CI.DataConnectionID            = CTT.DataConnectionID
    LEFT JOIN       dim.Customer                   AS CO ON CO.CustomerAccountNo           = CTT.OrderAccount
                                                        AND CO.Partition                   = CTT.CustomerTransactionPartition
                                                        AND CO.DataConnectionID            = CTT.DataConnectionID
                                                        AND CO.CompanyID                   = CTT.CompanyID
    LEFT JOIN       dim.Voucher                    AS VC ON VC.Voucher                     = CTT.Voucher
                                                        AND VC.Partition                   = CTT.CustomerTransactionPartition
                                                        AND VC.CompanyID                   = CTT.CompanyID
                                                        AND VC.DataConnectionID            = CTT.DataConnectionID
    LEFT JOIN       dim.FinancialDimensions        AS FD ON FD.DefaultDimension            = CTT.DefaultDimension
                                                        AND FD.Partition                   = CTT.CustomerTransactionPartition
                                                        AND FD.DataConnectionID            = CTT.DataConnectionID
    LEFT JOIN       dim.YesNo                      AS YN ON YN.YesNoCode                   = CTT.IsInvoice
    LEFT JOIN       dim.YesNo                      AS YN2 ON YN2.YesNoCode                 = CTT.IsCredit ;



GO


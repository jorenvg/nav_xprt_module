EXEC dbo.drop_object @object = N'fact.LoadSalesForecast' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;                        -- int
GO

CREATE PROCEDURE fact.LoadSalesForecast
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @table_name sysname = N'SalesForecast' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.SalesForecast ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO fact.SalesForecast (
      StageID
    , ComponentExecutionID
    , CompanyID
    , DataConnectionID
    , SalesForecastRecID
    , SalesForecastPartition
    , FinancialDimensionsID
    , ForecastStartDateID
    , ForecastEndDateID
    , ItemID
    , ForecastModelID
    , CustomerAccountID
    , CurrencyTransactionID
    , CurrencyReportID
    , WMSlocationID
    , LocationID
    , ProductID
    , TrackingID
    , ForecastInventoryQuantity
    , ForecastSalesQuantity
    , ForecastDiscountPercent
    , ForecastSalesUnit
    , ForecastAmount
    , ForecastAmount_RCY
    , ForecastCostPrice
    , ForecastCostPrice_RCY
    , ForecastSalesPrice
    , ForecastSalesPrice_RCY
    , ForecastDiscountAmount
    , ForecastDiscountAmount_RCY
    , ForecastPriceUnit
    , ForecastPriceUnit_RCY
    )
    SELECT StageID
         , @component_execution_id
         , CompanyID
         , DataConnectionID
         , SalesForecastRecID
         , SalesForecastPartition
         , FinancialDimensionsID
         , ForecastStartDateID
         , ForecastEndDateID
         , ItemID
         , ForecastModelID
         , CustomerAccountID
         , CurrencyTransactionID
         , CurrencyReportID
         , WMSlocationID
         , LocationID
         , ProductID
         , TrackingID
         , ForecastInventoryQuantity
         , ForecastSalesQuantity
         , ForecastDiscountPercent
         , ForecastSalesUnit
         , ForecastAmount
         , ForecastAmount_RCY
         , ForecastCostPrice
         , ForecastCostPrice_RCY
         , ForecastSalesPrice
         , ForecastSalesPrice_RCY
         , ForecastDiscountAmount
         , ForecastDiscountAmount_RCY
         , ForecastPriceUnit
         , ForecastPriceUnit_RCY
      FROM fact.SalesForecastView ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.SalesForecast
         SET      ComponentExecutionID = @component_execution_id
                , CompanyID = V.CompanyID
                , DataConnectionID = V.DataConnectionID
                , SalesForecastRecID = V.SalesForecastRecID
                , SalesForecastPartition = V.SalesForecastPartition
                , FinancialDimensionsID = V.FinancialDimensionsID
                , ForecastStartDateID = V.ForecastStartDateID
                , ForecastEndDateID = V.ForecastEndDateID
                , ItemID = V.ItemID
                , ForecastModelID = V.ForecastModelID
                , CustomerAccountID = V.CustomerAccountID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , WMSlocationID = V.WMSlocationID
                , LocationID = V.LocationID
                , ProductID = V.ProductID
                , TrackingID = V.TrackingID
                , ForecastInventoryQuantity = V.ForecastInventoryQuantity
                , ForecastSalesQuantity = V.ForecastSalesQuantity
                , ForecastDiscountPercent = V.ForecastDiscountPercent
                , ForecastSalesUnit = V.ForecastSalesUnit
                , ForecastAmount = V.ForecastAmount
                , ForecastAmount_RCY = V.ForecastAmount_RCY
                , ForecastCostPrice = V.ForecastCostPrice
                , ForecastCostPrice_RCY = V.ForecastCostPrice_RCY
                , ForecastSalesPrice = V.ForecastSalesPrice
                , ForecastSalesPrice_RCY = V.ForecastSalesPrice_RCY
                , ForecastDiscountAmount = V.ForecastDiscountAmount
                , ForecastDiscountAmount_RCY = V.ForecastDiscountAmount_RCY
                , ForecastPriceUnit = V.ForecastPriceUnit
                , ForecastPriceUnit_RCY = V.ForecastPriceUnit_RCY
        FROM      fact.SalesForecast     F
       INNER JOIN fact.SalesForecastView AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.SalesForecast (
        StageID
      , ComponentExecutionID
      , CompanyID
      , DataConnectionID
      , SalesForecastRecID
      , SalesForecastPartition
      , FinancialDimensionsID
      , ForecastStartDateID
      , ForecastEndDateID
      , ItemID
      , ForecastModelID
      , CustomerAccountID
      , CurrencyTransactionID
      , CurrencyReportID
      , WMSlocationID
      , LocationID
      , ProductID
      , TrackingID
      , ForecastInventoryQuantity
      , ForecastSalesQuantity
      , ForecastDiscountPercent
      , ForecastSalesUnit
      , ForecastAmount
      , ForecastAmount_RCY
      , ForecastCostPrice
      , ForecastCostPrice_RCY
      , ForecastSalesPrice
      , ForecastSalesPrice_RCY
      , ForecastDiscountAmount
      , ForecastDiscountAmount_RCY
      , ForecastPriceUnit
      , ForecastPriceUnit_RCY
      )
      SELECT V.StageID
           , @component_execution_id
           , V.CompanyID
           , V.DataConnectionID
           , V.SalesForecastRecID
           , V.SalesForecastPartition
           , V.FinancialDimensionsID
           , V.ForecastStartDateID
           , V.ForecastEndDateID
           , V.ItemID
           , V.ForecastModelID
           , V.CustomerAccountID
           , CurrencyTransactionID
           , CurrencyReportID
           , V.WMSlocationID
           , V.LocationID
           , V.ProductID
           , V.TrackingID
           , V.ForecastInventoryQuantity
           , V.ForecastSalesQuantity
           , V.ForecastDiscountPercent
           , V.ForecastSalesUnit
           , V.ForecastAmount
           , ForecastAmount_RCY
           , V.ForecastCostPrice
           , ForecastCostPrice_RCY
           , V.ForecastSalesPrice
           , ForecastSalesPrice_RCY
           , V.ForecastDiscountAmount
           , ForecastDiscountAmount_RCY
           , V.ForecastPriceUnit
           , ForecastPriceUnit_RCY
        FROM fact.SalesForecastView V
       WHERE V.ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.SalesForecast F WHERE F.StageID = V.StageID) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.SalesForecast
       WHERE EXISTS ( SELECT 1
                        FROM fact.SalesForecastView V
                       WHERE V.ExecutionFlag = 'D'
                         AND V.StageID       = [fact].[SalesForecast].StageID) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
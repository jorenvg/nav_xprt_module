EXEC dbo.drop_object 'delta.PurchasePacking', 'T' ;
GO
CREATE TABLE [delta].[PurchasePacking]
(
  StageId                         BIGINT            NOT NULL
, ExecutionFlag                   NCHAR(1)          NOT NULL
, [CompanyID]                     [INT]             NOT NULL
, [DataConnectionID]              [INT]             NOT NULL
, [ComponentExecutionID]          INT               NOT NULL
, [PurchasePackingTransRecId]     [BIGINT]          NULL
, [PurchasePackingTransPartition] BIGINT            NULL
, [DocumentDate]                  [DATE]            NULL
, [ShippingExpectedDate]          [DATETIME]        NULL
, [DeliveryDate]                  [DATETIME]        NULL
, [WMSlocationCode]               [NVARCHAR](100)   NULL
, [LocationCode]                  [NVARCHAR](100)   NULL
, [ProductConfigCode]             [NVARCHAR](100)   NULL
, [ProductColorCode]              [NVARCHAR](100)   NULL
, [ProductInventSizeCode]         [NVARCHAR](100)   NULL
, [ProductStyleCode]              [NVARCHAR](100)   NULL
, [TrackingBatchCode]             [NVARCHAR](100)   NULL
, [TrackingSerialCode]            [NVARCHAR](100)   NULL
, [PurchaserCode]                 [BIGINT]          NULL
, [PurchaseRequesterCode]         [BIGINT]          NULL
, [VendorAccountCode]             [NVARCHAR](100)   NULL
, [InvoiceAccountCode]            [NVARCHAR](100)   NULL
, [DeliveryAddressCode]           [BIGINT]          NULL
, [CompanyCode]                   [NVARCHAR](10)    NULL
, [ItemCode]                      [NVARCHAR](100)   NULL
, [CurrencyCode]                  [NVARCHAR](10)    NULL
, [FinancialDimensionCode]        [BIGINT]          NULL
, [OrderTypeCode]                 [INT]             NULL
, PurchasePoolCode                [NVARCHAR](100)   NULL
, [DeliveryTermsCode]             [NVARCHAR](100)   NULL
, [DeliveryModeCode]              [NVARCHAR](100)   NULL
, PaymentModeCode                 [NVARCHAR](100)   NULL
, PaymentTermsCode                [NVARCHAR](100)   NULL
, [PurchaseCode]                  [NVARCHAR](100)   NULL
, [PurchasePackingSlip]           [NVARCHAR](100)   NULL
, [PurchaseUnitCode]              [NVARCHAR](100)   NULL
, [OrderedQuantity]               DECIMAL(19,4)     NULL
, [RemainingQuantity]             DECIMAL(19,4)     NULL
, [DeliveredQuantity]             DECIMAL(19,4)     NULL
, [InventoryQuantity]             DECIMAL(19,4)     NULL
, [InventoryQuantityOpen]         DECIMAL(19,4)     NULL
, [ValueMST]                      DECIMAL(19,4)     NULL
) ; 
EXEC dbo.drop_object @object = N'fact.LoadGeneralLedger' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;                        -- int
GO

CREATE PROCEDURE fact.LoadGeneralLedger
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  DECLARE @table_name sysname = N'GeneralLedger' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;

  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.GeneralLedger ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO [fact].[GeneralLedger] (
      DataConnectionID
    , CompanyID
    , ComponentExecutionID
    , Partition
    , FinancialDimensionsID
    , PostingDateID
    , VoucherID
    , VoucherTextID
    , ReasonID
    , PostingLayerID
    , FiscalCalendarID
    , LedgerTransactionTypeID
    , ChartOfAccountsID
    , ClosedPeriodID
    , CurrencyTransactionID
    , CurrencyReportID
    , PostingTypeID
    , IsCredit
    , Balance
    , TransactionAmount
    , TransactionAmount_RCY
    , AccountingAmount
    , ReportingAmount
    , StageId
    )
    SELECT DataConnectionID
         , CompanyID
         , @component_execution_id AS ComponentExecutionID
         , Partition
         , FinancialDimensionsID
         , PostingDateID
         , VoucherID
         , VoucherTextID
         , ReasonID
         , PostingLayerID
         , FiscalCalendarID
         , LedgerTransactionTypeID
         , ChartOfAccountsID
         , ClosedPeriodID
         , CurrencyTransactionID
         , CurrencyReportID
         , PostingTypeID
         , IsCredit
         , Balance
         , TransactionAmount
         , TransactionAmount_RCY
         , AccountingAmount
         , ReportingAmount
         , StageId
      FROM [fact].[GeneralLedgerView] ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN

      UPDATE      [fact].[GeneralLedger]
         SET      DataConnectionID = V.DataConnectionID
                , CompanyID = V.CompanyID
                , ComponentExecutionID = @component_execution_id
                , Partition = V.Partition
                , FinancialDimensionsID = V.FinancialDimensionsID
                , PostingDateID = V.PostingDateID
                , VoucherID = V.VoucherID
                , VoucherTextID = V.VoucherTextID
                , ReasonID = V.ReasonID
                , PostingLayerID = V.PostingLayerID
                , FiscalCalendarID = V.FiscalCalendarID
                , LedgerTransactionTypeID = V.LedgerTransactionTypeID
                , ChartOfAccountsID = V.ChartOfAccountsID
                , ClosedPeriodID = V.ClosedPeriodID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , PostingTypeID = V.PostingTypeID
                , IsCredit = V.IsCredit
                , Balance = V.Balance
                , TransactionAmount = V.TransactionAmount
                , TransactionAmount_RCY = V.TransactionAmount_RCY
                , AccountingAmount = V.AccountingAmount
                , ReportingAmount = V.ReportingAmount
        FROM      [fact].[GeneralLedger]     AS F
       INNER JOIN [fact].[GeneralLedgerView] AS V ON F.StageId = V.StageId
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN

      INSERT INTO [fact].[GeneralLedger] (
        DataConnectionID
      , CompanyID
      , ComponentExecutionID
      , Partition
      , FinancialDimensionsID
      , PostingDateID
      , VoucherID
      , VoucherTextID
      , ReasonID
      , PostingLayerID
      , FiscalCalendarID
      , LedgerTransactionTypeID
      , ChartOfAccountsID
      , ClosedPeriodID
      , CurrencyTransactionID
      , CurrencyReportID
      , PostingTypeID
      , IsCredit
      , Balance
      , TransactionAmount
      , TransactionAmount_RCY
      , AccountingAmount
      , ReportingAmount
      , StageId
      )
      SELECT DataConnectionID
           , CompanyID
           , @component_execution_id AS ComponentExecutionID
           , Partition
           , FinancialDimensionsID
           , PostingDateID
           , VoucherID
           , VoucherTextID
           , ReasonID
           , PostingLayerID
           , FiscalCalendarID
           , LedgerTransactionTypeID
           , ChartOfAccountsID
           , ClosedPeriodID
           , CurrencyTransactionID
           , CurrencyReportID
           , PostingTypeID
           , IsCredit
           , Balance
           , TransactionAmount
           , TransactionAmount_RCY
           , AccountingAmount
           , ReportingAmount
           , StageId
        FROM [fact].[GeneralLedgerView] AS V
       WHERE ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.GeneralLedger AS F WHERE F.StageId = V.StageId) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0 
    BEGIN
      DELETE FROM [fact].[GeneralLedger]
			WHERE EXISTS ( SELECT 1
							FROM	[fact].[GeneralLedgerView] V
							WHERE	ExecutionFlag	= 'D'
							AND		v.StageId		= [fact].[GeneralLedger].StageId) ;

       SELECT @deleted = @@ROWCOUNT ;
                             
    END ;
  END ;
END ;
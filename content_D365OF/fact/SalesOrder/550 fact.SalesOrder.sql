EXEC dbo.drop_object 'fact.SalesOrder', 'T' ;
GO
CREATE TABLE [fact].[SalesOrder]
(
  [InventTransCode]          [NVARCHAR](100)   NULL
, [DataAreaCode]             [NVARCHAR](100)   NULL
, [Partition]                [BIGINT]          NULL
, [CreatedDate]              [DATE]            NULL
, [CompanyID]                [INT]             NOT NULL
, [DataConnectionID]         [INT]             NOT NULL
, [ComponentExecutionID]     INT               NOT NULL
, [CreatedDateID]            [INT]             NOT NULL
, [ReceiptRequestedDateID]   [INT]             NOT NULL
, [ShippingRequestedDateID]  [INT]             NOT NULL
, [ReceiptConfirmedDateID]   [INT]             NOT NULL
, [ShippingConfirmedDateID]  [INT]             NOT NULL
, [WMSlocationID]            [INT]             NOT NULL
, [LocationID]               [INT]             NOT NULL
, [ProductID]                [INT]             NOT NULL
, [TrackingID]               [INT]             NOT NULL
, [SalesTakerID]             [INT]             NOT NULL
, [SalesResponsibleID]       [INT]             NOT NULL
, [CustomerAccountID]        [INT]             NOT NULL
, [InvoiceAccountID]         [INT]             NOT NULL
, [DeliveryAddressID]        [INT]             NOT NULL
, [ItemID]                   [INT]             NOT NULL
, [CurrencyTransactionID]    [INT]             NOT NULL
, [CurrencyReportID]         [INT]             NOT NULL
, [FinancialDimensionsID]    [INT]             NOT NULL
, [ReasonID]                 [INT]             NOT NULL
, [SalesStatusID]            [INT]             NOT NULL
, [OrderTypeID]              [INT]             NOT NULL
, [SalesOriginID]            [INT]             NOT NULL
, [SalesPoolID]              [INT]             NOT NULL
, [SalesGroupID]             [INT]             NOT NULL
, [ModeOfDeliveryID]         [INT]             NOT NULL
, [DeliveryTermsID]          [INT]             NOT NULL
, [DeliveryTypeID]           [INT]             NOT NULL
, [MethodOfPaymentID]        [INT]             NOT NULL
, [PaymentTermsID]           [INT]             NOT NULL
, [SalesOrderID]             [INT]             NOT NULL
, [SalesUnitID]              [INT]             NOT NULL
, [SalesTaxID]               [INT]             NOT NULL
, [SalesQuantity]            DECIMAL(19,4)     NULL
, [PriceUnit]                DECIMAL(19,4)     NULL
, [PriceUnit_RCY]            DECIMAL(19,4)     NULL
, [CostPrice]                DECIMAL(19,4)     NULL
, [CostPrice_RCY]            DECIMAL(19,4)     NULL
, [SalesPrice]               DECIMAL(19,4)     NULL
, [SalesPrice_RCY]           DECIMAL(19,4)     NULL
, [Discount]                 DECIMAL(19,4)     NULL
, [Discount_RCY]             DECIMAL(19,4)     NULL
, [DiscountPercent]          DECIMAL(19,4)     NULL
, [MultilineDiscount]        DECIMAL(19,4)     NULL
, [MultilineDiscount_RCY]    DECIMAL(19,4)     NULL
, [MultilineDiscountPercent] DECIMAL(19,4)     NULL
, [SalesCharges]             DECIMAL(19,4)     NULL
, [SalesCharges_RCY]         DECIMAL(19,4)     NULL
, [NetAmount]                DECIMAL(19,4)     NULL
, [NetAmount_RCY]            DECIMAL(19,4)     NULL
, InventoryQuantity          DECIMAL(19,4)     NULL
, InventoryQuantityOpen      DECIMAL(19,4)     NULL
, SalesQuantityOpen          DECIMAL(19,4)     NULL
, AmountInclTaxExclDiscount  DECIMAL(19,4)     NULL
, StageID                    BIGINT            NOT NULL
) ;

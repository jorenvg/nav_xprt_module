EXEC dbo.drop_object 'delta.SalesOrderFunction', 'F' ;
GO
CREATE FUNCTION delta.SalesOrderFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            SL.stage_id                                                                                                                AS StageID
                           , @execution_flag                                                                                                            AS ExecutionFlag
                           , SL.INVENTTRANSID                                                                                                           AS InventTransCode
                           , SL.DATAAREAID                                                                                                              AS DataAreaCode
                           , SL.PARTITION                                                                                                               AS Partition
                           , SL.company_id                                                                                                              AS CompanyID
                           , SL.data_connection_id                                                                                                      AS DataConnectionID
                           , @component_execution_id                                                                                                    AS ComponentExecutionID
                           , CAST(SL.CREATEDDATETIME AS DATE)                                                                                           AS CreatedDate
                           , COALESCE(NULLIF(SL.RECEIPTDATEREQUESTED, '1900-01-01 00:00:00'), NULLIF(ST.RECEIPTDATEREQUESTED, '1900-01-01 00:00:00'))   AS ReceiptRequestedDate
                           , COALESCE(NULLIF(SL.SHIPPINGDATEREQUESTED, '1900-01-01 00:00:00'), NULLIF(ST.SHIPPINGDATEREQUESTED, '1900-01-01 00:00:00')) AS ShippingRequestedDate
                           , COALESCE(NULLIF(SL.RECEIPTDATECONFIRMED, '1900-01-01 00:00:00'), NULLIF(ST.RECEIPTDATECONFIRMED, '1900-01-01 00:00:00'))   AS ReceiptConfirmedDate
                           , COALESCE(NULLIF(SL.SHIPPINGDATECONFIRMED, '1900-01-01 00:00:00'), NULLIF(ST.SHIPPINGDATECONFIRMED, '1900-01-01 00:00:00')) AS ShippingConfirmedDate
                           , COALESCE(NULLIF(ID.WMSLOCATIONID, ''), 'N/A')                                                                              AS WMSlocationCode
                           , COALESCE(NULLIF(ID.INVENTLOCATIONID, ''), 'N/A')                                                                           AS LocationCode
                           , COALESCE(NULLIF(ID.CONFIGID, ''), 'N/A')                                                                                   AS ProductConfigCode
                           , COALESCE(NULLIF(ID.INVENTCOLORID, ''), 'N/A')                                                                              AS ProductColorCode
                           , COALESCE(NULLIF(ID.INVENTSIZEID, ''), 'N/A')                                                                               AS ProductInventSizeCode
                           , COALESCE(NULLIF(ID.INVENTSTYLEID, ''), 'N/A')                                                                              AS ProductStyleCode
                           , COALESCE(NULLIF(ID.INVENTBATCHID, ''), 'N/A')                                                                              AS TrackingBatchCode
                           , COALESCE(NULLIF(ID.INVENTSERIALID, ''), 'N/A')                                                                             AS TrackingSerialCode
                           , ST.WORKERSALESTAKER                                                                                                        AS SalesTakerCode
                           , ST.WORKERSALESRESPONSIBLE                                                                                                  AS SalesResponsibleCode
                           , ST.CUSTACCOUNT                                                                                                             AS CustomerAccountCode
                           , ST.INVOICEACCOUNT                                                                                                          AS InvoiceAccountCode
                           , SL.DATAAREAID                                                                                                              AS CompanyCode
                           , SL.ITEMID                                                                                                                  AS ItemCode
                           , SL.CURRENCYCODE                                                                                                            AS CurrencyCode
                           , SL.DEFAULTDIMENSION                                                                                                        AS FinancialDimensionCode
                           , SL.CREDITNOTEREASONCODE                                                                                                    AS CreditNoteReasonCode
                           , SL.DELIVERYPOSTALADDRESS                                                                                                   AS DeliveryAddressCode
                           , SL.SALESSTATUS                                                                                                             AS SalesStatusCode
                           , ST.SALESTYPE                                                                                                               AS OrderTypeCode
                           , ST.SALESORIGINID                                                                                                           AS SalesOriginCode
                           , ST.SALESPOOLID                                                                                                             AS SalesPoolCode
                           , ST.SALESGROUP                                                                                                              AS SalesGroupCode
                           , COALESCE(NULLIF(SL.DLVMODE, ''), ST.DLVMODE)                                                                               AS DeliveryModeCode
                           , COALESCE(NULLIF(SL.DLVTERM, ''), ST.DLVTERM)                                                                               AS DeliveryTermsCode
                           , SL.DELIVERYTYPE                                                                                                            AS DeliveryTypeCode
                           , ST.PAYMMODE                                                                                                                AS PaymentModeCode
                           , ST.PAYMENT                                                                                                                 AS PaymentTermsCode
                           , SL.SALESID                                                                                                                 AS SalesCode
                           , SL.SALESUNIT                                                                                                               AS SalesUnitCode
                           , SL.TAXGROUP                                                                                                                AS SalesTaxGroupCode
                           , SL.TAXITEMGROUP                                                                                                            AS SalesTaxItemGroupCode
                           , SL.SALESQTY                                                                                                                AS SalesQuantity
                           , SL.PRICEUNIT                                                                                                               AS PriceUnit
                           , SL.PRICEUNIT * ex.CrossRate                                                                                                AS PriceUnit_RCY
                           , SL.COSTPRICE                                                                                                               AS CostPrice
                           , SL.COSTPRICE * ex.CrossRate                                                                                                AS CostPrice_RCY
                           , SL.SALESPRICE                                                                                                              AS SalesPrice
                           , SL.SALESPRICE * ex.CrossRate                                                                                               AS SalesPrice_RCY
                           , SL.LINEDISC                                                                                                                AS Discount
                           , SL.LINEDISC * ex.CrossRate                                                                                                 AS Discount_RCY
                           , SL.LINEPERCENT                                                                                                             AS DiscountPercent
                           , SL.MULTILNDISC                                                                                                             AS MultilineDiscount
                           , SL.MULTILNDISC * ex.CrossRate                                                                                              AS MultilineDiscount_RCY
                           , SL.MULTILNPERCENT                                                                                                          AS MultilineDiscountPercent
                           , SL.SALESMARKUP                                                                                                             AS SalesCharges
                           , SL.SALESMARKUP * ex.CrossRate                                                                                              AS SalesCharges_RCY
                           , SL.LINEAMOUNT                                                                                                              AS NetAmount
                           , SL.LINEAMOUNT * ex.CrossRate                                                                                               AS NetAmount_RCY
                           , SL.QtyOrdered                                                                                                              AS InventoryQuantity
                           , SL.REMAININVENTPHYSICAL                                                                                                    AS InventoryQuantityOpen
                           , SL.REMAINSALESPHYSICAL                                                                                                     AS SalesQuantityOpen
             FROM            stage_ax.SALESLINE       AS SL
             LEFT OUTER JOIN stage_ax.SALESTABLE      ST ON SL.SALESID            = ST.SALESID
                                                        AND SL.DATAAREAID         = ST.DATAAREAID
                                                        AND SL.PARTITION          = ST.PARTITION
                                                        AND SL.data_connection_id = ST.data_connection_id
                                                        AND SL.company_id         = ST.company_id
             LEFT OUTER JOIN stage_ax.INVENTDIM       ID ON SL.INVENTDIMID        = ID.INVENTDIMID
                                                        AND SL.DATAAREAID         = ID.DATAAREAID
                                                        AND SL.PARTITION          = ID.PARTITION
                                                        AND SL.data_connection_id = ID.data_connection_id
                                                        AND SL.company_id         = ID.company_id
             LEFT JOIN       reporting_exchange_rates ex ON ex.FromCurrencyCode   = SL.CURRENCYCODE
                                                        AND ex.DataConnectionID   = SL.data_connection_id
                                                        AND SL.CREATEDDATETIME BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE
             /** FULL LOAD **/
                             ( @load_type        = 0
                           AND SL.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'N'
                           AND SL.execution_flag               = @execution_flag
                           AND SL.execution_timestamp          > @last_processed_timestamp)
               OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( SL.execution_flag           = @execution_flag
                             AND SL.execution_timestamp      > @last_processed_timestamp)
                            OR ( ST.execution_flag            = @execution_flag
                             AND ST.execution_timestamp       > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'D'
                           AND SL.execution_flag               = @execution_flag
                           AND SL.execution_timestamp          > @last_processed_timestamp)) ;
GO

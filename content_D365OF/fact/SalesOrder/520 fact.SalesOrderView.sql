EXEC dbo.drop_object @object = N'fact.SalesOrderView', @type = N'V' ;
GO
CREATE VIEW fact.SalesOrderView
AS
  SELECT            sl.StageId
                  , sl.ExecutionFlag
                  --	/***BK***/
                  , sl.InventTransCode
                  , sl.DataAreaCode
                  , sl.Partition
                  , sl.CreatedDate
                  , sl.CompanyID
                  , sl.DataConnectionID

                  --	 /***Dimensions*/
                  --	 --Dates
                  , COALESCE(cd.DateID, 0)                AS CreatedDateID
                  , COALESCE(rrd.DateID, 0)               AS ReceiptRequestedDateID
                  , COALESCE(srd.DateID, 0)               AS ShippingRequestedDateID
                  , COALESCE(rcd.DateID, 0)               AS ReceiptConfirmedDateID
                  , COALESCE(scd.DateID, 0)               AS ShippingConfirmedDateID
                  -- inventdimension
                  , COALESCE(wl.WMSLocationID, 0)         AS WMSlocationID
                  , COALESCE(l.LocationID, 0)             AS LocationID
                  , COALESCE(p.ProductID, 0)              AS ProductID
                  , COALESCE(t.TrackingID, 0)             AS TrackingID
                  --	--employee
                  , COALESCE(stEmp.EmployeeID, 0)         AS SalesTakerID
                  , COALESCE(srEmp.EmployeeID, 0)         AS SalesResponsibleID
                  --	 --customer
                  , COALESCE(cAcc.CustomerID, 0)          AS CustomerAccountID
                  , COALESCE(iAcc.CustomerID, 0)          AS InvoiceAccountID
                  --	-- address
                  , COALESCE(dAdr.AddressID, 0)           AS DeliveryAddressID
                  --	-- item
                  , COALESCE(i.ItemID, 0)                 AS ItemID
                  --	 -- currency
                  , COALESCE(cy.CurrencyID, 0)            AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)           AS CurrencyReportID
                  --	 -- financialdimension
                  , COALESCE(fd.FinancialDimensionsID, 0) AS FinancialDimensionsID
                  --	 -- reason code
                  , COALESCE(r.ReasonID, 0)               AS ReasonID
                  , COALESCE(sSts.SalesStatusID, 0)       AS SalesStatusID
                  , COALESCE(sot.SalesOrderTypeID, 0)     AS OrderTypeID
                  , COALESCE(sOrg.SalesOriginID, 0)       AS SalesOriginID
                  , COALESCE(sp.SalesPoolID, 0)           AS SalesPoolID
                  , COALESCE(sg.SalesGroupID, 0)          AS SalesGroupID
                  , COALESCE(modlv.ModeOfDeliveryID, 0)   AS ModeOfDeliveryID
                  , COALESCE(dt.DeliveryTermsID, 0)       AS DeliveryTermsID
                  , COALESCE(dtp.DeliveryTypeID, 0)       AS DeliveryTypeID
                  , COALESCE(mop.MethodOfPaymentID, 0)    AS MethodOfPaymentID
                  , COALESCE(pt.PaymentTermsID, 0)        AS PaymentTermsID
                  , COALESCE(so.SalesOrderID, 0)          AS SalesOrderID
                  , COALESCE(uom.UnitOfMeasureID, 0)      AS SalesUnitID
                  , COALESCE(sTax.TaxID, 0)               AS SalesTaxID

                  --	/***Measures */
                  , sl.SalesQuantity                      AS SalesQuantity
                  , sl.PriceUnit                          AS PriceUnit
                  , sl.PriceUnit_RCY                      AS PriceUnit_RCY
                  , sl.CostPrice                          AS CostPrice
                  , sl.CostPrice_RCY                      AS CostPrice_RCY
                  , sl.SalesPrice                         AS SalesPrice
                  , sl.SalesPrice_RCY                     AS SalesPrice_RCY
                  , sl.Discount                           AS Discount
                  , sl.Discount_RCY                       AS Discount_RCY
                  , sl.DiscountPercent                    AS DiscountPercent
                  , sl.MultilineDiscount                  AS MultilineDiscount
                  , sl.MultilineDiscount_RCY              AS MultilineDiscount_RCY
                  , sl.MultilineDiscountPercent           AS MultilineDiscountPercent
                  , sl.SalesCharges                       AS SalesCharges
                  , sl.SalesCharges_RCY                   AS SalesCharges_RCY
                  , sl.NetAmount                          AS NetAmount
                  , sl.NetAmount_RCY                      AS NetAmount_RCY
                  , sl.InventoryQuantity                  AS InventoryQuantity
                  , sl.InventoryQuantityOpen              AS InventoryQuantityOpen
                  , sl.SalesQuantityOpen                  AS SalesQuantityOpen


                  /*** Calculated Measures ***/
                  , CASE
                      WHEN sl.MultilineDiscountPercent <> 0
                       AND sl.MultilineDiscountPercent IS NOT NULL THEN (sl.NetAmount - sl.Discount) - ((sl.NetAmount - sl.Discount) / sl.MultilineDiscountPercent)
                      ELSE sl.NetAmount - sl.Discount -- no multiline discount
                    END                                   AS AmountInclTaxExclDiscount
    --SELECT COUNT(*)  -- 176308
    FROM            delta.SalesOrder        AS sl
    LEFT JOIN       dim.Date                AS cd ON cd.Date                     = sl.CreatedDate
    LEFT JOIN       dim.Date                AS rrd ON rrd.Date                   = sl.ReceiptRequestedDate
    LEFT JOIN       dim.Date                AS srd ON srd.Date                   = sl.ShippingRequestedDate
    LEFT JOIN       dim.Date                AS rcd ON rcd.Date                   = sl.ReceiptConfirmedDate
    LEFT JOIN       dim.Date                AS scd ON scd.Date                   = sl.ShippingConfirmedDate
    LEFT JOIN       dim.WMSLocation         AS wl ON wl.WMSLocationCode          = sl.WMSlocationCode
                                                 AND wl.LocationCode             = sl.LocationCode
                                                 AND wl.Partition                = sl.Partition
                                                 AND wl.DataConnectionID         = sl.DataConnectionID
                                                 AND wl.CompanyID                = sl.CompanyID
    LEFT JOIN       dim.Location            AS l ON l.LocationCode               = sl.LocationCode
                                                AND l.Partition                  = sl.Partition
                                                AND l.DataConnectionID           = sl.DataConnectionID
                                                AND l.CompanyID                  = sl.CompanyID
    LEFT JOIN       dim.Product             AS p ON p.InventSize                 = sl.ProductInventSizeCode
                                                AND p.InventColor                = sl.ProductColorCode
                                                AND p.InventStyle                = sl.ProductStyleCode
                                                AND p.InventConfiguration        = sl.ProductConfigCode
                                                AND p.Partition                  = sl.Partition
                                                AND p.DataConnectionID           = sl.DataConnectionID
                                                AND p.CompanyID                  = sl.CompanyID
    LEFT JOIN       dim.Tracking            AS t ON t.InventBatchCode            = sl.TrackingBatchCode
                                                AND t.InventSerialCode           = sl.TrackingSerialCode
                                                AND t.Partition                  = sl.Partition
                                                AND t.DataConnectionID           = sl.DataConnectionID
                                                AND t.CompanyID                  = sl.CompanyID
    LEFT JOIN       dim.Customer            AS cAcc ON cAcc.CustomerAccountNo    = sl.CustomerAccountCode
                                                   AND cAcc.Partition            = sl.Partition
                                                   AND cAcc.DataConnectionID     = sl.DataConnectionID
                                                   AND cAcc.CompanyID            = sl.CompanyID
    LEFT JOIN       dim.Customer            AS iAcc ON iAcc.CustomerAccountNo    = sl.InvoiceAccountCode
                                                   AND iAcc.Partition            = sl.Partition
                                                   AND iAcc.DataConnectionID     = sl.DataConnectionID
                                                   AND iAcc.CompanyID            = sl.CompanyID
    LEFT JOIN       dim.Address             AS dAdr ON dAdr.AddressRecID         = sl.DeliveryAddressCode
                                                   AND dAdr.Partition            = sl.Partition
                                                   AND dAdr.DataConnectionID     = sl.DataConnectionID
    LEFT JOIN       dim.Item                AS i ON i.ItemNumber                 = sl.ItemCode
                                                AND i.Partition                  = sl.Partition
                                                AND i.DataConnectionID           = sl.DataConnectionID
                                                AND i.CompanyID                  = sl.CompanyID
    LEFT JOIN       dim.FinancialDimensions AS fd ON fd.DefaultDimension         = sl.FinancialDimensionCode
                                                 AND fd.Partition                = sl.Partition
                                                 AND fd.DataConnectionID         = sl.DataConnectionID
    LEFT JOIN       dim.MethodOfPayment     AS mop ON mop.MethodOfPaymentCode    = sl.PaymentModeCode
                                                  AND mop.Partition              = sl.Partition
                                                  AND mop.DataConnectionID       = sl.DataConnectionID
                                                  AND mop.CompanyID              = sl.CompanyID
    LEFT JOIN       dim.PaymentTerms        AS pt ON pt.PaymentTermsCode         = sl.PaymentTermsCode
                                                 AND pt.Partition                = sl.Partition
                                                 AND pt.DataConnectionID         = sl.DataConnectionID
                                                 AND pt.CompanyID                = sl.CompanyID
    LEFT JOIN       dim.ModeOfDelivery      AS modlv ON modlv.ModeOfDeliveryCode = sl.DeliveryModeCode
                                                    AND modlv.Partition          = sl.Partition
                                                    AND modlv.DataConnectionID   = sl.DataConnectionID
                                                    AND modlv.CompanyID          = sl.CompanyID
    LEFT JOIN       dim.DeliveryTerms       AS dt ON dt.DeliveryTermsCode        = sl.DeliveryTermsCode
                                                 AND dt.Partition                = sl.Partition
                                                 AND dt.DataConnectionID         = sl.DataConnectionID
                                                 AND dt.CompanyID                = sl.CompanyID
    LEFT JOIN       dim.DeliveryType        AS dtp ON dtp.DeliveryTypeCode       = sl.DeliveryTypeCode
                                                  AND dtp.DataConnectionID       = sl.DataConnectionID
    LEFT JOIN       dim.SalesGroup          AS sg ON sg.SalesGroupCode           = sl.SalesGroupCode
                                                 AND sg.Partition                = sl.Partition
                                                 AND sg.DataConnectionID         = sl.DataConnectionID
                                                 AND sg.CompanyID                = sl.CompanyID
    LEFT JOIN       dim.SalesStatus         AS sSts ON sSts.SalesStatusCode      = sl.SalesStatusCode
                                                   AND sSts.DataConnectionID     = sl.DataConnectionID
    LEFT JOIN       dim.SalesOrigin         AS sOrg ON sOrg.SalesOriginCode      = sl.SalesOriginCode
                                                   AND sOrg.DataConnectionID     = sl.DataConnectionID
                                                   AND sOrg.CompanyID            = sl.CompanyID
    LEFT JOIN       dim.SalesOrder          AS so ON so.SalesOrder               = sl.SalesCode
                                                 AND so.Partition                = sl.Partition
                                                 AND so.DataConnectionID         = sl.DataConnectionID
                                                 AND so.CompanyID                = sl.CompanyID
    LEFT JOIN       dim.Tax                 AS sTax ON sTax.TaxItemGroupCode     = sl.SalesTaxItemGroupCode
                                                   AND sTax.TaxGroupCode         = sl.SalesTaxGroupCode
                                                   AND sTax.Partition            = sl.Partition
                                                   AND sTax.DataConnectionID     = sl.DataConnectionID
                                                   AND sTax.CompanyID            = sl.CompanyID
    LEFT JOIN       dim.SalesOrderType      AS sot ON sot.SalesOrderTypeCode     = sl.OrderTypeCode
                                                  AND sot.DataConnectionID       = sl.DataConnectionID
    LEFT JOIN       dim.SalesPool           AS sp ON sp.SalesPoolCode            = sl.SalesPoolCode
                                                 AND sp.Partition                = sl.Partition
                                                 AND sp.DataConnectionID         = sl.DataConnectionID
                                                 AND sp.CompanyID                = sl.CompanyID
    LEFT JOIN       dim.Currency            AS cy ON cy.Code                     = sl.CurrencyCode
                                                 AND cy.Partition                = sl.Partition
                                                 AND cy.DataConnectionID         = sl.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport      AS rcy ON rcy.Partition              = sl.Partition
    LEFT JOIN       dim.Reason              AS r ON r.ReasonRefRecId             = sl.CreditNoteReasonCode
                                                AND r.Partition                  = sl.Partition
                                                AND r.DataConnectionID           = sl.DataConnectionID
                                                AND r.CompanyID                  = sl.CompanyID
    LEFT JOIN       dim.Employee            AS stEmp ON stEmp.EmployeeRecID      = sl.SalesTakerCode
                                                    AND stEmp.Partition          = sl.Partition
                                                    AND stEmp.DataConnectionID   = sl.DataConnectionID
    LEFT JOIN       dim.Employee            AS srEmp ON srEmp.EmployeeRecID      = sl.SalesResponsibleCode
                                                    AND srEmp.Partition          = sl.Partition
                                                    AND srEmp.DataConnectionID   = sl.DataConnectionID
    LEFT JOIN       dim.UnitOfMeasure       AS uom ON uom.UnitOfMeasureCode      = sl.SalesUnitCode
                                                  AND uom.Partition              = sl.Partition
                                                  AND uom.DataConnectionID       = sl.DataConnectionID ;
GO



EXEC dbo.drop_object 'delta.SalesOrder', 'T' ;
GO
CREATE TABLE [delta].[SalesOrder]
(
  [InventTransCode]          [NVARCHAR](100)   NULL
, [DataAreaCode]             [NVARCHAR](10)    NULL
, [Partition]                [BIGINT]          NULL
, [CompanyID]                [INT]             NOT NULL
, [DataConnectionID]         [INT]             NOT NULL
, [ComponentExecutionID]     INT               NOT NULL
, [CreatedDate]              [DATE]            NULL
, [ReceiptRequestedDate]     [DATETIME]        NULL
, [ShippingRequestedDate]    [DATETIME]        NULL
, [ReceiptConfirmedDate]     [DATETIME]        NULL
, [ShippingConfirmedDate]    [DATETIME]        NULL
, [WMSlocationCode]          [NVARCHAR](100)   NULL
, [LocationCode]             [NVARCHAR](100)   NULL
, [ProductConfigCode]        [NVARCHAR](100)   NULL
, [ProductColorCode]         [NVARCHAR](100)   NULL
, [ProductInventSizeCode]    [NVARCHAR](100)   NULL
, [ProductStyleCode]         [NVARCHAR](100)   NULL
, [TrackingBatchCode]        [NVARCHAR](100)   NULL
, [TrackingSerialCode]       [NVARCHAR](100)   NULL
, [SalesTakerCode]           [BIGINT]          NULL
, [SalesResponsibleCode]     [BIGINT]          NULL
, [CustomerAccountCode]      [NVARCHAR](100)   NULL
, [InvoiceAccountCode]       [NVARCHAR](100)   NULL
, [CompanyCode]              [NVARCHAR](10)    NULL
, [ItemCode]                 [NVARCHAR](100)   NULL
, [CurrencyCode]             [NVARCHAR](10)    NULL
, [FinancialDimensionCode]   [BIGINT]          NULL
, [CreditNoteReasonCode]     [BIGINT]          NULL
, [DeliveryAddressCode]      [BIGINT]          NULL
, [SalesStatusCode]          [INT]             NULL
, [OrderTypeCode]            [INT]             NULL
, [SalesOriginCode]          [NVARCHAR](100)   NULL
, [SalesPoolCode]            [NVARCHAR](100)   NULL
, [SalesGroupCode]           [NVARCHAR](100)   NULL
, [DeliveryModeCode]         [NVARCHAR](100)   NULL
, [DeliveryTermsCode]        [NVARCHAR](100)   NULL
, [DeliveryTypeCode]         [NVARCHAR](100)   NULL
, [PaymentModeCode]          [NVARCHAR](100)   NULL
, [PaymentTermsCode]         [NVARCHAR](100)   NULL
, [SalesCode]                [NVARCHAR](100)   NULL
, [SalesUnitCode]            [NVARCHAR](100)   NULL
, [SalesTaxGroupCode]        [NVARCHAR](100)   NULL
, [SalesTaxItemGroupCode]    [NVARCHAR](100)   NULL
, [SalesQuantity]            DECIMAL(19,4)     NULL
, [PriceUnit]                DECIMAL(19,4)     NULL
, [PriceUnit_RCY]            DECIMAL(19,4)     NULL
, [CostPrice]                DECIMAL(19,4)     NULL
, [CostPrice_RCY]            DECIMAL(19,4)     NULL
, [SalesPrice]               DECIMAL(19,4)     NULL
, [SalesPrice_RCY]           DECIMAL(19,4)     NULL
, [Discount]                 DECIMAL(19,4)     NULL
, [Discount_RCY]             DECIMAL(19,4)     NULL
, [DiscountPercent]          DECIMAL(19,4)     NULL
, [MultilineDiscount]        DECIMAL(19,4)     NULL
, [MultilineDiscount_RCY]    DECIMAL(19,4)     NULL
, [MultilineDiscountPercent] DECIMAL(19,4)     NULL
, [SalesCharges]             DECIMAL(19,4)     NULL
, [SalesCharges_RCY]         DECIMAL(19,4)     NULL
, [NetAmount]                DECIMAL(19,4)     NULL
, [NetAmount_RCY]            DECIMAL(19,4)     NULL
, [InventoryQuantity]        DECIMAL(19,4)     NULL
, [InventoryQuantityOpen]    DECIMAL(19,4)     NULL
, [SalesQuantityOpen]        DECIMAL(19,4)     NULL
, [StageId]                  BIGINT            NOT NULL
, [ExecutionFlag]            NCHAR(1)          NOT NULL
) ;
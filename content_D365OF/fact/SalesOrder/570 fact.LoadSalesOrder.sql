EXEC dbo.drop_object @object = N'fact.LoadSalesOrder' -- nvarchar(128)
                   , @type = N'P'                     -- nchar(2)
                   , @debug = 0 ;                     -- int
GO

CREATE PROCEDURE fact.LoadSalesOrder
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  DECLARE @table_name sysname = N'SalesOrder' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.SalesOrder ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO fact.SalesOrder (
      InventTransCode
    , DataAreaCode
    , Partition
    , CreatedDate
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , CreatedDateID
    , ReceiptRequestedDateID
    , ShippingRequestedDateID
    , ReceiptConfirmedDateID
    , ShippingConfirmedDateID
    , WMSlocationID
    , LocationID
    , ProductID
    , TrackingID
    , SalesTakerID
    , SalesResponsibleID
    , CustomerAccountID
    , InvoiceAccountID
    , DeliveryAddressID
    , ItemID
    , CurrencyTransactionID
    , CurrencyReportID
    , FinancialDimensionsID
    , ReasonID
    , SalesStatusID
    , OrderTypeID
    , SalesOriginID
    , SalesPoolID
    , SalesGroupID
    , ModeOfDeliveryID
    , DeliveryTermsID
    , DeliveryTypeID
    , MethodOfPaymentID
    , PaymentTermsID
    , SalesOrderID
    , SalesUnitID
    , SalesTaxID
    , SalesQuantity
    , PriceUnit
    , PriceUnit_RCY
    , CostPrice
    , CostPrice_RCY
    , SalesPrice
    , SalesPrice_RCY
    , Discount
    , Discount_RCY
    , DiscountPercent
    , MultilineDiscount
    , MultilineDiscount_RCY
    , MultilineDiscountPercent
    , SalesCharges
    , SalesCharges_RCY
    , NetAmount
    , NetAmount_RCY
    , InventoryQuantity
    , InventoryQuantityOpen
    , SalesQuantityOpen
    , AmountInclTaxExclDiscount
    , StageID
    )
    SELECT InventTransCode
         , DataAreaCode
         , Partition
         , CreatedDate
         , CompanyID
         , DataConnectionID
         , @component_execution_id
         , CreatedDateID
         , ReceiptRequestedDateID
         , ShippingRequestedDateID
         , ReceiptConfirmedDateID
         , ShippingConfirmedDateID
         , WMSlocationID
         , LocationID
         , ProductID
         , TrackingID
         , SalesTakerID
         , SalesResponsibleID
         , CustomerAccountID
         , InvoiceAccountID
         , DeliveryAddressID
         , ItemID
         , CurrencyTransactionID
         , CurrencyReportID
         , FinancialDimensionsID
         , ReasonID
         , SalesStatusID
         , OrderTypeID
         , SalesOriginID
         , SalesPoolID
         , SalesGroupID
         , ModeOfDeliveryID
         , DeliveryTermsID
         , DeliveryTypeID
         , MethodOfPaymentID
         , PaymentTermsID
         , SalesOrderID
         , SalesUnitID
         , SalesTaxID
         , SalesQuantity
         , PriceUnit
         , PriceUnit_RCY
         , CostPrice
         , CostPrice_RCY
         , SalesPrice
         , SalesPrice_RCY
         , Discount
         , Discount_RCY
         , DiscountPercent
         , MultilineDiscount
         , MultilineDiscount_RCY
         , MultilineDiscountPercent
         , SalesCharges
         , SalesCharges_RCY
         , NetAmount
         , NetAmount_RCY
         , InventoryQuantity
         , InventoryQuantityOpen
         , SalesQuantityOpen
         , AmountInclTaxExclDiscount
         , StageID
      FROM fact.SalesOrderView ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.SalesOrder
         SET      InventTransCode = V.InventTransCode
                , DataAreaCode = V.DataAreaCode
                , Partition = V.Partition
                , CreatedDate = V.CreatedDate
                , CompanyID = V.CompanyID
                , DataConnectionID = V.DataConnectionID
                , ComponentExecutionID = @component_execution_id
                , CreatedDateID = V.CreatedDateID
                , ReceiptRequestedDateID = V.ReceiptRequestedDateID
                , ShippingRequestedDateID = V.ShippingRequestedDateID
                , ReceiptConfirmedDateID = V.ReceiptConfirmedDateID
                , ShippingConfirmedDateID = V.ShippingConfirmedDateID
                , WMSlocationID = V.WMSlocationID
                , LocationID = V.LocationID
                , ProductID = V.ProductID
                , TrackingID = V.TrackingID
                , SalesTakerID = V.SalesTakerID
                , SalesResponsibleID = V.SalesResponsibleID
                , CustomerAccountID = V.CustomerAccountID
                , InvoiceAccountID = V.InvoiceAccountID
                , DeliveryAddressID = V.DeliveryAddressID
                , ItemID = V.ItemID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , FinancialDimensionsID = V.FinancialDimensionsID
                , ReasonID = V.ReasonID
                , SalesStatusID = V.SalesStatusID
                , OrderTypeID = V.OrderTypeID
                , SalesOriginID = V.SalesOriginID
                , SalesPoolID = V.SalesPoolID
                , SalesGroupID = V.SalesGroupID
                , ModeOfDeliveryID = V.ModeOfDeliveryID
                , DeliveryTermsID = V.DeliveryTermsID
                , DeliveryTypeID = V.DeliveryTypeID
                , MethodOfPaymentID = V.MethodOfPaymentID
                , PaymentTermsID = V.PaymentTermsID
                , SalesOrderID = V.SalesOrderID
                , SalesUnitID = V.SalesUnitID
                , SalesTaxID = V.SalesTaxID
                , SalesQuantity = V.SalesQuantity
                , PriceUnit = V.PriceUnit
                , CostPrice = V.CostPrice
                , CostPrice_RCY = V.CostPrice_RCY
                , SalesPrice = V.SalesPrice
                , SalesPrice_RCY = V.SalesPrice_RCY
                , Discount = V.Discount
                , Discount_RCY = V.Discount_RCY
                , DiscountPercent = V.DiscountPercent
                , MultilineDiscount = V.MultilineDiscount
                , MultilineDiscount_RCY = V.MultilineDiscount_RCY
                , MultilineDiscountPercent = V.MultilineDiscountPercent
                , SalesCharges = V.SalesCharges
                , SalesCharges_RCY = V.SalesCharges_RCY
                , NetAmount = V.NetAmount
                , NetAmount_RCY = V.NetAmount_RCY
                , InventoryQuantity = V.InventoryQuantity
                , InventoryQuantityOpen = V.InventoryQuantityOpen
                , SalesQuantityOpen = V.SalesQuantityOpen
                , AmountInclTaxExclDiscount = V.AmountInclTaxExclDiscount
        FROM      fact.SalesOrder     F
       INNER JOIN fact.SalesOrderView AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.SalesOrder (
        InventTransCode
      , DataAreaCode
      , Partition
      , CreatedDate
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , CreatedDateID
      , ReceiptRequestedDateID
      , ShippingRequestedDateID
      , ReceiptConfirmedDateID
      , ShippingConfirmedDateID
      , WMSlocationID
      , LocationID
      , ProductID
      , TrackingID
      , SalesTakerID
      , SalesResponsibleID
      , CustomerAccountID
      , InvoiceAccountID
      , DeliveryAddressID
      , ItemID
      , CurrencyTransactionID
      , CurrencyReportID
      , FinancialDimensionsID
      , ReasonID
      , SalesStatusID
      , OrderTypeID
      , SalesOriginID
      , SalesPoolID
      , SalesGroupID
      , ModeOfDeliveryID
      , DeliveryTermsID
      , DeliveryTypeID
      , MethodOfPaymentID
      , PaymentTermsID
      , SalesOrderID
      , SalesUnitID
      , SalesTaxID
      , SalesQuantity
      , PriceUnit
      , PriceUnit_RCY
      , CostPrice
      , CostPrice_RCY
      , SalesPrice
      , SalesPrice_RCY
      , Discount
      , Discount_RCY
      , DiscountPercent
      , MultilineDiscount
      , MultilineDiscount_RCY
      , MultilineDiscountPercent
      , SalesCharges
      , SalesCharges_RCY
      , NetAmount
      , NetAmount_RCY
      , InventoryQuantity
      , InventoryQuantityOpen
      , SalesQuantityOpen
      , AmountInclTaxExclDiscount
      , StageID
      )
      SELECT V.InventTransCode
           , V.DataAreaCode
           , V.Partition
           , V.CreatedDate
           , V.CompanyID
           , V.DataConnectionID
           , @component_execution_id
           , V.CreatedDateID
           , V.ReceiptRequestedDateID
           , V.ShippingRequestedDateID
           , V.ReceiptConfirmedDateID
           , V.ShippingConfirmedDateID
           , V.WMSlocationID
           , V.LocationID
           , V.ProductID
           , V.TrackingID
           , V.SalesTakerID
           , V.SalesResponsibleID
           , V.CustomerAccountID
           , V.InvoiceAccountID
           , V.DeliveryAddressID
           , V.ItemID
           , V.CurrencyTransactionID
           , V.CurrencyReportID
           , V.FinancialDimensionsID
           , V.ReasonID
           , V.SalesStatusID
           , V.OrderTypeID
           , V.SalesOriginID
           , V.SalesPoolID
           , V.SalesGroupID
           , V.ModeOfDeliveryID
           , V.DeliveryTermsID
           , V.DeliveryTypeID
           , V.MethodOfPaymentID
           , V.PaymentTermsID
           , V.SalesOrderID
           , V.SalesUnitID
           , V.SalesTaxID
           , V.SalesQuantity
           , V.PriceUnit
           , V.PriceUnit_RCY
           , V.CostPrice
           , V.CostPrice_RCY
           , V.SalesPrice
           , V.SalesPrice_RCY
           , V.Discount
           , V.Discount_RCY
           , V.DiscountPercent
           , V.MultilineDiscount
           , V.MultilineDiscount_RCY
           , V.MultilineDiscountPercent
           , V.SalesCharges
           , V.SalesCharges_RCY
           , V.NetAmount
           , V.NetAmount_RCY
           , V.InventoryQuantity
           , V.InventoryQuantityOpen
           , V.SalesQuantityOpen
           , V.AmountInclTaxExclDiscount
           , V.StageID
        FROM fact.SalesOrderView V
       WHERE V.ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.SalesOrder F WHERE F.StageID = V.StageID) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.SalesOrder
       WHERE EXISTS ( SELECT 1
                        FROM fact.SalesOrderView
                       WHERE ExecutionFlag               = 'D'
                         AND [fact].[SalesOrder].StageID = StageID) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
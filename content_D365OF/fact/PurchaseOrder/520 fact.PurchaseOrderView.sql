EXEC dbo.drop_object @object = N'fact.PurchaseOrderView', @type = N'V' ;
GO
CREATE VIEW fact.PurchaseOrderView
AS
  SELECT            StageID                   = pl.StageID
                  , ExecutionFlag             = pl.ExecutionFlag
                  --	/***BK***/
                  , pl.InventTransCode
                  , pl.DataAreaCode
                  , pl.Partition
                  , pl.CreatedDate
                  , pl.CompanyID
                  , pl.DataConnectionID

                  --	 /***Dimensions*/
                  --Dates
                  , CreatedDateID             = COALESCE(cd.DateID, 0)
                  , ShippingRequestedDateID   = COALESCE(srd.DateID, 0)
                  , ShippingConfirmedDateID   = COALESCE(scd.DateID, 0)
                  , PurchaseLineTypeID        = COALESCE(plt.PurchaseTypeID, 0)
                  --Inventdimension
                  , WMSlocationID             = COALESCE(wl.WMSLocationID, 0)
                  , LocationID                = COALESCE(l.LocationID, 0)
                  , ProductID                 = COALESCE(p.ProductID, 0)
                  , TrackingID                = COALESCE(t.TrackingID, 0)
                  --Employee
                  , PurchasePlacerID          = COALESCE(stEmp.EmployeeID, 0)
                  , RequesterID               = COALESCE(reqEmp.EmployeeID, 0)
                  --Customer
                  , VendorAccountID           = COALESCE(cAcc.VendorID, 0)
                  , InvoiceAccountID          = COALESCE(iAcc.VendorID, 0)
                  --Address
                  , DeliveryAddressID         = COALESCE(dAdr.AddressID, 0)
                  --Item
                  , ItemID                    = COALESCE(i.ItemID, 0)
                  --Currency
                  , CurrencyTransactionID     = COALESCE(cy.CurrencyID, 0)
                  , CurrencyReportID          = COALESCE(rcy.CurrencyID, 0)
                  --Financialdimension
                  , FinancialDimensionsID     = COALESCE(fd.FinancialDimensionsID, 0)
                  , PurchaseStatusID          = COALESCE(sSts.PurchaseStatusID, 0)
                  , PurchaseTypeID            = COALESCE(sot.PurchaseTypeID, 0)
                  , PurchasePoolID            = COALESCE(sp.PurchasePoolID, 0)
                  , ModeOfDeliveryID          = COALESCE(modlv.ModeOfDeliveryID, 0)
                  , DeliveryTermsID           = COALESCE(dt.DeliveryTermsID, 0)
                  , DeliveryTypeID            = COALESCE(dtp.DeliveryTypeID, 0)
                  , MethodOfPaymentID         = COALESCE(mop.MethodOfPaymentID, 0)
                  , PaymentTermsID            = COALESCE(pt.PaymentTermsID, 0)
                  , PurchaseOrderID           = COALESCE(so.PurchaseOrderID, 0)
                  , PurchaseUnitID            = COALESCE(uom.UnitOfMeasureID, 0)

                  /***Measures */
                  , PurchaseQuantity          = pl.PurchaseQuantity
                  , PurchasePrice             = pl.PurchasePrice
                  , PurchasePrice_RCY         = pl.PurchasePrice_RCY
                  , Discount                  = pl.Discount
                  , Discount_RCY              = pl.Discount_RCY
                  , DiscountPercent           = pl.DiscountPercent
                  , MultilineDiscount         = pl.MultilineDiscount_RCY
                  , MultilineDiscount_RCY     = pl.MultilineDiscount
                  , MultilineDiscountPercent  = pl.MultilineDiscountPercent
                  , PurchaseCharges           = pl.PurchaseCharges
                  , PurchaseCharges_RCY       = pl.PurchaseCharges_RCY
                  , NetAmount                 = pl.NetAmount
                  , NetAmount_RCY             = pl.NetAmount_RCY
                  , InventoryQuantity         = pl.InventoryQuantity
                  , InventoryQuantityOpen     = pl.InventoryQuantityOpen
                  , PurchaseQuantityOpen      = pl.PurchaseQuantityOpen

                  /*** Calculated Measures ***/
                  , AmountInclTaxExclDiscount = CASE
                                                  WHEN pl.MultilineDiscountPercent <> 0
                                                   AND pl.MultilineDiscountPercent IS NOT NULL THEN (pl.NetAmount - pl.Discount) - ((pl.NetAmount - pl.Discount) / pl.MultilineDiscountPercent)
                                                  ELSE pl.NetAmount - pl.Discount -- no multiline discount
                                                END
    --  SELECT COUNT(*)
    FROM            delta.PurchaseOrder     AS pl
    LEFT JOIN       dim.Date                AS cd ON cd.Date                     = pl.CreatedDate
    LEFT JOIN       dim.Date                AS srd ON srd.Date                   = pl.ShippingRequestedDate
    LEFT JOIN       dim.Date                AS scd ON scd.Date                   = pl.ShippingConfirmedDate
    LEFT JOIN       dim.WMSLocation         AS wl ON wl.WMSLocationCode          = pl.WMSlocationCode
                                                 AND wl.LocationCode             = pl.LocationCode
                                                 AND wl.Partition                = pl.Partition
                                                 AND wl.DataConnectionID         = pl.DataConnectionID
                                                 AND pl.CompanyID                = wl.CompanyID
    LEFT JOIN       dim.Location            AS l ON l.LocationCode               = pl.LocationCode
                                                AND l.Partition                  = pl.Partition
                                                AND l.DataConnectionID           = pl.DataConnectionID
                                                AND l.CompanyID                  = pl.CompanyID
    LEFT JOIN       dim.Product             AS p ON p.InventSize                 = pl.ProductInventSizeCode
                                                AND p.InventColor                = pl.ProductColorCode
                                                AND p.InventStyle                = pl.ProductStyleCode
                                                AND p.InventConfiguration        = pl.ProductConfigCode
                                                AND p.Partition                  = pl.Partition
                                                AND p.DataConnectionID           = pl.DataConnectionID
                                                AND p.CompanyID                  = pl.CompanyID
    LEFT JOIN       dim.Tracking            AS t ON t.InventBatchCode            = pl.TrackingBatchCode
                                                AND t.InventSerialCode           = pl.TrackingSerialCode
                                                AND t.Partition                  = pl.Partition
                                                AND t.CompanyID                  = pl.CompanyID
                                                AND p.DataConnectionID           = pl.DataConnectionID
                                                AND p.CompanyID                  = pl.CompanyID
    LEFT JOIN       dim.Vendor              AS cAcc ON cAcc.VendorAccountNo      = pl.VendorAccountCode
                                                   AND cAcc.Partition            = pl.Partition
                                                   AND cAcc.DataConnectionID     = pl.DataConnectionID
                                                   AND cAcc.CompanyID            = pl.CompanyID
    LEFT JOIN       dim.Vendor              AS iAcc ON iAcc.VendorAccountNo      = pl.InvoiceAccountCode
                                                   AND iAcc.Partition            = pl.Partition
                                                   AND iAcc.DataConnectionID     = pl.DataConnectionID
                                                   AND iAcc.CompanyID            = pl.CompanyID
    LEFT JOIN       dim.Address             AS dAdr ON dAdr.AddressRecID         = pl.DeliveryAddressCode
                                                   AND dAdr.Partition            = pl.Partition
                                                   AND dAdr.DataConnectionID     = pl.DataConnectionID
    LEFT JOIN       dim.Item                AS i ON i.ItemNumber                 = pl.ItemCode
                                                AND l.Partition                  = pl.Partition
                                                AND i.DataConnectionID           = pl.DataConnectionID
                                                AND i.CompanyID                  = pl.CompanyID
    LEFT JOIN       dim.FinancialDimensions AS fd ON fd.DefaultDimension         = pl.FinancialDimensionCode
                                                 AND fd.Partition                = pl.Partition
                                                 AND fd.DataConnectionID         = pl.DataConnectionID
    LEFT JOIN       dim.MethodOfPayment     AS mop ON mop.MethodOfPaymentCode    = pl.PaymentModeCode
                                                  AND mop.Partition              = pl.Partition
                                                  AND mop.DataConnectionID       = pl.DataConnectionID
                                                  AND mop.CompanyID              = pl.CompanyID
    LEFT JOIN       dim.PaymentTerms        AS pt ON pt.PaymentTermsCode         = pl.PaymentTermsCode
                                                 AND pt.Partition                = pl.Partition
                                                 AND pt.DataConnectionID         = pl.DataConnectionID
                                                 AND pt.CompanyID                = pl.CompanyID
    LEFT JOIN       dim.ModeOfDelivery      AS modlv ON modlv.ModeOfDeliveryCode = pl.DeliveryModeCode
                                                    AND modlv.Partition          = pl.Partition
                                                    AND modlv.DataConnectionID   = pl.DataConnectionID
                                                    AND modlv.CompanyID          = pl.CompanyID
    LEFT JOIN       dim.DeliveryTerms       AS dt ON dt.DeliveryTermsCode        = pl.DeliveryTermsCode
                                                 AND dt.Partition                = pl.Partition
                                                 AND dt.DataConnectionID         = pl.DataConnectionID
                                                 AND dt.CompanyID                = pl.CompanyID
    LEFT JOIN       dim.DeliveryType        AS dtp ON dtp.DeliveryTypeCode       = pl.DeliveryTypeCode
                                                  AND dtp.DataConnectionID       = pl.DataConnectionID
    LEFT JOIN       dim.PurchaseStatus      AS sSts ON sSts.PurchaseStatusCode   = pl.PurchaseStatusCode
                                                   AND sSts.DataConnectionID     = pl.DataConnectionID
    LEFT JOIN       dim.PurchaseOrder       AS so ON so.PurchaseOrder            = pl.PurchaseCode
                                                 AND so.Partition                = pl.Partition
                                                 AND so.DataConnectionID         = pl.DataConnectionID
                                                 AND so.CompanyID                = pl.CompanyID
    LEFT JOIN       dim.PurchaseType        AS sot ON sot.PurchaseTypeCode       = pl.PurchaseTypeCode
                                                  AND sot.DataConnectionID       = pl.DataConnectionID
    LEFT JOIN       dim.PurchasePool        AS sp ON sp.PurchasePoolCode         = pl.PurchasePoolCode
                                                 AND sp.Partition                = pl.Partition
                                                 AND sp.DataConnectionID         = pl.DataConnectionID
                                                 AND sp.CompanyID                = pl.CompanyID
    LEFT JOIN       dim.Currency            AS cy ON cy.Code                     = pl.CurrencyCode
                                                 AND cy.Partition                = pl.Partition
                                                 AND cy.DataConnectionID         = pl.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport      rcy ON rcy.Partition                 = pl.Partition
    LEFT JOIN       dim.Employee            AS stEmp ON stEmp.EmployeeRecID      = pl.PurchasePlacerCode
                                                    AND stEmp.Partition          = pl.Partition
                                                    AND stEmp.DataConnectionID   = pl.DataConnectionID
    LEFT JOIN       dim.Employee            AS reqEmp ON reqEmp.EmployeeRecID    = pl.Requester
                                                     AND reqEmp.Partition        = pl.Partition
                                                     AND reqEmp.DataConnectionID = pl.DataConnectionID
    LEFT JOIN       dim.UnitOfMeasure       AS uom ON uom.UnitOfMeasureCode      = pl.PurchaseUnitCode
                                                  AND uom.Partition              = pl.Partition
                                                  AND uom.DataConnectionID       = pl.DataConnectionID
    LEFT JOIN       dim.PurchaseType        AS plt ON plt.PurchaseTypeCode       = pl.PurchaseLineTypeCode
                                                  AND plt.DataConnectionID       = pl.DataConnectionID ;
GO


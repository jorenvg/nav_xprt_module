EXEC dbo.drop_object 'delta.PurchaseOrderFunction', 'F' ;
GO
CREATE FUNCTION delta.PurchaseOrderFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            PL.stage_id                                      AS StageId
                           , @execution_flag                                  AS ExecutionFlag
                           , PL.company_id                                    AS CompanyID
                           , PL.data_connection_id                            AS DataConnectionID
                           , @component_execution_id                          AS ComponentExecutionId
                           , PL.INVENTTRANSID                                 AS InventTransCode
                           , PL.DATAAREAID                                    AS DataAreaCode
                           , PL.PARTITION                                     AS Partition
                           , CAST(PL.CREATEDDATETIME AS DATE)                 AS CreatedDate
                           , PL.SHIPPINGDATEREQUESTED                         AS ShippingRequestedDate
                           , PL.SHIPPINGDATECONFIRMED                         AS ShippingConfirmedDate
                           , PL.PURCHASETYPE                                  AS PurchaseLineTypeCode
                           , COALESCE(NULLIF(ID.WMSLOCATIONID, ''), 'N/A')    AS WMSlocationCode
                           , COALESCE(NULLIF(ID.INVENTLOCATIONID, ''), 'N/A') AS LocationCode
                           , COALESCE(NULLIF(ID.CONFIGID, ''), 'N/A')         AS ProductConfigCode
                           , COALESCE(NULLIF(ID.INVENTCOLORID, ''), 'N/A')    AS ProductColorCode
                           , COALESCE(NULLIF(ID.INVENTSIZEID, ''), 'N/A')     AS ProductInventSizeCode
                           , COALESCE(NULLIF(ID.INVENTSTYLEID, ''), 'N/A')    AS ProductStyleCode
                           , COALESCE(NULLIF(ID.INVENTBATCHID, ''), 'N/A')    AS TrackingBatchCode
                           , COALESCE(NULLIF(ID.INVENTSERIALID, ''), 'N/A')   AS TrackingSerialCode
                           , PT.WORKERPURCHPLACER                             AS PurchasePlacerCode
                           , PT.REQUESTER                                     AS Requester
                           , PT.ORDERACCOUNT                                  AS VendorAccountCode
                           , PT.INVOICEACCOUNT                                AS InvoiceAccountCode
                           , PL.DATAAREAID                                    AS CompanyCode
                           , PL.ITEMID                                        AS ItemCode
                           , PL.CURRENCYCODE                                  AS CurrencyCode
                           , PL.DEFAULTDIMENSION                              AS FinancialDimensionCode
                           , PL.DELIVERYPOSTALADDRESS                         AS DeliveryAddressCode
                           , PL.PURCHSTATUS                                   AS PurchaseStatusCode
                           , PT.PURCHASETYPE                                  AS PurchaseTypeCode
                           , PT.PURCHPOOLID                                   AS PurchasePoolCode
                           , PT.DLVMODE                                       AS DeliveryModeCode
                           , PT.DLVTERM                                       AS DeliveryTermsCode
                           , PL.DELIVERYTYPE                                  AS DeliveryTypeCode
                           , PT.PAYMMODE                                      AS PaymentModeCode
                           , PT.PAYMENT                                       AS PaymentTermsCode
                           , PL.PURCHID                                       AS PurchaseCode
                           , PL.PURCHUNIT                                     AS PurchaseUnitCode
                           , PL.TAXGROUP                                      AS PurchaseTaxGroupCode
                           , PL.TAXITEMGROUP                                  AS PurchaseTaxItemGroupCode
                           , PL.PURCHQTY                                      AS PurchaseQuantity
                           , PL.PRICEUNIT                                     AS PriceUnit
                           , PL.PRICEUNIT * ex.CrossRate                      AS PriceUnit_RCY
                           , PL.PURCHPRICE                                    AS PurchasePrice
                           , PL.PURCHPRICE * ex.CrossRate                     AS PurchasePrice_RCY
                           , PL.LINEDISC                                      AS Discount
                           , PL.LINEDISC * ex.CrossRate                       AS Discount_RCY
                           , PL.LINEPERCENT                                   AS DiscountPercent
                           , PL.MULTILNDISC                                   AS MultilineDiscount
                           , PL.MULTILNDISC * ex.CrossRate                    AS MultilineDiscount_RCY
                           , PL.MULTILNPERCENT                                AS MultilineDiscountPercent
                           , PL.PURCHMARKUP                                   AS PurchaseCharges
                           , PL.PURCHMARKUP * ex.CrossRate                    AS PurchaseCharges_RCY
                           , PL.LINEAMOUNT                                    AS NetAmount
                           , PL.LINEAMOUNT * ex.CrossRate                     AS NetAmount_RCY
                           , PL.QtyOrdered                                    AS InventoryQuantity
                           , PL.REMAININVENTPHYSICAL                          AS InventoryQuantityOpen
                           , PL.REMAINPURCHPHYSICAL                           AS PurchaseQuantityOpen
             FROM            stage_ax.PURCHLINE       AS PL
             LEFT OUTER JOIN stage_ax.PURCHTABLE      PT ON PL.PURCHID            = PT.PURCHID
                                                        AND PL.data_connection_id = PT.data_connection_id
                                                        AND PL.company_id         = PT.company_id
                                                        AND PL.PARTITION          = PT.PARTITION
             LEFT OUTER JOIN stage_ax.INVENTDIM       ID ON PL.INVENTDIMID        = ID.INVENTDIMID
                                                        AND PL.data_connection_id = ID.data_connection_id
                                                        AND PL.company_id         = ID.company_id
                                                        AND PL.PARTITION          = ID.PARTITION
             LEFT JOIN       reporting_exchange_rates ex ON ex.FromCurrencyCode   = PL.CURRENCYCODE
                                                        AND ex.DataConnectionID   = PL.data_connection_id
                                                        AND PL.CREATEDDATETIME BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE
             /** FULL LOAD **/
                             ( @load_type        = 0
                           AND PL.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'N'
                           AND PL.execution_flag               = @execution_flag
                           AND PL.execution_timestamp          > @last_processed_timestamp)
               OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( PL.execution_flag           = @execution_flag
                             AND PL.execution_timestamp      > @last_processed_timestamp)
                            OR ( PT.execution_flag            = @execution_flag
                             AND PT.execution_timestamp       > @last_processed_timestamp)
                            OR ( ID.execution_flag          = @execution_flag
                             AND ID.execution_timestamp     > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'D'
                           AND PL.execution_flag               = @execution_flag
                           AND PL.execution_timestamp          > @last_processed_timestamp)) ;
GO

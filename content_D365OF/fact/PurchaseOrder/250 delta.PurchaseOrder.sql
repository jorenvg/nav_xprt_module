EXEC dbo.drop_object 'delta.PurchaseOrder', 'T' ;
GO
CREATE TABLE [delta].[PurchaseOrder]
(
  StageId                    [BIGINT]          NOT NULL
, ExecutionFlag              [NCHAR](1)        NOT NULL
, [CompanyID]                [INT]             NOT NULL
, [DataConnectionID]         [INT]             NOT NULL
, ComponentExecutionId       [INT]             NOT NULL
, [InventTransCode]          [NVARCHAR](100)   NULL
, [DataAreaCode]             [NVARCHAR](10)    NULL
, [Partition]                [BIGINT]          NULL
, [CreatedDate]              [DATE]            NULL
, [ShippingRequestedDate]    [DATETIME]        NULL
, [ShippingConfirmedDate]    [DATETIME]        NULL
, [PurchaseLineTypeCode]     [INT]             NULL
, [WMSlocationCode]          [NVARCHAR](100)   NULL
, [LocationCode]             [NVARCHAR](100)   NULL
, [ProductConfigCode]        [NVARCHAR](100)   NULL
, [ProductColorCode]         [NVARCHAR](100)   NULL
, [ProductInventSizeCode]    [NVARCHAR](100)   NULL
, [ProductStyleCode]         [NVARCHAR](100)   NULL
, [TrackingBatchCode]        [NVARCHAR](100)   NULL
, [TrackingSerialCode]       [NVARCHAR](100)   NULL
, [PurchasePlacerCode]       [BIGINT]          NULL
, [Requester]                [BIGINT]          NULL
, [VendorAccountCode]        [NVARCHAR](100)   NULL
, [InvoiceAccountCode]       [NVARCHAR](100)   NULL
, [CompanyCode]              [NVARCHAR](10)    NULL
, [ItemCode]                 [NVARCHAR](100)   NULL
, [CurrencyCode]             [NVARCHAR](10)    NULL
, [FinancialDimensionCode]   [BIGINT]          NULL
, [DeliveryAddressCode]      [BIGINT]          NULL
, [PurchaseStatusCode]       [INT]             NULL
, [PurchaseTypeCode]         [INT]             NULL
, [PurchasePoolCode]         [NVARCHAR](100)   NULL
, [DeliveryModeCode]         [NVARCHAR](100)   NULL
, [DeliveryTermsCode]        [NVARCHAR](100)   NULL
, [DeliveryTypeCode]         [NVARCHAR](100)   NULL
, [PaymentModeCode]          [NVARCHAR](100)   NULL
, [PaymentTermsCode]         [NVARCHAR](100)   NULL
, [PurchaseCode]             [NVARCHAR](100)   NULL
, [PurchaseUnitCode]         [NVARCHAR](100)   NULL
, [PurchaseTaxGroupCode]     [NVARCHAR](100)   NULL
, [PurchaseTaxItemGroupCode] [NVARCHAR](100)   NULL
, [PurchaseQuantity]         DECIMAL(19,4)     NULL
, [PriceUnit]                DECIMAL(19,4)     NULL
, [PriceUnit_RCY]            DECIMAL(19,4)     NULL
, [PurchasePrice]            DECIMAL(19,4)     NULL
, [PurchasePrice_RCY]        DECIMAL(19,4)     NULL
, [Discount]                 DECIMAL(19,4)     NULL
, [Discount_RCY]             DECIMAL(19,4)     NULL
, [DiscountPercent]          DECIMAL(19,4)     NULL
, [MultilineDiscount]        DECIMAL(19,4)     NULL
, [MultilineDiscount_RCY]    DECIMAL(19,4)     NULL
, [MultilineDiscountPercent] DECIMAL(19,4)     NULL
, [PurchaseCharges]          DECIMAL(19,4)     NULL
, [PurchaseCharges_RCY]      DECIMAL(19,4)     NULL
, [NetAmount]                DECIMAL(19,4)     NULL
, [NetAmount_RCY]            DECIMAL(19,4)     NULL
, [InventoryQuantity]        DECIMAL(19,4)     NULL
, [InventoryQuantityOpen]    DECIMAL(19,4)     NULL
, [PurchaseQuantityOpen]     DECIMAL(19,4)     NULL
) ;
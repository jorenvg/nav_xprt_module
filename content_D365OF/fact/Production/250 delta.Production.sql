EXEC dbo.drop_object 'delta.Production', 'T' ;
GO
CREATE TABLE [delta].[Production]
(
  StageId                   BIGINT          NOT NULL
, ExecutionFlag             NCHAR(20)       NOT NULL
, ComponentExecutionId      INT             NOT NULL
, [ProductionRecId]         [BIGINT]        NULL
, [Partition]               [BIGINT]        NULL
, [DataConnectionID]        [INT]           NULL
, [CompanyId]               [INT]           NULL
, [ItemCode]                NVARCHAR(60)    NULL
, [ProductionOrderCode]     NVARCHAR(512)   NULL
, [RouteCode]               NVARCHAR(80)    NULL
, [OperationCode]           NVARCHAR(80)    NULL
, [ConsumptionType]         NVARCHAR(512)   NULL
, [ResourceCode]            NVARCHAR(120)   NULL
, [CostGroupCode]           NVARCHAR(200)   NULL
, [UnitOfMeasureCode]       NVARCHAR(100)   NULL
, [TransactionDate]         DATETIME        NULL
, [DeliveryDate]            DATETIME        NULL
, [ScheduledStartDate]      DATETIME        NULL
, [ScheduledEndDate]        DATETIME        NULL
, [CreatedDate]             DATETIME        NULL
, [EstimatedDate]           DATETIME        NULL
, [ScheduledDate]           DATETIME        NULL
, [StartedDate]             DATETIME        NULL
, [ReportAsFinishedDate]    DATETIME        NULL
, [EndedDate]               DATETIME        NULL
, [LocationCode]            NVARCHAR(512)   NULL
, [TrackingBatchCode]       NVARCHAR(512)   NULL
, [TrackingSerialCode]      NVARCHAR(512)   NULL
, [WMSLocationCode]         NVARCHAR(512)   NULL
, [ProductConfigCode]       NVARCHAR(512)   NULL
, [ProductColorCode]        NVARCHAR(512)   NULL
, [ProductInventSizeCode]   NVARCHAR(512)   NULL
, [ProductStyleCode]        NVARCHAR(512)   NULL
, [CalculationTypeCode]     INT             NULL
--Measures
, [EstimatedCostMST]        DECIMAL(19,4)   NULL
, [EstimatedCostRCY]        DECIMAL(19,4)   NULL
, [EstimatedQuantity]       DECIMAL(19,4)   NULL
, [EstimatedHours]          DECIMAL(19,4)   NULL
, [EstimatedMarkUpMST]      DECIMAL(19,4)   NULL
, [EstimatedMarkUpRCY]      DECIMAL(19,4)   NULL
, [ActualCostMST]           DECIMAL(19,4)   NULL
, [ActualCostRCY]           DECIMAL(19,4)   NULL
, [ActualQuantity]          DECIMAL(19,4)   NULL
, [ActualCostAdjustmentMST] DECIMAL(19,4)   NULL
, [ActualCostAdjustmentRCY] DECIMAL(19,4)   NULL
, [ActualMarkUpMST]         DECIMAL(19,4)   NULL
, [ActualMarkUpRCY]         DECIMAL(19,4)   NULL
, [ActualHours]             DECIMAL(19,4)   NULL
) ;

EXEC dbo.drop_object 'fact.Production', 'T' ;
GO
CREATE TABLE [fact].[Production]
(
  [StageId]                     [BIGINT]        NOT NULL
, [Partition]                   [BIGINT]        NULL
, [CompanyID]                   [INT]           NOT NULL
, [DataConnectionID]            [INT]           NOT NULL
, [ComponentExecutionID]        [INT]           NOT NULL
, ExecutionFlag                 NCHAR(40)       NOT NULL
, [ProductionRecId]             [BIGINT]        NULL
, [ItemID]                      [INT]           NULL
, [ProductionOrderID]           [INT]           NULL
, [RouteOperationID]            [INT]           NULL
, [ConsumptionID]               [INT]           NULL
, [CostGroupID]                 [INT]           NULL
, [UnitOfMeasureID]             [INT]           NULL
, [ProductionCalculationTypeID] [INT]           NULL
, [TransactionDateID]           [INT]           NULL
, [DeliveryDateID]              [INT]           NULL
, [ScheduledStartDateID]        [INT]           NULL
, [ScheduledEndDateID]          [INT]           NULL
, [CreatedDateID]               [INT]           NULL
, [EstimatedDateID]             [INT]           NULL
, [ScheduledDateID]             [INT]           NULL
, [StartedDateID]               [INT]           NULL
, [ReportedAsFinishedDateID]    [INT]           NULL
, [EndedDateID]                 [INT]           NULL
, [LocationID]                  [INT]           NULL
, [TrackingID]                  [INT]           NULL
, [WMSLocationID]               [INT]           NULL
, [ProductID]                   [INT]           NULL
--Measures
, [EstimatedCostMST]            DECIMAL(19,4)   NULL
, [EstimatedCostRCY]            DECIMAL(19,4)   NULL
, [EstimatedQuantity]           DECIMAL(19,4)   NULL
, [EstimatedHours]              DECIMAL(19,4)   NULL
, [EstimatedMarkUpMST]          DECIMAL(19,4)   NULL
, [EstimatedMarkUpRCY]          DECIMAL(19,4)   NULL
, [ActualCostMST]               DECIMAL(19,4)   NULL
, [ActualCostRCY]               DECIMAL(19,4)   NULL
, [ActualQuantity]              DECIMAL(19,4)   NULL
, [ActualCostAdjustmentMST]     DECIMAL(19,4)   NULL
, [ActualCostAdjustmentRCY]     DECIMAL(19,4)   NULL
, [ActualMarkUpMST]             DECIMAL(19,4)   NULL
, [ActualMarkUpRCY]             DECIMAL(19,4)   NULL
, [ActualHours]                 DECIMAL(19,4)   NULL
) ;


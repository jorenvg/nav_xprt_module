EXEC Dbo.Drop_Object @Object = N'fact.ProductionView', @Type = N'V' ;
GO
CREATE VIEW fact.ProductionView
AS
  SELECT            StageID                                      = DP.StageID
                  , ExecutionFlag                                = DP.ExecutionFlag
                  --	/***BK***/
                  , DP.Partition
                  , DP.CompanyId
                  , DP.ProductionRecId
                  , DP.DataConnectionId
                  , DP.ComponentExecutionId
                  --Dates
                  , COALESCE(TD.DateId, 0)                       AS TransactionDateID
                  , COALESCE(DD.DateId, 0)                       AS DeliveryDateID
                  , COALESCE(SSD.DateId, 0)                      AS ScheduledStartDateID
                  , COALESCE(SED.DateId, 0)                      AS ScheduledEndDateID
                  , COALESCE(CD.DateId, 0)                       AS CreatedDateID
                  , COALESCE(EST.DateId, 0)                      AS EstimatedDateID
                  , COALESCE(SD.DateId, 0)                       AS ScheduledDateID
                  , COALESCE(STD.DateId, 0)                      AS StartedDateID
                  , COALESCE(RPD.DateId, 0)                      AS ReportedAsFinishedDateID
                  , COALESCE(ED.DateId, 0)                       AS EndedDateID
                  --	 /***Dimensions*/
                  --Production
                  , COALESCE(I.ItemID, 0)                        AS ItemID
                  , COALESCE(PO.ProductionOrderID, 0)            AS ProductionOrderID
                  , COALESCE(RO.RouteOperationID, 0)             AS RouteOperationID
                  , COALESCE(C.ConsumptionID, 0)                 AS ConsumptionID
                  , COALESCE(CG.CostGroupID, 0)                  AS CostGroupID
                  , COALESCE(UOM.UnitOfMeasureID, 0)             AS UnitOfMeasureID
                  , COALESCE(PCT.ProductionCalculationTypeId, 0) AS ProductionCalculationTypeID
                  --Invent Dim Dimensions                      
                  , COALESCE(PROD.ProductId, 0)                  AS ProductID
                  , COALESCE(T.TrackingId, 0)                    AS TrackingID
                  , COALESCE(LOC.LocationID, 0)                  AS LocationID
                  , COALESCE(WL.WMSlocationID, 0)                AS WMSlocationID
                  /***Measures */
                  , COALESCE(DP.EstimatedCostMST, 0)             AS EstimatedCostMST
                  , COALESCE(DP.EstimatedCostRCY, 0)             AS EstimatedCostRCY
                  , COALESCE(DP.EstimatedQuantity, 0)            AS EstimatedQuantity
                  , COALESCE(DP.EstimatedHours, 0)               AS EstimatedHours
                  , COALESCE(DP.EstimatedMarkUpMST, 0)           AS EstimatedMarkUpMST
                  , COALESCE(DP.EstimatedMarkUpRCY, 0)           AS EstimatedMarkUpRCY
                  , COALESCE(DP.ActualCostMST, 0)                AS ActualCostMST
                  , COALESCE(DP.ActualCostRCY, 0)                AS ActualCostRCY
                  , COALESCE(DP.ActualQuantity, 0)               AS ActualQuantity
                  , COALESCE(DP.ActualCostAdjustmentMST, 0)      AS ActualCostAdjustmentMST
                  , COALESCE(DP.ActualCostAdjustmentRCY, 0)      AS ActualCostAdjustmentRCY
                  , COALESCE(DP.ActualMarkUpMST, 0)              AS ActualMarkUpMST
                  , COALESCE(DP.ActualMarkUpRCY, 0)              AS ActualMarkUpRCY
                  , COALESCE(DP.ActualHours, 0)                  AS ActualHours
    -- select COUNT(*) 
    FROM            Delta.Production              AS DP
    LEFT OUTER JOIN Dim.Date                      AS TD ON TD.Date                    = DP.TransactionDate
    LEFT OUTER JOIN Dim.Date                      AS DD ON DD.Date                    = DP.DeliveryDate
    LEFT OUTER JOIN Dim.Date                      AS SSD ON SSD.Date                  = DP.ScheduledStartDate
    LEFT OUTER JOIN Dim.Date                      AS SED ON SED.Date                  = DP.ScheduledEndDate
    LEFT OUTER JOIN Dim.Date                      AS CD ON CD.Date                    = DP.CreatedDate
    LEFT OUTER JOIN Dim.Date                      AS EST ON EST.Date                  = DP.EstimatedDate
    LEFT OUTER JOIN Dim.Date                      AS SD ON SD.Date                    = DP.ScheduledDate
    LEFT OUTER JOIN Dim.Date                      AS STD ON STD.Date                  = DP.StartedDate
    LEFT OUTER JOIN Dim.Date                      AS RPD ON RPD.Date                  = DP.ReportAsFinishedDate
    LEFT OUTER JOIN Dim.Date                      AS ED ON ED.Date                    = DP.EndedDate
    LEFT OUTER JOIN Dim.Item                      AS I ON I.ItemNumber                = DP.ItemCode
                                                      AND I.Partition                 = DP.Partition
                                                      AND I.DataConnectionId          = DP.DataConnectionId
                                                      AND I.CompanyId                 = DP.CompanyId
    LEFT OUTER JOIN Dim.ProductionOrder           AS PO ON PO.ProductionOrder         = DP.ProductionOrderCode
                                                       AND PO.Partition               = DP.Partition
                                                       AND PO.DataConnectionId        = DP.DataConnectionId
                                                       AND PO.CompanyId               = DP.CompanyId
    LEFT OUTER JOIN Dim.RouteOperation            AS RO ON RO.RouteCode               = DP.RouteCode
                                                       AND RO.OperationCode           = DP.OperationCode
                                                       AND RO.Partition               = DP.Partition
                                                       AND RO.DataConnectionId        = DP.DataConnectionId
                                                       AND RO.CompanyID               = DP.CompanyId
    LEFT OUTER JOIN Dim.Consumption               AS C ON C.ResourceCode              = DP.ResourceCode
                                                      AND C.ConsumptionType           = DP.ConsumptionType
                                                      AND C.CompanyId                 = DP.CompanyId
                                                      AND C.Partition                 = DP.Partition
                                                      AND C.DataConnectionId          = DP.DataConnectionId
    LEFT OUTER JOIN Dim.CostGroup                 AS CG ON CG.CostGroupCode           = DP.CostGroupCode
                                                       AND CG.Partition               = DP.Partition
                                                       AND CG.DataConnectionId        = DP.DataConnectionId
                                                       AND CG.CompanyId               = DP.CompanyId
    LEFT OUTER JOIN Dim.UnitOfMeasure             AS UOM ON UOM.UnitOfMeasureCode     = DP.UnitOfMeasureCode
                                                        AND UOM.Partition             = DP.Partition
                                                        AND UOM.DataConnectionId      = DP.DataConnectionId
    LEFT OUTER JOIN dim.ProductionCalculationType AS PCT ON PCT.CalculationTypeCode   = DP.CalculationTypeCode
                                                        AND PCT.DataConnectionId      = DP.DataConnectionId
    LEFT OUTER JOIN Dim.Product                   AS PROD ON PROD.InventSize          = DP.ProductInventSizeCode
                                                         AND PROD.InventColor         = DP.ProductColorCode
                                                         AND PROD.InventsTyle         = DP.ProductsTyleCode
                                                         AND PROD.InventConfiguration = DP.ProductConFigCode
                                                         AND PROD.Partition           = DP.Partition
                                                         AND PROD.DataConnectionId    = DP.DataConnectionId
                                                         AND PROD.CompanyId           = DP.CompanyId
    LEFT OUTER JOIN Dim.Tracking                  AS T ON T.InventBatchCode           = DP.TrackingBatchCode
                                                      AND T.InventSerialCode          = DP.TrackingSerialCode
                                                      AND T.Partition                 = DP.Partition
                                                      AND T.DataConnectionId          = DP.DataConnectionId
                                                      AND T.CompanyId                 = DP.CompanyId
    LEFT OUTER JOIN Dim.Location                  AS LOC ON LOC.LocationCode          = DP.LocationCode
                                                        AND LOC.Partition             = DP.Partition
                                                        AND LOC.DataConnectionId      = DP.DataConnectionId
                                                        AND LOC.CompanyId             = DP.CompanyId
    LEFT OUTER JOIN Dim.Wmslocation               AS WL ON WL.Wmslocationcode         = DP.Wmslocationcode
                                                       AND WL.LocationCode            = DP.LocationCode
                                                       AND WL.Partition               = DP.Partition
                                                       AND WL.DataConnectionId        = DP.DataConnectionId
                                                       AND WL.CompanyId               = DP.CompanyId ;
GO


EXEC dbo.drop_object @object = N'dim.LoadMarkupType' -- nvarchar(128)
                   , @type = N'P'                    -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadMarkupType
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.MarkupType
     SET      StageID = smtv.StageID
            , MarkupDescription = smtv.MarkupDescription
            , ChargesModule = smtv.ChargesModule
            , ComponentExecutionID = @component_execution_id
    FROM      dim.MarkupType     AS smt
   INNER JOIN dim.MarkupTypeView AS smtv ON smt.ChargesModuleCode = smtv.ChargesModuleCode
                                        AND smtv.MarkupCode       = smt.MarkupCode
                                        AND smt.PARTITION         = smtv.PARTITION
                                        AND smt.DataConnectionID  = smtv.DataConnectionID
                                        AND smt.CompanyID         = smtv.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.MarkupType (
    StageID, DataConnectionID, CompanyID, Partition, ComponentExecutionID, MarkupCode, MarkupDescription, ChargesModuleCode, ChargesModule
  )
  SELECT smtv.StageID
       , smtv.DataConnectionID
       , smtv.CompanyID
       , smtv.Partition
       , @component_execution_id
       , smtv.MarkupCode
       , smtv.MarkupDescription
       , smtv.ChargesModuleCode
       , smtv.ChargesModule
    FROM dim.MarkupTypeView AS smtv
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.MarkupType AS smt
                       WHERE smt.ChargesModuleCode = smtv.ChargesModuleCode
                         AND smt.MarkupCode        = smtv.MarkupCode
                         AND smt.DataConnectionID  = smtv.DataConnectionID
                         AND smt.CompanyID         = smtv.CompanyID
                         AND smt.PARTITION         = smtv.PARTITION) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
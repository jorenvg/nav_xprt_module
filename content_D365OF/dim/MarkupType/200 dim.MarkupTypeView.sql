EXEC dbo.drop_object @object = N'dim.MarkupTypeView', @type = N'V' ;
GO
CREATE VIEW dim.MarkupTypeView
AS
  SELECT      m.MARKUPCODE --BKey                                
            , ISNULL(NULLIF(m.TXT, ''), 'N/A')          AS MarkupDescription
            , ISNULL(NULLIF(m.MODULETYPE, ''), -1)      AS ChargesModuleCode
            , ISNULL(NULLIF(cm.DESCRIPTION, ''), 'N/A') AS ChargesModule
            , m.PARTITION  --BKey                                            
            , m.data_connection_id                      AS DataConnectionID
            , m.company_id                              AS CompanyID
            , m.stage_id                                AS StageID
    FROM      stage_ax.MARKUPTABLE AS m
    LEFT JOIN meta.enumerations    cm ON m.ModuleType    = cm.OptionID
                                     AND cm.[TableName]  = 'MarkupTable'
                                     AND cm.[ColumnName] = 'ModuleType' ;
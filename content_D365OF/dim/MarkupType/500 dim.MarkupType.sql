EXEC dbo.drop_object 'dim.MarkupType', 'T' ;
GO
CREATE TABLE [dim].[MarkupType]
(
  MarkupTypeID         INT             IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT          NULL
, ComponentExecutionID INT             NOT NULL
, CompanyID            INT             NOT NULL
, DataConnectionID     INT             NOT NULL
, [MarkupCode]         [NVARCHAR](30)  NOT NULL
, [MarkupDescription]  [NVARCHAR](128) NOT NULL
, [ChargesModuleCode]  [INT]           NOT NULL
, [ChargesModule]      [NVARCHAR](255) NOT NULL
, [PARTITION]          [BIGINT]        NOT NULL
) ;

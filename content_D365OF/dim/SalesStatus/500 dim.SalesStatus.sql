EXEC dbo.drop_object 'dim.SalesStatus', 'T' ;

CREATE TABLE dim.SalesStatus
(
  SalesStatusID        INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, SalesStatusCode      INT           NULL
, SalesStatusDesc      NVARCHAR(128) NULL
) ;



EXEC dbo.drop_object @object = N'dim.LoadPostingLayer' -- nvarchar(128)
                   , @type = N'P'                      -- nchar(2)
                   , @debug = 0 ;                      -- int
GO

CREATE PROCEDURE [dim].[LoadPostingLayer]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.PostingLayer
     SET      PostingLayerDesc = PLV.PostingLayerDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.PostingLayer     PL
   INNER JOIN dim.PostingLayerView PLV ON PLV.PostingLayerCode = PL.PostingLayerCode
                                      AND PLV.DataConnectionID = PL.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.PostingLayer (
    DataConnectionID, ComponentExecutionID, PostingLayerCode, PostingLayerDesc
  )
  SELECT PLV.DataConnectionID
       , COALESCE(@component_execution_id, -1)
       , PLV.PostingLayerCode
       , PLV.PostingLayerDesc
    FROM dim.PostingLayerView PLV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.PostingLayer PL
                       WHERE PLV.PostingLayerCode = PL.PostingLayerCode
                         AND PLV.DataConnectionID = PL.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
GO




-- ################################################################################ --
-- #####                         Table: dim.PurchasePackingSlip               ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.PurchasePackingSlip', 'T' ;

CREATE TABLE dim.PurchasePackingSlip
(
  PurchasePackingSlipID INT          IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID      INT          NOT NULL
, CompanyID             INT          NOT NULL
, Partition             BIGINT       NOT NULL
, ExecutionTimestamp    ROWVERSION   NOT NULL
, ComponentExecutionID  INT          NOT NULL
, PurchasePackingSlip   NVARCHAR(50) NOT NULL
) ;
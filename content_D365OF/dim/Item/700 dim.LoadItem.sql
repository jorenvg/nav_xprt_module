EXEC dbo.drop_object @object = N'dim.LoadItem' -- nvarchar(128)
                   , @type = N'P'              -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadItem]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Item
     SET      StageID = ITV.StageID
            , ProductName = ITV.ProductName
            , ProductNo = ITV.ProductNo
            , ProductNoName = ITV.ProductNoName
            , StorageDimensionGroupCode = ITV.StorageDimensionGroupCode
            , StorageDimensionGroupDesc = ITV.StorageDimensionGroupDesc
            , StorageDimensionGroupCodeDesc = ITV.StorageDimensionGroupCodeDesc
            , ItemModelGroupCode = ITV.ItemModelGroupCode
            , ItemModelGroupDesc = ITV.ItemModelGroupDesc
            , ItemModelGroupCodeDesc = ITV.ItemModelGroupCodeDesc
            , UnitOfMeasureCode = ITV.UnitOfMeasureCode
            , UnitOfMeasureDesc = ITV.UnitOfMeasureDesc
            , UnitOfMeasureCodeDesc = ITV.UnitOfMeasureCodeDesc
            , VendorCode = ITV.VendorCode
            , VendorDesc = ITV.VendorDesc
            , VendorCodeDesc = ITV.VendorCodeDesc
            , CommissionGroupCode = ITV.CommissionGroupCode
            , CommissionGroupDesc = ITV.CommissionGroupDesc
            , CommissionGroupCodeDesc = ITV.CommissionGroupCodeDesc
            , ItemRebateGroupCode = ITV.ItemRebateGroupCode
            , ItemRebateGroupDesc = ITV.ItemRebateGroupDesc
            , ItemRebateGroupCodeDesc = ITV.ItemRebateGroupCodeDesc
            , CountryOfOriginCode = ITV.CountryOfOriginCode
            , CountryOfOriginDesc = ITV.CountryOfOriginDesc
            , CountryOfOriginCodeDesc = ITV.CountryOfOriginCodeDesc
            , StateOfOriginCode = ITV.StateOfOriginCode
            , StateOfOriginDesc = ITV.StateOfOriginDesc
            , StateOfOriginCodeDesc = ITV.StateOfOriginCodeDesc
            , ItemGroupCode = ITV.ItemGroupCode
            , ItemGroupDesc = ITV.ItemGroupDesc
            , ItemGroupCodeDesc = ITV.ItemGroupCodeDesc
            , CostGroupCode = ITV.CostGroupCode
            , CostGroupDesc = ITV.CostGroupDesc
            , CostGroupCodeDesc = ITV.CostGroupCodeDesc
            , ProjectCategoryGroupCode = ITV.ProjectCategoryGroupCode
            , ProjectCategoryGroupDesc = ITV.ProjectCategoryGroupDesc
            , ProjectCategoryGroupCodeDesc = ITV.ProjectCategoryGroupCodeDesc
            , ABCCarryingCostGroupCode = ITV.ABCCarryingCostGroupCode
            , ABCCarryingCostGroupDesc = ITV.ABCCarryingCostGroupDesc
            , ABCMarginGroupCode = ITV.ABCMarginGroupCode
            , ABCMarginGroupDesc = ITV.ABCMarginGroupDesc
            , ABCRevenueGroupCode = ITV.ABCRevenueGroupCode
            , ABCRevenueGroupDesc = ITV.ABCRevenueGroupDesc
            , ABCValueGroupCode = ITV.ABCValueGroupCode
            , ABCValueGroupDesc = ITV.ABCValueGroupDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Item     IT
   INNER JOIN dim.ItemView ITV ON ITV.ItemNumber       = IT.ItemNumber
                              AND ITV.DataConnectionID = IT.DataConnectionID
                              AND ITV.Partition        = IT.Partition
                              AND ITV.CompanyID        = IT.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Item (
    StageID
  , DataConnectionID
  , ComponentExecutionID
  , CompanyID
  , Partition
  , ItemNumber
  , ProductName
  , ProductNo
  , ProductNoName
  , StorageDimensionGroupCode
  , StorageDimensionGroupDesc
  , StorageDimensionGroupCodeDesc
  , ItemModelGroupCode
  , ItemModelGroupDesc
  , ItemModelGroupCodeDesc
  , UnitOfMeasureCode
  , UnitOfMeasureDesc
  , UnitOfMeasureCodeDesc
  , VendorCode
  , VendorDesc
  , VendorCodeDesc
  , CommissionGroupCode
  , CommissionGroupDesc
  , CommissionGroupCodeDesc
  , ItemRebateGroupCode
  , ItemRebateGroupDesc
  , ItemRebateGroupCodeDesc
  , CountryOfOriginCode
  , CountryOfOriginDesc
  , CountryOfOriginCodeDesc
  , StateOfOriginCode
  , StateOfOriginDesc
  , StateOfOriginCodeDesc
  , ItemGroupCode
  , ItemGroupDesc
  , ItemGroupCodeDesc
  , CostGroupCode
  , CostGroupDesc
  , CostGroupCodeDesc
  , ProjectCategoryGroupCode
  , ProjectCategoryGroupDesc
  , ProjectCategoryGroupCodeDesc
  , ABCCarryingCostGroupCode
  , ABCCarryingCostGroupDesc
  , ABCMarginGroupCode
  , ABCMarginGroupDesc
  , ABCRevenueGroupCode
  , ABCRevenueGroupDesc
  , ABCValueGroupCode
  , ABCValueGroupDesc
  )
  SELECT ITV.StageID
       , ITV.DataConnectionID
       , @component_execution_id
       , ITV.CompanyID
       , ITV.Partition
       , ITV.ItemNumber
       , ITV.ProductName
       , ITV.ProductNo
       , ITV.ProductNoName
       , ITV.StorageDimensionGroupCode
       , ITV.StorageDimensionGroupDesc
       , ITV.StorageDimensionGroupCodeDesc
       , ITV.ItemModelGroupCode
       , ITV.ItemModelGroupDesc
       , ITV.ItemModelGroupCodeDesc
       , ITV.UnitOfMeasureCode
       , ITV.UnitOfMeasureDesc
       , ITV.UnitOfMeasureCodeDesc
       , ITV.VendorCode
       , ITV.VendorDesc
       , ITV.VendorCodeDesc
       , ITV.CommissionGroupCode
       , ITV.CommissionGroupDesc
       , ITV.CommissionGroupCodeDesc
       , ITV.ItemRebateGroupCode
       , ITV.ItemRebateGroupDesc
       , ITV.ItemRebateGroupCodeDesc
       , ITV.CountryOfOriginCode
       , ITV.CountryOfOriginDesc
       , ITV.CountryOfOriginCodeDesc
       , ITV.StateOfOriginCode
       , ITV.StateOfOriginDesc
       , ITV.StateOfOriginCodeDesc
       , ITV.ItemGroupCode
       , ITV.ItemGroupDesc
       , ITV.ItemGroupCodeDesc
       , ITV.CostGroupCode
       , ITV.CostGroupDesc
       , ITV.CostGroupCodeDesc
       , ITV.ProjectCategoryGroupCode
       , ITV.ProjectCategoryGroupDesc
       , ITV.ProjectCategoryGroupCodeDesc
       , ITV.ABCCarryingCostGroupCode
       , ITV.ABCCarryingCostGroupDesc
       , ITV.ABCMarginGroupCode
       , ITV.ABCMarginGroupDesc
       , ITV.ABCRevenueGroupCode
       , ITV.ABCRevenueGroupDesc
       , ITV.ABCValueGroupCode
       , ITV.ABCValueGroupDesc
    FROM dim.ItemView ITV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Item IT
                       WHERE ITV.ItemNumber       = IT.ItemNumber
                         AND ITV.DataConnectionID = IT.DataConnectionID
                         AND ITV.Partition        = IT.Partition
                         AND ITV.CompanyID        = IT.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
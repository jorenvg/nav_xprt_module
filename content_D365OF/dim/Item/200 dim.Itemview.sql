EXEC dbo.drop_object 'dim.ItemView', 'V' ;
GO

CREATE VIEW [dim].[ItemView]
AS
  SELECT
    /** SYSTEM COLUMNS **/
              it.stage_id                                                                                          AS StageID
            , it.company_id                                                                                        AS CompanyID
            , it.PARTITION                                                                                         AS Partition
            , it.data_connection_id                                                                                AS DataConnectionID

            /** BUSINESS KEY **/
            , ISNULL(NULLIF(it.ITEMID, ''), 'N/A')                                                                 AS ItemNumber

            /** OTHER FIELDS **/
            , ISNULL(NULLIF(pt.NAME, ''), 'N/A')                                                                   AS ProductName
            , ISNULL(NULLIF(ep.DISPLAYPRODUCTNUMBER, ''), 'N/A')                                                   AS ProductNo
            , ISNULL(NULLIF(ep.DISPLAYPRODUCTNUMBER, '') + ' - ', '') + ISNULL(NULLIF(pt.NAME, ''), 'N/A')         AS ProductNoName
            , ISNULL(NULLIF(sdg.NAME, ''), 'N/A')                                                                  AS StorageDimensionGroupCode
            , ISNULL(NULLIF(sdg.DESCRIPTION, ''), 'N/A')                                                           AS StorageDimensionGroupDesc
            , ISNULL(NULLIF(sdg.NAME, '') + ' - ', '') + ISNULL(NULLIF(sdg.DESCRIPTION, ''), 'N/A')                AS StorageDimensionGroupCodeDesc
            , ISNULL(NULLIF(img.MODELGROUPID, ''), 'N/A')                                                          AS ItemModelGroupCode
            , ISNULL(NULLIF(img.NAME, ''), 'N/A')                                                                  AS ItemModelGroupDesc
            , ISNULL(NULLIF(img.MODELGROUPID, '') + ' - ', '') + ISNULL(NULLIF(img.NAME, ''), 'N/A')               AS ItemModelGroupCodeDesc
            , ISNULL(NULLIF(it.BOMUNITID, ''), 'N/A')                                                              AS UnitOfMeasureCode
            , ISNULL(NULLIF(umt.DESCRIPTION, ''), 'N/A')                                                           AS UnitOfMeasureDesc
            , ISNULL(NULLIF(it.BOMUNITID, '') + ' - ', '') + ISNULL(NULLIF(umt.DESCRIPTION, ''), 'N/A')            AS UnitOfMeasureCodeDesc
            , ISNULL(NULLIF(it.PRIMARYVENDORID, ''), 'N/A')                                                        AS VendorCode
            , ISNULL(NULLIF(dpt.NAME, ''), 'N/A')                                                                  AS VendorDesc
            , ISNULL(NULLIF(it.PRIMARYVENDORID, '') + ' - ', '') + ISNULL(NULLIF(dpt.NAME, ''), 'N/A')             AS VendorCodeDesc
            , ISNULL(NULLIF(it.COMMISSIONGROUPID, ''), 'N/A')                                                      AS CommissionGroupCode
            , ISNULL(NULLIF(ccg.NAME, ''), 'N/A')                                                                  AS CommissionGroupDesc
            , ISNULL(NULLIF(it.COMMISSIONGROUPID, '') + ' - ', '') + ISNULL(NULLIF(ccg.NAME, ''), 'N/A')           AS CommissionGroupCodeDesc
            , ISNULL(NULLIF(it.PDSITEMREBATEGROUPID, ''), 'N/A')                                                   AS ItemRebateGroupCode
            , ISNULL(NULLIF(itg.DESCRIPTION, ''), 'N/A')                                                           AS ItemRebateGroupDesc
            , ISNULL(NULLIF(it.PDSITEMREBATEGROUPID, '') + ' - ', '') + ISNULL(NULLIF(itg.DESCRIPTION, ''), 'N/A') AS ItemRebateGroupCodeDesc
            , ISNULL(NULLIF(it.ORIGCOUNTRYREGIONID, ''), 'N/A')                                                    AS CountryOfOriginCode
            , ISNULL(NULLIF(crt.SHORTNAME, ''), 'N/A')                                                             AS CountryOfOriginDesc
            , ISNULL(NULLIF(it.ORIGCOUNTRYREGIONID, '') + ' - ', '') + ISNULL(NULLIF(crt.SHORTNAME, ''), 'N/A')    AS CountryOfOriginCodeDesc
            , ISNULL(NULLIF(it.ORIGSTATEID, ''), 'N/A')                                                            AS StateOfOriginCode
            , ISNULL(NULLIF(las.NAME, ''), 'N/A')                                                                  AS StateOfOriginDesc
            , ISNULL(NULLIF(it.ORIGSTATEID, '') + ' - ', '') + ISNULL(NULLIF(las.NAME, ''), 'N/A')                 AS StateOfOriginCodeDesc
            , ISNULL(NULLIF(itig.ITEMGROUPID, ''), 'N/A')                                                          AS ItemGroupCode
            , ISNULL(NULLIF(itig.NAME, ''), 'N/A')                                                                 AS ItemGroupDesc
            , ISNULL(NULLIF(itig.ITEMGROUPID, '') + ' - ', '') + ISNULL(NULLIF(itig.NAME, ''), 'N/A')              AS ItemGroupCodeDesc
            , ISNULL(NULLIF(it.COSTGROUPID, ''), 'N/A')                                                            AS CostGroupCode
            , ISNULL(NULLIF(bcg.NAME, ''), 'N/A')                                                                  AS CostGroupDesc
            , ISNULL(NULLIF(it.COSTGROUPID, '') + ' - ', '') + ISNULL(NULLIF(bcg.NAME, ''), 'N/A')                 AS CostGroupCodeDesc
            , ISNULL(NULLIF(it.PROJCATEGORYID, ''), 'N/A')                                                         AS ProjectCategoryGroupCode
            , ISNULL(NULLIF(ct.CATEGORYNAME, ''), 'N/A')                                                           AS ProjectCategoryGroupDesc
            , ISNULL(NULLIF(it.PROJCATEGORYID, '') + ' - ', '') + ISNULL(NULLIF(ct.CATEGORYNAME, ''), 'N/A')       AS ProjectCategoryGroupCodeDesc
            , ISNULL(enum_cc.OptionID, -1)                                                                         AS ABCCarryingCostGroupCode
            , ISNULL(NULLIF(enum_cc.Description, ''), 'N/A')                                                       AS ABCCarryingCostGroupDesc
            , ISNULL(enum_mar.OptionID, -1)                                                                        AS ABCMarginGroupCode
            , ISNULL(NULLIF(enum_mar.Description, ''), 'N/A')                                                      AS ABCMarginGroupDesc
            , ISNULL(enum_rev.OptionID, -1)                                                                        AS ABCRevenueGroupCode
            , ISNULL(NULLIF(enum_mar.Description, ''), 'N/A')                                                      AS ABCRevenueGroupDesc
            , ISNULL(enum_val.OptionID, -1)                                                                        AS ABCValueGroupCode
            , ISNULL(NULLIF(enum_val.Description, ''), 'N/A')                                                      AS ABCValueGroupDesc
    FROM      stage_ax.INVENTTABLE  AS it
   CROSS JOIN meta.current_instance AS ci
    LEFT JOIN stage_ax.ECORESPRODUCTTRANSLATION                 AS pt ON it.PRODUCT                     = pt.PRODUCT
                                                                     AND pt.LANGUAGEID                  = ci.language_description
                                                                     AND it.data_connection_id          = pt.data_connection_id
                                                                     AND it.PARTITION                   = pt.PARTITION
    LEFT JOIN stage_ax.ECORESPRODUCT                            AS ep ON it.PRODUCT                     = ep.RECID
                                                                     AND it.data_connection_id          = ep.data_connection_id
                                                                     AND it.PARTITION                   = ep.PARTITION
    LEFT JOIN stage_ax.ECORESSTORAGEDIMENSIONGROUPPRODUCT       AS dgp ON it.PRODUCT                    = dgp.PRODUCT
                                                                      AND it.data_connection_id         = dgp.data_connection_id
                                                                      AND it.PARTITION                  = dgp.PARTITION
    LEFT JOIN stage_ax.ECORESSTORAGEDIMENSIONGROUP              AS sdg ON dgp.STORAGEDIMENSIONGROUP     = sdg.RECID
                                                                      AND it.data_connection_id         = sdg.data_connection_id
                                                                      AND it.PARTITION                  = sdg.PARTITION
    LEFT JOIN stage_ax.INVENTMODELGROUPITEM                     AS mgi ON it.ITEMID                     = mgi.ITEMID
                                                                      AND it.DATAAREAID                 = mgi.ITEMDATAAREAID
                                                                      AND it.data_connection_id         = mgi.data_connection_id
                                                                      AND it.PARTITION                  = mgi.PARTITION
    LEFT JOIN stage_ax.INVENTMODELGROUP                         AS img ON mgi.MODELGROUPID              = img.MODELGROUPID
                                                                      AND mgi.MODELGROUPDATAAREAID      = img.DATAAREAID
                                                                      AND it.data_connection_id         = img.data_connection_id
                                                                      AND it.PARTITION                  = img.PARTITION
    LEFT JOIN stage_ax.UNITOFMEASURE                            AS uom ON it.BOMUNITID                  = uom.SYMBOL
                                                                      AND it.data_connection_id         = uom.data_connection_id
                                                                      AND it.PARTITION                  = uom.PARTITION
    LEFT JOIN stage_ax.UNITOFMEASURETRANSLATION                 AS umt ON uom.RECID                     = umt.UNITOFMEASURE
                                                                      AND umt.LANGUAGEID                = ci.language_description
                                                                      AND it.data_connection_id         = umt.data_connection_id
                                                                      AND it.PARTITION                  = umt.PARTITION
    LEFT JOIN stage_ax.VENDTABLE                                AS vt ON it.PRIMARYVENDORID             = vt.ACCOUNTNUM
                                                                     AND it.company_id                  = vt.company_id
                                                                     AND it.data_connection_id          = vt.data_connection_id
                                                                     AND it.PARTITION                   = vt.PARTITION
    LEFT JOIN stage_ax.DIRPARTYTABLE                            AS dpt ON vt.PARTY                      = dpt.RECID
                                                                      AND it.data_connection_id         = dpt.data_connection_id
                                                                      AND it.PARTITION                  = dpt.PARTITION
    LEFT JOIN stage_ax.COMMISSIONCUSTOMERGROUP                  AS ccg ON it.COMMISSIONGROUPID          = ccg.GROUPID
                                                                      AND it.company_id                 = ccg.company_id
                                                                      AND it.data_connection_id         = ccg.data_connection_id
                                                                      AND it.PARTITION                  = ccg.PARTITION
    LEFT JOIN stage_ax.PDSITEMREBATEGROUP                       AS itg ON it.PDSITEMREBATEGROUPID       = itg.PDSITEMREBATEGROUPID
                                                                      AND it.data_connection_id         = itg.data_connection_id
                                                                      AND it.PARTITION                  = itg.PARTITION
                                                                      AND it.company_id                 = itg.company_id
    LEFT JOIN stage_ax.LOGISTICSADDRESSCOUNTRYREGION            AS cr ON it.ORIGCOUNTRYREGIONID         = cr.COUNTRYREGIONID
                                                                     AND it.data_connection_id          = cr.data_connection_id
                                                                     AND it.PARTITION                   = cr.PARTITION
    LEFT JOIN stage_ax.LOGISTICSADDRESSCOUNTRYREGIONTRANSLATION AS crt ON cr.COUNTRYREGIONID            = crt.COUNTRYREGIONID
                                                                      AND it.data_connection_id         = crt.data_connection_id
                                                                      AND it.PARTITION                  = crt.PARTITION
                                                                      AND crt.LANGUAGEID                = ci.language_description
    LEFT JOIN stage_ax.LOGISTICSADDRESSSTATE                    AS las ON it.ORIGSTATEID                = las.STATEID
                                                                      AND it.data_connection_id         = las.data_connection_id
                                                                      AND it.PARTITION                  = las.PARTITION
                                                                      AND it.ORIGCOUNTRYREGIONID        = las.COUNTRYREGIONID
    LEFT JOIN stage_ax.INVENTITEMGROUPITEM                      AS igi ON it.ITEMID                     = igi.ITEMID
                                                                      AND it.DATAAREAID                 = igi.ITEMDATAAREAID
                                                                      AND it.data_connection_id         = igi.data_connection_id
                                                                      AND it.PARTITION                  = igi.PARTITION
    LEFT JOIN stage_ax.INVENTITEMGROUP                          AS itig ON igi.ITEMGROUPID              = itig.ITEMGROUPID
                                                                       AND igi.ITEMGROUPDATAAREAID      = itig.DATAAREAID
                                                                       AND it.data_connection_id        = itig.data_connection_id
                                                                       AND it.PARTITION                 = itig.PARTITION
    LEFT JOIN stage_ax.BOMCOSTGROUP                             AS bcg ON it.COSTGROUPID                = bcg.COSTGROUPID
                                                                      AND it.company_id                 = bcg.company_id
                                                                      AND it.data_connection_id         = bcg.data_connection_id
                                                                      AND it.PARTITION                  = bcg.PARTITION
    LEFT JOIN stage_ax.CATEGORYTABLE                            AS ct ON it.PROJCATEGORYID              = ct.CATEGORYID
                                                                     AND it.company_id                  = ct.company_id
                                                                     AND it.data_connection_id          = ct.data_connection_id
                                                                     AND it.PARTITION                   = ct.PARTITION
    LEFT JOIN meta.enumerations                                 AS enum_cc ON it.ABCTIEUP               = enum_cc.OptionID
                                                                          AND enum_cc.TableName         = 'INVENTTABLE'
                                                                          AND enum_cc.ColumnName        = 'ABCTIEUP'
    LEFT JOIN meta.enumerations                                 AS enum_mar ON it.ABCCONTRIBUTIONMARGIN = enum_mar.OptionID
                                                                           AND enum_mar.TableName       = 'INVENTTABLE'
                                                                           AND enum_mar.ColumnName      = 'ABCCONTRIBUTIONMARGIN'
    LEFT JOIN meta.enumerations                                 AS enum_rev ON it.ABCREVENUE            = enum_rev.OptionID
                                                                           AND enum_rev.TableName       = 'INVENTTABLE'
                                                                           AND enum_rev.ColumnName      = 'ABCREVENUE'
    LEFT JOIN meta.enumerations                                 AS enum_val ON it.ABCVALUE              = enum_val.OptionID
                                                                           AND enum_val.TableName       = 'INVENTTABLE'
                                                                           AND enum_val.ColumnName      = 'ABCVALUE' ;

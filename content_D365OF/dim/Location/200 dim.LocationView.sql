EXEC dbo.drop_object 'dim.LocationView', 'V' ;
GO

CREATE VIEW [dim].[LocationView]
AS
  SELECT            ISNULL(NULLIF(INS.SITEID, ''), 'N/A')                                                      AS SiteCode
                  , ISNULL(NULLIF(INS.NAME, ''), 'N/A')                                                        AS SiteDesc
                  , ISNULL(NULLIF(INS.SITEID, '') + ' - ', '') + ISNULL(NULLIF(INS.NAME, ''), 'N/A')           AS SiteCodeDesc
                  , ISNULL(NULLIF(INL.INVENTLOCATIONID, ''), 'N/A')                                            AS LocationCode
                  , ISNULL(NULLIF(INL.NAME, ''), 'N/A')                                                        AS LocationDesc
                  , ISNULL(NULLIF(INL.INVENTLOCATIONID, '') + ' - ', '') + ISNULL(NULLIF(INL.NAME, ''), 'N/A') AS LocationCodeDesc
                  , INL.company_id                                                                             AS CompanyID
                  , INL.data_connection_id                                                                     AS DataConnectionID
                  , INL.PARTITION                                                                              AS Partition
                  , INL.stage_id                                                                               AS StageID
    FROM            stage_ax.INVENTLOCATION INL
    LEFT OUTER JOIN stage_ax.INVENTSITE     INS ON INS.SITEID     = INL.INVENTSITEID
                                               AND INL.PARTITION  = INS.PARTITION
                                               AND INS.DATAAREAID = INL.DATAAREAID ;




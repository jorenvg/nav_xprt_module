EXEC dbo.drop_object @object = N'dim.LoadStatusIssue' -- nvarchar(128)
                   , @type = N'P'                     -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadStatusIssue]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.StatusIssue
     SET      StatusIssueDesc = SOTV.StatusIssueDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.StatusIssue     SOT
   INNER JOIN dim.StatusIssueView SOTV ON SOTV.StatusIssueCode  = SOT.StatusIssueCode
                                      AND SOTV.DataConnectionID = SOT.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.StatusIssue (
    DataConnectionID, ComponentExecutionID, StatusIssueCode, StatusIssueDesc
  )
  SELECT SOTV.DataConnectionID
       , @component_execution_id
       , SOTV.StatusIssueCode
       , SOTV.StatusIssueDesc
    FROM dim.StatusIssueView SOTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.StatusIssue SOT
                       WHERE SOTV.StatusIssueCode  = SOT.StatusIssueCode
                         AND SOTV.DataConnectionID = SOT.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
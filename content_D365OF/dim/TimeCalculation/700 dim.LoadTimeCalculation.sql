EXEC dbo.drop_object @object = N'dim.LoadTimeCalculation' -- nvarchar(128)
                   , @type = N'P'                         -- nchar(2)
                   , @debug = 0 ;                         -- int
GO

CREATE PROCEDURE dim.LoadTimeCalculation
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE dim.TimeCalculation
     SET TimeCalculationDesc = vw.[DESC]
       , TimeCalculationDesc_NL = vw.DESC_NL
    FROM dim.TimeCalculation     AS tbl
    JOIN dim.TimeCalculationView vw ON tbl.TimeCalculationCode = vw.Code ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT dim.TimeCalculation (
    TimeCalculationCode, TimeCalculationDesc, TimeCalculationDesc_NL
  )
  SELECT Code
       , [DESC]
       , Desc_NL
    FROM dim.TimeCalculationView vw
   WHERE NOT EXISTS (SELECT 1 FROM dim.TimeCalculation AS tbl WHERE tbl.TimeCalculationCode = vw.Code) ;

  SELECT @inserted = @@ROWCOUNT ;

END ;

GO


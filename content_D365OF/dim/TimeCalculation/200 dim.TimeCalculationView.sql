EXEC dbo.drop_object @object = N'dim.TimeCalculationView', @type = N'V' ;
GO
CREATE VIEW dim.TimeCalculationView
AS
  SELECT Code = 'CP', [DESC] = 'Current Period', [DESC_NL] = 'Geselecteerde periode'
  UNION
  SELECT Code = 'PLY', [DESC] = 'Same Period Last Year', [DESC_NL] = 'Dezelfde Periode Vorig jaar'
  UNION
  SELECT Code = 'YTD', [DESC] = 'Year to Date', [DESC_NL] = 'Jaar tot Dag'
  UNION
  SELECT Code = 'YTD-1', [DESC] = 'Year to Date - Last Year', [DESC_NL] = 'Jaar tot Dag - Vorig Jaar'
  UNION
  SELECT Code      = 'YTD-2'
       , [DESC]    = 'Year to Date - Next to last Year'
       , [DESC_NL] = 'Jaar tot Dag - Twee jaar geleden'
  UNION
  SELECT Code = 'RY', [DESC] = 'Rolling Year', [DESC_NL] = 'Lopend Jaar'
  UNION
  SELECT Code      = 'YTD-Diff-Abs'
       , [DESC]    = 'Year to Date Difference Absolute'
       , [DESC_NL] = 'Jaar tot Dag Verschil Absoluut'
  UNION
  SELECT Code      = 'YTD-Diff-Perc'
       , [DESC]    = 'Year to Date Difference Percentage'
       , [DESC_NL] = 'Jaar tot Dag Verschil Percentage' ;
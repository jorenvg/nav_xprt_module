EXEC dbo.drop_object @object = N'dim.LoadProduct' -- nvarchar(128)
                   , @type = N'P'                 -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadProduct
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Product
     SET      InventSize = PRV.InventSize
            , InventColor = PRV.InventColor
            , InventStyle = PRV.InventStyle
            , InventConfiguration = PRV.InventConfiguration
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Product     PR
   INNER JOIN dim.ProductView PRV ON PRV.InventSize          = PR.InventSize
                                 AND PRV.InventColor         = PR.InventColor
                                 AND PRV.InventStyle         = PR.InventStyle
                                 AND PRV.InventConfiguration = PR.InventConfiguration
                                 AND PRV.CompanyID           = PR.CompanyID
                                 AND PRV.Partition           = PR.Partition
                                 AND PRV.DataConnectionID    = PR.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Product (
    DataConnectionID, CompanyID, Partition, ComponentExecutionID, InventSize, InventColor, InventStyle, InventConfiguration
  )
  SELECT PRV.DataConnectionID
       , PRV.CompanyID
       , PRV.Partition
       , @component_execution_id
       , PRV.InventSize
       , PRV.InventColor
       , PRV.InventStyle
       , PRV.InventConfiguration
    FROM dim.ProductView PRV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Product PR
                       WHERE PRV.InventSize          = PR.InventSize
                         AND PRV.InventColor         = PR.InventColor
                         AND PRV.InventStyle         = PR.InventStyle
                         AND PRV.InventConfiguration = PR.InventConfiguration
                         AND PRV.CompanyID           = PR.CompanyID
                         AND PRV.Partition           = PR.Partition
                         AND PRV.DataConnectionID    = PR.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
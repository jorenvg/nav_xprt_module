EXEC dbo.drop_object 'dim.ProductView', 'V' ;
GO

CREATE VIEW [dim].ProductView
AS
  SELECT            DISTINCT
                    COALESCE(NULLIF(ES.NAME, ''), 'N/A') AS InventSize
                  , COALESCE(NULLIF(EC.NAME, ''), 'N/A') AS InventColor
                  , COALESCE(NULLIF(EY.NAME, ''), 'N/A') AS InventStyle
                  , COALESCE(NULLIF(EG.NAME, ''), 'N/A') AS InventConfiguration
                  , ID.company_id                        AS CompanyID
                  , ID.data_connection_id                AS DataConnectionID
                  , ID.Partition                         AS Partition
    FROM            stage_ax.INVENTDIM           AS ID
    LEFT OUTER JOIN stage_ax.ECORESSIZE          ES ON ID.INVENTSIZEID  = ES.NAME
                                                   AND ID.PARTITION     = ES.PARTITION
    LEFT OUTER JOIN stage_ax.ECORESCOLOR         EC ON ID.INVENTCOLORID = EC.NAME
                                                   AND ID.PARTITION     = EC.PARTITION
    LEFT OUTER JOIN stage_ax.ECORESSTYLE         EY ON ID.INVENTSTYLEID = EY.NAME
                                                   AND ID.PARTITION     = EY.PARTITION
    LEFT OUTER JOIN stage_ax.ECORESCONFIGURATION EG ON ID.CONFIGID      = EG.NAME
                                                   AND ID.PARTITION     = EG.PARTITION ;
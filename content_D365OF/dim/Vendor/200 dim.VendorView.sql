EXEC dbo.drop_object 'dim.VendorView', 'V' ;
GO

CREATE VIEW [dim].[VendorView]
AS
  SELECT      ISNULL(NULLIF(VT.ACCOUNTNUM, ''), 'N/A')                                                                 AS VendorAccountNo  --BK        
            , ISNULL(NULLIF(DPT.NAME, ''), 'N/A')                                                                      AS VendorAccountName
            , ISNULL(NULLIF(VT.ACCOUNTNUM, '') + ' - ', '') + ISNULL(NULLIF(DPT.NAME, ''), 'N/A')                      AS VendorAccountNoName
            , ISNULL(NULLIF(lpa.[ADDRESS], ''), 'N/A')                                                                 AS AddressDesc
            , ISNULL(NULLIF(lpa.STREET, ''), 'N/A')                                                                    AS Street
            , ISNULL(NULLIF(lpa.[ZIPCODE], ''), 'N/A')                                                                 AS ZipCode
            , ISNULL(NULLIF(lpa.[CITY], ''), 'N/A')                                                                    AS City
            , ISNULL(NULLIF(lpa.COUNTY, ''), 'N/A')                                                                    AS County
            , ISNULL(NULLIF(lpa.[STATE], ''), 'N/A')                                                                   AS StateCode
            , ISNULL(NULLIF(st.Name, ''), 'N/A')                                                                       AS StateDesc
            , ISNULL(NULLIF(lpa.[STATE], '') + ' - ', '') + ISNULL(NULLIF(st.NAME, ''), 'N/A')                         AS StateCodeDesc
            , ISNULL(NULLIF(lpa.COUNTRYREGIONID, ''), 'N/A')                                                           AS CountryCode
            , ISNULL(NULLIF(cny.SHORTNAME, ''), 'N/A')                                                                 AS CountryShortDesc
            , ISNULL(NULLIF(cny.LONGNAME, ''), 'N/A')                                                                  AS CountryLongDesc
            , ISNULL(NULLIF(lpa.COUNTRYREGIONID, '') + ' - ', '') + ISNULL(NULLIF(cny.SHORTNAME, ''), 'N/A')           AS CountryCodeDesc
            , ISNULL(NULLIF(VT.VENDGROUP, ''), 'N/A')                                                                  AS VendorGroupCode
            , ISNULL(NULLIF(vg.NAME, ''), 'N/A')                                                                       AS VendorGroupDesc
            , ISNULL(NULLIF(VT.VENDGROUP, '') + ' - ', '') + ISNULL(NULLIF(vg.NAME, ''), 'N/A')                        AS VendorGroupCodeDesc
            , ISNULL(NULLIF(VT.ITEMBUYERGROUPID, ''), 'N/A')                                                           AS VendorBuyerGroupCode
            , ISNULL(NULLIF(bg.[Description], ''), 'N/A')                                                              AS VendorBuyerGroupDesc
            , ISNULL(NULLIF(VT.ITEMBUYERGROUPID, '') + ' - ', '') + ISNULL(NULLIF(bg.[Description], ''), 'N/A')        AS VendorBuyerGroupCodeDesc
            , ISNULL(VT.BLOCKED, -1)                                                                                   AS VendorOnHold
            , ISNULL(NULLIF(VT.TAMREBATEGROUPID, ''), 'N/A')                                                           AS VendorRebateGroupCode
            , ISNULL(NULLIF(rg.[DESCRIPTION], ''), 'N/A')                                                              AS VendorRebateGroupDesc
            , ISNULL(NULLIF(VT.TAMREBATEGROUPID, '') + ' - ', '') + ISNULL(NULLIF(rg.[Description], ''), 'N/A')        AS VendorRebateGroupCodeDesc
            , ISNULL(NULLIF(VT.SEGMENTID, ''), 'N/A')                                                                  AS VendorSegmentCode
            , ISNULL(NULLIF(sg.[DESCRIPTION], ''), 'N/A')                                                              AS VendorSegmentDesc
            , ISNULL(NULLIF(VT.SEGMENTID, '') + ' - ', '') + ISNULL(NULLIF(sg.[Description], ''), 'N/A')               AS VendorSegmentCodeDesc
            , ISNULL(NULLIF(VT.SUBSEGMENTID, ''), 'N/A')                                                               AS VendorSubSegmentCode
            , ISNULL(NULLIF(ssg.[SUBSEGMENTDESCRIPTION], ''), 'N/A')                                                   AS VendorSubSegmentDesc
            , ISNULL(NULLIF(VT.SUBSEGMENTID, '') + ' - ', '') + ISNULL(NULLIF(ssg.[SUBSEGMENTDESCRIPTION], ''), 'N/A') AS VendorSubSegmentCodeDesc
            , ISNULL(NULLIF(VT.CASHDISC, ''), 'N/A')                                                                   AS CashDiscountCode
            , ISNULL(NULLIF(cd.[DESCRIPTION], ''), 'N/A')                                                              AS CashDiscountDesc
            , ISNULL(NULLIF(VT.CASHDISC, '') + ' - ', '') + ISNULL(NULLIF(cd.[DESCRIPTION], ''), 'N/A')                AS CashDiscountCodeDesc
            , ISNULL(NULLIF(VT.PRICEGROUP, ''), 'N/A')                                                                 AS VendorPriceGroupCode
            , ISNULL(NULLIF(pg.NAME, ''), 'N/A')                                                                       AS VendorPriceGroupDesc
            , ISNULL(NULLIF(VT.PRICEGROUP, '') + ' - ', '') + ISNULL(NULLIF(pg.NAME, ''), 'N/A')                       AS VendorPriceGroupCodeDesc
            , IIF(ic.RECID IS NULL, 0, 1)                                                                              AS InterCompanyVendor
            , VT.data_connection_id                                                                                    AS DataConnectionID --BK      
            , VT.company_id                                                                                            AS CompanyID        --BK        
            , VT.Partition                                                                                             AS Partition        --BK          
            , VT.stage_id                                                                                              AS StageID
    FROM      stage_ax.VENDTABLE                                AS VT
    LEFT JOIN stage_ax.DIRPARTYTABLE                            DPT ON VT.PARTY            = DPT.RECID
                                                                   AND VT.PARTITION        = DPT.PARTITION
    LEFT JOIN stage_ax.LOGISTICSLOCATION                        lc ON lc.RECID             = DPT.PRIMARYADDRESSLOCATION
                                                                  AND VT.PARTITION         = lc.PARTITION
    LEFT JOIN stage_ax.LOGISTICSPOSTALADDRESS                   lpa ON lpa.LOCATION        = lc.RECID
                                                                   AND GETDATE() BETWEEN lpa.VALIDFROM AND lpa.VALIDTO
                                                                   AND VT.PARTITION        = lpa.PARTITION
    LEFT JOIN stage_ax.LOGISTICSADDRESSCOUNTRYREGIONTRANSLATION cny ON lpa.COUNTRYREGIONID = cny.COUNTRYREGIONID
                                                                   AND VT.PARTITION        = cny.PARTITION
                                                                   AND cny.LANGUAGEID      = (SELECT language_description FROM meta.current_instance)
    LEFT JOIN stage_ax.LOGISTICSADDRESSSTATE                    st ON lpa.COUNTRYREGIONID  = st.COUNTRYREGIONID
                                                                  AND lpa.STATE            = st.STATEID
                                                                  AND VT.PARTITION         = st.PARTITION
    LEFT JOIN stage_ax.VENDGROUP                                vg ON VT.VENDGROUP         = vg.VENDGROUP
                                                                  AND VT.PARTITION         = vg.PARTITION
                                                                  AND VT.company_id        = vg.company_id
    LEFT JOIN stage_ax.INVENTBUYERGROUP                         bg ON VT.ITEMBUYERGROUPID  = bg.GROUP_
                                                                  AND VT.PARTITION         = bg.PARTITION
                                                                  AND VT.company_id        = bg.company_id
    LEFT JOIN stage_ax.TAMVENDREBATEGROUP                       rg ON VT.TAMREBATEGROUPID  = rg.VENDREBATEGROUPID
                                                                  AND VT.PARTITION         = rg.PARTITION
                                                                  AND VT.company_id        = rg.company_id
    LEFT JOIN stage_ax.SMMBUSRELSEGMENTGROUP                    sg ON VT.SEGMENTID         = sg.SEGMENTID
                                                                  AND VT.PARTITION         = sg.PARTITION
                                                                  AND VT.company_id        = sg.company_id
    LEFT JOIN stage_ax.SMMBUSRELSUBSEGMENTGROUP                 ssg ON VT.SUBSEGMENTID     = ssg.SEGMENTID
                                                                   AND VT.PARTITION        = ssg.PARTITION
                                                                   AND VT.company_id       = ssg.company_id
    LEFT JOIN stage_ax.CASHDISC                                 cd ON VT.CASHDISC          = cd.CASHDISCCODE
                                                                  AND VT.PARTITION         = cd.PARTITION
                                                                  AND VT.company_id        = cd.company_id
    LEFT JOIN stage_ax.PRICEDISCGROUP                           pg ON VT.PRICEGROUP        = pg.GROUPID
                                                                  AND VT.PARTITION         = pg.PARTITION
                                                                  AND VT.company_id        = pg.company_id
    LEFT JOIN stage_ax.INTERCOMPANYTRADINGPARTNER               ic ON VT.PARTY             = ic.VENDORPARTY
                                                                  AND VT.PARTITION         = ic.PARTITION
                                                                  AND VT.DATAAREAID        = ic.VENDORDATAAREAID ;
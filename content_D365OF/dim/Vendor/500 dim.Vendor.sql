EXEC dbo.drop_object 'dim.Vendor', 'T' ;

CREATE TABLE dim.Vendor
(
  VendorID                    INT             IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                     BIGINT          NULL
, DataConnectionID            INT             NOT NULL
, ExecutionTimestamp          ROWVERSION      NOT NULL
, ComponentExecutionID        INT             NOT NULL
, CompanyID                   INT             NOT NULL
, Partition                   BIGINT          NOT NULL
, [VendorAccountNo]           [NVARCHAR](30)  NOT NULL
, [VendorAccountName]         [NVARCHAR](128) NOT NULL
, [VendorAccountNoName]       [NVARCHAR](255) NOT NULL
, [AddressDesc]               [NVARCHAR](512) NOT NULL
, [Street]                    [NVARCHAR](250) NOT NULL
, [ZipCode]                   [NVARCHAR](30)  NOT NULL
, [City]                      [NVARCHAR](100) NOT NULL
, [County]                    [NVARCHAR](100) NOT NULL
, [StateCode]                 [NVARCHAR](30)  NOT NULL
, [StateDesc]                 [NVARCHAR](100) NOT NULL
, [StateCodeDesc]             [NVARCHAR](128) NOT NULL
, [CountryCode]               [NVARCHAR](30)  NOT NULL
, [CountryShortDesc]          [NVARCHAR](255) NOT NULL
, [CountryLongDesc]           [NVARCHAR](255) NOT NULL
, [CountryCodeDesc]           [NVARCHAR](512) NOT NULL
, [VendorGroupCode]           [NVARCHAR](30)  NOT NULL
, [VendorGroupDesc]           [NVARCHAR](100) NOT NULL
, [VendorGroupCodeDesc]       [NVARCHAR](128) NOT NULL
, [VendorBuyerGroupCode]      [NVARCHAR](30)  NOT NULL
, [VendorBuyerGroupDesc]      [NVARCHAR](100) NOT NULL
, [VendorBuyerGroupCodeDesc]  [NVARCHAR](128) NOT NULL
, [VendorOnHold]              [INT]           NOT NULL
, [VendorRebateGroupCode]     [NVARCHAR](30)  NOT NULL
, [VendorRebateGroupDesc]     [NVARCHAR](100) NOT NULL
, [VendorRebateGroupCodeDesc] [NVARCHAR](128) NOT NULL
, [VendorSegmentCode]         [NVARCHAR](30)  NOT NULL
, [VendorSegmentDesc]         [NVARCHAR](100) NOT NULL
, [VendorSegmentCodeDesc]     [NVARCHAR](128) NOT NULL
, [VendorSubSegmentDesc]      [NVARCHAR](100) NOT NULL
, [VendorSubSegmentCode]      [NVARCHAR](30)  NOT NULL
, [VendorSubSegmentCodeDesc]  [NVARCHAR](128) NOT NULL
, [CashDiscountCode]          [NVARCHAR](30)  NOT NULL
, [CashDiscountDesc]          [NVARCHAR](100) NOT NULL
, [CashDiscountCodeDesc]      [NVARCHAR](128) NOT NULL
, [VendorPriceGroupCode]      [NVARCHAR](30)  NOT NULL
, [VendorPriceGroupDesc]      [NVARCHAR](100) NOT NULL
, [VendorPriceGroupCodeDesc]  [NVARCHAR](128) NOT NULL
, [InterCompanyVendor]        [INT]           NOT NULL
) ;
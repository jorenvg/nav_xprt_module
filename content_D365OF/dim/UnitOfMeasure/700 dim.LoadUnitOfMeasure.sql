EXEC dbo.drop_object @object = N'dim.LoadUnitOfMeasure' -- nvarchar(128)
                   , @type = N'P'                       -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadUnitOfMeasure
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.UnitOfMeasure
     SET      StageID = UOMV.StageID
            , UnitOfMeasureDesc = UOMV.UnitOfMeasureDesc
            , UnitOfMeasureCodeDesc = UOMV.UnitOfMeasureCodeDesc
            , UnitOfMeasureClassDesc = UOMV.UnitOfMeasureClassDesc
            , UnitOfMeasureClassCodeDesc = UOMV.UnitOfMeasureClassCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.UnitOfMeasure     UOM
   INNER JOIN dim.UnitOfMeasureView UOMV ON UOMV.UnitOfMeasureCode      = UOM.UnitOfMeasureCode
                                        AND UOMV.UnitOfMeasureClassCode = UOM.UnitOfMeasureClassCode
                                        AND UOMV.Partition              = UOM.Partition
                                        AND UOMV.DataConnectionID       = UOM.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.UnitOfMeasure (
    StageID, DataConnectionID, ComponentExecutionID, Partition, UnitOfMeasureCode, UnitOfMeasureDesc, UnitOfMeasureCodeDesc, UnitOfMeasureClassCode, UnitOfMeasureClassDesc, UnitOfMeasureClassCodeDesc
  )
  SELECT UOMV.StageID
       , UOMV.DataConnectionID
       , @component_execution_id
       , Partition
       , UOMV.UnitOfMeasureCode
       , UOMV.UnitOfMeasureDesc
       , UOMV.UnitOfMeasureCodeDesc
       , UOMV.UnitOfMeasureClassCode
       , UOMV.UnitOfMeasureClassDesc
       , UOMV.UnitOfMeasureClassCodeDesc
    FROM dim.UnitOfMeasureView UOMV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.UnitOfMeasure UOM
                       WHERE UOMV.UnitOfMeasureCode     = UOM.UnitOfMeasureCode
                         AND UOMV.UnitOfMeasureCodeDesc = UOM.UnitOfMeasureCodeDesc
                         AND UOMV.Partition             = UOM.Partition
                         AND UOMV.DataConnectionID      = UOM.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
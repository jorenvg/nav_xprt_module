EXEC Dbo.Drop_Object @Object = N'dim.LoadCostGroup'
-- nvarchar(128)
                   , @Type = N'P'
-- nchar(2)
                   , @Debug = 0 ;
-- int
GO


CREATE PROCEDURE [dim].LoadCostGroup
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @Deleted = 0 ;
  SET @Updated = 0 ;
  SET @Inserted = 0 ;

  UPDATE      Dim.CostGroup
     SET      StageId = CG.StageId
            , DataConnectionID = CG.DataConnectionID
            , CompanyId = CG.CompanyId
            , Partition = CG.Partition
            , ComponentExecutionId = @Component_Execution_Id
            , CostGroupType = CG.CostGroupType
            , CostGroupTypeDescription = CG.CostGroupTypeDescription
            , CostGroupName = CG.CostGroupName
    FROM      Dim.CostGroup     AS CG
   INNER JOIN Dim.CostGroupView AS CGV ON CG.CostGroupCode    = CGV.CostGroupCode
                                      AND CG.CompanyId        = CGV.CompanyId
                                      AND CG.Partition        = CGV.Partition
                                      AND CG.DataConnectionId = CGV.DataConnectionId ;

  SELECT @Updated = @@RowCount ;

  INSERT INTO Dim.CostGroup (
    StageId, DataConnectionId, CompanyId, Partition, ComponentExecutionId, CostGroupCode, CostGroupType, CostGroupTypeDescription, CostGroupName
  )
  SELECT CGV.StageId
       , CGV.DataConnectionId
       , CGV.CompanyId
       , CGV.Partition
       , @Component_Execution_Id
       , CGV.CostGroupCode
       , CGV.CostGroupType
       , CGV.CostGroupTypeDescription
       , CGV.CostGroupName
    FROM Dim.CostGroupView CGV
   WHERE NOT EXISTS ( SELECT 1
                        FROM Dim.CostGroup CG
                       WHERE CG.CostGroupCode    = CGV.CostGroupCODE
                         AND CG.CompanyId        = CGV.CompanyId
                         AND CG.Partition        = CGV.Partition
                         AND CG.DataConnectionId = CGV.DataConnectionId) ;

  SELECT @Inserted = @@RowCount ;
END ;

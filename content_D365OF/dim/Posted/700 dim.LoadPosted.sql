EXEC dbo.drop_object @object = N'dim.LoadPosted' -- nvarchar(128)
                   , @type = N'P'                -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadPosted]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Posted
     SET      PostedDesc = SOTV.PostedDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Posted     SOT
   INNER JOIN dim.PostedView SOTV ON SOTV.PostedCode       = SOT.PostedCode
                                 AND SOTV.DataConnectionID = SOT.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Posted (
    DataConnectionID, ComponentExecutionID, PostedCode, PostedDesc
  )
  SELECT SOTV.DataConnectionID
       , @component_execution_id
       , SOTV.PostedCode
       , SOTV.PostedDesc
    FROM dim.PostedView SOTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Posted SOT
                       WHERE SOTV.PostedCode       = SOT.PostedCode
                         AND SOTV.DataConnectionID = SOT.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
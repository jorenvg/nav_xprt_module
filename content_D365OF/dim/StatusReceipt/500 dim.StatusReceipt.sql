EXEC dbo.drop_object 'dim.StatusReceipt', 'T' ;

CREATE TABLE dim.StatusReceipt
(
  StatusReceiptID      INT        IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT        NOT NULL
, ExecutionTimestamp   ROWVERSION NOT NULL
, ComponentExecutionID INT        NOT NULL
, StatusReceiptCode    INT        NOT NULL
, StatusReceiptDesc    NVARCHAR(128)
) ;
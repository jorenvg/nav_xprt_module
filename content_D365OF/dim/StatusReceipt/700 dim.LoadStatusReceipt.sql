EXEC dbo.drop_object @object = N'dim.LoadStatusReceipt' -- nvarchar(128)
                   , @type = N'P'                       -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadStatusReceipt]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.StatusReceipt
     SET      StatusReceiptDesc = SOTV.StatusReceiptDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.StatusReceipt     SOT
   INNER JOIN dim.StatusReceiptView SOTV ON SOTV.StatusReceiptCode = SOT.StatusReceiptCode
                                        AND SOTV.DataConnectionID  = SOT.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.StatusReceipt (
    DataConnectionID, ComponentExecutionID, StatusReceiptCode, StatusReceiptDesc
  )
  SELECT SOTV.DataConnectionID
       , @component_execution_id
       , SOTV.StatusReceiptCode
       , SOTV.StatusReceiptDesc
    FROM dim.StatusReceiptView SOTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.StatusReceipt SOT
                       WHERE SOTV.StatusReceiptCode = SOT.StatusReceiptCode
                         AND SOTV.DataConnectionID  = SOT.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
EXEC dbo.drop_object @object = N'dim.LoadBuyerGroup' -- nvarchar(128)
                   , @type = N'P'                    -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadBuyerGroup
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.BuyerGroup
     SET      StageID = BGV.StageID
            , BuyerGroupDesc = BGV.BuyerGroupDesc
            , BuyerGroupCodeDesc = BGV.BuyerGroupCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.BuyerGroup     BG
   INNER JOIN dim.BuyerGroupView BGV ON BG.BuyerGroupCode   = BGV.BuyerGroupCode
                                    AND BG.CompanyID        = BGV.CompanyID
                                    AND BG.Partition        = BGV.Partition
                                    AND BG.DataConnectionID = BGV.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.BuyerGroup (
    StageID, DataConnectionID, CompanyID, Partition, ComponentExecutionID, BuyerGroupCode, BuyerGroupDesc, BuyerGroupCodeDesc
  )
  SELECT BGV.StageID
       , BGV.DataConnectionID
       , BGV.CompanyID
       , BGV.Partition
       , @component_execution_id
       , BGV.BuyerGroupCode
       , BGV.BuyerGroupDesc
       , BGV.BuyerGroupCodeDesc
    FROM dim.BuyerGroupView BGV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.BuyerGroup BG
                       WHERE BGV.BuyerGroupCode   = BG.BuyerGroupCode
                         AND BGV.CompanyID        = BG.CompanyID
                         AND BGV.Partition        = BG.Partition
                         AND BGV.DataConnectionID = BG.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
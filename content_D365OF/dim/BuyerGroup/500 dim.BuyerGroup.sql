
-- ################################################################################ --
-- #####                         Table: dim.BuyerGroup                        ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.BuyerGroup', 'T' ;

CREATE TABLE dim.BuyerGroup
(
  BuyerGroupID         INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, BuyerGroupCode       NVARCHAR(512) NOT NULL
, BuyerGroupDesc       NVARCHAR(512) NOT NULL
, BuyerGroupCodeDesc   NVARCHAR(512) NULL
) ;
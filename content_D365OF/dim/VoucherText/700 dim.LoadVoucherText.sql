
EXEC dbo.drop_object @object = N'dim.LoadVoucherText', -- nvarchar(128)
  @type = N'P', -- nchar(2)
  @debug = 0;
 -- int
GO 

CREATE PROCEDURE [dim].LoadVoucherText
  @component_execution_id INT = -1
, @load_type TINYINT = 1
, @inserted INT = NULL OUTPUT
, @updated INT = NULL OUTPUT
, @deleted INT = NULL OUTPUT
AS
  BEGIN
			
    SET @deleted = 0;
    SET @updated = 0;
    SET @inserted = 0;
  
    UPDATE  dim.VoucherText
    SET    
      VoucherText = TRV.VoucherText,
		 ComponentExecutionID = @component_execution_id
    FROM    dim.VoucherText TR
            INNER JOIN dim.VoucherTextView TRV ON TRV.VoucherCode = TR.VoucherCode
													  AND TRV.VoucherVersion = TR.VoucherVersion
                                                      AND TRV.CompanyID = TR.CompanyID
													  AND TRV.Partition = TR.Partition
                                                      AND TRV.DataConnectionID = TR.DataConnectionID;
			
				
    SELECT  @updated = @@ROWCOUNT;

    INSERT  INTO dim.VoucherText
            (       
            DataConnectionID     
            ,CompanyID            
            ,Partition               
            ,ComponentExecutionID 
            ,VoucherCode                
            ,VoucherVersion           
            ,VoucherText          

            )
            SELECT 
              TRV.DataConnectionID
              , TRV.CompanyID
              , TRV.Partition                  
              , @component_execution_id

              , TRV.VoucherCode
              , TRV.VoucherVersion
              , TRV.VoucherText

            FROM    dim.VoucherTextView TRV
            WHERE   NOT EXISTS ( SELECT *
                                 FROM   dim.VoucherText TR
                                 WHERE  TRV.VoucherCode = TR.VoucherCode
                                    AND TRV.VoucherVersion = TR.VoucherVersion
                                    AND TRV.CompanyID = TR.CompanyID
                                    AND TRV.Partition = TR.Partition
                                    AND TRV.DataConnectionID = TR.DataConnectionID );

    SELECT  @inserted = @@ROWCOUNT;

  END 
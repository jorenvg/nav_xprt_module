
EXEC dbo.drop_object 'dim.VoucherTextView', 'V';
GO

CREATE VIEW [dim].[VoucherTextView]
AS
  SELECT
    VoucherText = ISNULL(NULLIF(PT.TEXT,''),'N/A')
  , VoucherCode = PT.RECID
  , VoucherVersion = PT.RECVERSION
  , CompanyID = PT.company_id
  , DataConnectionID = PT.data_connection_id
  , Partition = PT.Partition
  , StageID = PT.stage_id
  FROM    stage_ax.GENERALJOURNALACCOUNTENTRY PT
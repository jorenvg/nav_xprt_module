EXEC dbo.drop_object @object = N'dim.LoadSalesOrder' -- nvarchar(128)
                   , @type = N'P'                    -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadSalesOrder
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesOrder
     SET      StageID = SOV.StageID
            , CompanyID = SOV.CompanyId
            , DataConnectionID = SOV.DataConnectionId
            , Partition = SOV.Partition
            , ComponentExecutionID = @component_execution_id
            , SalesOrder = SOV.SalesOrder
            , SalesTypeCode = SOV.SalesTypeCode
            , SalesTypeDescription = SOV.SalesTypeDescription
            , SalesPoolCode = SOV.SalesPoolCode
            , SalesPoolDescription = SOV.SalesPoolDescription
            , SalesPoolCodeDescription = SOV.SalesPoolCodeDescription
            , SalesStatusCode = SOV.SalesStatusCode
            , SalesStatusDescription = SOV.SalesStatusDescription
    FROM      dim.SalesOrder     SO
   INNER JOIN dim.SalesOrderView SOV ON SOV.SalesOrder       = SO.SalesOrder
                                    AND SOV.CompanyID        = SO.CompanyID
                                    AND SOV.Partition        = SO.Partition
                                    AND SOV.DataConnectionID = SO.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.SalesOrder (
    StageID
  , CompanyID
  , DataConnectionID
  , Partition
  , ComponentExecutionID
  , SalesOrder
  , SalesTypeCode
  , SalesTypeDescription
  , SalesPoolCode
  , SalesPoolDescription
  , SalesPoolCodeDescription
  , SalesStatusCode
  , SalesStatusDescription
  )
  SELECT SOV.StageID
       , SOV.CompanyID
       , SOV.DataConnectionID
       , SOV.Partition
       , @component_execution_id
       , SOV.SalesOrder
       , SOV.SalesTypeCode
       , SOV.SalesTypeDescription
       , SOV.SalesPoolCode
       , SOV.SalesPoolDescription
       , SOV.SalesPoolCodeDescription
       , SOV.SalesStatusCode
       , SOV.SalesStatusDescription
    FROM dim.SalesOrderView SOV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesOrder SO
                       WHERE SOV.SalesOrder       = SO.SalesOrder
                         AND SOV.CompanyID        = SO.CompanyID
                         AND SOV.Partition        = SO.Partition
                         AND SOV.DataConnectionID = SO.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;


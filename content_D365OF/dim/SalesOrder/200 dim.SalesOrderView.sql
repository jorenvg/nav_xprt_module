EXEC dbo.drop_object 'dim.SalesOrderView', 'V' ;
GO

CREATE VIEW [dim].[SalesOrderView]
AS
  SELECT            PT.stage_id                                                                            AS StageID
                  , PT.company_id                                                                          AS CompanyID
                  , PT.Partition                                                                           AS Partition
                  , PT.data_connection_id                                                                  AS DataConnectionID
                  , COALESCE(NULLIF(PT.SALESID, ''), 'N/A')                                                AS SalesOrder
                  , COALESCE(ST_ENUM.OptionID, -1)                                                         AS SalesTypeCode
                  , ISNULL(NULLIF(ST_ENUM.Description, ''), 'N/A')                                         AS SalesTypeDescription
                  , COALESCE(NULLIF(PT.SalesPoolId, ''), 'N/A')                                            AS SalesPoolCode
                  , ISNULL(NULLIF(SP.Name, ''), 'N/A')                                                     AS SalesPoolDescription
                  , ISNULL(NULLIF(PT.SALESPOOLID, ''), 'N/A') + ISNULL(' - ' + NULLIF(SP.NAME, ''), 'N/A') AS SalesPoolCodeDescription
                  , COALESCE(SS_ENUM.OptionId, -1)                                                         AS SalesStatusCode
                  , COALESCE(NULLIF(SS_ENUM.Description, ''), 'N/A')                                       AS SalesStatusDescription
    -- select COUNT(*)
    FROM            stage_ax.SALESTABLE AS PT
    LEFT OUTER JOIN stage_ax.SalesPool  AS SP ON PT.SalesPoolId             = SP.SalesPoolId
                                             AND PT.Company_Id              = SP.Company_Id
                                             AND PT.Data_Connection_Id      = SP.Data_Connection_Id
    LEFT OUTER JOIN meta.Enumerations   AS ST_ENUM ON ST_ENUM.TableName     = 'Salesline'
                                                  AND ST_ENUM.ColumnName    = 'SalesType'
                                                  AND PT.Salestype          = ST_ENUM.OptionId
                                                  AND PT.data_connection_id = ST_ENUM.dataconnectionid
    LEFT OUTER JOIN meta.Enumerations   AS SS_ENUM ON SS_ENUM.TableName     = 'SalesTable'
                                                  AND SS_ENUM.ColumnName    = 'SalesStatus'
                                                  AND PT.SalesStatus        = SS_ENUM.OptionId
                                                  AND PT.Data_Connection_Id = SS_ENUM.DataConnectionId ;


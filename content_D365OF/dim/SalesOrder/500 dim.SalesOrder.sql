
-- ################################################################################ --
-- #####                         Table: dim.SalesOrder                        ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.SalesOrder', 'T' ;

CREATE TABLE dim.SalesOrder
(
  SalesOrderID             INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                  BIGINT        NOT NULL
, CompanyID                INT           NOT NULL
, DataConnectionID         INT           NOT NULL
, Partition                BIGINT        NOT NULL
, ExecutionTimestamp       ROWVERSION    NOT NULL
, ComponentExecutionID     INT           NOT NULL
, SalesOrder               NVARCHAR(100) NULL
, SalesTypeCode            INT           NULL
, SalesTypeDescription     NVARCHAR(256)
, SalesPoolCode            NVARCHAR(40)  NULL
, SalesPoolDescription     NVARCHAR(256) NULL
, SalesPoolCodeDescription NVARCHAR(512) NULL
, SalesStatusCode          INT           NULL
, SalesStatusDescription   NVARCHAR(256)
) ;





-- ################################################################################ --
-- #####                         Table: dim.Consumption                     ##### --
-- ################################################################################ --
EXEC Dbo.Drop_Object 'dim.Consumption ', 'T' ;

CREATE TABLE dim.Consumption
(
  ConsumptionID               INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID            INT           NOT NULL
, CompanyID                   INT           NOT NULL
, Partition                   BIGINT        NOT NULL
, ExecutionTimestamp          ROWVERSION    NOT NULL
, ComponentExecutionID        INT           NOT NULL
, ConsumptionType             NVARCHAR(512) NULL
, SourceType                  NVARCHAR(512) NULL
, ResourceCode                NVARCHAR(120) NULL
, ResourceDescription         NVARCHAR(120) NULL
, ResourceCodeDescription     NVARCHAR(255) NULL
, GroupCode                   NVARCHAR(40)  NULL
, GroupDescription            NVARCHAR(160) NULL
, GroupCodeDescription        NVARCHAR(255) NULL
, ProductGroupCode            NVARCHAR(40)  NULL
, ProductGroupDescription     NVARCHAR(160) NULL
, ProductGroupCodeDescription NVARCHAR(255) NULL
, ResourceType                NVARCHAR(160) NULL
) ;
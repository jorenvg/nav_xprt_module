EXEC Dbo.Drop_Object 'dim.ConsumptionWorkCenterView', 'V' ;
GO

CREATE VIEW [dim].ConsumptionWorkCenterView
AS
  SELECT            DISTINCT
                    PCT.Company_Id                                                                            AS CompanyID
                  , PCT.Data_Connection_Id                                                                    AS DataConnectionID
                  , PCT.Partition                                                                             AS Partition
                  , COALESCE(CT_ENUM.Description, 'N/A')                                                      AS ConsumptionType
                  , COALESCE(ST_ENUM.Description, 'N/A')                                                      AS SourceType
                  , COALESCE(NULLIF(PCT.[Resource_], ''), 'N/A')                                              AS ResourceCode
                  , COALESCE(NULLIF(WCT.Name, ''), 'N/A')                                                     AS ResourceDescription
                  , COALESCE(NULLIF(PCT.[Resource_], ''), 'N/A') + COALESCE(' - ' + NULLIF(WCT.NAME, ''), '') AS ResourceCodeDescription
                  , 'N/A'                                                                                     AS GroupCode
                  , 'N/A'                                                                                     AS GroupDescription
                  , 'N/A'                                                                                     AS GroupCodeDescription
                  , 'N/A'                                                                                     AS ProductGroupCode
                  , 'N/A'                                                                                     AS ProductGroupDescription
                  , 'N/A'                                                                                     AS ProductGroupCodeDescription
                  , COALESCE(NULLIF(WCT_ENUM.Description, ''), 'N/A')                                         AS ResourceType
    FROM            Stage_Ax.ProdCalcTrans AS PCT
    LEFT OUTER JOIN stage_ax.INVENTTABLE   AS IT ON PCT.[RESOURCE_]              = IT.ITEMID
                                                AND PCT.company_id               = IT.company_id
                                                AND PCT.data_connection_id       = IT.data_connection_id
    LEFT OUTER JOIN meta.enumerations      AS ST_ENUM ON ST_ENUM.TableName       = 'Consumption'
                                                     AND ST_ENUM.ColumnName      = 'SourceType'
                                                     AND ST_ENUM.OptionID        = 1
                                                     AND PCT.data_connection_id  = ST_ENUM.dataconnectionid
    LEFT OUTER JOIN meta.enumerations      AS CT_ENUM ON CT_ENUM.TableName       = 'Consumption'
                                                     AND CT_ENUM.ColumnName      = 'CalcType'
                                                     AND PCT.CALCTYPE            = CT_ENUM.OptionID
                                                     AND PCT.data_connection_id  = CT_ENUM.dataconnectionid
    LEFT OUTER JOIN stage_ax.Wrkctrtable   AS WCT ON PCT.Resource_               = WCT.Wrkctrid
                                                 AND PCT.Company_Id              = WCT.company_id
                                                 AND PCT.DataAreaId              = WCT.DataAreaId
                                                 AND PCT.Data_Connection_Id      = WCT.Data_Connection_Id
    LEFT OUTER JOIN meta.Enumerations      AS WCT_ENUM ON WCT_ENUM.TableName     = 'WrkCtrTable'
                                                      AND WCT_ENUM.ColumnName    = 'WrkCtrType'
                                                      AND WCT_ENUM.OptionID      = PCT.Calctype
                                                      AND PCT.Data_Connection_Id = WCT_ENUM.DataConnectionId
   WHERE            1 = 1
     AND            PCT.CALCTYPE IN ( 4, 5, 6 ) ; -- Consumption --> Item, Bom


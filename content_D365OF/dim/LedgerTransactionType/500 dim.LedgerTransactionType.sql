
-- ################################################################################ --
-- #####                         Table: dim.LedgerTransactionType             ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.LedgerTransactionType', 'T' ;
GO
CREATE TABLE [dim].[LedgerTransactionType]
(
  LedgerTransactionTypeID   INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID          INT           NOT NULL
, ExecutionTimestamp        ROWVERSION    NOT NULL
, ComponentExecutionID      INT           NOT NULL
, LedgerTransactionTypeCode INT           NOT NULL
, LedgerTransactionTypeDesc NVARCHAR(255) NULL
) ;
GO
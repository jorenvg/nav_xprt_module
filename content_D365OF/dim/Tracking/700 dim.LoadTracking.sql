EXEC dbo.drop_object @object = N'dim.LoadTracking' -- nvarchar(128)
                   , @type = N'P'                  -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadTracking
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Tracking
     SET      InventBatchDesc = TRV.InventBatchDesc
            , InventBatchCodeDesc = TRV.InventBatchCodeDesc
            , InventSerialDesc = TRV.InventSerialDesc
            , InventSerialCodeDesc = TRV.InventSerialCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Tracking     TR
   INNER JOIN dim.TrackingView TRV ON TRV.InventBatchCode  = TR.InventBatchCode
                                  AND TRV.InventSerialCode = TR.InventSerialCode
                                  AND TRV.CompanyID        = TR.CompanyID
                                  AND TRV.Partition        = TR.Partition
                                  AND TRV.DataConnectionID = TR.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Tracking (
    DataConnectionID, CompanyID, ComponentExecutionID, Partition, InventBatchCode, InventBatchDesc, InventBatchCodeDesc, InventSerialCode, InventSerialDesc, InventSerialCodeDesc
  )
  SELECT TRV.DataConnectionID
       , TRV.CompanyID
       , @component_execution_id
       , TRV.Partition
       , TRV.InventBatchCode
       , TRV.InventBatchDesc
       , TRV.InventBatchCodeDesc
       , TRV.InventSerialCode
       , TRV.InventSerialDesc
       , TRV.InventSerialCodeDesc
    FROM dim.TrackingView TRV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Tracking TR
                       WHERE TRV.InventBatchCode  = TR.InventBatchCode
                         AND TRV.InventSerialCode = TR.InventSerialCode
                         AND TRV.CompanyID        = TR.CompanyID
                         AND TRV.Partition        = TR.Partition
                         AND TRV.DataConnectionID = TR.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;

END ;
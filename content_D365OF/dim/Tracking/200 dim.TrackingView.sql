EXEC dbo.drop_object 'dim.TrackingView', 'V' ;
GO

CREATE VIEW [dim].[TrackingView]
AS
  SELECT            DISTINCT
                    ISNULL(NULLIF(ID.INVENTBATCHID, ''), 'N/A')                                                    AS InventBatchCode
                  , ISNULL(NULLIF(IB.DESCRIPTION, ''), 'N/A')                                                      AS InventBatchDesc
                  , ISNULL(NULLIF(ID.INVENTBATCHID, '') + ' - ', '') + ISNULL(NULLIF(IB.DESCRIPTION, ''), 'N/A')   AS InventBatchCodeDesc
                  , ISNULL(NULLIF(ID.INVENTSERIALID, ''), 'N/A')                                                   AS InventSerialCode
                  , ISNULL(NULLIF(INS.DESCRIPTION, ''), 'N/A')                                                     AS InventSerialDesc
                  , ISNULL(NULLIF(ID.INVENTSERIALID, '') + ' - ', '') + ISNULL(NULLIF(INS.DESCRIPTION, ''), 'N/A') AS InventSerialCodeDesc
                  , ID.company_id                                                                                  AS CompanyID
                  , ID.data_connection_id                                                                          AS DataConnectionID
                  , ID.Partition                                                                                   AS Partition
    FROM            stage_ax.INVENTDIM    AS ID
    LEFT OUTER JOIN stage_ax.INVENTBATCH  IB ON ID.INVENTBATCHID   = IB.INVENTBATCHID
    LEFT OUTER JOIN stage_ax.INVENTSERIAL INS ON ID.INVENTSERIALID = INS.INVENTSERIALID ;






-- ################################################################################ --
-- #####                         Table: dim.Tracking                          ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.Tracking', 'T' ;

CREATE TABLE dim.Tracking
(
  TrackingID           INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, SiteCode             NVARCHAR(50)  NULL
, InventBatchCode      NVARCHAR(50)  NULL
, InventBatchDesc      NVARCHAR(100) NULL
, InventBatchCodeDesc  NVARCHAR(255) NULL
, InventSerialCode     NVARCHAR(50)  NULL
, InventSerialDesc     NVARCHAR(100) NULL
, InventSerialCodeDesc NVARCHAR(255) NULL
) ;

EXEC dbo.drop_object @object = N'dim.LoadBudgetModel', -- nvarchar(128)
  @type = N'P', -- nchar(2)
  @debug = 0 -- int
GO 

CREATE PROCEDURE [dim].[LoadBudgetModel] 
  @component_execution_id INT = -1
, @load_type TINYINT = 1
, @inserted INT = NULL OUTPUT
, @updated INT = NULL OUTPUT
, @deleted INT = NULL OUTPUT
AS
BEGIN
			
    SET @deleted = 0;
    SET @updated = 0;
    SET @inserted = 0;
  
  UPDATE dim.BudgetModel SET 
		  StageID               = BMV.StageID
		, BudgetModelDesc	    = BMV.BudgetModelDesc
		, BudgetModelTypeCode   = BMV.BudgetModelTypeCode
		, BudgetModelTypeDesc   = BMV.BudgetModelTypeDesc
		, BudgetSubmodelCode    = BMV.BudgetSubmodelCode
		, ComponentExecutionID  = @component_execution_id
  FROM
    dim.BudgetModel BM
    INNER JOIN dim.BudgetModelView BMV ON BMV.BudgetModelCode     = BM.BudgetModelCode 
										AND BMV.CompanyID         = BM.CompanyID
										AND BMV.DataConnectionID  = BM.DataConnectionID 
										AND BMV.Partition         = BM.Partition
			
  SELECT @updated = @@ROWCOUNT

  INSERT INTO dim.BudgetModel
  (
		  StageID
		, DataConnectionID
		, ComponentExecutionID
		, CompanyID
		, Partition
		, BudgetModelCode
		, BudgetModelDesc
		, BudgetModelTypeCode
		, BudgetModelTypeDesc
		, BudgetSubmodelCode
  )
  SELECT
		  BMV.StageID
		, BMV.DataConnectionID
		, COALESCE(@component_execution_id,-1)
		, BMV.CompanyID
		, BMV.Partition
		, BMV.BudgetModelCode
		, BMV.BudgetModelDesc
		, BMV.BudgetModelTypeCode
		, BMV.BudgetModelTypeDesc
		, BMV.BudgetSubmodelCode
  FROM
    dim.BudgetModelView BMV
  WHERE
    NOT EXISTS (SELECT * FROM dim.BudgetModel BM WHERE BMV.BudgetModelCode        = BM.BudgetModelCode 
														AND BMV.CompanyID         = BM.CompanyID
														AND BMV.DataConnectionID  = BM.DataConnectionID 
														AND BMV.Partition         = BM.Partition)
  
  SELECT @inserted = @@ROWCOUNT
END 
GO



EXEC Dbo.Drop_Object 'dim.ProductionOrderView', 'V' ;
GO

CREATE VIEW [dim].[ProductionOrderView]
AS
  SELECT            PT.stage_id                                                                           AS StageID
                  , PT.company_id                                                                         AS CompanyID
                  , PT.PARTITION                                                                          AS Partition
                  , PT.data_connection_id                                                                 AS DataConnectionID
                  , COALESCE(NULLIF(PT.ProdId, ''), 'N/A')                                                AS ProductionOrder
                  , COALESCE(PT.ProdStatus, -1)                                                           AS ProductionStatusCode
                  , COALESCE(PT.ProdType, -1)                                                             AS ProductionTypeCode
                  , COALESCE(NULLIF(PT_ENUM.Description, ''), 'N/A')                                      AS ProductionTypeDescription
                  , COALESCE(NULLIF(PS_ENUM.Description, ''), 'N/A')                                      AS ProductionStatusDescription
                  , COALESCE(PT.SchedStatus, -1)                                                          AS SchedulingStatusCode
                  , COALESCE(NULLIF(SS_ENUM.Description, ''), 'N/A')                                      AS SchedulingStatusDescription
                  , COALESCE(PT.BackOrderStatus, -1)                                                      AS RemainStatusCode
                  , COALESCE(NULLIF(BOS_ENUM.Description, ''), 'N/A')                                     AS RemainStatusDescription
                  , COALESCE(NULLIF(PP.ProdPoolId, ''), 'N/A')                                            AS ProductionPoolCode
                  , COALESCE(NULLIF(PP.Name, ''), 'N/A')                                                  AS ProductionPoolDescription
                  , COALESCE(NULLIF(PP.ProdPoolId, '') + ' - ', '') + ISNULL(NULLIF(PP.Name, ''), 'N/A')  AS ProductionPoolCodeDescription
                  , COALESCE(NULLIF(PG.ProdGroupId, ''), 'N/A')                                           AS ProductionGroupCode
                  , COALESCE(NULLIF(PG.Name, ''), 'N/A')                                                  AS ProductionGroupDescription
                  , COALESCE(NULLIF(PG.ProdGroupId, '') + ' - ', '') + ISNULL(NULLIF(PG.Name, ''), 'N/A') AS ProductionGroupCodeDescription
    FROM            Stage_Ax.ProdTable AS PT
    LEFT OUTER JOIN Stage_Ax.ProdPool  AS PP ON PT.ProdPoolId          = PP.ProdPoolId
                                            AND PT.Company_Id          = PP.Company_Id
                                            AND PT.PARTITION           = PP.PARTITION
                                            AND PT.Data_Connection_Id  = PP.Data_Connection_Id
    LEFT OUTER JOIN Stage_Ax.ProdGroup AS PG ON PT.ProdGroupId         = PG.ProdGroupId
                                            AND PT.Company_Id          = PG.Company_Id
                                            AND PT.PARTITION           = PG.PARTITION
                                            AND PT.Data_Connection_Id  = PG.Data_Connection_Id
    LEFT OUTER JOIN Meta.Enumerations  PS_ENUM ON PS_ENUM.TableName    = 'PRODTABLE'
                                              AND PS_ENUM.ColumnName   = 'PRODSTATUS'
                                              AND PS_ENUM.OptionId     = PT.ProdStatus
    LEFT OUTER JOIN Meta.Enumerations  SS_ENUM ON SS_ENUM.TableName    = 'PRODTABLE'
                                              AND SS_ENUM.ColumnName   = 'SchedStatus'
                                              AND SS_ENUM.OptionId     = PT.SchedStatus
    LEFT OUTER JOIN Meta.Enumerations  BOS_ENUM ON BOS_ENUM.TableName  = 'PRODTABLE'
                                               AND BOS_ENUM.ColumnName = 'BackorderStatus'
                                               AND BOS_ENUM.OptionId   = PT.BackOrderStatus
    LEFT OUTER JOIN Meta.Enumerations  PT_ENUM ON PT_ENUM.TableName    = 'PRODTABLE'
                                              AND PT_ENUM.ColumnName   = 'ProdType'
                                              AND PT_ENUM.OptionId     = PT.ProdType ;
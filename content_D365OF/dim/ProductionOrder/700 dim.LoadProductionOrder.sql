EXEC Dbo.Drop_Object @Object = N'dim.LoadProductionOrder'
-- nvarchar(128)
                   , @Type = N'P'
-- nchar(2)
                   , @Debug = 0 ;
-- int
GO


CREATE PROCEDURE [dim].LoadProductionOrder
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @Deleted = 0 ;
  SET @Updated = 0 ;
  SET @Inserted = 0 ;

  UPDATE      Dim.ProductionOrder
     SET      StageId = POV.StageId
            , ComponentExecutionId = @Component_Execution_Id
            , ProductionStatusCode = POV.ProductionStatusCode
            , ProductionStatusDescription = POV.ProductionStatusDescription
            , ProductionTypeCode = POV.ProductionTypeCode
            , ProductionTypeDescription = POV.ProductionTypeDescription
            , SchedulingStatusCode = POV.SchedulingStatusCode
            , SchedulingStatusDescription = POV.SchedulingStatusDescription
            , RemainStatusCode = POV.RemainStatusCode
            , RemainStatusDescription = POV.RemainStatusDescription
            , ProductionPoolCode = POV.ProductionPoolCode
            , ProductionPoolDescription = POV.ProductionPoolDescription
            , ProductionPoolCodeDescription = POV.ProductionPoolCodeDescription
            , ProductionGroupCode = POV.ProductionGroupCode
            , ProductionGroupDescription = POV.ProductionGroupDescription
            , ProductionGroupCodeDescription = POV.ProductionGroupCodeDescription
    FROM      Dim.ProductionOrder     PO
   INNER JOIN Dim.ProductionOrderView POV ON POV.ProductionOrder  = PO.ProductionOrder
                                         AND POV.CompanyId        = PO.CompanyId
                                         AND POV.Partition        = PO.Partition
                                         AND POV.DataConnectionId = PO.DataConnectionId ;

  SELECT @Updated = @@RowCount ;

  INSERT INTO Dim.ProductionOrder (
    StageId
  , CompanyId
  , Partition
  , DataConnectionId
  , ComponentExecutionId
  , ProductionOrder
  , ProductionStatusCode
  , ProductionStatusDescription
  , SchedulingStatusCode
  , SchedulingStatusDescription
  , RemainStatusCode
  , RemainStatusDescription
  , ProductionTypeCode
  , ProductionTypeDescription
  , ProductionPoolCode
  , ProductionPoolDescription
  , ProductionPoolCodeDescription
  , ProductionGroupCode
  , ProductionGroupDescription
  , ProductionGroupCodeDescription
  )
  SELECT POV.StageId
       , POV.CompanyId
       , POV.Partition
       , POV.DataConnectionId
       , @Component_Execution_Id
       , POV.ProductionOrder
       , POV.ProductionStatusCode
       , POV.ProductionStatusDescription
       , POV.SchedulingStatusCode
       , POV.SchedulingStatusDescription
       , POV.RemainStatusCode
       , POV.RemainStatusDescription
       , POV.ProductionTypeCode
       , POV.ProductionTypeDescription
       , POV.ProductionPoolCode
       , POV.ProductionPoolDescription
       , POV.ProductionPoolCodeDescription
       , POV.ProductionGroupCode
       , POV.ProductionGroupDescription
       , POV.ProductionGroupCodeDescription
    FROM Dim.ProductionOrderView POV
   WHERE NOT EXISTS ( SELECT 1
                        FROM Dim.ProductionOrder PO
                       WHERE POV.ProductionOrder  = PO.ProductionOrder
                         AND POV.CompanyId        = PO.CompanyId
                         AND POV.Partition        = PO.Partition
                         AND POV.DataConnectionId = PO.DataConnectionId) ;

  SELECT @Inserted = @@RowCount ;
END ;

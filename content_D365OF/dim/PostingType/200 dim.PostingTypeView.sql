EXEC dbo.drop_object 'dim.PostingTypeView', 'V' ;
GO
CREATE VIEW [dim].[PostingTypeView]
AS
  SELECT ELT.OptionID    AS PostingTypeCode
       , ELT.Description AS PostingTypeDesc
       , ELT.DataConnectionID
    FROM meta.Enumerations AS ELT
   WHERE ELT.TableName  = 'MainAccount'
     AND ELT.ColumnName = 'PostingType' ;
EXEC Dbo.Drop_Object 'dim.RouteOperationView', 'V' ;
GO

CREATE VIEW [dim].[RouteOperationView]
AS
  SELECT            R.Stage_ID                                                                          AS StageID
                  , R.company_id                                                                        AS CompanyID
                  , R.Partition                                                                         AS Partition
                  , R.data_connection_id                                                                AS DataconnectionID
                  , COALESCE(NULLIF(R.ROUTEID, ''), 'N/A')                                              AS RouteCode
                  , COALESCE(NULLIF(rt.NAME, ''), 'N/A')                                                AS RouteDescription
                  , COALESCE(NULLIF(R.RouteiD, ''), 'N/A') + COALESCE(' - ' + NULLIF(rt.Name, ''), '')  AS RouteCodeDescription
                  , COALESCE(R.OPRPRIORITY, -1)                                                         AS Priority
                  , COALESCE(R.OPRNUM, -1)                                                              AS OperationNumber
                  , COALESCE(R.OPRNUMNEXT, -1)                                                          AS OperationNumberNext
                  , COALESCE(NULLIF(R.OPRID, ''), 'N/A')                                                AS OperationCode
                  , COALESCE(NULLIF(rot.NAME, ''), 'N/A')                                               AS OperationDescription
                  , COALESCE(NULLIF(R.[OprID], ''), 'N/A') + COALESCE(' - ' + NULLIF(rot.Name, ''), '') AS OperationCodeDescription
                  , COALESCE(R.LEVEL_, -1)                                                              AS RouteLevel
    FROM            Stage_Ax.ROUTE         AS R
    LEFT OUTER JOIN Stage_Ax.ROUTETABLE    rt ON rt.ROUTEID            = R.ROUTEID
                                             AND R.company_id          = rt.company_id
                                             AND R.PARTITION           = rt.PARTITION
                                             AND R.data_connection_id  = rt.data_connection_id
    LEFT OUTER JOIN Stage_Ax.ROUTEOPRTABLE rot ON rot.OPRID            = R.OPRID
                                              AND R.company_id         = rot.company_id
                                              AND R.PARTITION          = rot.PARTITION
                                              AND R.data_connection_id = rot.data_connection_id ;
-- ################################################################################ --
-- #####                         Table: dim.RouteOperation                           ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.RouteOperation', 'T' ;

CREATE TABLE dim.RouteOperation
(
  RouteOperationID         INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                  BIGINT        NULL
, CompanyID                INT           NOT NULL
, Partition                BIGINT        NOT NULL
, DataConnectionID         INT           NOT NULL
, ExecutionTimestamp       ROWVERSION    NOT NULL
, ComponentExecutionID     INT           NULL
, RouteCode                NVARCHAR(40)  NULL
, RouteDescription         NVARCHAR(240) NULL
, RouteCodeDescription     NVARCHAR(512) NULL
, Priority                 INT           NULL
, OperationNumber          INT           NULL
, OperationNumberNext      INT           NULL
, OperationCode            NVARCHAR(40)  NULL
, OperationDescription     NVARCHAR(120) NULL
, OperationCodeDescription NVARCHAR(255) NULL
, RouteLevel               INT           NULL
) ;

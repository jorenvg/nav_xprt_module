EXEC dbo.drop_object @object = N'dim.LoadForecastModel' -- nvarchar(128)
                   , @type = N'P'                       -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadForecastModel]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.ForecastModel
     SET      StageID = FV.StageID
            , ForecastModelDesc = FV.ForecastModelDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.ForecastModel     FM
   INNER JOIN dim.ForecastModelView FV ON FV.ForecastModelCode = FM.ForecastModelCode
                                      AND FV.DataConnectionID  = FM.DataConnectionID
                                      AND FV.Partition         = FM.Partition
                                      AND FV.CompanyID         = FM.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.ForecastModel (
    StageID, DataConnectionID, ComponentExecutionID, CompanyID, Partition, ForecastModelCode, ForecastModelDesc
  )
  SELECT FV.STageID
       , FV.DataConnectionID
       , @component_execution_id
       , FV.CompanyID
       , FV.Partition
       , FV.ForecastModelCode
       , FV.ForecastModelDesc
    FROM dim.ForecastModelView FV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.ForecastModel FM
                       WHERE FV.ForecastModelCode = FM.ForecastModelCode
                         AND FV.DataConnectionID  = FM.DataConnectionID
                         AND FV.Partition         = FM.Partition
                         AND FV.CompanyID         = FM.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;

END ;
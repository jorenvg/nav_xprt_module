EXEC dbo.drop_object 'dim.ForecastModelView', 'V' ;
GO

CREATE VIEW [dim].[ForecastModelView]
AS
  SELECT ISNULL(NULLIF(MODELID, ''), 'N/A') AS ForecastModelCode
       , ISNULL(NULLIF(TXT, ''), 'N/A')     AS ForecastModelDesc
       , company_id                         AS CompanyID
       , data_connection_id                 AS DataConnectionID
       , PARTITION                          AS Partition
       , stage_id                           AS StageID
    FROM stage_ax.FORECASTMODEL
   WHERE [Type] = 0 ;
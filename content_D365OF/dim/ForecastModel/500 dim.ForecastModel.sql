EXEC dbo.drop_object 'dim.ForecastModel', 'T' ;

CREATE TABLE dim.ForecastModel
(
  ForecastModelID      INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, ForecastModelCode    NVARCHAR(30)  NOT NULL
, ForecastModelDesc    NVARCHAR(100) NOT NULL
) ;
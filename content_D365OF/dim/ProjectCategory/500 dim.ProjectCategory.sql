EXEC dbo.drop_object 'dim.ProjectCategory', 'T' ;

CREATE TABLE dim.ProjectCategory
(
  ProjectCategoryID            INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY CLUSTERED
-- system
, StageID                      BIGINT        NULL
, DataConnectionID             INT           NOT NULL
, CompanyID                    INT           NOT NULL
, Partition                    BIGINT        NOT NULL
, ExecutionTimestamp           ROWVERSION    NOT NULL
, ComponentExecutionID         INT           NOT NULL
-- bk
, CategoryCode                 NVARCHAR(100) NOT NULL
-- attributes
, CategoryDescription          NVARCHAR(255) NOT NULL
, CategoryCodeDescription      NVARCHAR(500) NULL
, CategoryTypeCode             INT           NOT NULL
, CategoryTypeDescription      NVARCHAR(255) NOT NULL
, CategoryGroupCode            NVARCHAR(50)  NOT NULL
, CategoryGroupDescription     NVARCHAR(255) NOT NULL
, CategoryGroupCodeDescription NVARCHAR(500) NOT NULL
) ;
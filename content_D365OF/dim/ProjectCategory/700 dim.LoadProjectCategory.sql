EXEC dbo.drop_object @object = N'dim.LoadProjectCategory' -- nvarchar(128)
                   , @type = N'P'                         -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadProjectCategory
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.ProjectCategory
     SET      StageID = PCV.StageID
            , CategoryDescription = PCV.CategoryDescription
            , CategoryCodeDescription = PCV.CategoryCodeDescription
            , CategoryTypeCode = PCV.CategoryTypeCode
            , CategoryTypeDescription = PCV.CategoryTypeDescription
            , CategoryGroupCode = PCV.CategoryGroupCode
            , CategoryGroupDescription = PCV.CategoryGroupDescription
            , CategoryGroupCodeDescription = PCV.CategoryGroupCodeDescription
            , ComponentExecutionID = @component_execution_id
    FROM      dim.ProjectCategory     AS PC
   INNER JOIN dim.ProjectCategoryView AS PCV ON PCV.CategoryCode     = PC.CategoryCode
                                            AND PCV.CompanyID        = PC.CompanyID
                                            AND PCV.Partition        = PC.Partition
                                            AND PCV.DataConnectionID = PC.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.ProjectCategory (
    StageID
  , DataConnectionID
  , CompanyID
  , Partition
  , ComponentExecutionID
  , CategoryCode
  , CategoryDescription
  , CategoryCodeDescription
  , CategoryTypeCode
  , CategoryTypeDescription
  , CategoryGroupCode
  , CategoryGroupDescription
  , CategoryGroupCodeDescription
  )
  SELECT PCV.StageID
       , PCV.DataConnectionID
       , PCV.CompanyID
       , PCV.Partition
       , @component_execution_id
       , PCV.CategoryCode
       , PCV.CategoryDescription
       , PCV.CategoryCodeDescription
       , PCV.CategoryTypeCode
       , PCV.CategoryTypeDescription
       , PCV.CategoryGroupCode
       , PCV.CategoryGroupDescription
       , PCV.CategoryGroupCodeDescription
    FROM dim.ProjectCategoryView AS PCV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.ProjectCategory AS PC
                       WHERE PCV.CategoryCode     = PC.CategoryCode
                         AND PCV.CompanyID        = PC.CompanyID
                         AND PCV.Partition        = PC.Partition
                         AND PCV.DataConnectionID = PC.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
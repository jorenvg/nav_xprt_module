/***
This view always only shows a single currency, the current reporting currency set in the meta.instances.
This can contain multiple records if the more than one partition is loaded is specified.
***/
EXEC dbo.drop_object @object = N'dim.CurrencyReport', @type = N'V' ;
GO
CREATE VIEW dim.CurrencyReport
AS
  SELECT c.CurrencyID
       , c.DataConnectionID
       , c.ExecutionTimestamp
       , c.ComponentExecutionID
       , c.Partition
       , c.Code
       , c.Description
       , c.CodeDesc
    FROM dim.Currency AS c
   WHERE EXISTS (SELECT 1 FROM meta.current_instance WHERE reporting_currency_code = c.Code) ;
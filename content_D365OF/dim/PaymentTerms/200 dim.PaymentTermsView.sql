EXEC dbo.drop_object 'dim.PaymentTermsView', 'V' ;
GO

CREATE VIEW [dim].[PaymentTermsView]
AS
  SELECT ISNULL(NULLIF(PT.PAYMTERMID, ''), 'N/A')                       AS PaymentTermsCode
       , ISNULL(NULLIF(PT.DESCRIPTION, ''), 'N/A')                      AS PaymentTermsDesc
       , ISNULL(NULLIF(PT.PAYMTERMID, '') + ' - ', '') + PT.DESCRIPTION AS PaymentTermsCodeDesc
       , ISNULL(NULLIF(PT.NUMOFDAYS, -1), -1)                           AS NumberOfDays
       , PT.company_id                                                  AS CompanyID
       , PT.data_connection_id                                          AS DataConnectionID
       , PT.Partition                                                   AS Partition
       , PT.stage_id                                                    AS StageID
    FROM stage_ax.PAYMTERM AS PT ;




-- ################################################################################ --
-- #####                         Table: dim.PaymentTerms                      ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.PaymentTerms', 'T' ;

CREATE TABLE dim.PaymentTerms
(
  PaymentTermsID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, PaymentTermsCode     NVARCHAR(512) NULL
, PaymentTermsDesc     NVARCHAR(512) NULL
, PaymentTermsCodeDesc NVARCHAR(512) NULL
, NumberOfDays         INT           NULL
) ;
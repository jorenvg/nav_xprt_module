EXEC dbo.drop_object @object = N'dim.LoadPaymentTerms' -- nvarchar(128)
                   , @type = N'P'                      -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadPaymentTerms
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.PaymentTerms
     SET      StageID = PTV.StageID
            , PaymentTermsDesc = PTV.PaymentTermsDesc
            , PaymentTermsCodeDesc = PTV.PaymentTermsCodeDesc
            , NumberOfDays = PTV.NumberOfDays
            , ComponentExecutionID = @component_execution_id
    FROM      dim.PaymentTerms     PT
   INNER JOIN dim.PaymentTermsView PTV ON PTV.PaymentTermsCode = PT.PaymentTermsCode
                                      AND PTV.CompanyID        = PT.CompanyID
                                      AND PTV.Partition        = PT.Partition
                                      AND PTV.DataConnectionID = PT.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.PaymentTerms (
    StageID, DataConnectionID, CompanyID, Partition, ComponentExecutionID, PaymentTermsCode, PaymentTermsDesc, PaymentTermsCodeDesc, NumberOfDays
  )
  SELECT PTV.StageID
       , PTV.DataConnectionID
       , PTV.CompanyID
       , PTV.Partition
       , @component_execution_id
       , PTV.PaymentTermsCode
       , PTV.PaymentTermsDesc
       , PTV.PaymentTermsCodeDesc
       , PTV.NumberOfDays
    FROM dim.PaymentTermsView PTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.PaymentTerms PT
                       WHERE PTV.PaymentTermsCode = PT.PaymentTermsCode
                         AND PTV.CompanyID        = PT.CompanyID
                         AND PTV.Partition        = PT.Partition
                         AND PTV.DataConnectionID = PT.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
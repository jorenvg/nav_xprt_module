EXEC dbo.drop_object @object = N'dim.AgeGroupView', @type = N'V' ;
GO
CREATE VIEW dim.AgeGroupView
AS
  SELECT '0-30' AS Code, 'less then 30 days' AS [Desc], -2147483648 AS [Min], 30 AS [Max]
  UNION
  SELECT '31-60' AS Code, 'between the 31 and 60 days' AS [Desc], 31 AS [Min], 60 AS [Max]
  UNION
  SELECT '61-90', 'between the 61 and 90 days' AS [Desc], 61 AS [Min], 90 AS [Max]
  UNION
  SELECT '91+' AS Code, 'more than 91 days' AS [Desc], 91 AS [Min], 2147483647 AS [Max] ;
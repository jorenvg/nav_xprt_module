EXEC dbo.drop_object 'dim.SalesOrderType', 'T' ;

CREATE TABLE dim.SalesOrderType
(
  SalesOrderTypeID     INT        IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT        NOT NULL
, ExecutionTimestamp   ROWVERSION NOT NULL
, ComponentExecutionID INT        NOT NULL
, SalesOrderTypeCode   INT        NOT NULL
, SalesOrderTypeDesc   NVARCHAR(128)
) ;
EXEC dbo.drop_object 'dim.SalesOrderTypeView', 'V' ;
GO

CREATE VIEW [dim].[SalesOrderTypeView]
AS
  SELECT ISNULL(OptionID, -1)                     AS SalesOrderTypeCode
       , ISNULL(NULLIF([Description], ''), 'N/A') AS SalesOrderTypeDesc
       , DataConnectionID
    FROM meta.enumerations
   WHERE TableName  = 'Salesline'
     AND ColumnName = 'SalesType' ;

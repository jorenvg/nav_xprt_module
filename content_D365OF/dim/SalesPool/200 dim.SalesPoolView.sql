EXEC dbo.drop_object 'dim.SalesPoolView', 'V' ;
GO

CREATE VIEW [dim].[SalesPoolView]
AS
  SELECT ISNULL(NULLIF(SALESPOOLID, ''), 'N/A')                                           AS SalesPoolCode
       , ISNULL(NULLIF(NAME, ''), 'N/A')                                                  AS SalesPoolDesc
       , ISNULL(NULLIF(SALESPOOLID, ''), 'N/A') + ISNULL(' - ' + NULLIF(NAME, ''), 'N/A') AS SalesPoolCodeDesc
       , data_connection_id                                                               AS DataConnectionID
       , company_id                                                                       AS CompanyID
       , Partition                                                                        AS Partition
       , stage_id                                                                         AS StageID
    FROM stage_ax.SALESPOOL ;
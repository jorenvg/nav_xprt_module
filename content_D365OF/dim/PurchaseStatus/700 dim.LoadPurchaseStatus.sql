EXEC dbo.drop_object @object = N'dim.LoadPurchaseStatus' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadPurchaseStatus
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.PurchaseStatus
     SET      PurchaseStatusDesc = PSV.PurchaseStatusDesc
            , PurchaseStatusCodeDesc = PSV.PurchaseStatusCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.PurchaseStatus     PS
   INNER JOIN dim.PurchaseStatusView PSV ON PSV.PurchaseStatusCode = PS.PurchaseStatusCode
                                        AND PSV.DataConnectionID   = PS.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.PurchaseStatus (
    DataConnectionID, ComponentExecutionID, PurchaseStatusCode, PurchaseStatusDesc, PurchaseStatusCodeDesc
  )
  SELECT PSV.DataConnectionID
       , @component_execution_id
       , PSV.PurchaseStatusCode
       , PSV.PurchaseStatusDesc
       , PSV.PurchaseStatusCodeDesc
    FROM dim.PurchaseStatusView PSV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.PurchaseStatus PS
                       WHERE PSV.PurchaseStatusCode = PS.PurchaseStatusCode
                         AND PSV.DataConnectionID   = PS.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
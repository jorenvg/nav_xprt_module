EXEC dbo.drop_object 'dim.PurchaseStatusView', 'V' ;
GO

CREATE VIEW [dim].[PurchaseStatusView]
AS
  SELECT ISNULL(NULLIF(ENUM.OptionID, -1), -1)                                                                              AS PurchaseStatusCode
       , ISNULL(NULLIF(ENUM.DESCRIPTION, ''), 'N/A')                                                                        AS PurchaseStatusDesc
       , ISNULL(NULLIF(CONVERT(VARCHAR(255), ENUM.OptionID), '') + ' - ', '') + ISNULL(NULLIF(ENUM.DESCRIPTION, ''), 'N/A') AS PurchaseStatusCodeDesc
       , ENUM.DataConnectionID                                                                                              AS DataConnectionID
    FROM meta.enumerations AS ENUM
   WHERE ENUM.TableName  = 'PURCHTABLE'
     AND ENUM.ColumnName = 'PURCHSTATUS' ;

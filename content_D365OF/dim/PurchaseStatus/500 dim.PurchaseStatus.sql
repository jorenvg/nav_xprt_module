
-- ################################################################################ --
-- #####                         Table: dim.PurchaseStatus                    ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.PurchaseStatus', 'T' ;

CREATE TABLE dim.PurchaseStatus
(
  PurchaseStatusID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID       INT           NOT NULL
, ExecutionTimestamp     ROWVERSION    NOT NULL
, ComponentExecutionID   INT           NOT NULL
, PurchaseStatusCode     INT           NOT NULL
, PurchaseStatusDesc     NVARCHAR(255) NOT NULL
, PurchaseStatusCodeDesc NVARCHAR(512) NULL
) ;
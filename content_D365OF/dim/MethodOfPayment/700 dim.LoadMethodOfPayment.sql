EXEC dbo.drop_object @object = N'dim.LoadMethodOfPayment' -- nvarchar(128)
                   , @type = N'P'                         -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadMethodOfPayment]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.MethodOfPayment
     SET      StageID = MPV.StageID
            , MethodOfPaymentDesc = MPV.MethodOfPaymentDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.MethodOfPayment     MP
   INNER JOIN dim.MethodOfPaymentView MPV ON MPV.MethodOfPaymentCode = MP.MethodOfPaymentCode
                                         AND MPV.DataConnectionID    = MP.DataConnectionID
                                         AND MPV.Partition           = MP.Partition
                                         AND MPV.CompanyID           = MP.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.MethodOfPayment (
    StageID, DataConnectionID, ComponentExecutionID, CompanyID, Partition, MethodOfPaymentCode, MethodOfPaymentDesc
  )
  SELECT MPV.StageID
       , MPV.DataConnectionID
       , @component_execution_id
       , MPV.CompanyID
       , MPV.Partition
       , MPV.MethodOfPaymentCode
       , MPV.MethodOfPaymentDesc
    FROM dim.MethodOfPaymentView MPV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.MethodOfPayment MP
                       WHERE MPV.MethodOfPaymentCode = MP.MethodOfPaymentCode
                         AND MPV.DataConnectionID    = MP.DataConnectionID
                         AND MPV.Partition           = MP.Partition
                         AND MPV.CompanyID           = MP.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
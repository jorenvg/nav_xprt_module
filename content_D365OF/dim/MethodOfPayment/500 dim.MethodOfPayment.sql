EXEC dbo.drop_object 'dim.MethodOfPayment', 'T' ;

CREATE TABLE dim.MethodOfPayment
(
  MethodOfPaymentID    INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, MethodOfPaymentCode  NVARCHAR(30)  NOT NULL
, MethodOfPaymentDesc  NVARCHAR(128) NOT NULL
) ;
EXEC dbo.drop_object @object = N'dim.TimeIntelligenceView', @type = N'V' ;
GO
CREATE VIEW dim.TimeIntelligenceView
AS
  SELECT 'YTD' AS Code, 'Year to date' AS [Desc], 1 AS [SortBy]
  UNION
  SELECT 'LYTD' AS Code, 'Last year to date' AS [Desc], 2 AS [SortBy]
  UNION
  SELECT 'MTD' AS Code, 'Month to date' AS [Desc], 3 AS [SortBy]
  UNION
  SELECT 'LYMTD' AS Code, 'Last year month to date' AS [Desc], 4 AS [SortBy]
  UNION
  SELECT 'RY' AS Code, 'Rolling year' AS [Desc], 5 AS [SortBy]
  UNION
  SELECT 'SPLY' AS Code, 'Same period last year' AS [Desc], 6 AS [SortBy]
  UNION
  SELECT 'PP' AS Code, 'Parallel period' AS [Desc], 7 AS [SortBy] ;


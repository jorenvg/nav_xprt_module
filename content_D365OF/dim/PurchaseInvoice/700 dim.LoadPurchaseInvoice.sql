EXEC dbo.drop_object @object = N'dim.LoadPurchaseInvoice' -- nvarchar(128)
                   , @type = N'P'                         -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadPurchaseInvoice
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesInvoice
     SET      ComponentExecutionID = @component_execution_id
            , InvoiceDate = PINV.InvoiceDate
            , DueDate = PINV.DueDate
            , ClosedDate = PINV.ClosedDate
    FROM      dim.SalesInvoice     PIN
   INNER JOIN dim.SalesInvoiceView PINV ON PINV.SalesInvoice     = PIN.SalesInvoice
                                      AND PINV.CompanyID        = PIN.CompanyID
                                      AND PINV.Partition        = PIN.Partition
                                      AND PINV.DataConnectionID = PIN.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.PurchaseInvoice (
    DataConnectionID, CompanyID, Partition, ComponentExecutionID, PurchaseInvoice, InvoiceDate, DueDate, ClosedDate
  )
  SELECT PINV.DataConnectionID
       , PINV.CompanyID
       , PINV.Partition
       , @component_execution_id
       , PINV.PurchaseInvoice
       , PINV.InvoiceDate
       , PINV.DueDate
       , PINV.ClosedDate
     FROM dim.PurchaseInvoiceView AS PINV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.PurchaseInvoice AS PIN
                       WHERE PINV.PurchaseInvoice  = PIN.PurchaseInvoice
                         AND PINV.CompanyID        = PIN.CompanyID
                         AND PINV.Partition        = PIN.Partition
                         AND PINV.DataConnectionID = PIN.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
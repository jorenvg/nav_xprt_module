
-- ################################################################################ --
-- #####                         Table: dim.PurchaseInvoice                   ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.PurchaseInvoice', 'T' ;

CREATE TABLE dim.PurchaseInvoice
(
  PurchaseInvoiceID    INT          IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT          NOT NULL
, CompanyID            INT          NOT NULL
, Partition            BIGINT       NOT NULL
, ExecutionTimestamp   ROWVERSION   NOT NULL
, ComponentExecutionID INT          NOT NULL
, PurchaseInvoice      NVARCHAR(50) NOT NULL
, InvoiceDate          DATETIME     NOT NULL
, ClosedDate           DATETIME     NOT NULL
, DueDate              DATETIME     NOT NULL
) ;
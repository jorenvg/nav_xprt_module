EXEC dbo.drop_object 'dim.SalesTaxDirectionView', 'V' ;
GO
CREATE VIEW [dim].[SalesTaxDirectionView]
AS
  SELECT ELT.OptionID    AS SalesTaxDirectionCode
       , ELT.Description AS SalesTaxDirectionDesc
       , ELT.DataConnectionID
    FROM meta.Enumerations AS ELT
   WHERE ELT.TableName  = 'TaxTrans'
     AND ELT.ColumnName = 'TaxDirection' ;

-- ################################################################################ --
-- #####                         Table: dim.SalesTaxDirection                 ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.SalesTaxDirection', 'T' ;
GO
CREATE TABLE [dim].[SalesTaxDirection]
(
  SalesTaxDirectionID   INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID      INT           NOT NULL
, ExecutionTimestamp    ROWVERSION    NOT NULL
, ComponentExecutionID  INT           NOT NULL
, SalesTaxDirectionCode INT           NOT NULL
, SalesTaxDirectionDesc NVARCHAR(128) NULL
) ;
GO
/***
  Supports Source: Only D365 For Operations and Finance Plaform Version 10+. 
    - Field DLVMODE.SHIPCARRIERTYPE cannot be exported in an entity
***/

EXEC dbo.drop_object @object = N'dim.LoadModeOfDelivery' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadModeOfDelivery
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.ModeOfDelivery
     SET      StageID = MDV.StageID
            , ModeOfDeliveryDesc = MDV.ModeOfDeliveryDesc
            , ModeOfDeliveryCodeDesc = MDV.ModeOfDeliveryCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.ModeOfDelivery     AS MD
   INNER JOIN dim.ModeOfDeliveryView AS MDV ON MDV.ModeOfDeliveryCode = MD.ModeOfDeliveryCode
                                           AND MDV.Partition          = MD.Partition
                                           AND MDV.DataConnectionID   = MD.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.ModeOfDelivery (
    StageID
  , DataConnectionID
  , CompanyID
  , Partition
  , ComponentExecutionID
  , ModeOfDeliveryCode
  , ModeOfDeliveryDesc
  , ModeOfDeliveryCodeDesc
  )
  SELECT MDV.StageID
       , MDV.DataConnectionID
       , MDV.CompanyID
       , MDV.Partition
       , @component_execution_id
       , MDV.ModeOfDeliveryCode
       , MDV.ModeOfDeliveryDesc
       , MDV.ModeOfDeliveryCodeDesc
    FROM dim.ModeOfDeliveryView AS MDV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.ModeOfDelivery AS MD
                       WHERE MDV.ModeOfDeliveryCode = MD.ModeOfDeliveryCode
                         AND MDV.CompanyID          = MD.CompanyID
                         AND MDV.Partition          = MD.Partition
                         AND MDV.DataConnectionID   = MD.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
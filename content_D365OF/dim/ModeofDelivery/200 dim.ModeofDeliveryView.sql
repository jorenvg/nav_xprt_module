/***
  Supports Source: Only D365 For Operations and Finance Plaform Version 10+. 
    - Field DLVMODE.SHIPCARRIERTYPE cannot be exported in an entity
***/

EXEC dbo.drop_object 'dim.ModeOfDeliveryView', 'V' ;
GO

CREATE VIEW [dim].[ModeOfDeliveryView]
AS
  SELECT DISTINCT
         ISNULL(NULLIF(DM.CODE, ''), 'N/A')                                      AS ModeOfDeliveryCode
       , ISNULL(NULLIF(DM.TXT, ''), 'N/A')                                       AS ModeOfDeliveryDesc
       , ISNULL(NULLIF(CONVERT(VARCHAR(255), DM.CODE), '') + ' - ', '') + DM.TXT AS ModeOfDeliveryCodeDesc
       , DM.company_id                                                           AS CompanyID
       , DM.PARTITION                                                            AS Partition
       , DM.data_connection_id                                                   AS DataConnectionID
       , DM.stage_id                                                             AS StageID
    FROM stage_ax.DLVMODE AS DM ;

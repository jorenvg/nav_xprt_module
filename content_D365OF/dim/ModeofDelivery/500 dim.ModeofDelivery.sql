

/***
################################################################################
#####                         Table: dim.ModeOfDelivery                    #####
################################################################################

  Supports Source: Only D365 For Operations and Finance Plaform Version 10+. 
	  - Field DLVMODE.SHIPCARRIERTYPE cannot be exported in an entity
***/

EXEC dbo.drop_object 'dim.ModeOfDelivery', 'T' ;

CREATE TABLE dim.ModeOfDelivery
(
  ModeOfDeliveryID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                BIGINT        NULL
, DataConnectionID       INT           NOT NULL
, CompanyID              INT           NOT NULL
, Partition              BIGINT        NOT NULL
, ExecutionTimestamp     ROWVERSION    NOT NULL
, ComponentExecutionID   INT           NOT NULL
, ModeOfDeliveryCode     NVARCHAR(512) NOT NULL
, ModeOfDeliveryDesc     NVARCHAR(512) NOT NULL
, ModeOfDeliveryCodeDesc NVARCHAR(512) NULL
) ;
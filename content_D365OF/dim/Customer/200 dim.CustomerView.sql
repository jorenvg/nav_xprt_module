EXEC dbo.drop_object 'dim.CustomerView', 'V' ;
GO

CREATE VIEW [dim].[CustomerView]
AS
  SELECT            DISTINCT
                    ISNULL(NULLIF(CT.ACCOUNTNUM, ''), 'N/A')                                                                              AS CustomerAccountNo
                  , ISNULL(NULLIF(DPT.NAME, ''), 'N/A')                                                                                   AS CustomerAccountName
                  , ISNULL(NULLIF(CT.ACCOUNTNUM, '') + ' - ', '') + ISNULL(NULLIF(DPT.NAME, ''), 'N/A')                                   AS CustomerAccountNoName
                  , ISNULL(NULLIF(CT.CUSTGROUP, ''), 'N/A')                                                                               AS CustomerGroupCode
                  , ISNULL(NULLIF(CG.NAME, ''), 'N/A')                                                                                    AS CustomerGroupDesc
                  , ISNULL(NULLIF(CT.CUSTGROUP, '') + ' - ', '') + ISNULL(NULLIF(CG.NAME, ''), 'N/A')                                     AS CustomerGroupCodeDesc
                  , ISNULL(NULLIF(CT.CUSTCLASSIFICATIONID, ''), 'N/A')                                                                    AS CustomerClassificationGroupCode
                  , ISNULL(NULLIF(CCG.TXT, ''), 'N/A')                                                                                    AS CustomerClassificationGroupDesc
                  , ISNULL(NULLIF(CT.CUSTCLASSIFICATIONID, '') + ' - ', '') + ISNULL(NULLIF(CCG.TXT, ''), 'N/A')                          AS CustomerClassificationGroupCodeDesc
                  , ISNULL(NULLIF(LPA.ADDRESS, ''), 'N/A')                                                                                AS CustomerAddress
                  , ISNULL(NULLIF(LPA.CITY, ''), 'N/A')                                                                                   AS CustomerCity
                  , ISNULL(NULLIF(LPA.STREET, ''), 'N/A')                                                                                 AS CustomerStreet
                  , ISNULL(NULLIF(LPA.ZIPCODE, ''), 'N/A')                                                                                AS CustomerZipCode
                  , ISNULL(NULLIF(LPA.COUNTRYREGIONID, ''), 'N/A')                                                                        AS CustomerCountryCode
                  , ISNULL(NULLIF(CRT.SHORTNAME, ''), 'N/A')                                                                              AS CustomerCountryDesc
                  , ISNULL(NULLIF(CONVERT(VARCHAR(255), LPA.COUNTRYREGIONID), '') + ' - ', '') + ISNULL(NULLIF(CRT.SHORTNAME, ''), 'N/A') AS CustomerCountryCodeDesc
                  , ISNULL(NULLIF(CT.STATISTICSGROUP, ''), 'N/A')                                                                         AS CustomerStatisticsGroupCode
                  , ISNULL(NULLIF(CSG.STATGROUPNAME, ''), 'N/A')                                                                          AS CustomerStatisticsGroupDesc
                  , ISNULL(NULLIF(CT.STATISTICSGROUP, '') + ' - ', '') + ISNULL(NULLIF(CSG.STATGROUPNAME, ''), 'N/A')                     AS CustomerStatisticsGroupCodeDesc
                  , ISNULL(NULLIF(CT.SEGMENTID, ''), 'N/A')                                                                               AS CustomerSegmentCode
                  , ISNULL(NULLIF(SG.DESCRIPTION, ''), 'N/A')                                                                             AS CustomerSegmentDesc
                  , ISNULL(NULLIF(CT.SEGMENTID, '') + ' - ', '') + ISNULL(NULLIF(SG.DESCRIPTION, ''), 'N/A')                              AS CustomerSegmentCodeDesc
                  , ISNULL(NULLIF(CT.SUBSEGMENTID, ''), 'N/A')                                                                            AS CustomerSubSegmentCode
                  , ISNULL(NULLIF(SSG.SUBSEGMENTDESCRIPTION, ''), 'N/A')                                                                  AS CustomerSubSegmentDesc
                  , ISNULL(NULLIF(CT.SUBSEGMENTID, '') + ' - ', '') + ISNULL(NULLIF(SSG.SUBSEGMENTDESCRIPTION, ''), 'N/A')                AS CustomerSubSegmentCodeDesc
                  , ISNULL(NULLIF(CT.COMPANYCHAINID, ''), 'N/A')                                                                          AS CustomerCompanyChainCode
                  , ISNULL(NULLIF(CH.DESCRIPTION, ''), 'N/A')                                                                             AS CustomerCompanyChainDesc
                  , ISNULL(NULLIF(CT.COMPANYCHAINID, '') + ' - ', '') + ISNULL(NULLIF(CH.DESCRIPTION, ''), 'N/A')                         AS CustomerCompanyChainCodeDesc
                  , ISNULL(NULLIF(CT.SALESDISTRICTID, ''), 'N/A')                                                                         AS CustomerSalesDistrictCode
                  , ISNULL(NULLIF(SSD.DESCRIPTION, ''), 'N/A')                                                                            AS CustomerSalesDistrictDesc
                  , ISNULL(NULLIF(CT.SALESDISTRICTID, '') + ' - ', '') + ISNULL(NULLIF(SSD.DESCRIPTION, ''), 'N/A')                       AS CustomerSalesDistrictCodeDesc
                  , ISNULL(NULLIF(CT.COMMISSIONGROUP, ''), 'N/A')                                                                         AS CustomerCommissionGroupCode
                  , ISNULL(NULLIF(COCG.NAME, ''), 'N/A')                                                                                  AS CustomerCommissionGroupDesc
                  , ISNULL(NULLIF(CT.COMMISSIONGROUP, '') + ' - ', '') + ISNULL(NULLIF(COCG.NAME, ''), 'N/A')                             AS CustomerCommissionGroupCodeDesc
                  , ISNULL(NULLIF(BRT.BUSRELTYPEID, ''), 'N/A')                                                                           AS CustomerType
                  , CT.company_id                                                                                                         AS CompanyID
                  , CT.data_connection_id                                                                                                 AS DataConnectionID
                  , CT.Partition                                                                                                          AS Partition
                  , CT.stage_id                                                                                                           AS StageID
    FROM            stage_ax.CUSTTABLE                                CT
    LEFT OUTER JOIN stage_ax.DIRPARTYTABLE                            DPT ON CT.PARTY                = DPT.RECID
                                                                         AND CT.PARTITION            = DPT.PARTITION
    LEFT OUTER JOIN stage_ax.CUSTGROUP                                CG ON CT.CUSTGROUP             = CG.CUSTGROUP
                                                                        AND CT.DATAAREAID            = CG.DATAAREAID
                                                                        AND CT.PARTITION             = CG.PARTITION
    LEFT OUTER JOIN stage_ax.CUSTCLASSIFICATIONGROUP                  CCG ON CT.CUSTCLASSIFICATIONID = CCG.CODE
                                                                         AND CT.DATAAREAID           = CCG.DATAAREAID
                                                                         AND CT.PARTITION            = CCG.PARTITION
    LEFT OUTER JOIN stage_ax.DIRPARTYTABLE                            DPTT ON DPTT.RECID             = CT.PARTY
                                                                          AND CT.PARTITION           = DPTT.PARTITION
    LEFT OUTER JOIN stage_ax.LOGISTICSLOCATION                        LC ON LC.RECID                 = DPTT.PRIMARYADDRESSLOCATION
                                                                        AND LC.PARTITION             = DPTT.PARTITION
    LEFT OUTER JOIN stage_ax.LOGISTICSPOSTALADDRESS                   LPA ON LPA.LOCATION            = LC.RECID
                                                                         AND GETDATE() BETWEEN LPA.VALIDFROM AND LPA.VALIDTO
                                                                         AND LPA.PARTITION           = LC.PARTITION
    LEFT OUTER JOIN stage_ax.LOGISTICSADDRESSCOUNTRYREGIONTRANSLATION CRT ON LPA.COUNTRYREGIONID     = CRT.COUNTRYREGIONID
                                                                         AND CT.PARTITION            = CRT.PARTITION
                                                                         AND CRT.LANGUAGEID          = (SELECT language_description FROM meta.current_instance)
    LEFT OUTER JOIN stage_ax.CUSTSTATISTICSGROUP                      CSG ON CT.STATISTICSGROUP      = CSG.CUSTSTATISTICSGROUP
                                                                         AND CT.DATAAREAID           = CSG.DATAAREAID
                                                                         AND CT.PARTITION            = CSG.PARTITION
    LEFT OUTER JOIN stage_ax.SMMBUSRELSEGMENTGROUP                    SG ON CT.SEGMENTID             = SG.SEGMENTID
                                                                        AND CT.DATAAREAID            = SG.DATAAREAID
                                                                        AND CT.PARTITION             = SG.PARTITION
    LEFT OUTER JOIN stage_ax.SMMBUSRELSUBSEGMENTGROUP                 SSG ON CT.SEGMENTID            = SSG.SEGMENTID
                                                                         AND CT.SUBSEGMENTID         = SSG.SUBSEGMENTID
                                                                         AND CT.DATAAREAID           = SSG.DATAAREAID
                                                                         AND CT.PARTITION            = SSG.PARTITION
    LEFT OUTER JOIN stage_ax.SMMBUSRELCHAINGROUP                      CH ON CT.COMPANYCHAINID        = CH.CHAINID
                                                                        AND CT.DATAAREAID            = CH.DATAAREAID
                                                                        AND CT.PARTITION             = CH.PARTITION
    LEFT OUTER JOIN stage_ax.SMMBUSRELSALESDISTRICTGROUP              SSD ON CT.SALESDISTRICTID      = SSD.SALESDISTRICTID
                                                                         AND CT.DATAAREAID           = SSD.DATAAREAID
                                                                         AND CT.PARTITION            = SSD.PARTITION
    LEFT OUTER JOIN stage_ax.COMMISSIONCUSTOMERGROUP                  COCG ON CT.COMMISSIONGROUP     = COCG.GROUPID
                                                                          AND CT.DATAAREAID          = COCG.DATAAREAID
                                                                          AND CT.PARTITION           = COCG.PARTITION
    LEFT OUTER JOIN stage_ax.SMMBUSRELTABLE                           BRT ON CT.PARTY                = BRT.PARTY
                                                                         AND CT.DATAAREAID           = BRT.DATAAREAID
                                                                         AND CT.PARTITION            = BRT.PARTITION ;


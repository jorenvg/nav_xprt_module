EXEC dbo.drop_object @object = N'dim.LoadWMSLocation' -- nvarchar(128)
                   , @type = N'P'                     -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadWMSLocation
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.WMSLocation
     SET      StageID = WMSL.StageID
            , WMSLocationDesc = WMSL.WMSLocationDesc
            , WMSLocationCodeDesc = WMSL.WMSLocationCode
            , WMSAisleNo = WMSL.WMSAisleNo
            , WMSAisle = WMSL.WMSAisle
            , ComponentExecutionID = @component_execution_id
    FROM      dim.WMSLocation     WMSL
   INNER JOIN dim.WMSLocationView WMSLV ON WMSLV.WMSLocationCode  = WMSL.WMSLocationCode
                                       AND WMSLV.LocationCode     = WMSL.LocationCode
                                       AND WMSLV.CompanyID        = WMSL.CompanyID
                                       AND WMSLV.Partition        = WMSL.Partition
                                       AND WMSLV.DataConnectionID = WMSL.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.WMSLocation (
    StageID, DataConnectionID, CompanyID, Partition, ComponentExecutionID, WMSLocationCode, WMSLocationDesc, WMSLocationCodeDesc, LocationCode, WMSAisle, WMSAisleNo
  )
  SELECT WMSLV.StageID
       , WMSLV.DataConnectionID
       , WMSLV.CompanyID
       , WMSLV.Partition
       , @component_execution_id
       , WMSLV.WMSLocationCode
       , WMSLV.WMSLocationDesc
       , WMSLV.WMSLocationCodeDesc
       , WMSLV.LocationCode
       , WMSLV.WMSAisle
       , WMSLV.WMSAisleNo
    FROM dim.WMSLocationView WMSLV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.WMSLocation WMSL
                       WHERE WMSLV.WMSLocationCode  = WMSL.WMSLocationCode
                         AND WMSLV.LocationCode     = WMSL.LocationCode
                         AND WMSLV.CompanyID        = WMSL.CompanyID
                         AND WMSLV.Partition        = WMSL.Partition
                         AND WMSLV.DataConnectionID = WMSL.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
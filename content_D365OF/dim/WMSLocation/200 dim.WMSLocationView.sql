EXEC dbo.drop_object 'dim.WMSLocationView', 'V' ;
GO

CREATE VIEW [dim].[WMSLocationView]
AS
  SELECT            ISNULL(NULLIF(WMSL.CHECKTEXT, ''), 'N/A')                                                      AS WMSLocationDesc
                  , ISNULL(NULLIF(WMSL.WMSLOCATIONID, '') + ' - ', '') + ISNULL(NULLIF(WMSL.CHECKTEXT, ''), 'N/A') AS WMSLocationCodeDesc
                  , ISNULL(NULLIF(WMSA.AISLEID, ''), 'N/A')                                                        AS WMSAisle
                  , ISNULL(NULLIF(WMSA.AISLENO, -1), -1)                                                           AS WMSAisleNo
                  , ISNULL(NULLIF(WMSL.INVENTLOCATIONID, ''), 'N/A')                                               AS LocationCode    --BK     
                  , ISNULL(NULLIF(WMSL.WMSLOCATIONID, ''), 'N/A')                                                  AS WMSLocationCode --BK   
                  , WMSL.company_id                                                                                AS CompanyID       --BK   
                  , WMSL.Partition                                                                                 AS Partition
                  , WMSL.data_connection_id                                                                        AS DataConnectionID
                  , WMSL.stage_id                                                                                  AS StageID
    FROM            stage_ax.WMSLOCATION AS WMSL
    LEFT OUTER JOIN stage_ax.WMSAISLE    WMSA ON WMSA.AISLEID          = WMSL.AISLEID
                                             AND WMSA.INVENTLOCATIONID = WMSL.INVENTLOCATIONID
                                             AND WMSA.DATAAREAID       = WMSL.DATAAREAID
                                             AND WMSA.PARTITION        = WMSL.PARTITION ;



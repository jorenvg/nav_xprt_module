EXEC dbo.drop_object 'dim.TaxExemptView', 'V' ;
GO

CREATE VIEW [dim].[TaxExemptView]
AS
  SELECT ISNULL(NULLIF(TEC.EXEMPTCODE, ''), 'N/A')                                                      AS TaxExemptCode
       , ISNULL(NULLIF(TEC.DESCRIPTION, ''), 'N/A')                                                     AS TaxExemptDesc
       , ISNULL(NULLIF(TEC.EXEMPTCODE, ''), 'N/A') + ISNULL(' - ' + NULLIF(TEC.DESCRIPTION, ''), 'N/A') AS TaxExemptCodeDesc
       , TEC.data_connection_id                                                                         AS DataConnectionID
       , TEC.company_id                                                                                 AS CompanyID
       , TEC.Partition                                                                                  AS Partition
       , TEC.stage_id                                                                                   AS StageID
    FROM stage_ax.TaxExemptCodeTable AS TEC ;
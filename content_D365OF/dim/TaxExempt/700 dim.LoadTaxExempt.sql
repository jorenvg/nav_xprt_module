EXEC dbo.drop_object @object = N'dim.LoadTaxExempt' -- nvarchar(128)
                   , @type = N'P'                   -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadTaxExempt]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.TaxExempt
     SET      StageID = TEV.StageID
            , TaxExemptDesc = TEV.TaxExemptDesc
            , TaxExemptCodeDesc = TEV.TaxExemptCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.TaxExempt     TE
   INNER JOIN dim.TaxExemptView TEV ON TEV.TaxExemptCode    = TE.TaxExemptCode
                                   AND TEV.DataConnectionID = TE.DataConnectionID
                                   AND TEV.Partition        = TE.Partition
                                   AND TEV.CompanyID        = TE.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.TaxExempt (
    StageID, DataConnectionID, ComponentExecutionID, CompanyID, Partition, TaxExemptCode, TaxExemptDesc, TaxExemptCodeDesc
  )
  SELECT TEV.StageID
       , TEV.DataConnectionID
       , @component_execution_id
       , TEV.CompanyID
       , TEV.Partition
       , TEV.TaxExemptCode
       , TEV.TaxExemptDesc
       , TEV.TaxExemptCodeDesc
    FROM dim.TaxExemptView TEV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.TaxExempt TE
                       WHERE TE.TaxExemptCode    = TEV.TaxExemptCode
                         AND TE.DataConnectionID = TEV.DataConnectionID
                         AND TE.Partition        = TEV.Partition
                         AND TE.CompanyID        = TEV.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
EXEC dbo.drop_object 'dim.TaxExempt', 'T' ;

CREATE TABLE dim.TaxExempt
(
  TaxExemptID          INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, TaxExemptCode        NVARCHAR(10)  NOT NULL
, TaxExemptDesc        NVARCHAR(100) NOT NULL
, TaxExemptCodeDesc    NVARCHAR(128) NOT NULL
) ;
EXEC dbo.drop_object @object = N'dim.LoadSalesPackingSlip' -- nvarchar(128)
                   , @type = N'P'                          -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadSalesPackingSlip]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  INSERT INTO dim.SalesPackingSlip (
    DataConnectionID, ComponentExecutionID, CompanyID, Partition, SalesPackingSlip
  )
  SELECT SPSV.DataConnectionID
       , @component_execution_id
       , SPSV.CompanyID
       , SPSV.Partition
       , SPSV.SalesPackingSlip
    FROM dim.SalesPackingSlipView AS SPSV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesPackingSlip AS SPS
                       WHERE SPSV.SalesPackingSlip = SPS.SalesPackingSlip
                         AND SPSV.CompanyID        = SPS.CompanyID
                         AND SPSV.Partition        = SPS.Partition
                         AND SPSV.DataConnectionID = SPS.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
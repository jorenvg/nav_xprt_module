
-- ################################################################################ --
-- #####                         Table: dim.ClosedPeriod                      ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.ClosedPeriod', 'T' ;
GO
CREATE TABLE [dim].[ClosedPeriod]
(
  ClosedPeriodID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, ClosedPeriodCode     INT           NOT NULL
, ClosedPeriodDesc     NVARCHAR(255) NULL
) ;
GO
EXEC dbo.drop_object @object = N'dim.LoadClosedPeriod' -- nvarchar(128)
                   , @type = N'P'                      -- nchar(2)
                   , @debug = 0 ;                      -- int
GO

CREATE PROCEDURE [dim].[LoadClosedPeriod]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.ClosedPeriod
     SET      ClosedPeriodDesc = CPV.ClosedPeriodDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.ClosedPeriod     CP
   INNER JOIN dim.ClosedPeriodView CPV ON CPV.ClosedPeriodCode = CP.ClosedPeriodCode
                                      AND CPV.DataConnectionID = CP.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.ClosedPeriod (
    DataConnectionID, ComponentExecutionID, ClosedPeriodCode, ClosedPeriodDesc
  )
  SELECT CPV.DataConnectionID
       , COALESCE(@component_execution_id, -1)
       , CPV.ClosedPeriodCode
       , CPV.ClosedPeriodDesc
    FROM dim.ClosedPeriodView CPV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.ClosedPeriod CP
                       WHERE CPV.ClosedPeriodCode = CP.ClosedPeriodCode
                         AND CPV.DataConnectionID = CP.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
GO



EXEC dbo.drop_object @object = N'dim.LoadEmployee' -- nvarchar(128)
                   , @type = N'P'                  -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadEmployee]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Employee
     SET      StageID = EMV.StageID
            , EmployeeNumber = EMV.EmployeeNumber
            , EmployeeName = EMV.EmployeeName
            , EmployeeTitle = EMV.EmployeeTitle
            , EmployeeOfficeLocation = EMV.EmployeeOfficeLocation
            , DataConnectionID = EMV.DataConnectionID
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Employee     EM
   INNER JOIN dim.EmployeeView EMV ON EMV.Partition        = EM.Partition
                                  AND EMV.EmployeeRecID    = EM.EmployeeRecID
                                  AND EMV.DataConnectionID = EM.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Employee (
    StageID, DataConnectionID, ComponentExecutionID, Partition, EmployeeRecID, EmployeeNumber, EmployeeName, EmployeeTitle, EmployeeOfficeLocation
  )
  SELECT EMV.StageID
       , EMV.DataConnectionID
       , @component_execution_id
       , EMV.Partition
       , EMV.EmployeeRecID
       , EMV.EmployeeNumber
       , EMV.EmployeeName
       , EMV.EmployeeTitle
       , EMV.EmployeeOfficeLocation
    FROM dim.EmployeeView EMV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Employee EM
                       WHERE EMV.Partition        = EM.Partition
                         AND EMV.EmployeeRecID    = EM.EmployeeRecID
                         AND EMV.DataConnectionID = EM.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
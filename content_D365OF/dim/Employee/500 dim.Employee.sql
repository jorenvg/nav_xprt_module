-- ################################################################################ --
-- #####                         Table: dim.Employee                          ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.Employee', 'T' ;

CREATE TABLE dim.Employee
(
  EmployeeID             INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                BIGINT        NULL
, DataConnectionID       INT           NOT NULL
, ExecutionTimestamp     ROWVERSION    NOT NULL
, ComponentExecutionID   INT           NOT NULL
, Partition              BIGINT        NOT NULL
, EmployeeRecID          BIGINT        NOT NULL
, EmployeeNumber         NVARCHAR(25)  NULL
, EmployeeName           NVARCHAR(100) NULL
, EmployeeTitle          NVARCHAR(30)  NULL
, EmployeeOfficeLocation NVARCHAR(255) NULL
) ;
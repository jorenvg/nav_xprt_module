EXEC dbo.drop_object 'dim.TaxView', 'V' ;
GO

CREATE VIEW [dim].[TaxView]
AS
  SELECT      ISNULL(NULLIF(TT.TAXCODE, ''), 'N/A')                                                         AS TaxCode          --B_key                                           
            , ISNULL(NULLIF(TT.TAXNAME, ''), 'N/A')                                                         AS TaxDesc
            , ISNULL(NULLIF(TT.TAXCODE, ''), 'N/A') + ISNULL(' - ' + NULLIF(TT.TAXNAME, ''), 'N/A')         AS TaxCodeDesc
            , ISNULL(NULLIF(TGH.TAXGROUP, ''), 'N/A')                                                       AS TaxGroupCode     --B_key
            , ISNULL(NULLIF(TGH.TAXGROUPNAME, ''), 'N/A')                                                   AS TaxGroupDesc
            , ISNULL(NULLIF(TGH.TAXGROUP, ''), 'N/A') + ISNULL(' - ' + NULLIF(TGH.TAXGROUPNAME, ''), 'N/A') AS TaxGroupCodeDesc
            , ISNULL(NULLIF(TIGH.TAXITEMGROUP, ''), 'N/A')                                                  AS TaxItemGroupCode --B_key
            , ISNULL(NULLIF(TIGH.NAME, ''), 'N/A')                                                          AS TaxItemGroupDesc
            , ISNULL(NULLIF(TIGH.TAXITEMGROUP, ''), 'N/A') + ISNULL(' - ' + NULLIF(TIGH.NAME, ''), 'N/A')   AS TaxItemGroupCodeDesc
            , TT.data_connection_id                                                                         AS DataConnectionID
            , TT.company_id                                                                                 AS CompanyID
            , TT.PARTITION                                                                                  AS Partition
    FROM      stage_ax.TAXTABLE            AS TT
    LEFT JOIN stage_ax.TAXGROUPDATA        TGD ON TT.TAXCODE             = TGD.TAXCODE
                                              AND TT.PARTITION           = TGD.PARTITION
                                              AND TT.company_id          = TGD.company_id
                                              AND TT.data_connection_id  = TGD.data_connection_id
    LEFT JOIN stage_ax.TAXGROUPHEADING     TGH ON TGD.TAXGROUP           = TGH.TAXGROUP
                                              AND TT.PARTITION           = TGH.PARTITION
                                              AND TT.company_id          = TGH.company_id
                                              AND TT.data_connection_id  = TGH.data_connection_id
    LEFT JOIN stage_ax.TAXONITEM           TOI ON TT.TAXCODE             = TOI.TAXCODE
                                              AND TT.PARTITION           = TOI.PARTITION
                                              AND TT.company_id          = TOI.company_id
                                              AND TT.data_connection_id  = TOI.data_connection_id
    LEFT JOIN stage_ax.TAXITEMGROUPHEADING TIGH ON TOI.TAXITEMGROUP      = TIGH.TAXITEMGROUP
                                               AND TT.PARTITION          = TIGH.PARTITION
                                               AND TT.company_id         = TIGH.company_id
                                               AND TT.data_connection_id = TIGH.data_connection_id ;
EXEC Dbo.Drop_Object @Object = N'dim.LoadProductionCalculationType '
-- nvarchar(128)
                   , @Type = N'P'
-- nchar(2)
                   , @Debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadProductionCalculationType
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @Deleted = 0 ;
  SET @Updated = 0 ;
  SET @Inserted = 0 ;

  UPDATE      Dim.ProductionCalculationType
     SET      CalculationType = PCT.CalculationType
            , ComponentExecutionId = @Component_Execution_Id
            , Dataconnectionid = PCTV.dataconnectionid
    FROM      Dim.ProductionCalculationType     AS PCT
   INNER JOIN Dim.ProductionCalculationTypeView PCTV ON PCT.CalculationTypeCode = PCTV.CalculationTypeCode ;

  SELECT @Updated = @@RowCount ;

  INSERT INTO Dim.ProductionCalculationType (
    CalculationTypeCode, CalculationType, ComponentExecutionId, dataconnectionid
  )
  SELECT PCTV.CalculationTypeCode
       , PCTV.CalculationType
       , @Component_Execution_Id
       , PCTV.DataconnectionID
    FROM Dim.ProductionCalculationTypeView PCTV
   WHERE NOT EXISTS (SELECT 1 FROM Dim.ProductionCalculationType PCT WHERE PCTV.CalculationType = PCT.CalculationType) ;

  SELECT @Inserted = @@RowCount ;
END ;


EXEC dbo.drop_object 'dim.PurchasePoolView', 'V' ;
GO

CREATE VIEW [dim].[PurchasePoolView]
AS
  SELECT ISNULL(NULLIF(PP.PURCHPOOLID, ''), 'N/A')                                           AS PurchasePoolCode
       , ISNULL(NULLIF(PP.NAME, ''), 'N/A')                                                  AS PurchasePoolDesc
       , ISNULL(NULLIF(PP.PURCHPOOLID, '') + ' - ', '') + ISNULL(NULLIF(PP.NAME, ''), 'N/A') AS PurchasePoolCodeDesc
       , PP.company_id                                                                       AS CompanyID
       , PP.data_connection_id                                                               AS DataConnectionID
       , PP.stage_id                                                                         AS StageID
       , PP.Partition                                                                        AS Partition
    FROM stage_ax.PURCHPOOL AS PP ;



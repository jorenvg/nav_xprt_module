EXEC dbo.drop_object @object = N'dim.LoadFiscalCalendar' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadFiscalCalendar
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.FiscalCalendar
     SET      StageID = a.StageID
            , [Desc] = a.[Desc]
            , FiscalYear = a.FiscalYear
            , FiscalPeriodName = a.FiscalPeriodName
            , FiscalQuarterCode = a.FiscalQuarterCode
            , FiscalQuarterDesc = a.FiscalQuarterDesc
            , FiscalMonthCode = a.FiscalMonthCode
            , FiscalMonthDesc = a.FiscalMonthDesc
            , FiscalPeriodTypeCode = a.FiscalPeriodTypeCode
            , FiscalPeriodTypeDesc = a.FiscalPeriodTypeDesc
            , FiscalPeriodStart = a.FiscalPeriodStart
            , FiscalPeriodEnd = a.FiscalPeriodEnd
            , FiscalCalendarRecID = a.FiscalCalendarRecID
            , FiscalCalendarRecVersion = a.FiscalCalendarRecVersion
            , FiscalCalendarYearRecID = a.FiscalCalendarYearRecID
            , FiscalCalendarYearRecVersion = a.FiscalCalendarYearRecVersion
            , FiscalCalendarPeriodRecID = a.FiscalCalendarPeriodRecID
            , FiscalCalendarPeriodRecVersion = a.FiscalCalendarPeriodRecVersion
            , ComponentExecutionID = @component_execution_id
    FROM      dim.FiscalCalendar     b
   INNER JOIN dim.FiscalCalendarView a ON a.FiscalCalendarRecID       = b.FiscalCalendarRecID
                                      AND a.FiscalCalendarYearRecID   = b.FiscalCalendarYearRecID
                                      AND a.FiscalCalendarPeriodRecID = b.FiscalCalendarPeriodRecID
                                      AND a.DataConnectionID          = b.DataConnectionID
                                      AND a.[Partition]               = b.[Partition] ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.FiscalCalendar (
    StageID
  , DataConnectionID
  , ComponentExecutionID
  , [Name]
  , [Desc]
  , FiscalYear
  , FiscalPeriodName
  , FiscalQuarterCode
  , FiscalQuarterDesc
  , FiscalMonthCode
  , FiscalMonthDesc
  , FiscalPeriodTypeCode
  , FiscalPeriodTypeDesc
  , FiscalPeriodStart
  , FiscalPeriodEnd
  , [Partition]
  , FiscalCalendarRecID
  , FiscalCalendarRecVersion
  , FiscalCalendarYearRecID
  , FiscalCalendarYearRecVersion
  , FiscalCalendarPeriodRecID
  , FiscalCalendarPeriodRecVersion
  )
  SELECT a.StageID
       , a.DataConnectionID
       , @component_execution_id
       , [Name]
       , [Desc]
       , FiscalYear
       , FiscalPeriodName
       , FiscalQuarterCode
       , FiscalQuarterDesc
       , FiscalMonthCode
       , FiscalMonthDesc
       , FiscalPeriodTypeCode
       , FiscalPeriodTypeDesc
       , FiscalPeriodStart
       , FiscalPeriodEnd
       , [Partition]
       , FiscalCalendarRecID
       , FiscalCalendarRecVersion
       , FiscalCalendarYearRecID
       , FiscalCalendarYearRecVersion
       , FiscalCalendarPeriodRecID
       , FiscalCalendarPeriodRecVersion
    FROM dim.FiscalCalendarView a
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.FiscalCalendar b
                       WHERE a.FiscalCalendarRecID       = b.FiscalCalendarRecID
                         AND a.FiscalCalendarYearRecID   = b.FiscalCalendarYearRecID
                         AND a.FiscalCalendarPeriodRecID = b.FiscalCalendarPeriodRecID
                         AND a.DataConnectionID          = b.DataConnectionID
                         AND a.[Partition]               = b.[Partition]) ;
  SELECT @inserted = @@ROWCOUNT ;

END ;

GO
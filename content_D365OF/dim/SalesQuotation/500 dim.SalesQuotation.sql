-- ################################################################################ --
-- #####                         Table: dim.SalesQuotation                    ##### --
-- ################################################################################ --

EXEC dbo.drop_object 'dim.SalesQuotation', 'T' ;

CREATE TABLE dim.SalesQuotation
(
  SalesQuotationID                INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY CLUSTERED
-- system
, StageID                         BIGINT        NOT NULL
, CompanyID                       INT           NOT NULL
, DataConnectionID                INT           NOT NULL
, Partition                       BIGINT        NOT NULL
, ExecutionTimestamp              ROWVERSION    NOT NULL
, ComponentExecutionID            INT           NOT NULL
-- bk
, SalesQuotationCode              NVARCHAR(100) NULL
-- attributes
, SalesQuotationTypeCode          INT           NOT NULL
, SalesQuotationTypeDescription   NVARCHAR(255) NOT NULL
, SalesPoolCode                   NVARCHAR(50)  NOT NULL
, SalesPoolDescription            NVARCHAR(255) NOT NULL
, SalesPoolCodeDescription        NVARCHAR(500) NOT NULL
, SalesQuotationStatusCode        INT           NOT NULL
, SalesQuotationStatusDescription NVARCHAR(255) NOT NULL
) ;

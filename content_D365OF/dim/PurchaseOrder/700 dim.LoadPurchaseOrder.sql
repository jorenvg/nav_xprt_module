EXEC dbo.drop_object @object = N'dim.LoadPurchaseOrder' -- nvarchar(128)
                   , @type = N'P'                       -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadPurchaseOrder
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.PurchaseOrder
     SET      StageID = POV.StageID
            , DataConnectionID = POV.DataConnectionID
            , CompanyID = POV.CompanyID
            , Partition = POV.Partition
            , ComponentExecutionID = @component_execution_id
            , PurchaseOrder = POV.PurchaseOrder
            , PurchaseTypeCode = POV.PurchaseTypeCode
            , PurchaseTypeDescription = POV.PurchaseTypeDescription
            , PurchaseName = POV.PurchaseName
            , PurchasePoolCode = POV.PurchasePoolCode
            , PurchasePoolDescription = POV.PurchasePoolDescription
            , PurchasePoolCodeDescription = POV.PurchasePoolCodeDescription
            , PurchaseStatusCode = POV.PurchaseStatusCode
            , PurchaseStatusDescription = POV.PurchaseStatusDescription
    FROM      dim.PurchaseOrder     PO
   INNER JOIN dim.PurchaseOrderView POV ON POV.PurchaseOrder    = PO.PurchaseOrder
                                       AND POV.CompanyID        = PO.CompanyID
                                       AND POV.Partition        = PO.Partition
                                       AND POV.DataConnectionID = PO.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.PurchaseOrder (
    StageID
  , DataConnectionID
  , CompanyID
  , Partition
  , ComponentExecutionID
  , PurchaseOrder
  , PurchaseTypeCode
  , PurchaseTypeDescription
  , PurchaseName
  , PurchasePoolCode
  , PurchasePoolDescription
  , PurchasePoolCodeDescription
  , PurchaseStatusCode
  , PurchaseStatusDescription
  )
  SELECT POV.StageID
       , POV.DataConnectionID
       , POV.CompanyID
       , POV.Partition
       , @component_execution_id
       , POV.PurchaseOrder
       , POV.PurchaseTypeCode
       , POV.PurchaseTypeDescription
       , POV.PurchaseName
       , POV.PurchasePoolCode
       , POV.PurchasePoolDescription
       , POV.PurchasePoolCodeDescription
       , POV.PurchaseStatusCode
       , POV.PurchaseStatusDescription
    FROM dim.PurchaseOrderView POV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.PurchaseOrder PO
                       WHERE POV.PurchaseOrder    = PO.PurchaseOrder
                         AND POV.CompanyID        = PO.CompanyID
                         AND POV.Partition        = PO.Partition
                         AND POV.DataConnectionID = PO.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
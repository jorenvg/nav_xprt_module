EXEC dbo.drop_object @object = N'dim.LoadYesNo' -- nvarchar(128)
                   , @type = N'P'               -- nchar(2)
                   , @debug = 0 ;               -- int
GO

CREATE PROCEDURE dim.LoadYesNo
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  DELETE FROM dim.YesNo WHERE YesNoId = 0 ;

  UPDATE dim.YesNo
     SET YesNoDesc = vw.[Desc]
       , YesNoBit = vw.[Bit]
       , YesNoSortBy = vw.SortBy
    FROM dim.YesNo     AS tbl
    JOIN dim.YesNoView vw ON tbl.YesNoCode = vw.Code ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT dim.YesNo (
    YesNoCode, YesNoDesc, YesNoBit, YesNoSortBy
  )
  SELECT Code
       , [Desc]
       , [Bit]
       , SortBy
    FROM dim.YesNoView vw
   WHERE NOT EXISTS (SELECT 1 FROM dim.YesNo AS tbl WHERE tbl.YesNoCode = vw.Code) ;

  SELECT @inserted = @@ROWCOUNT ;

END ;

GO


EXEC dbo.drop_object 'dim.DeliveryTermsView', 'V' ;
GO

CREATE VIEW [dim].[DeliveryTermsView]
AS
  SELECT ISNULL(NULLIF(DT.CODE, ''), 'N/A')                                          AS DeliveryTermsCode
       , ISNULL(NULLIF(DT.TXT, ''), 'N/A')                                           AS DeliveryTermsDesc
       , ISNULL(NULLIF(DT.CODE, '') + ' - ', '') + ISNULL(NULLIF(DT.TXT, ''), 'N/A') AS DeliveryTermsCodeDesc
       , DT.company_id                                                               AS CompanyID
       , DT.Partition                                                                AS Partition
       , DT.data_connection_id                                                       AS DataConnectionID
       , DT.stage_id                                                                 AS StageID
    FROM stage_ax.DLVTERM AS DT ;



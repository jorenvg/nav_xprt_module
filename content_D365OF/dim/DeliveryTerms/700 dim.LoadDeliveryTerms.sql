EXEC dbo.drop_object @object = N'dim.LoadDeliveryTerms' -- nvarchar(128)
                   , @type = N'P'                       -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadDeliveryTerms
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.DeliveryTerms
     SET      StageID = DTV.StageID
            , DeliveryTermsDesc = DTV.DeliveryTermsDesc
            , DeliveryTermsCodeDesc = DTV.DeliveryTermsCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.DeliveryTerms     DT
   INNER JOIN dim.DeliveryTermsView DTV ON DTV.DeliveryTermsCode = DT.DeliveryTermsCode
                                       AND DTV.CompanyID         = DT.CompanyID
                                       AND DTV.Partition         = DT.Partition
                                       AND DTV.DataConnectionID  = DT.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.DeliveryTerms (
    StageID, DataConnectionID, CompanyID, Partition, ComponentExecutionID, DeliveryTermsCode, DeliveryTermsDesc, DeliveryTermsCodeDesc
  )
  SELECT DTV.StageID
       , DTV.DataConnectionID
       , DTV.CompanyID
       , DTV.Partition
       , @component_execution_id
       , DTV.DeliveryTermsCode
       , DTV.DeliveryTermsDesc
       , DTV.DeliveryTermsCodeDesc
    FROM dim.DeliveryTermsView DTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.DeliveryTerms DT
                       WHERE DTV.DeliveryTermsCode = DT.DeliveryTermsCode
                         AND DTV.CompanyID         = DT.CompanyID
                         AND DTV.Partition         = DT.Partition
                         AND DTV.DataConnectionID  = DT.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;

END ;
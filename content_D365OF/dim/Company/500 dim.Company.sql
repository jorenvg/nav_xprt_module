EXEC dbo.drop_object 'dim.Company', 'T' ;

CREATE TABLE dim.Company
(
  CompanyID            INT           NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION
, Partition            BIGINT        NOT NULL
, ComponentExecutionID INT
, Code                 NVARCHAR(60)  NOT NULL
, Name                 NVARCHAR(100) NOT NULL
, AccountingCurrency   NVARCHAR(30)  NOT NULL
) ;
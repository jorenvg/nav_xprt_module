EXEC dbo.drop_object @object = N'dim.LoadCompany' -- nvarchar(128)
                   , @type = N'P'                 -- nchar(2)
                   , @debug = 0 ;                 -- int
GO

CREATE PROCEDURE dim.LoadCompany
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  -- UPDATE any changes to existing companies.
  UPDATE      dim.Company
     SET      Name = CV.Name
            , AccountingCurrency = CV.AccountingCurrency
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Company     c
   INNER JOIN dim.CompanyView CV ON CV.CompanyID        = c.CompanyID
                                AND CV.DataConnectionID = c.DataConnectionID
                                AND CV.Partition        = c.Partition ;

  SELECT @updated = @@ROWCOUNT ;

  -- INSERT NEW companies that did not exist in the company dimension 
  INSERT INTO dim.Company (
    CompanyID, Code, Name, AccountingCurrency, DataConnectionID, ComponentExecutionID, Partition
  )
  SELECT CV.CompanyID
       , CV.Code
       , CV.Name
       , CV.AccountingCurrency
       , CV.DataConnectionID
       , @component_execution_id
       , CV.Partition
    FROM dim.CompanyView AS CV
   WHERE NOT EXISTS (SELECT 1 FROM dim.Company tbl WHERE CV.CompanyID = tbl.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;


EXEC dbo.drop_object 'dim.BudgetCodeView', 'V';
GO
CREATE VIEW dim.BudgetCodeView
AS
--BudgetCode
SELECT 
  ISNULL(NULLIF(BTC.Name,''),'N/A')                                                     AS BudgetCode     
, ISNULL(NULLIF(BTC.Description,''),'N/A')                                              AS BudgetDesc     
, ISNULL(NULLIF(BTC.Name, '') + ' - ', '') + ISNULL(NULLIF(BTC.Description,''),'N/A')   AS BudgetCodeDesc 
, ISNULL(NULLIF(BTC.BudgetTransactionType,''),-1)                                       AS BudgetTypeCode 
, ISNULL(NULLIF(EBTC.Description,''),'N/A')                                             AS BudgetTypeDesc 
, BTC.PARTITION                                                                         AS Partition
, BTC.company_id                                                                        AS CompanyID
, BTC.data_connection_id                                                                AS DataConnectionID
, BTC.stage_id                                                                          AS StageID
  
  FROM
  stage_ax.BudgetTransactionCode AS BTC
  LEFT JOIN meta.enumerations EBTC ON EBTC.TableName = 'BudgetTransactionCode' 
                                   AND EBTC.ColumnName = 'BudgetTransactionType' 
                                   AND BTC.BUDGETTRANSACTIONTYPE = EBTC.OptionID
GO
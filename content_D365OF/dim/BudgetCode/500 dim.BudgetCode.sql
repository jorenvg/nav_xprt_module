
-- ################################################################################ --
-- #####                         Table: dim.BudgetCode                        ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.BudgetCode', 'T'
GO
CREATE TABLE [dim].[BudgetCode]
(
	  BudgetCodeID          INT IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
	, StageID               BIGINT NULL
	, DataConnectionID      INT NOT NULL
	, CompanyID             INT NOT NULL
	, ExecutionTimestamp    ROWVERSION NOT NULL
	, ComponentExecutionID  INT NOT NULL
	, Partition             BIGINT NOT NULL
	, BudgetCode            NVARCHAR(255) NOT NULL
	, BudgetDesc            NVARCHAR(255) NULL
	, BudgetCodeDesc        NVARCHAR(255) NOT NULL
	, BudgetTypeCode        INT NOT NULL
	, BudgetTypeDesc        NVARCHAR(255) NULL
)
GO
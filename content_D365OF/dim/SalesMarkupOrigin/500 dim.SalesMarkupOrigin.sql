EXEC dbo.drop_object 'dim.SalesMarkupOrigin', 'T' ;
GO
CREATE TABLE [dim].[SalesMarkupOrigin]
(
  [SalesMarkupOriginID] INT             NOT NULL IDENTITY(1, 1) PRIMARY KEY NONCLUSTERED
, StageID               BIGINT          NULL
, DataConnectionID      INT             NOT NULL
, ExecutionTimestamp    ROWVERSION      NOT NULL
, ComponentExecutionID  INT             NOT NULL
, [OriginTableCode]     [INT]           NOT NULL
, [OriginTable]         [NVARCHAR](100) NOT NULL
, [OriginMeasureGroup]  [VARCHAR](60)   NOT NULL
) ;
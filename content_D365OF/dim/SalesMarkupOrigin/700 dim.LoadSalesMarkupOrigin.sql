EXEC dbo.drop_object @object = N'dim.LoadSalesMarkupOrigin' -- nvarchar(128)
                   , @type = N'P'                           -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadSalesMarkupOrigin
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesMarkupOrigin
     SET      StageID = smov.StageID
            , OriginMeasureGroup = smov.OriginMeasureGroup
            , ComponentExecutionID = @component_execution_id
    FROM      dim.SalesMarkupOrigin     AS smo
   INNER JOIN dim.SalesMarkupOriginView AS smov ON smo.OriginTableCode  = smov.OriginTableCode
                                               AND smov.OriginTable     = smo.OriginTable
                                               AND smo.DataConnectionID = smov.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.SalesMarkupOrigin (
    StageID, DataConnectionID, ComponentExecutionID, OriginTableCode, OriginTable, OriginMeasureGroup
  )
  SELECT smov.StageID
       , smov.DataConnectionID
       , @component_execution_id
       , smov.OriginTableCode
       , smov.OriginTable
       , smov.OriginMeasureGroup
    FROM dim.SalesMarkupOriginView AS smov
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesMarkupOrigin AS smo
                       WHERE smo.OriginTableCode  = smov.OriginTableCode
                         AND smo.OriginTable      = smov.OriginTable
                         AND smo.DataConnectionID = smov.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
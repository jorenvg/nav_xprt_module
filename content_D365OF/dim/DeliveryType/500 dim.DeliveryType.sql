
-- ################################################################################ --
-- #####                         Table: dim.DeliveryType                      ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.DeliveryType', 'T' ;

CREATE TABLE dim.DeliveryType
(
  DeliveryTypeID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, DeliveryTypeCode     INT           NOT NULL
, DeliveryTypeDesc     NVARCHAR(512) NULL
, DeliveryTypeCodeDesc NVARCHAR(512) NULL
) ;
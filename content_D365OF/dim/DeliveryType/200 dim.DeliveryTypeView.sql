EXEC dbo.drop_object 'dim.DeliveryTypeView', 'V' ;
GO

CREATE VIEW [dim].[DeliveryTypeView]
AS
  SELECT ENUM.OptionID                                                                                                         AS DeliveryTypeCode
       , ISNULL(NULLIF(ENUM.DESCRIPTION, ''), 'N/A')                                                                           AS DeliveryTypeDesc
       , ISNULL(NULLIF(CONVERT(VARCHAR(255), ENUM.OptionID), '') + ' - ', 'N/A') + ISNULL(NULLIF(ENUM.DESCRIPTION, ''), 'N/A') AS DeliveryTypeCodeDesc
       , ENUM.DataConnectionID                                                                                                 AS DataConnectionID
    FROM meta.enumerations AS ENUM
   WHERE ENUM.TableName  = 'PURCHLINE'
     AND ENUM.ColumnName = 'DELIVERYTYPE' ;


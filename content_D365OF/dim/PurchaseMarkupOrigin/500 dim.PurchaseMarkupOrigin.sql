EXEC dbo.drop_object 'dim.PurchaseMarkupOrigin', 'T' ;
GO
CREATE TABLE [dim].[PurchaseMarkupOrigin]
(
  [PurchaseMarkupOriginID] INT             NOT NULL IDENTITY(1, 1) PRIMARY KEY NONCLUSTERED
, StageID                  BIGINT          NULL
, DataConnectionID         INT             NOT NULL
, ExecutionTimestamp       ROWVERSION      NOT NULL
, ComponentExecutionID     INT             NOT NULL
, [OriginTableCode]        [INT]           NOT NULL
, [OriginTable]            [NVARCHAR](255) NOT NULL
, [OriginMeasureGroup]     [VARCHAR](100)  NOT NULL
) ;
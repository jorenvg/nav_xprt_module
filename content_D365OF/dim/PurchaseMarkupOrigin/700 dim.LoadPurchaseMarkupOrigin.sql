EXEC dbo.drop_object @object = N'dim.LoadPurchaseMarkupOrigin' -- nvarchar(128)
                   , @type = N'P'                              -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadPurchaseMarkupOrigin
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.PurchaseMarkupOrigin
     SET      StageID = smov.StageID
            , OriginMeasureGroup = smov.OriginMeasureGroup
            , ComponentExecutionID = @component_execution_id
    FROM      dim.PurchaseMarkupOrigin     AS smo
   INNER JOIN dim.PurchaseMarkupOriginView AS smov ON smo.OriginTableCode  = smov.OriginTableCode
                                                  AND smov.OriginTable     = smo.OriginTable
                                                  AND smo.DataConnectionID = smov.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.PurchaseMarkupOrigin (
    StageID, DataConnectionID, ComponentExecutionID, OriginTableCode, OriginTable, OriginMeasureGroup
  )
  SELECT smov.StageID
       , smov.DataConnectionID
       , @component_execution_id
       , smov.OriginTableCode
       , smov.OriginTable
       , smov.OriginMeasureGroup
    FROM dim.PurchaseMarkupOriginView AS smov
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.PurchaseMarkupOrigin AS smo
                       WHERE smo.OriginTableCode  = smov.OriginTableCode
                         AND smo.OriginTable      = smov.OriginTable
                         AND smo.DataConnectionID = smov.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
EXEC dbo.drop_object 'dim.ChartOfAccountsView', 'V' ;
GO
CREATE VIEW [dim].[ChartOfAccountsView]
AS
  -- ChartOfAccounts
  SELECT      LCOA.RECID                                                                                                                AS ChartOfAccountsCode
            , ISNULL(NULLIF(LCOA.NAME, ''), 'N/A')                                                                                      AS ChartOfAccountsName
            , ISNULL(NULLIF(LCOA.DESCRIPTION, ''), 'N/A')                                                                               AS ChartOfAccountsDesc
            , MA.RecID                                                                                                                  AS MainAccountRecID
            , ISNULL(NULLIF(MA.MAINACCOUNTID, ''), 'N/A')                                                                               AS MainAccountCode
            , ISNULL(NULLIF(MA.NAME, ''), 'N/A')                                                                                        AS AccountName
            , ISNULL(NULLIF(CONVERT(VARCHAR(255), MA.MAINACCOUNTID), '') + ' - ', '') + ISNULL(NULLIF(MA.NAME, ''), 'N/A')              AS AccountCodeDesc
            , MA.TYPE                                                                                                                   AS AccountTypeCode
            --Enum 'MainAccount', 'Type'     
            , ISNULL(NULLIF(EMA.Description, ''), 'N/A')                                                                                AS AccountTypeDesc
            , MA.ACCOUNTCATEGORYREF                                                                                                     AS AccountCategoryCode
            , ISNULL(NULLIF(MAC.DESCRIPTION, ''), 'N/A')                                                                                AS AccountCategoryDesc
            , ISNULL(NULLIF(CONVERT(VARCHAR(255), MA.ACCOUNTCATEGORYREF), '') + ' - ', '') + ISNULL(NULLIF(MAC.DESCRIPTION, ''), 'N/A') AS AccountCategoryCodeDesc
            , CASE WHEN MA.TYPE IN ( 0, 1, 2 ) THEN 0 ELSE 1 END                                                                        AS Balance
            , COALESCE(emat.Description, 'N/A')                                                                                         AS MainAccountType
            , MA.PARTITION                                                                                                              AS PARTITION
            , MA.data_connection_id                                                                                                     AS DataConnectionID
            , MA.stage_id                                                                                                               AS StageID
    FROM      stage_ax.MAINACCOUNT           AS MA
    LEFT JOIN stage_ax.LEDGERCHARTOFACCOUNTS LCOA ON MA.LEDGERCHARTOFACCOUNTS = LCOA.RECID
    LEFT JOIN stage_ax.MAINACCOUNTCATEGORY   MAC ON MA.ACCOUNTCATEGORYREF     = MAC.ACCOUNTCATEGORYREF
                                                AND MA.PARTITION              = MAC.PARTITION
    LEFT JOIN meta.enumerations              EMA ON EMA.TableName             = 'MainAccount'
                                                AND EMA.ColumnName            = 'Type'
                                                AND EMA.OptionID              = MA.TYPE
                                                AND EMA.DataConnectionID      = MA.data_connection_id
    LEFT JOIN meta.enumerations              emat ON EMA.TableName            = 'MainAccount'
                                                 AND EMA.ColumnName           = 'MainAccountType'
                                                 AND EMA.OptionID             = CASE WHEN MA.TYPE IN ( 0, 1, 2 ) THEN 0 ELSE 1 END
                                                 AND EMA.DataConnectionID     = MA.data_connection_id ;
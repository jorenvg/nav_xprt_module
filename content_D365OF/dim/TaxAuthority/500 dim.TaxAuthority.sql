EXEC dbo.drop_object 'dim.TaxAuthority', 'T' ;

CREATE TABLE dim.TaxAuthority
(
  TaxAuthorityID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, TaxAuthorityCode     NVARCHAR(30)  NOT NULL
, TaxAuthorityDesc     NVARCHAR(128) NOT NULL
, TaxAuthorityCodeDesc NVARCHAR(255) NOT NULL
) ;
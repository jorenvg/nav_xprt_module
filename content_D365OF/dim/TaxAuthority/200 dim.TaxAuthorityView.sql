EXEC dbo.drop_object 'dim.TaxAuthorityView', 'V' ;
GO

CREATE VIEW [dim].[TaxAuthorityView]
AS
  SELECT ISNULL(NULLIF(TA.TaxAuthority, ''), 'N/A')                                              AS TaxAuthorityCode
       , ISNULL(NULLIF(TA.Name, ''), 'N/A')                                                      AS TaxAuthorityDesc
       , ISNULL(NULLIF(TA.TaxAuthority, ''), 'N/A') + ISNULL(' - ' + NULLIF(TA.Name, ''), 'N/A') AS TaxAuthorityCodeDesc
       , TA.data_connection_id                                                                   AS DataConnectionID
       , TA.company_id                                                                           AS CompanyID
       , TA.Partition                                                                            AS Partition
       , TA.stage_id                                                                             AS StageID
    FROM stage_ax.TaxAuthorityAddress AS TA ;
EXEC dbo.drop_object @object = N'dim.LoadSalesGroup' -- nvarchar(128)
                   , @type = N'P'                    -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadSalesGroup]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesGroup
     SET      StageID = SGV.StageID
            , SalesGroupDesc = SGV.SalesGroupDesc
            , SalesGroupCodeDesc = SGV.SalesGroupCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.SalesGroup     SG
   INNER JOIN dim.SalesGroupView SGV ON SGV.SalesGroupCode   = SG.SalesGroupCode
                                    AND SGV.DataConnectionID = SG.DataConnectionID
                                    AND SGV.Partition        = SG.Partition
                                    AND SGV.CompanyID        = SG.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.SalesGroup (
    StageID, DataConnectionID, ComponentExecutionID, CompanyID, Partition, SalesGroupCode, SalesGroupDesc, SalesGroupCodeDesc
  )
  SELECT SGV.StageID
       , SGV.DataConnectionID
       , @component_execution_id
       , SGV.CompanyID
       , SGV.Partition
       , SGV.SalesGroupCode
       , SGV.SalesGroupDesc
       , SGV.SalesGroupCodeDesc
    FROM dim.SalesGroupView SGV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesGroup SG
                       WHERE SGV.SalesGroupCode   = SG.SalesGroupCode
                         AND SGV.DataConnectionID = SG.DataConnectionID
                         AND SGV.Partition        = SG.Partition
                         AND SGV.CompanyID        = SG.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
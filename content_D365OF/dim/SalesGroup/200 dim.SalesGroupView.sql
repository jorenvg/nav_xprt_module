EXEC dbo.drop_object 'dim.SalesGroupView', 'V' ;
GO

CREATE VIEW [dim].[SalesGroupView]
AS
  SELECT ISNULL(NULLIF(GROUPID, ''), 'N/A')                                           AS SalesGroupCode
       , ISNULL(NULLIF(NAME, ''), 'N/A')                                              AS SalesGroupDesc
       , ISNULL(NULLIF(GROUPID, ''), 'N/A') + ISNULL(' - ' + NULLIF(NAME, ''), 'N/A') AS SalesGroupCodeDesc
       , data_connection_id                                                           AS DataConnectionID
       , company_id                                                                   AS CompanyID
       , Partition                                                                    AS Partition
       , stage_id                                                                     AS StageID
    FROM stage_ax.COMMISSIONSALESGROUP ;
EXEC dbo.drop_object 'dim.SalesGroup', 'T' ;

CREATE TABLE dim.SalesGroup
(
  SalesGroupID         INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, SalesGroupCode       NVARCHAR(30)  NOT NULL
, SalesGroupDesc       NVARCHAR(128) NOT NULL
, SalesGroupCodeDesc   NVARCHAR(255) NOT NULL
) ;
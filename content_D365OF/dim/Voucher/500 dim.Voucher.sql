EXEC dbo.drop_object 'dim.Voucher', 'T' ;

CREATE TABLE dim.Voucher
(
  VoucherID            INT          IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT          NOT NULL
, ExecutionTimestamp   ROWVERSION   NOT NULL
, ComponentExecutionID INT          NOT NULL
, CompanyID            INT          NOT NULL
, Voucher              NVARCHAR(30) NOT NULL
, IsGL                 BIT          NOT NULL
, [Partition]          BIGINT       NOT NULL
) ;
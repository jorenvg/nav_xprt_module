EXEC dbo.drop_object @object = N'help.Inventory_PhysicalReversedView', @type = N'V' ;
GO
CREATE VIEW help.Inventory_PhysicalReversedView
AS
  SELECT      it.data_connection_id                           AS DataConnectionID
            , it.company_id                                   AS CompanyID
            , itp_fin.DEFAULTDIMENSION
            , it.CURRENCYCODE
            , it.INVENTTRANSORIGIN
            , it.VALUEOPEN
            , it.INVOICEID
            , it.DATESTATUS
            , it.DATEEXPECTED
            , it.INVENTDIMID                                  AS INVENTDIMID
            , it.STATUSISSUE                                  AS STATUSISSUE
            , it.STATUSRECEIPT                                AS STATUSRECEIPT
            , it.DATAAREAID                                   AS DATAAREAID
            , it.PARTITION                                    AS PARTITION
            , it.RECID                                        AS RECID
            , ito.DATAAREAID                                  AS DATAAREAID#2
            , ito.PARTITION                                   AS PARTITION#2
            , ito.INVENTTRANSID                               AS INVENTTRANSID
            , ito.REFERENCECATEGORY                           AS REFERENCECATEGORY
            , ito.REFERENCEID                                 AS REFERENCE
            , itp_fin.DATAAREAID                              AS DATAAREAID#3
            , itp_fin.PARTITION                               AS PARTITION#3
            , itp_fin.ITEMID                                  AS ITEMID
            , itp_fin.VOUCHER                                 AS VOUCHER
            , itp_fin.TRANSDATE                               AS TRANSDATE
            , itp_fin.TRANSBEGINTIME                          AS TRANSBEGINTIME
            , ipt_phys.DATAAREAID                             AS DATAAREAID#4
            , ipt_phys.PARTITION                              AS PARTITION#4
            , ipt_phys.INVENTTRANSPOSTINGTYPE                 AS INVENTTRANSPOSTINGTYPE
            , ipt_phys.ISPOSTED                               AS ISPOSTED
            , ipt_phys.POSTINGTYPE                            AS POSTINGTYPE
            , ipt_phys.POSTINGTYPEOFFSET                      AS POSTINGTYPEOFFSET
            , ipt_phys.LEDGERDIMENSION                        AS LEDGERDIMENSION
            , ipt_phys.OFFSETLEDGERDIMENSION                  AS LEDGERDIMENSIONOFFSET
            , CAST(-it.QTY AS NUMERIC(32, 16))                AS QTY
            , CAST(-it.COSTAMOUNTPHYSICAL AS NUMERIC(32, 16)) AS AMOUNT
    FROM      stage_ax.INVENTTRANS       AS it
   CROSS JOIN stage_ax.INVENTTRANSORIGIN AS ito
   CROSS JOIN stage_ax.INVENTTRANSPOSTING AS itp_fin
   CROSS JOIN stage_ax.INVENTTRANSPOSTING AS ipt_phys
   WHERE      NOT it.execution_flag = 'D'
     AND      it.INVENTTRANSORIGIN              = ito.RECID
     AND      it.DATAAREAID                     = ito.DATAAREAID
     AND      it.PARTITION                      = ito.PARTITION
     --   financial posting types
     AND      ( itp_fin.INVENTTRANSPOSTINGTYPE  = 1
            AND it.VOUCHER                      = itp_fin.VOUCHER
            AND it.DATAAREAID                   = itp_fin.DATAAREAID
            AND it.PARTITION                    = itp_fin.PARTITION
            AND it.DATEFINANCIAL                = itp_fin.TRANSDATE
            AND it.DATAAREAID                   = itp_fin.DATAAREAID
            AND it.PARTITION                    = itp_fin.PARTITION
            AND ito.RECID                       = itp_fin.INVENTTRANSORIGIN
            AND ito.DATAAREAID                  = itp_fin.DATAAREAID
            AND ito.PARTITION                   = itp_fin.PARTITION)
     --  physical posting types
     AND      ( ipt_phys.INVENTTRANSPOSTINGTYPE = 0
            AND it.VOUCHERPHYSICAL              = ipt_phys.VOUCHER
            AND it.DATAAREAID                   = ipt_phys.DATAAREAID
            AND it.PARTITION                    = ipt_phys.PARTITION
            AND it.DATEPHYSICAL                 = ipt_phys.TRANSDATE
            AND it.DATAAREAID                   = ipt_phys.DATAAREAID
            AND it.PARTITION                    = ipt_phys.PARTITION
            AND ito.RECID                       = ipt_phys.INVENTTRANSORIGIN
            AND ito.DATAAREAID                  = ipt_phys.DATAAREAID
            AND ito.PARTITION                   = ipt_phys.PARTITION) ;


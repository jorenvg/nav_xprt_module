EXEC dbo.drop_object @object = N'help.Inventory_FinancialTransactionView', @type = N'V' ;
GO
CREATE VIEW help.Inventory_FinancialTransactionView
AS
  SELECT      it.data_connection_id      AS DataConnectionID
            , it.company_id              AS CompanyID
            , itp.DEFAULTDIMENSION
            , it.CURRENCYCODE
            , it.INVENTTRANSORIGIN
            , it.VALUEOPEN
            , it.INVOICEID
            , it.DATESTATUS
            , it.DATEEXPECTED
            , SUM(it.QTY)                AS QTY
            , SUM(it.COSTAMOUNTPOSTED)   AS AMOUNT
            , it.INVENTDIMID             AS INVENTDIMID
            , it.STATUSISSUE             AS STATUSISSUE
            , it.STATUSRECEIPT           AS STATUSRECEIPT
            , it.DATAAREAID              AS DATAAREAID
            , it.PARTITION               AS PARTITION
            , 1010                       AS RECID
            , ito.DATAAREAID             AS DATAAREAID#2
            , ito.PARTITION              AS PARTITION#2
            , ito.INVENTTRANSID          AS INVENTTRANSID
            , ito.REFERENCECATEGORY      AS REFERENCECATEGORY
            , ito.REFERENCEID            AS REFERENCE
            , itp.DATAAREAID             AS DATAAREAID#3
            , itp.PARTITION              AS PARTITION#3
            , itp.ITEMID                 AS ITEMID
            , itp.VOUCHER                AS VOUCHER
            , itp.TRANSDATE              AS TRANSDATE
            , itp.INVENTTRANSPOSTINGTYPE AS INVENTTRANSPOSTINGTYPE
            , itp.ISPOSTED               AS ISPOSTED
            , itp.POSTINGTYPE            AS POSTINGTYPE
            , itp.POSTINGTYPEOFFSET      AS POSTINGTYPEOFFSET
            , itp.LEDGERDIMENSION        AS LEDGERDIMENSION
            , itp.OFFSETLEDGERDIMENSION  AS LEDGERDIMENSIONOFFSET
            , itp.TRANSBEGINTIME         AS TRANSBEGINTIME
    FROM      stage_ax.INVENTTRANS       AS it
   CROSS JOIN stage_ax.INVENTTRANSORIGIN AS ito
   CROSS JOIN stage_ax.INVENTTRANSPOSTING AS itp
   WHERE      it.INVENTTRANSORIGIN  = ito.RECID
     AND      it.DATAAREAID              = ito.DATAAREAID
     AND      it.PARTITION               = ito.PARTITION
     AND      itp.INVENTTRANSPOSTINGTYPE = 1 -- only financial inventtranspositing types
     AND      it.VOUCHER                 = itp.VOUCHER
     AND      it.DATAAREAID              = itp.DATAAREAID
     AND      it.PARTITION               = itp.PARTITION
     AND      it.DATEFINANCIAL           = itp.TRANSDATE
     AND      it.DATAAREAID              = itp.DATAAREAID
     AND      it.PARTITION               = itp.PARTITION
     AND      ito.RECID                  = itp.INVENTTRANSORIGIN
     AND      ito.DATAAREAID             = itp.DATAAREAID
     AND      ito.PARTITION              = itp.PARTITION
     AND      NOT it.execution_flag = 'D'
   GROUP BY it.INVENTDIMID
          , it.STATUSISSUE
          , it.STATUSRECEIPT
          , it.DATAAREAID
          , it.PARTITION
          , ito.DATAAREAID
          , ito.PARTITION
          , ito.INVENTTRANSID
          , ito.REFERENCECATEGORY
          , ito.REFERENCEID
          , itp.DATAAREAID
          , itp.PARTITION
          , itp.ITEMID
          , itp.VOUCHER
          , itp.TRANSDATE
          , itp.INVENTTRANSPOSTINGTYPE
          , itp.ISPOSTED
          , itp.POSTINGTYPE
          , itp.POSTINGTYPEOFFSET
          , itp.LEDGERDIMENSION
          , itp.OFFSETLEDGERDIMENSION
          , itp.TRANSBEGINTIME
          , it.data_connection_id
          , it.company_id
          , itp.DEFAULTDIMENSION
          , it.CURRENCYCODE
          , it.INVENTTRANSORIGIN
          , it.STATUSISSUE
          , it.STATUSRECEIPT
          , it.VALUEOPEN
          , it.INVOICEID
          , it.DATESTATUS
          , it.DATEEXPECTED ;

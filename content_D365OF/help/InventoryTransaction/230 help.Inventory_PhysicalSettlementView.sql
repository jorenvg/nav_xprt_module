EXEC dbo.drop_object @object = N'help.Inventory_PhysicalSettlementView', @type = N'V' ;
GO
CREATE VIEW help.Inventory_PhysicalSettlementView
AS
  SELECT      it.data_connection_id            AS DataConnectionID
            , it.company_id                    AS CompanyID
            , [is].DEFAULTDIMENSION
            , it.CURRENCYCODE
            , it.INVENTTRANSORIGIN
            , it.VALUEOPEN
            , it.INVOICEID
            , it.DATESTATUS
            , it.DATEEXPECTED
            , it.INVENTDIMID                   AS INVENTDIMID
            , it.STATUSISSUE                   AS STATUSISSUE
            , it.STATUSRECEIPT                 AS STATUSRECEIPT
            , it.DATAAREAID                    AS DATAAREAID
            , it.PARTITION                     AS PARTITION
            , 1010                             AS RECID
            , ito.DATAAREAID                   AS DATAAREAID#2
            , ito.PARTITION                    AS PARTITION#2
            , ito.INVENTTRANSID                AS INVENTTRANSID
            , ito.REFERENCECATEGORY            AS REFERENCECATEGORY
            , ito.REFERENCEID                  AS REFERENCE
            , SUM([is].COSTAMOUNTADJUSTMENT)   AS AMOUNT
            , [is].DATAAREAID                  AS DATAAREAID#3
            , [is].PARTITION                   AS PARTITION#3
            , [is].ITEMID                      AS ITEMID
            , [is].VOUCHER                     AS VOUCHER
            , [is].TRANSDATE                   AS TRANSDATE
            , [is].POSTED                      AS ISPOSTED
            , [is].BALANCESHEETPOSTING         AS POSTINGTYPE
            , [is].OPERATIONSPOSTING           AS POSTINGTYPEOFFSET
            , [is].BALANCESHEETLEDGERDIMENSION AS LEDGERDIMENSION
            , [is].OPERATIONSLEDGERDIMENSION   AS LEDGERDIMENSIONOFFSET
            , [is].TRANSBEGINTIME              AS TRANSBEGINTIME
            , CAST(0 AS NUMERIC(32, 16))       AS QTY
            , CAST(0 AS INT)                   AS INVENTTRANSPOSTINGTYPE
    FROM      stage_ax.INVENTTRANS       AS it
   CROSS JOIN stage_ax.INVENTTRANSORIGIN AS ito
   CROSS JOIN stage_ax.INVENTSETTLEMENT AS [is]
   WHERE      it.INVENTTRANSORIGIN = ito.RECID
     AND      it.DATAAREAID             = ito.DATAAREAID
     AND      it.PARTITION              = ito.PARTITION
     AND      [is].SETTLEMODEL          = 7 -- PhysicalValue SettleModel
     AND      it.RECID                  = [is].TRANSRECID
     AND      it.DATAAREAID             = [is].DATAAREAID
     AND      it.PARTITION              = [is].PARTITION
     AND      NOT it.execution_flag = 'D'
   GROUP BY it.INVENTDIMID
          , it.STATUSISSUE
          , it.STATUSRECEIPT
          , it.DATAAREAID
          , it.PARTITION
          , ito.DATAAREAID
          , ito.PARTITION
          , ito.INVENTTRANSID
          , ito.REFERENCECATEGORY
          , ito.REFERENCEID
          , [is].DATAAREAID
          , [is].PARTITION
          , [is].ITEMID
          , [is].VOUCHER
          , [is].TRANSDATE
          , [is].POSTED
          , [is].BALANCESHEETPOSTING
          , [is].OPERATIONSPOSTING
          , [is].BALANCESHEETLEDGERDIMENSION
          , [is].OPERATIONSLEDGERDIMENSION
          , [is].TRANSBEGINTIME
          , it.data_connection_id
          , it.company_id
          , [is].DEFAULTDIMENSION
          , it.CURRENCYCODE
          , it.INVENTTRANSORIGIN
          , it.STATUSISSUE
          , it.STATUSRECEIPT
          , it.VALUEOPEN
          , it.INVOICEID
          , it.DATESTATUS
          , it.DATEEXPECTED ;

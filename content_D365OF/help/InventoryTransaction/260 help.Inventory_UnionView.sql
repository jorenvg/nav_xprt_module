EXEC dbo.drop_object @object = N'help.Inventory_UnionView', @type = N'V' ;
GO
CREATE VIEW help.Inventory_UnionView
AS
  SELECT iptv.DataConnectionID
       , iptv.CompanyID
       , iptv.DEFAULTDIMENSION
       , iptv.CURRENCYCODE
       , iptv.INVENTTRANSORIGIN
       , iptv.VALUEOPEN
       , iptv.INVOICEID
       , iptv.DATESTATUS
       , iptv.DATEEXPECTED
       , iptv.AMOUNT                 AS AMOUNT
       , iptv.TRANSBEGINTIME         AS TRANSBEGINTIME
       , iptv.INVENTDIMID            AS INVENTDIMID
       , iptv.INVENTTRANSID          AS INVENTTRANSID
       , iptv.INVENTTRANSPOSTINGTYPE AS INVENTTRANSPOSTINGTYPE
       , iptv.ISPOSTED               AS ISPOSTED
       , iptv.ITEMID                 AS ITEMID
       , iptv.LEDGERDIMENSION        AS LEDGERDIMENSION
       , iptv.LEDGERDIMENSIONOFFSET  AS LEDGERDIMENSIONOFFSET
       , iptv.POSTINGTYPE            AS POSTINGTYPE
       , iptv.POSTINGTYPEOFFSET      AS POSTINGTYPEOFFSET
       , iptv.QTY                    AS QTY
       , iptv.REFERENCE              AS REFERENCE
       , iptv.REFERENCECATEGORY      AS REFERENCECATEGORY
       , iptv.TRANSDATE              AS TRANSDATE
       , iptv.VOUCHER                AS VOUCHER
       , iptv.STATUSISSUE            AS STATUSISSUE
       , iptv.STATUSRECEIPT          AS STATUSRECEIPT
       , iptv.DATAAREAID             AS DATAAREAID
       , iptv.PARTITION              AS PARTITION
       , iptv.RECID                  AS RECID
       , 1                           AS UnionAllBranchId
    FROM help.Inventory_PhysicalTransactionView AS iptv
  UNION ALL
  SELECT ipav.DataConnectionID
       , ipav.CompanyID
       , ipav.DEFAULTDIMENSION
       , ipav.CURRENCYCODE
       , ipav.INVENTTRANSORIGIN
       , ipav.VALUEOPEN
       , ipav.INVOICEID
       , ipav.DATESTATUS
       , ipav.DATEEXPECTED
       , ipav.AMOUNT
       , ipav.TRANSBEGINTIME
       , ipav.INVENTDIMID
       , ipav.INVENTTRANSID
       , ipav.INVENTTRANSPOSTINGTYPE
       , ipav.ISPOSTED
       , ipav.ITEMID
       , ipav.LEDGERDIMENSION
       , ipav.LEDGERDIMENSIONOFFSET
       , ipav.POSTINGTYPE
       , ipav.POSTINGTYPEOFFSET
       , ipav.QTY
       , ipav.REFERENCE
       , ipav.REFERENCECATEGORY
       , ipav.TRANSDATE
       , ipav.VOUCHER
       , ipav.STATUSISSUE
       , ipav.STATUSRECEIPT
       , ipav.DATAAREAID
       , ipav.PARTITION
       , ipav.RECID
       , 2
    FROM help.Inventory_PhysicalAdjustmentView AS ipav
  UNION ALL
  SELECT iprv.DataConnectionID
       , iprv.CompanyID
       , iprv.DEFAULTDIMENSION
       , iprv.CURRENCYCODE
       , iprv.INVENTTRANSORIGIN
       , iprv.VALUEOPEN
       , iprv.INVOICEID
       , iprv.DATESTATUS
       , iprv.DATEEXPECTED
       , iprv.AMOUNT
       , iprv.TRANSBEGINTIME
       , iprv.INVENTDIMID
       , iprv.INVENTTRANSID
       , iprv.INVENTTRANSPOSTINGTYPE
       , iprv.ISPOSTED
       , iprv.ITEMID
       , iprv.LEDGERDIMENSION
       , iprv.LEDGERDIMENSIONOFFSET
       , iprv.POSTINGTYPE
       , iprv.POSTINGTYPEOFFSET
       , iprv.QTY
       , iprv.REFERENCE
       , iprv.REFERENCECATEGORY
       , iprv.TRANSDATE
       , iprv.VOUCHER
       , iprv.STATUSISSUE
       , iprv.STATUSRECEIPT
       , iprv.DATAAREAID
       , iprv.PARTITION
       , iprv.RECID
       , 3
    FROM help.Inventory_PhysicalReversedView AS iprv
  UNION ALL
  SELECT ipsv.DataConnectionID
       , ipsv.CompanyID
       , ipsv.DEFAULTDIMENSION
       , ipsv.CURRENCYCODE
       , ipsv.INVENTTRANSORIGIN
       , ipsv.VALUEOPEN
       , ipsv.INVOICEID
       , ipsv.DATESTATUS
       , ipsv.DATEEXPECTED
       , ipsv.AMOUNT
       , ipsv.TRANSBEGINTIME
       , ipsv.INVENTDIMID
       , ipsv.INVENTTRANSID
       , ipsv.INVENTTRANSPOSTINGTYPE
       , ipsv.ISPOSTED
       , ipsv.ITEMID
       , ipsv.LEDGERDIMENSION
       , ipsv.LEDGERDIMENSIONOFFSET
       , ipsv.POSTINGTYPE
       , ipsv.POSTINGTYPEOFFSET
       , ipsv.QTY
       , ipsv.REFERENCE
       , ipsv.REFERENCECATEGORY
       , ipsv.TRANSDATE
       , ipsv.VOUCHER
       , ipsv.STATUSISSUE
       , ipsv.STATUSRECEIPT
       , ipsv.DATAAREAID
       , ipsv.PARTITION
       , ipsv.RECID
       , 4
    FROM help.Inventory_PhysicalSettlementView AS ipsv
  UNION ALL
  SELECT iftv.DataConnectionID
       , iftv.CompanyID
       , iftv.DEFAULTDIMENSION
       , iftv.CURRENCYCODE
       , iftv.INVENTTRANSORIGIN
       , iftv.VALUEOPEN
       , iftv.INVOICEID
       , iftv.DATESTATUS
       , iftv.DATEEXPECTED
       , iftv.AMOUNT
       , iftv.TRANSBEGINTIME
       , iftv.INVENTDIMID
       , iftv.INVENTTRANSID
       , iftv.INVENTTRANSPOSTINGTYPE
       , iftv.ISPOSTED
       , iftv.ITEMID
       , iftv.LEDGERDIMENSION
       , iftv.LEDGERDIMENSIONOFFSET
       , iftv.POSTINGTYPE
       , iftv.POSTINGTYPEOFFSET
       , iftv.QTY
       , iftv.REFERENCE
       , iftv.REFERENCECATEGORY
       , iftv.TRANSDATE
       , iftv.VOUCHER
       , iftv.STATUSISSUE
       , iftv.STATUSRECEIPT
       , iftv.DATAAREAID
       , iftv.PARTITION
       , iftv.RECID
       , 5
    FROM help.Inventory_FinancialTransactionView AS iftv
  UNION ALL
  SELECT ifav.DataConnectionID
       , ifav.CompanyID
       , ifav.DEFAULTDIMENSION
       , ifav.CURRENCYCODE
       , ifav.INVENTTRANSORIGIN
       , ifav.VALUEOPEN
       , ifav.INVOICEID
       , ifav.DATESTATUS
       , ifav.DATEEXPECTED
       , ifav.AMOUNT
       , ifav.TRANSBEGINTIME
       , ifav.INVENTDIMID
       , ifav.INVENTTRANSID
       , ifav.INVENTTRANSPOSTINGTYPE
       , ifav.ISPOSTED
       , ifav.ITEMID
       , ifav.LEDGERDIMENSION
       , ifav.LEDGERDIMENSIONOFFSET
       , ifav.POSTINGTYPE
       , ifav.POSTINGTYPEOFFSET
       , ifav.QTY
       , ifav.REFERENCE
       , ifav.REFERENCECATEGORY
       , ifav.TRANSDATE
       , ifav.VOUCHER
       , ifav.STATUSISSUE
       , ifav.STATUSRECEIPT
       , ifav.DATAAREAID
       , ifav.PARTITION
       , ifav.RECID
       , 6
    FROM help.Inventory_FinancialAdjustmentView AS ifav ;

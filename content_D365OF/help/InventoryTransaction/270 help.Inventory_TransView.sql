EXEC dbo.drop_object @object = N'help.Inventory_TransView', @type = N'V' ;
GO
CREATE VIEW help.Inventory_TransView
AS
  SELECT iuv.DataConnectionID
       , iuv.CompanyID
       , iuv.DEFAULTDIMENSION
       , iuv.CURRENCYCODE
       , iuv.INVENTTRANSORIGIN
       , iuv.VALUEOPEN
       , iuv.INVOICEID
       , iuv.DATESTATUS
       , iuv.DATEEXPECTED
       , SUM(iuv.AMOUNT)            AS AMOUNT
       , SUM(iuv.QTY)               AS QTY
       , iuv.TRANSBEGINTIME         AS TRANSBEGINTIME
       , iuv.INVENTDIMID            AS INVENTDIMID
       , iuv.INVENTTRANSID          AS INVENTTRANSID
       , iuv.INVENTTRANSPOSTINGTYPE AS INVENTTRANSPOSTINGTYPE
       , iuv.ISPOSTED               AS ISPOSTED
       , iuv.ITEMID                 AS ITEMID
       , iuv.LEDGERDIMENSION        AS LEDGERDIMENSION
       , iuv.LEDGERDIMENSIONOFFSET  AS LEDGERDIMENSIONOFFSET
       , iuv.POSTINGTYPE            AS POSTINGTYPE
       , iuv.POSTINGTYPEOFFSET      AS POSTINGTYPEOFFSET
       , iuv.REFERENCE              AS REFERENCE
       , iuv.REFERENCECATEGORY      AS REFERENCECATEGORY
       , iuv.TRANSDATE              AS TRANSDATE
       , iuv.VOUCHER                AS VOUCHER
       , iuv.STATUSISSUE            AS STATUSISSUE
       , iuv.STATUSRECEIPT          AS STATUSRECEIPT
       , iuv.DATAAREAID             AS DATAAREAID
       , iuv.PARTITION              AS PARTITION
       , 1010                       AS RECID
    FROM help.Inventory_UnionView AS iuv
   GROUP BY iuv.TRANSBEGINTIME
          , iuv.INVENTDIMID
          , iuv.INVENTTRANSID
          , iuv.INVENTTRANSPOSTINGTYPE
          , iuv.ISPOSTED
          , iuv.ITEMID
          , iuv.LEDGERDIMENSION
          , iuv.LEDGERDIMENSIONOFFSET
          , iuv.POSTINGTYPE
          , iuv.POSTINGTYPEOFFSET
          , iuv.REFERENCE
          , iuv.REFERENCECATEGORY
          , iuv.TRANSDATE
          , iuv.VOUCHER
          , iuv.STATUSISSUE
          , iuv.STATUSRECEIPT
          , iuv.DATAAREAID
          , iuv.PARTITION
          , iuv.DataConnectionID
          , iuv.CompanyID
          , iuv.DEFAULTDIMENSION
          , iuv.CURRENCYCODE
          , iuv.INVENTTRANSORIGIN
          , iuv.VALUEOPEN
          , iuv.INVOICEID
          , iuv.DATESTATUS
          , iuv.DATEEXPECTED ;

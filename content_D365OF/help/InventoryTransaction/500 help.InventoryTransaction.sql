EXEC dbo.drop_object 'help.InventoryTransaction', 'T' ;
GO
CREATE TABLE [help].[InventoryTransaction]
(
  [DataConnectionID]       INT              NOT NULL
, [CompanyID]              INT              NOT NULL
, [ComponentExecutionID]   INT              NOT NULL
, [ExecutionTimestamp]     ROWVERSION       NOT NULL
, [INCREMENTALDATE]        [DATETIME]       NULL
, [INVENTDIMID]            [NVARCHAR](100)  NULL
, [STATUSISSUE]            [INT]            NULL
, [STATUSRECEIPT]          [INT]            NULL
, [PARTITION]              [BIGINT]         NULL
, [INVENTTRANSID]          [NVARCHAR](100)  NULL
, INVENTCOLORID            NVARCHAR(50)     NULL
, INVENTSITEID             NVARCHAR(50)     NULL
, INVENTSIZEID             NVARCHAR(50)     NULL
, INVENTSTYLEID            NVARCHAR(50)     NULL
, CONFIGID                 NVARCHAR(50)     NULL
, WMSlocationCode          NVARCHAR(50)     NULL
, LocationCode             NVARCHAR(50)     NULL
, TrackingBatchCode        NVARCHAR(100)    NULL
, TrackingSerialCode       NVARCHAR(100)    NULL
, [INVOICEID]              [NVARCHAR](100)  NULL
, [REFERENCECATEGORY]      [INT]            NULL
, [REFERENCE]              [NVARCHAR](100)  NULL
, [ITEMID]                 [NVARCHAR](100)  NULL
, [VOUCHER]                [NVARCHAR](100)  NULL
, [TRANSDATE]              [DATETIME]       NULL
, [DATESTATUS]             [DATETIME]       NULL
, [DATEEXPECTED]           [DATETIME]       NULL
, [VALUEOPEN]              [INT]            NULL
, [INVENTTRANSPOSTINGTYPE] [INT]            NULL
, [INVENTTRANSORIGIN]      [BIGINT]         NULL
, [ISPOSTED]               [BIT]            NULL
, [POSTINGTYPE]            [INT]            NULL
, [POSTINGTYPEOFFSET]      [INT]            NULL
, [LEDGERDIMENSION]        [BIGINT]         NULL
, [LEDGERDIMENSIONOFFSET]  [BIGINT]         NULL
, [DEFAULTDIMENSION]       [BIGINT]         NULL
, [CURRENCYCODE]           [NVARCHAR](10)   NULL
, [QUANTITY]               [NUMERIC](16, 4) NULL
, ISSUEQUANTITY            [NUMERIC](16, 4) NULL
, RECEIPTQUANTITY          [NUMERIC](16, 4) NULL
, [AMOUNT]                 [NUMERIC](16, 4) NULL
, ISSUEAMOUNT              [NUMERIC](16, 4) NULL
, RECEIPTAMOUNT            [NUMERIC](16, 4) NULL
, ACCOUNTINGCURRENCY       [NVARCHAR](10)   NULL
) ;

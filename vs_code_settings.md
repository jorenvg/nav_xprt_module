# User Settings

## Powershell Internal settings

Since VSCode 1.9 the setting for the default inteface is just a user setting. instead of a workspace setting. To activate the Powershell internal terminal set this setting in the user settings (File > Preferences > User Settings.)

```JSON
"terminal.integrated.shell.windows": "\\windows\\system32\\WindowsPowerShell\\v1.0\\powershell.exe",
```

## Usefull VS Code extensions

Search for the extension and install them, if you think they could be usefull.

### general extension

* *Open*: Opens the current file in the default application usefull for opening sql files in SSMS
* *vscode-icons*: Shows icons for specific files, makes it easier to identify the files.

### Powershell

* *Powershell*: Lets you debug and execute powershell files without starting the file from command line.

### Git extensions

* Git Lens

## Markdown extensions

* Markdownlint: check for markdown syntax
* Markdown All in One

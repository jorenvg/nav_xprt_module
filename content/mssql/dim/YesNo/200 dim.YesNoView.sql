EXEC dbo.drop_object @object = N'dim.YesNoView', @type = N'V' ;
GO
CREATE VIEW dim.YesNoView
AS
  SELECT 0 AS Code, 'No' AS [Desc], 0 AS [Bit], 1 AS [SortBy]
  UNION
  SELECT 1 AS Code, 'Yes' AS [Desc], 1 AS [Bit], 0 AS [SortBy] ;

EXEC dbo.drop_object 'dim.TimeIntelligence', 'T' ;

CREATE TABLE dim.TimeIntelligence
(
  TimeIntelligenceID     INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, TimeIntelligenceCode   NVARCHAR(10)  NOT NULL
, TimeIntelligenceDesc   NVARCHAR(120) NOT NULL
, TimeIntelligenceSortBy INT           NOT NULL
) ;

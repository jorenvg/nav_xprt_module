/************************************
*** uses SQLCMD variables:
***   component_type : nvarchar()
***   component_name: nvarchar()
***   instance: int
***********************************/

-- Add DWH Indexes 
DECLARE @table_index AS dbo.table_indexes ;

INSERT INTO @table_index (
  table_identifier, index_name, index_type, index_columns, included_columns, fill_factor, is_unique
)
VALUES
  ('$(component_type).$(component_name)', 'ix_c_$(component_type)_$(component_name)', 'CLUSTERED', 'Date', NULL, 90, 1)
, ('$(component_type).$(component_name)', 'ix_nc_$(component_type)_$(component_name)_PK', 'NONCLUSTERED', 'DateID', NULL, 90, 1) ;

EXEC meta.add_all_dwh_indexes @instance_id = $(instance)
                            , @component_category = '$(component_type)'
                            , @component_name = '$(component_type)$(component_name)'
                            , @table_indexes = @table_index
                            , @debug = 0 ;
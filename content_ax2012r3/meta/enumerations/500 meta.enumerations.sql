--EXECUTE dbo.drop_object 'meta.enumerations_create_table', 'P'
--GO
--CREATE PROCEDURE meta.enumerations_create_table
--AS
EXECUTE dbo.drop_object 'meta.enumerations', 'T' ;
GO
CREATE TABLE meta.enumerations
(
  [EnumerationID]      INT           IDENTITY(1, 1) NOT NULL
, [DataConnectionID]   INT           NOT NULL
, [TableID]            INT           NOT NULL
, [ColumnID]           INT           NOT NULL
, [OptionID]           INT           NOT NULL
, [TableName]          NVARCHAR(128) NOT NULL
, [ColumnName]         NVARCHAR(128) NOT NULL
, [Version]            NVARCHAR(128) NOT NULL
, [Description]        NVARCHAR(128) NOT NULL
, [DescriptionDefault] NVARCHAR(128) NOT NULL
) ;

ALTER TABLE meta.enumerations ADD CONSTRAINT PK_enumerations PRIMARY KEY (EnumerationID) ;

CREATE INDEX IX_enumerations ON meta.enumerations (TableID, ColumnID) ;
CREATE INDEX IX_enumerations1
  ON meta.enumerations (DataConnectionID, TableID, ColumnID, OptionID)
  INCLUDE (Description) ;
CREATE INDEX IX_enumerations2
  ON meta.enumerations (DataConnectionID, OptionID, TableName, ColumnName)
  INCLUDE (Description) ;
CREATE INDEX IX_enumerations3 ON meta.enumerations (TableName, ColumnName) ;
GO
EXEC dbo.drop_object @object = N'help.ExchangeRatesView' -- nvarchar(128)
                   , @type = N'V'                        -- nchar(2)
                   , @debug = 0 ;
-- int
GO
CREATE VIEW help.ExchangeRatesView
AS
  WITH cte_EXCHANGERATEEURODENOMINATIONVIEW AS (
    SELECT            T1.CURRENCYCODE AS CURRENCYCODE
                    , T1.EXCHANGERATE AS EXCHANGERATE
                    , T1.STARTDATE    AS STARTDATE
                    , T1.PARTITION    AS PARTITION
                    , T1.RECID        AS RECID
                    , T2.PARTITION    AS PARTITION#2
                    , T2.CURRENCYCODE AS EUROCURRENCYCODE
                    , T1.data_connection_id
      FROM            stage_ax.CURRENCYEURODENOMINATION T1
      LEFT OUTER JOIN stage_ax.CURRENCY                 T2 ON ( (T2.ISEURO          = 1)
                                                            AND (T1.PARTITION       = T2.PARTITION))
                                                          AND T1.data_connection_id = T2.data_connection_id
  )
     , cte_EXCHANGERATEDENTODENBETWEENSTART AS (
    SELECT            T1.CURRENCYCODE     AS CURRENCYCODE1
                    , T1.STARTDATE        AS STARTDATE1
                    , T1.EXCHANGERATE     AS EURODENOMINATIONRATE1
                    , T1.EUROCURRENCYCODE AS EUROCURRENCYCODE
                    , T1.PARTITION        AS PARTITION
                    , T1.RECID            AS RECID
                    , T2.PARTITION        AS PARTITION#2
                    , T2.CURRENCYCODE     AS CURRENCYCODE2
                    , T2.STARTDATE        AS STARTDATE2
                    , T2.EXCHANGERATE     AS EURODENOMINATIONRATE2
                    , T3.PARTITION        AS PARTITION#3
                    , T3.EXCHANGERATETYPE AS EXCHANGERATETYPE
                    , T3.FROMCURRENCYCODE AS FROMCURRENCYCODE
                    , T3.TOCURRENCYCODE   AS TOCURRENCYCODE
                    , T4.PARTITION        AS PARTITION#4
                    , T4.EXCHANGERATE     AS EXCHANGERATE
                    , T4.VALIDFROM        AS VALIDFROM
                    , T4.VALIDTO          AS VALIDTO
                    , T1.data_connection_id
      FROM            cte_EXCHANGERATEEURODENOMINATIONVIEW T1
      LEFT OUTER JOIN stage_ax.CURRENCYEURODENOMINATION    T2 ON ( T1.PARTITION = T2.PARTITION
                                                               AND T1.data_connection_id = T2.data_connection_id)
     CROSS JOIN       stage_ax.EXCHANGERATECURRENCYPAIR    T3
     CROSS JOIN       stage_ax.EXCHANGERATE T4
     WHERE            ( T3.RECID = T4.EXCHANGERATECURRENCYPAIR
                    AND (T3.PARTITION = T4.PARTITION))
  )
     , cte_ExchangeRateView AS (
    SELECT      T1.FROMCURRENCYCODE AS FROMCURRENCYCODE
              , T1.TOCURRENCYCODE   AS TOCURRENCYCODE
              , T1.EXCHANGERATETYPE AS EXCHANGERATETYPE
              , T1.PARTITION        AS PARTITION
              , T1.RECID            AS RECID
              , T2.PARTITION        AS PARTITION#2
              , T2.EXCHANGERATE     AS EXCHANGERATE
              , T2.VALIDFROM        AS VALIDFROM
              , T2.VALIDTO          AS VALIDTO
              , T3.PARTITION        AS PARTITION#3
              , T1.data_connection_id
      FROM      stage_ax.EXCHANGERATECURRENCYPAIR T1
     CROSS JOIN stage_ax.EXCHANGERATE             T2
     CROSS JOIN stage_ax.EXCHANGERATETYPE T3
     WHERE      ( T1.RECID       = T2.EXCHANGERATECURRENCYPAIR
              AND (T1.PARTITION  = T2.PARTITION))
       AND      ( T1.EXCHANGERATETYPE = T3.RECID
              AND (T1.PARTITION       = T3.PARTITION))
  )
     , cte_EXCHANGERATEUNIONVIEW AS (
    SELECT T1.EXCHANGERATETYPE                          AS EXCHANGERATETYPE
         , T1.VALIDFROM                                 AS VALIDFROM
         , T1.VALIDTO                                   AS VALIDTO
         , T1.PARTITION                                 AS PARTITION
         , 1010                                         AS RECID
         , (CAST((T1.FROMCURRENCYCODE) AS NVARCHAR(3))) AS FROMCURRENCYCODE
         , (CAST((T1.TOCURRENCYCODE) AS NVARCHAR(3)))   AS TOCURRENCYCODE
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16))) AS EXCHANGERATE
         , T1.data_connection_id
      FROM cte_ExchangeRateView T1
    UNION
    SELECT T1.EXCHANGERATETYPE
         , T1.VALIDFROM
         , T1.VALIDTO
         , T1.PARTITION
         , 1010
         , (CAST((T1.TOCURRENCYCODE) AS NVARCHAR(3)))                                                                             AS FROMCURRENCYCODE
         , (CAST((T1.FROMCURRENCYCODE) AS NVARCHAR(3)))                                                                           AS TOCURRENCYCODE
         , (CAST((CASE T1.EXCHANGERATE WHEN 0.0 THEN 0.0 ELSE 100.0 * (1.0 / (T1.EXCHANGERATE / 100.0)) END) AS NUMERIC(32, 16))) AS EXCHANGERATE
         , T1.data_connection_id
      FROM cte_ExchangeRateView T1
     WHERE NOT (EXISTS ( SELECT 'x'
                           FROM stage_ax.EXCHANGERATECURRENCYPAIR T2
                          WHERE ( ( ( T1.TOCURRENCYCODE = T2.FROMCURRENCYCODE
                                  AND ( T1.PARTITION = T2.PARTITION
                                    AND T1.data_connection_id = T2.data_connection_id))
                                AND ( T1.FROMCURRENCYCODE = T2.TOCURRENCYCODE
                                  AND ( T1.PARTITION = T2.PARTITION
                                    AND T1.data_connection_id = T2.data_connection_id)))
                              AND ( T1.EXCHANGERATETYPE = T2.EXCHANGERATETYPE
                                AND ( T1.PARTITION = T2.PARTITION
                                  AND T1.data_connection_id = T2.data_connection_id)))))
  )
     , cte_EXCHANGERATEPRIORTOSTARTDATEVIEW AS (
    SELECT            T1.FROMCURRENCYCODE                                                                                                       AS FROMCURRENCYCODE
                    , T1.TOCURRENCYCODE                                                                                                         AS TOCURRENCYCODE
                    , T1.VALIDFROM                                                                                                              AS VALIDFROM
                    , T1.VALIDTO                                                                                                                AS VALIDTO
                    , T1.EXCHANGERATE                                                                                                           AS EXCHANGERATE
                    , T1.EXCHANGERATETYPE                                                                                                       AS EXCHANGERATETYPE
                    , T1.PARTITION                                                                                                              AS PARTITION
                    , T1.RECID                                                                                                                  AS RECID
                    , T2.PARTITION                                                                                                              AS PARTITION#2
                    , T3.PARTITION                                                                                                              AS PARTITION#3
                    , (CAST((CASE
                               WHEN T2.CURRENCYCODE IS NOT NULL THEN T2.CURRENCYCODE
                               WHEN T3.CURRENCYCODE IS NOT NULL THEN T3.CURRENCYCODE
                               ELSE ''
                             END) AS NVARCHAR(3)))                                                                                              AS FIXEDCURRENCY1
                    , (CAST((CASE
                               WHEN T2.CURRENCYCODE IS NOT NULL
                                AND T3.CURRENCYCODE IS NOT NULL THEN T3.CURRENCYCODE
                               ELSE ''
                             END) AS NVARCHAR(3)))                                                                                              AS FIXEDCURRENCY2
                    , (CAST((CASE
                               WHEN T2.CURRENCYCODE IS NOT NULL THEN T2.STARTDATE
                               WHEN T3.CURRENCYCODE IS NOT NULL THEN T3.STARTDATE
                               ELSE ''
                             END) AS DATETIME))                                                                                                 AS FIXEDSTARTDATE1
                    , (CAST((CASE WHEN T2.CURRENCYCODE IS NOT NULL AND T3.CURRENCYCODE IS NOT NULL THEN T3.STARTDATE ELSE '' END) AS DATETIME)) AS FIXEDSTARTDATE2
                    , (CAST((CASE
                               WHEN T2.CURRENCYCODE IS NOT NULL THEN T2.EUROCURRENCYCODE
                               WHEN T3.CURRENCYCODE IS NOT NULL THEN T3.EUROCURRENCYCODE
                               ELSE ''
                             END) AS NVARCHAR(3)))                                                                                              AS EUROCURRENCYCODE
                    , T1.data_connection_id
      FROM            cte_EXCHANGERATEUNIONVIEW            T1
      LEFT OUTER JOIN cte_EXCHANGERATEEURODENOMINATIONVIEW T2 ON ( T1.FROMCURRENCYCODE     = T2.CURRENCYCODE
                                                               AND ( T1.PARTITION          = T2.PARTITION
                                                                 AND T1.data_connection_id = T2.data_connection_id))
      LEFT OUTER JOIN cte_EXCHANGERATEEURODENOMINATIONVIEW T3 ON ( T1.TOCURRENCYCODE       = T3.CURRENCYCODE
                                                               AND ( T1.PARTITION          = T3.PARTITION
                                                                 AND T1.data_connection_id = T2.data_connection_id))
  )
     , cte_EXCHANGERATESAMEFROMTOCURRENCYVIEW AS (
    SELECT            T1.RECID                           AS EXCHANGERATETYPERECID
                    , T1.PARTITION                       AS PARTITION
                    , T1.RECID                           AS RECID
                    , T2.PARTITION                       AS PARTITION#2
                    , T2.CURRENCYCODE                    AS FROMCURRENCYCODE
                    , T2.CURRENCYCODE                    AS TOCURRENCYCODE
                    , (CAST(('19000101') AS DATETIME))   AS VALIDFROM
                    , (CAST(('21541231') AS DATETIME))   AS VALIDTO
                    , (CAST((100.0) AS NUMERIC(32, 16))) AS EXCHANGERATE
                    , T1.data_connection_id
      FROM            stage_ax.EXCHANGERATETYPE T1
      LEFT OUTER JOIN stage_ax.CURRENCY         T2 ON ( T1.PARTITION          = T2.PARTITION
                                                    AND T1.data_connection_id = T2.data_connection_id)
  )
     , EXCHANGERATEDENTOEUROAFTERSTART AS (
    SELECT            T1.EUROCURRENCYCODE AS EUROCURRENCYCODE
                    , T1.CURRENCYCODE     AS CURRENCYCODE
                    , T1.STARTDATE        AS STARTDATE
                    , T1.EXCHANGERATE     AS EXCHANGERATE
                    , T1.PARTITION        AS PARTITION
                    , T1.RECID            AS RECID
                    , T2.PARTITION        AS PARTITION#2
                    , T2.RECID            AS EXCHANGERATETYPE
                    , T1.data_connection_id
      FROM            cte_EXCHANGERATEEURODENOMINATIONVIEW T1
      LEFT OUTER JOIN stage_ax.EXCHANGERATETYPE            T2 ON ( T1.PARTITION          = T2.PARTITION
                                                               AND T1.data_connection_id = T2.data_connection_id)
  )
     , cte_EXCHANGERATEDENTODENAFTERBOTHSTART AS (
    SELECT            T1.CURRENCYCODE AS CURRENCYCODE1
                    , T1.STARTDATE    AS STARTDATE1
                    , T1.EXCHANGERATE AS EXCHANGERATE1
                    , T1.PARTITION    AS PARTITION
                    , T1.RECID        AS RECID
                    , T2.PARTITION    AS PARTITION#2
                    , T2.STARTDATE    AS STARTDATE2
                    , T2.EXCHANGERATE AS EXCHANGERATE2
                    , T2.CURRENCYCODE AS CURRENCYCODE2
                    , T3.PARTITION    AS PARTITION#3
                    , T3.RECID        AS EXCHANGERATETYPE
                    , T1.data_connection_id
      FROM            cte_EXCHANGERATEEURODENOMINATIONVIEW T1
      LEFT OUTER JOIN stage_ax.CURRENCYEURODENOMINATION    T2 ON ( T2.CURRENCYCODE         <> T1.CURRENCYCODE
                                                               AND ( T2.PARTITION          = T1.PARTITION
                                                                 AND T1.data_connection_id = T2.data_connection_id))
      LEFT OUTER JOIN stage_ax.EXCHANGERATETYPE            T3 ON ( T1.PARTITION            = T3.PARTITION
                                                               AND T1.data_connection_id   = T3.data_connection_id)
  )
     , cte_EXCHANGERATEDENTOVARAFTERSTARTVIEW AS (
    SELECT      T1.EXCHANGERATETYPE AS EXCHANGERATETYPE
              , T1.FROMCURRENCYCODE AS FROMCURRENCYCODE
              , T1.TOCURRENCYCODE   AS TOCURRENCYCODE
              , T1.PARTITION        AS PARTITION
              , T1.RECID            AS RECID
              , T2.PARTITION        AS PARTITION#2
              , T2.STARTDATE        AS STARTDATE
              , T2.CURRENCYCODE     AS EURODENCURRENCYCODE
              , T2.EXCHANGERATE     AS EURODENEXCHANGERATE
              , T3.PARTITION        AS PARTITION#3
              , T3.VALIDFROM        AS VALIDFROM
              , T3.VALIDTO          AS VALIDTO
              , T3.EXCHANGERATE     AS EXCHANGERATE
              , T1.data_connection_id
      FROM      stage_ax.EXCHANGERATECURRENCYPAIR    T1
     CROSS JOIN cte_EXCHANGERATEEURODENOMINATIONVIEW T2
     CROSS JOIN stage_ax.EXCHANGERATE T3
     WHERE      ( T1.FROMCURRENCYCODE     = T2.EUROCURRENCYCODE
              AND ( T1.PARTITION          = T2.PARTITION
                AND T1.data_connection_id = T2.data_connection_id))
       AND      ( ( T3.VALIDTO                 >= T2.STARTDATE
                AND ( T3.PARTITION             = T2.PARTITION
                  AND T3.data_connection_id    = T2.data_connection_id))
              AND ( T1.RECID                   = T3.EXCHANGERATECURRENCYPAIR
                AND ( T1.PARTITION             = T3.PARTITION
                  AND T1.data_connection_id    = T3.data_connection_id)))
       AND      NOT (EXISTS ( SELECT 'x'
                                FROM cte_EXCHANGERATEEURODENOMINATIONVIEW T4
                               WHERE ( T1.TOCURRENCYCODE = T4.CURRENCYCODE
                                   AND ( T1.PARTITION = T4.PARTITION
                                     AND T1.data_connection_id = T4.data_connection_id))))
  )
     , cte_EXCHANGERATEVARTODENAFTERSTARTVIEW AS (
    SELECT      T1.EXCHANGERATETYPE AS EXCHANGERATETYPE
              , T1.FROMCURRENCYCODE AS FROMCURRENCYCODE
              , T1.TOCURRENCYCODE   AS TOCURRENCYCODE
              , T1.PARTITION        AS PARTITION
              , T1.RECID            AS RECID
              , T2.PARTITION        AS PARTITION#2
              , T2.STARTDATE        AS STARTDATE
              , T2.CURRENCYCODE     AS EURODENCURRENCYCODE
              , T2.EXCHANGERATE     AS EURODENEXCHANGERATE
              , T3.PARTITION        AS PARTITION#3
              , T3.VALIDFROM        AS VALIDFROM
              , T3.VALIDTO          AS VALIDTO
              , T3.EXCHANGERATE     AS EXCHANGERATE
              , T1.data_connection_id
      FROM      stage_ax.EXCHANGERATECURRENCYPAIR    T1
     CROSS JOIN cte_EXCHANGERATEEURODENOMINATIONVIEW T2
     CROSS JOIN stage_ax.EXCHANGERATE T3
     WHERE      ( T1.TOCURRENCYCODE       = T2.EUROCURRENCYCODE
              AND ( T1.PARTITION          = T2.PARTITION
                AND T1.data_connection_id = T2.data_connection_id))
       AND      ( ( T3.VALIDTO                 >= T2.STARTDATE
                AND ( T3.PARTITION             = T2.PARTITION
                  AND T1.data_connection_id    = T2.data_connection_id))
              AND ( T1.RECID                   = T3.EXCHANGERATECURRENCYPAIR
                AND ( T1.PARTITION             = T3.PARTITION
                  AND T1.data_connection_id    = T3.data_connection_id)))
       AND      (NOT (EXISTS ( SELECT 'x'
                                 FROM cte_EXCHANGERATEEURODENOMINATIONVIEW T4
                                WHERE ( T1.FROMCURRENCYCODE = T4.CURRENCYCODE
                                    AND ( T1.PARTITION = T4.PARTITION
                                      AND T1.data_connection_id = T4.data_connection_id)))))
       AND      (NOT (EXISTS ( SELECT 'x'
                                 FROM stage_ax.EXCHANGERATECURRENCYPAIR T5
                                WHERE ( ( ( T1.EXCHANGERATETYPE = T5.EXCHANGERATETYPE
                                        AND ( T1.PARTITION = T5.PARTITION
                                          AND T1.data_connection_id = T5.data_connection_id))
                                      AND ( T1.TOCURRENCYCODE = T5.FROMCURRENCYCODE
                                        AND ( T1.PARTITION = T5.PARTITION
                                          AND T1.data_connection_id = T5.data_connection_id)))
                                    AND ( T1.FROMCURRENCYCODE = T5.TOCURRENCYCODE
                                      AND ( T1.PARTITION = T5.PARTITION
                                        AND T1.data_connection_id = T5.data_connection_id))))))
  )
     , cte_EXCHANGERATEEFFECTIVEVIEW AS (
    SELECT T1.PARTITION                                 AS PARTITION
         , T1.RECID                                     AS RECID
         , 1                                            AS UnionAllBranchId
         , (CAST((T1.FROMCURRENCYCODE) AS NVARCHAR(3))) AS FROMCURRENCYCODE
         , (CAST((T1.TOCURRENCYCODE) AS NVARCHAR(3)))   AS TOCURRENCYCODE
         , (CAST((T1.VALIDFROM) AS DATETIME))           AS VALIDFROM
         , (CAST((T1.VALIDTO) AS DATETIME))             AS VALIDTO
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16))) AS EXCHANGERATE1
         , (CAST((0.0) AS NUMERIC(32, 16)))             AS EXCHANGERATE2
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16))) AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))      AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEPRIORTOSTARTDATEVIEW T1
     WHERE ( (FIXEDCURRENCY1 = '')
         AND (FIXEDCURRENCY2 = ''))
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 2
         , (CAST((T1.FROMCURRENCYCODE) AS NVARCHAR(3)))                                                                     AS FROMCURRENCYCODE
         , (CAST((T1.TOCURRENCYCODE) AS NVARCHAR(3)))                                                                       AS TOCURRENCYCODE
         , (CAST((T1.VALIDFROM) AS DATETIME))                                                                               AS VALIDFROM
         , (CAST((CASE WHEN T1.VALIDTO <= T1.FIXEDSTARTDATE1 THEN T1.VALIDTO ELSE T1.FIXEDSTARTDATE1 - 1 END) AS DATETIME)) AS VALIDTO
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16)))                                                                     AS EXCHANGERATE1
         , (CAST((0.0) AS NUMERIC(32, 16)))                                                                                 AS EXCHANGERATE2
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16)))                                                                     AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                                          AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEPRIORTOSTARTDATEVIEW T1
     WHERE ( ( NOT ((FIXEDCURRENCY1 = ''))
           AND (FIXEDCURRENCY2 = ''))
         AND (VALIDFROM < FIXEDSTARTDATE1))
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 3
         , (CAST((T1.FROMCURRENCYCODE) AS NVARCHAR(3))) AS FROMCURRENCYCODE
         , (CAST((T1.TOCURRENCYCODE) AS NVARCHAR(3)))   AS TOCURRENCYCODE
         , (CAST((T1.VALIDFROM) AS DATETIME))           AS VALIDFROM
         , (CAST((CASE
                    WHEN T1.VALIDTO <= T1.FIXEDSTARTDATE1
                     AND T1.VALIDTO <= T1.FIXEDSTARTDATE2 THEN T1.VALIDTO
                    WHEN T1.FIXEDSTARTDATE1 <= T1.FIXEDSTARTDATE2 THEN T1.FIXEDSTARTDATE1 - 1
                    ELSE T1.FIXEDSTARTDATE2 - 1
                  END) AS DATETIME))                    AS VALIDTO
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16))) AS EXCHANGERATE1
         , (CAST((0.0) AS NUMERIC(32, 16)))             AS EXCHANGERATE2
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16))) AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))      AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEPRIORTOSTARTDATEVIEW T1
     WHERE ( ( NOT ((FIXEDCURRENCY1 = ''))
           AND NOT ((FIXEDCURRENCY2 = '')))
         AND ( (VALIDFROM < FIXEDSTARTDATE1)
           AND (VALIDFROM < FIXEDSTARTDATE2)))
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 4
         , (CAST((T1.FROMCURRENCYCODE) AS NVARCHAR(3))) AS FROMCURRENCYCODE
         , (CAST((T1.TOCURRENCYCODE) AS NVARCHAR(3)))   AS TOCURRENCYCODE
         , (CAST((T1.VALIDFROM) AS DATETIME))           AS VALIDFROM
         , (CAST((T1.VALIDTO) AS DATETIME))             AS VALIDTO
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16))) AS EXCHANGERATE1
         , (CAST((0.0) AS NUMERIC(32, 16)))             AS EXCHANGERATE2
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16))) AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPERECID) AS BIGINT)) AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATESAMEFROMTOCURRENCYVIEW T1
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 5
         , (CAST((T1.EUROCURRENCYCODE) AS NVARCHAR(3))) AS FROMCURRENCYCODE
         , (CAST((T1.CURRENCYCODE) AS NVARCHAR(3)))     AS TOCURRENCYCODE
         , (CAST((T1.STARTDATE) AS DATETIME))           AS VALIDFROM
         , (CAST(('21541231') AS DATETIME))             AS VALIDTO
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16))) AS EXCHANGERATE1
         , (CAST((0.0) AS NUMERIC(32, 16)))             AS EXCHANGERATE2
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16))) AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))      AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM EXCHANGERATEDENTOEUROAFTERSTART T1
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 6
         , (CAST((T1.CURRENCYCODE) AS NVARCHAR(3)))                                                                               AS FROMCURRENCYCODE
         , (CAST((T1.EUROCURRENCYCODE) AS NVARCHAR(3)))                                                                           AS TOCURRENCYCODE
         , (CAST((T1.STARTDATE) AS DATETIME))                                                                                     AS VALIDFROM
         , (CAST(('21541231') AS DATETIME))                                                                                       AS VALIDTO
         , (CAST((CASE T1.EXCHANGERATE WHEN 0.0 THEN 0.0 ELSE 100.0 * (1.0 / (T1.EXCHANGERATE / 100.0)) END) AS NUMERIC(32, 16))) AS EXCHANGERATE1
         , (CAST((0.0) AS NUMERIC(32, 16)))                                                                                       AS EXCHANGERATE2
         , (CAST((CASE T1.EXCHANGERATE WHEN 0.0 THEN 0.0 ELSE 100.0 * (1.0 / (T1.EXCHANGERATE / 100.0)) END) AS NUMERIC(32, 16))) AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                                                AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM EXCHANGERATEDENTOEUROAFTERSTART T1
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 7
         , (CAST((T1.EURODENCURRENCYCODE) AS NVARCHAR(3)))                                                      AS FROMCURRENCYCODE
         , (CAST((T1.TOCURRENCYCODE) AS NVARCHAR(3)))                                                           AS TOCURRENCYCODE
         , (CAST((CASE WHEN T1.VALIDFROM >= T1.STARTDATE THEN T1.VALIDFROM ELSE T1.STARTDATE END) AS DATETIME)) AS VALIDFROM
         , (CAST((T1.VALIDTO) AS DATETIME))                                                                     AS VALIDTO
         , (CAST((T1.EURODENEXCHANGERATE) AS NUMERIC(32, 16)))                                                  AS EXCHANGERATE1
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16)))                                                         AS EXCHANGERATE2
         , (CAST((CASE T1.EURODENEXCHANGERATE
                    WHEN 0.0 THEN 0.0
                    ELSE 100.0 * T1.EXCHANGERATE / T1.EURODENEXCHANGERATE
                  END) AS NUMERIC(32, 16)))                                                                     AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                              AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEDENTOVARAFTERSTARTVIEW T1
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 8
         , (CAST((T1.TOCURRENCYCODE) AS NVARCHAR(3)))                                                                                    AS FROMCURRENCYCODE
         , (CAST((T1.EURODENCURRENCYCODE) AS NVARCHAR(3)))                                                                               AS TOCURRENCYCODE
         , (CAST((CASE WHEN T1.VALIDFROM >= T1.STARTDATE THEN T1.VALIDFROM ELSE T1.STARTDATE END) AS DATETIME))                          AS VALIDFROM
         , (CAST((T1.VALIDTO) AS DATETIME))                                                                                              AS VALIDTO
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16)))                                                                                  AS EXCHANGERATE1
         , (CAST((T1.EURODENEXCHANGERATE) AS NUMERIC(32, 16)))                                                                           AS EXCHANGERATE2
         , (CAST((CASE T1.EXCHANGERATE WHEN 0.0 THEN 0.0 ELSE 100.0 * T1.EURODENEXCHANGERATE / T1.EXCHANGERATE END) AS NUMERIC(32, 16))) AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                                                       AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEDENTOVARAFTERSTARTVIEW T1
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 9
         , (CAST((T1.FROMCURRENCYCODE) AS NVARCHAR(3)))                                                                           AS FROMCURRENCYCODE
         , (CAST((T1.EURODENCURRENCYCODE) AS NVARCHAR(3)))                                                                        AS TOCURRENCYCODE
         , (CAST((CASE WHEN T1.VALIDFROM >= T1.STARTDATE THEN T1.VALIDFROM ELSE T1.STARTDATE END) AS DATETIME))                   AS VALIDFROM
         , (CAST((T1.VALIDTO) AS DATETIME))                                                                                       AS VALIDTO
         , (CAST((CASE T1.EXCHANGERATE WHEN 0.0 THEN 0.0 ELSE 100.0 * (1.0 / (T1.EXCHANGERATE / 100.0)) END) AS NUMERIC(32, 16))) AS EXCHANGERATE1
         , (CAST((T1.EURODENEXCHANGERATE) AS NUMERIC(32, 16)))                                                                    AS EXCHANGERATE2
         , (CAST((CASE T1.EXCHANGERATE
                    WHEN 0.0 THEN 0.0
                    ELSE 100.0 * T1.EURODENEXCHANGERATE / (100.0 * (1.0 / (T1.EXCHANGERATE / 100.0)))
                  END) AS NUMERIC(32, 16)))                                                                                       AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                                                AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEVARTODENAFTERSTARTVIEW T1
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 10
         , (CAST((T1.EURODENCURRENCYCODE) AS NVARCHAR(3)))                                                                        AS FROMCURRENCYCODE
         , (CAST((T1.FROMCURRENCYCODE) AS NVARCHAR(3)))                                                                           AS TOCURRENCYCODE
         , (CAST((CASE WHEN T1.VALIDFROM >= T1.STARTDATE THEN T1.VALIDFROM ELSE T1.STARTDATE END) AS DATETIME))                   AS VALIDFROM
         , (CAST((T1.VALIDTO) AS DATETIME))                                                                                       AS VALIDTO
         , (CAST((T1.EURODENEXCHANGERATE) AS NUMERIC(32, 16)))                                                                    AS EXCHANGERATE1
         , (CAST((CASE T1.EXCHANGERATE WHEN 0.0 THEN 0.0 ELSE 100.0 * (1.0 / (T1.EXCHANGERATE / 100.0)) END) AS NUMERIC(32, 16))) AS EXCHANGERATE2
         , (CAST((CASE
                    WHEN T1.EXCHANGERATE = 0.0
                      OR T1.EURODENEXCHANGERATE = 0.0 THEN 0.0
                    ELSE 100.0 * (100.0 * (1.0 / (T1.EXCHANGERATE / 100.0))) / T1.EURODENEXCHANGERATE
                  END) AS NUMERIC(32, 16)))                                                                                       AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                                                AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEVARTODENAFTERSTARTVIEW T1
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 11
         , (CAST((T1.CURRENCYCODE1) AS NVARCHAR(3)))                                                                                 AS FROMCURRENCYCODE
         , (CAST((T1.CURRENCYCODE2) AS NVARCHAR(3)))                                                                                 AS TOCURRENCYCODE
         , (CAST((CASE WHEN T1.STARTDATE1 >= T1.STARTDATE2 THEN T1.STARTDATE1 ELSE T1.STARTDATE2 END) AS DATETIME))                  AS VALIDFROM
         , (CAST(('21541231') AS DATETIME))                                                                                          AS VALIDTO
         , (CAST((T1.EXCHANGERATE1) AS NUMERIC(32, 16)))                                                                             AS EXCHANGERATE1
         , (CAST((T1.EXCHANGERATE2) AS NUMERIC(32, 16)))                                                                             AS EXCHANGERATE2
         , (CAST((CASE T1.EXCHANGERATE1 WHEN 0.0 THEN 0.0 ELSE 100.0 * T1.EXCHANGERATE2 / T1.EXCHANGERATE1 END) AS NUMERIC(32, 16))) AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                                                   AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEDENTODENAFTERBOTHSTART T1
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 12
         , (CAST((T1.CURRENCYCODE1) AS NVARCHAR(3)))                                                              AS FROMCURRENCYCODE
         , (CAST((T1.CURRENCYCODE2) AS NVARCHAR(3)))                                                              AS TOCURRENCYCODE
         , (CAST((CASE WHEN T1.VALIDFROM >= T1.STARTDATE1 THEN T1.VALIDFROM ELSE T1.STARTDATE1 END) AS DATETIME)) AS VALIDFROM
         , (CAST((CASE WHEN T1.VALIDTO <= T1.STARTDATE2 THEN T1.VALIDTO ELSE T1.STARTDATE2 - 1 END) AS DATETIME)) AS VALIDTO
         , (CAST((T1.EURODENOMINATIONRATE1) AS NUMERIC(32, 16)))                                                  AS EXCHANGERATE1
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16)))                                                           AS EXCHANGERATE2
         , (CAST((CASE T1.EURODENOMINATIONRATE1
                    WHEN 0.0 THEN 0.0
                    ELSE 100.0 * T1.EXCHANGERATE / T1.EURODENOMINATIONRATE1
                  END) AS NUMERIC(32, 16)))                                                                       AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                                AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEDENTODENBETWEENSTART T1
     WHERE ( ( ( (FROMCURRENCYCODE = EUROCURRENCYCODE)
             AND (TOCURRENCYCODE = CURRENCYCODE2))
           AND (STARTDATE1 < STARTDATE2))
         AND ( ( ( ( (VALIDFROM <= STARTDATE1)
                 AND (VALIDTO >= STARTDATE1))
                OR ( (VALIDFROM >= STARTDATE1)
                 AND (VALIDTO <= STARTDATE2)))
              OR ( (VALIDFROM < STARTDATE2)
               AND (VALIDTO >= STARTDATE2)))
            OR ( (VALIDFROM < STARTDATE1)
             AND (VALIDTO > STARTDATE2))))
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 13
         , (CAST((T1.CURRENCYCODE1) AS NVARCHAR(3)))                                                                              AS FROMCURRENCYCODE
         , (CAST((T1.CURRENCYCODE2) AS NVARCHAR(3)))                                                                              AS TOCURRENCYCODE
         , (CAST((CASE WHEN T1.VALIDFROM >= T1.STARTDATE1 THEN T1.VALIDFROM ELSE T1.STARTDATE1 END) AS DATETIME))                 AS VALIDFROM
         , (CAST((CASE WHEN T1.VALIDTO <= T1.STARTDATE2 THEN T1.VALIDTO ELSE T1.STARTDATE2 - 1 END) AS DATETIME))                 AS VALIDTO
         , (CAST((T1.EURODENOMINATIONRATE1) AS NUMERIC(32, 16)))                                                                  AS EXCHANGERATE1
         , (CAST((CASE T1.EXCHANGERATE WHEN 0.0 THEN 0.0 ELSE 100.0 * (1.0 / (T1.EXCHANGERATE / 100.0)) END) AS NUMERIC(32, 16))) AS EXCHANGERATE2
         , (CAST((CASE
                    WHEN T1.EXCHANGERATE = 0.0
                      OR T1.EURODENOMINATIONRATE1 = 0.0 THEN 0.0
                    ELSE 100.0 * (100.0 * (1.0 / (T1.EXCHANGERATE / 100.0))) / T1.EURODENOMINATIONRATE1
                  END) AS NUMERIC(32, 16)))                                                                                       AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                                                AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEDENTODENBETWEENSTART T1
     WHERE ( ( ( ( T1.FROMCURRENCYCODE         = T1.CURRENCYCODE2
               AND ( T1.PARTITION              = T1.PARTITION
                 AND T1.data_connection_id     = T1.data_connection_id))
             AND ( T1.TOCURRENCYCODE           = T1.EUROCURRENCYCODE
               AND ( T1.PARTITION              = T1.PARTITION
                 AND T1.data_connection_id     = T1.data_connection_id)))
           AND ( T1.STARTDATE1                 < T1.STARTDATE2
             AND ( T1.PARTITION                = T1.PARTITION
               AND T1.data_connection_id       = T1.data_connection_id)))
         AND ( ( ( ( ( T1.VALIDFROM            <= T1.STARTDATE1
                   AND ( T1.PARTITION          = T1.PARTITION
                     AND T1.data_connection_id = T1.data_connection_id))
                 AND ( T1.VALIDTO              >= T1.STARTDATE1
                   AND ( T1.PARTITION          = T1.PARTITION
                     AND T1.data_connection_id = T1.data_connection_id)))
                OR ( ( T1.VALIDFROM            >= T1.STARTDATE1
                   AND ( T1.PARTITION          = T1.PARTITION
                     AND T1.data_connection_id = T1.data_connection_id))
                 AND ( T1.VALIDTO              <= T1.STARTDATE2
                   AND ( T1.PARTITION          = T1.PARTITION
                     AND T1.data_connection_id = T1.data_connection_id))))
              OR ( ( T1.VALIDFROM              < T1.STARTDATE2
                 AND ( T1.PARTITION            = T1.PARTITION
                   AND T1.data_connection_id   = T1.data_connection_id))
               AND ( T1.VALIDTO                >= T1.STARTDATE2
                 AND ( T1.PARTITION            = T1.PARTITION
                   AND T1.data_connection_id   = T1.data_connection_id))))
            OR ( ( T1.VALIDFROM                < T1.STARTDATE1
               AND ( T1.PARTITION              = T1.PARTITION
                 AND T1.data_connection_id     = T1.data_connection_id))
             AND ( T1.VALIDTO                  > T1.STARTDATE2
               AND ( T1.PARTITION              = T1.PARTITION
                 AND T1.data_connection_id     = T1.data_connection_id)))))
       AND NOT (EXISTS ( SELECT 'x'
                           FROM stage_ax.EXCHANGERATECURRENCYPAIR T2
                          WHERE ( ( ( T1.FROMCURRENCYCODE = T2.TOCURRENCYCODE
                                  AND ( T1.PARTITION = T2.PARTITION
                                    AND T1.data_connection_id = T2.data_connection_id))
                                AND ( T1.TOCURRENCYCODE = T2.FROMCURRENCYCODE
                                  AND ( T1.PARTITION = T2.PARTITION
                                    AND T1.data_connection_id = T2.data_connection_id)))
                              AND ( T1.EXCHANGERATETYPE = T2.EXCHANGERATETYPE
                                AND ( T1.PARTITION = T2.PARTITION
                                  AND T1.data_connection_id = T2.data_connection_id)))))
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 14
         , (CAST((T1.CURRENCYCODE1) AS NVARCHAR(3)))                                                                                       AS FROMCURRENCYCODE
         , (CAST((T1.CURRENCYCODE2) AS NVARCHAR(3)))                                                                                       AS TOCURRENCYCODE
         , (CAST((CASE WHEN T1.VALIDFROM >= T1.STARTDATE2 THEN T1.VALIDFROM ELSE T1.STARTDATE2 END) AS DATETIME))                          AS VALIDFROM
         , (CAST((CASE WHEN T1.VALIDTO <= T1.STARTDATE1 THEN T1.VALIDTO ELSE T1.STARTDATE1 - 1 END) AS DATETIME))                          AS VALIDTO
         , (CAST((T1.EXCHANGERATE) AS NUMERIC(32, 16)))                                                                                    AS EXCHANGERATE1
         , (CAST((T1.EURODENOMINATIONRATE2) AS NUMERIC(32, 16)))                                                                           AS EXCHANGERATE2
         , (CAST((CASE T1.EXCHANGERATE WHEN 0.0 THEN 0.0 ELSE 100.0 * T1.EURODENOMINATIONRATE2 / T1.EXCHANGERATE END) AS NUMERIC(32, 16))) AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                                                         AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEDENTODENBETWEENSTART T1
     WHERE ( ( ( (FROMCURRENCYCODE = EUROCURRENCYCODE)
             AND (TOCURRENCYCODE = CURRENCYCODE1))
           AND (STARTDATE1 > STARTDATE2))
         AND ( ( ( ( (VALIDFROM <= STARTDATE2)
                 AND (VALIDTO >= STARTDATE2))
                OR ( (VALIDFROM >= STARTDATE2)
                 AND (VALIDTO <= STARTDATE1)))
              OR ( (VALIDFROM < STARTDATE1)
               AND (VALIDTO >= STARTDATE1)))
            OR ( (VALIDFROM < STARTDATE2)
             AND (VALIDTO > STARTDATE1))))
    UNION ALL
    SELECT T1.PARTITION
         , T1.RECID
         , 15
         , (CAST((T1.CURRENCYCODE1) AS NVARCHAR(3)))                                                                              AS FROMCURRENCYCODE
         , (CAST((T1.CURRENCYCODE2) AS NVARCHAR(3)))                                                                              AS TOCURRENCYCODE
         , (CAST((CASE WHEN T1.VALIDFROM >= T1.STARTDATE2 THEN T1.VALIDFROM ELSE T1.STARTDATE2 END) AS DATETIME))                 AS VALIDFROM
         , (CAST((CASE WHEN T1.VALIDTO <= T1.STARTDATE1 THEN T1.VALIDTO ELSE T1.STARTDATE1 - 1 END) AS DATETIME))                 AS VALIDTO
         , (CAST((CASE T1.EXCHANGERATE WHEN 0.0 THEN 0.0 ELSE 100.0 * (1.0 / (T1.EXCHANGERATE / 100.0)) END) AS NUMERIC(32, 16))) AS EXCHANGERATE1
         , (CAST((T1.EURODENOMINATIONRATE2) AS NUMERIC(32, 16)))                                                                  AS EXCHANGERATE2
         , (CAST((CASE T1.EXCHANGERATE
                    WHEN 0.0 THEN 0.0
                    ELSE 100.0 * T1.EURODENOMINATIONRATE2 / (100.0 * (1.0 / (T1.EXCHANGERATE / 100.0)))
                  END) AS NUMERIC(32, 16)))                                                                                       AS CROSSRATE
         , (CAST((T1.EXCHANGERATETYPE) AS BIGINT))                                                                                AS EXCHANGERATETYPE
         , T1.data_connection_id
      FROM cte_EXCHANGERATEDENTODENBETWEENSTART T1
     WHERE ( ( ( ( T1.FROMCURRENCYCODE         = T1.CURRENCYCODE1
               AND ( T1.PARTITION              = T1.PARTITION
                 AND T1.data_connection_id     = T1.data_connection_id))
             AND ( T1.TOCURRENCYCODE           = T1.EUROCURRENCYCODE
               AND ( T1.PARTITION              = T1.PARTITION
                 AND T1.data_connection_id     = T1.data_connection_id)))
           AND ( T1.STARTDATE1                 > T1.STARTDATE2
             AND ( T1.PARTITION                = T1.PARTITION
               AND T1.data_connection_id       = T1.data_connection_id)))
         AND ( ( ( ( ( T1.VALIDFROM            <= T1.STARTDATE2
                   AND ( T1.PARTITION          = T1.PARTITION
                     AND T1.data_connection_id = T1.data_connection_id))
                 AND ( T1.VALIDTO              >= T1.STARTDATE2
                   AND ( T1.PARTITION          = T1.PARTITION
                     AND T1.data_connection_id = T1.data_connection_id)))
                OR ( ( T1.VALIDFROM            >= T1.STARTDATE2
                   AND ( T1.PARTITION          = T1.PARTITION
                     AND T1.data_connection_id = T1.data_connection_id))
                 AND ( T1.VALIDTO              <= T1.STARTDATE1
                   AND ( T1.PARTITION          = T1.PARTITION
                     AND T1.data_connection_id = T1.data_connection_id))))
              OR ( ( T1.VALIDFROM              < T1.STARTDATE1
                 AND ( T1.PARTITION            = T1.PARTITION
                   AND T1.data_connection_id   = T1.data_connection_id))
               AND ( T1.VALIDTO                >= T1.STARTDATE1
                 AND ( T1.PARTITION            = T1.PARTITION
                   AND T1.data_connection_id   = T1.data_connection_id))))
            OR ( ( T1.VALIDFROM                < T1.STARTDATE2
               AND ( T1.PARTITION              = T1.PARTITION
                 AND T1.data_connection_id     = T1.data_connection_id))
             AND ( T1.VALIDTO                  > T1.STARTDATE1
               AND ( T1.PARTITION              = T1.PARTITION
                 AND T1.data_connection_id     = T1.data_connection_id)))))
       AND NOT (EXISTS ( SELECT 'x'
                           FROM stage_ax.EXCHANGERATECURRENCYPAIR T2
                          WHERE ( ( ( T1.FROMCURRENCYCODE = T2.TOCURRENCYCODE
                                  AND ( T1.PARTITION = T2.PARTITION
                                    AND T1.data_connection_id = T2.data_connection_id))
                                AND ( T1.TOCURRENCYCODE = T2.FROMCURRENCYCODE
                                  AND ( T1.PARTITION = T2.PARTITION
                                    AND T1.data_connection_id = T2.data_connection_id)))
                              AND ( T1.EXCHANGERATETYPE = T2.EXCHANGERATETYPE
                                AND ( T1.PARTITION = T2.PARTITION
                                  AND T1.data_connection_id = T2.data_connection_id)))))
  )
  SELECT      CROSSRATE = erv.CROSSRATE / 100 --- to get the actual rate we need to divide the rate 
            , erv.EXCHANGERATE1
            , erv.EXCHANGERATE2
            , erv.EXCHANGERATETYPE
            , erv.FROMCURRENCYCODE
            , erv.TOCURRENCYCODE
            , erv.VALIDFROM
            , erv.VALIDTO
            , erv.PARTITION
            , erv.RECID
            , erv.data_connection_id
    FROM      cte_EXCHANGERATEEFFECTIVEVIEW AS erv
   CROSS JOIN ( SELECT TOP 1
                       5637144576 -- the initial partition should always be the same....
                  AS INITIAL_PARTITION)     INITIAL_PARTITION
   WHERE      1            = 1
     -- filters the initial partition, to only select a single partition for exchange rates. When joining with exchange rates the the partition does not need to be joined
     AND      (PARTITION        = INITIAL_PARTITION.INITIAL_PARTITION)
     -- filters the exchange rate type, to only select the main exchange rate type configured in ax.
     AND      (EXCHANGERATETYPE = ( SELECT TOP (1)
                                           SYSTEMEXCHANGERATETYPE
                                      FROM stage_ax.SYSTEMPARAMETERS
                                     WHERE (PARTITION              = INITIAL_PARTITION.INITIAL_PARTITION)
                                       AND (SYSTEMEXCHANGERATETYPE > 0))) ;
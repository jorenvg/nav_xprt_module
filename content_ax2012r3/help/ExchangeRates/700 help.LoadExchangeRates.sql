EXEC dbo.drop_object @object = N'help.LoadExchangeRates' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;                        -- int
GO

CREATE PROCEDURE help.LoadExchangeRates
  @component_execution_id INT     = -1
, @load_type              TINYINT = 0
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  IF @load_type = 1 --incremental, not supported--> full load
  BEGIN
    --log warning
    DECLARE @proc sysname = dbo.get_proc_full_name(@@PROCID) ;
    EXEC [audit].[log_warning_unsupported_load_type] @procedure = @proc
                                                   , @load_type = @load_type
                                                   , @component_execution_id = @component_execution_id ;
  END ;

  SELECT @deleted = COUNT(1) FROM help.ExchangeRates ;
  EXECUTE dbo.truncate_table 'help.ExchangeRates' ;

  INSERT INTO help.ExchangeRates WITH (TABLOCK) (
    CrossRate
  , ExchangeRate1
  , ExchangeRate2
  , ExchangeRateType
  , FromCurrencyCode
  , ToCurrencyCode
  , ValidFrom
  , ValidTo
  , RecId
  , PARTITION
  -- System
  , DataConnectionID
  , ComponentExecutionID
  )
  SELECT CROSSRATE
       , EXCHANGERATE1
       , EXCHANGERATE2
       , EXCHANGERATETYPE
       , FROMCURRENCYCODE
       , TOCURRENCYCODE
       , VALIDFROM
       , ValidTo
       , RECID
       , PARTITION
       , data_connection_id
       , COALESCE(@component_execution_id, -1)
    FROM help.ExchangeRatesView ;

  SELECT @inserted = @@ROWCOUNT, @updated = 0 ;
END ;
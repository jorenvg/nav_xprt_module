-- ################################################################################ --
-- #####                     Table: help.ExchangeRates	                          ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'help.ExchangeRates', 'T' ;

CREATE TABLE help.ExchangeRates
(
  CrossRate            DECIMAL(38, 20) NOT NULL --only this rate is used in the BI
, ExchangeRate1        DECIMAL(38, 20) NOT NULL
, ExchangeRate2        DECIMAL(38, 20) NOT NULL
, ExchangeRateType     BIGINT          NOT NULL
, FromCurrencyCode     NVARCHAR(20)    NOT NULL
, ToCurrencyCode       NVARCHAR(20)    NOT NULL
, ValidFrom            DATETIME        NOT NULL
, ValidTo              DATETIME        NOT NULL
, RecId                BIGINT          NOT NULL
, Partition            BIGINT          NOT NULL
                                                -- System
, DataConnectionID     INT             NOT NULL
, ExecutionTimestamp   ROWVERSION      NOT NULL
, ComponentExecutionID INT             NOT NULL
) ;

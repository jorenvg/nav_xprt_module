EXEC dbo.drop_object @object = N'help.Inventory_CubeView', @type = N'V' ;
GO
CREATE VIEW help.Inventory_CubeView
AS
  SELECT            it.DataConnectionID
                  , it.CompanyID
                  , it.STATUSISSUE
                  , it.STATUSRECEIPT
                  , it.DEFAULTDIMENSION
                  , it.CURRENCYCODE
                  , it.INVENTTRANSORIGIN
                  , it.VALUEOPEN
                  , it.DATESTATUS
                  , it.DATEEXPECTED
                  , it.AMOUNT                                                                         AS AMOUNT
                  , it.INVENTDIMID                                                                    AS INVENTDIMID
                  , it.INVENTTRANSID                                                                  AS INVENTTRANSID
                  , it.INVENTTRANSPOSTINGTYPE                                                         AS INVENTTRANSPOSTINGTYPE
                  , it.ISPOSTED                                                                       AS ISPOSTED
                  , it.ITEMID                                                                         AS ITEMID
                  , it.LEDGERDIMENSION                                                                AS LEDGERDIMENSION
                  , it.LEDGERDIMENSIONOFFSET                                                          AS LEDGERDIMENSIONOFFSET
                  , it.POSTINGTYPE                                                                    AS POSTINGTYPE
                  , it.POSTINGTYPEOFFSET                                                              AS POSTINGTYPEOFFSET
                  , it.QTY                                                                            AS QTY
                  , it.REFERENCE                                                                      AS REFERENCE
                  , it.REFERENCECATEGORY                                                              AS REFERENCECATEGORY
                  , it.TRANSBEGINTIME                                                                 AS TRANSBEGINTIME
                  , it.TRANSDATE                                                                      AS TRANSDATE
                  , it.VOUCHER                                                                        AS VOUCHER
                  , it.DATAAREAID                                                                     AS DATAAREAID
                  , it.PARTITION                                                                      AS PARTITION
                  , it.RECID                                                                          AS RECID
                  , id.DATAAREAID                                                                     AS DATAAREAID#2
                  , id.PARTITION                                                                      AS PARTITION#2
                  , id.INVENTCOLORID                                                                  AS INVENTCOLORID
                  , id.INVENTSITEID                                                                   AS INVENTSITEID
                  , id.INVENTSIZEID                                                                   AS INVENTSIZEID
                  , id.INVENTSTYLEID                                                                  AS INVENTSTYLEID
                  , id.CONFIGID                                                                       AS CONFIGID
                  , id.WMSLOCATIONID                                                                  AS WMSlocationCode
                  , id.INVENTLOCATIONID                                                               AS LocationCode
                  , id.INVENTBATCHID                                                                  AS TrackingBatchCode
                  , id.INVENTSERIALID                                                                 AS TrackingSerialCode
                  , it.INVOICEID                                                                      AS INVOICEID
                  , L.ACCOUNTINGCURRENCY
                  -- statusissue/statusreceipt 0 then quantity and amount should be 0 
                  , CAST(CASE WHEN it.STATUSISSUE = 0 THEN 0 ELSE -it.QTY END AS NUMERIC(32, 16))     AS ISSUEQTY
                  , CAST(CASE WHEN it.STATUSRECEIPT = 0 THEN 0 ELSE it.QTY END AS NUMERIC(32, 16))    AS RECEIPTQTY
                  , CAST(CASE WHEN it.STATUSISSUE = 0 THEN 0 ELSE -it.AMOUNT END AS NUMERIC(32, 16))  AS ISSUEAMOUNT
                  , CAST(CASE WHEN it.STATUSRECEIPT = 0 THEN 0 ELSE it.AMOUNT END AS NUMERIC(32, 16)) AS RECEIPTAMOUNT
    FROM            help.Inventory_TransView AS it
    LEFT OUTER JOIN stage_ax.LEDGER          AS L ON it.DATAAREAID = L.NAME
                                                 AND it.PARTITION = L.PARTITION
   CROSS JOIN       stage_ax.INVENTDIM       AS id
   WHERE            it.INVENTDIMID = id.INVENTDIMID
     AND            it.DATAAREAID             = id.DATAAREAID
     AND            it.PARTITION              = id.PARTITION
     AND            EXISTS ( SELECT 1
                               FROM stage_ax.INVENTTABLE AS ita
                              WHERE it.ITEMID     = ita.ITEMID
                                AND it.DATAAREAID = ita.DATAAREAID
                                AND it.PARTITION  = ita.PARTITION
                                AND EXISTS ( SELECT 1
                                               FROM stage_ax.ECORESPRODUCT AS erp
                                              WHERE erp.PRODUCTTYPE = 1 --item
                                                AND ita.PRODUCT     = erp.RECID
                                                AND ita.PARTITION   = erp.PARTITION)
                                AND EXISTS ( SELECT 1
                                               FROM stage_ax.INVENTMODELGROUPITEM AS imgi
                                              WHERE imgi.ITEMID         = ita.ITEMID
                                                AND imgi.PARTITION      = ita.PARTITION
                                                AND imgi.ITEMDATAAREAID = ita.DATAAREAID
                                                AND imgi.PARTITION      = ita.PARTITION
                                                AND EXISTS ( SELECT 1
                                                               FROM stage_ax.INVENTMODELGROUP AS img
                                                              WHERE img.STOCKEDPRODUCT        = 1 -- inventory tracks transactions for items and services
                                                                AND imgi.MODELGROUPID         = img.MODELGROUPID
                                                                AND imgi.PARTITION            = img.PARTITION
                                                                AND imgi.MODELGROUPDATAAREAID = img.DATAAREAID
                                                                AND imgi.PARTITION            = img.PARTITION))) ;

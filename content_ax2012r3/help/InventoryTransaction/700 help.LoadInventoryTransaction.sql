EXEC dbo.drop_object @object = N'help.LoadInventoryTransaction' -- nvarchar(128)
                   , @type = N'P'                               -- nchar(2)
                   , @debug = 0 ;                               -- int
GO

CREATE PROCEDURE help.LoadInventoryTransaction
  @component_execution_id INT     = -1
, @load_type              TINYINT = 0
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;


  IF @load_type = 0 -- Full Load
  BEGIN
    SELECT @deleted = COUNT(1) FROM help.InventoryTransaction ;
    EXECUTE dbo.truncate_table 'help.InventoryTransaction' ;
  END ;

  DECLARE @incremental_from_date DATETIME ;
  SET @incremental_from_date = ISNULL((SELECT MAX(INCREMENTALDATE) FROM help.InventoryTransaction), '1900-01-01') ;

  -- INSERT InventoryTransaction
  INSERT INTO help.InventoryTransaction WITH (TABLOCK) (
    DataConnectionID
  , CompanyID
  , ComponentExecutionID
  , [INCREMENTALDATE]
  , [INVENTDIMID]
  , [STATUSISSUE]
  , [STATUSRECEIPT]
  , [PARTITION]
  , [INVENTTRANSID]
  , INVENTCOLORID
  , INVENTSITEID
  , INVENTSIZEID
  , INVENTSTYLEID
  , CONFIGID
  , WMSlocationCode
  , LocationCode
  , TrackingBatchCode
  , TrackingSerialCode
  , INVOICEID
  , [REFERENCECATEGORY]
  , [REFERENCE]
  , [ITEMID]
  , [VOUCHER]
  , [TRANSDATE]
  , [DATESTATUS]
  , [DATEEXPECTED]
  , [VALUEOPEN]
  , [INVENTTRANSPOSTINGTYPE]
  , [INVENTTRANSORIGIN]
  , [ISPOSTED]
  , [POSTINGTYPE]
  , [POSTINGTYPEOFFSET]
  , [LEDGERDIMENSION]
  , [LEDGERDIMENSIONOFFSET]
  , [DEFAULTDIMENSION]
  , [CURRENCYCODE]
  , [QUANTITY]
  , ISSUEQUANTITY
  , RECEIPTQUANTITY
  , [AMOUNT]
  , ISSUEAMOUNT
  , RECEIPTAMOUNT
  , ACCOUNTINGCURRENCY
  )
  SELECT vw.DataConnectionID
       , vw.CompanyID
       , @component_execution_id
       , vw.TRANSBEGINTIME
       , vw.INVENTDIMID
       , vw.STATUSISSUE
       , vw.STATUSRECEIPT
       , vw.PARTITION
       , vw.INVENTTRANSID
       , vw.INVENTCOLORID
       , vw.INVENTSITEID
       , vw.INVENTSIZEID
       , vw.INVENTSTYLEID
       , vw.CONFIGID
       , vw.WMSlocationCode
       , vw.LocationCode
       , vw.TrackingBatchCode
       , vw.TrackingSerialCode
       , vw.INVOICEID
       , vw.REFERENCECATEGORY
       , vw.REFERENCE
       , vw.ITEMID
       , vw.VOUCHER
       , vw.TRANSDATE
       , vw.DATESTATUS
       , vw.DATEEXPECTED
       , vw.VALUEOPEN
       , vw.INVENTTRANSPOSTINGTYPE
       , vw.INVENTTRANSORIGIN
       , vw.ISPOSTED
       , vw.POSTINGTYPE
       , vw.POSTINGTYPEOFFSET
       , vw.LEDGERDIMENSION
       , vw.LEDGERDIMENSIONOFFSET
       , vw.DEFAULTDIMENSION
       , vw.CURRENCYCODE
       , vw.QTY
       , vw.ISSUEQTY
       , vw.RECEIPTQTY
       , vw.AMOUNT
       , vw.ISSUEAMOUNT
       , vw.RECEIPTAMOUNT
       , vw.ACCOUNTINGCURRENCY
    FROM help.Inventory_CubeView AS vw
   WHERE vw.TRANSBEGINTIME > @incremental_from_date ;

  SELECT @inserted = @inserted + @@ROWCOUNT, @updated = 0 ;

END ;


GO

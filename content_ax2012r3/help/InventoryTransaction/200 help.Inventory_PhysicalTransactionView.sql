EXEC dbo.drop_object @object = N'help.Inventory_PhysicalTransactionView', @type = N'V' ;
GO
CREATE VIEW help.Inventory_PhysicalTransactionView
AS

  --Phyiscal Transactions
  SELECT      IT.data_connection_id      AS DataConnectionID
            , IT.company_id              AS CompanyID
            , ITP.DEFAULTDIMENSION
            , IT.CURRENCYCODE
            , IT.INVENTTRANSORIGIN
            , IT.VALUEOPEN
            , IT.INVOICEID
            , IT.DATESTATUS
            , IT.DATEEXPECTED
            , SUM(IT.QTY)                AS QTY
            , SUM(IT.COSTAMOUNTPHYSICAL) AS AMOUNT
            , IT.INVENTDIMID             AS INVENTDIMID
            , IT.STATUSISSUE             AS STATUSISSUE
            , IT.STATUSRECEIPT           AS STATUSRECEIPT
            , IT.DATAAREAID              AS DATAAREAID
            , IT.PARTITION               AS PARTITION
            , 1010                       AS RECID
            , ITO.DATAAREAID             AS DATAAREAID#2
            , ITO.PARTITION              AS PARTITION#2
            , ITO.INVENTTRANSID          AS INVENTTRANSID
            , ITO.REFERENCECATEGORY      AS REFERENCECATEGORY
            , ITO.REFERENCEID            AS REFERENCE
            , ITP.DATAAREAID             AS DATAAREAID#3
            , ITP.PARTITION              AS PARTITION#3
            , ITP.ITEMID                 AS ITEMID
            , ITP.VOUCHER                AS VOUCHER
            , ITP.TRANSDATE              AS TRANSDATE
            , ITP.INVENTTRANSPOSTINGTYPE AS INVENTTRANSPOSTINGTYPE
            , ITP.ISPOSTED               AS ISPOSTED
            , ITP.POSTINGTYPE            AS POSTINGTYPE
            , ITP.POSTINGTYPEOFFSET      AS POSTINGTYPEOFFSET
            , ITP.LEDGERDIMENSION        AS LEDGERDIMENSION
            , ITP.OFFSETLEDGERDIMENSION  AS LEDGERDIMENSIONOFFSET
            , ITP.TRANSBEGINTIME         AS TRANSBEGINTIME
    FROM      stage_ax.INVENTTRANS       AS IT
   CROSS JOIN stage_ax.INVENTTRANSORIGIN AS ITO
   CROSS JOIN stage_ax.INVENTTRANSPOSTING AS ITP
   WHERE      NOT IT.execution_flag = 'D'
     AND      IT.INVENTTRANSORIGIN       = ITO.RECID
     AND      IT.DATAAREAID              = ITO.DATAAREAID
     AND      IT.PARTITION               = ITO.PARTITION
     AND      ITP.INVENTTRANSPOSTINGTYPE = 0 -- only physical inventtranspositing types
     AND      IT.VOUCHERPHYSICAL         = ITP.VOUCHER
     AND      IT.DATAAREAID              = ITP.DATAAREAID
     AND      IT.PARTITION               = ITP.PARTITION
     AND      IT.DATEPHYSICAL            = ITP.TRANSDATE
     AND      IT.DATAAREAID              = ITP.DATAAREAID
     AND      IT.PARTITION               = ITP.PARTITION
     AND      ITO.RECID                  = ITP.INVENTTRANSORIGIN
     AND      ITO.DATAAREAID             = ITP.DATAAREAID
     AND      ITO.PARTITION              = ITP.PARTITION
   GROUP BY IT.INVENTDIMID
          , IT.STATUSISSUE
          , IT.STATUSRECEIPT
          , IT.DATAAREAID
          , IT.PARTITION
          , ITO.DATAAREAID
          , ITO.PARTITION
          , ITO.INVENTTRANSID
          , ITO.REFERENCECATEGORY
          , ITO.REFERENCEID
          , ITP.DATAAREAID
          , ITP.PARTITION
          , ITP.ITEMID
          , ITP.VOUCHER
          , ITP.TRANSDATE
          , ITP.INVENTTRANSPOSTINGTYPE
          , ITP.ISPOSTED
          , ITP.POSTINGTYPE
          , ITP.POSTINGTYPEOFFSET
          , ITP.LEDGERDIMENSION
          , ITP.OFFSETLEDGERDIMENSION
          , ITP.TRANSBEGINTIME
          , IT.data_connection_id
          , IT.company_id
          , ITP.DEFAULTDIMENSION
          , IT.CURRENCYCODE
          , IT.INVENTTRANSORIGIN
          , IT.VALUEOPEN
          , IT.INVOICEID
          , IT.DATESTATUS
          , IT.DATEEXPECTED ;

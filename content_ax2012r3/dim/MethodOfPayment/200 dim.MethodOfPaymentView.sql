EXEC dbo.drop_object 'dim.MethodOfPaymentView', 'V';
GO

CREATE VIEW [dim].[MethodOfPaymentView] AS

SELECT

			  ISNULL(NULLIF(PAYMMODE,''),'N/A')   AS MethodOfPaymentCode
			, ISNULL(NULLIF(NAME,''),'N/A')       AS MethodOfPaymentDesc
			, data_connection_id                  AS DataConnectionID   
			, company_id                          AS CompanyID          
			, Partition                           AS Partition          
			, stage_id                            AS StageID            
FROM
			stage_ax.CUSTPAYMMODETABLE


-- ################################################################################ --
-- #####                         Table: dim.Product                           ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.Product', 'T' ;

CREATE TABLE dim.Product
(
  ProductID            INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, InventSize           NVARCHAR(512) NULL
, InventColor          NVARCHAR(512) NULL
, InventStyle          NVARCHAR(512) NULL
, InventConfiguration  NVARCHAR(512) NULL
) ;
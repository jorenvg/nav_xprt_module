EXEC dbo.drop_object @object = N'dim.LoadDepreciationProfile', @type = N'P', @debug = 0 ;
GO

CREATE PROCEDURE [dim].LoadDepreciationProfile
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.DepreciationProfile
     SET      StageID = DPV.StageID
            , DataConnectionID = DPV.DataConnectionID
            , CompanyID = DPV.CompanyID
            , Partition = DPV.Partition
            , ComponentExecutionID = @component_execution_id
            , DepreciationProfileName = DPV.DepreciationProfileName
            , DepreciationProfileCodeName = DPV.DepreciationProfileCodeName
    FROM      dim.DepreciationProfile     AS DP
   INNER JOIN dim.DepreciationProfileView AS DPV ON DP.DepreciationProfileCode = DPV.DepreciationProfileCode
                                                AND DP.CompanyID               = DPV.CompanyID
                                                AND DP.Partition               = DPV.Partition
                                                AND DP.DataConnectionID        = DPV.DataConnectionID ;

  SELECT @updated = @@RowCount ;

  INSERT INTO dim.DepreciationProfile (
    StageID
  , DataConnectionID
  , CompanyID
  , Partition
  , ComponentExecutionID
  , DepreciationProfileCode
  , DepreciationProfileName
  , DepreciationProfileCodeName
  )
  SELECT DPV.StageID
       , DPV.DataConnectionID
       , DPV.CompanyID
       , DPV.Partition
       , @component_execution_id
       , DPV.DepreciationProfileCode
       , DPV.DepreciationProfileName
       , DPV.DepreciationProfileCodeName
    FROM dim.DepreciationProfileView AS DPV
   WHERE NOT EXISTS ( SELECT 1
                        FROM dim.DepreciationProfile AS DP
                       WHERE DP.DepreciationProfileCode = DPV.DepreciationProfileCode
                         AND DP.CompanyID               = DPV.CompanyID
                         AND DP.Partition               = DPV.Partition
                         AND DP.DataConnectionID        = DPV.DataConnectionID) ;

  SELECT @inserted = @@RowCount ;
END ;

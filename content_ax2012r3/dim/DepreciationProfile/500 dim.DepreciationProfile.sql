EXEC dbo.drop_object 'dim.DepreciationProfile', 'T' ;
GO
CREATE TABLE dim.DepreciationProfile
(
  DepreciationProfileID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY CLUSTERED
, StageID                     BIGINT        NULL
, DataConnectionID            INT           NOT NULL
, CompanyID                   INT           NOT NULL
, Partition                   BIGINT        NOT NULL
, ExecutionTimestamp          ROWVERSION    NOT NULL
, ComponentExecutionID        INT           NOT NULL
-- bk
, DepreciationProfileCode     NVARCHAR(30)  NOT NULL
-- attributes
, DepreciationProfileName     NVARCHAR(100) NOT NULL
, DepreciationProfileCodeName NVARCHAR(255) NOT NULL
) ;

/****************************************************************************************************
  Functionality:  Shows the depreciation profiles from AX based on the design 
  (https://hillstar.sharepoint.com/:w:/s/birds37/EdrRO6H0uWtDjTVDN2Py_yABUIfJV2RWEVeEyVct7Th1pA?e=fzDZgW) 
  used for the dimension Depreciation Profile
  Created by:     AzureAD\JeroenvanLier,     Date:           25/09/2018
    
  Date            Changed by      Ticket/Change       Description
  2018/11/15 	Jeroen van Lier 			DEV-1238 		Added CodeName attribute, removed coalesce from BK
*****************************************************************************************************/
EXEC dbo.drop_object 'dim.DepreciationProfileView', 'V' ;
GO

CREATE VIEW [dim].DepreciationProfileView
AS
  SELECT ADP.stage_id                                                                        AS StageID
       , ADP.company_id                                                                      AS CompanyID
       , ADP.PARTITION                                                                       AS Partition
       , ADP.data_connection_id                                                              AS DataConnectionID
       -- bk
       , ADP.PROFILE                                                                         AS DepreciationProfileCode
       -- attributes
       , COALESCE(NULLIF(ADP.NAME, ''), 'N/A')                                               AS DepreciationProfileName
       , COALESCE(NULLIF(ADP.PROFILE, '') + ' - ', '') + COALESCE(NULLIF(ADP.NAME, ''), 'N/A') AS DepreciationProfileCodeName
    FROM stage_ax.ASSETDEPRECIATIONPROFILE AS ADP ;


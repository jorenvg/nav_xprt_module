/**************************
*** This procedure will deploy the procedure dim.LoadFinancialDimensions based on the financial dimension metadata in the meta.financial_dimensions 
*** The load procedure will import data from the views into the FinancialDimensions table the view inserted/updated are:
*** - dim.FinancialDimensionsDefaultDimensionsView
*** - dim.FinancialDimensionsLedgerDimensionsView 
***************************/

EXEC dbo.drop_object @object = N'dim.DeployLoadFinancialDimensionsProcedure' -- nvarchar(128)
                   , @type = N'P' ;
GO

CREATE PROCEDURE dim.DeployLoadFinancialDimensionsProcedure @debug BIT = 1
AS
BEGIN

  --DECLARE @debug BIT = 0 -- set this flag to run this script in debug mode.

  DECLARE @max_sequence     AS INT
        , @sequence_no      AS INT
        , @current_sequence AS VARCHAR(6)
        , @newline          AS VARCHAR(6)
        , @tab              AS VARCHAR(6) ;

  DECLARE @deploy_query AS NVARCHAR(MAX) = '' ;
  DECLARE @update_query AS NVARCHAR(MAX) = '' ;
  DECLARE @insert_query AS NVARCHAR(MAX) = '' ;
  DECLARE @select_query AS NVARCHAR(MAX) = '' ;
  DECLARE @do_query AS NVARCHAR(MAX) = '' ;

  SELECT @max_sequence = MAX([sequence]) FROM meta.financial_dimensions ;
  SET @newline = CHAR(10) ;
  SET @tab = CHAR(9) ;

  IF EXISTS ( SELECT 1
                    FROM meta.financial_dimensions AS fd
                    JOIN meta.data_connections     AS dc ON dc.data_connection_id = fd.data_connection_id
                    JOIN meta.current_instance     AS ci ON ci.instance_id        = dc.instance_id)
  BEGIN

    SET @update_query += ', ' ;
    SET @insert_query += ', ' ;
    SET @select_query += ', ' ;

    DECLARE DimensionAttributeTable CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR
      SELECT DISTINCT [sequence] FROM meta.financial_dimensions ORDER BY [sequence] ;
    OPEN DimensionAttributeTable ;
    FETCH NEXT FROM DimensionAttributeTable
     INTO @sequence_no ;
    WHILE @@FETCH_STATUS = 0
    BEGIN
      SET @current_sequence = CAST(@sequence_no AS VARCHAR(4)) ;
      SET @update_query += @tab + 'Value' + @current_sequence + ' = a.Value' + @current_sequence + ',' + @newline ;
      SET @update_query += @tab + 'Description' + @current_sequence + ' = a.Description' + @current_sequence + CASE WHEN @sequence_no = @max_sequence THEN '' + @newline ELSE ',' + @newline END ;
      SET @insert_query += @tab + 'Value' + @current_sequence + ',' + @newline ;
      SET @insert_query += @tab + 'Description' + @current_sequence + CASE WHEN @sequence_no = @max_sequence THEN '' + @newline ELSE ',' + @newline END ;
      SET @select_query += @tab + 'a.Value' + @current_sequence + ',' + @newline ;
      SET @select_query += @tab + 'a.Description' + @current_sequence + CASE WHEN @sequence_no = @max_sequence THEN '' + @newline ELSE ',' + @newline END ;
      FETCH NEXT FROM DimensionAttributeTable
       INTO @sequence_no ;
    END ;
    CLOSE DimensionAttributeTable ;
    DEALLOCATE DimensionAttributeTable ;
  END ;

  SET @do_query += '
EXEC dbo.drop_object @object = N''dim.LoadFinancialDimensions'', -- nvarchar(128)
  @type = N''P'', -- nchar(2)
  @debug = 0 -- int;
' ;
  SET @deploy_query += '
CREATE PROCEDURE dim.LoadFinancialDimensions
    @component_execution_id INT = -1
, @load_type TINYINT = 1
, @inserted INT = NULL OUTPUT
, @updated INT = NULL OUTPUT
, @deleted INT = NULL OUTPUT
AS
BEGIN
    SET @deleted = 0
    SET @updated = 0
    SET @inserted = 0

  --instantiate view
  SELECT * INTO #FinancialDimensionsDefaultDimensionsView
  FROM dim.FinancialDimensionsDefaultDimensionsView
  CREATE UNIQUE CLUSTERED INDEX UCI_FinancialDimensionsDefaultDimensionsView ON #FinancialDimensionsDefaultDimensionsView(DefaultDimension, DataConnectionID, Partition);  

    --instantiate view
	SELECT * INTO #FinancialDimensionsLedgerDimensionsView
	FROM dim.FinancialDimensionsLedgerDimensionsView
	CREATE UNIQUE CLUSTERED INDEX UCI_FinancialDimensionsLedgerDimensionsView ON #FinancialDimensionsLedgerDimensionsView(LedgerDimension, DataConnectionID, Partition);  


 	UPDATE dim.FinancialDimensions SET		
		ComponentExecutionId = COALESCE(@component_execution_id,-1)
' ;
  SET @deploy_query += @update_query ;
  SET @deploy_query += '	FROM
		dim.FinancialDimensions b
		INNER JOIN #FinancialDimensionsDefaultDimensionsView a ON a.DefaultDimension = b.DefaultDimension AND a.DataConnectionID = b.DataConnectionID AND a.Partition = b.Partition
	
 	SELECT @updated = @@ROWCOUNT

 	UPDATE dim.FinancialDimensions SET		
		ComponentExecutionId = COALESCE(@component_execution_id,-1)
' ;
  SET @deploy_query += @update_query ;
  SET @deploy_query += '	FROM
		dim.FinancialDimensions b
		INNER JOIN #FinancialDimensionsLedgerDimensionsView a ON a.LedgerDimension = b.LedgerDimension AND a.DataConnectionID = b.DataConnectionID AND a.Partition = b.Partition
	
 	SELECT @updated += @@ROWCOUNT

 	INSERT INTO dim.FinancialDimensions
 	(
		DataConnectionID,
		ComponentExecutionId,
		Partition,
		DefaultDimension
' ;
  SET @deploy_query += @insert_query ;
  SET @deploy_query += ' 	)
 	SELECT
		a.DataConnectionID,
		COALESCE(@component_execution_id,-1),
		a.Partition,
		a.DefaultDimension
' ;
  SET @deploy_query += @select_query ;
  SET @deploy_query += ' 	FROM
		#FinancialDimensionsDefaultDimensionsView a
 	WHERE
		NOT EXISTS(SELECT * FROM dim.FinancialDimensions b WHERE a.DefaultDimension = b.DefaultDimension AND a.DataConnectionID = b.DataConnectionID AND a.Partition = b.Partition)

 	SELECT @inserted = @@ROWCOUNT

 	INSERT INTO dim.FinancialDimensions
 	(
		DataConnectionID,
		ComponentExecutionId,
		Partition,
		LedgerDimension
' ;
  SET @deploy_query += @insert_query ;
  SET @deploy_query += ' 	)
 	SELECT
		a.DataConnectionID,
		COALESCE(@component_execution_id,-1),
		a.Partition,
		a.LedgerDimension
' ;
  SET @deploy_query += @select_query ;
  SET @deploy_query += ' 	FROM
		#FinancialDimensionsLedgerDimensionsView a
 	WHERE
		NOT EXISTS(SELECT * FROM dim.FinancialDimensions b WHERE a.LedgerDimension = b.LedgerDimension AND a.DataConnectionID = b.DataConnectionID AND a.Partition = b.Partition)

 	SELECT @inserted += @@ROWCOUNT

END;' ;

  IF @debug = 1 BEGIN
EXECUTE dbo.long_print @do_query ;
EXECUTE dbo.long_print @deploy_query ;
  END ;
  ELSE BEGIN
EXECUTE sys.sp_executesql @do_query ;
EXECUTE sys.sp_executesql @deploy_query ;
  END ;

END ;
GO

EXEC dim.DeployLoadFinancialDimensionsProcedure @debug = 0 ; -- bit

/**************************
*** This procedure generates views for the default dimension and ledger dimension called
*** - dim.FinancialDimensionsDefaultDimensionsView
*** - dim.FinancialDimensionsLedgerDimensionsView 
*** 
*** The code and description values for each dimension are retrieved from the tables/column defined in the meta.financialdimensions
*** If the table is not available in the source the default NULL value is used
***************************/

EXEC dbo.drop_object @object = N'dim.DeployFinancialDimensionViews' -- nvarchar(128)
                   , @type = N'P' ;
GO

CREATE PROCEDURE dim.DeployFinancialDimensionViews @debug BIT = 0
AS
BEGIN

  --DECLARE @Debug BIT = 1 -- set this flag to run in debug mode.

  DECLARE @TableName NVARCHAR(255) ;
  DECLARE @Name NVARCHAR(255) ;
  DECLARE @value_column NVARCHAR(255) ;
  DECLARE @description_column NVARCHAR(255) ;
  DECLARE @Schema NVARCHAR(20) ;

  DECLARE @deploy_query    NVARCHAR(MAX) = ''
        , @select_query    NVARCHAR(MAX) = ''
        , @join_query      NVARCHAR(MAX) = ''
        , @do_query        NVARCHAR(MAX) = ''
        , @deploy_query_ld NVARCHAR(MAX) = ''
        , @select_query_ld NVARCHAR(MAX) = ''
        , @join_query_ld   NVARCHAR(MAX) = ''
        , @where_query_ld  NVARCHAR(MAX) = '' ;
  DECLARE @Position     INT = 0
        , @is_available BIT = 0 ;
  DECLARE @Pos AS NVARCHAR(6) ;
  DECLARE @newline AS VARCHAR(6) ;

  SET @newline = CHAR(10) ;

  SELECT @Schema = COALESCE(s.stage_schema, meta.get_stage_schema(s.connection_category), 'dbo') + N'.'
    FROM meta.schemas          AS s
    JOIN meta.data_connections AS dc ON dc.data_connection_id = s.data_connection_id
    JOIN meta.current_instance AS ci ON ci.instance_id        = dc.instance_id ;

  --Create the drop statement code.
  SET @do_query += N'EXEC dbo.drop_object ''dim.FinancialDimensionsDefaultDimensionsView'', ''V'';' + @newline ;
  SET @do_query += N'EXEC dbo.drop_object ''dim.FinancialDimensionsLedgerDimensionsView'', ''V'';' + @newline ;

  --Code to start the create view statement.
  SET @deploy_query += N'CREATE VIEW dim.FinancialDimensionsDefaultDimensionsView AS (' + @newline ;

  SET @deploy_query_ld += N'CREATE VIEW dim.FinancialDimensionsLedgerDimensionsView AS (' + @newline ;


  SET @select_query = N'SELECT
-- default dimension columns
	 dd.DataConnectionID
	,dd.Partition
	,dd.DefaultDimension
-- values and description columns for each financial dimension' + @newline ;


  SET @select_query_ld = N'SELECT
-- ledger dimension columns
	 dd.DataConnectionID
	,dd.Partition
	,dd.LedgerDimension
-- values and description columns for each financial dimension' + @newline ;


  --Create the sql code for the query for the list of default dimensions.
  SET @join_query
    = N'FROM
-- query for list of the default dimensions
(
	SELECT DISTINCT f.data_connection_id AS DataConnectionID, f.PARTITION, f.DIMENSIONATTRIBUTEVALUESET as DefaultDimension
	FROM ' + @Schema + N'DIMENSIONATTRIBUTEVALUESETITEM f
	WHERE f.DIMENSIONATTRIBUTEVALUESET IS NOT NULL
	) AS dd' + @newline + @newline ;


  SET @join_query_ld = N'FROM
-- query for list of the default dimensions
(
SELECT DISTINCT f.data_connection_id AS DataConnectionID, f.PARTITION, f.RECID as LedgerDimension
FROM ' + @Schema + N'DIMENSIONATTRIBUTEVALUECOMBINATION f
) AS dd' + @newline + @newline ;

  SET @where_query_ld += N'WHERE 1=0' + @newline ;

  -- Loop over the financial dimensions defined in the meta data table.
  DECLARE DimensionAttributeTable CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR
    SELECT fd.[table]
         , fd.name
         , fd.value
         , fd.description
         , fd.sequence
         , rt.is_available
      FROM meta.financial_dimensions fd
      JOIN meta.replication_tables   AS rt ON fd.[table] = rt.table_definition_table_name
     ORDER BY fd.recid ;
  OPEN DimensionAttributeTable ;
  FETCH NEXT FROM DimensionAttributeTable
   INTO @TableName
      , @Name
      , @value_column
      , @description_column
      , @Position
      , @is_available ;

  WHILE @@FETCH_STATUS = 0
  BEGIN
    SET @Pos = CONVERT(NVARCHAR(6), @Position) ;

    IF @is_available = 1
    BEGIN
      /** DEFAULTDIMENSION VIEW START **/
      -- create code for the joins for the financial dimensions
      SET @join_query += N'-- query for financial dimension ''' + @Name + N'''' + @newline ;
      SET @join_query += N'LEFT OUTER JOIN (' + @newline ;
      SET @join_query += N'	SELECT DISTINCT ' ;
      SET @join_query += N'f.DIMENSIONATTRIBUTEVALUESET as DefaultDimension, c.Name, 
        CAST(a.' + @value_column + N' AS VARCHAR) AS Value, 
        a.' + @description_column + N' AS [Description],
		a.data_connection_id AS DataConnectionID,
        a.Partition
    FROM ' + @Schema + @TableName + N' a 
    INNER JOIN ' + @Schema + N'DIMENSIONATTRIBUTEVALUE b ON b.EntityInstance = a.Recid AND b.data_connection_id = a.data_connection_id AND b.Partition = a.Partition
    INNER JOIN ' + @Schema + N'DIMENSIONATTRIBUTE c ON b.DimensionAttribute = c.Recid AND b.data_connection_id = c.data_connection_id AND b.Partition = c.Partition
    INNER JOIN ' + @Schema + N'DIMENSIONHIERARCHYLEVEL d ON c.RECID = d.DIMENSIONATTRIBUTE AND b.data_connection_id = d.data_connection_id AND b.Partition = d.Partition
    LEFT OUTER JOIN ' + @Schema
                         + N'DIMENSIONATTRIBUTEVALUESETITEM f ON b.RECID = f.DimensionAttributeValue AND b.data_connection_id = f.data_connection_id AND b.Partition = f.Partition
    WHERE
		c.Type <> 2 AND
		f.DIMENSIONATTRIBUTEVALUESET IS NOT NULL
		AND c.NAME = ''' + @Name + N'''' + @newline ;
      SET @join_query += N') AS fd' + @Pos + @newline + N'ON fd' + @Pos + N'.DefaultDimension = dd.DefaultDimension' + @newline + N'AND fd' + @Pos + N'.DataConnectionID = dd.DataConnectionID'
                         + @newline + N' AND fd' + @Pos + N'.Partition = dd.Partition' + @newline + @newline ;

      --create the code for the select columns
      SET @select_query += N'	,fd' + @Pos + N'.Value AS Value' + @Pos + @newline ;
      SET @select_query += N'	,fd' + @Pos + N'.Description AS Description' + @Pos + @newline ;
      /** DEFAULTDIMENSION VIEW END **/

      /** LEDGERDIMENSION VIEW START **/
      -- create code for the joins for the financial dimensions
      SET @join_query_ld += N'-- query for financial dimension ''' + @Name + N'''' + @newline ;
      SET @join_query_ld += N'LEFT OUTER JOIN (' + @newline ;
      SET @join_query_ld += N'
    SELECT DISTINCT ' ;
      SET @join_query_ld += N'DAVC.RECID as LEDGERDIMENSION, c.Name,
        CAST(a.' + @value_column + N' AS VARCHAR) AS Value,
        a.' + @description_column + N' AS [Description],
        a.data_connection_id AS DataConnectionID,
        a.Partition
    FROM ' + @Schema + @TableName + N' a
        INNER JOIN ' + @Schema + N'DIMENSIONATTRIBUTEVALUE b ON b.EntityInstance = a.Recid AND b.data_connection_id = a.data_connection_id AND b.Partition = a.Partition
        INNER JOIN ' + @Schema + N'DIMENSIONATTRIBUTE c ON b.DimensionAttribute = c.Recid AND b.data_connection_id = c.data_connection_id AND b.Partition = c.Partition
        INNER JOIN ' + @Schema
                            + N'DIMENSIONATTRIBUTELEVELVALUE DALV ON b.RECID = DALV.DIMENSIONATTRIBUTEVALUE AND b.data_connection_id = DALV.data_connection_id AND b.Partition = DALV.Partition
        INNER JOIN ' + @Schema
                            + N'DIMENSIONATTRIBUTEVALUEGROUP DAVG ON DALV.DIMENSIONATTRIBUTEVALUEGROUP = DAVG.RECID AND b.data_connection_id = DAVG.data_connection_id AND b.Partition = DAVG.Partition
        INNER JOIN ' + @Schema
                            + N'DIMENSIONATTRIBUTEVALUEGROUPCOMBINATION DAVGC ON DAVG.RECID = DAVGC.DIMENSIONATTRIBUTEVALUEGROUP AND b.data_connection_id = DAVGC.data_connection_id AND b.Partition = DAVGC.Partition
        INNER JOIN ' + @Schema
                            + N'DIMENSIONATTRIBUTEVALUECOMBINATION DAVC ON DAVGC.DIMENSIONATTRIBUTEVALUECOMBINATION = DAVC.RECID AND b.data_connection_id = DAVC.data_connection_id AND b.Partition = DAVC.Partition
        INNER JOIN ' + @Schema
                            + N'DIMENSIONHIERARCHYLEVEL d ON c.RECID = d.DIMENSIONATTRIBUTE AND b.data_connection_id = d.data_connection_id AND b.Partition = d.Partition
    WHERE
        c.Type <> 2
        AND c.NAME = ''' + @Name + N'''' + @newline ;
      SET @join_query_ld += N') AS ld' + @Pos + @newline + N'ON ld' + @Pos + N'.LedgerDimension = dd.LedgerDimension' + @newline + N'AND ld' + @Pos + N'.DataConnectionID = dd.DataConnectionID'
                            + @newline + N' AND ld' + @Pos + N'.Partition = dd.Partition' + @newline + @newline ;

      --create the code for the select columns
      SET @select_query_ld += N',ld' + @Pos + N'.Value AS Value' + @Pos + @newline ;
      SET @select_query_ld += N',ld' + @Pos + N'.Description AS Description' + @Pos + @newline ;

      SET @where_query_ld += N'OR ld' + @Pos + N'.Value IS NOT NULL' + @newline ;
      SET @where_query_ld += N'OR ld' + @Pos + N'.Description IS NOT NULL' + @newline ;
    /** LEDGERDIMENSION VIEW END **/
    END ;
    ELSE
    BEGIN
      --source table for this financial dimension is not available. Use NULL values
      --create the code for the select columns
      SET @select_query += N'	,NULL AS Value' + @Pos + @newline ;
      SET @select_query += N'	,NULL AS Description' + @Pos + @newline ;

      --create the code for the select columns
      SET @select_query_ld += N',NULL AS Value' + @Pos + @newline ;
      SET @select_query_ld += N',NULL AS Description' + @Pos + @newline ;
    END ;

    --Go to next dimension
    FETCH NEXT FROM DimensionAttributeTable
     INTO @TableName
        , @Name
        , @value_column
        , @description_column
        , @Position
        , @is_available ;
  END ;
  CLOSE DimensionAttributeTable ;
  DEALLOCATE DimensionAttributeTable ;

  --Combining T-SQL code
  SET @deploy_query += @newline + @select_query + @newline + @join_query + @newline + N');' ;

  SET @deploy_query_ld += @newline + @select_query_ld + @newline + @join_query_ld + @where_query_ld + N');' ;


  IF @debug = 1
  BEGIN
    EXECUTE dbo.long_print @do_query ;
    PRINT 'GO' ;
    EXECUTE dbo.long_print @deploy_query ;
    PRINT 'GO' ;
    EXECUTE dbo.long_print @deploy_query_ld ;
    PRINT 'GO' ;
  END ;
  ELSE
  BEGIN
    EXECUTE sys.sp_executesql @do_query ;
    EXECUTE sys.sp_executesql @deploy_query ;
    EXECUTE sys.sp_executesql @deploy_query_ld ;
  END ;

END ;
GO

EXEC dim.DeployFinancialDimensionViews @debug = 0 ;
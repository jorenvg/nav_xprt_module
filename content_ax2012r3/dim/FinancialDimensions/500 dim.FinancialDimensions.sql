EXEC dbo.drop_object 'dim.FinancialDimensions', 'T' ;
GO
CREATE TABLE [dim].[FinancialDimensions]
(
  [FinancialDimensionsID] [INT] IDENTITY(1, 1) NOT NULL
, DataConnectionID        INT   NOT NULL
, ExecutionTimestamp      ROWVERSION
, ComponentExecutionID    INT
, Partition               BIGINT
, DefaultDimension        BIGINT
, LedgerDimension         BIGINT
, CONSTRAINT [PK_dim_FinancialDimensions] PRIMARY KEY NONCLUSTERED ([FinancialDimensionsID] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON
                                                                                                     , ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] ;

DECLARE @sqlcode AS VARCHAR(512) ;
DECLARE @sequence INT ;
DECLARE financial_dimension_cursor CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR
  SELECT sequence FROM meta.financial_dimensions ORDER BY sequence ;
OPEN financial_dimension_cursor ;
FETCH NEXT FROM financial_dimension_cursor
 INTO @sequence ;

WHILE @@FETCH_STATUS = 0
BEGIN
  SET @sqlcode
    = 'ALTER TABLE [dim].[FinancialDimensions] ADD ' + 'Value' + CAST(@sequence AS VARCHAR(10)) + ' NVARCHAR(255) NULL, ' + 'Description' + CAST(@sequence AS VARCHAR(10)) + ' NVARCHAR(255) NULL' ;
  EXECUTE (@sqlcode) ;
  FETCH NEXT FROM financial_dimension_cursor
   INTO @sequence ;
END ;
GO
EXEC dbo.drop_object @object = N'dim.LoadTaxAuthority' -- nvarchar(128)
                   , @type = N'P'                      -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadTaxAuthority]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.TaxAuthority
     SET      StageID = TAV.StageID
            , TaxAuthorityDesc = TAV.TaxAuthorityDesc
            , TaxAuthorityCodeDesc = TAV.TaxAuthorityCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.TaxAuthority     TA
   INNER JOIN dim.TaxAuthorityView TAV ON TAV.TaxAuthorityCode = TA.TaxAuthorityCode
                                      AND TAV.DataConnectionID = TA.DataConnectionID
                                      AND TAV.Partition        = TA.Partition
                                      AND TAV.CompanyID        = TA.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.TaxAuthority (
    StageID, DataConnectionID, ComponentExecutionID, CompanyID, Partition, TaxAuthorityCode, TaxAuthorityDesc, TaxAuthorityCodeDesc
  )
  SELECT TAV.StageID
       , TAV.DataConnectionID
       , @component_execution_id
       , TAV.CompanyID
       , TAV.Partition
       , TAV.TaxAuthorityCode
       , TAV.TaxAuthorityDesc
       , TAV.TaxAuthorityCodeDesc
    FROM dim.TaxAuthorityView TAV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.TaxAuthority TA
                       WHERE TAV.TaxAuthorityCode = TA.TaxAuthorityCode
                         AND TAV.DataConnectionID = TA.DataConnectionID
                         AND TAV.Partition        = TA.Partition
                         AND TAV.CompanyID        = TA.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
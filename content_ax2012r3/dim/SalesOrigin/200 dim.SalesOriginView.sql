EXEC dbo.drop_object 'dim.SalesOriginView', 'V' ;
GO

CREATE VIEW [dim].[SalesOriginView]
AS
  SELECT ISNULL(NULLIF(SO.ORIGINID, ''), 'N/A')                                                     AS SalesOriginCode
       , ISNULL(NULLIF(SO.DESCRIPTION, ''), 'N/A')                                                  AS SalesOriginDesc
       , ISNULL(NULLIF(SO.ORIGINID, ''), 'N/A') + ISNULL(' - ' + NULLIF(SO.DESCRIPTION, ''), 'N/A') AS SalesOriginCodeDesc
       , SO.company_id                                                                              AS CompanyID
       , SO.data_connection_id                                                                      AS DataConnectionID
       , SO.Partition                                                                               AS Partition
       , SO.stage_id                                                                                AS StageID
    FROM stage_ax.SALESORIGIN AS SO ;
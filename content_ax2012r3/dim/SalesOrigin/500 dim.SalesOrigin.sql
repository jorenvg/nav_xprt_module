EXEC dbo.drop_object 'dim.SalesOrigin', 'T' ;

CREATE TABLE dim.SalesOrigin
(
  SalesOriginID        INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, SalesOriginCode      NVARCHAR(30)  NOT NULL
, SalesOriginDesc      NVARCHAR(128) NULL
, SalesOriginCodeDesc  NVARCHAR(255) NULL
) ;
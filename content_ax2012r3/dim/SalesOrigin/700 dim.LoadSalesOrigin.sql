EXEC dbo.drop_object @object = N'dim.LoadSalesOrigin' -- nvarchar(128)
                   , @type = N'P'                     -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadSalesOrigin]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesOrigin
     SET      StageID = SOV.StageID
            , SalesOriginDesc = SOV.SalesOriginDesc
            , SalesOriginCodeDesc = SOV.SalesOriginCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.SalesOrigin     SO
   INNER JOIN dim.SalesOriginView SOV ON SOV.SalesOriginCode  = SO.SalesOriginCode
                                     AND SOV.DataConnectionID = SO.DataConnectionID
                                     AND SOV.Partition        = SO.Partition
                                     AND SOV.CompanyID        = SO.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.SalesOrigin (
    StageID, DataConnectionID, ComponentExecutionID, CompanyID, Partition, SalesOriginCode, SalesOriginDesc, SalesOriginCodeDesc
  )
  SELECT SOV.StageID
       , SOV.DataConnectionID
       , @component_execution_id
       , SOV.CompanyID
       , SOV.Partition
       , SOV.SalesOriginCode
       , SOV.SalesOriginDesc
       , SOV.SalesOriginCodeDesc
    FROM dim.SalesOriginView SOV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesOrigin SO
                       WHERE SO.SalesOriginCode  = SOV.SalesOriginCode
                         AND SO.DataConnectionID = SOV.DataConnectionID
                         AND SO.Partition        = SOV.Partition
                         AND SO.CompanyID        = SOV.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
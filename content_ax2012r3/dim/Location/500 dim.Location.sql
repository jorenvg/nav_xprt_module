
-- ################################################################################ --
-- #####                         Table: dim.Location                       ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.Location', 'T' ;

CREATE TABLE dim.Location
(
  LocationID           INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, SiteCode             NVARCHAR(512) NULL
, SiteDesc             NVARCHAR(512) NULL
, SiteCodeDesc         NVARCHAR(512) NULL
, LocationCode         NVARCHAR(512) NULL
, LocationDesc         NVARCHAR(512) NULL
, LocationCodeDesc     NVARCHAR(512) NULL
) ;


EXEC dbo.drop_object @object = N'dim.LoadLocation' -- nvarchar(128)
                   , @type = N'P'                  -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadLocation
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Location
     SET      StageID = LV.StageID
            , SiteDesc = LV.SiteDesc
            , SiteCodeDesc = LV.SiteCodeDesc
            , LocationDesc = LV.LocationDesc
            , LocationCodeDesc = LV.LocationCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Location     L
   INNER JOIN dim.LocationView LV ON LV.SiteCode         = L.SiteCode
                                 AND LV.LocationCode     = L.LocationCode
                                 AND LV.CompanyID        = L.CompanyID
                                 AND LV.Partition        = L.Partition
                                 AND LV.DataConnectionID = L.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Location (
    StageID, DataConnectionID, CompanyID, Partition, ComponentExecutionID, SiteCode, SiteDesc, SiteCodeDesc, LocationCode, LocationDesc, LocationCodeDesc
  )
  SELECT LV.StageID
       , LV.DataConnectionID
       , LV.CompanyID
       , LV.Partition
       , @component_execution_id
       , LV.SiteCode
       , LV.SiteDesc
       , LV.SiteCodeDesc
       , LV.LocationCode
       , LV.LocationDesc
       , LV.LocationCodeDesc
    FROM dim.LocationView LV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Location L
                       WHERE LV.LocationCode     = L.LocationCode
                         AND LV.SiteCode         = L.SiteCode
                         AND LV.CompanyID        = L.CompanyID
                         AND LV.Partition        = L.Partition
                         AND LV.DataConnectionID = L.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
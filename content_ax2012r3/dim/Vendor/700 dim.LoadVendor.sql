EXEC dbo.drop_object @object = N'dim.LoadVendor' -- nvarchar(128)
                   , @type = N'P'                -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadVendor]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Vendor
     SET      StageID = VEV.StageID
            , VendorAccountName = VEV.VendorAccountName
            , VendorAccountNoName = VEV.VendorAccountNoName
            , AddressDesc = VEV.AddressDesc
            , Street = VEV.Street
            , ZipCode = VEV.ZipCode
            , City = VEV.City
            , County = VEV.County
            , StateCode = VEV.StateCode
            , StateDesc = VEV.StateDesc
            , StateCodeDesc = VEV.StateCodeDesc
            , CountryCode = VEV.CountryCode
            , CountryShortDesc = VEV.CountryShortDesc
            , CountryLongDesc = VEV.CountryLongDesc
            , CountryCodeDesc = VEV.CountryCodeDesc
            , VendorGroupCode = VEV.VendorGroupCode
            , VendorGroupDesc = VEV.VendorGroupDesc
            , VendorGroupCodeDesc = VEV.VendorGroupCodeDesc
            , VendorBuyerGroupCode = VEV.VendorBuyerGroupCode
            , VendorBuyerGroupDesc = VEV.VendorBuyerGroupDesc
            , VendorBuyerGroupCodeDesc = VEV.VendorBuyerGroupCodeDesc
            , VendorOnHold = VEV.VendorOnHold
            , VendorRebateGroupCode = VEV.VendorRebateGroupCode
            , VendorRebateGroupDesc = VEV.VendorRebateGroupDesc
            , VendorRebateGroupCodeDesc = VEV.VendorRebateGroupCodeDesc
            , VendorSegmentCode = VEV.VendorSegmentCode
            , VendorSegmentDesc = VEV.VendorSegmentDesc
            , VendorSegmentCodeDesc = VEV.VendorSegmentCodeDesc
            , VendorSubSegmentCode = VEV.VendorSubSegmentCode
            , VendorSubSegmentDesc = VEV.VendorSubSegmentDesc
            , VendorSubSegmentCodeDesc = VEV.VendorSubSegmentCodeDesc
            , CashDiscountCode = VEV.CashDiscountCode
            , CashDiscountDesc = VEV.CashDiscountDesc
            , CashDiscountCodeDesc = VEV.CashDiscountCodeDesc
            , VendorPriceGroupCode = VEV.VendorPriceGroupCode
            , VendorPriceGroupDesc = VEV.VendorPriceGroupDesc
            , VendorPriceGroupCodeDesc = VEV.VendorPriceGroupCodeDesc
            , InterCompanyVendor = VEV.InterCompanyVendor
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Vendor     VE
   INNER JOIN dim.VendorView VEV ON VEV.VendorAccountNo  = VE.VendorAccountNo
                                AND VEV.DataConnectionID = VE.DataConnectionID
                                AND VEV.Partition        = VE.Partition
                                AND VEV.CompanyID        = VE.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Vendor (
    StageID
  , DataConnectionID
  , ComponentExecutionID
  , CompanyID
  , Partition
  , VendorAccountNo
  , VendorAccountName
  , VendorAccountNoName
  , AddressDesc
  , Street
  , ZipCode
  , City
  , County
  , StateCode
  , StateDesc
  , StateCodeDesc
  , CountryCode
  , CountryShortDesc
  , CountryLongDesc
  , CountryCodeDesc
  , VendorGroupCode
  , VendorGroupDesc
  , VendorGroupCodeDesc
  , VendorBuyerGroupCode
  , VendorBuyerGroupDesc
  , VendorBuyerGroupCodeDesc
  , VendorOnHold
  , VendorRebateGroupCode
  , VendorRebateGroupDesc
  , VendorRebateGroupCodeDEsc
  , VendorSegmentCode
  , VendorSegmentDesc
  , VendorSegmentCodeDesc
  , VendorSubSegmentCode
  , VendorSubSegmentDesc
  , VendorSubSegmentCodeDesc
  , CashDiscountCode
  , CashDiscountDesc
  , CashDiscountCodeDesc
  , VendorPriceGroupCode
  , VendorPriceGroupDesc
  , VendorPriceGroupCodeDesc
  , InterCompanyVendor
  )
  SELECT VEV.StageID
       , VEV.DataConnectionID
       , @component_execution_id
       , VEV.CompanyID
       , VEV.Partition
       , VEV.VendorAccountNo
       , VEV.VendorAccountName
       , VEV.VendorAccountNoName
       , VEV.AddressDesc
       , VEV.Street
       , VEV.ZipCode
       , VEV.City
       , VEV.County
       , VEV.StateCode
       , VEV.StateDesc
       , VEV.StateCodeDesc
       , VEV.CountryCode
       , VEV.CountryShortDesc
       , VEV.CountryLongDesc
       , VEV.CountryCodeDesc
       , VEV.VendorGroupCode
       , VEV.VendorGroupDesc
       , VEV.VendorGroupCodeDesc
       , VEV.VendorBuyerGroupCode
       , VEV.VendorBuyerGroupDesc
       , VEV.VendorBuyerGroupCodeDesc
       , VEV.VendorOnHold
       , VEV.VendorRebateGroupCode
       , VEV.VendorRebateGroupDesc
       , VEV.VendorRebateGroupCodeDesc
       , VEV.VendorSegmentCode
       , VEV.VendorSegmentDesc
       , VEV.VendorSegmentCodeDesc
       , VEV.VendorSubSegmentCode
       , VEV.VendorSubSegmentDesc
       , VEV.VendorSubSegmentCodeDesc
       , VEV.CashDiscountCode
       , VEV.CashDiscountDesc
       , VEV.CashDiscountCodeDesc
       , VEV.VendorPriceGroupCode
       , VEV.VendorPriceGroupDesc
       , VEV.VendorPriceGroupCodeDesc
       , VEV.InterCompanyVendor
    FROM dim.VendorView VEV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Vendor VE
                       WHERE VEV.VendorAccountNo  = VE.VendorAccountNo
                         AND VEV.DataConnectionID = VE.DataConnectionID
                         AND VEV.Partition        = VE.Partition
                         AND VEV.CompanyID        = VE.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;

END ;
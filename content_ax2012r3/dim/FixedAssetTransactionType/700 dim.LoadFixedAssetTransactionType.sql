EXEC dbo.drop_object @object = N'dim.LoadFixedAssetTransactionType', @type = N'P', @debug = 0 ;
GO

CREATE PROCEDURE [dim].LoadFixedAssetTransactionType
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.FixedAssetTransactionType
     SET      ComponentExecutionID = @component_execution_id
            , FixedAssetTransactionTypeDescription = FAT.FixedAssetTransactionTypeDescription
    FROM      dim.FixedAssetTransactionType     AS FAT
   INNER JOIN dim.FixedAssetTransactionTypeView AS FATV ON FAT.FixedAssetTransactionTypeCode = FATV.FixedAssetTransactionTypeCode
                                                       AND FAT.DataConnectionID              = FATV.DataConnectionID ;

  SELECT @updated = @@RowCount ;

  INSERT INTO dim.FixedAssetTransactionType (
    ComponentExecutionID
  , DataConnectionID
  , FixedAssetTransactionTypeCode
  , FixedAssetTransactionTypeDescription
  )
  SELECT @component_execution_id
       , FATV.DataConnectionID
       , FATV.FixedAssetTransactionTypeCode
       , FATV.FixedAssetTransactionTypeDescription
    FROM dim.FixedAssetTransactionTypeView AS FATV
   WHERE NOT EXISTS ( SELECT 1
                        FROM dim.FixedAssetTransactionType AS FAT
                       WHERE FAT.FixedAssetTransactionTypeCode = FATV.FixedAssetTransactionTypeCode
                         AND FAT.DataConnectionID              = FATV.DataConnectionID) ;

  SELECT @inserted = @@RowCount ;
END ;

EXEC dbo.drop_object 'dim.FixedAssetTransactionType ', 'T' ;
GO
CREATE TABLE dim.FixedAssetTransactionType
(
  FixedAssetTransactionTypeID          INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY CLUSTERED
, DataConnectionID                     INT           NOT NULL
, ExecutionTimestamp                   ROWVERSION    NOT NULL
, ComponentExecutionID                 INT           NOT NULL
, FixedAssetTransactionTypeCode        INT           NULL
, FixedAssetTransactionTypeDescription NVARCHAR(128) NULL
) ;


EXEC dbo.drop_object 'dim.FixedAssetTransactionTypeView', 'V' ;
GO

CREATE VIEW [dim].[FixedAssetTransactionTypeView]
AS
  SELECT ADT_ENUM.DataConnectionID             AS DataConnectionID
       , ADT_ENUM.OptionID                     AS FixedAssetTransactionTypeCode
       , COALESCE(ADT_ENUM.Description, 'N/A') AS FixedAssetTransactionTypeDescription
    FROM meta.enumerations AS ADT_ENUM
   WHERE ADT_ENUM.TableName  = 'AssetTrans'
     AND ADT_ENUM.ColumnName = 'TransType' ;

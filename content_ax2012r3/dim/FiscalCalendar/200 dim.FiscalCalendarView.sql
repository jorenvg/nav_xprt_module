EXEC dbo.drop_object 'dim.FiscalCalendarView', 'V' ;
GO
CREATE VIEW dim.FiscalCalendarView
AS
  SELECT      fc.CALENDARID          AS Name
            , fc.DESCRIPTION         AS [Desc]
            , fcy.NAME               AS FiscalYear
            , fcp.NAME               AS FiscalPeriodName
            , fcp.QUARTER            AS FiscalQuarterCode
            , efcpq.Description      AS FiscalQuarterDesc --Enum
            , fcp.MONTH              AS FiscalMonthCode
            , efcpm.Description      AS FiscalMonthDesc   --Enum
            , fcp.TYPE               AS FiscalPeriodTypeCode
            , efcp.Description       AS FiscalPeriodTypeDesc
            , fcp.STARTDATE          AS FiscalPeriodStart
            , fcp.ENDDATE            AS FiscalPeriodEnd
            , fcp.PARTITION
            , fc.RECID               AS FiscalCalendarRecID
            , fc.RECVERSION          AS FiscalCalendarRecVersion
            , fcy.RECID              AS FiscalCalendarYearRecID
            , fcy.RECVERSION         AS FiscalCalendarYearRecVersion
            , fcp.RECID              AS FiscalCalendarPeriodRecID
            , fcp.RECVERSION         AS FiscalCalendarPeriodRecVersion
            , fcp.data_connection_id AS DataConnectionID
            , fcp.stage_id           AS StageID
    FROM      stage_ax.FISCALCALENDARPERIOD fcp
    LEFT JOIN stage_ax.FISCALCALENDAR       fc ON fc.RECID                  = fcp.FISCALCALENDAR
                                              AND fc.PARTITION              = fcp.PARTITION
                                              AND fc.data_connection_id     = fcp.data_connection_id
    LEFT JOIN stage_ax.FISCALCALENDARYEAR   fcy ON fcp.FISCALCALENDAR       = fcy.FISCALCALENDAR
                                               AND fcp.PARTITION            = fcy.PARTITION
                                               AND fcp.data_connection_id   = fcy.data_connection_id
                                               AND fcp.FISCALCALENDARYEAR   = fcy.RECID
    LEFT JOIN meta.enumerations             efcp ON efcp.TableName          = 'FiscalCalendarPeriod'
                                                AND efcp.ColumnName         = 'Type'
                                                AND efcp.OptionID           = fcp.TYPE
                                                AND efcp.DataConnectionID   = fcp.data_connection_id
    LEFT JOIN meta.enumerations             efcpm ON efcpm.TableName        = 'FiscalCalendarPeriod'
                                                 AND efcpm.ColumnName       = 'Month'
                                                 AND efcpm.OptionID         = fcp.MONTH
                                                 AND efcp.DataConnectionID  = fcp.data_connection_id
    LEFT JOIN meta.enumerations             efcpq ON efcpm.TableName        = 'FiscalCalendarPeriod'
                                                 AND efcpq.ColumnName       = 'Quarter'
                                                 AND efcpq.OptionID         = fcp.QUARTER
                                                 AND efcpq.DataConnectionID = fcp.data_connection_id ;
GO
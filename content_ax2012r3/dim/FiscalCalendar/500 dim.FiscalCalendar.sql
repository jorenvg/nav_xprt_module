-- ################################################################################ --
-- #####                         Table: dim.FiscalCalendar                    ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.FiscalCalendar', 'T' ;
GO
CREATE TABLE dim.FiscalCalendar
(
  FiscalCalendarID                 INT             IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                          BIGINT          NULL
, DataConnectionID                 INT             NOT NULL
, ExecutionTimestamp               ROWVERSION      NOT NULL
, ComponentExecutionID             INT             NOT NULL
, [Name]                           [NVARCHAR](30)  NULL
, [Desc]                           [NVARCHAR](100) NULL
, [FiscalYear]                     [NVARCHAR](30)  NULL
, [FiscalPeriodName]               [NVARCHAR](100) NULL
, [FiscalQuarterCode]              [INT]           NULL
, [FiscalQuarterDesc]              [NVARCHAR](255) NULL
, [FiscalMonthCode]                [INT]           NULL
, [FiscalMonthDesc]                [NVARCHAR](255) NULL
, [FiscalPeriodTypeCode]           [INT]           NULL
, [FiscalPeriodTypeDesc]           [NVARCHAR](255) NULL
, [FiscalPeriodStart]              [DATETIME]      NULL
, [FiscalPeriodEnd]                [DATETIME]      NULL
, [Partition]                      [BIGINT]        NULL
, [FiscalCalendarRecID]            [BIGINT]        NULL
, [FiscalCalendarRecVersion]       [INT]           NULL
, [FiscalCalendarYearRecID]        [BIGINT]        NULL
, [FiscalCalendarYearRecVersion]   [INT]           NULL
, [FiscalCalendarPeriodRecID]      [BIGINT]        NULL
, [FiscalCalendarPeriodRecVersion] [INT]           NULL
) ;
GO
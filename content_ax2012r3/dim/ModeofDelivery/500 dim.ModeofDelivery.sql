
-- ################################################################################ --
-- #####                         Table: dim.ModeOfDelivery                    ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.ModeOfDelivery', 'T' ;

CREATE TABLE dim.ModeOfDelivery
(
  ModeOfDeliveryID           INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                    BIGINT        NULL
, DataConnectionID           INT           NOT NULL
, CompanyID                  INT           NOT NULL
, Partition                  BIGINT        NOT NULL
, ExecutionTimestamp         ROWVERSION    NOT NULL
, ComponentExecutionID       INT           NOT NULL
, ModeOfDeliveryCode         NVARCHAR(512) NOT NULL
, ModeOfDeliveryDesc         NVARCHAR(512) NOT NULL
, ModeOfDeliveryCodeDesc     NVARCHAR(512) NULL
, ShipCarrierDLVTypeCode     INT           NOT NULL
, ShipCarrierDLVTypeDesc     NVARCHAR(512) NOT NULL
, ShipCarrierDLVTypeCodeDesc NVARCHAR(512) NULL
) ;
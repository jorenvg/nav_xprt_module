EXEC dbo.drop_object 'dim.ModeOfDeliveryView', 'V' ;
GO

CREATE VIEW [dim].[ModeOfDeliveryView]
AS
  SELECT            ISNULL(NULLIF(DM.CODE, ''), 'N/A')                                                               AS ModeOfDeliveryCode
                  , ISNULL(NULLIF(DM.TXT, ''), 'N/A')                                                                AS ModeOfDeliveryDesc
                  , ISNULL(NULLIF(CONVERT(VARCHAR(255), DM.Code), '') + ' - ', '') + DM.TXT                          AS ModeOfDeliveryCodeDesc
                  , ISNULL(NULLIF(DM.SHIPCARRIERDLVTYPE, -1), -1)                                                    AS ShipCarrierDLVTypeCode
                  , ISNULL(NULLIF(ENUM.DESCRIPTION, ''), 'N/A')                                                      AS ShipCarrierDLVTypeDesc
                  , ISNULL(NULLIF(CONVERT(VARCHAR(255), DM.SHIPCARRIERDLVTYPE), '') + ' - ', ' ') + ENUM.DESCRIPTION AS ShipCarrierDLVTypeCodeDesc
                  , DM.company_id                                                                                    AS CompanyID
                  , DM.Partition                                                                                     AS Partition
                  , DM.data_connection_id                                                                            AS DataConnectionID
                  , DM.stage_id                                                                                      AS StageID
    FROM            stage_ax.DLVMODE  DM
    LEFT OUTER JOIN meta.enumerations ENUM ON DM.SHIPCARRIERDLVTYPE = ENUM.OptionID
                                          AND ENUM.TableName        = 'DLVMODE'
                                          AND ENUM.ColumnName       = 'SHIPCARRIERDLVTYPE' ;


EXEC dbo.drop_object @object = N'dim.LoadModeOfDelivery' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadModeOfDelivery
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.ModeOfDelivery
     SET      StageID = MDV.StageID
            , ModeOfDeliveryDesc = MDV.ModeOfDeliveryDesc
            , ModeOfDeliveryCodeDesc = MDV.ModeOfDeliveryCodeDesc
            , ShipCarrierDLVTypeDesc = MDV.ShipCarrierDLVTypeDesc
            , ShipCarrierDLVTypeCodeDesc = MDV.ShipCarrierDLVTypeCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.ModeOfDelivery     MD
   INNER JOIN dim.ModeOfDeliveryView MDV ON MDV.ModeOfDeliveryCode     = MD.ModeOfDeliveryCode
                                        AND MDV.ShipCarrierDLVTypeCode = MD.ShipCarrierDLVTypeCode
                                        AND MDV.CompanyID              = MD.CompanyID
                                        AND MDV.Partition              = MD.Partition
                                        AND MDV.DataConnectionID       = MD.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.ModeOfDelivery (
    StageID
  , DataConnectionID
  , CompanyID
  , Partition
  , ComponentExecutionID
  , ModeOfDeliveryCode
  , ModeOfDeliveryDesc
  , ModeOfDeliveryCodeDesc
  , ShipCarrierDLVTypeCode
  , ShipCarrierDLVTypeDesc
  , ShipCarrierDLVTypeCodeDesc
  )
  SELECT MDV.StageID
       , MDV.DataConnectionID
       , MDV.CompanyID
       , MDV.Partition
       , @component_execution_id
       , MDV.ModeOfDeliveryCode
       , MDV.ModeOfDeliveryDesc
       , MDV.ModeOfDeliveryCodeDesc
       , MDV.ShipCarrierDLVTypeCode
       , MDV.ShipCarrierDLVTypeDesc
       , MDV.ShipCarrierDLVTypeCodeDesc
    FROM dim.ModeOfDeliveryView MDV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.ModeOfDelivery MD
                       WHERE MDV.ModeOfDeliveryCode = MD.ModeOfDeliveryCode
                         AND MDV.CompanyID          = MD.CompanyID
                         AND MDV.Partition          = MD.Partition
                         AND MDV.DataConnectionID   = MD.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
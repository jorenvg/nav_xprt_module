EXEC dbo.drop_object 'dim.YesNo', 'T' ;

CREATE TABLE dim.YesNo
(
  YesNoID     INT         IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, YesNoCode   INT         NOT NULL
, YesNoDesc   NVARCHAR(3) NOT NULL
, YesNoBit    BIT         NOT NULL
, YesNoSortBy INT         NOT NULL
) ;

EXECUTE dbo.drop_object 'dim.Date', 'T' ;
GO

CREATE TABLE dim.Date
(
  [DateID]               [INT]           NOT NULL PRIMARY KEY NONCLUSTERED
, ExecutionTimestamp     ROWVERSION
, ComponentExecutionId   INT
, [Date]                 [DATETIME]      NOT NULL
, [FullDescription]      [NVARCHAR](255) NOT NULL
, [DescYMD]              [NVARCHAR](255) NOT NULL
, [DescDMY]              [NVARCHAR](255) NOT NULL
, [DescMDY]              [NVARCHAR](255) NOT NULL
, [DescYYYYMMDD]         [NVARCHAR](255) NOT NULL
, [DescDDMMYYYY]         [NVARCHAR](255) NOT NULL
, [DescMMDDYYYY]         [NVARCHAR](255) NOT NULL
, [CYearNo]              [INT]           NOT NULL
, [CIsoYearNo]           [INT]           NOT NULL
, [CQuarterNo]           [INT]           NOT NULL
, [CMonthNo]             [INT]           NOT NULL
, [CWeekNo]              [INT]           NOT NULL
, [CYearMonthNo]         [INT]           NOT NULL
, [CYearQuarterNo]       [INT]           NOT NULL
, [CDayInMonthNo]        [INT]           NOT NULL
, [CDayWeekNo]           [INT]           NOT NULL
, [CDayYearNo]           [INT]           NOT NULL
, [CYearWeekNo]          [INT]           NOT NULL
, [CHalfYearNo]          [INT]           NOT NULL
, [CYearDesc]            [NVARCHAR](255) NOT NULL
, [CIsoYearDesc]         [NVARCHAR](255) NOT NULL
, [CMonthDesc]           [NVARCHAR](255) NOT NULL
, [CQuarterDesc]         [NVARCHAR](255) NOT NULL
, [CWeekDesc]            [NVARCHAR](255) NOT NULL
, [CYearMonthDesc]       [NVARCHAR](255) NOT NULL
, [CYearQuarterDesc]     [NVARCHAR](255) NOT NULL
, [CMonthYearDesc]       [NVARCHAR](255) NOT NULL
, [CQuarterYearDesc]     [NVARCHAR](255) NOT NULL
, [CYearWeekDesc]        [NVARCHAR](255) NOT NULL
, [CWeekYearDesc]        [NVARCHAR](255) NOT NULL
, [CDayDesc]             [NVARCHAR](255) NOT NULL
, [CHalfYearDesc]        [NVARCHAR](255) NOT NULL
, [CFirstDayMonthDate]   [DATETIME]      NOT NULL
, [CLastDayMonthDate]    [DATETIME]      NOT NULL
, [CNumberOfDaysInMonth] [INT]           NOT NULL
, [CEndMonth]            [INT]           NOT NULL
, [CStartMonth]          [INT]           NOT NULL
, [Past]                 [INT]           NOT NULL
, [DateNo]               [INT]           NOT NULL
, [CMonthNoDesc]         [VARCHAR](10)   NOT NULL
, [CDayWeekDesc]         [NVARCHAR](255) NOT NULL
, [CDayInMonthDesc]      [NVARCHAR](100) NOT NULL
, [TDatePY]              DATETIME        NOT NULL
, [TYearMonthNo]         INT             NOT NULL
, [TMonthDays]           INT             NOT NULL,
--[FCYearNo] [int] NULL,
--[FCPeriodNo] [int] NULL,
--[FCYearDesc] [nvarchar](255) NULL,
--[FCPeriodDesc] [nvarchar](255) NULL,
--[FCYearPeriodDesc] [nvarchar](255) NULL,
--[FCPeriodYearDesc] [nvarchar](255) NULL,
--[FCFirstDayPeriod] [datetime] NULL,
--[FCLastDayPeriod] [datetime] NULL,
--[FCFirstDayYear] [datetime] NULL,
--[FCLastDayYear] [datetime] NULL,
--[FCStartPeriod] [int] NULL,
--[FCEndPeriod] [int] NULL,
--[FCClosed] [int] NULL,
--[FCWeek] [nvarchar](50) NULL,
--[FCQuarter] [nvarchar](50) NULL,
--[FCMonth] [nvarchar](50) NULL,
--[FCYearQuarter] [nvarchar](50) NULL,
--[FCYearMonth] [nvarchar](50) NULL,
--[FCYearWeek] [nvarchar](50) NULL,
--[FCYearMonthNo] [nvarchar](50) NULL,
) ON [PRIMARY] ;


-- ################################################################################ --
-- #####                         Table: dim.InventoryPostingType              ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.InventoryPostingType', 'T' ;
GO
CREATE TABLE [dim].[InventoryPostingType]
(
  InventoryPostingTypeID   INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID         INT           NOT NULL
, ExecutionTimestamp       ROWVERSION    NOT NULL
, ComponentExecutionID     INT           NOT NULL
, InventoryPostingTypeCode INT           NOT NULL
, InventoryPostingTypeDesc NVARCHAR(255) NULL
) ;
GO
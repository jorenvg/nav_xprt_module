EXEC dbo.drop_object 'dim.InventoryPostingTypeView', 'V' ;
GO
CREATE VIEW [dim].[InventoryPostingTypeView]
AS
  SELECT ELT.OptionID    AS InventoryPostingTypeCode
       , ELT.Description AS InventoryPostingTypeDesc
       , ELT.DataConnectionID
    FROM meta.Enumerations AS ELT
   WHERE ELT.TableName  = 'InventTransPosting'
     AND ELT.ColumnName = 'InventTransPostingType' ;
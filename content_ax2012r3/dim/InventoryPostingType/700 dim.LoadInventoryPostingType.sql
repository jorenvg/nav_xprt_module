EXEC dbo.drop_object @object = N'dim.LoadInventoryPostingType' -- nvarchar(128)
                   , @type = N'P'                              -- nchar(2)
                   , @debug = 0 ;                              -- int
GO

CREATE PROCEDURE [dim].[LoadInventoryPostingType]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.InventoryPostingType
     SET      InventoryPostingTypeDesc = PTV.InventoryPostingTypeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.InventoryPostingType     PT
   INNER JOIN dim.InventoryPostingTypeView PTV ON PTV.InventoryPostingTypeCode = PT.InventoryPostingTypeCode
                                              AND PTV.DataConnectionID         = PT.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.InventoryPostingType (
    DataConnectionID, ComponentExecutionID, InventoryPostingTypeCode, InventoryPostingTypeDesc
  )
  SELECT PTV.DataConnectionID
       , COALESCE(@component_execution_id, -1)
       , PTV.InventoryPostingTypeCode
       , PTV.InventoryPostingTypeDesc
    FROM dim.InventoryPostingTypeView PTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.InventoryPostingType PT
                       WHERE PTV.InventoryPostingTypeCode = PT.InventoryPostingTypeCode
                         AND PTV.DataConnectionID         = PT.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
GO




-- ################################################################################ --
-- #####                         Table: dim.VoucherText                       ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.VoucherText', 'T'

CREATE TABLE dim.VoucherText
(
	VoucherTextID             INT IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
,	DataConnectionID          INT NOT NULL
,	CompanyID                 INT NOT NULL
,	Partition                 BIGINT NOT NULL
,	ExecutionTimestamp        ROWVERSION NOT NULL
,	ComponentExecutionID      INT NOT NULL
,	VoucherCode               BIGINT NOT NULL
, VoucherVersion            INT NOT NULL
, VoucherText               NVARCHAR(255)
)	
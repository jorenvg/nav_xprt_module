EXEC dbo.drop_object @object = N'dim.LoadTimeIntelligence' -- nvarchar(128)
                   , @type = N'P'                          -- nchar(2)
                   , @debug = 0 ;                          -- int
GO

CREATE PROCEDURE dim.LoadTimeIntelligence
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  DELETE FROM dim.TimeIntelligence WHERE TimeIntelligenceId = 0 ;

  UPDATE dim.TimeIntelligence
     SET TimeIntelligenceDesc = vw.[Desc]
       , TimeIntelligenceSortBy = vw.SortBy
    FROM dim.TimeIntelligence     AS tbl
    JOIN dim.TimeIntelligenceView vw ON tbl.TimeIntelligenceCode = vw.Code ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT dim.TimeIntelligence (
    TimeIntelligenceCode, TimeIntelligenceDesc, TimeIntelligenceSortBy
  )
  SELECT Code
       , [Desc]
       , SortBy
    FROM dim.TimeIntelligenceView vw
   WHERE NOT EXISTS (SELECT 1 FROM dim.TimeIntelligence AS tbl WHERE tbl.TimeIntelligenceCode = vw.Code) ;

  SELECT @inserted = @@ROWCOUNT ;

END ;

GO



-- ################################################################################ --
-- #####                         Table: dim.CostGroup                     ##### --
-- ################################################################################ --
EXEC Dbo.Drop_Object 'dim.CostGroup ', 'T' ;

CREATE TABLE dim.CostGroup
(
  CostGroupID              INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                  BIGINT        NULL
, DataConnectionID         INT           NOT NULL
, CompanyID                INT           NOT NULL
, Partition                BIGINT        NOT NULL
, ExecutionTimestamp       ROWVERSION    NOT NULL
, ComponentExecutionID     INT           NOT NULL
, CostGroupCode            NVARCHAR(100) NULL
, CostGroupType            INT           NULL
, CostGroupTypeDescription NVARCHAR(200) NULL
, CostGroupName            NVARCHAR(100) NULL
) ;


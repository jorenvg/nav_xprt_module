EXEC Dbo.Drop_Object 'dim.CostGroupView', 'V' ;
GO

CREATE VIEW [dim].[CostGroupView]
AS
  SELECT            BCG.stage_id                                      AS StageID
                  , BCG.company_id                                    AS CompanyID
                  , BCG.PARTITION                                     AS Partition
                  , BCG.data_connection_id                            AS DataConnectionID
                  , COALESCE(NULLIF(BCG.CostGroupId, ''), 'N/A')      AS CostGroupCode
                  , COALESCE(BCG.CostGroupType, -1)                   AS CostGroupType
                  , COALESCE(NULLIF(BCG_ENUM.Description, ''), 'N/A') AS CostGroupTypeDescription
                  , COALESCE(NULLIF(BCG.Name, ''), 'N/A')             AS CostGroupName
    FROM            Stage_Ax.BomCostGroup AS BCG
    LEFT OUTER JOIN Meta.Enumerations     AS BCG_ENUM ON BCG_ENUM.TableName  = 'BOMCostGroup'
                                                     AND BCG_ENUM.ColumnName = 'CostGroupType'
                                                     AND BCG.CostGroupType   = BCG_ENUM.OptionId ;

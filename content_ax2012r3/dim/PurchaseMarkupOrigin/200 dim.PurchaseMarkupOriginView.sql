EXEC dbo.drop_object @object = N'dim.PurchaseMarkupOriginView', @type = N'V' ;
GO
CREATE VIEW dim.PurchaseMarkupOriginView
AS
  SELECT TABLEID                 AS OriginTableCode  --Bkey
       , SQLNAME                 AS OriginTable      --Bkey
       , CASE
           WHEN SQLD.SQLNAME IN ( 'PurchaseLine', 'PurchaseTable' ) THEN 'PurchaseLine'
           WHEN SQLD.SQLNAME IN ( 'VendPackingSlipTrans', 'VendPackingSlipJour' ) THEN 'PurcasePacking'
           WHEN SQLD.SQLNAME IN ( 'VendInvoiceTrans', 'VendInvoiceJour' ) THEN 'PurchaseInvoice'
         END                     AS OriginMeasureGroup
       , SQLD.data_connection_id AS DataConnectionID --Bkey
       , SQLD.stage_id           AS StageID
    FROM stage_ax.SQLDICTIONARY AS SQLD
   WHERE SQLD.SQLNAME IN ( 'PurchaseLine', 'PurchaseTable', 'VendPackingSlipTrans', 'VendPackingSlipJour', 'VendInvoiceTrans', 'VendInvoiceJour' )
     AND SQLD.FIELDID = 0 ;


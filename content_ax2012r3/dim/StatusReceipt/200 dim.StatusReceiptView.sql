EXEC dbo.drop_object 'dim.StatusReceiptView', 'V' ;
GO

CREATE VIEW [dim].[StatusReceiptView]
AS
  SELECT ISNULL(OptionID, -1)                     AS StatusReceiptCode
       , ISNULL(NULLIF([Description], ''), 'N/A') AS StatusReceiptDesc
       , DataConnectionID
    FROM meta.enumerations
   WHERE TableName  = 'InventTrans'
     AND ColumnName = 'StatusReceipt' ;

EXEC dbo.drop_object 'dim.PurchaseInvoiceView', 'V' ;
GO

CREATE VIEW [dim].[PurchaseInvoiceView]
AS
  SELECT -- BK of the dimension is not unique in the table VENDINVOICEJOUR
                    ISNULL(NULLIF(VIJ.INVOICEID, ''), 'N/A')                                             AS PurchaseInvoice
                  , VIJ.company_id                                                                       AS CompanyID
                  , VIJ.data_connection_id                                                               AS DataConnectionID
                  , VIJ.PARTITION                                                                        AS Partition
                  , MIN(VT.TRANSDATE)                                                                    AS InvoiceDate
                  , ISNULL(NULLIF(MIN(VT.CLOSED), '1900-01-01 00:00:00.000'), '9999-01-01 00:00:00.000') AS ClosedDate
                  , ISNULL(NULLIF(MIN(VT.DUEDATE), '1900-01-01 00:00:00.000'), MIN(VT.TRANSDATE))        AS DueDate
    FROM            stage_ax.VENDINVOICEJOUR AS VIJ
    LEFT OUTER JOIN stage_ax.VENDTRANS       AS VT ON VIJ.INVOICEID          = VT.INVOICE
                                                  AND VIJ.company_id         = VT.company_id
                                                  AND VIJ.data_connection_id = VT.data_connection_id
                                                  AND VIJ.PARTITION          = VT.PARTITION
   WHERE            VIJ.INVOICEID IS NOT NULL
   GROUP BY VIJ.INVOICEID
          , VIJ.company_id
          , VIJ.data_connection_id
          , VIJ.PARTITION ;

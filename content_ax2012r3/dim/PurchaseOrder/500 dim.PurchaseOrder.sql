
-- ################################################################################ --
-- #####                         Table: dim.PurchaseOrder                     ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.PurchaseOrder', 'T' ;

CREATE TABLE dim.PurchaseOrder
(
  PurchaseOrderID             INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                     BIGINT        NULL
, DataConnectionID            INT           NOT NULL
, CompanyID                   INT           NOT NULL
, Partition                   BIGINT        NOT NULL
, ExecutionTimestamp          ROWVERSION    NOT NULL
, ComponentExecutionID        INT           NOT NULL
, PurchaseOrder               NVARCHAR(512) NOT NULL
, PurchaseTypeCode            INT           NULL
, PurchaseTypeDescription     NVARCHAR(256) NULL
, PurchaseName                NVARCHAR(100) NULL
, PurchasePoolCode            NVARCHAR(20)  NULL
, PurchasePoolDescription     NVARCHAR(120) NULL
, PurchasePoolCodeDescription NVARCHAR(256) NULL
, PurchaseStatusCode          INT
, PurchaseStatusDescription   NVARCHAR(256) NULL
) ;




EXEC dbo.drop_object 'dim.PurchaseOrderView', 'V' ;
GO

CREATE VIEW [dim].[PurchaseOrderView]
AS
  SELECT            PT.stage_id                                                                             AS StageID
                  , PT.company_id                                                                           AS CompanyID
                  , PT.data_connection_id                                                                   AS DataConnectionID
                  , PT.Partition                                                                            AS Partition
                  , COALESCE(NULLIF(PT.PURCHID, ''), 'N/A')                                                 AS PurchaseOrder
                  , COALESCE(PT_ENUM.OptionID, -1)                                                          AS PurchaseTypeCode
                  , COALESCE(NULLIF(PT_ENUM.Description, ''), 'N/A')                                        AS PurchaseTypeDescription
                  , COALESCE(NULLIF(PT.PurchName, ''), 'N/A')                                               AS PurchaseName
                  , COALESCE(NULLIF(PP.PURCHPOOLID, ''), 'N/A')                                             AS PurchasePoolCode
                  , COALESCE(NULLIF(PP.NAME, ''), 'N/A')                                                    AS PurchasePoolDescription
                  , COALESCE(NULLIF(PP.PURCHPOOLID, '') + ' - ', '') + COALESCE(NULLIF(PP.NAME, ''), 'N/A') AS PurchasePoolCodeDescription
                  , COALESCE(PS_ENUM.OptionId, -1)                                                          AS PurchaseStatusCode
                  , COALESCE(NULLIF(PS_ENUM.DESCRIPTION, ''), 'N/A')                                        AS PurchaseStatusDescription
    FROM            stage_ax.PURCHTABLE AS PT
    LEFT OUTER JOIN stage_ax.PURCHPOOL  AS PP ON PT.PurchPoolId             = PP.PurchPoolId
                                             AND PT.Data_Connection_Id      = PP.Data_Connection_Id
                                             AND PT.Company_Id              = PP.Company_Id
                                             AND PT.Partition               = PP.Partition
    LEFT OUTER JOIN meta.enumerations   AS PT_ENUM ON PT_ENUM.TableName     = 'PURCHTABLE'
                                                  AND PT_ENUM.ColumnName    = 'PURCHASETYPE'
                                                  AND PT.PurchaseType       = PT_ENUM.OptionId
                                                  AND PT.Data_Connection_Id = PT_ENUM.DataConnectionId
    LEFT OUTER JOIN meta.Enumerations   AS PS_ENUM ON PS_ENUM.TableName     = 'PurchTable'
                                                  AND PS_ENUM.ColumnName    = 'PurchStatus'
                                                  AND PT.PurchStatus        = PS_ENUM.OptionId
                                                  AND PT.Data_Connection_Id = PS_ENUM.DataConnectionId ;



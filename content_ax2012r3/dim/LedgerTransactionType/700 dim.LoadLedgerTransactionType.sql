EXEC dbo.drop_object @object = N'dim.LoadLedgerTransactionType' -- nvarchar(128)
                   , @type = N'P'                               -- nchar(2)
                   , @debug = 0 ;                               -- int
GO

CREATE PROCEDURE [dim].[LoadLedgerTransactionType]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.LedgerTransactionType
     SET      LedgerTransactionTypeDesc = a.LedgerTransactionTypeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.LedgerTransactionType     b
   INNER JOIN dim.LedgerTransactionTypeView a ON a.LedgerTransactionTypeCode = b.LedgerTransactionTypeCode
                                             AND a.DataConnectionID          = b.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.LedgerTransactionType (
    DataConnectionID, ComponentExecutionID, LedgerTransactionTypeCode, LedgerTransactionTypeDesc
  )
  SELECT a.DataConnectionID
       , COALESCE(@component_execution_id, -1)
       , a.LedgerTransactionTypeCode
       , a.LedgerTransactionTypeDesc
    FROM dim.LedgerTransactionTypeView a
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.LedgerTransactionType b
                       WHERE a.LedgerTransactionTypeCode = b.LedgerTransactionTypeCode
                         AND a.DataConnectionID          = b.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
GO



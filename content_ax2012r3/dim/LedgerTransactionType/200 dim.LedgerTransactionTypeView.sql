EXEC dbo.drop_object 'dim.LedgerTransactionTypeView', 'V' ;
GO
CREATE VIEW [dim].[LedgerTransactionTypeView]
AS
  SELECT ELT.OptionID    AS LedgerTransactionTypeCode
       , ELT.Description AS LedgerTransactionTypeDesc
       , ELT.DataConnectionID
    FROM meta.Enumerations AS ELT
   WHERE ELT.TableName  = 'LedgerTrans'
     AND ELT.ColumnName = 'TransType' ; --[GENERALJOURNALENTRY].JOURNALCATEGORY
EXEC Dbo.Drop_Object 'dim.ProductionCalculationTypeView', 'V' ;
GO

CREATE VIEW [dim].[ProductionCalculationTypeView]
AS
  SELECT COALESCE(me.OptionId, -1)                   AS CalculationTypeCode
       , COALESCE(NULLIF(me.Description, ''), 'N/A') AS CalculationType
       , me.DataConnectionId                         AS DataconnectionID
    FROM Meta.Enumerations AS me
   WHERE columnname = 'calctype'
     AND tablename  = 'prodcalctrans' ;


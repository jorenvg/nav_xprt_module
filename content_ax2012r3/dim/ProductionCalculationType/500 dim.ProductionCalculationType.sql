
-- ################################################################################ --
-- #####                         Table: dim.ProductionCalculationType                     ##### --
-- ################################################################################ --
EXEC Dbo.Drop_Object 'dim.ProductionCalculationType', 'T' ;

CREATE TABLE dim.ProductionCalculationType
(
  ProductionCalculationTypeID INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, ExecutionTimestamp          ROWVERSION    NOT NULL
, ComponentExecutionID        INT           NOT NULL
, CalculationTypeCode         INT           NULL
, CalculationType             NVARCHAR(512) NULL
, DataconnectionID            INT           NULL
) ;


EXEC dbo.drop_object 'dim.CurrencyView', 'V' ;
GO
CREATE VIEW [dim].[CurrencyView]
AS
  SELECT C.CURRENCYCODE                                         AS Code
       , ISNULL(NULLIF(C.TXT, ''), 'N/A')                       AS Description
       , C.CURRENCYCODE + ISNULL(' - ' + NULLIF(C.TXT, ''), '') AS CodeDesc
       , C.Partition                                            AS Partition
       , C.data_connection_id                                   AS DataConnectionID
       , C.stage_id                                             AS StageID
    FROM stage_ax.CURRENCY      C
    JOIN ( SELECT CURRENCYCODE
                , PARTITION
                , MAX(RecID) AS RecID
             FROM stage_ax.CURRENCY
            GROUP BY CURRENCYCODE
                   , PARTITION) CM ON C.CurrencyCode = CM.CurrencyCode
                                  AND C.PARTITION    = CM.PARTITION
                                  AND C.RecID        = CM.RecID ;
GO




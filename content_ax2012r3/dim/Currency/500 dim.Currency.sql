-- ################################################################################ --
-- #####                         Table: dim.Currency                          ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.Currency', 'T' ;
GO
CREATE TABLE dim.Currency
(
  CurrencyID           INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, Partition            BIGINT        NULL
, Code                 NVARCHAR(255) NOT NULL
, Description          NVARCHAR(512) NOT NULL
, CodeDesc             NVARCHAR(512)
, NameNoDesc           NVARCHAR(512)
) ;
GO
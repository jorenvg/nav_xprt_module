EXEC dbo.drop_object @object = N'dim.LoadCurrency' -- nvarchar(128)
                   , @type = N'P'                  -- nchar(2)
                   , @debug = 0 ;                  -- int
GO

CREATE PROCEDURE [dim].[LoadCurrency]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Currency
     SET      StageID = CUV.StageID
            , Description = CUV.Description
            , CodeDesc = CUV.CodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Currency     CU
   INNER JOIN dim.CurrencyView CUV ON CUV.Code             = CU.Code
                                  AND CUV.DataConnectionID = CU.DataConnectionID
                                  AND CUV.Partition        = CU.Partition ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Currency (
    StageID, DataConnectionID, ComponentExecutionID, Partition, Code, Description, CodeDesc
  )
  SELECT CUV.StageID
       , CUV.DataConnectionID
       , COALESCE(@component_execution_id, -1)
       , CUV.Partition
       , CUV.Code
       , CUV.Description
       , CUV.CodeDesc
    FROM dim.CurrencyView CUV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Currency CU
                       WHERE CUV.Code             = CU.Code
                         AND CUV.DataConnectionID = CU.DataConnectionID
                         AND CUV.Partition        = CU.Partition) ;

  SELECT @inserted = @@ROWCOUNT ;


-- This is currently not implemented because there is no currency registered for each company. This might be required to implement at a later time.

-- If local currency code does not exist in Currency table
-- INSERT INTO dim.Currency
-- (
-- 	DataSourceID,
-- 	CompanyID,
-- 	Code,
-- 	Description,
-- 	NoNameDesc,
-- 	NameNoDesc
-- )
-- SELECT
-- 	CUV.DataSourceID,
-- 	CUV.CompanyID,
-- 	CUV.LocalCurrency,
-- 	CUV.LocalCurrency,
-- 	CUV.LocalCurrency,
-- 	CUV.LocalCurrency
-- FROM
-- 	setup.Company a
-- WHERE
-- 	NOT EXISTS (SELECT * FROM dim.Currency b WHERE CUV.LocalCurrency = CU.Code AND CUV.CompanyID = CU.CompanyID AND CUV.DataSourceID = CU.DataSourceID)

END ;
GO
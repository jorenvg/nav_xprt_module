
-- ################################################################################ --
-- #####                         Table: dim.BudgetModel                       ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.BudgetModel', 'T'
GO
CREATE TABLE dim.BudgetModel
(
			  BudgetModelID         INT IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
			, StageID               BIGINT NULL
			, DataConnectionID      INT NOT NULL
			, CompanyID             INT NOT NULL
			, ExecutionTimestamp    ROWVERSION NOT NULL
			, ComponentExecutionID  INT NOT NULL
			, Partition             BIGINT NOT NULL
			, BudgetModelCode       NVARCHAR(512) NULL
			, BudgetModelDesc       NVARCHAR(512) NULL
			, BudgetModelTypeCode   INT NULL
			, BudgetModelTypeDesc   NVARCHAR(512) NULL
			, BudgetSubmodelCode    NVARCHAR(512) NULL
)
GO
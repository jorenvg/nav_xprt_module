
EXEC dbo.drop_object 'dim.BudgetModelView', 'V';
GO
CREATE VIEW [dim].[BudgetModelView] AS
--BudgetModel
SELECT
  ISNULL(NULLIF(BM.ModelID,''),'N/A')             AS BudgetModelCode    
, ISNULL(NULLIF(BM.txt,''),'N/A')                 AS BudgetModelDesc    
, ISNULL(NULLIF(BM.Type,''),-1)                   AS BudgetModelTypeCode
, ISNULL(NULLIF(EBM.Description,''),'N/A')        AS BudgetModelTypeDesc
, ISNULL(NULLIF(BM.SUBMODELID,''),'N/A')          AS BudgetSubmodelCode 
, BM.company_id                                   AS CompanyID          
, BM.data_connection_id                           AS DataConnectionID   
, BM.PARTITION                                    AS Partition          
, BM.stage_id                                     AS StageID            
  
  FROM
  stage_ax.BUDGETMODEL AS BM
  LEFT JOIN meta.enumerations EBM ON EBM.TableName = 'BudgetModel' 
                                AND EBM.ColumnName = 'Type' 
                                AND EBM.OptionID = BM.Type

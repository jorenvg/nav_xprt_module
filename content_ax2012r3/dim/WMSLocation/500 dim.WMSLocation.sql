
-- ################################################################################ --
-- #####                         Table: dim.WMSLocation                       ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.WMSLocation', 'T' ;

CREATE TABLE dim.WMSLocation
(
  WMSLocationID        INT           IDENTITY(1, 1) NOT NULL
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, WMSLocationCode      NVARCHAR(50)  NOT NULL
, WMSLocationDesc      NVARCHAR(100) NULL
, WMSLocationCodeDesc  NVARCHAR(255) NULL
, LocationCode         NVARCHAR(50)  NULL
, WMSAisle             NVARCHAR(50)  NULL
, WMSAisleNo           NVARCHAR(50)  NULL
) ;
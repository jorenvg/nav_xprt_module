EXEC dbo.drop_object 'dim.SalesQuotationView', 'V' ;
GO

CREATE VIEW [dim].[SalesQuotationView]
AS
  SELECT            SQT.stage_id                                                                              AS StageID
                  , SQT.company_id                                                                            AS CompanyID
                  , SQT.PARTITION                                                                             AS Partition
                  , SQT.data_connection_id                                                                    AS DataConnectionId
                  -- bk
                  , COALESCE(NULLIF(SQT.QUOTATIONID, ''), 'N/A')                                              AS SalesQuotationCode
                  -- attributes
                  , COALESCE(ST_ENUM.OptionID, -1)                                                            AS SalesQuotationTypeCode
                  , COALESCE(NULLIF(ST_ENUM.Description, ''), 'N/A')                                          AS SalesQuotationTypeDescription
                  , COALESCE(NULLIF(SQT.SALESPOOLID, ''), 'N/A')                                              AS SalesPoolCode
                  , COALESCE(NULLIF(SP.NAME, ''), 'N/A')                                                      AS SalesPoolDescription
                  , COALESCE(NULLIF(SQT.SALESPOOLID, ''), 'N/A') + ISNULL(' - ' + NULLIF(SP.NAME, ''), 'N/A') AS SalesPoolCodeDescription
                  , COALESCE(SS_ENUM.OptionID, -1)                                                            AS SalesQuotationStatusCode
                  , COALESCE(NULLIF(SS_ENUM.Description, ''), 'N/A')                                          AS SalesQuotationStatusDescription
    -- select COUNT(*)
    FROM            stage_ax.SALESQUOTATIONTABLE AS SQT
    LEFT OUTER JOIN stage_ax.SALESPOOL           AS SP ON SQT.SALESPOOLID             = SP.SALESPOOLID
                                                      AND SQT.data_connection_id      = SP.data_connection_id
                                                      AND SQT.company_id              = SP.company_id
    LEFT OUTER JOIN meta.enumerations            AS ST_ENUM ON ST_ENUM.TableName      = 'SalesQuotationTable'
                                                           AND ST_ENUM.ColumnName     = 'QuotationType'
                                                           AND SQT.QUOTATIONTYPE      = ST_ENUM.OptionID
                                                           AND SQT.data_connection_id = ST_ENUM.DataConnectionID
    LEFT OUTER JOIN meta.enumerations            AS SS_ENUM ON SS_ENUM.TableName      = 'SalesQuotationTable'
                                                           AND SS_ENUM.ColumnName     = 'QuotationStatus'
                                                           AND SQT.QUOTATIONSTATUS    = SS_ENUM.OptionID
                                                           AND SQT.data_connection_id = SS_ENUM.DataConnectionID ;
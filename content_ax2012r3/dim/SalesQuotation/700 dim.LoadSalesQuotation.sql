EXEC dbo.drop_object @object = N'dim.LoadSalesQuotation' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadSalesQuotation
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesQuotation
     SET      StageID = SQV.StageID
            , ComponentExecutionID = @component_execution_id
            , SalesQuotationTypeCode = SQV.SalesQuotationTypeCode
            , SalesQuotationTypeDescription = SQV.SalesQuotationTypeDescription
            , SalesPoolCode = SQV.SalesPoolCode
            , SalesPoolDescription = SQV.SalesPoolDescription
            , SalesPoolCodeDescription = SQV.SalesPoolCodeDescription
            , SalesQuotationStatusCode = SQV.SalesQuotationStatusCode
            , SalesQuotationStatusDescription = SQV.SalesQuotationStatusDescription
    FROM      dim.SalesQuotation     AS SQ
   INNER JOIN dim.SalesQuotationView AS SQV ON SQV.SalesQuotationCode = SQ.SalesQuotationCode
                                           AND SQV.CompanyID          = SQ.CompanyID
                                           AND SQV.Partition          = SQ.Partition
                                           AND SQV.DataConnectionId   = SQ.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.SalesQuotation (
    StageID
  , CompanyID
  , DataConnectionID
  , Partition
  , ComponentExecutionID
  , SalesQuotationCode
  , SalesQuotationTypeCode
  , SalesQuotationTypeDescription
  , SalesPoolCode
  , SalesPoolDescription
  , SalesPoolCodeDescription
  , SalesQuotationStatusCode
  , SalesQuotationStatusDescription
  )
  SELECT SQV.StageID
       , SQV.CompanyID
       , SQV.DataConnectionId
       , SQV.Partition
       , @component_execution_id
       , SQV.SalesQuotationCode
       , SQV.SalesQuotationTypeCode
       , SQV.SalesQuotationTypeDescription
       , SQV.SalesPoolCode
       , SQV.SalesPoolDescription
       , SQV.SalesPoolCodeDescription
       , SQV.SalesQuotationStatusCode
       , SQV.SalesQuotationStatusDescription
    FROM dim.SalesQuotationView AS SQV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesQuotation AS SQ
                       WHERE SQV.SalesQuotationCode = SQ.SalesQuotationCode
                         AND SQV.CompanyID          = SQ.CompanyID
                         AND SQV.Partition          = SQ.Partition
                         AND SQV.DataConnectionId   = SQ.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;

EXEC dbo.drop_object 'dim.Tax', 'T' ;

CREATE TABLE dim.Tax
(
  TaxID                INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, TaxCode              NVARCHAR(30)  NOT NULL
, TaxDesc              NVARCHAR(60)  NOT NULL
, TaxCodeDesc          NVARCHAR(100) NOT NULL
, TaxGroupCode         NVARCHAR(10)  NOT NULL
, TaxGroupDesc         NVARCHAR(100) NOT NULL
, TaxGroupCodeDesc     NVARCHAR(128) NOT NULL
, TaxItemGroupCode     NVARCHAR(10)  NOT NULL
, TaxItemGroupDesc     NVARCHAR(100) NOT NULL
, TaxItemGroupCodeDesc NVARCHAR(128) NOT NULL
) ;
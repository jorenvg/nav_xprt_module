EXEC dbo.drop_object @object = N'dim.LoadTax' -- nvarchar(128)
                   , @type = N'P'             -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadTax]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Tax
     SET      TaxDesc = STV.TaxDesc
            , TaxCodeDesc = STV.TaxCodeDesc
            , TaxGroupCode = STV.TaxGroupCode
            , TaxGroupDesc = STV.TaxGroupDesc
            , TaxGroupCodeDesc = STV.TaxGroupCodeDesc
            , TaxItemGroupCode = STV.TaxItemGroupCode
            , TaxItemGroupDesc = STV.TaxItemGroupDesc
            , TaxItemGroupCodeDesc = STV.TaxItemGroupCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Tax     ST
   INNER JOIN dim.TaxView STV ON STV.TaxCode          = ST.TaxCode
                             AND STV.TaxGroupCode     = ST.TaxGroupCode
                             AND STV.TaxItemGroupCode = ST.TaxItemGroupCode
                             AND STV.DataConnectionID = ST.DataConnectionID
                             AND STV.Partition        = ST.Partition
                             AND STV.CompanyID        = ST.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Tax (
    DataConnectionID, ComponentExecutionID, CompanyID, Partition, TaxCode, TaxDesc, TaxCodeDesc, TaxGroupCode, TaxGroupDesc, TaxGroupCodeDesc, TaxItemGroupCode, TaxItemGroupDesc, TaxItemGroupCodeDesc
  )
  SELECT STV.DataConnectionID
       , @component_execution_id
       , STV.CompanyID
       , STV.Partition
       , STV.TaxCode
       , STV.TaxDesc
       , STV.TaxCodeDesc
       , STV.TaxGroupCode
       , STV.TaxGroupDesc
       , STV.TaxGroupCodeDesc
       , STV.TaxItemGroupCode
       , STV.TaxItemGroupDesc
       , STV.TaxItemGroupCodeDesc
    FROM dim.TaxView STV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Tax ST
                       WHERE STV.TaxCode          = ST.TaxCode
                         AND STV.TaxGroupCode     = ST.TaxGroupCode
                         AND STV.TaxItemGroupCode = ST.TaxItemGroupCode
                         AND STV.DataConnectionID = ST.DataConnectionID
                         AND STV.Partition        = ST.Partition
                         AND STV.CompanyID        = ST.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
EXEC dbo.drop_object 'dim.StatusIssue', 'T' ;

CREATE TABLE dim.StatusIssue
(
  StatusIssueID        INT        IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT        NOT NULL
, ExecutionTimestamp   ROWVERSION NOT NULL
, ComponentExecutionID INT        NOT NULL
, StatusIssueCode      INT        NOT NULL
, StatusIssueDesc      NVARCHAR(128)
) ;
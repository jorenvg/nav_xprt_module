EXEC dbo.drop_object 'dim.StatusIssueView', 'V' ;
GO

CREATE VIEW [dim].[StatusIssueView]
AS
  SELECT ISNULL(OptionID, -1)                     AS StatusIssueCode
       , ISNULL(NULLIF([Description], ''), 'N/A') AS StatusIssueDesc
       , DataConnectionID
    FROM meta.enumerations
   WHERE TableName  = 'InventTrans'
     AND ColumnName = 'StatusIssue' ;

EXEC dbo.drop_object @object = N'dim.LoadPurchasePool' -- nvarchar(128)
                   , @type = N'P'                      -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadPurchasePool
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.PurchasePool
     SET      StageID = PPV.StageID
            , PurchasePoolDesc = PPV.PurchasePoolDesc
            , PurchasePoolCodeDesc = PPV.PurchasePoolCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.PurchasePool     PP
   INNER JOIN dim.PurchasePoolView PPV ON PPV.PurchasePoolCode = PP.PurchasePoolCode
                                      AND PPV.CompanyID        = PP.CompanyID
                                      AND PPV.Partition        = PP.Partition
                                      AND PPV.DataConnectionID = PP.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.PurchasePool (
    StageID, DataConnectionID, CompanyID, Partition, ComponentExecutionID, PurchasePoolCode, PurchasePoolDesc, PurchasePoolCodeDesc
  )
  SELECT PPV.StageID
       , PPV.DataConnectionID
       , PPV.CompanyID
       , PPV.Partition
       , @component_execution_id
       , PPV.PurchasePoolCode
       , PPV.PurchasePoolDesc
       , PPV.PurchasePoolCodeDesc
    FROM dim.PurchasePoolView PPV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.PurchasePool PP
                       WHERE PPV.PurchasePoolCode = PP.PurchasePoolCode
                         AND PPV.CompanyID        = PP.CompanyID
                         AND PPV.Partition        = PP.Partition
                         AND PPV.DataConnectionID = PP.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
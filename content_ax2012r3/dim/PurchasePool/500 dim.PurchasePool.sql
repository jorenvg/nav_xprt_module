
-- ################################################################################ --
-- #####                         Table: dim.PurchasePool                      ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.PurchasePool', 'T' ;

CREATE TABLE dim.PurchasePool
(
  PurchasePoolID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, PurchasePoolCode     NVARCHAR(50)  NOT NULL
, PurchasePoolDesc     NVARCHAR(255) NOT NULL
, PurchasePoolCodeDesc NVARCHAR(512) NULL
) ;
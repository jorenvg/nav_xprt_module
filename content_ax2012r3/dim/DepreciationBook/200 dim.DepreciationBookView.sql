/****************************************************************************************************
    Functionality:  Shows the depreciation Book from AX based on the design 
    (https://hillstar.sharepoint.com/:w:/s/birds37/EdrRO6H0uWtDjTVDN2Py_yABUIfJV2RWEVeEyVct7Th1pA?e=fzDZgW) 
    used for the dimension Depreciation Book
    Created by:     AzureAD\JeroenvanLier,     Date:           25/09/2018
    
    Date            Changed by        Ticket/Change       Description
    2018/11/15      Jeroen van Lier   dev-1238           Updated dimension to adhere to coding standards, improved formatting, and removed the RemainingPeriods attribute
*****************************************************************************************************/
EXEC dbo.drop_object 'dim.DepreciationBookView', 'V' ;
GO

CREATE VIEW [dim].DepreciationBookView
AS
  SELECT            ADB.stage_id                                                                                            AS StageID
                  , ADB.company_id                                                                                          AS CompanyID
                  , ADB.PARTITION                                                                                           AS Partition
                  , ADB.data_connection_id                                                                                  AS DataConnectionID
                  -- BK
                  , ADB.ASSETID                                                                                             AS FixedAssetCode
                  , ADBT.DEPRECIATIONBOOKID                                                                                 AS DepreciationBookCode
                  -- Attributes
                  , COALESCE(NULLIF(ADBT.DESCRIPTION, ''), 'N/A')                                                           AS DepreciationBookDescription
                  , COALESCE(NULLIF(ADBT.DEPRECIATIONBOOKID, '') + ' - ', '') + ISNULL(NULLIF(ADBT.DESCRIPTION, ''), 'N/A') AS DepreciationBookCodeDescription
                  , COALESCE(ADB.ASSETSTATUS, -1)                                                                           AS AssetStatusCode
                  , COALESCE(DB_ENUM.Description, 'N/A')                                                                    AS AssetStatusDescription
                  , COALESCE(ADB.DEPRECIATIONCONVENTION, -1)                                                                AS DepreciationConventionCode
                  , COALESCE(DBC_ENUM.Description, 'N/A')                                                                   AS DepreciationConventionDescription
                  , COALESCE(ADB.SERVICELIFE, 0)                                                                            AS ServiceLife
                  , COALESCE(ADB.LIFETIME, 0)                                                                               AS DepreciationPeriods
    FROM            stage_ax.ASSETDEPBOOK      AS ADB
    LEFT OUTER JOIN stage_ax.ASSETDEPBOOKTABLE AS ADBT ON ADB.DEPRECIATIONBOOKID         = ADBT.DEPRECIATIONBOOKID
                                                      AND ADB.company_id                 = ADBT.company_id
                                                      AND ADB.PARTITION                  = ADBT.PARTITION
                                                      AND ADB.data_connection_id         = ADBT.data_connection_id
    LEFT OUTER JOIN meta.enumerations          AS DB_ENUM ON DB_ENUM.TableName           = 'AssetBookMerge'
                                                         AND DB_ENUM.ColumnName          = 'Status'
                                                         AND ADB.ASSETSTATUS             = DB_ENUM.OptionID
                                                         AND ADB.data_connection_id      = DB_ENUM.DataConnectionID
    LEFT OUTER JOIN meta.enumerations          AS DBC_ENUM ON DBC_ENUM.TableName         = 'AssetDepreciationConvention'
                                                          AND DBC_ENUM.ColumnName        = 'FrequencyOfDepreciation'
                                                          AND ADB.DEPRECIATIONCONVENTION = DBC_ENUM.OptionID
                                                          AND ADB.data_connection_id     = DBC_ENUM.DataConnectionID ;

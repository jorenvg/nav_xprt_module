EXEC dbo.drop_object @object = N'dim.LoadDepreciationBook', @type = N'P', @debug = 0 ;
GO

CREATE PROCEDURE [dim].LoadDepreciationBook
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.DepreciationBook
     SET      StageID = DBV.StageID
            , ComponentExecutionID = @component_execution_id
            , DepreciationBookDescription = DBV.DepreciationBookDescription
            , DepreciationBookCodeDescription = DBV.DepreciationBookCodeDescription
            , AssetStatusCode = DBV.AssetStatusCode
            , AssetStatusDescription = DBV.AssetStatusDescription
            , DepreciationConventionCode = DBV.DepreciationConventionCode
            , DepreciationConventionDescription = DBV.DepreciationConventionDescription
            , ServiceLife = DBV.ServiceLife
            , DepreciationPeriods = DBV.DepreciationPeriods
    FROM      dim.DepreciationBook     AS DB
   INNER JOIN dim.DepreciationBookView AS DBV ON DB.FixedAssetCode       = DBV.FixedAssetCode
                                             AND DB.DepreciationBookCode = DBV.DepreciationBookCode
                                             AND DB.CompanyID            = DBV.CompanyID
                                             AND DB.Partition            = DBV.Partition
                                             AND DB.DataConnectionID     = DBV.DataConnectionID ;

  SELECT @updated = @@RowCount ;
  INSERT INTO dim.DepreciationBook (
    StageID
  , DataConnectionID
  , CompanyID
  , Partition
  , ComponentExecutionID
  , FixedAssetCode
  , DepreciationBookCode
  , DepreciationBookDescription
  , DepreciationBookCodeDescription
  , AssetStatusCode
  , AssetStatusDescription
  , DepreciationConventionCode
  , DepreciationConventionDescription
  , ServiceLife
  , DepreciationPeriods
  )
  SELECT DBV.StageID
       , DBV.DataConnectionID
       , DBV.CompanyID
       , DBV.Partition
       , @component_execution_id
       -- bk
       , DBV.FixedAssetCode
       , DBV.DepreciationBookCode
       -- attibutes
       , DBV.DepreciationBookDescription
       , DBV.DepreciationBookCodeDescription
       , DBV.AssetStatusCode
       , DBV.AssetStatusDescription
       , DBV.DepreciationConventionCode
       , DBV.DepreciationConventionDescription
       , DBV.ServiceLife
       , DBV.DepreciationPeriods
    FROM dim.DepreciationBookView AS DBV
   WHERE NOT EXISTS ( SELECT 1
                        FROM dim.DepreciationBook AS DB
                       WHERE DB.FixedAssetCode       = DBV.FixedAssetCode
                         AND DB.DepreciationBookCode = DBV.DepreciationBookCode
                         AND DB.CompanyID            = DBV.CompanyID
                         AND DB.Partition            = DBV.Partition
                         AND DB.DataConnectionID     = DBV.DataConnectionID) ;

  SELECT @inserted = @@RowCount ;
END ;

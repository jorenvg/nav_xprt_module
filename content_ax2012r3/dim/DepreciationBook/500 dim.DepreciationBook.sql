EXEC dbo.drop_object 'dim.DepreciationBook', 'T' ;
GO
CREATE TABLE dim.DepreciationBook
( -- System
  DepreciationBookID                INT             IDENTITY(1, 1) NOT NULL PRIMARY KEY CLUSTERED
, StageID                           BIGINT          NULL
, DataConnectionID                  INT             NOT NULL
, CompanyID                         INT             NOT NULL
, Partition                         BIGINT          NOT NULL
, ExecutionTimestamp                ROWVERSION      NOT NULL
, ComponentExecutionID              INT             NOT NULL
-- BK
, FixedAssetCode                    NVARCHAR(30)    NULL
, DepreciationBookCode              NVARCHAR(30)    NULL
-- Attributes
, DepreciationBookDescription       NVARCHAR(255)   NOT NULL
, DepreciationBookCodeDescription   NVARCHAR(500)   NOT NULL
, AssetStatusCode                   INT             NOT NULL
, AssetStatusDescription            NVARCHAR(255)   NOT NULL
, DepreciationConventionCode        INT             NOT NULL
, DepreciationConventionDescription NVARCHAR(255)   NOT NULL
, ServiceLife                       NUMERIC(32, 16) NOT NULL
, DepreciationPeriods               INT             NOT NULL
) ;


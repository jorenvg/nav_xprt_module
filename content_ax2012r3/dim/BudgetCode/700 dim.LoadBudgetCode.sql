
EXEC dbo.drop_object @object = N'dim.LoadBudgetCode', -- nvarchar(128)
  @type = N'P', -- nchar(2)
  @debug = 0 -- int
GO 

CREATE PROCEDURE [dim].[LoadBudgetCode] 
  @component_execution_id INT = -1
, @load_type TINYINT = 1
, @inserted INT = NULL OUTPUT
, @updated INT = NULL OUTPUT
, @deleted INT = NULL OUTPUT
AS
BEGIN
			
    SET @deleted = 0;
    SET @updated = 0;
    SET @inserted = 0;
  
  UPDATE dim.BudgetCode SET 
		  StageID               = BCV.StageID
		, BudgetDesc            = BCV.BudgetDesc
		, BudgetCodeDesc        = BCV.BudgetCodeDesc
		, BudgetTypeCode        = BCV.BudgetTypeCode
		, BudgetTypeDesc        = BCV.BudgetTypeDesc
		, ComponentExecutionID  = @component_execution_id
  FROM
    dim.BudgetCode BC
    INNER JOIN dim.BudgetCodeView BCV ON BCV.BudgetCode           = BC.BudgetCode 
										AND BCV.DataConnectionID  = BC.DataConnectionID 
										AND BCV.CompanyID         = BC.CompanyID
										AND BCV.Partition         = BC.Partition
			
  SELECT @updated = @@ROWCOUNT

  INSERT INTO dim.BudgetCode
  (
		  StageID
		, DataConnectionID
		, ComponentExecutionID
		, CompanyID
		, Partition
		, BudgetCode
		, BudgetDesc
		, BudgetCodeDesc
		, BudgetTypeCode
		, BudgetTypeDesc
  )
  SELECT
		  BCV.StageID
		, BCV.DataConnectionID
		, COALESCE(@component_execution_id,-1)
		, BCV.CompanyID
		, BCV.Partition
		, BCV.BudgetCode
		, BCV.BudgetDesc
		, BCV.BudgetCodeDesc
		, BCV.BudgetTypeCode
		, BCV.BudgetTypeDesc
  FROM
    dim.BudgetCodeView BCV
  WHERE
    NOT EXISTS (SELECT * FROM dim.BudgetCode BC WHERE BCV.BudgetCode = BC.BudgetCode 
													AND BCV.CompanyID = BC.CompanyID
													AND BCV.DataConnectionID = BC.DataConnectionID 
													AND BCV.Partition = BC.Partition)
  
  SELECT @inserted = @@ROWCOUNT
END 
GO
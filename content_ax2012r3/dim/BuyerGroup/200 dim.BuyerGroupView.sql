EXEC dbo.drop_object 'dim.BuyerGroupView', 'V' ;
GO

CREATE VIEW [dim].[BuyerGroupView]
AS
  SELECT ISNULL(NULLIF(IBG.GROUP_, ''), 'N/A')                                                   AS BuyerGroupCode
       , ISNULL(NULLIF(IBG.DESCRIPTION, ''), 'N/A')                                              AS BuyerGroupDesc
       , ISNULL(NULLIF(IBG.GROUP_, '') + ' - ', '') + ISNULL(NULLIF(IBG.DESCRIPTION, ''), 'N/A') AS BuyerGroupCodeDesc
       , IBG.company_id                                                                          AS CompanyID
       , IBG.data_connection_id                                                                  AS DataConnectionID
       , IBG.Partition                                                                           AS Partition
       , IBG.stage_id                                                                            AS StageID
    FROM stage_ax.INVENTBUYERGROUP AS IBG ;


-- ################################################################################ --
-- #####                         Table: dim.DeliveryTerms                     ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.DeliveryTerms', 'T' ;

CREATE TABLE dim.DeliveryTerms
(
  DeliveryTermsID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID               BIGINT        NULL
, DataConnectionID      INT           NOT NULL
, CompanyID             INT           NOT NULL
, Partition             BIGINT
, ExecutionTimestamp    ROWVERSION    NOT NULL
, ComponentExecutionID  INT           NOT NULL
, DeliveryTermsCode     NVARCHAR(512) NOT NULL
, DeliveryTermsDesc     NVARCHAR(512) NOT NULL
, DeliveryTermsCodeDesc NVARCHAR(512) NOT NULL
) ;
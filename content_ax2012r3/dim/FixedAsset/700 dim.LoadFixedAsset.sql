EXEC dbo.drop_object @object = N'dim.LoadFixedAsset', @type = N'P', @debug = 0 ;
GO

CREATE PROCEDURE [dim].LoadFixedAsset
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.FixedAsset
     SET      StageID = FAV.StageID
            , ComponentExecutionID = @component_execution_id
            , FixedAssetName = FAV.FixedAssetName
            , FixedAssetCodeName = FAV.FixedAssetCodeName
            , FixedAssetGroupCode = FAV.FixedAssetGroupCode
            , FixedAssetGroupName = FAV.FixedAssetGroupName
            , FixedAssetTypeCode = FAV.FixedAssetTypeCode
            , FixedAssetTypeDescription = FAV.FixedAssetTypeDescription
            , FixedAssetMajorTypeCode = FAV.FixedAssetMajorTypeCode
            , FixedAssetMajorTypeDescription = FAV.FixedAssetMajorTypeDescription
            , FixedAssetMajorTypeCodeDescription = FAV.FixedAssetMajorTypeCodeDescription
            , FixedAssetPropertyTypeCode = FAV.FixedAssetPropertyTypeCode
            , FixedAssetPropertyTypeDescription = FAV.FixedAssetPropertyTypeDescription
            , FixedAssetResponsibleCode = FAV.FixedAssetResponsibleCode
            , FixedAssetResponsibleName = FAV.FixedAssetResponsibleName
            , FixedAssetResponsibleCodeName = FAV.FixedAssetResponsibleCodeName
            , FixedAssetLocationCode = FAV.FixedAssetLocationCode
            , FixedAssetLocationName = FAV.FixedAssetLocationName
    FROM      dim.FixedAsset     AS FA
   INNER JOIN dim.FixedAssetView AS FAV ON FA.FixedAssetCode   = FAV.FixedAssetCode
                                       AND FA.CompanyID        = FAV.CompanyID
                                       AND FA.Partition        = FAV.Partition
                                       AND FA.DataConnectionID = FAV.DataConnectionID ;

  SELECT @updated = @@RowCount ;

  INSERT INTO dim.FixedAsset (
    StageID
  , DataConnectionID
  , CompanyID
  , Partition
  , ComponentExecutionID
  , FixedAssetCode
  , FixedAssetName
  , FixedAssetCodeName
  , FixedAssetGroupCode
  , FixedAssetGroupName
  , FixedAssetGroupCodeName
  , FixedAssetTypeCode
  , FixedAssetTypeDescription
  , FixedAssetMajorTypeCode
  , FixedAssetMajorTypeDescription
  , FixedAssetMajorTypeCodeDescription
  , FixedAssetPropertyTypeCode
  , FixedAssetPropertyTypeDescription
  , FixedAssetResponsibleCode
  , FixedAssetResponsibleName
  , FixedAssetResponsibleCodeName
  , FixedAssetLocationCode
  , FixedAssetLocationName
  , FixedAssetLocationCodeName
  )
  SELECT FAV.StageId
       , FAV.DataConnectionID
       , FAV.CompanyID
       , FAV.Partition
       , @component_execution_id
       , FAV.FixedAssetCode
       , FAV.FixedAssetName
       , FAV.FixedAssetCodeName
       , FAV.FixedAssetGroupCode
       , FAV.FixedAssetGroupName
       , FAV.FixedAssetGroupCodeName
       , FAV.FixedAssetTypeCode
       , FAV.FixedAssetTypeDescription
       , FAV.FixedAssetMajorTypeCode
       , FAV.FixedAssetMajorTypeDescription
       , FAV.FixedAssetMajorTypeCodeDescription
       , FAV.FixedAssetPropertyTypeCode
       , FAV.FixedAssetPropertyTypeDescription
       , FAV.FixedAssetResponsibleCode
       , FAV.FixedAssetResponsibleName
       , FAV.FixedAssetResponsibleCodeName
       , FAV.FixedAssetLocationCode
       , FAV.FixedAssetLocationName
       , FAV.FixedAssetLocationCodeName
    FROM dim.FixedAssetView AS FAV
   WHERE NOT EXISTS ( SELECT 1
                        FROM dim.FixedAsset AS FA
                       WHERE FA.FixedAssetCode   = FAV.FixedAssetCode
                         AND FA.CompanyID        = FAV.CompanyID
                         AND FA.Partition        = FAV.Partition
                         AND FA.DataConnectionID = FAV.DataConnectionID) ;

  SELECT @inserted = @@RowCount ;
END ;


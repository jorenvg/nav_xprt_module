/****************************************************************************************************
    Functionality:  Shows the fixed assets from AX based on the design 
    (https://hillstar.sharepoint.com/:w:/s/birds37/EdrRO6H0uWtDjTVDN2Py_yABUIfJV2RWEVeEyVct7Th1pA?e=fzDZgW) 
    used for the dimension Fixed Asset
    Created by:     AzureAD\JeroenvanLier,     Date:           25/09/2018
    
    Date            Changed by      Ticket/Change       Description

*****************************************************************************************************/
EXEC dbo.drop_object @object = N'dim.FixedAssetView' -- nvarchar(128)
                   , @type = N'V' ;                  -- nchar(2)
GO
CREATE VIEW dim.FixedAssetView
AS
  SELECT            at.stage_id                                                                                      AS StageID
                  , at.company_id                                                                                    AS CompanyID
                  , at.PARTITION                                                                                     AS Partition
                  , at.data_connection_id                                                                            AS DataConnectionID
                  , at.ASSETID                                                                                       AS FixedAssetCode
                  , COALESCE(NULLIF(at.NAME, ''), 'N/A')                                                             AS FixedAssetName
                  , at.ASSETID + ' - ' + COALESCE(NULLIF(at.NAME, ''), 'N/A')                                        AS FixedAssetCodeName
                  , COALESCE(NULLIF(at.ASSETGROUP, ''), 'N/A')                                                       AS FixedAssetGroupCode
                  , COALESCE(NULLIF(ag.NAME, ''), 'N/A')                                                             AS FixedAssetGroupName
                  , COALESCE(NULLIF(at.ASSETGROUP, ''), 'N/A') + ' - ' + COALESCE(NULLIF(ag.NAME, ''), 'N/A')        AS FixedAssetGroupCodeName
                  , COALESCE(at.ASSETTYPE, -1)                                                                       AS FixedAssetTypeCode
                  , COALESCE(NULLIF(e_at.Description, ''), 'N/A')                                                    AS FixedAssetTypeDescription
                  , COALESCE(NULLIF(at.MAJORTYPE, ''), 'N/A')                                                        AS FixedAssetMajorTypeCode
                  , COALESCE(NULLIF(amt.Description, ''), 'N/A')                                                     AS FixedAssetMajorTypeDescription
                  , COALESCE(NULLIF(at.MAJORTYPE, ''), 'N/A') + ' - ' + COALESCE(NULLIF(amt.Description, ''), 'N/A') AS FixedAssetMajorTypeCodeDescription
                  , COALESCE(at.PROPERTYTYPE, -1)                                                                    AS FixedAssetPropertyTypeCode
                  , COALESCE(NULLIF(e_pt.Description, ''), 'N/A')                                                    AS FixedAssetPropertyTypeDescription
                  , COALESCE(NULLIF(hw.PERSONNELNUMBER, ''), 'N/A')                                                  AS FixedAssetResponsibleCode
                  , COALESCE(NULLIF(dpt.NAME, ''), 'N/A')                                                            AS FixedAssetResponsibleName
                  , COALESCE(NULLIF(hw.PERSONNELNUMBER, ''), 'N/A') + ' - ' + COALESCE(NULLIF(dpt.NAME, ''), 'N/A')  AS FixedAssetResponsibleCodeName
                  , COALESCE(NULLIF(at.LOCATION, ''), 'N/A')                                                         AS FixedAssetLocationCode
                  , COALESCE(NULLIF(al.NAME, ''), 'N/A')                                                             AS FixedAssetLocationName
                  , COALESCE(NULLIF(at.LOCATION, ''), 'N/A') + ' - ' + COALESCE(NULLIF(al.NAME, ''), 'N/A')          AS FixedAssetLocationCodeName
    FROM            stage_ax.ASSETTABLE     AS at
    LEFT OUTER JOIN stage_ax.ASSETGROUP     AS ag ON at.ASSETGROUP           = ag.GROUPID
                                                 AND at.company_id           = ag.company_id
                                                 AND at.PARTITION            = ag.PARTITION
                                                 AND at.data_connection_id   = ag.data_connection_id
    LEFT OUTER JOIN stage_ax.ASSETMAJORTYPE AS amt ON at.MAJORTYPE           = amt.MAJORTYPE
                                                  AND at.company_id          = amt.company_id
                                                  AND at.PARTITION           = amt.PARTITION
                                                  AND at.data_connection_id  = amt.data_connection_id
    LEFT OUTER JOIN stage_ax.ASSETLOCATION  AS al ON at.LOCATION             = al.LOCATION
                                                 AND at.company_id           = al.company_id
                                                 AND at.PARTITION            = al.PARTITION
                                                 AND at.data_connection_id   = al.data_connection_id
    LEFT OUTER JOIN stage_ax.HCMWORKER      AS hw ON at.WORKERRESPONSIBLE    = hw.RECID
                                                 AND at.PARTITION            = hw.PARTITION
                                                 AND at.data_connection_id   = hw.data_connection_id
    LEFT OUTER JOIN stage_ax.DIRPARTYTABLE  AS dpt ON hw.PERSON              = dpt.RECID
                                                  AND at.PARTITION           = dpt.PARTITION
                                                  AND at.data_connection_id  = dpt.data_connection_id
    LEFT OUTER JOIN meta.enumerations       AS e_at ON at.ASSETTYPE          = e_at.OptionID
                                                   AND e_at.TableName        = 'ASSETTABLE'
                                                   AND e_at.ColumnName       = 'ASSETTYPE'
                                                   AND at.data_connection_id = e_at.DataConnectionID
    LEFT OUTER JOIN meta.enumerations       AS e_pt ON at.PROPERTYTYPE       = e_pt.OptionID
                                                   AND e_pt.TableName        = 'ASSETTABLE'
                                                   AND e_pt.ColumnName       = 'PropertyType'
                                                   AND at.data_connection_id = e_at.DataConnectionID ;

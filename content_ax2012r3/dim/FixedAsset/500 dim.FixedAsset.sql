EXEC dbo.drop_object 'dim.FixedAsset ', 'T' ;
GO
CREATE TABLE dim.FixedAsset
(
  FixedAssetID                       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                            BIGINT        NOT NULL
, DataConnectionID                   INT           NOT NULL
, CompanyID                          INT           NOT NULL
, Partition                          BIGINT        NOT NULL
, ExecutionTimestamp                 ROWVERSION    NOT NULL
, ComponentExecutionID               INT           NOT NULL
/** BK **/
, FixedAssetCode                     NVARCHAR(50)  NOT NULL
/** ATTRIBUTES **/
, FixedAssetName                     NVARCHAR(100) NOT NULL
, FixedAssetCodeName                 NVARCHAR(255) NOT NULL
, FixedAssetGroupCode                NVARCHAR(30)  NOT NULL
, FixedAssetGroupName                NVARCHAR(50)  NOT NULL
, FixedAssetGroupCodeName            NVARCHAR(100) NOT NULL
, FixedAssetTypeCode                 INT           NOT NULL
, FixedAssetTypeDescription          NVARCHAR(100) NOT NULL
, FixedAssetMajorTypeCode            NVARCHAR(50)  NOT NULL
, FixedAssetMajorTypeDescription     NVARCHAR(100) NOT NULL
, FixedAssetMajorTypeCodeDescription NVARCHAR(255) NOT NULL
, FixedAssetPropertyTypeCode         INT           NOT NULL
, FixedAssetPropertyTypeDescription  NVARCHAR(100) NOT NULL
, FixedAssetResponsibleCode          NVARCHAR(50)  NOT NULL
, FixedAssetResponsibleName          NVARCHAR(255) NOT NULL
, FixedAssetResponsibleCodeName      NVARCHAR(512) NOT NULL
, FixedAssetLocationCode             NVARCHAR(50)  NOT NULL
, FixedAssetLocationName             NVARCHAR(100) NOT NULL
, FixedAssetLocationCodeName         NVARCHAR(255) NOT NULL
) ;
EXEC dbo.drop_object 'dim.TimeCalculation', 'T' ;

CREATE TABLE dim.TimeCalculation
(
  TimeCalculationID      INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, TimeCalculationCode    NVARCHAR(20)  NOT NULL
, TimeCalculationDesc    NVARCHAR(100) NULL
, TimeCalculationDesc_NL NVARCHAR(100) NULL
) ;
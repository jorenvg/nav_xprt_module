
-- ################################################################################ --
-- #####                         Table: dim.ProductionOrder                     ##### --
-- ################################################################################ --
EXEC Dbo.Drop_Object 'dim.ProductionOrder', 'T' ;

CREATE TABLE dim.ProductionOrder
(
  ProductionOrderID              INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                        BIGINT        NULL
, DataConnectionID               INT           NOT NULL
, CompanyID                      INT           NOT NULL
, Partition                      BIGINT        NOT NULL
, ExecutionTimestamp             ROWVERSION    NOT NULL
, ComponentExecutionID           INT           NOT NULL
, ProductionOrder                NVARCHAR(512) NOT NULL
, ProductionStatusCode           INT           NULL
, ProductionStatusDescription    NVARCHAR(256) NULL
, ProductionTypeCode             INT           NULL
, ProductionTypeDescription      NVARCHAR(256) NULL
, SchedulingStatusCode           INT           NULL
, SchedulingStatusDescription    NVARCHAR(256) NULL
, RemainStatusCode               INT           NULL
, RemainStatusDescription        NVARCHAR(256) NULL
, ProductionPoolCode             NVARCHAR(20)  NULL
, ProductionPoolDescription      NVARCHAR(120) NULL
, ProductionPoolCodeDescription  NVARCHAR(150) NULL
, ProductionGroupCode            NVARCHAR(20)  NULL
, ProductionGroupDescription     NVARCHAR(120) NULL
, ProductionGroupCodeDescription NVARCHAR(255) NULL
) ;


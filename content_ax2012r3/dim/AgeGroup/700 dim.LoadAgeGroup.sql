EXEC dbo.drop_object @object = N'dim.LoadAgeGroup' -- nvarchar(128)
                   , @type = N'P'                  -- nchar(2)
                   , @debug = 0 ;                  -- int
GO

CREATE PROCEDURE dim.LoadAgeGroup
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;


  DELETE FROM dim.AgeGroup WHERE AgeGroupId = 0 ;

  UPDATE dim.AgeGroup
     SET AgeGroupDesc = vw.[DESC]
       , AgeGroupMin = vw.[Min]
       , AgeGroupMax = vw.[Max]
    FROM dim.AgeGroup     AS tbl
    JOIN dim.AgeGroupView vw ON tbl.AgeGroupCode = vw.Code ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT dim.AgeGroup (
    AgeGroupCode, AgeGroupDesc, AgeGroupMin, AgeGroupMax
  )
  SELECT Code
       , [DESC]
       , [Min]
       , [Max]
    FROM dim.AgeGroupView vw
   WHERE NOT EXISTS (SELECT 1 FROM dim.AgeGroup AS tbl WHERE tbl.AgeGroupCode = vw.Code) ;

  SELECT @inserted = @@ROWCOUNT ;

END ;

GO


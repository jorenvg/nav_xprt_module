EXEC dbo.drop_object 'dim.AgeGroup', 'T' ;

CREATE TABLE dim.AgeGroup
(
  AgeGroupID   INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, AgeGroupCode NVARCHAR(20)  NOT NULL
, AgeGroupDesc NVARCHAR(100) NOT NULL
, AgeGroupMin  INT           NOT NULL
, AgeGroupMax  INT           NOT NULL
) ;

EXEC dbo.drop_object @object = N'dim.SalesMarkupOriginView', @type = N'V' ;
GO
CREATE VIEW dim.SalesMarkupOriginView
AS
  SELECT TABLEID                 AS OriginTableCode  --Bkey
       , SQLNAME                 AS OriginTable      --Bkey 
       , CASE
           WHEN SQLD.SQLNAME IN ( 'SalesLine', 'SalesTable' ) THEN 'SalesLine'
           WHEN SQLD.SQLNAME IN ( 'CustPackingSlipTrans', 'CustPackingSlipJour' ) THEN 'SalesPacking'
           WHEN SQLD.SQLNAME IN ( 'CustInvoiceTrans', 'CustInvoiceJour' ) THEN 'SalesInvoice'
         END                     AS OriginMeasureGroup
       , SQLD.data_connection_id AS DataConnectionID --Bkey
       , SQLD.stage_id           AS StageID
    FROM stage_ax.SQLDICTIONARY AS SQLD
   WHERE SQLD.SQLNAME IN ( 'SalesLine', 'SalesTable', 'CustPackingSlipTrans', 'CustPackingSlipJour', 'CustInvoiceTrans', 'CustInvoiceJour' )
     AND SQLD.FIELDID = 0 ;

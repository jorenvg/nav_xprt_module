EXEC dbo.drop_object @object = N'dim.LoadSalesOrderType' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadSalesOrderType]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesOrderType
     SET      SalesOrderTypeDesc = SOTV.SalesOrderTypeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.SalesOrderType     SOT
   INNER JOIN dim.SalesOrderTypeView SOTV ON SOTV.SalesOrderTypeCode = SOT.SalesOrderTypeCode
                                         AND SOTV.DataConnectionID   = SOT.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.SalesOrderType (
    DataConnectionID, ComponentExecutionID, SalesOrderTypeCode, SalesOrderTypeDesc
  )
  SELECT SOTV.DataConnectionID
       , @component_execution_id
       , SOTV.SalesOrderTypeCode
       , SOTV.SalesOrderTypeDesc
    FROM dim.SalesOrderTypeView SOTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesOrderType SOT
                       WHERE SOTV.SalesOrderTypeCode = SOT.SalesOrderTypeCode
                         AND SOTV.DataConnectionID   = SOT.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
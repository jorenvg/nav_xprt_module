EXEC dbo.drop_object @object = N'dim.LoadSalesTaxDirection' -- nvarchar(128)
                   , @type = N'P'                           -- nchar(2)
                   , @debug = 0 ;                           -- int
GO

CREATE PROCEDURE [dim].[LoadSalesTaxDirection]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesTaxDirection
     SET      SalesTaxDirectionDesc = TDV.SalesTaxDirectionDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.SalesTaxDirection     TD
   INNER JOIN dim.SalesTaxDirectionView TDV ON TDV.SalesTaxDirectionCode = TD.SalesTaxDirectionCode
                                           AND TDV.DataConnectionID      = TD.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.SalesTaxDirection (
    DataConnectionID, ComponentExecutionID, SalesTaxDirectionCode, SalesTaxDirectionDesc
  )
  SELECT TDV.DataConnectionID
       , COALESCE(@component_execution_id, -1)
       , TDV.SalesTaxDirectionCode
       , TDV.SalesTaxDirectionDesc
    FROM dim.SalesTaxDirectionView TDV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesTaxDirection TD
                       WHERE TDV.SalesTaxDirectionCode = TD.SalesTaxDirectionCode
                         AND TDV.DataConnectionID      = TD.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;

END ;
GO



EXEC dbo.drop_object 'dim.Item', 'T' ;
GO
CREATE TABLE [dim].[Item]
(
  ItemID                          INT             IDENTITY(1, 1) NOT NULL PRIMARY KEY CLUSTERED
, StageID                         BIGINT          NULL
, ExecutionTimestamp              ROWVERSION      NOT NULL
, ComponentExecutionID            INT             NOT NULL
, [ItemNumber]                    [NVARCHAR](30)  NOT NULL
, [ProductName]                   [NVARCHAR](100) NOT NULL
, [ProductNo]                     [NVARCHAR](100) NOT NULL
, [ProductNoName]                 [NVARCHAR](512) NOT NULL
, [StorageDimensionGroupCode]     [NVARCHAR](30)  NOT NULL
, [StorageDimensionGroupDesc]     [NVARCHAR](128) NOT NULL
, [StorageDimensionGroupCodeDesc] [NVARCHAR](512) NOT NULL
, [ItemModelGroupCode]            [NVARCHAR](30)  NOT NULL
, [ItemModelGroupDesc]            [NVARCHAR](100) NOT NULL
, [ItemModelGroupCodeDesc]        [NVARCHAR](512) NOT NULL
, [UnitOfMeasureCode]             [NVARCHAR](30)  NOT NULL
, [UnitOfMeasureDesc]             [NVARCHAR](100) NOT NULL
, [UnitOfMeasureCodeDesc]         [NVARCHAR](512) NOT NULL
, [VendorCode]                    [NVARCHAR](30)  NOT NULL
, [VendorDesc]                    [NVARCHAR](128) NOT NULL
, [VendorCodeDesc]                [NVARCHAR](512) NOT NULL
, [CommissionGroupCode]           [NVARCHAR](30)  NOT NULL
, [CommissionGroupDesc]           [NVARCHAR](100) NOT NULL
, [CommissionGroupCodeDesc]       [NVARCHAR](512) NOT NULL
, [ItemRebateGroupCode]           [NVARCHAR](30)  NOT NULL
, [ItemRebateGroupDesc]           [NVARCHAR](100) NOT NULL
, [ItemRebateGroupCodeDesc]       [NVARCHAR](512) NOT NULL
, [CountryOfOriginCode]           [NVARCHAR](30)  NOT NULL
, [CountryOfOriginDesc]           [NVARCHAR](512) NOT NULL
, [CountryOfOriginCodeDesc]       [NVARCHAR](512) NOT NULL
, [StateOfOriginCode]             [NVARCHAR](30)  NOT NULL
, [StateOfOriginDesc]             [NVARCHAR](100) NOT NULL
, [StateOfOriginCodeDesc]         [NVARCHAR](512) NOT NULL
, [ItemGroupCode]                 [NVARCHAR](30)  NOT NULL
, [ItemGroupDesc]                 [NVARCHAR](100) NOT NULL
, [ItemGroupCodeDesc]             [NVARCHAR](512) NOT NULL
, [CostGroupCode]                 [NVARCHAR](30)  NOT NULL
, [CostGroupDesc]                 [NVARCHAR](100) NOT NULL
, [CostGroupCodeDesc]             [NVARCHAR](512) NOT NULL
, [ProjectCategoryGroupCode]      [NVARCHAR](30)  NOT NULL
, [ProjectCategoryGroupDesc]      [NVARCHAR](100) NOT NULL
, [ProjectCategoryGroupCodeDesc]  [NVARCHAR](512) NOT NULL
, [ABCCarryingCostGroupCode]      [INT]           NOT NULL
, [ABCCarryingCostGroupDesc]      [NVARCHAR](255) NOT NULL
, [ABCMarginGroupCode]            [INT]           NOT NULL
, [ABCMarginGroupDesc]            [NVARCHAR](255) NOT NULL
, [ABCRevenueGroupCode]           [INT]           NOT NULL
, [ABCRevenueGroupDesc]           [NVARCHAR](255) NOT NULL
, [ABCValueGroupCode]             [INT]           NOT NULL
, [ABCValueGroupDesc]             [NVARCHAR](255) NOT NULL
, [CompanyID]                     [INT]           NOT NULL
, [DataConnectionID]              [INT]           NOT NULL
, [Partition]                     [BIGINT]        NOT NULL
) ;

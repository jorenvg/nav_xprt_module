EXEC dbo.drop_object @object = N'dim.LoadVoucher' -- nvarchar(128)
                   , @type = N'P'                 -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadVoucher]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  INSERT INTO dim.Voucher (
    DataConnectionID, ComponentExecutionID, Voucher, IsGL, [Partition], CompanyID
  )
  SELECT a.DataConnectionID
       , @component_execution_id
       , Voucher
       , IsGL
       , [Partition]
       , CompanyID
    FROM dim.VoucherView AS a
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Voucher AS b
                       WHERE a.Voucher          = b.Voucher
                         AND a.Partition        = b.Partition
                         AND a.CompanyID        = b.CompanyID
                         AND a.DataConnectionID = b.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;

END ;
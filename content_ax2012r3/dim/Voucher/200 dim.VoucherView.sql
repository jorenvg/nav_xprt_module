/****************************************************************************************************
    Functionality:  Selects all vouchers from the subledgers (Payables, Receivables, Inventory) and joins them with the vouchers available in GL
    This dimension can be used to validate, and connect the different subledgers to the GL.
    Created by:     AzureAD\JeroenvanLier,     Date:           29/06/2018
    
    Date          Changed by              Ticket/Change   Description
    29/06/2018		AzureAD\JeroenvanLier		DEV-960		      Renamed the dimension to dim.Voucher instead of dim.JournalVoucher
*****************************************************************************************************/
EXEC dbo.drop_object 'dim.VoucherView', 'V' ;
GO

CREATE VIEW [dim].[VoucherView]
AS
  WITH voucher_subledger AS (
    SELECT DISTINCT
           i.VOUCHER            AS Voucher
         , i.data_connection_id AS DataConnectionID
         , i.company_id         AS CompanyID
         , i.PARTITION          AS Partition
      FROM stage_ax.INVENTTRANSPOSTING AS i
    UNION
    SELECT DISTINCT
           i.VOUCHER            AS Voucher
         , i.data_connection_id AS DataConnectionID
         , i.company_id         AS CompanyID
         , i.PARTITION          AS Partition
      FROM stage_ax.CUSTTRANS AS i
    UNION
    SELECT DISTINCT
           i.VOUCHER            AS Voucher
         , i.data_connection_id AS DataConnectionID
         , i.company_id         AS CompanyID
         , i.PARTITION          AS Partition
      FROM stage_ax.VENDTRANS AS i
  )
     , voucher_gl AS (
    SELECT DISTINCT
           gje.SUBLEDGERVOUCHER   AS Voucher
         , 1                      AS IsGL
         , gje.data_connection_id AS DataConnectionID
         , gje.company_id         AS CompanyID
         , gje.PARTITION          AS Partition
      FROM stage_ax.GENERALJOURNALENTRY AS gje
  )
  SELECT            ISNULL(gje.Voucher, s.Voucher)                   AS Voucher
                  , ISNULL(gje.IsGL, 0)                              AS IsGL
                  , ISNULL(gje.DataConnectionID, s.DataConnectionID) AS DataConnectionID
                  , ISNULL(gje.CompanyID, s.CompanyID)               AS CompanyID
                  , ISNULL(gje.Partition, s.Partition)               AS Partition
    FROM            voucher_gl        AS gje
    FULL OUTER JOIN voucher_subledger AS s ON gje.Voucher          = s.Voucher
                                          AND gje.CompanyID        = s.CompanyID
                                          AND gje.DataConnectionID = s.DataConnectionID
                                          AND gje.Partition        = s.Partition ;
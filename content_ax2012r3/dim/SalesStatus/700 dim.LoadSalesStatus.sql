EXEC dbo.drop_object @object = N'dim.LoadSalesStatus' -- nvarchar(128)
                   , @type = N'P'                     -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadSalesStatus]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesStatus
     SET      SalesStatusDesc = SLSV.SalesStatusDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.SalesStatus     SLS
   INNER JOIN dim.SalesStatusView SLSV ON SLSV.SalesStatusCode  = SLS.SalesStatusCode
                                      AND SLSV.DataConnectionID = SLS.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.SalesStatus (
    DataConnectionID, ComponentExecutionID, SalesStatusCode, SalesStatusDesc
  )
  SELECT SLSV.DataConnectionID
       , @component_execution_id
       , SLSV.SalesStatusCode
       , SLSV.SalesStatusDesc
    FROM dim.SalesStatusView SLSV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesStatus SLS
                       WHERE SLSV.SalesStatusCode  = SLS.SalesStatusCode
                         AND SLSV.DataConnectionID = SLS.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
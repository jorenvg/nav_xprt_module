EXEC dbo.drop_object 'dim.SalesStatusView', 'V' ;
GO

CREATE VIEW [dim].[SalesStatusView]
AS
  SELECT ISNULL(OptionID, -1)                   AS SalesStatusCode
       , ISNULL(NULLIF(Description, ''), 'N/A') AS SalesStatusDesc
       , DataConnectionID
    FROM meta.enumerations
   WHERE TableName  = 'SALESTABLE'
     AND ColumnName = 'SALESSTATUS' ;


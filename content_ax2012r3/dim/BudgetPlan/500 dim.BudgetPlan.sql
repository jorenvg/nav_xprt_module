-- ################################################################################ --
-- #####                         Table: dim.BudgetPlan                        ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.BudgetPlan', 'T' ;
GO
CREATE TABLE dim.BudgetPlan
(
  BudgetPlanID                INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                     BIGINT        NULL
, DataConnectionID            INT           NOT NULL
, ExecutionTimestamp          ROWVERSION    NOT NULL
, ComponentExecutionID        INT           NOT NULL
, Partition                   BIGINT        NULL
, BudgetPlanCode              NVARCHAR(512) NOT NULL
, BudgetPlanDesc              NVARCHAR(512) NOT NULL
, BudgetPlanCodeDesc          NVARCHAR(512) NOT NULL
, BudgetProcessCode           NVARCHAR(512) NOT NULL
, BudgetProcessDesc           NVARCHAR(512) NOT NULL
, BudgetProcessCodeDesc       NVARCHAR(512) NOT NULL
, BudgetPlanningStageCode     NVARCHAR(512) NOT NULL
, BudgetPlanningStageDesc     NVARCHAR(512) NOT NULL
, BudgetPlanningStageCodeDesc NVARCHAR(512) NOT NULL
) ;
GO
EXEC dbo.drop_object 'dim.BudgetPlanView', 'V' ;
GO
CREATE VIEW [dim].[BudgetPlanView]
AS
  --BudgetPlan
  SELECT      ISNULL(NULLIF(BP.DOCUMENTNUMBER, ''), 'N/A')                                           AS BudgetPlanCode
            , ISNULL(NULLIF(BP.NAME, ''), 'N/A')                                                     AS BudgetPlanDesc
            , ISNULL(NULLIF(BP.DOCUMENTNUMBER, '') + ' - ', '') + ISNULL(NULLIF(BP.NAME, ''), 'N/A') AS BudgetPlanCodeDesc
            , ISNULL(NULLIF(BPP.NAME, ''), 'N/A')                                                    AS BudgetProcessCode
            , ISNULL(NULLIF(BPP.DESCRIPTION, ''), 'N/A')                                             AS BudgetProcessDesc
            , ISNULL(NULLIF(BPP.NAME, '') + ' - ', '') + ISNULL(NULLIF(BPP.DESCRIPTION, ''), 'N/A')  AS BudgetProcessCodeDesc
            , ISNULL(NULLIF(BPS.NAME, ''), 'N/A')                                                    AS BudgetPlanningStageCode
            , ISNULL(NULLIF(BPS.DESCRIPTION, ''), 'N/A')                                             AS BudgetPlanningStageDesc
            , ISNULL(NULLIF(BPS.NAME, '') + ' - ', '') + ISNULL(NULLIF(BPS.DESCRIPTION, ''), 'N/A')  AS BudgetPlanningStageCodeDesc
            , BP.data_connection_id                                                                  AS DataConnectionID
            , BP.PARTITION                                                                           AS Partition
            , BP.stage_id                                                                            AS StageID
    FROM      stage_ax.BUDGETPLANHEADER      BP
    LEFT JOIN stage_ax.BUDGETPLANNINGPROCESS BPP ON BP.BUDGETPLANNINGPROCESS = BPP.RecID
                                                AND BP.PARTITION             = BPP.PARTITION
                                                AND BP.data_connection_id    = BPP.data_connection_id
    LEFT JOIN stage_ax.BUDGETPLANNINGSTAGE   BPS ON BP.BUDGETPLANNINGSTAGE   = BPS.RECID
                                                AND BP.PARTITION             = BPS.PARTITION
                                                AND BP.data_connection_id    = BPS.data_connection_id ;


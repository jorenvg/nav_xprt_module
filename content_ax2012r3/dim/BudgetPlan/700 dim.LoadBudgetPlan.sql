EXEC dbo.drop_object @object = N'dim.LoadBudgetPlan' -- nvarchar(128)
                   , @type = N'P'                    -- nchar(2)
                   , @debug = 0 ;                    -- int
GO

CREATE PROCEDURE [dim].[LoadBudgetPlan]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.BudgetPlan
     SET      StageID = BPV.StageID
            , BudgetPlanDesc = BPV.BudgetPlanDesc
            , BudgetPlanCodeDesc = BPV.BudgetPlanCodeDesc
            , BudgetProcessCode = BPV.BudgetProcessCode
            , BudgetProcessDesc = BPV.BudgetProcessDesc
            , BudgetProcessCodeDesc = BPV.BudgetProcessCodeDesc
            , BudgetPlanningStageCode = BPV.BudgetPlanningStageCode
            , BudgetPlanningStageDesc = BPV.BudgetPlanningStageDesc
            , BudgetPlanningStageCodeDesc = BPV.BudgetPlanningStageCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.BudgetPlan     BP
   INNER JOIN dim.BudgetPlanView BPV ON BPV.BudgetPlanCode   = BP.BudgetPlanCode
                                    AND BPV.DataConnectionID = BP.DataConnectionID
                                    AND BPV.Partition        = BP.Partition ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.BudgetPlan (
    StageID
  , DataConnectionID
  , ComponentExecutionID
  , Partition
  , BudgetPlanCode
  , BudgetPlanDesc
  , BudgetPlanCodeDesc
  , BudgetProcessCode
  , BudgetProcessDesc
  , BudgetProcessCodeDesc
  , BudgetPlanningStageCode
  , BudgetPlanningStageDesc
  , BudgetPlanningStageCodeDesc
  )
  SELECT BPV.StageID
       , BPV.DataConnectionID
       , COALESCE(@component_execution_id, -1)
       , BPV.Partition
       , BPV.BudgetPlanCode
       , BPV.BudgetPlanDesc
       , BPV.BudgetPlanCodeDesc
       , BPV.BudgetProcessCode
       , BPV.BudgetProcessDesc
       , BPV.BudgetProcessCodeDesc
       , BPV.BudgetPlanningStageCode
       , BPV.BudgetPlanningStageDesc
       , BPV.BudgetPlanningStageCodeDesc
    FROM dim.BudgetPlanView BPV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.BudgetPlan BP
                       WHERE BPV.BudgetPlanCode   = BP.BudgetPlanCode
                         AND BPV.DataConnectionID = BP.DataConnectionID
                         AND BPV.Partition        = BP.Partition) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
GO



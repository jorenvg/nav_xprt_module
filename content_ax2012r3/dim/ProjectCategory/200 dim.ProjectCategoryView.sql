EXEC dbo.drop_object 'dim.ProjectCategoryView', 'V' ;
GO

CREATE VIEW [dim].[ProjectCategoryView]
AS
  SELECT
    -- system information
                    PC.stage_id                                                                                                   AS StageID
                  , PC.data_connection_id                                                                                         AS DataConnectionID
                  , PC.PARTITION                                                                                                  AS Partition
                  , PC.company_id                                                                                                 AS CompanyID
                  -- business key
                  , PC.CATEGORYID                                                                                                 AS CategoryCode
                  -- attributes
                  , COALESCE(NULLIF(PC.NAME, ''), 'N/A')                                                                          AS CategoryDescription
                  , COALESCE(NULLIF(PC.CATEGORYID, '') + ' - ', '') + COALESCE(NULLIF(PC.NAME, ''), 'N/A') AS CategoryCodeDescription
                  , COALESCE(ENUM.OptionID, -1)                                                                                   AS CategoryTypeCode
                  , COALESCE(ENUM.Description, 'N/A')                                                                             AS CategoryTypeDescription
                  , COALESCE(NULLIF(PC.CATEGORYGROUPID, ''), 'N/A')                                                               AS CategoryGroupCode
                  , COALESCE(NULLIF(PCG.NAME, ''), 'N/A')                                                                         AS CategoryGroupDescription
                  , COALESCE(NULLIF(PC.CATEGORYGROUPID, '') + ' - ', '') + COALESCE(NULLIF(PCG.NAME, ''), 'N/A')                  AS CategoryGroupCodeDescription
    FROM            stage_ax.PROJCATEGORY      AS PC
    LEFT OUTER JOIN stage_ax.PROJCATEGORYGROUP AS PCG ON PC.CATEGORYGROUPID = PCG.CATEGORYGROUPID
                                                     AND PC.PARTITION       = PCG.PARTITION
                                                     AND PC.company_id      = PCG.company_id
    LEFT OUTER JOIN meta.enumerations          AS ENUM ON PC.CATEGORYTYPE   = ENUM.OptionID
                                                      AND ENUM.TableName    = 'PROJCATEGORY'
                                                      AND ENUM.ColumnName   = 'CATEGORYTYPE' ;
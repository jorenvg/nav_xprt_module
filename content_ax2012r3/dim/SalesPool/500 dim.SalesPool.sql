EXEC dbo.drop_object 'dim.SalesPool', 'T' ;

CREATE TABLE dim.SalesPool
(
  SalesPoolID          INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, SalesPoolCode        NVARCHAR(30)  NOT NULL
, SalesPoolDesc        NVARCHAR(128) NOT NULL
, SalesPoolCodeDesc    NVARCHAR(255) NOT NULL
) ;

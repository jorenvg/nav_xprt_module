EXEC dbo.drop_object @object = N'dim.LoadSalesPool' -- nvarchar(128)
                   , @type = N'P'                   -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadSalesPool]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesPool
     SET      StageID = SPV.StageID
            , SalesPoolDesc = SPV.SalesPoolDesc
            , SalesPoolCodeDesc = SPV.SalesPoolCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.SalesPool     SP
   INNER JOIN dim.SalesPoolView SPV ON SPV.SalesPoolCode    = SP.SalesPoolCode
                                   AND SPV.DataConnectionID = SP.DataConnectionID
                                   AND SPV.CompanyID        = SP.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.SalesPool (
    StageID, DataConnectionID, ComponentExecutionID, CompanyID, Partition, SalesPoolCode, SalesPoolDesc, SalesPoolCodeDesc
  )
  SELECT SPV.StageID
       , SPV.DataConnectionID
       , @component_execution_id
       , SPV.CompanyID
       , SPV.Partition
       , SPV.SalesPoolCode
       , SPV.SalesPoolDesc
       , SPV.SalesPoolCodeDesc
    FROM dim.SalesPoolView SPV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesPool SP
                       WHERE SPV.SalesPoolCode    = SP.SalesPoolCode
                         AND SPV.DataConnectionID = SP.DataConnectionID
                         AND SPV.CompanyID        = SP.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
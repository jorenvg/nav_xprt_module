EXEC dbo.drop_object @object = N'dim.LoadSalesInvoice' -- nvarchar(128)
                   , @type = N'P'                      -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadSalesInvoice
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.SalesInvoice
     SET      ComponentExecutionID = @component_execution_id
            , InvoiceDate = SIV.InvoiceDate
            , DueDate = SIV.DueDate
            , ClosedDate = SIV.ClosedDate
    FROM      dim.SalesInvoice     SI
   INNER JOIN dim.SalesInvoiceView SIV ON SIV.SalesInvoice     = SI.SalesInvoice
                                      AND SIV.CompanyID        = SI.CompanyID
                                      AND SIV.Partition        = SI.Partition
                                      AND SIV.DataConnectionID = SI.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.SalesInvoice (
    DataConnectionID, CompanyID, ComponentExecutionID, Partition, SalesInvoice, InvoiceDate, DueDate, ClosedDate
  )
  SELECT SIV.DataConnectionID
       , SIV.CompanyID
       , @component_execution_id
       , SIV.Partition
       , SIV.SalesInvoice
       , SIV.InvoiceDate
       , SIV.DueDate
       , SIV.ClosedDate
    FROM dim.SalesInvoiceView AS SIV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.SalesInvoice AS SI
                       WHERE SIV.SalesInvoice     = SI.SalesInvoice
                         AND SIV.CompanyID        = SI.CompanyID
                         AND SIV.Partition        = SI.Partition
                         AND SIV.DataConnectionID = SI.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;

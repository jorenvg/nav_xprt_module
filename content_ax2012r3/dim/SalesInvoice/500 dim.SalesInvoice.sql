
-- ################################################################################ --
-- #####                         Table: dim.SalesInvoice                   ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.SalesInvoice', 'T' ;

CREATE TABLE dim.SalesInvoice
(
  SalesInvoiceID       INT          IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT          NOT NULL
, CompanyID            INT          NOT NULL
, ExecutionTimestamp   ROWVERSION   NOT NULL
, Partition            BIGINT       NOT NULL
, ComponentExecutionID INT          NOT NULL
, SalesInvoice         NVARCHAR(50) NOT NULL
, InvoiceDate          DATETIME     NOT NULL
, ClosedDate           DATETIME     NOT NULL
, DueDate              DATETIME     NOT NULL
) ;
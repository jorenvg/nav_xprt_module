EXEC dbo.drop_object 'dim.SalesInvoiceView', 'V' ;
GO

CREATE VIEW [dim].[SalesInvoiceView]
AS
  SELECT -- BK of the dimension is not unique in the table VENDPACKINGSLIPJOUR
                    CIJ.INVOICEID                                                                        AS SalesInvoice
                  , CIJ.company_id                                                                       AS CompanyID
                  , CIJ.data_connection_id                                                               AS DataConnectionID
                  , CIJ.PARTITION                                                                        AS Partition
                  , MIN(CT.TRANSDATE)                                                                    AS InvoiceDate
                  , ISNULL(NULLIF(MIN(CT.CLOSED), '1900-01-01 00:00:00.000'), '9999-01-01 00:00:00.000') AS ClosedDate
                  , ISNULL(NULLIF(MIN(CT.DUEDATE), '1900-01-01 00:00:00.000'),   MIN(CT.TRANSDATE))      AS DueDate
    FROM            stage_ax.CUSTINVOICEJOUR AS CIJ
    LEFT OUTER JOIN stage_ax.CUSTTRANS       AS CT ON CIJ.INVOICEID          = CT.INVOICE
                                                  AND CIJ.company_id         = CT.company_id
                                                  AND CIJ.data_connection_id = CT.data_connection_id
                                                  AND CIJ.PARTITION          = CT.PARTITION
   WHERE            CIJ.INVOICEID IS NOT NULL
   GROUP BY CIJ.INVOICEID
          , CIJ.company_id
          , CIJ.data_connection_id
          , CIJ.PARTITION ;

EXEC dbo.drop_object @object = N'dim.LoadRouteOperation' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadRouteOperation]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.RouteOperation
     SET      StageID = RO.StageID
            , CompanyID = ROT.CompanyID
            , DataConnectionID = ROT.DataConnectionID
            , ComponentExecutionID = @component_execution_id
            , Partition = ROT.Partition
            , RouteCode = ROT.RouteCode
            , RouteDescription = ROT.RouteDescription
            , RouteCodeDescription = ROT.RouteCodeDescription
            , Priority = ROT.Priority
            , OperationNumber = ROT.OperationNumber
            , OperationNumberNext = ROT.OperationNumberNext
            , OperationDescription = ROT.OperationDescription
            , OperationCodeDescription = ROT.OperationCodeDescription
            , Routelevel = ROT.Routelevel
    FROM      dim.RouteOperation     AS RO
   INNER JOIN dim.RouteOperationView AS ROT ON RO.RouteCode        = ROT.RouteCode
                                           AND RO.OperationCode    = ROT.OperationCode
                                           AND RO.DataConnectionID = ROT.DataConnectionID
                                           AND RO.Partition        = ROT.Partition ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.RouteOperation (
    StageID
  , CompanyID
  , DataConnectionID
  , ComponentExecutionID
  , Partition
  , RouteCode
  , RouteDescription
  , RouteCodeDescription
  , Priority
  , OperationNumber
  , OperationNumberNext
  , OperationCode
  , OperationDescription
  , OperationCodeDescription
  , Routelevel
  )
  SELECT ROV.StageID
       , ROV.CompanyID
       , ROV.DataConnectionID
       , @component_execution_id
       , Partition
       , ROV.RouteCode
       , ROV.RouteDescription
       , ROV.RouteCodeDescription
       , ROV.Priority
       , ROV.OperationNumber
       , ROV.OperationNumberNext
       , ROV.OperationCode
       , ROV.OperationDescription
       , ROV.OperationCodeDescription
       , ROV.Routelevel
    FROM dim.RouteOperationView AS ROV
   WHERE NOT EXISTS ( SELECT 1
                        FROM dim.RouteOperation AS RO
                       WHERE RO.RouteCode        = ROV.RouteCode
                         AND RO.OperationCode    = ROV.OperationCode
                         AND RO.Partition        = ROV.Partition
                         AND RO.DataConnectionID = ROV.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
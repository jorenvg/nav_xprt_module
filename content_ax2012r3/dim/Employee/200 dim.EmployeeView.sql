EXEC dbo.drop_object 'dim.EmployeeView', 'V' ;
GO

CREATE VIEW [dim].[EmployeeView]
AS
  SELECT      ISNULL(NULLIF(hcm.PERSONNELNUMBER, ''), 'N/A') AS EmployeeNumber
            , ISNULL(NULLIF(dpt.Name, ''), 'N/A')            AS EmployeeName
            , ISNULL(NULLIF(cmt.TITLEID, ''), 'N/A')         AS EmployeeTitle
            , ISNULL(NULLIF(wt.OFFICELOCATION, ''), 'N/A')   AS EmployeeOfficeLocation
            , hcm.data_connection_id                         AS DataConnectionID
            , hcm.Partition                                  AS Partition
            , hcm.RECID                                      AS EmployeeRecID
            , hcm.stage_id                                   AS StageID
    FROM      stage_ax.HCMWORKER      hcm
    LEFT JOIN stage_ax.HCMWORKERTITLE wt ON hcm.RECID      = wt.WORKER
                                        AND GETDATE() BETWEEN wt.VALIDFROM AND wt.VALIDTO
                                        AND hcm.PARTITION  = wt.PARTITION
    LEFT JOIN stage_ax.HCMTITLE       cmt ON wt.TITLE      = cmt.RECID
                                         AND hcm.PARTITION = cmt.PARTITION
    LEFT JOIN stage_ax.DIRPARTYTABLE  dpt ON hcm.PERSON    = dpt.RECID
                                         AND hcm.PARTITION = dpt.PARTITION ;
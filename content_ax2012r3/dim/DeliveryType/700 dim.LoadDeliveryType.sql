EXEC dbo.drop_object @object = N'dim.LoadDeliveryType' -- nvarchar(128)
                   , @type = N'P'                      -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadDeliveryType
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.DeliveryType
     SET      DeliveryTypeDesc = DTV.DeliveryTypeDesc
            , DeliveryTypeCodeDesc = DTV.DeliveryTypeCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.DeliveryType     DT
   INNER JOIN dim.DeliveryTypeView DTV ON DTV.DeliveryTypeCode = DT.DeliveryTypeCode
                                      AND DTV.DataConnectionID = DT.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.DeliveryType (
    DataConnectionID, ComponentExecutionID, DeliveryTypeCode, DeliveryTypeDesc, DeliveryTypeCodeDesc
  )
  SELECT DTV.DataConnectionID
       , @component_execution_id
       , DTV.DeliveryTypeCode
       , DTV.DeliveryTypeDesc
       , DTV.DeliveryTypeCodeDesc
    FROM dim.DeliveryTypeView DTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.DeliveryType DT
                       WHERE DTV.DeliveryTypeCode = DT.DeliveryTypeCode
                         AND DTV.DataConnectionID = DT.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
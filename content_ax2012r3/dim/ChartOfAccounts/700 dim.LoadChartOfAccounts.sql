EXEC dbo.drop_object @object = N'dim.LoadChartOfAccounts' -- nvarchar(128)
                   , @type = N'P'                         -- nchar(2)
                   , @debug = 0 ;                         -- int
GO

CREATE PROCEDURE [dim].[LoadChartOfAccounts]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.ChartOfAccounts
     SET      StageID = CAV.StageID
            , ChartOfAccountsName = CAV.ChartOfAccountsName
            , ChartOfAccountsDesc = CAV.ChartOfAccountsDesc
            , MainAccountCode = CAV.MainAccountCode
            , AccountName = CAV.AccountName
            , AccountCodeDesc = CAV.AccountCodeDesc
            , AccountTypeCode = CAV.AccountTypeCode
            , AccountTypeDesc = CAV.AccountTypeDesc
            , AccountCategoryCode = CAV.AccountCategoryCode
            , AccountCategoryDesc = CAV.AccountCategoryDesc
            , AccountCategoryCodeDesc = CAV.AccountCategoryCodeDesc
            , MainAccountRecID = CAV.MainAccountRecID
            , Balance = CAV.Balance
            , ComponentExecutionID = @component_execution_id
    FROM      dim.ChartOfAccounts     CA
   INNER JOIN dim.ChartOfAccountsView CAV ON CAV.MainAccountCode     = CA.MainAccountCode
                                         AND CAV.ChartOfAccountsCode = CA.ChartOfAccountsCode
                                         AND CAV.DataConnectionID    = CA.DataConnectionID
                                         AND CAV.Partition           = CA.Partition ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.ChartOfAccounts (
    StageID
  , DataConnectionID
  , ComponentExecutionID
  , Partition
  , ChartOfAccountsCode
  , ChartOfAccountsName
  , ChartOfAccountsDesc
  , MainAccountRecID
  , MainAccountCode
  , AccountName
  , AccountCodeDesc
  , AccountTypeCode
  , AccountTypeDesc
  , AccountCategoryCode
  , AccountCategoryDesc
  , AccountCategoryCodeDesc
  , Balance
  )
  SELECT CAV.StageID
       , CAV.DataConnectionID
       , COALESCE(@component_execution_id, -1)
       , [Partition]
       , CAV.ChartOfAccountsCode
       , CAV.ChartOfAccountsName
       , CAV.ChartOfAccountsDesc
       , CAV.MainAccountRecID
       , CAV.MainAccountCode
       , CAV.AccountName
       , CAV.AccountCodeDesc
       , CAV.AccountTypeCode
       , CAV.AccountTypeDesc
       , CAV.AccountCategoryCode
       , CAV.AccountCategoryDesc
       , CAV.AccountCategoryCodeDesc
       , CAV.Balance
    FROM dim.ChartOfAccountsView CAV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.ChartOfAccounts CA
                       WHERE CAV.MainAccountCode     = CA.MainAccountCode
                         AND CAV.ChartOfAccountsCode = CA.ChartOfAccountsCode
                         AND CAV.DataConnectionID    = CA.DataConnectionID
                         AND CAV.[Partition]         = CA.[Partition]) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
GO



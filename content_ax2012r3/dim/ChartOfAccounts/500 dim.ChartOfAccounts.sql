-- ################################################################################ --
-- #####                         Table: dim.ChartOfAccounts                   ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.ChartOfAccounts', 'T' ;
GO
CREATE TABLE [dim].[ChartOfAccounts]
(
  ChartOfAccountsID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                 BIGINT        NULL
, DataConnectionID        INT           NOT NULL
, ExecutionTimestamp      ROWVERSION    NOT NULL
, ComponentExecutionID    INT           NOT NULL
, Partition               BIGINT        NOT NULL
, ChartOfAccountsCode     BIGINT        NOT NULL --BKey
, ChartOfAccountsName     NVARCHAR(100) NULL
, ChartOfAccountsDesc     NVARCHAR(100) NULL
, MainAccountRecID        BIGINT        NOT NULL --BKey
, MainAccountCode         NVARCHAR(60)  NULL
, AccountName             NVARCHAR(100) NULL
, AccountCodeDesc         NVARCHAR(255) NOT NULL
, AccountTypeCode         INT           NULL
, AccountTypeDesc         NVARCHAR(255) NULL
, AccountCategoryCode     INT           NULL
, AccountCategoryDesc     NVARCHAR(100) NULL
, AccountCategoryCodeDesc NVARCHAR(100) NULL
, Balance                 BIT           NOT NULL
) ;
GO
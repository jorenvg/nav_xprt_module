
-- ################################################################################ --
-- #####                         Table: dim.Customer                          ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.Customer', 'T' ;

CREATE TABLE dim.Customer
(
  CustomerID                          INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                             BIGINT        NULL
, DataConnectionID                    INT           NOT NULL
, CompanyID                           INT           NOT NULL
, Partition                           BIGINT        NOT NULL
, ExecutionTimestamp                  ROWVERSION    NOT NULL
, ComponentExecutionID                INT           NOT NULL
, CustomerAccountNo                   NVARCHAR(512) NULL
, CustomerAccountName                 NVARCHAR(512) NULL
, CustomerAccountNoName               NVARCHAR(512) NULL
, CustomerGroupCode                   NVARCHAR(512) NULL
, CustomerGroupDesc                   NVARCHAR(512) NULL
, CustomerGroupCodeDesc               NVARCHAR(512) NULL
, CustomerClassificationGroupCode     NVARCHAR(512) NULL
, CustomerClassificationGroupDesc     NVARCHAR(512) NULL
, CustomerClassificationGroupCodeDesc NVARCHAR(512) NULL
, CustomerAddress                     NVARCHAR(512) NULL
, CustomerCity                        NVARCHAR(512) NULL
, CustomerStreet                      NVARCHAR(512) NULL
, CustomerZipCode                     NVARCHAR(512) NULL
, CustomerCountryCode                 NVARCHAR(512) NULL
, CustomerCountryDesc                 NVARCHAR(255) NULL
, CustomerCountryCodeDesc             NVARCHAR(512) NULL
, CustomerStatisticsGroupCode         NVARCHAR(512) NULL
, CustomerStatisticsGroupDesc         NVARCHAR(512) NULL
, CustomerStatisticsGroupCodeDesc     NVARCHAR(512) NULL
, CustomerSegmentCode                 NVARCHAR(512) NULL
, CustomerSegmentDesc                 NVARCHAR(512) NULL
, CustomerSegmentCodeDesc             NVARCHAR(512) NULL
, CustomerSubSegmentCode              NVARCHAR(512) NULL
, CustomerSubSegmentDesc              NVARCHAR(512) NULL
, CustomerSubSegmentCodeDesc          NVARCHAR(512) NULL
, CustomerCompanyChainCode            NVARCHAR(512) NULL
, CustomerCompanyChainDesc            NVARCHAR(512) NULL
, CustomerCompanyChainCodeDesc        NVARCHAR(512) NULL
, CustomerSalesDistrictCode           NVARCHAR(512) NULL
, CustomerSalesDistrictDesc           NVARCHAR(512) NULL
, CustomerSalesDistrictCodeDesc       NVARCHAR(512) NULL
, CustomerCommissionGroupCode         NVARCHAR(512) NULL
, CustomerCommissionGroupDesc         NVARCHAR(512) NULL
, CustomerCommissionGroupCodeDesc     NVARCHAR(512) NULL
, CustomerType                        NVARCHAR(512) NULL,
) ;
EXEC dbo.drop_object @object = N'dim.LoadCustomer' -- nvarchar(128)
                   , @type = N'P'                  -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadCustomer
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Customer
     SET      StageID = CUSTV.StageID
            , CustomerAccountName = CUSTV.CustomerAccountName
            , CustomerAccountNoName = CUSTV.CustomerAccountNoName
            , CustomerGroupCode = CUSTV.CustomerGroupCode
            , CustomerGroupDesc = CUSTV.CustomerGroupDesc
            , CustomerGroupCodeDesc = CUSTV.CustomerGroupCodeDesc
            , CustomerClassificationGroupCode = CUSTV.CustomerClassificationGroupCode
            , CustomerClassificationGroupDesc = CUSTV.CustomerClassificationGroupDesc
            , CustomerClassificationGroupCodeDesc = CUSTV.CustomerClassificationGroupCodeDesc
            , CustomerAddress = CUSTV.CustomerAddress
            , CustomerCity = CUSTV.CustomerCity
            , CustomerStreet = CUSTV.CustomerStreet
            , CustomerZipCode = CUSTV.CustomerZipCode
            , CustomerCountryCode = CUSTV.CustomerCountryCode
            , CustomerCountryDesc = CUSTV.CustomerCountryDesc
            , CustomerCountryCodeDesc = CUSTV.CustomerCountryCodeDesc
            , CustomerStatisticsGroupCode = CUSTV.CustomerStatisticsGroupCode
            , CustomerStatisticsGroupDesc = CUSTV.CustomerStatisticsGroupDesc
            , CustomerStatisticsGroupCodeDesc = CUSTV.CustomerStatisticsGroupCodeDesc
            , CustomerSegmentCode = CUSTV.CustomerSegmentCode
            , CustomerSegmentDesc = CUSTV.CustomerSegmentDesc
            , CustomerSegmentCodeDesc = CUSTV.CustomerSegmentCodeDesc
            , CustomerSubSegmentCode = CUSTV.CustomerSubSegmentCode
            , CustomerSubSegmentDesc = CUSTV.CustomerSubSegmentDesc
            , CustomerSubSegmentCodeDesc = CUSTV.CustomerSubSegmentCodeDesc
            , CustomerCompanyChainCode = CUSTV.CustomerCompanyChainCode
            , CustomerCompanyChainDesc = CUSTV.CustomerCompanyChainDesc
            , CustomerCompanyChainCodeDesc = CUSTV.CustomerCompanyChainCodeDesc
            , CustomerSalesDistrictCode = CUSTV.CustomerSalesDistrictCode
            , CustomerSalesDistrictDesc = CUSTV.CustomerSalesDistrictDesc
            , CustomerSalesDistrictCodeDesc = CUSTV.CustomerSalesDistrictCodeDesc
            , CustomerCommissionGroupCode = CUSTV.CustomerCommissionGroupCode
            , CustomerCommissionGroupDesc = CUSTV.CustomerCommissionGroupDesc
            , CustomerCommissionGroupCodeDesc = CUSTV.CustomerCommissionGroupCodeDesc
            , CustomerType = CUSTV.CustomerType
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Customer     CUST
   INNER JOIN dim.CustomerView CUSTV ON CUSTV.CustomerAccountNo = CUST.CustomerAccountNo
                                    AND CUSTV.CompanyID         = CUST.CompanyID
                                    AND CUSTV.Partition         = CUST.Partition
                                    AND CUSTV.DataConnectionID  = CUST.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Customer (
    StageID
  , DataConnectionID
  , CompanyID
  , Partition
  , ComponentExecutionID
  , CustomerAccountNo
  , CustomerAccountName
  , CustomerAccountNoName
  , CustomerGroupCode
  , CustomerGroupDesc
  , CustomerGroupCodeDesc
  , CustomerClassificationGroupCode
  , CustomerClassificationGroupDesc
  , CustomerClassificationGroupCodeDesc
  , CustomerAddress
  , CustomerCity
  , CustomerStreet
  , CustomerZipCode
  , CustomerCountryCode
  , CustomerCountryDesc
  , CustomerCountryCodeDesc
  , CustomerStatisticsGroupCode
  , CustomerStatisticsGroupDesc
  , CustomerStatisticsGroupCodeDesc
  , CustomerSegmentCode
  , CustomerSegmentDesc
  , CustomerSegmentCodeDesc
  , CustomerSubSegmentCode
  , CustomerSubSegmentDesc
  , CustomerSubSegmentCodeDesc
  , CustomerCompanyChainCode
  , CustomerCompanyChainDesc
  , CustomerCompanyChainCodeDesc
  , CustomerSalesDistrictCode
  , CustomerSalesDistrictDesc
  , CustomerSalesDistrictCodeDesc
  , CustomerCommissionGroupCode
  , CustomerCommissionGroupDesc
  , CustomerCommissionGroupCodeDesc
  , CustomerType
  )
  SELECT CUSTV.StageID
       , CUSTV.DataConnectionID
       , CUSTV.CompanyID
       , CUSTV.Partition
       , @component_execution_id
       , CUSTV.CustomerAccountNo
       , CUSTV.CustomerAccountName
       , CUSTV.CustomerAccountNoName
       , CUSTV.CustomerGroupCode
       , CUSTV.CustomerGroupDesc
       , CUSTV.CustomerGroupCodeDesc
       , CUSTV.CustomerClassificationGroupCode
       , CUSTV.CustomerClassificationGroupDesc
       , CUSTV.CustomerClassificationGroupCodeDesc
       , CUSTV.CustomerAddress
       , CUSTV.CustomerCity
       , CUSTV.CustomerStreet
       , CUSTV.CustomerZipCode
       , CUSTV.CustomerCountryCode
       , CUSTV.CustomerCountryDesc
       , CUSTV.CustomerCountryCodeDesc
       , CUSTV.CustomerStatisticsGroupCode
       , CUSTV.CustomerStatisticsGroupDesc
       , CUSTV.CustomerStatisticsGroupCodeDesc
       , CUSTV.CustomerSegmentCode
       , CUSTV.CustomerSegmentDesc
       , CUSTV.CustomerSegmentCodeDesc
       , CUSTV.CustomerSubSegmentCode
       , CUSTV.CustomerSubSegmentDesc
       , CUSTV.CustomerSubSegmentCodeDesc
       , CUSTV.CustomerCompanyChainCode
       , CUSTV.CustomerCompanyChainDesc
       , CUSTV.CustomerCompanyChainCodeDesc
       , CUSTV.CustomerSalesDistrictCode
       , CUSTV.CustomerSalesDistrictDesc
       , CUSTV.CustomerSalesDistrictCodeDesc
       , CUSTV.CustomerCommissionGroupCode
       , CUSTV.CustomerCommissionGroupDesc
       , CUSTV.CustomerCommissionGroupCodeDesc
       , CUSTV.CustomerType
    FROM dim.CustomerView CUSTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Customer CUST
                       WHERE CUSTV.CustomerAccountNo = CUST.CustomerAccountNo
                         AND CUSTV.CompanyID         = CUST.CompanyID
                         AND CUSTV.Partition         = CUST.Partition
                         AND CUSTV.DataConnectionID  = CUST.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
-- ################################################################################ --
-- #####                         Table: dim.Address                           ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.Address', 'T';

CREATE TABLE dim.Address
  (
      AddressID 			INT IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
  	, StageID 				BIGINT NULL
	, DataConnectionID 		INT NOT NULL
	, Partition 			BIGINT NOT NULL
	, ExecutionTimestamp 	ROWVERSION NOT NULL
	, ComponentExecutionID 	INT NULL
    , AddressRecID 			BIGINT NOT NULL
	, LocationCode 			BIGINT NOT NULL
	, AddressCode 			NVARCHAR (1000) NULL
	, CountryCode 			NVARCHAR(30) NULL
	, CountryShortName 		NVARCHAR (1000) NULL
	, CountryLongName 		NVARCHAR (1000) NULL
	, CountryCodeDesc 		NVARCHAR(1000) NULL
	, ZipCode 				NVARCHAR(30) NULL
	, State 				NVARCHAR(30) NULL
	, County 				NVARCHAR(30) NULL
	, City 					NVARCHAR(255) NULL
	, Street 				NVARCHAR(1000) NULL
	, Streetnumber 			NVARCHAR(255) NULL
	, Postbox 				NVARCHAR(255) NULL
	, DistictCode 			BIGINT
	, DistrictDesc 			NVARCHAR(255) NULL
	, DistictCodeDesc 		NVARCHAR(1000) NULL
	, BuildingCompliment 	NVARCHAR(1000) NULL
	, AddressDesc 			NVARCHAR(1000) NULL
	, AddressCodeDesc 		NVARCHAR(1000) NULL
	, IsPostalAddress 		INT NULL

  )
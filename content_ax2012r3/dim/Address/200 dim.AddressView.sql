/****
	DEV-765 Filter to add only include active dates has been removed. This was not taken into account in the Load procedure. 
	Thus this view will add all address, even if the ValidTo-ValidFrom is in the past.
****/

EXEC dbo.drop_object 'dim.AddressView', 'V' ;
GO

CREATE VIEW [dim].[AddressView]
AS
  SELECT ISNULL(A.RECID, -1)                                                        AS AddressRecID      
       , ISNULL(A.LOCATION, -1)                                                     AS LocationCode      
       , ISNULL(NULLIF(A.ADDRESS, ''), 'N/A')                                       AS AddressCode       
       , ISNULL(NULLIF(LL.DESCRIPTION, ''), 'N/A')                                  AS AddressDesc       
       , ISNULL(NULLIF(CONVERT(VARCHAR(255), A.ADDRESS), '') + ' - ', '')             
        + ISNULL(NULLIF(LL.DESCRIPTION, ''), 'N/A')                                 AS AddressCodeDesc 
       , ISNULL(NULLIF(A.COUNTRYREGIONID, ''), 'N/A')                               AS CountryCode       
       , ISNULL(NULLIF(CT.SHORTNAME, ''), 'N/A')                                    AS CountryShortName  
       , ISNULL(NULLIF(CT.LONGNAME, ''), 'N/A')                                     AS CountryLongName   
       , ISNULL(NULLIF(CONVERT(VARCHAR(255), A.COUNTRYREGIONID), '') + ' - ', '')    
       + ISNULL(NULLIF(CT.SHORTNAME, ''), 'N/A')                                    AS CountryCodeDesc 
       , ISNULL(NULLIF(A.ZIPCODE, ''), 'N/A')                                       AS ZipCode           
       , ISNULL(NULLIF(A.STATE, ''), 'N/A')                                         AS State             
       , ISNULL(NULLIF(A.COUNTY, ''), 'N/A')                                        AS County            
       , ISNULL(NULLIF(A.CITY, ''), 'N/A')                                          AS City              
       , ISNULL(NULLIF(A.STREET, ''), 'N/A')                                        AS Street            
       , ISNULL(NULLIF(A.STREETNUMBER, ''), 'N/A')                                  AS Streetnumber      
       , ISNULL(NULLIF(A.POSTBOX, ''), 'N/A')                                       AS Postbox           
       , ISNULL(A.DISTRICT, -1)                                                     AS DistictCode       
       , ISNULL(NULLIF(A.DISTRICTNAME, ''), 'N/A')                                  AS DistrictDesc      
       , ISNULL(NULLIF(CONVERT(VARCHAR(255), A.DISTRICT), '') + ' - ', '')           
        + ISNULL(NULLIF(A.DISTRICTNAME, ''), 'N/A')                                 AS DistictCodeDesc   
       , ISNULL(NULLIF(A.BUILDINGCOMPLIMENT, ''), 'N/A')                            AS BuildingCompliment
       , ISNULL(LL.ISPOSTALADDRESS, -1)                                             AS IsPostalAddress   
       , A.data_connection_id                                                       AS DataConnectionID  
       , A.PARTITION                                                                AS Partition         
       , A.stage_id                                                                 AS StageID           
    FROM stage_ax.LOGISTICSPOSTALADDRESS                        A
    LEFT JOIN stage_ax.LOGISTICSLOCATION                        LL ON A.LOCATION           = LL.RECID
                                                                  AND A.PARTITION          = LL.PARTITION
                                                                  AND A.data_connection_id = LL.data_connection_id
    LEFT JOIN stage_ax.LOGISTICSADDRESSCOUNTRYREGIONTRANSLATION CT ON A.COUNTRYREGIONID    = CT.COUNTRYREGIONID
                                                                  AND A.PARTITION          = CT.PARTITION
                                                                  AND A.data_connection_id = CT.data_connection_id
                                                                  AND CT.LANGUAGEID        = (SELECT language_description FROM meta.current_instance)
   WHERE 1 = 1 ;
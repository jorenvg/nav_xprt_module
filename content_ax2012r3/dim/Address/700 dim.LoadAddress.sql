
EXEC dbo.drop_object @object = N'dim.LoadAddress', -- nvarchar(128)
  @type = N'P', -- nchar(2)
  @debug = 0;
 -- int
GO 

CREATE PROCEDURE [dim].[LoadAddress]
  @component_execution_id INT = -1
, @load_type TINYINT = 1
, @inserted INT = NULL OUTPUT
, @updated INT = NULL OUTPUT
, @deleted INT = NULL OUTPUT
AS
  BEGIN
			
    SET @deleted = 0;
    SET @updated = 0;
    SET @inserted = 0;
  
    UPDATE  dim.Address
    SET    
			  StageID 				      = AV.StageId
			, LocationCode 			    = AV.LocationCode
			, AddressCode 			    = AV.AddressCode
			, CountryCode 			    = AV.CountryCode
			, CountryShortName 		  = AV.CountryShortName
			, CountryLongName 		  = AV.CountryLongName
			, CountryCodeDesc 		  = AV.CountryCodeDesc
			, ZipCode 				      = AV.ZipCode
			, State 				        = AV.State
			, County 				        = AV.County
			, City 					        = AV.City
			, Street 				        = AV.Street
			, Streetnumber 			    = AV.Streetnumber
			, Postbox 				      = AV.Postbox
			, DistictCode 			    = AV.DistictCode
			, DistrictDesc 			    = AV.DistrictDesc
			, DistictCodeDesc 		  = AV.DistictCodeDesc
			, BuildingCompliment 	  = AV.BuildingCompliment
			, AddressDesc 			    = AV.AddressDesc
			, AddressCodeDesc 		  = AV.AddressCodeDesc
			, IsPostalAddress 		  = AV.IsPostalAddress
			, ComponentExecutionID 	= @component_execution_id
     FROM    dim.Address A
    INNER JOIN dim.AddressView AV ON AV.AddressRecID = A.AddressRecID 
								AND AV.DataConnectionID = A.DataConnectionID
								AND AV.Partition = A.Partition;
	
    SELECT  @updated = @@ROWCOUNT;

    INSERT  INTO dim.Address
            (
			  StageID      
			, DataConnectionID
			, ComponentExecutionID
			, Partition
			, AddressRecID
			, LocationCode
			, AddressCode
			, CountryCode
			, CountryShortName
			, CountryLongName
			, CountryCodeDesc
			, ZipCode
			, State
			, County
			, City
			, Street
			, Streetnumber
			, Postbox
			, DistictCode
			, DistrictDesc
			, DistictCodeDesc
			, BuildingCompliment
			, AddressDesc
			, AddressCodeDesc
			, IsPostalAddress

	        )
    SELECT 
			  AV.StageID
			, AV.DataConnectionID
			, @component_execution_id
			, Partition
			, AV.AddressRecID
			, AV.LocationCode
			, AV.AddressCode
			, AV.CountryCode
			, AV.CountryShortName
			, AV.CountryLongName
			, AV.CountryCodeDesc
			, AV.ZipCode
			, AV.State
			, AV.County
			, AV.City
			, AV.Street
			, AV.Streetnumber
			, AV.Postbox
			, AV.DistictCode
			, AV.DistrictDesc
			, AV.DistictCodeDesc
			, AV.BuildingCompliment
			, AV.AddressDesc
			, AV.AddressCodeDesc
			, AV.IsPostalAddress
          
    FROM    dim.AddressView AV
    WHERE   NOT EXISTS ( SELECT *
                         FROM   dim.Address A
                         WHERE  AV.AddressRecID = A.AddressRecID
								AND AV.Partition = A.Partition
                                AND AV.DataConnectionID = A.DataConnectionID );

    SELECT  @inserted = @@ROWCOUNT;
  END; 
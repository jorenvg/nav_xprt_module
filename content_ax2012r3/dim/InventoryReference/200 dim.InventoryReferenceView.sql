EXEC dbo.drop_object 'dim.InventoryReferenceView', 'V' ;
GO

CREATE VIEW [dim].[InventoryReferenceView]
AS
  SELECT      ito.RECID                                   AS Recid
            , ISNULL(NULLIF(ito.REFERENCEID, ''), 'N/A')  AS ReferenceCode
            , ISNULL(ito.REFERENCECATEGORY, -1)           AS ReferenceCategoryCode
            , ISNULL(NULLIF(enum.Description, ''), 'N/A') AS ReferenceCategoryDesc
            , ISNULL(NULLIF(ito.ITEMID, ''), 'N/A')       AS ItemNumber
            , ito.PARTITION                               AS Partition
            , ito.data_connection_id                      AS DataConnectionID
            , ito.stage_id                                AS StageID
    FROM      stage_ax.INVENTTRANSORIGIN AS ito
    LEFT JOIN meta.enumerations          AS enum ON ito.REFERENCECATEGORY  = enum.OptionID
                                                AND ito.data_connection_id = enum.DataConnectionID
                                                AND enum.TableName         = 'InventTransOrigin'
                                                AND enum.ColumnName        = 'ReferenceCategory' ;

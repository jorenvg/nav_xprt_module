EXEC dbo.drop_object @object = N'dim.LoadInventoryReference' -- nvarchar(128)
                   , @type = N'P'                            -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadInventoryReference]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.InventoryReference
     SET      ReferenceCode = vw.ReferenceCode
            , ReferenceCategoryCode = vw.ReferenceCategoryCode
            , ReferenceCategoryDesc = vw.ReferenceCategoryDesc
            , ComponentExecutionID = @component_execution_id
            , ItemNumber = vw.ItemNumber
            , StageId = vw.StageId
    FROM      dim.InventoryReference     AS tbl
   INNER JOIN dim.InventoryReferenceView AS vw ON vw.Recid            = tbl.Recid
                                              AND vw.Partition        = tbl.Partition
                                              AND vw.DataConnectionID = tbl.DataConnectionID ;
  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.InventoryReference (
    DataConnectionID, ComponentExecutionID, Recid, ReferenceCode, ReferenceCategoryCode, ReferenceCategoryDesc, ItemNumber, Partition, StageId
  )
  SELECT vw.DataConnectionID
       , @component_execution_id
       , vw.Recid
       , vw.ReferenceCode
       , vw.ReferenceCategoryCode
       , vw.ReferenceCategoryDesc
       , vw.ItemNumber
       , vw.Partition
       , vw.StageId
    FROM dim.InventoryReferenceView AS vw
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.InventoryReference AS tbl
                       WHERE vw.Recid            = tbl.Recid
                         AND vw.Partition        = tbl.Partition
                         AND vw.DataConnectionID = tbl.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
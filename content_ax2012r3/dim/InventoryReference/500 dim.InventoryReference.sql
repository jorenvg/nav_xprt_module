EXEC dbo.drop_object 'dim.InventoryReference', 'T' ;

CREATE TABLE dim.InventoryReference
(
  InventoryReferenceID  INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, Recid                 BIGINT        NOT NULL
, DataConnectionID      INT           NOT NULL
, ExecutionTimestamp    ROWVERSION    NOT NULL
, ComponentExecutionID  INT           NOT NULL
, ItemNumber            NVARCHAR(50)  NOT NULL
, ReferenceCode         NVARCHAR(30)  NOT NULL
, ReferenceCategoryCode INT           NOT NULL
, ReferenceCategoryDesc NVARCHAR(128) NOT NULL
, Partition             BIGINT        NOT NULL
, StageID               INT           NOT NULL
) ;

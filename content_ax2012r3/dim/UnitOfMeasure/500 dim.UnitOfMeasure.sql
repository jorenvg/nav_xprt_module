
-- ################################################################################ --
-- #####                         Table: dim.UnitOfMeasure                     ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.UnitOfMeasure', 'T' ;

CREATE TABLE dim.UnitOfMeasure
(
  UnitOfMeasureID            INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID                    BIGINT        NULL
, DataConnectionID           INT           NOT NULL
, ExecutionTimestamp         ROWVERSION    NOT NULL
, ComponentExecutionID       INT           NOT NULL
, Partition                  BIGINT        NOT NULL
, UnitOfMeasureCode          NVARCHAR(50)  NULL
, UnitOfMeasureDesc          NVARCHAR(100) NULL
, UnitOfMeasureCodeDesc      NVARCHAR(255) NULL
, UnitOfMeasureClassCode     INT           NULL
, UnitOfMeasureClassDesc     NVARCHAR(100) NULL
, UnitOfMeasureClassCodeDesc NVARCHAR(128) NULL
) ;
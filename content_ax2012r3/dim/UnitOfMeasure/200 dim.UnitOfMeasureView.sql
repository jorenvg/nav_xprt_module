EXEC dbo.drop_object 'dim.UnitOfMeasureView', 'V' ;
GO

CREATE VIEW [dim].[UnitOfMeasureView]
AS
  SELECT            ISNULL(NULLIF(UOM.SYMBOL, ''), 'N/A')                                                                                       AS UnitOfMeasureCode
                  , ISNULL(NULLIF(UMT.DESCRIPTION, ''), 'N/A')                                                                                  AS UnitOfMeasureDesc
                  , ISNULL(NULLIF(UOM.SYMBOL, '') + ' - ', '') + ISNULL(NULLIF(UMT.DESCRIPTION, ''), 'N/A')                                     AS UnitOfMeasureCodeDesc
                  , ISNULL(NULLIF(UOM.UNITOFMEASURECLASS, -1), -1)                                                                              AS UnitOfMeasureClassCode
                  , ISNULL(NULLIF(ENUM.DESCRIPTION, ''), 'N/A')                                                                                 AS UnitOfMeasureClassDesc
                  , ISNULL(NULLIF(CONVERT(VARCHAR(255), UOM.UNITOFMEASURECLASS), '') + ' - ', '') + ISNULL(NULLIF(ENUM.DESCRIPTION, ''), 'N/A') AS UnitOfMeasureClassCodeDesc
                  , UOM.data_connection_id                                                                                                      AS DataConnectionID
                  , UOM.Partition                                                                                                               AS Partition
                  , UOM.stage_id                                                                                                                AS StageID
    FROM            stage_ax.UNITOFMEASURE            AS UOM
    LEFT OUTER JOIN stage_ax.UNITOFMEASURETRANSLATION UMT ON UOM.RECID               = UMT.UNITOFMEASURE
                                                         AND UMT.LANGUAGEID          = (SELECT language_description FROM meta.current_instance)
    LEFT OUTER JOIN meta.enumerations                 ENUM ON UOM.UNITOFMEASURECLASS = ENUM.OptionID
                                                          AND ENUM.TableName         = 'UNITOFMEASURE'
                                                          AND ENUM.ColumnName        = 'UNITOFMEASURECLASS' ;







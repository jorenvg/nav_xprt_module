EXEC Dbo.Drop_Object 'dim.ConsumptionOtherView', 'V' ;
GO

CREATE VIEW [dim].ConsumptionOtherView
AS
  SELECT            DISTINCT
                    PCT.Company_Id                                   AS CompanyID
                  , PCT.Data_Connection_Id                           AS DataConnectionId
                  , PCT.Partition                                    AS Partition
                  , COALESCE(NULLIF(CT_ENUM.Description, ''), 'N/A') AS ConsumptionType
                  , COALESCE(NULLIF(ST_ENUM.Description, ''), 'N/A') AS SourceType
                  , COALESCE(NULLIF(PCT.[Resource_], ''), 'N/A')     AS ResourceCode
                  , 'N/A'                                            AS ResourceDescription
                  , COALESCE(NULLIF(PCT.[Resource_], ''), 'N/A')     AS ResourceCodeDescription
                  , 'N/A'                                            AS GroupCode
                  , 'N/A'                                            AS GroupDescription
                  , 'N/A'                                            AS GroupCodeDescription
                  , 'N/A'                                            AS ProductGroupCode
                  , 'N/A'                                            AS ProductGroupDescription
                  , 'N/A'                                            AS ProductGroupCodeDescription
                  , 'N/A'                                            AS ResourceType
    FROM            Stage_Ax.ProdCalcTrans AS PCT
    LEFT OUTER JOIN Meta.Enumerations      CT_ENUM ON CT_ENUM.TableName  = 'Consumption'
                                                  AND CT_ENUM.ColumnName = 'CalcType'
                                                  AND PCT.Calctype       = CT_ENUM.OptionId
    LEFT OUTER JOIN Meta.Enumerations      ST_ENUM ON ST_ENUM.TableName  = 'Consumption'
                                                  AND ST_ENUM.ColumnName = 'SourceType'
                                                  AND ST_ENUM.OptionId   = 2
   WHERE            1 = 1
     AND            PCT.CalcType NOT IN ( 0, 1, 2, 3, 5, 6 ) ; -- Consumption --> All Other 
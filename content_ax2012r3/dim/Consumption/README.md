# Developing the Consumption dimension

The following scripts have been provided for the consuption dimension. 
the script should be checked and validated, if the data is selected correctly.

- Each field should be surrounded by the `COALESCE(ISNULL())` expression.
- The description field place holder expression should be defined.
- Drop and create view should be added to these scriptions

These scripts should be included in the views:

- dim.ConsumptionItemView
- dim.ConsumtionWorkcenterView

During the load procedure each script should be updated/inserted seperatly. See **help.Inventory** component for an example on how it is implemented. 

After development this readme document can be removed.
EXEC Dbo.Drop_Object 'dim.ConsumptionItemView', 'V' ;
GO

CREATE VIEW [dim].[ConsumptionItemView]
AS
  SELECT            DISTINCT
                    PCT.company_id                                                                             AS CompanyID
                  , PCT.data_connection_id                                                                     AS DataConnectionId
                  , PCT.Partition                                                                              AS Partition
                  , COALESCE(NULLIF(CT_ENUM.Description, ''), 'N/A')                                           AS ConsumptionType
                  , COALESCE(NULLIF(ST_ENUM.Description, ''), 'N/A')                                           AS SourceType
                  , COALESCE(NULLIF(PCT.[Resource_], ''), 'N/A')                                               AS ResourceCode
                  , COALESCE(NULLIF(ERPT.Name, ''), 'N/A')                                                     AS ResourceDescription
                  , COALESCE(NULLIF(PCT.[Resource_], ''), 'N/A') + COALESCE(' - ' + NULLIF(ERPT.NAME, ''), '') AS ResourceCodeDescription
                  , COALESCE(NULLIF(IIGI.ITEMGROUPID, ''), 'N/A')                                              AS GroupCode        -- Item Group
                  , COALESCE(NULLIF(IIG.Name, ''), 'N/A')                                                      AS GroupDescription
                  , COALESCE(NULLIF(IIGI.ITEMGROUPID, ''), 'N/A') + COALESCE(' - ' + NULLIF(IIG.NAME, ''), '') AS GroupCodeDescription
                  , COALESCE(NULLIF(IT.PRODGROUPID, ''), 'N/A')                                                AS ProductGroupCode -- Production Group
                  , COALESCE(NULLIF(PG.Name, ''), 'N/A')                                                       AS ProductGroupDescription
                  , COALESCE(NULLIF(IT.PRODGROUPID, ''), 'N/A') + COALESCE(' - ' + NULLIF(PG.NAME, ''), '')    AS ProductGroupCodeDescription
                  , 'N/A'                                                                                      AS ResourceType
    FROM            stage_ax.ProdCalcTrans                                   AS PCT
    LEFT OUTER JOIN stage_ax.INVENTTABLE                                     AS IT ON PCT.[RESOURCE_] = IT.ITEMID
                                                                                  AND PCT.CALCTYPE IN ( 1, 2, 3 )
                                                                                  AND PCT.company_id = IT.company_id
                                                                                  AND PCT.data_connection_id = IT.data_connection_id
    LEFT OUTER JOIN stage_ax.ECORESPRODUCT                                   AS ERP ON IT.Product = ERP.RecID
                                                                                   AND ERP.PRODUCTTYPE = 1
                                                                                   AND PCT.data_connection_id = ERP.data_connection_id
   CROSS JOIN       (SELECT Language_Description FROM meta.current_instance) AS CI
    LEFT OUTER JOIN stage_ax.EcoRespRoducttranslation AS ERPT ON ERP.RECID                 = ERPT.PRODUCT
                                                             AND ERPT.LANGUAGEID           = CI.Language_Description
                                                             AND ERP.data_connection_id    = ERPT.data_connection_id
    LEFT OUTER JOIN stage_ax.INVENTITEMGROUPITEM      AS IIGI ON IT.ITEMID                 = IIGI.ITEMID
                                                             AND PCT.DATAAREAID            = IIGI.ITEMDATAAREAID
                                                             AND PCT.data_connection_id    = IIGI.data_connection_id
    LEFT OUTER JOIN stage_ax.INVENTITEMGROUP          AS IIG ON IIGI.ITEMGROUPID           = IIG.ITEMGROUPID
                                                            AND IIGI.ITEMGROUPDATAAREAID   = IIG.DATAAREAID
                                                            AND IIGI.data_connection_id    = IIG.data_connection_id
    LEFT OUTER JOIN stage_ax.PRODGROUP                AS PG ON IT.PRODGROUPID              = PG.PRODGROUPID
                                                           AND IT.DataAreaId               = PG.dataareaid
                                                           AND IT.data_connection_id       = PG.data_connection_id
    LEFT OUTER JOIN meta.enumerations                 AS ST_ENUM ON ST_ENUM.TableName      = 'Consumption'
                                                                AND ST_ENUM.ColumnName     = 'SourceType'
                                                                AND ST_ENUM.OptionID       = 0
                                                                AND PCT.data_connection_id = ST_ENUM.dataconnectionid
    LEFT OUTER JOIN meta.enumerations                 AS CT_ENUM ON CT_ENUM.TableName      = 'Consumption'
                                                                AND CT_ENUM.ColumnName     = 'CalcType'
                                                                AND PCT.CALCTYPE           = CT_ENUM.OptionID
                                                                AND PCT.data_connection_id = CT_ENUM.dataconnectionid
   WHERE            1 = 1
     AND            PCT.CALCTYPE IN ( 1, 2, 3 ) ; -- Consumption --> Item, Bom



EXEC Dbo.Drop_Object @Object = N'dim.LoadConsumption'
-- nvarchar(128)
                   , @Type = N'P'
-- nchar(2)
                   , @Debug = 0 ;
-- int
GO


CREATE PROCEDURE [dim].LoadConsumption
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @Deleted = 0 ;
  SET @Updated = 0 ;
  SET @Inserted = 0 ;

  /****************************************************************************************************
    Functionality:  This section will execute the update of Dim ConsumptionItemView

    Date            Changed by      Ticket/Change       Description

*****************************************************************************************************/


  UPDATE      Dim.Consumption
     SET      ComponentExecutionId = @component_execution_id
            , SourceType = CIV.SourceType
            , ResourceDescription = CIV.ResourceDescription
            , ResourceCodeDescription = CIV.ResourceCodeDescription
            , GroupCode = CIV.GroupCode
            , GroupDescription = CIV.GroupDescription
            , GroupCodeDescription = CIV.GroupCodeDescription
            , ProductGroupCode = CIV.ProductGroupCode
            , ProductGroupDescription = CIV.ProductGroupDescription
            , ProductGroupCodeDescription = CIV.ProductGroupCodeDescription
    FROM      Dim.Consumption         AS C
   INNER JOIN Dim.ConsumptionItemView AS CIV ON CIV.ConsumptionType = C.ConsumptionType
                                            AND CIV.ResourceCode    = C.ResourceCode
                                            AND C.CompanyId         = CIV.CompanyId
                                            AND C.Partition         = CIV.Partition
                                            AND C.DataConnectionId  = CIV.DataConnectionId ;

  SELECT @Updated = @@RowCount ;

  INSERT INTO Dim.Consumption (
    DataConnectionId
  , CompanyId
  , Partition
  , ComponentExecutionId
  , ConsumptionType
  , SourceType
  , ResourceCode
  , ResourceDescription
  , ResourceCodeDescription
  , GroupCode
  , GroupDescription
  , GroupCodeDescription
  , ProductGroupCode
  , ProductGroupDescription
  , ProductGroupCodeDescription
  , ResourceType
  )
  SELECT CIV.DataConnectionId
       , CIV.CompanyId
       , CIV.Partition
       , @Component_Execution_Id
       , CIV.ConsumptionType
       , CIV.SourceType
       , CIV.ResourceCode
       , CIV.ResourceDescription
       , CIV.ResourceCodeDescription
       , CIV.GroupCode
       , CIV.GroupDescription
       , CIV.GroupCodeDescription
       , CIV.ProductGroupCode
       , CIV.ProductGroupDescription
       , CIV.ProductGroupCodeDescription
       , CIV.ResourceType
    FROM Dim.ConsumptionItemView CIV
   WHERE NOT EXISTS ( SELECT *
                        FROM Dim.Consumption C
                       WHERE C.ConsumptionType  = CIV.ConsumptionType
                         AND C.CompanyId        = CIV.CompanyId
                         AND C.Partition        = CIV.Partition
                         AND C.DataConnectionId = CIV.DataConnectionId) ;

  SELECT @Inserted = @@RowCount ;

  /****************************************************************************************************
    Functionality:  This section will execute the update of Dim ConsumptionWorkCenterView

    Date            Changed by      Ticket/Change       Description

*****************************************************************************************************/

  UPDATE      Dim.Consumption
     SET      ComponentExecutionId = @Component_Execution_Id
    FROM      Dim.Consumption               AS C
   INNER JOIN Dim.ConsumptionWorkCenterView AS CWCV ON CWCV.ConsumptionType  = C.ConsumptionType
                                                   AND CWCV.ResourceCode     = C.ResourceCode
                                                   AND CWCV.CompanyId        = C.CompanyId
                                                   AND CWCV.Partition        = C.Partition
                                                   AND CWCV.DataConnectionId = C.DataConnectionId ;

  SELECT @Updated = +@@RowCount ;

  INSERT INTO Dim.Consumption (
    DataConnectionId
  , CompanyId
  , Partition
  , ComponentExecutionId
  , ConsumptionType
  , SourceType
  , ResourceCode
  , ResourceDescription
  , ResourceCodeDescription
  , GroupCode
  , GroupDescription
  , GroupCodeDescription
  , ProductGroupCode
  , ProductGroupDescription
  , ProductGroupCodeDescription
  , ResourceType
  )
  SELECT CWCV.DataConnectionId
       , CWCV.CompanyId
       , CWCV.Partition
       , @Component_Execution_Id
       , CWCV.ConsumptionType
       , CWCV.SourceType
       , CWCV.ResourceCode
       , CWCV.ResourceDescription
       , CWCV.ResourceCodeDescription
       , CWCV.GroupCode
       , CWCV.GroupDescription
       , CWCV.GroupCodeDescription
       , CWCV.ProductGroupCode
       , CWCV.ProductGroupDescription
       , CWCV.ProductGroupCodeDescription
       , CWCV.ResourceType
    FROM Dim.ConsumptionWorkCenterView CWCV
   WHERE NOT EXISTS ( SELECT *
                        FROM Dim.Consumption C
                       WHERE C.ConsumptionType  = CWCV.ConsumptionType
                         AND C.CompanyId        = CWCV.CompanyId
                         AND C.Partition        = CWCV.Partition
                         AND C.DataConnectionId = CWCV.DataConnectionId) ;

  SELECT @Inserted = +@@RowCount ;


  /****************************************************************************************************
    Functionality:  This section will execute the update of Dim ConsumptionOtherView

    Date            Changed by      Ticket/Change       Description

*****************************************************************************************************/

  UPDATE      Dim.Consumption
     SET      ComponentExecutionId = @Component_Execution_Id
    FROM      Dim.Consumption          AS C
   INNER JOIN Dim.ConsumptionOtherView AS COV ON COV.ConsumptionType  = C.ConsumptionType
                                             AND COV.ResourceCode     = C.ResourceCode
                                             AND COV.CompanyId        = C.CompanyId
                                             AND COV.Partition        = C.Partition
                                             AND COV.DataConnectionId = C.DataConnectionId ;

  SELECT @Updated = +@@RowCount ;

  INSERT INTO Dim.Consumption (
    DataConnectionId
  , CompanyId
  , Partition
  , ComponentExecutionId
  , ConsumptionType
  , SourceType
  , ResourceCode
  , ResourceDescription
  , ResourceCodeDescription
  , GroupCode
  , GroupDescription
  , GroupCodeDescription
  , ProductGroupCode
  , ProductGroupDescription
  , ProductGroupCodeDescription
  , ResourceType
  )
  SELECT COV.DataConnectionId
       , COV.CompanyId
       , COV.Partition
       , @Component_Execution_Id
       , COV.ConsumptionType
       , COV.SourceType
       , COV.ResourceCode
       , COV.ResourceDescription
       , COV.ResourceCodeDescription
       , COV.GroupCode
       , COV.GroupDescription
       , COV.GroupCodeDescription
       , COV.ProductGroupCode
       , COV.ProductGroupDescription
       , COV.ProductGroupCodeDescription
       , COV.ResourceType
    FROM Dim.ConsumptionOtherView COV
   WHERE NOT EXISTS ( SELECT 1
                        FROM Dim.Consumption C
                       WHERE C.ConsumptionType  = COV.ConsumptionType
                         AND C.CompanyId        = COV.CompanyId
                         AND C.Partition        = COV.Partition
                         AND C.DataConnectionId = COV.DataConnectionId) ;

  SELECT @Inserted = +@@RowCount ;

END ;


EXEC dbo.drop_object @object = N'dim.LoadPostingType' -- nvarchar(128)
                   , @type = N'P'                     -- nchar(2)
                   , @debug = 0 ;                     -- int
GO

CREATE PROCEDURE [dim].[LoadPostingType]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.PostingType
     SET      PostingTypeDesc = PTV.PostingTypeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.PostingType     PT
   INNER JOIN dim.PostingTypeView PTV ON PTV.PostingTypeCode  = PT.PostingTypeCode
                                     AND PTV.DataConnectionID = PT.DataConnectionID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.PostingType (
    DataConnectionID, ComponentExecutionID, PostingTypeCode, PostingTypeDesc
  )
  SELECT PTV.DataConnectionID
       , COALESCE(@component_execution_id, -1)
       , PTV.PostingTypeCode
       , PTV.PostingTypeDesc
    FROM dim.PostingTypeView PTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.PostingType PT
                       WHERE PTV.PostingTypeCode  = PT.PostingTypeCode
                         AND PTV.DataConnectionID = PT.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
GO




-- ################################################################################ --
-- #####                         Table: dim.PostingType                       ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.PostingType', 'T' ;
GO
CREATE TABLE [dim].[PostingType]
(
  PostingTypeID        INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, PostingTypeCode      INT           NOT NULL
, PostingTypeDesc      NVARCHAR(255) NULL
) ;
GO
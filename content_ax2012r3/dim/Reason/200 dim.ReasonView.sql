EXEC dbo.drop_object 'dim.ReasonView', 'V' ;
GO

CREATE VIEW [dim].[ReasonView]
AS
  SELECT      ISNULL(NULLIF(RT.REASON, ''), 'N/A')         AS ReasonCode
            , ISNULL(NULLIF(RT.DESCRIPTION, ''), 'N/A')    AS ReasonDesc
            , ISNULL(NULLIF(RTR.REASONCOMMENT, ''), 'N/A') AS AdditionalDesc
            , ISNULL(RTR.RECID, -1)                        AS ReasonRefRecId
            , RT.company_id                                AS CompanyID
            , RT.data_connection_id                        AS DataConnectionID
            , RT.Partition                                 AS Partition
            , RT.stage_id                                  AS StageID
    FROM      stage_ax.REASONTABLE    AS RT
    LEFT JOIN stage_ax.REASONTABLEREF RTR ON RT.REASON             = RTR.REASON
                                         AND RT.PARTITION          = RTR.PARTITION
                                         AND RT.company_id         = RTR.company_id
                                         AND RT.data_connection_id = RTR.data_connection_id ;


EXEC dbo.drop_object 'dim.Reason', 'T' ;

CREATE TABLE dim.Reason
(
  ReasonID             INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, StageID              BIGINT        NULL
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, CompanyID            INT           NOT NULL
, Partition            BIGINT        NOT NULL
, ReasonCode           NVARCHAR(30)  NOT NULL
, ReasonDesc           NVARCHAR(100) NOT NULL
, AdditionalDesc       NVARCHAR(128) NOT NULL
, ReasonRefRecId       BIGINT        NOT NULL
) ;
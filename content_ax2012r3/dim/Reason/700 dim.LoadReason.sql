EXEC dbo.drop_object @object = N'dim.LoadReason' -- nvarchar(128)
                   , @type = N'P'                -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].[LoadReason]
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.Reason
     SET      StageID = RV.StageID
            , ReasonDesc = RV.ReasonDesc
            , AdditionalDesc = RV.AdditionalDesc
            , ReasonCode = RV.ReasonCode
            , ComponentExecutionID = @component_execution_id
    FROM      dim.Reason     R
   INNER JOIN dim.ReasonView RV ON RV.ReasonRefRecId   = R.ReasonRefRecId
                               AND RV.DataConnectionID = R.DataConnectionID
                               AND RV.Partition        = R.Partition
                               AND RV.CompanyID        = R.CompanyID ;

  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.Reason (
    StageID, DataConnectionID, ComponentExecutionID, CompanyID, Partition, ReasonCode, ReasonDesc, AdditionalDesc, ReasonRefRecId
  )
  SELECT RV.StageID
       , RV.DataConnectionID
       , @component_execution_id
       , RV.CompanyID
       , RV.Partition
       , RV.ReasonCode
       , RV.ReasonDesc
       , RV.AdditionalDesc
       , RV.ReasonRefRecId
    FROM dim.ReasonView RV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.Reason R
                       WHERE RV.ReasonRefRecId   = R.ReasonRefRecId
                         AND RV.DataConnectionID = R.DataConnectionID
                         AND RV.Partition        = R.Partition
                         AND RV.CompanyID        = R.CompanyID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
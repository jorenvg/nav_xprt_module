EXEC dbo.drop_object 'dim.ClosedPeriodView', 'V' ;
GO
CREATE VIEW [dim].[ClosedPeriodView]
AS
  SELECT ELT.OptionID    AS ClosedPeriodCode
       , ELT.Description AS ClosedPeriodDesc
       , ELT.DataConnectionID
    FROM meta.Enumerations AS ELT
   WHERE ELT.TableName  = 'LedgerTrans'
     AND ELT.ColumnName = 'ClosedPeriodDesc' ;
EXEC dbo.drop_object 'dim.SalesPackingSlipView', 'V' ;
GO

CREATE VIEW [dim].[SalesPackingSlipView]
AS
  SELECT DISTINCT -- BK of the dimension is not unique in the table CUSTPACKINGSLIPJOUR
         PACKINGSLIPID      AS SalesPackingSlip
       , data_connection_id AS DataConnectionID
       , company_id         AS CompanyID
       , PARTITION          AS Partition
    FROM stage_ax.CUSTPACKINGSLIPJOUR ;
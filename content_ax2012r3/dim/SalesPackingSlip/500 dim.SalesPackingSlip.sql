EXEC dbo.drop_object 'dim.SalesPackingSlip', 'T' ;

CREATE TABLE dim.SalesPackingSlip
(
  SalesPackingSlipID   INT          IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT          NOT NULL
, ExecutionTimestamp   ROWVERSION   NOT NULL
, ComponentExecutionID INT          NOT NULL
, CompanyID            INT          NOT NULL
, Partition            BIGINT       NOT NULL
, SalesPackingSlip     NVARCHAR(50) NOT NULL
) ;
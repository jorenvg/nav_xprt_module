
-- ################################################################################ --
-- #####                         Table: dim.PurchaseType                      ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.PurchaseType', 'T' ;

CREATE TABLE dim.PurchaseType
(
  PurchaseTypeID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, PurchaseTypeCode     INT           NOT NULL
, PurchaseTypeDesc     NVARCHAR(50)  NOT NULL
, PurchaseTypeCodeDesc NVARCHAR(100) NULL
) ;
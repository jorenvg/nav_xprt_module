EXEC dbo.drop_object @object = N'dim.LoadPurchaseType' -- nvarchar(128)
                   , @type = N'P'                      -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadPurchaseType
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  UPDATE      dim.PurchaseType
     SET      PurchaseTypeDesc = PTV.PurchaseTypeDesc
            , PurchaseTypeCodeDesc = PTV.PurchaseTypeCodeDesc
            , ComponentExecutionID = @component_execution_id
    FROM      dim.PurchaseType     PT
   INNER JOIN dim.PurchaseTypeView PTV ON PTV.PurchaseTypeCode = PT.PurchaseTypeCode
                                      AND PTV.DataConnectionID = PT.DataConnectionID ;


  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO dim.PurchaseType (
    DataConnectionID, ComponentExecutionID, PurchaseTypeCode, PurchaseTypeDesc, PurchaseTypeCodeDesc
  )
  SELECT PTV.DataConnectionID
       , @component_execution_id
       , PTV.PurchaseTypeCode
       , PTV.PurchaseTypeDesc
       , PTV.PurchaseTypeCodeDesc
    FROM dim.PurchaseTypeView PTV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.PurchaseType PT
                       WHERE PTV.PurchaseTypeCode = PT.PurchaseTypeCode
                         AND PTV.DataConnectionID = PT.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
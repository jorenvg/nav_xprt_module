EXEC dbo.drop_object 'dim.PurchaseTypeView', 'V' ;
GO

CREATE VIEW [dim].[PurchaseTypeView]
AS
  SELECT ISNULL(NULLIF(ENUM.OptionID, -1), -1)                                                                              AS PurchaseTypeCode
       , ISNULL(NULLIF(ENUM.DESCRIPTION, ''), 'N/A')                                                                        AS PurchaseTypeDesc
       , ISNULL(NULLIF(CONVERT(VARCHAR(255), ENUM.OptionID), '') + ' - ', '') + ISNULL(NULLIF(ENUM.DESCRIPTION, ''), 'N/A') AS PurchaseTypeCodeDesc
       , ENUM.DataConnectionID                                                                                              AS DataConnectionID
    FROM meta.enumerations AS ENUM
   WHERE ENUM.TableName  = 'PURCHTABLE'
     AND ENUM.ColumnName = 'PURCHASETYPE' ;


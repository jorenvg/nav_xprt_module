EXEC dbo.drop_object @object = N'dim.LoadPurchasePackingSlip' -- nvarchar(128)
                   , @type = N'P'                             -- nchar(2)
                   , @debug = 0 ;
-- int
GO

CREATE PROCEDURE [dim].LoadPurchasePackingSlip
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  INSERT INTO dim.PurchasePackingSlip (
    DataConnectionID, CompanyID, Partition, ComponentExecutionID, PurchasePackingSlip
  )
  SELECT PPSV.DataConnectionID
       , PPSV.CompanyID
       , PPSV.Partition
       , @component_execution_id
       , PPSV.PurchasePackingSlip
    FROM dim.PurchasePackingSlipView AS PPSV
   WHERE NOT EXISTS ( SELECT *
                        FROM dim.PurchasePackingSlip AS PPS
                       WHERE PPSV.PurchasePackingSlip = PPS.PurchasePackingSlip
                         AND PPSV.CompanyID           = PPS.CompanyID
                         AND PPSV.Partition           = PPS.Partition
                         AND PPSV.DataConnectionID    = PPS.DataConnectionID) ;

  SELECT @inserted = @@ROWCOUNT ;
END ;
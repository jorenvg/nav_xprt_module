EXEC dbo.drop_object 'dim.PurchasePackingSlipView', 'V' ;
GO

CREATE VIEW [dim].[PurchasePackingSlipView]
AS
  SELECT DISTINCT -- BK of the dimension is not unique in the table VENDPACKINGSLIPJOUR
         VPS.PACKINGSLIPID      AS PurchasePackingSlip
       , VPS.company_id         AS CompanyID
       , VPS.data_connection_id AS DataConnectionID
       , VPS.PARTITION          AS Partition
    FROM stage_ax.VENDPACKINGSLIPJOUR AS VPS ;



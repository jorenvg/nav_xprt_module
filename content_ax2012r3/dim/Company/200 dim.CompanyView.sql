EXEC dbo.drop_object 'dim.CompanyView', 'V' ;
GO

CREATE VIEW dim.CompanyView
AS
  SELECT      c.company_id                          AS CompanyID
            , c.data_connection_id                  AS DataConnectionID
            , c.partition                           AS Partition
            , COALESCE(c.code, 'N/A')               AS Code
            , COALESCE(da.NAME, 'N/A')              AS Name
            , COALESCE(l.ACCOUNTINGCURRENCY, 'N/A') AS AccountingCurrency
    FROM      meta.companies    AS c
    LEFT JOIN stage_ax.DATAAREA da ON c.code      = da.ID
                                  AND c.partition = da.PARTITION
    LEFT JOIN stage_ax.LEDGER   l ON c.code       = l.NAME
                                 AND c.partition  = l.PARTITION
   WHERE      c.is_virtual = 0 ; -- only inlude physical companies ;


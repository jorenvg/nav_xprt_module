EXEC dbo.drop_object 'dim.Posted', 'T' ;

CREATE TABLE dim.Posted
(
  PostedID             INT        IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT        NOT NULL
, ExecutionTimestamp   ROWVERSION NOT NULL
, ComponentExecutionID INT        NOT NULL
, PostedCode           INT        NOT NULL
, PostedDesc           NVARCHAR(255)
) ;
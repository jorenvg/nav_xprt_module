EXEC dbo.drop_object 'dim.PostedView', 'V' ;
GO

CREATE VIEW [dim].[PostedView]
AS
  SELECT ISNULL(OptionID, -1)                     AS PostedCode
       , ISNULL(NULLIF([Description], ''), 'N/A') AS PostedDesc
       , DataConnectionID
    FROM meta.enumerations
   WHERE TableName  = 'InventTrans'
     AND ColumnName = 'Posted' ;

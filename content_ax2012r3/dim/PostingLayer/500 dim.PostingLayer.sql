
-- ################################################################################ --
-- #####                         Table: dim.PostingLayer                      ##### --
-- ################################################################################ --
EXEC dbo.drop_object 'dim.PostingLayer', 'T' ;
GO
CREATE TABLE [dim].[PostingLayer]
(
  PostingLayerID       INT           IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
, DataConnectionID     INT           NOT NULL
, ExecutionTimestamp   ROWVERSION    NOT NULL
, ComponentExecutionID INT           NOT NULL
, PostingLayerCode     INT           NOT NULL
, PostingLayerDesc     NVARCHAR(255) NULL
) ;
GO
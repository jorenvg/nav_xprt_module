EXEC dbo.drop_object 'dim.PostingLayerView', 'V' ;
GO
CREATE VIEW [dim].[PostingLayerView]
AS
  SELECT ELT.OptionID    AS PostingLayerCode
       , ELT.Description AS PostingLayerDesc
       , ELT.DataConnectionID
    FROM meta.Enumerations AS ELT
   WHERE ELT.TableName  = 'GeneralJournalEntry'
     AND ELT.ColumnName = 'PostingLayer' ;
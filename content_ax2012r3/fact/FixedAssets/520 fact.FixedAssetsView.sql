EXEC dbo.drop_object @object = N'fact.FixedAssetsView', @type = N'V' ;
GO
CREATE VIEW fact.FixedAssetsView
AS
  SELECT            DFA.StageId                                  AS StageID
                  , DFA.ExecutionFlag                            AS ExecutionFlag
                  , DFA.Partition                                AS Partition
                  , DFA.CompanyId                                AS CompanyId
                  , DFA.DataConnectionID                         AS DataConnectionId
                  --	/***BK***/
                  , COALESCE(TD.DateID, 0)                       AS TransactionDateID
                  , COALESCE(AD.DateID, 0)                       AS AcquisitionDateID
                  , COALESCE(DD.DateID, 0)                       AS DisposalDateID
                  , COALESCE(FA.FixedAssetID, 0)                 AS FixedAssetID
                  , COALESCE(DP.DepreciationProfileID, 0)        AS DepreciationProfileID
                  , COALESCE(FAT.FixedAssetTransactionTypeID, 0) AS FixedAssetTransactionTypeId
                  , COALESCE(DB.DepreciationBookID, 0)           AS DepreciationBookID
                  --Measures
                  , COALESCE(DFA.DepreciationAmountMST, 0)       AS DepreciationAmountMST
                  , COALESCE(DFA.AcquisitionAmountMST, 0)        AS AcquisitionAmountMST
                  , COALESCE(DFA.DepreciationAmount, 0)          AS DepreciationAmount
                  , COALESCE(DFA.AcquisitionAmount, 0)           AS AcquisitionAmount
    -- select COUNT(*) 
    FROM            delta.FixedAssets             AS DFA
    LEFT OUTER JOIN dim.Date                      AS TD ON DFA.TransactionDate                = TD.Date
    LEFT OUTER JOIN dim.Date                      AS AD ON DFA.AcquisitionDate                = AD.Date
    LEFT OUTER JOIN dim.Date                      AS DD ON DFA.DisposalDate                   = DD.Date
    LEFT OUTER JOIN dim.FixedAsset                AS FA ON DFA.FixedAssetCode                 = FA.FixedAssetCode
                                                       AND DFA.CompanyId                      = FA.CompanyID
                                                       AND DFA.Partition                      = FA.Partition
                                                       AND DFA.DataConnectionID               = FA.DataConnectionID
    LEFT OUTER JOIN dim.DepreciationProfile       AS DP ON DFA.DepreciationProfileCode        = DP.DepreciationProfileCode
                                                       AND DFA.CompanyId                      = DP.CompanyID
                                                       AND DFA.Partition                      = DP.Partition
                                                       AND DFA.DataConnectionID               = DP.DataConnectionID
    LEFT OUTER JOIN dim.FixedAssetTransactionType AS FAT ON FAT.FixedAssetTransactionTypeCode = DFA.FixedAssetTransactionTypeCode
                                                        AND DFA.DataConnectionID              = FAT.DataConnectionID
    LEFT OUTER JOIN dim.DepreciationBook          AS DB ON DFA.FixedAssetCode                 = DB.FixedAssetCode
                                                       AND DFA.DepreciationBookcode           = DB.DepreciationBookCode
                                                       AND DFA.CompanyId                      = DB.CompanyID
                                                       AND DFA.Partition                      = DB.Partition
                                                       AND DFA.DataConnectionID               = DB.DataConnectionID ;

GO


EXEC dbo.drop_object 'fact.FixedAssets', 'T' ;
GO
CREATE TABLE [fact].FixedAssets
(
  StageId                     BIGINT          NOT NULL
, ExecutionFlag               NCHAR(1)        NOT NULL
, ComponentExecutionID        INT             NOT NULL
, Partition                   BIGINT          NULL
, DataConnectionID            INT             NOT NULL
, CompanyID                   INT             NOT NULL
-- dimensions
, TransactionDateID           INT             NOT NULL
, AcquisitionDateID           INT             NOT NULL
, DisposalDateID              INT             NOT NULL
, FixedAssetID                INT             NOT NULL
, DepreciationProfileID       INT             NOT NULL
, FixedAssetTransactionTypeId INT             NOT NULL
, DepreciationBookId          INT             NOT NULL
-- measures
, DepreciationAmountMST       DECIMAL(19,4)   NULL
, AcquisitionAmountMST        DECIMAL(19,4)   NULL
, DepreciationAmount          DECIMAL(19,4)   NULL
, AcquisitionAmount           DECIMAL(19,4)   NULL
) ;




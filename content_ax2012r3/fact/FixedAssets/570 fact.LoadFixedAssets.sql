EXEC dbo.drop_object @object = N'fact.LoadFixedAssets' -- nvarchar(128)
                   , @type = N'P'                      -- nchar(2)
                   , @debug = 0 ;                      -- int
GO

CREATE PROCEDURE fact.LoadFixedAssets
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  DECLARE @table_name sysname = N'FixedAssets' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;
  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.FixedAssets ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;
    INSERT INTO fact.FixedAssets (
      StageId
    , ExecutionFlag
    , ComponentExecutionID
    , Partition
    , DataConnectionID
    , CompanyID
    -- BK
    , TransactionDateID
    , AcquisitionDateID
    , DisposalDateID
    , FixedAssetID
    , DepreciationProfileID
    , FixedAssetTransactionTypeId
    , DepreciationBookId
    -- Measures         
    , DepreciationAmountMST
    , AcquisitionAmountMST
    , DepreciationAmount
    , AcquisitionAmount
    )
    SELECT StageId
         , ExecutionFlag
         , @component_execution_id
         , Partition
         , DataConnectionID
         , CompanyID
         -- BK	
         , TransactionDateID
         , AcquisitionDateID
         , DisposalDateID
         , FixedAssetID
         , DepreciationProfileID
         , FixedAssetTransactionTypeId
         , DepreciationBookId
         -- Measures                                                                                              
         , DepreciationAmountMST
         , AcquisitionAmountMST
         , DepreciationAmount
         , AcquisitionAmount
      FROM fact.FixedAssetsView ;
    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load                                                                                     
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.FixedAssets
         SET      StageId = F.StageId
                , ExecutionFlag = F.ExecutionFlag
                , ComponentExecutionID = @component_execution_id
                , Partition = F.Partition
                , DataConnectionID = F.DataConnectionID
                , CompanyID = F.CompanyID
                -- BK
                , TransactionDateID = F.TransactionDateID
                , AcquisitionDateID = F.AcquisitionDateID
                , DisposalDateID = F.DisposalDateID
                , FixedAssetID = F.FixedAssetID
                , DepreciationProfileID = F.DepreciationProfileID
                , FixedAssetTransactionTypeId = F.FixedAssetTransactionTypeId
                , DepreciationBookId = F.DepreciationBookId
                -- Measures         		              
                , DepreciationAmountMST = F.DepreciationAmountMST
                , AcquisitionAmountMST = F.AcquisitionAmountMST
                , DepreciationAmount = F.DepreciationAmount
                , AcquisitionAmount = F.AcquisitionAmount
        FROM      fact.FixedAssets     AS F
       INNER JOIN fact.FixedAssetsView AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U' ;
      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.FixedAssets (
        StageId
      , ExecutionFlag
      , ComponentExecutionID
      , Partition
      , DataConnectionID
      , CompanyID
      , TransactionDateID
      , AcquisitionDateID
      , DisposalDateID
      , FixedAssetID
      , DepreciationProfileID
      , FixedAssetTransactionTypeId
      , DepreciationBookId
      , DepreciationAmountMST
      , AcquisitionAmountMST
      , DepreciationAmount
      , AcquisitionAmount
      )
      SELECT V.StageId
           , V.ExecutionFlag
           , @component_execution_id
           , V.Partition
           , V.DataConnectionID
           , V.CompanyID
           , V.TransactionDateID
           , V.AcquisitionDateID
           , V.DisposalDateID
           , V.FixedAssetID
           , V.DepreciationProfileID
           , V.FixedAssetTransactionTypeId
           , V.DepreciationBookId
           , V.DepreciationAmountMST
           , V.AcquisitionAmountMST
           , V.DepreciationAmount
           , V.AcquisitionAmount
        FROM fact.FixedAssetsView AS V
       WHERE V.ExecutionFlag = 'N'
         AND NOT EXISTS (SELECT 1 FROM fact.FixedAssets AS F WHERE F.StageID = V.StageID) ;
      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.FixedAssets
       WHERE EXISTS ( SELECT 1
                        FROM fact.SalesOrderView
                       WHERE ExecutionFlag                = 'D'
                         AND [fact].[FixedAssets].StageID = StageID) ;
      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;

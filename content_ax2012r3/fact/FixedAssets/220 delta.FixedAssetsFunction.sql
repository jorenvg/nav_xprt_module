EXEC dbo.drop_object 'delta.FixedAssetsFunction', 'F' ;
GO
CREATE FUNCTION delta.FixedAssetsFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN SELECT            ADT.Stage_Id                                                        AS StageId
                         , @execution_flag                                                     AS ExecutionFlag
                         , @component_execution_id                                             AS ComponentExecutionId
                         , ADT.Partition                                                       AS Partition
                         , ADT.Data_Connection_Id                                              AS DataConnectionID
                         , ADT.Company_Id                                                      AS CompanyId
                         --dimensions
                         , ADT.TransDate                                                       AS TransactionDate
                         , ADB.AcquisitionDate                                                 AS AcquisitionDate
                         , ADB.DisposalDate                                                    AS DisposalDate
                         , AT.AssetId                                                          AS FixedAssetCode
                         , ADB.DepreciationBookId                                              AS DepreciationBookCode
                         , ADP.Profile                                                         AS DepreciationProfileCode
                         , ADT.TransType                                                       AS FixedAssetTransactionTypeCode
                         -- measures
                         , CASE WHEN ADT.TransType = 3 THEN (ADT.AmountMst * -1) ELSE NULL END AS DepreciationAmountMST
                         , CASE WHEN ADT.TransType = 1 THEN ADT.AmountMst ELSE NULL END        AS AcquisitionAmountMST
                         , CASE WHEN ADT.TransType = 3 THEN (ADT.AmountCur * -1) ELSE NULL END AS DepreciationAmount
                         , CASE WHEN ADT.TransType = 1 THEN ADT.AmountCur ELSE NULL END        AS AcquisitionAmount
           FROM            stage_ax.AssetDepbooktrans        AS ADT
           LEFT OUTER JOIN stage_ax.AssetTable               AS AT ON ADT.AssetId              = AT.AssetId
                                                                  AND ADT.Company_Id           = AT.Company_Id
                                                                  AND ADT.Partition            = AT.Partition
                                                                  AND ADT.Data_Connection_Id   = AT.Data_Connection_Id
           LEFT OUTER JOIN stage_ax.AssetDepbooktable        AS ADBT ON ADT.DepreciationBookId = ADBT.DepreciationBookId
                                                                    AND ADT.Company_Id         = ADBT.Company_Id
                                                                    AND ADT.Partition          = ADBT.Partition
                                                                    AND ADT.Data_Connection_Id = ADBT.Data_Connection_Id
           LEFT OUTER JOIN stage_ax.AssetDepreciationProfile AS ADP ON ADP.Profile             = ADBT.DepreciationProfile
                                                                   AND ADP.Company_Id          = ADBT.Company_Id
                                                                   AND ADP.Partition           = ADBT.Partition
                                                                   AND ADP.Data_Connection_Id  = ADBT.Data_Connection_Id
           LEFT OUTER JOIN stage_ax.AssetDepbook             AS ADB ON ADB.AssetId             = ADT.AssetId
                                                                   AND ADB.DepreciationBookId  = ADBT.DepreciationBookId
                                                                   AND ADB.Company_Id          = AT.Company_Id
                                                                   AND ADB.Partition           = AT.Partition
                                                                   AND ADB.Data_Connection_Id  = AT.Data_Connection_Id
          WHERE
           /** FULL LOAD **/
                           ( @load_type         = 0
                         AND ADT.execution_flag <> 'D')
             OR
             /** INCREMENTAL, NEW RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'N'
                         AND ADT.execution_flag               = @execution_flag
                         AND ADT.execution_timestamp          > @last_processed_timestamp)
             OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( ADT.execution_flag           = @execution_flag
                             AND ADT.execution_timestamp      > @last_processed_timestamp)
                            OR ( AT.execution_flag            = @execution_flag
                             AND AT.execution_timestamp       > @last_processed_timestamp)
                            OR ( ADBT.execution_flag          = @execution_flag
                             AND ADBT.execution_timestamp     > @last_processed_timestamp)
                            OR ( ADP.execution_flag           = @execution_flag
                             AND ADP.execution_timestamp      > @last_processed_timestamp)))
                          OR
                          /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                        = 1
                           AND @execution_flag                   = 'D'
                           AND ADT.execution_flag                = @execution_flag
                           AND ADT.execution_timestamp           > @last_processed_timestamp) ;

GO
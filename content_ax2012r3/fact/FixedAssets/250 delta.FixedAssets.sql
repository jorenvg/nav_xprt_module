EXEC dbo.drop_object 'delta.FixedAssets', 'T' ;
GO
CREATE TABLE [delta].[FixedAssets]
(
  StageId                       BIGINT          NOT NULL
, ExecutionFlag                 NCHAR(10)       NULL
, ComponentExecutionId          INT             NOT NULL
, Partition                     BIGINT          NULL
, DataConnectionID              INT             NULL
, CompanyId                     INT             NULL
-- dimensions
, TransactionDate               DATETIME        NULL
, AcquisitionDate               DATETIME        NULL
, DisposalDate                  DATETIME        NULL
, FixedAssetCode                NVARCHAR(50)    NULL
, DepreciationProfileCode       NVARCHAR(30)    NULL
, DepreciationBookcode          NVARCHAR(30)    NULL
, FixedAssetTransactionTypeCode INT             NULL
-- measures
, DepreciationAmountMST         DECIMAL(19,4)   NULL
, AcquisitionAmountMST          DECIMAL(19,4)   NULL
, DepreciationAmount            DECIMAL(19,4)   NULL
, AcquisitionAmount             DECIMAL(19,4)   NULL
) ;                             















































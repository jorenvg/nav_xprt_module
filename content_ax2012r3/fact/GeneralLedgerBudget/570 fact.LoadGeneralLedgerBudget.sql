EXEC dbo.drop_object @object = N'fact.LoadGeneralLedgerBudget' -- nvarchar(128)
                   , @type = N'P'                              -- nchar(2)
                   , @debug = 0 ;                              -- int
GO

CREATE PROCEDURE fact.LoadGeneralLedgerBudget
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  DECLARE @table_name SYSNAME = N'GeneralLedgerBudget' ;
  DECLARE @delta_table_identifier SYSNAME = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier SYSNAME = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sp_executeSQL @SQL
                   , @parmas
                   , @Delta_Inserts = @Delta_Inserts OUTPUT
                   , @Delta_Updates = @Delta_Updates OUTPUT
                   , @Delta_Deletes = @Delta_Deletes OUTPUT ;

  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.GeneralLedgerBudget ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO [fact].[GeneralLedgerBudget] (
      DataConnectionID
    , CompanyID
    , ComponentExecutionID
    , Partition
    , FinancialDimensionsID
    , VoucherID
    , PostingDateID
    , BudgetModelID
    , BudgetCodeID
    , BudgetPlanID
    , ChartOfAccountsID
    , CurrencyTransactionID
    , CurrencyReportID
    , IsCredit
    , Balance
    , TransactionAmount
    , TransactionAmount_RCY
    , AccountingAmount
    , StageId
    )
    SELECT DataConnectionID
         , CompanyID
         , @component_execution_id
         , Partition
         , FinancialDimensionsID
         , VoucherID
         , PostingDateID
         , BudgetModelID
         , BudgetCodeID
         , BudgetPlanID
         , ChartOfAccountsID
         , CurrencyTransactionID
         , CurrencyReportID
         , IsCredit
         , Balance
         , TransactionAmount
         , TransactionAmount_RCY
         , AccountingAmount
         , StageId
      FROM [fact].[GeneralLedgerBudgetView] ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      [fact].[GeneralLedgerBudget]
         SET      DataConnectionID = V.DataConnectionID
                , CompanyID = V.CompanyID
                , ComponentExecutionID = @component_execution_id
                , Partition = V.Partition
                , FinancialDimensionsID = V.FinancialDimensionsID
                , VoucherID = V.VoucherID
                , PostingDateID = V.PostingDateID
                , BudgetModelID = V.BudgetModelID
                , BudgetCodeID = V.BudgetCodeID
                , BudgetPlanID = V.BudgetPlanID
                , ChartOfAccountsID = V.ChartOfAccountsID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , IsCredit = V.IsCredit
                , Balance = V.Balance
                , TransactionAmount = V.TransactionAmount
                , TransactionAmount_RCY = V.TransactionAmount_RCY
                , AccountingAmount = V.AccountingAmount
                , StageId = V.StageId
        FROM      [fact].[GeneralLedgerBudget]     F
       INNER JOIN [fact].[GeneralLedgerBudgetView] AS V ON F.StageId = V.StageId
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO [fact].[GeneralLedgerBudget] (
        DataConnectionID
      , CompanyID
      , ComponentExecutionID
      , Partition
      , FinancialDimensionsID
      , VoucherID
      , PostingDateID
      , BudgetModelID
      , BudgetCodeID
      , BudgetPlanID
      , ChartOfAccountsID
      , CurrencyTransactionID
      , CurrencyReportID
      , IsCredit
      , Balance
      , TransactionAmount
      , TransactionAmount_RCY
      , AccountingAmount
      , StageId
      )
      SELECT DataConnectionID      = V.DataConnectionID
           , CompanyID             = V.CompanyID
           , ComponentExecutionID  = @component_execution_id
           , Partition             = V.Partition
           , FinancialDimensionsID = V.FinancialDimensionsID
           , VoucherID             = V.VoucherID
           , PostingDateID         = V.PostingDateID
           , BudgetModelID         = V.BudgetModelID
           , BudgetCodeID          = V.BudgetCodeID
           , BudgetPlanID          = V.BudgetPlanID
           , ChartOfAccountsID     = V.ChartOfAccountsID
           , CurrencyTransactionID = V.CurrencyTransactionID
           , CurrencyReportID      = V.CurrencyReportID
           , IsCredit              = V.IsCredit
           , Balance               = V.Balance
           , TransactionAmount     = V.TransactionAmount
           , TransactionAmount_RCY = V.TransactionAmount_RCY
           , AccountingAmount      = V.AccountingAmount
           , StageId               = V.StageId
        FROM [fact].[GeneralLedgerBudgetView] V
       WHERE ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.GeneralLedgerBudget F WHERE F.StageId = V.StageId) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM [fact].[GeneralLedgerBudget]
       WHERE EXISTS ( SELECT 1
                        FROM [fact].[GeneralLedgerBudgetView] V
                       WHERE ExecutionFlag = 'D'
                         AND V.StageId     = [fact].[GeneralLedgerBudget].StageId) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
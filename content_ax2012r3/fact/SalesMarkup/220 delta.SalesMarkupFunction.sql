EXEC dbo.drop_object 'delta.SalesMarkupFunction', 'F' ;
GO
CREATE FUNCTION delta.SalesMarkupFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
              , MarkupTrans AS (
             SELECT            mt.stage_id
                             , mt.component_execution_id
                             , mt.execution_timestamp
                             , mt.data_connection_id
                             , mt.execution_flag
                             , mt.company_id
                             , mt.TRANSRECID
                             , mt.MARKUPCODE
                             , mt.CURRENCYCODE
                             , mt.VALUE
                             , mt.CALCULATEDAMOUNT
                             , mt.TAXAMOUNT
                             , mt.POSTED
                             , mt.TRANSDATE
                             , mt.TAXGROUP
                             , mt.TAXITEMGROUP
                             , mt.MARKUPCATEGORY
                             , mt.MODULETYPE
                             , mt.PARTITION
                             , mt.RECID
                             , SQLD.TABLEID
                             , SQLD.SQLNAME
                             , CASE
                                 WHEN SQLD.SQLNAME = 'SalesLine' THEN sl.SALESID
                                 WHEN SQLD.SQLNAME = 'SalesTable' THEN sl.SALESID
                                 WHEN SQLD.SQLNAME = 'CustPackingSlipTrans' THEN cpst.SALESID
                                 WHEN SQLD.SQLNAME = 'CustPackingSlipJour' THEN CPSJ.SALESID
                                 WHEN SQLD.SQLNAME = 'CustPackingSlipTrans' THEN cpst.SALESID
                                 WHEN SQLD.SQLNAME = 'CustPackingSlipJour' THEN CPSJ.SALESID
                                 ELSE NULL
                               END AS SalesOrderCode
                             , CASE
                                 WHEN SQLD.SQLNAME = 'CustInvoiceTrans' THEN cit.INVOICEID
                                 WHEN SQLD.SQLNAME = 'CustInvoiceJour' THEN CIJ.INVOICEID
                                 ELSE NULL
                               END AS SalesInvoice
                             , CASE
                                 WHEN SQLD.SQLNAME = 'CustPackingSlipTrans' THEN cpst.PACKINGSLIPID
                                 WHEN SQLD.SQLNAME = 'CustPackingSlipJour' THEN CPSJ.PACKINGSLIPID
                                 ELSE NULL
                               END AS SalesPackingSlipCode
               FROM            stage_ax.MARKUPTRANS          mt
               JOIN            stage_ax.SQLDICTIONARY        SQLD ON mt.TRANSTABLEID            = SQLD.TABLEID
                                                                 AND SQLD.FIELDID               = 0
                                                                 AND mt.data_connection_id      = SQLD.data_connection_id
               LEFT OUTER JOIN stage_ax.SALESLINE            AS sl ON sl.RECID                  = mt.TRANSRECID
                                                                  AND sl.PARTITION              = mt.PARTITION
                                                                  AND sl.data_connection_id     = mt.data_connection_id
                                                                  AND sl.company_id             = mt.company_id
               LEFT OUTER JOIN stage_ax.SALESTABLE           AS ST ON ST.RECID                  = mt.TRANSRECID
                                                                  AND ST.PARTITION              = mt.PARTITION
                                                                  AND ST.data_connection_id     = mt.data_connection_id
                                                                  AND ST.company_id             = mt.company_id
               LEFT OUTER JOIN stage_ax.CUSTPACKINGSLIPTRANS AS cpst ON cpst.RECID              = mt.TRANSRECID
                                                                    AND cpst.PARTITION          = mt.PARTITION
                                                                    AND cpst.data_connection_id = mt.data_connection_id
                                                                    AND cpst.company_id         = mt.company_id
               LEFT OUTER JOIN stage_ax.CUSTPACKINGSLIPJOUR  AS CPSJ ON CPSJ.RECID              = mt.TRANSRECID
                                                                    AND CPSJ.PARTITION          = mt.PARTITION
                                                                    AND CPSJ.data_connection_id = mt.data_connection_id
                                                                    AND CPSJ.company_id         = mt.company_id
               LEFT OUTER JOIN stage_ax.CUSTINVOICETRANS     AS cit ON cit.RECID                = mt.TRANSRECID
                                                                   AND cit.PARTITION            = mt.PARTITION
                                                                   AND cit.data_connection_id   = mt.data_connection_id
                                                                   AND cit.company_id           = mt.company_id
               LEFT OUTER JOIN stage_ax.CUSTINVOICEJOUR      AS CIJ ON CIJ.RECID                = mt.TRANSRECID
                                                                   AND CIJ.PARTITION            = mt.PARTITION
                                                                   AND CIJ.data_connection_id   = mt.data_connection_id
                                                                   AND CIJ.company_id           = mt.company_id
              WHERE            SQLD.SQLNAME IN ( 'SalesLine', 'SalesTable', 'CustPackingSlipTrans', 'CustPackingSlipJour', 'CustInvoiceTrans', 'CustInvoiceJour' )
           )
           SELECT      MT.stage_id                                                     AS StageID
                     , @execution_flag                                                 AS ExecutionFlag
                     , @component_execution_id                                         AS ComponentExecutionID
                     , MT.RECID                                                        AS SalesMarkupRecID
                     , MT.PARTITION                                                    AS SalesMarkupPartition
                     , MT.company_id                                                   AS CompanyID
                     , MT.data_connection_id                                           AS DataConnectionID
                     , MT.TABLEID                                                      AS OriginTableCode
                     , MT.SQLNAME                                                      AS OriginTable
                     , MT.TRANSRECID                                                   AS OriginTransRecID
                     , MT.SalesOrderCode                                               AS SalesOrderCode
                     , MT.SalesInvoice                                                 AS SalesInvoice
                     , CIJ.INVOICEDATE                                                 AS InvoiceDate
                     , CIJ.NUMBERSEQUENCEGROUP                                         AS NumberSequenceGroup
                     , MT.SalesPackingSlipCode                                         AS SalesPackingSlipCode
                     , CAST(MT.TRANSDATE AS DATE)                                      AS TransactionDate
                     , CAST(CIJ.DOCUMENTDATE AS DATE)                                  AS DocumentDate
                     , CPSJ.DELIVERYDATE                                               AS DeliveryDate
                     , COALESCE(CIJ.INVOICEPOSTALADDRESS, CPSJ.INVOICEPOSTALADDRESS)   AS InvoiceAddressCode
                     , COALESCE(CIJ.DELIVERYPOSTALADDRESS, CPSJ.DELIVERYPOSTALADDRESS) AS DeliveryAddressCode
                     , COALESCE(ST.CUSTACCOUNT, CIJ.ORDERACCOUNT)                      AS CustomerAccountCode
                     , COALESCE(ST.INVOICEACCOUNT, CIJ.INVOICEACCOUNT)                 AS InvoiceAccountCode
                     , ST.RECEIPTDATEREQUESTED                                         AS ReceiptRequestedDate
                     , ST.SHIPPINGDATEREQUESTED                                        AS ShippingRequestedDate
                     , ST.RECEIPTDATECONFIRMED                                         AS ReceiptConfirmedDate
                     , ST.SHIPPINGDATECONFIRMED                                        AS ShippingConfirmedDate
                     , COALESCE(ST.WORKERSALESTAKER, CPSJ.WORKERSALESTAKER)            AS SalesTakerCode
                     , ST.WORKERSALESRESPONSIBLE                                       AS SalesResponsibleCode
                     , COALESCE(CIJ.LEDGERVOUCHER, CPSJ.LEDGERVOUCHER)                 AS VoucherCode
                     , COALESCE(CIJ.SALESTYPE, CPSJ.SALESTYPE)                         AS SalesOrderTypeCode
                     , COALESCE(ST.DLVTERM, CPSJ.DLVTERM)                              AS DeliveryTermCode
                     , COALESCE(ST.DLVMODE, CPSJ.DLVMODE)                              AS DeliveryModeCode
                     , ST.PAYMENT                                                      AS PaymentTermCode
                     , ST.PAYMMODE                                                     AS PaymentModeCode
                     , COALESCE(ST.SALESORIGINID, CIJ.SALESORIGINID)                   AS SalesOriginCode
                     , ST.SALESGROUP                                                   AS SalesGroupCode
                     , ST.SALESPOOLID                                                  AS SalesPoolCode
                     , MT.CURRENCYCODE                                                 AS CurrencyCode
                     , MT.MARKUPCODE                                                   AS MarkupCode
                     , MT.MODULETYPE                                                   AS ModuleType
                     , MT.TAXGROUP                                                     AS TaxGroupCode
                     , MT.TAXITEMGROUP                                                 AS TaxItemGroupCode
                     , MT.CALCULATEDAMOUNT                                             AS MarkUpCalculatedAmount
                     , MT.CALCULATEDAMOUNT * ex.CrossRate                              AS MarkUpCalculatedAmount_RCY
                     , MT.POSTED                                                       AS MarkUpPostedAmount
                     , MT.POSTED * ex.CrossRate                                        AS MarkUpPostedAmount_RCY
                     , MT.TAXAMOUNT                                                    AS MarkupTaxAmount
                     , MT.TAXAMOUNT * ex.CrossRate                                     AS MarkupTaxAmount_RCY
             FROM      MarkupTrans                  AS MT
             LEFT JOIN stage_ax.SALESTABLE          ST ON ST.SALESID              = MT.SalesOrderCode
                                                      AND MT.data_connection_id   = ST.data_connection_id
                                                      AND MT.company_id           = ST.company_id
                                                      AND MT.PARTITION            = ST.PARTITION
             LEFT JOIN stage_ax.CUSTINVOICEJOUR     CIJ ON CIJ.INVOICEID          = MT.SalesInvoice
                                                       AND MT.data_connection_id  = CIJ.data_connection_id
                                                       AND MT.company_id          = CIJ.company_id
                                                       AND MT.PARTITION           = CIJ.PARTITION
             LEFT JOIN stage_ax.CUSTPACKINGSLIPJOUR CPSJ ON CPSJ.PACKINGSLIPID    = MT.SalesPackingSlipCode
                                                        AND MT.data_connection_id = CPSJ.data_connection_id
                                                        AND MT.company_id         = CPSJ.company_id
                                                        AND MT.PARTITION          = CPSJ.PARTITION
             LEFT JOIN reporting_exchange_rates     ex ON ex.FromCurrencyCode     = MT.CURRENCYCODE
                                                      AND ex.DataConnectionID     = MT.data_connection_id
                                                      AND ex.Partition            = MT.PARTITION
                                                      AND MT.TRANSDATE BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE
             /** FULL LOAD **/
                       ( @load_type        = 0
                     AND MT.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                       ( @load_type                = 1
                     AND @execution_flag           = 'N'
                     AND MT.execution_flag         = @execution_flag
                     AND MT.execution_timestamp    > @last_processed_timestamp)
               OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( MT.execution_flag           = @execution_flag
                             AND MT.execution_timestamp      > @last_processed_timestamp)
                            OR ( CIJ.execution_flag            = @execution_flag
                             AND CIJ.execution_timestamp       > @last_processed_timestamp)
                            OR ( CPSJ.execution_flag          = @execution_flag
                             AND CPSJ.execution_timestamp     > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                       ( @load_type                = 1
                     AND @execution_flag           = 'D'
                     AND MT.execution_flag         = @execution_flag
                     AND MT.execution_timestamp    > @last_processed_timestamp)) ;
GO

EXEC dbo.drop_object @object = N'fact.SalesMarkupView', @type = N'V' ;
GO
CREATE VIEW fact.SalesMarkupView
AS
  SELECT            sm.StageId                           AS StageID
                  , sm.ExecutionFlag                     AS ExecutionFlag
                  /*** BK ***/
                  , sm.SalesMarkupRecID
                  , sm.SalesMarkupPartition
                  , sm.CompanyID
                  , sm.DataConnectionID
                  , COALESCE(td.DateID, 0)               AS TransactionDateID
                  , COALESCE(dd.DateID, 0)               AS DocumentDateID
                  , COALESCE(sd.DateID, 0)               AS DeliveryDateID
                  , COALESCE(rrd.DateID, 0)              AS ReceiptRequestedDateID
                  , COALESCE(srd.DateID, 0)              AS ShippingRequestedDateID
                  , COALESCE(rcd.DateID, 0)              AS ReceiptConfirmedDateID
                  , COALESCE(scd.DateID, 0)              AS ShippingConfirmedDateID
                  , COALESCE(iadr.AddressID, 0)          AS InvoiceAddressID
                  , COALESCE(dadr.AddressID, 0)          AS DeliveryAddressID
                  , COALESCE(cAcc.CustomerID, 0)         AS CustomerAccountID
                  , COALESCE(iAcc.CustomerID, 0)         AS InvoiceAccountID
                  , COALESCE(stEmp.EmployeeID, 0)        AS SalesTakerID
                  , COALESCE(srEmp.EmployeeID, 0)        AS SalesResponsibleID
                  , COALESCE(jv.VoucherID, 0)            AS VoucherID
                  , COALESCE(sot.SalesOrderTypeID, 0)    AS SalesOrderTypeID
                  , COALESCE(dt.DeliveryTermsID, 0)      AS DeliveryTermID
                  , COALESCE(modlv.ModeOfDeliveryID, 0)  AS ModeOfDeliveryID
                  , COALESCE(pt.PaymentTermsID, 0)       AS PaymentTermsID
                  , COALESCE(mop.MethodOfPaymentID, 0)   AS MethodOfPaymentID
                  , COALESCE(orign.SalesOriginID, 0)     AS SalesOriginID
                  , COALESCE(sg.SalesGroupID, 0)         AS SalesGroupID
                  , COALESCE(sp.SalesPoolID, 0)          AS SalesPoolID
                  , COALESCE(cy.CurrencyID, 0)           AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)          AS CurrencyReportID
                  , COALESCE(smo.SalesMarkupOriginID, 0) AS SalesMarkupOriginID
                  , COALESCE(smt.MarkupTypeID, 0)        AS MarkupTypeID
                  , COALESCE(so.SalesOrderID, 0)         AS SalesOrderID
                  , COALESCE(si.SalesInvoiceID, 0)       AS SalesInvoiceID
                  , COALESCE(sps.SalesPackingSlipID, 0)  AS SalesPackingSlipID
                  /*** Measures ***/
                  , sm.MarkUpCalculatedAmount
                  , sm.MarkUpCalculatedAmount_RCY
                  , sm.MarkUpPostedAmount
                  , sm.MarkUpPostedAmount_RCY
                  , sm.MarkupTaxAmount
                  , sm.MarkupTaxAmount_RCY
    FROM            delta.SalesMarkup     AS sm
    LEFT JOIN       dim.Date              AS td ON td.Date                     = sm.TransactionDate
    LEFT JOIN       dim.Date              AS dd ON dd.Date                     = sm.DocumentDate
    LEFT JOIN       dim.Date              AS sd ON sd.Date                     = sm.DeliveryDate
    LEFT JOIN       dim.Date              AS rrd ON rrd.Date                   = sm.ReceiptRequestedDate
    LEFT JOIN       dim.Date              AS srd ON srd.Date                   = sm.ShippingRequestedDate
    LEFT JOIN       dim.Date              AS rcd ON rcd.Date                   = sm.ReceiptConfirmedDate
    LEFT JOIN       dim.Date              AS scd ON scd.Date                   = sm.ShippingConfirmedDate
    LEFT JOIN       dim.Address           AS iadr ON iadr.AddressRecID         = sm.InvoiceAddressCode
                                                 AND iadr.DataConnectionID     = sm.DataConnectionID
                                                 AND iadr.Partition            = sm.SalesMarkupPartition
    LEFT JOIN       dim.Address           AS dadr ON dadr.AddressRecID         = sm.DeliveryAddressCode
                                                 AND dadr.DataConnectionID     = sm.DataConnectionID
                                                 AND dadr.Partition            = sm.SalesMarkupPartition
    LEFT JOIN       dim.Customer          AS cAcc ON cAcc.CustomerAccountNo    = sm.CustomerAccountCode
                                                 AND cAcc.Partition            = sm.SalesMarkupPartition
                                                 AND cAcc.DataConnectionID     = sm.DataConnectionID
                                                 AND cAcc.CompanyID            = sm.CompanyID
    LEFT JOIN       dim.Customer          AS iAcc ON iAcc.CustomerAccountNo    = sm.InvoiceAccountCode
                                                 AND iAcc.Partition            = sm.SalesMarkupPartition
                                                 AND iAcc.DataConnectionID     = sm.DataConnectionID
                                                 AND iAcc.CompanyID            = sm.CompanyID
    LEFT JOIN       dim.Employee          AS stEmp ON stEmp.EmployeeRecID      = sm.SalesTakerCode
                                                  AND stEmp.Partition          = sm.SalesMarkupPartition
                                                  AND stEmp.DataConnectionID   = sm.DataConnectionID
    LEFT JOIN       dim.Employee          AS srEmp ON srEmp.EmployeeRecID      = sm.SalesResponsibleCode
                                                  AND srEmp.Partition          = sm.SalesMarkupPartition
                                                  AND srEmp.DataConnectionID   = sm.DataConnectionID
    LEFT JOIN       dim.Voucher           AS jv ON jv.Voucher                  = sm.VoucherCode
                                               AND jv.CompanyID                = sm.CompanyID
                                               AND jv.Partition                = sm.SalesMarkupPartition
                                               AND jv.DataConnectionID         = sm.DataConnectionID
    LEFT JOIN       dim.SalesOrderType    AS sot ON sot.SalesOrderTypeCode     = sm.SalesOrderTypeCode
                                                AND sot.DataConnectionID       = sm.DataConnectionID
    LEFT JOIN       dim.DeliveryTerms     AS dt ON dt.DeliveryTermsCode        = sm.DeliveryTermCode
                                               AND dt.Partition                = sm.SalesMarkupPartition
                                               AND dt.DataConnectionID         = sm.DataConnectionID
                                               AND dt.CompanyID                = sm.CompanyID
    LEFT JOIN       dim.ModeOfDelivery    AS modlv ON modlv.ModeOfDeliveryCode = sm.DeliveryModeCode
                                                  AND modlv.Partition          = sm.SalesMarkupPartition
                                                  AND modlv.DataConnectionID   = sm.DataConnectionID
                                                  AND modlv.CompanyID          = sm.CompanyID
    LEFT JOIN       dim.PaymentTerms      AS pt ON pt.PaymentTermsCode         = sm.PaymentTermCode
                                               AND pt.Partition                = sm.SalesMarkupPartition
                                               AND pt.DataConnectionID         = sm.DataConnectionID
                                               AND pt.CompanyID                = sm.CompanyID
    LEFT JOIN       dim.MethodOfPayment   AS mop ON mop.MethodOfPaymentCode    = sm.PaymentModeCode
                                                AND mop.Partition              = sm.SalesMarkupPartition
                                                AND mop.DataConnectionID       = sm.DataConnectionID
                                                AND mop.CompanyID              = sm.CompanyID
    LEFT JOIN       dim.SalesOrigin       AS orign ON orign.SalesOriginCode    = sm.SalesOriginCode
                                                  AND orign.DataConnectionID   = sm.DataConnectionID
                                                  AND orign.CompanyID          = sm.CompanyID
    LEFT JOIN       dim.SalesGroup        AS sg ON sg.SalesGroupCode           = sm.SalesGroupCode
                                               AND sg.Partition                = sm.SalesMarkupPartition
                                               AND sg.DataConnectionID         = sm.DataConnectionID
                                               AND sg.CompanyID                = sm.CompanyID
    LEFT JOIN       dim.SalesPool         AS sp ON sp.SalesPoolCode            = sm.SalesPoolCode
                                               AND sp.Partition                = sm.SalesMarkupPartition
                                               AND sp.DataConnectionID         = sm.DataConnectionID
                                               AND sp.CompanyID                = sm.CompanyID
    LEFT JOIN       dim.Currency          AS cy ON cy.Code                     = sm.CurrencyCode
                                               AND cy.Partition                = sm.SalesMarkupPartition
                                               AND cy.DataConnectionID         = sm.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport    AS rcy ON sm.SalesMarkupPartition    = rcy.Partition
    LEFT JOIN       dim.SalesMarkupOrigin AS smo ON smo.OriginTableCode        = sm.OriginTableCode
                                                AND smo.OriginTable            = sm.OriginTable
                                                AND smo.DataConnectionID       = sm.DataConnectionID
    LEFT JOIN       dim.SalesOrder        AS so ON so.SalesOrder               = sm.SalesOrderCode
                                               AND so.Partition                = sm.SalesMarkupPartition
                                               AND so.DataConnectionID         = sm.DataConnectionID
                                               AND so.CompanyID                = sm.CompanyID
    LEFT JOIN       dim.SalesInvoice      AS si ON si.SalesInvoice             = sm.SalesInvoice
                                               AND si.CompanyID                = sm.CompanyID
                                               AND si.Partition                = sm.SalesMarkupPartition
                                               AND si.DataConnectionID         = sm.DataConnectionID
    LEFT JOIN       dim.SalesPackingSlip  AS sps ON sps.SalesPackingSlip       = sm.SalesPackingSlipCode
                                                AND sps.CompanyID              = sm.CompanyID
                                                AND sps.Partition              = sm.SalesMarkupPartition
                                                AND sps.DataConnectionID       = sm.DataConnectionID
    LEFT JOIN       dim.MarkupType        AS smt ON smt.ChargesModuleCode      = sm.ModuleType
                                                AND smt.MarkupCode             = sm.MarkupCode
                                                AND smt.PARTITION              = sm.SalesMarkupPartition
                                                AND smt.DataConnectionID       = sm.DataConnectionID
                                                AND smt.CompanyID              = sm.CompanyID ;




EXEC dbo.drop_object 'delta.ProductionFunction', 'F' ;
GO
CREATE FUNCTION delta.ProductionFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            PCT.Stage_Id                                                       AS StageId
                           , @Execution_Flag                                                    AS ExecutionFlag
                           , @Component_Execution_Id                                            AS ComponentExecutionId
                           , PCT.Partition                                                      AS Partition
                           , PCT.Data_Connection_Id                                             AS DataConnectionID
                           , PCT.Company_Id                                                     AS CompanyId
                           , PCT.RecId                                                          AS ProductionRecId
                           , PCT.TransDate                                                      AS TransactionDate
                           , pt.DlvDate                                                         AS DeliveryDate
                           , pt.SchedStart                                                      AS ScheduledStartDate
                           , pt.SchedEnd                                                        AS ScheduledEndDate
                           , CAST(pt.CreatedDateTime AS DATE)                                   AS CreatedDate
                           , pt.CalcDate                                                        AS EstimatedDate
                           , pt.SchedDate                                                       AS ScheduledDate
                           , pt.StUpDate                                                        AS StartedDate
                           , pt.FinishedDate                                                    AS ReportAsFinishedDate
                           , pt.RealDate                                                        AS EndedDate
                           , COALESCE(NULLIF(ID.WMSLOCATIONID, ''), 'N/A')                      AS WMSLocationCode
                           , COALESCE(NULLIF(ID.CONFIGID, ''), 'N/A')                           AS ProductConfigCode
                           , COALESCE(NULLIF(ID.INVENTCOLORID, ''), 'N/A')                      AS ProductColorCode
                           , COALESCE(NULLIF(ID.INVENTSIZEID, ''), 'N/A')                       AS ProductInventSizeCode
                           , COALESCE(NULLIF(ID.INVENTLOCATIONID, ''), 'N/A')                   AS LocationCode
                           , COALESCE(NULLIF(ID.INVENTSTYLEID, ''), 'N/A')                      AS ProductStyleCode
                           , COALESCE(NULLIF(ID.INVENTBATCHID, ''), 'N/A')                      AS TrackingBatchCode
                           , COALESCE(NULLIF(ID.INVENTSERIALID, ''), 'N/A')                     AS TrackingSerialCode
                           , pt.ItemID                                                          AS ItemCode
                           , pt.ProdID                                                          AS ProductionOrderCode
                           , pt.RouteID                                                         AS RouteCode
                           , pr.OprID                                                           AS OperationCode
                           , PCT.Resource_                                                      AS ResourceCode
                           , CT_ENUM.Description                                                AS ConsumptionType
                           , PCT.CostGroupID                                                    AS CostGroupCode
                           , PCT.UnitID                                                         AS UnitOfMeasureCode
                           , PCT.CalcType                                                       AS CalculationTypeCode
                           --Measures                                                             
                           , PCT.COSTAMOUNT                                                     AS EstimatedCostMST
                           , PCT.CostAmount * EX.CrossRate                                      AS EstimatedCostRCY
                           , CASE WHEN PCT.CALCTYPE <> 5 THEN PCT.CONSUMPVARIABLE ELSE NULL END AS EstimatedQuantity
                           , CASE WHEN PCT.CALCTYPE = 5 THEN PCT.CONSUMPVARIABLE ELSE NULL END  AS EstimatedHours
                           , PCT.COSTMARKUP                                                     AS EstimatedMarkUpMST
                           , PCT.CostMarkUp * EX.CrossRate                                      AS EstimatedMarkUpRCY
                           , PCT.REALCOSTAMOUNT                                                 AS ActualCostMST
                           , PCT.RealCostAmount * EX.CrossRate                                  AS ActualCostRCY
                           , CASE WHEN PCT.CALCTYPE <> 5 THEN PCT.REALQTY ELSE NULL END         AS ActualQuantity
                           , PCT.REALCOSTADJUSTMENT                                             AS ActualCostAdjustmentMST
                           , PCT.REALCOSTADJUSTMENT * EX.CrossRate                              AS ActualCostAdjustmentRCY
                           , PCT.SALESMARKUP                                                    AS ActualMarkUpMST
                           , PCT.SALESMARKUP * EX.CrossRate                                     AS ActualMarkUpRCY
                           , CASE WHEN PCT.CALCTYPE = 5 THEN PCT.REALQTY ELSE NULL END          AS ActualHours
             FROM            stage_ax.ProdCalcTrans   AS PCT
             LEFT OUTER JOIN Stage_AX.PRODTABLE       AS pt ON PCT.COLLECTREFPRODID        = pt.PRODID
                                                           AND PCT.DATAAREAID              = pt.DATAAREAID
                                                           AND PCT.PARTITION               = pt.PARTITION
             LEFT OUTER JOIN Stage_AX.INVENTDIM       AS ID ON ID.INVENTDIMID              = pt.INVENTDIMID
                                                           AND ID.DATAAREAID               = pt.DATAAREAID
                                                           AND ID.PARTITION                = pt.PARTITION
             LEFT OUTER JOIN stage_ax.SQLDICTIONARY   AS SD_PR ON PCT.IDREFTABLEID         = SD_PR.TABLEID
                                                              AND SD_PR.FIELDID            = 0
                                                              AND SD_PR.NAME               = 'PRODROUTE'
                                                              AND PCT.Data_connection_id   = SD_PR.Data_connection_id
             LEFT OUTER JOIN Stage_AX.ProdRoute       AS pr ON PCT.IDREFRECID              = pr.RecID
                                                           AND PCT.IDREFTABLEID            = SD_PR.TABLEID
                                                           AND PCT.Company_ID              = pr.company_id
                                                           AND PCT.Data_connection_id      = pr.Data_connection_id
             LEFT OUTER JOIN meta.enumerations        AS CT_ENUM ON CT_ENUM.TableName      = 'Consumption'
                                                                AND CT_ENUM.ColumnName     = 'CalcType'
                                                                AND PCT.CALCTYPE           = CT_ENUM.OptionID
                                                                AND PCT.data_connection_id = CT_ENUM.dataconnectionid
             LEFT OUTER JOIN Stage_AX.LEDGER          AS L ON L.Name                       = PCT.DataAreaID
                                                          AND PCT.Partition                = L.Partition
                                                          AND PCT.data_connection_id       = L.data_connection_id
             LEFT OUTER JOIN reporting_exchange_rates AS EX ON EX.FromCurrencyCode         = L.AccountingCurrency
                                                           AND EX.DataConnectionID         = PCT.data_connection_id
                                                           AND PCT.TransDate BETWEEN EX.ValidFrom AND EX.ValidTo
            WHERE            PCT.CalcType        <> 0
              AND            (
                               /** FULL LOAD **/
                               ( @load_type                 = 0
                             AND PCT.execution_flag         <> 'D')
                            OR
                            /** INCREMENTAL, NEW RECORDS **/
                               ( @load_type                    = 1
                             AND @execution_flag               = 'N'
                             AND PCT.execution_flag            = @execution_flag
                             AND PCT.execution_timestamp       > @last_processed_timestamp)
                            OR
                            /** INCREMENTAL, UPDATED RECORDS **/
                               ( @load_type                    = 1
                             AND @execution_flag               = 'U'
                             AND ( ( PCT.execution_flag        = @execution_flag
                                 AND PCT.execution_timestamp   > @last_processed_timestamp)
                                OR ( pt.execution_flag         = @execution_flag
                                 AND pt.execution_timestamp    > @last_processed_timestamp)
                                OR ( ID.execution_flag         = @execution_flag
                                 AND ID.execution_timestamp    > @last_processed_timestamp)
                                OR ( SD_PR.execution_flag      = @execution_flag
                                 AND SD_PR.execution_timestamp > @last_processed_timestamp)
                                OR ( pr.execution_flag         = @execution_flag
                                 AND pr.execution_timestamp    > @last_processed_timestamp)
                                OR ( L.execution_flag          = @execution_flag
                                 AND L.execution_timestamp     > @last_processed_timestamp)))
                            OR
                            /** INCREMENTAL, DELETED RECORDS **/
                               ( @load_type                    = 1
                             AND @execution_flag               = 'D'
                             AND PCT.execution_flag            = @execution_flag
                             AND PCT.execution_timestamp       > @last_processed_timestamp))) ;

GO
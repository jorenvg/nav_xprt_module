EXEC dbo.drop_object @object = N'fact.LoadProduction' -- nvarchar(128)
                   , @type = N'P'                     -- nchar(2)
                   , @debug = 0 ;                     -- int
GO

CREATE PROCEDURE fact.LoadProduction
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  DECLARE @table_name sysname = N'Production' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.Production ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO fact.Production (
      StageId
    , Partition
    , ProductionRecID
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , ExecutionFlag
    , ItemID
    , ProductionOrderID
    , RouteOperationID
    , ConsumptionID
    , CostGroupID
    , UnitOfMeasureID
    , ProductionCalculationTypeID
    , TransactionDateID
    , DeliveryDateID
    , ScheduledStartDateID
    , ScheduledEndDateID
    , CreatedDateID
    , EstimatedDateID
    , ScheduledDateID
    , StartedDateID
    , ReportedAsFinishedDateID
    , EndedDateID
    , LocationID
    , TrackingID
    , WMSLocationID
    , ProductID
    --Measures
    , EstimatedCostMST
    , EstimatedCostRCY
    , EstimatedQuantity
    , EstimatedHours
    , EstimatedMarkUpMST
    , EstimatedMarkUpRCY
    , ActualCostMST
    , ActualCostRCY
    , ActualQuantity
    , ActualCostAdjustmentMST
    , ActualCostAdjustmentRCY
    , ActualMarkUpMST
    , ActualMarkUpRCY
    , ActualHours
    )
    SELECT StageId
         , Partition
         , ProductionRecID
         , CompanyID
         , DataConnectionID
         , ComponentExecutionID
         , ExecutionFlag
         , ItemID
         , ProductionOrderID
         , RouteOperationID
         , ConsumptionID
         , CostGroupID
         , UnitOfMeasureID
         , ProductionCalculationTypeID
         , TransactionDateID
         , DeliveryDateID
         , ScheduledStartDateID
         , ScheduledEndDateID
         , CreatedDateID
         , EstimatedDateID
         , ScheduledDateID
         , StartedDateID
         , ReportedAsFinishedDateID
         , EndedDateID
         , LocationID
         , TrackingID
         , WMSLocationID
         , ProductID
         --Measures
         , EstimatedCostMST
         , EstimatedCostRCY
         , EstimatedQuantity
         , EstimatedHours
         , EstimatedMarkUpMST
         , EstimatedMarkUpRCY
         , ActualCostMST
         , ActualCostRCY
         , ActualQuantity
         , ActualCostAdjustmentMST
         , ActualCostAdjustmentRCY
         , ActualMarkUpMST
         , ActualMarkUpRCY
         , ActualHours
      FROM fact.ProductionView ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.Production
         SET      StageId = V.StageId
                , Partition = V.Partition
                , ProductionRecID = V.ProductionRecID
                , CompanyID = V.CompanyID
                , DataConnectionID = V.DataConnectionID
                , ComponentExecutionID = V.ComponentExecutionID
                , ExecutionFlag = V.ExecutionFlag
                , ItemID = V.ItemID
                , ProductionOrderID = V.ProductionOrderID
                , RouteOperationID = V.RouteOperationID
                , ConsumptionID = V.ConsumptionID
                , CostGroupID = V.CostGroupID
                , UnitOfMeasureID = V.UnitOfMeasureID
                , ProductionCalculationTypeID = V.ProductionCalculationTypeID
                , TransactionDateID = V.TransactionDateID
                , DeliveryDateID = V.DeliveryDateID
                , ScheduledStartDateID = V.ScheduledStartDateID
                , ScheduledEndDateID = V.ScheduledEndDateID
                , CreatedDateID = V.CreatedDateID
                , EstimatedDateID = V.EstimatedDateID
                , ScheduledDateID = V.ScheduledDateID
                , StartedDateID = V.StartedDateID
                , ReportedAsFinishedDateID = V.ReportedAsFinishedDateID
                , EndedDateID = V.EndedDateID
                , LocationID = V.LocationID
                , TrackingID = V.TrackingID
                , WMSLocationID = V.WMSLocationID
                , ProductID = V.ProductID
                --Measures
                , EstimatedCostMST = V.EstimatedCostMST
                , EstimatedCostRCY = V.EstimatedCostRCY
                , EstimatedQuantity = V.EstimatedQuantity
                , EstimatedHours = V.EstimatedHours
                , EstimatedMarkUpMST = V.EstimatedMarkUpMST
                , EstimatedMarkUpRCY = V.EstimatedMarkUpRCY
                , ActualCostMST = V.ActualCostMST
                , ActualCostRCY = V.ActualCostRCY
                , ActualQuantity = V.ActualQuantity
                , ActualCostAdjustmentMST = V.ActualCostAdjustmentMST
                , ActualCostAdjustmentRCY = V.ActualCostAdjustmentRCY
                , ActualMarkUpMST = V.ActualMarkUpMST
                , ActualMarkUpRCY = V.ActualMarkUpRCY
                , ActualHours = V.ActualHours
        FROM      fact.Production     AS F
       INNER JOIN fact.ProductionView AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.Production (
        StageId
      , Partition
      , ProductionRecID
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , ExecutionFlag
      , ItemID
      , ProductionOrderID
      , RouteOperationID
      , ConsumptionID
      , CostGroupID
      , UnitOfMeasureID
      , ProductionCalculationTypeID
      , TransactionDateID
      , DeliveryDateID
      , ScheduledStartDateID
      , ScheduledEndDateID
      , CreatedDateID
      , EstimatedDateID
      , ScheduledDateID
      , StartedDateID
      , ReportedAsFinishedDateID
      , EndedDateID
      , LocationID
      , TrackingID
      , WMSLocationID
      , ProductID
      --Measures
      , EstimatedCostMST
      , EstimatedCostRCY
      , EstimatedQuantity
      , EstimatedHours
      , EstimatedMarkUpMST
      , EstimatedMarkUpRCY
      , ActualCostMST
      , ActualCostRCY
      , ActualQuantity
      , ActualCostAdjustmentMST
      , ActualCostAdjustmentRCY
      , ActualMarkUpMST
      , ActualMarkUpRCY
      , ActualHours
      )
      SELECT V.StageId
           , V.Partition
           , V.ProductionRecID
           , V.CompanyID
           , V.DataConnectionID
           , @component_execution_id
           , V.ExecutionFlag
           , V.ItemID
           , V.ProductionOrderID
           , V.RouteOperationID
           , V.ConsumptionID
           , V.CostGroupID
           , V.UnitOfMeasureID
           , V.ProductionCalculationTypeID
           , V.TransactionDateID
           , V.DeliveryDateID
           , V.ScheduledStartDateID
           , V.ScheduledEndDateID
           , V.CreatedDateID
           , V.EstimatedDateID
           , V.ScheduledDateID
           , V.StartedDateID
           , V.ReportedAsFinishedDateID
           , V.EndedDateID
           , V.LocationID
           , V.TrackingID
           , V.WMSlocationID
           , V.ProductID
           , V.EstimatedCostMST
           , V.EstimatedCostRCY
           , V.EstimatedQuantity
           , V.EstimatedHours
           , V.EstimatedMarkUpMST
           , V.EstimatedMarkUpRCY
           , V.ActualCostMST
           , V.ActualCostRCY
           , V.ActualQuantity
           , V.ActualCostAdjustmentMST
           , V.ActualCostAdjustmentRCY
           , V.ActualMarkUpMST
           , V.ActualMarkUpRCY
           , V.ActualHours
        FROM fact.ProductionView AS V
       WHERE V.ExecutionFlag = 'N'
         AND NOT EXISTS (SELECT 1 FROM fact.Production F WHERE F.StageID = V.StageID) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.Production
       WHERE EXISTS ( SELECT 1
                        FROM fact.ProductionView
                       WHERE ExecutionFlag               = 'D'
                         AND [fact].[Production].StageID = StageID) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
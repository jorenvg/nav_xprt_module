/************************************************************************************
*** InventoryTransaction Load procedure, 
*** 
*************************************************************************************/

EXEC dbo.drop_object @object = N'fact.LoadInventoryTransaction' -- nvarchar(128)
                   , @type = N'P'                               -- nchar(2)
                   , @debug = 0 ;                               -- int
GO

CREATE PROCEDURE fact.LoadInventoryTransaction
  @component_execution_id INT     = -1
, @load_type              TINYINT = 0 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @table_identifier sysname = N'fact.InventoryTransaction' ;
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  /*debugging*/
  --DECLARE @deleted INT = -1
  --, @updated INT = -1
  --, @inserted INT = -1

  --DECLARE @load_type TINYINT = (SELECT load_Type FROM meta.dwh_components WHERE component_id = meta.get_component_id(@Table))

  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.InventoryTransaction ;
    EXEC dbo.truncate_table @table_name = @table_identifier ;
  END ;

  DECLARE @icremental_date DATETIME ;
  SELECT @icremental_date = COALESCE(MAX(IncrementalDate), '1900-01-01')
    FROM fact.InventoryTransaction ;

  INSERT INTO fact.InventoryTransaction (
    [ComponentExecutionID]
  , [CompanyId]
  , [DataConnectionID]
  , [IncrementalDate]
  , [TransactionDateID]
  , [WMSlocationID]
  , [LocationID]
  , [ProductID]
  , [TrackingID]
  , [InventoryReferenceID]
  , [InventoryPostingTypeID]
  , [StatusReceiptID]
  , [StatusIssueID]
  , [PostingTypeID]
  , [PostingTypeOffsetID]
  , [PostedID]
  , [VoucherID]
  , [ItemID]
  , [FinancialDimensionsID]
  , [CurrencyID]
  , [SalesInvoiceID]
  , [Amount]
  , [Amount_RCY]
  , [Quantity]
  , [IssueQuantity]
  , [IssueAmount]
  , [IssueAmount_RCY]
  , [ReceiptQuantity]
  , [ReceiptAmount]
  , [ReceiptAmount_RCY]
  )
  SELECT @component_execution_id
       , vw.CompanyID
       , vw.DataConnectionID
       , vw.[IncrementalDate]
       , vw.TransactionDateID
       , vw.WMSlocationID
       , vw.LocationID
       , vw.ProductID
       , vw.TrackingID
       , vw.InventoryReferenceID
       , vw.InventoryPostingTypeID
       , vw.StatusReceiptID
       , vw.StatusIssueID
       , vw.PostingTypeID
       , vw.PostingTypeOffsetID
       , vw.PostedID
       , vw.VoucherID
       , vw.ItemID
       , vw.FinancialDimensionsID
       , vw.CurrencyID
       , vw.SalesInvoiceID
       , vw.Amount
       , vw.Amount_RCY
       , vw.Quantity
       , vw.IssueQuantity
       , vw.IssueAmount
       , vw.IssueAmount_RCY
       , vw.ReceiptQuantity
       , vw.ReceiptAmount
       , vw.ReceiptAmount_RCY
    FROM fact.InventoryTransactionView AS vw
   WHERE IncrementalDate > @icremental_date ;

  SELECT @inserted = @@ROWCOUNT ;

END ;
EXEC dbo.drop_object @object = N'fact.PurchaseInvoiceView', @type = N'V' ;
GO
CREATE VIEW fact.PurchaseInvoiceView
AS
  SELECT            fpi.StageId                             AS StageID
                  , fpi.ExecutionFlag                       AS ExecutionFlag
                  /***BK***/
                  , fpi.PurchaseInvoiceLineRecId            AS PurchaseInvoiceLineRecID
                  , fpi.PurchaseInvoiceLinePartition        AS PurchaseInvoiceLinePartition
                  , fpi.CompanyId                           AS CompanyID
                  , fpi.DataConnectionID                    AS DataConnectionID
                  /***Dimensions*/
                  --Dates                                      
                  , COALESCE(dd.DateID, 0)                  AS DocumentDateID
                  , COALESCE(id.DateID, 0)                  AS InvoiceDateID
                  , COALESCE(sd.DateID, 0)                  AS DeliveryDateID
                  , COALESCE(sInv.PurchaseInvoiceID, 0)     AS PurchaseInvoiceID
                  -- inventdimension                           
                  , COALESCE(wl.WMSLocationID, 0)           AS WMSlocationID
                  , COALESCE(l.LocationID, 0)               AS LocationID
                  , COALESCE(p.ProductID, 0)                AS ProductID
                  , COALESCE(t.TrackingID, 0)               AS TrackingID
                  --customer                                   
                  , COALESCE(cAcc.VendorID, 0)              AS VendorAccountID
                  , COALESCE(iAcc.VendorID, 0)              AS InvoiceAccountID
                  -- address                                   
                  , COALESCE(dAdr.AddressID, 0)             AS DeliveryAddressID
                  -- item                                      
                  , COALESCE(i.ItemID, 0)                   AS ItemID
                  , COALESCE(ep.EmployeeID, 0)              AS PurchasePlacerID
                  , COALESCE(er.EmployeeID, 0)              AS PurchaseRequesterID
                  -- currency                                  
                  , COALESCE(cy.CurrencyID, 0)              AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)             AS CurrencyReportID
                  -- financialdimension                        
                  , COALESCE(fd.FinancialDimensionsID, 0)   AS FinancialDimensionsID
                  , COALESCE(jv.VoucherID, 0)               AS VoucherID
                  , COALESCE(sot.PurchaseTypeID, 0)         AS PurchaseTypeID
                  , COALESCE(modlv.ModeOfDeliveryID, 0)     AS ModeOfDeliveryID
                  , COALESCE(dt.DeliveryTermsID, 0)         AS DeliveryTermsID
                  , COALESCE(mop.MethodOfPaymentID, 0)      AS MethodOfPaymentID
                  , COALESCE(pt.PaymentTermsID, 0)          AS PaymentTermsID
                  , COALESCE(so.PurchaseOrderID, 0)         AS PurchaseOrderID
                  , COALESCE(uom.UnitOfMeasureID, 0)        AS PurchaseUnitID
                  , COALESCE(pp.PurchasePoolID, 0)          AS PurchasePoolID
                  --	/***Measures */	                         
                  , fpi.InventoryQuantity                   AS InventoryQuantity
                  , fpi.DeliveredWithoutPackingSlipQuantity AS DeliveredWithoutPackingSlipQuantity
                  , fpi.InvoicedQuantity                    AS InvoicedQuantity
                  , fpi.PurchasePrice                       AS PurchasePrice
                  , fpi.PurchasePrice_RCY                   AS PurchasePrice_RCY
                  , fpi.Discount                            AS Discount
                  , fpi.Discount_RCY                        AS Discount_RCY
                  , fpi.DiscountPercent                     AS DiscountPercent
                  , fpi.MultiLineDiscount                   AS MultilineDiscount
                  , fpi.MultiLineDiscount_RCY               AS MultilineDiscount_RCY
                  , fpi.MultiLineDiscountPercent            AS MultilineDiscountPercent
                  , fpi.NetAmount                           AS NetAmount
                  , fpi.NetAmount_RCY                       AS NetAmount_RCY
                  , fpi.NetAmountMST                        AS NetAmountMST
                  , fpi.TaxAmount                           AS TaxAmount
                  , fpi.TaxAmount_RCY                       AS TaxAmount_RCY
                  , fpi.AmountExclTax                       AS AmountExclTax
                  , fpi.AmountExclTaxMST                    AS AmountExclTaxMST   
                  , fpi.DiscountAmount                      AS DiscountAmount    
                  , fpi.DiscountAmount_RCY                  AS DiscountAmount_RCY

    FROM            delta.PurchaseInvoice   AS fpi
    LEFT JOIN       dim.Date                AS dd ON dd.Date                           = fpi.DocumentDate
    LEFT JOIN       dim.Date                AS id ON id.Date                           = fpi.InvoiceDate
    LEFT JOIN       dim.Date                AS sd ON sd.Date                           = fpi.ShipDate
    LEFT JOIN       dim.Employee            AS ep ON ep.EmployeeRecID                  = fpi.PurchasePlacerCode
                                                 AND ep.Partition                      = fpi.PurchaseInvoiceLinePartition
                                                 AND ep.DataConnectionID               = fpi.DataConnectionID
    LEFT JOIN       dim.Employee            AS er ON er.EmployeeRecID                  = fpi.PurchaseRequesterCode
                                                 AND er.Partition                      = fpi.PurchaseInvoiceLinePartition
                                                 AND er.DataConnectionID               = fpi.DataConnectionID
    LEFT JOIN       dim.WMSLocation         AS wl ON wl.WMSLocationCode                = fpi.WMSlocationCode
                                                 AND wl.LocationCode                   = fpi.LocationCode
                                                 AND wl.Partition                      = fpi.PurchaseInvoiceLinePartition
                                                 AND wl.DataConnectionID               = fpi.DataConnectionID
                                                 AND wl.CompanyID                      = fpi.CompanyId
    LEFT JOIN       dim.Location            AS l ON l.LocationCode                     = fpi.LocationCode
                                                AND l.Partition                        = fpi.PurchaseInvoiceLinePartition
                                                AND l.DataConnectionID                 = fpi.DataConnectionID
                                                AND l.CompanyID                        = fpi.CompanyId
    LEFT JOIN       dim.Product             AS p ON p.InventSize                       = fpi.ProductInventSizeCode
                                                AND p.InventColor                      = fpi.ProductColorCode
                                                AND p.InventStyle                      = fpi.ProductStyleCode
                                                AND p.InventConfiguration              = fpi.ProductConfigCode
                                                AND p.Partition                        = fpi.PurchaseInvoiceLinePartition
                                                AND p.CompanyID                        = fpi.CompanyId
                                                AND p.DataConnectionID                 = fpi.DataConnectionID
                                                AND p.CompanyID                        = fpi.CompanyId
    LEFT JOIN       dim.Tracking            AS t ON t.InventBatchCode                  = fpi.TrackingBatchCode
                                                AND t.InventSerialCode                 = fpi.TrackingSerialCode
                                                AND t.Partition                        = fpi.PurchaseInvoiceLinePartition
                                                AND t.CompanyID                        = fpi.CompanyId
                                                AND p.DataConnectionID                 = fpi.DataConnectionID
                                                AND p.CompanyID                        = fpi.CompanyId
    LEFT JOIN       dim.Vendor              AS cAcc ON cAcc.VendorAccountNo            = fpi.VendorAccountCode
                                                   AND cAcc.Partition                  = fpi.PurchaseInvoiceLinePartition
                                                   AND cAcc.DataConnectionID           = fpi.DataConnectionID
                                                   AND cAcc.CompanyID                  = fpi.CompanyId
    LEFT JOIN       dim.Vendor              AS iAcc ON iAcc.VendorAccountNo            = fpi.InvoiceAccountCode
                                                   AND iAcc.Partition                  = fpi.PurchaseInvoiceLinePartition
                                                   AND iAcc.DataConnectionID           = fpi.DataConnectionID
                                                   AND iAcc.CompanyID                  = fpi.CompanyId
    LEFT JOIN       dim.Address             AS dAdr ON dAdr.AddressRecID               = fpi.DeliveryAddressCode
                                                   AND dAdr.Partition                  = fpi.PurchaseInvoiceLinePartition
                                                   AND dAdr.DataConnectionID           = fpi.DataConnectionID
    LEFT JOIN       dim.Item                AS i ON i.ItemNumber                       = fpi.ItemCode
                                                AND i.Partition                        = fpi.PurchaseInvoiceLinePartition
                                                AND i.DataConnectionID                 = fpi.DataConnectionID
                                                AND i.CompanyID                        = fpi.CompanyId
    LEFT JOIN       dim.FinancialDimensions AS fd ON fd.DefaultDimension               = fpi.DefaultDimensionRecId
                                                 AND fd.Partition                      = fpi.PurchaseInvoiceLinePartition
                                                 AND fd.DataConnectionID               = fpi.DataConnectionID
    LEFT JOIN       dim.PaymentTerms        AS pt ON pt.PaymentTermsCode               = fpi.PaymentTermCode
                                                 AND pt.Partition                      = fpi.PurchaseInvoiceLinePartition
                                                 AND pt.DataConnectionID               = fpi.DataConnectionID
                                                 AND pt.CompanyID                      = fpi.CompanyId
    LEFT JOIN       dim.ModeOfDelivery      AS modlv ON modlv.ModeOfDeliveryCode       = fpi.DeliveryModeCode
                                                    AND modlv.Partition                = fpi.PurchaseInvoiceLinePartition
                                                    AND modlv.DataConnectionID         = fpi.DataConnectionID
                                                    AND modlv.CompanyID                = fpi.CompanyId
    LEFT JOIN       dim.DeliveryTerms       AS dt ON dt.DeliveryTermsCode              = fpi.DeliveryTermCode
                                                 AND dt.Partition                      = fpi.PurchaseInvoiceLinePartition
                                                 AND dt.DataConnectionID               = fpi.DataConnectionID
                                                 AND dt.CompanyID                      = fpi.CompanyId
    LEFT JOIN       dim.PurchaseOrder       AS so ON so.PurchaseOrder                  = fpi.PurchaseOrderCode
                                                 AND so.Partition                      = fpi.PurchaseInvoiceLinePartition
                                                 AND so.DataConnectionID               = fpi.DataConnectionID
                                                 AND so.CompanyID                      = fpi.CompanyId
    LEFT JOIN       dim.PurchaseType        AS sot ON sot.PurchaseTypeCode             = fpi.PurchaseOrderTypeCode
                                                  AND sot.DataConnectionID             = fpi.DataConnectionID
    LEFT JOIN       dim.PurchaseInvoice     AS sInv ON sInv.PurchaseInvoice            = fpi.PurchaseInvoice
                                                   AND sInv.CompanyID                  = fpi.CompanyId
                                                   AND sInv.Partition                  = fpi.PurchaseInvoiceLinePartition
                                                   AND sInv.DataConnectionID           = fpi.DataConnectionID
    LEFT JOIN       dim.Currency            AS cy ON cy.Code                           = fpi.CurrencyCode
                                                 AND cy.Partition                      = fpi.PurchaseInvoiceLinePartition
                                                 AND cy.DataConnectionID               = fpi.DataConnectionID
    LEFT JOIN       dim.UnitOfMeasure       AS uom ON uom.UnitOfMeasureCode            = fpi.PurchaseUnitOfMeasureCode
                                                  AND uom.Partition                    = fpi.PurchaseInvoiceLinePartition
                                                  AND uom.DataConnectionID             = fpi.DataConnectionID
    LEFT JOIN       dim.Voucher             AS jv ON jv.Voucher                        = fpi.VoucherCode
                                                 AND jv.CompanyId                      = fpi.CompanyId
                                                 AND jv.Partition                      = fpi.PurchaseInvoiceLinePartition
                                                 AND jv.DataConnectionID               = fpi.DataConnectionID
    LEFT JOIN       dim.MethodOfPayment     AS mop ON mop.MethodOfPaymentCode          = fpi.PaymentModeCode
                                                  AND mop.Partition                    = fpi.PurchaseInvoiceLinePartition
                                                  AND mop.DataConnectionID             = fpi.DataConnectionID
                                                  AND mop.CompanyID                    = fpi.CompanyId
    LEFT JOIN       dim.PurchasePool        AS pp ON pp.PurchasePoolCode               = fpi.PurchasePoolCode
                                                 AND pp.Partition                      = fpi.PurchaseInvoiceLinePartition
                                                 AND pp.DataConnectionID               = fpi.DataConnectionID
                                                 AND pp.CompanyID                      = fpi.CompanyId
    LEFT OUTER JOIN dim.CurrencyReport      AS rcy ON fpi.PurchaseInvoiceLinePartition = rcy.Partition ;




GO


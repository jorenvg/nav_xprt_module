EXEC dbo.drop_object 'delta.GeneralLedger', 'T' ;
GO


CREATE TABLE delta.GeneralLedger
(
  StageId               BIGINT
, ExecutionFlag         NCHAR(1)
, DataConnectionID      INT             NOT NULL
, CompanyID             INT             NOT NULL
, ComponentExecutionID  INT             NOT NULL
, Partition             BIGINT          NULL
, VoucherRecID           BIGINT          NOT NULL
, VoucherVersion        INT             NOT NULL
, LedgerDimension       BIGINT          NULL
, PostingDate           DATETIME        NULL
, Voucher               NVARCHAR(100)   NULL
, Journal               NVARCHAR(100)   NULL
, ReasonRef             BIGINT          NULL
, PostingLayer          INT             NULL
, FiscalCalendarPeriod  BIGINT          NULL
, FiscalCalendar        BIGINT          NULL
, LedgerTransType       INT             NULL
, LedgerChartofAccounts BIGINT          NULL
, MainAccount           NVARCHAR(100)   NULL
, ClosedPeriod          INT             NULL
, Currency              NVARCHAR(10)    NULL
, PostingType           INT             NULL
, IsCredit              INT             NULL
, Balance               INT             NULL
, TransactionAmount     DECIMAL(19,4)   NULL
, TransactionAmount_RCY DECIMAL(19,4)   NULL
, AccountingAmount      DECIMAL(19,4)   NULL
, ReportingAmount       DECIMAL(19,4)   NULL
) ;
GO
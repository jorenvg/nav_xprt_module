EXEC dbo.drop_object 'fact.GeneralLedgerView', 'V' ;
GO

CREATE VIEW fact.GeneralLedgerView
AS
  SELECT            gl.StageId
                  , gl.ExecutionFlag
                  , gl.DataConnectionID
                  , gl.CompanyID
                  , gl.Partition
                  , COALESCE(fd.FinancialDimensionsID, 0)   AS FinancialDimensionsID
                  , COALESCE(pd.DateID, 0)                  AS PostingDateID
                  , COALESCE(jv.VoucherID, 0)               AS VoucherID
                  , COALESCE(r.ReasonID, 0)                 AS ReasonID
                  , COALESCE(pl.PostingLayerID, 0)          AS PostingLayerID
                  , COALESCE(fc.FiscalCalendarID, 0)        AS FiscalCalendarID
                  , COALESCE(lt.LedgerTransactionTypeID, 0) AS LedgerTransactionTypeID
                  , COALESCE(coa.ChartOfAccountsID, 0)      AS ChartOfAccountsID
                  , COALESCE(cp.ClosedPeriodID, 0)          AS ClosedPeriodID
                  , COALESCE(cy.CurrencyID, 0)              AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)             AS CurrencyReportID
                  , COALESCE(pt.PostingTypeID, 0)           AS PostingTypeID
                  , COALESCE(vt.VoucherTextID,0)            AS VoucherTextID
                  , COALESCE(gl.IsCredit, -1)               AS IsCredit
                  , COALESCE(gl.Balance, -1)                AS Balance
                  , gl.TransactionAmount
                  , gl.TransactionAmount_RCY
                  , gl.AccountingAmount
                  , gl.ReportingAmount
    FROM            delta.GeneralLedger       AS gl
    LEFT OUTER JOIN dim.FinancialDimensions   AS fd ON gl.LedgerDimension        = fd.LedgerDimension
                                                   AND gl.DataConnectionID       = fd.DataConnectionID
    LEFT OUTER JOIN dim.Date                  AS pd ON gl.PostingDate            = pd.Date
    LEFT OUTER JOIN dim.Voucher               AS jv ON gl.Voucher                = jv.Voucher
                                                   AND gl.CompanyID              = jv.CompanyID
                                                   AND gl.DataConnectionID       = jv.DataConnectionID
                                                   AND gl.Partition              = jv.Partition
    LEFT OUTER JOIN dim.VoucherText           AS vt ON gl.VoucherRecID            = vt.VoucherCode
                                                    AND gl.VoucherVersion        = vt.VoucherVersion 
                                                    AND gl.DataConnectionID      = vt.DataConnectionID
                                                    AND gl.Partition             = vt.Partition
                                                    AND gl.CompanyID             = vt.CompanyID
    LEFT OUTER JOIN dim.Reason                AS r ON gl.ReasonRef               = r.ReasonRefRecId
                                                  AND gl.DataConnectionID        = r.DataConnectionID
                                                  AND gl.Partition               = r.Partition
    LEFT OUTER JOIN dim.PostingLayer          AS pl ON gl.PostingLayer           = pl.PostingLayerCode
                                                   AND gl.DataConnectionID       = pl.DataConnectionID
    LEFT OUTER JOIN dim.FiscalCalendar        AS fc ON gl.FiscalCalendarPeriod   = fc.FiscalCalendarPeriodRecID
                                                   AND gl.FiscalCalendar         = fc.FiscalCalendarRecID
                                                   AND gl.DataConnectionID       = fc.DataConnectionID
                                                   AND gl.Partition              = fc.Partition
    LEFT OUTER JOIN dim.LedgerTransactionType AS lt ON gl.LedgerTransType        = lt.LedgerTransactionTypeCode
                                                   AND gl.DataConnectionID       = lt.DataConnectionID
    LEFT OUTER JOIN dim.ChartOfAccounts       AS coa ON gl.LedgerChartofAccounts = coa.ChartOfAccountsCode
                                                    AND gl.MainAccount           = coa.MainAccountCode
                                                    AND gl.DataConnectionID      = coa.DataConnectionID
                                                    AND gl.Partition             = coa.Partition
    LEFT OUTER JOIN dim.ClosedPeriod          AS cp ON gl.ClosedPeriod           = cp.ClosedPeriodCode
                                                   AND gl.DataConnectionID       = cp.DataConnectionID
    LEFT OUTER JOIN dim.Currency              AS cy ON gl.Currency               = cy.Code
                                                   AND gl.DataConnectionID       = cy.DataConnectionID
                                                   AND gl.Partition              = cy.Partition
    LEFT OUTER JOIN dim.PostingType           AS pt ON gl.PostingType            = pt.PostingTypeCode
                                                   AND gl.DataConnectionID       = pt.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport        AS rcy ON gl.Partition             = rcy.Partition
                                                   
GO
EXEC dbo.drop_object 'fact.GeneralLedger', 'T' ;
GO
CREATE TABLE fact.GeneralLedger
(
  DataConnectionID        INT             NOT NULL
, CompanyID               INT             NOT NULL
, ComponentExecutionID    INT             NOT NULL
, Partition               BIGINT          NULL
, FinancialDimensionsID   INT             NOT NULL
, PostingDateID           INT             NOT NULL
, VoucherID               INT             NOT NULL
, ReasonID                INT             NOT NULL
, PostingLayerID          INT             NOT NULL
, FiscalCalendarID        INT             NOT NULL
, LedgerTransactionTypeID INT             NOT NULL
, ChartOfAccountsID       INT             NOT NULL
, ClosedPeriodID          INT             NOT NULL
, CurrencyTransactionID   INT             NOT NULL
, CurrencyReportID        INT             NOT NULL
, PostingTypeID           INT             NOT NULL
, VoucherTextID           INT             NOT NULL
, IsCredit                INT             NULL
, Balance                 INT             NULL
, TransactionAmount       DECIMAL(19,4)   NULL
, TransactionAmount_RCY   DECIMAL(19,4)   NULL
, AccountingAmount        DECIMAL(19,4)   NULL
, ReportingAmount         DECIMAL(19,4)   NULL
, StageId                 BIGINT
) ;
GO
/**********************************************************************************************
    Date            Changed by              Ticket/Change       Description
    02-11-2018      Simon van den Berg      DEV-1357            Remove join on ledger companyid
**********************************************************************************************/

EXEC dbo.drop_object 'delta.GeneralLedgerFunction', 'F' ;
GO
CREATE FUNCTION delta.GeneralLedgerFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            GJAE.stage_id                                      AS StageId
                           , @execution_flag                                    AS ExecutionFlag
                           , GJAE.LEDGERDIMENSION                               AS LedgerDimension
                           , GJE.ACCOUNTINGDATE                                 AS PostingDate
                           , GJE.SUBLEDGERVOUCHER                               AS Voucher
                           , GJAE.RECID                                         AS VoucherRecID
                           , GJAE.RECVERSION                                    AS VoucherVersion
                           , GJE.JOURNALNUMBER                                  AS Journal
                           , GJAE.REASONREF                                     AS ReasonRef
                           , GJE.POSTINGLAYER                                   AS PostingLayer
                           , GJE.FISCALCALENDARPERIOD                           AS FiscalCalendarPeriod
                           , FCP.FISCALCALENDAR                                 AS FiscalCalendar
                           , GJE.JOURNALCATEGORY                                AS LedgerTransType
                           , L.CHARTOFACCOUNTS                                  AS LedgerChartofAccounts
                           , MA.MAINACCOUNTID                                   AS MainAccount
                           , LFCP.STATUS                                        AS ClosedPeriod
                           , GJAE.TRANSACTIONCURRENCYCODE                       AS Currency
                           , GJAE.POSTINGTYPE                                   AS PostingType
                           , GJAE.ISCREDIT                                      AS IsCredit
                           , CASE WHEN MA.TYPE IN ( 0, 1, 2 ) THEN 0 ELSE 1 END AS Balance
                           , GJAE.TRANSACTIONCURRENCYAMOUNT                     AS TransactionAmount
                           , GJAE.TRANSACTIONCURRENCYAMOUNT * ex.CrossRate      AS TransactionAmount_RCY
                           , GJAE.ACCOUNTINGCURRENCYAMOUNT                      AS AccountingAmount
                           , GJAE.REPORTINGCURRENCYAMOUNT                       AS ReportingAmount
                           , GJAE.company_id                                    AS CompanyID
                           , GJAE.data_connection_id                            AS DataConnectionID
                           , GJAE.PARTITION                                     AS Partition
                           , @component_execution_id                            AS ComponentExecutionID
             FROM            stage_ax.GENERALJOURNALACCOUNTENTRY         AS GJAE
            INNER JOIN       stage_ax.GENERALJOURNALENTRY                AS GJE ON GJAE.GENERALJOURNALENTRY    = GJE.RECID
                                                                               AND GJAE.company_id             = GJE.company_id
                                                                               AND GJAE.data_connection_id     = GJE.data_connection_id
                                                                               AND GJAE.PARTITION              = GJE.PARTITION
             LEFT OUTER JOIN stage_ax.DIMENSIONATTRIBUTEVALUECOMBINATION AS DAVC ON GJAE.LEDGERDIMENSION       = DAVC.RECID
                                                                                AND GJAE.data_connection_id    = DAVC.data_connection_id
                                                                                AND GJAE.PARTITION             = DAVC.PARTITION
             LEFT OUTER JOIN stage_ax.MAINACCOUNT                        AS MA ON DAVC.MAINACCOUNT             = MA.RECID
                                                                              AND GJAE.data_connection_id      = MA.data_connection_id
                                                                              AND GJAE.PARTITION               = MA.PARTITION
             LEFT OUTER JOIN stage_ax.LEDGER                             AS L ON GJE.LEDGER                    = L.RECID
                                                                             AND GJAE.data_connection_id       = L.data_connection_id
                                                                             AND GJAE.PARTITION                = L.PARTITION
             LEFT OUTER JOIN stage_ax.FISCALCALENDARPERIOD               AS FCP ON GJE.FISCALCALENDARPERIOD    = FCP.RECID
                                                                               AND GJAE.data_connection_id     = FCP.data_connection_id
                                                                               AND GJAE.PARTITION              = FCP.PARTITION
             LEFT OUTER JOIN stage_ax.LEDGERFISCALCALENDARPERIOD         AS LFCP ON L.RECID                    = LFCP.LEDGER
                                                                                AND FCP.RECID                  = LFCP.FISCALCALENDARPERIOD
                                                                                AND GJAE.data_connection_id    = LFCP.data_connection_id
                                                                                AND GJAE.PARTITION             = LFCP.PARTITION
             LEFT JOIN       reporting_exchange_rates                    AS ex ON GJAE.TRANSACTIONCURRENCYCODE = ex.FromCurrencyCode
                                                                              AND GJAE.data_connection_id      = ex.DataConnectionID
                                                                              AND GJE.ACCOUNTINGDATE BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE
             /** FULL LOAD **/
                             ( @load_type          = 0
                           AND GJAE.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                        = 1
                           AND @execution_flag                   = 'N'
                           AND GJAE.execution_flag               = @execution_flag
                           AND GJAE.execution_timestamp          > @last_processed_timestamp)
               OR
               /** INCREMENTAL, UPDATED RECORDS **/
                             ( @load_type                        = 1
                           AND @execution_flag                   = 'U'
                           AND ( ( GJAE.execution_flag           = @execution_flag
                               AND GJAE.execution_timestamp      > @last_processed_timestamp)
                              OR ( GJE.execution_flag            = @execution_flag
                               AND GJE.execution_timestamp       > @last_processed_timestamp)
                              OR ( DAVC.execution_flag           = @execution_flag
                               AND DAVC.execution_timestamp      > @last_processed_timestamp)
                              OR ( MA.execution_flag             = @execution_flag
                               AND MA.execution_timestamp        > @last_processed_timestamp)
                              OR ( L.execution_flag              = @execution_flag
                               AND L.execution_timestamp         > @last_processed_timestamp)
                              OR ( L.execution_flag              = @execution_flag
                               AND L.execution_timestamp         > @last_processed_timestamp)
                              OR ( FCP.execution_flag            = @execution_flag
                               AND FCP.execution_timestamp       > @last_processed_timestamp)
                              OR ( LFCP.execution_flag           = @execution_flag
                               AND LFCP.execution_timestamp      > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                        = 1
                           AND @execution_flag                   = 'D'
                           AND GJAE.execution_flag               = @execution_flag
                           AND GJAE.execution_timestamp          > @last_processed_timestamp)) ;

GO
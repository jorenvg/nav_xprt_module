EXEC dbo.drop_object 'delta.GeneralLedgerTax', 'T' ;
GO


CREATE TABLE delta.GeneralLedgerTax
(
  StageId                      BIGINT          NOT NULL
, ExecutionFlag                NCHAR(1)        NOT NULL
, ComponentExecutionId         INT             NOT NULL
, DataConnectionID             INT             NOT NULL
, CompanyID                    INT             NOT NULL
, Partition                    BIGINT          NULL
, PostingDate                  DATETIME        NULL
, Voucher                      NVARCHAR(30)    NULL
, TaxCurrency                  NVARCHAR(3)     NULL
, TaxBaseTaxCurrency           NVARCHAR(3)     NULL
, TaxDirection                 INT             NULL
, TaxAuthority                 NVARCHAR(30)    NULL
, ExemptCode                   NVARCHAR(30)    NULL
, TaxCode                      NVARCHAR(30)    NULL
, TaxGroup                     NVARCHAR(30)    NULL
, TaxItemGroup                 NVARCHAR(30)    NULL
, TaxBaseAccountingAmount      DECIMAL(19,4)   NULL
, TaxBaseTransactionAmount     DECIMAL(19,4)   NULL
, TaxBaseTransactionAmount_RCY DECIMAL(19,4)   NULL
, TaxBaseAmount                DECIMAL(19,4)   NULL
, TaxBaseAmount_RCY            DECIMAL(19,4)   NULL
, TaxAccountingAmount          DECIMAL(19,4)   NULL
, TaxTransactionAmount         DECIMAL(19,4)   NULL
, TaxTransactionAmount_RCY     DECIMAL(19,4)   NULL
, TaxAmount                    DECIMAL(19,4)   NULL
, TaxAmount_RCY                DECIMAL(19,4)   NULL
) ;
GO                             
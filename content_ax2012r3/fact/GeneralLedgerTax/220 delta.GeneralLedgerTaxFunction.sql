EXEC dbo.drop_object 'delta.GeneralLegderTaxFunction', 'F' ;
GO
CREATE FUNCTION delta.GeneralLegderTaxFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            TT.stage_id                                AS StageId
                           , @execution_flag                            AS ExecutionFlag
                           , @component_execution_id                    AS ComponentExecutionId
                           , TT.data_connection_id                      AS DataConnectionID
                           , TT.company_id                              AS CompanyID
                           , TT.PARTITION                               AS Partition
                           , TT.TransDate                               AS PostingDate
                           , TT.Voucher                                 AS Voucher
                           , TT.CurrencyCode                            AS TaxCurrency
                           , TT.SourceCurrencyCode                      AS TaxBaseTaxCurrency
                           , TT.TaxDirection                            AS TaxDirection
                           , TPH.TaxAuthority                           AS TaxAuthority
                           , TT.ExemptCode                              AS ExemptCode
                           , TT.TaxCode                                 AS TaxCode
                           , TT.TaxGroup                                AS TaxGroup
                           , TT.TaxItemGroup                            AS TaxItemGroup
                           , TT.TaxBaseAmount                           AS TaxBaseAccountingAmount
                           , TT.TaxBaseAmountCur                        AS TaxBaseTransactionAmount
                           , TT.TaxBaseAmountCur * ex_base.CrossRate    AS TaxBaseTransactionAmount_RCY
                           , TT.SourceBaseAmountCur                     AS TaxBaseAmount
                           , TT.SourceBaseAmountCur * ex_base.CrossRate AS TaxBaseAmount_RCY
                           , TT.TaxAmount                               AS TaxAccountingAmount
                           , TT.TaxAmountCur                            AS TaxTransactionAmount
                           , TT.TaxAmountCur * ex_base.CrossRate        AS TaxTransactionAmount_RCY
                           , TT.SourceTaxAmountCur                      AS TaxAmount
                           , TT.SourceTaxAmountCur * ex_base.CrossRate  AS TaxAmount_RCY
             FROM            Stage_ax.TaxTrans        AS TT
             LEFT OUTER JOIN Stage_ax.TaxPeriodHead   AS TPH ON TT.TAXPERIOD              = TPH.TAXPERIOD
                                                            AND TT.PARTITION              = TPH.PARTITION
                                                            AND TT.company_id             = TPH.company_id
                                                            AND TT.data_connection_id     = TPH.data_connection_id
             LEFT JOIN       reporting_exchange_rates AS ex ON TT.CURRENCYCODE            = ex.FromCurrencyCode
                                                           AND TT.data_connection_id      = ex.DataConnectionID
                                                           AND TT.PARTITION               = ex.Partition
                                                           AND TT.TRANSDATE BETWEEN ex.ValidFrom AND ex.ValidTo
             LEFT JOIN       reporting_exchange_rates AS ex_base ON TT.SOURCECURRENCYCODE = ex_base.FromCurrencyCode
                                                                AND TT.data_connection_id = ex_base.DataConnectionID
                                                                AND TT.TRANSDATE BETWEEN ex_base.ValidFrom AND ex_base.ValidTo
            WHERE
             /** FULL LOAD **/
                             ( @load_type        = 0
                           AND TT.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'N'
                           AND TT.execution_flag               = @execution_flag
                           AND TT.execution_timestamp          > @last_processed_timestamp)
               OR
               /** INCREMENTAL, UPDATED RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'U'
                           AND ( ( TT.execution_flag           = @execution_flag
                               AND TT.execution_timestamp      > @last_processed_timestamp)
                              OR ( TPH.execution_flag          = @execution_flag
                               AND TPH.execution_timestamp     > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'D'
                           AND TT.execution_flag               = @execution_flag
                           AND TT.execution_timestamp          > @last_processed_timestamp)) ;

GO
EXEC dbo.drop_object 'fact.GeneralLedgerTax', 'T' ;
GO
CREATE TABLE fact.GeneralLedgerTax
(
  StageId                      BIGINT          NOT NULL
, ComponentExecutionId         INT             NOT NULL
, DataConnectionID             INT             NOT NULL
, CompanyID                    INT             NOT NULL
, Partition                    BIGINT          NOT NULL
, PostingDateID                INT             NULL
, VoucherID                    INT             NOT NULL
, CurrencyTransactionID        INT             NOT NULL
, CurrencyReportID             INT             NOT NULL
, TaxBaseTaxCurrencyID         INT             NOT NULL
, SalesTaxDirectionID          INT             NOT NULL
, TaxAuthorityID               INT             NOT NULL
, TaxExemptID                  INT             NOT NULL
, TaxID                        INT             NOT NULL
, TaxBaseAccountingAmount      DECIMAL(19,4)   NULL
, TaxBaseTransactionAmount     DECIMAL(19,4)   NULL
, TaxBaseTransactionAmount_RCY DECIMAL(19,4)   NULL
, TaxBaseAmount                DECIMAL(19,4)   NULL
, TaxBaseAmount_RCY            DECIMAL(19,4)   NULL
, TaxAccountingAmount          DECIMAL(19,4)   NULL
, TaxTransactionAmount         DECIMAL(19,4)   NULL
, TaxTransactionAmount_RCY     DECIMAL(19,4)   NULL
, TaxAmount                    DECIMAL(19,4)   NULL
, TaxAmount_RCY                DECIMAL(19,4)   NULL
) ;
GO
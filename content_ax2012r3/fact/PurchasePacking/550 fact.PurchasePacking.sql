EXEC dbo.drop_object 'fact.PurchasePacking', 'T' ;
GO
CREATE TABLE [fact].[PurchasePacking]
(
  StageID                         BIGINT            NOT NULL
, [CompanyID]                     [INT]             NOT NULL
, [DataConnectionID]              [INT]             NOT NULL
, [ComponentExecutionID]          INT               NOT NULL
, [PurchasePackingTransRecID]     [BIGINT]          NULL
, [PurchasePackingTransPartition] [BIGINT]          NULL
, [DocumentDateID]                [INT]             NULL
, [ShippingExpectedDateID]        [INT]             NULL
, [DeliveryDateID]                [INT]             NULL
, [WMSlocationID]                 [INT]             NULL
, [LocationID]                    [INT]             NULL
, [ProductID]                     [INT]             NULL
, [TrackingID]                    [INT]             NULL
, [PurchaserID]                   [INT]             NULL
, [PurchaseRequesterID]           [INT]             NULL
, [VendorAccountID]               [INT]             NULL
, [InvoiceAccountID]              [INT]             NULL
, [DeliveryAddressID]             [INT]             NULL
, [ItemID]                        [INT]             NULL
, [CurrencyID]                    [INT]             NULL
, [FinancialDimensionsID]         [INT]             NULL
, [PurchaseTypeID]                [INT]             NULL
, [PurchasePoolID]                [INT]             NULL
, [ModeOfDeliveryID]              [INT]             NULL
, [DeliveryTermsID]               [INT]             NULL
, [MethodOfPaymentID]             [INT]             NULL
, [PaymentTermsID]                [INT]             NULL
, [PurchaseOrderID]               [INT]             NULL
, [PurchaseUnitID]                [INT]             NULL
, [PurchasePackingSlipID]         [INT]             NULL
, [OrderedQuantity]               DECIMAL(19,4)     NULL
, [RemainingQuantity]             DECIMAL(19,4)     NULL
, [DeliveredQuantity]             DECIMAL(19,4)     NULL
, [InventoryQuantity]             DECIMAL(19,4)     NULL
, [InventoryQuantityOpen]         DECIMAL(19,4)     NULL
, [ValueMST]                      DECIMAL(19,4)     NULL
) ;
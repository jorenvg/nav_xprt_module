EXEC dbo.drop_object 'delta.PurchasePackingFunction', 'F' ;
GO
CREATE FUNCTION delta.PurchasePackingFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            VPST.stage_id                                    AS StageId
                           , @execution_flag                                  AS ExecutionFlag
                           , VPST.company_id                                  AS CompanyID
                           , VPST.data_connection_id                          AS DataConnectionID
                           , @component_execution_id                          AS ComponentExecutionId
                           , VPST.RECID                                       AS PurchasePackingTransRecId
                           , VPST.PARTITION                                   AS PurchasePackingTransPartition
                           , CAST(VPSJ.DOCUMENTDATE AS DATE)                  AS DocumentDate
                           , VPST.PURCHASELINEEXPECTEDDELIVERYDATE            AS ShippingExpectedDate
                           , VPST.DELIVERYDATE                                AS DeliveryDate
                           , COALESCE(NULLIF(ID.WMSLOCATIONID, ''), 'N/A')    AS WMSlocationCode
                           , COALESCE(NULLIF(ID.INVENTLOCATIONID, ''), 'N/A') AS LocationCode
                           , COALESCE(NULLIF(ID.CONFIGID, ''), 'N/A')         AS ProductConfigCode
                           , COALESCE(NULLIF(ID.INVENTCOLORID, ''), 'N/A')    AS ProductColorCode
                           , COALESCE(NULLIF(ID.INVENTSIZEID, ''), 'N/A')     AS ProductInventSizeCode
                           , COALESCE(NULLIF(ID.INVENTSTYLEID, ''), 'N/A')    AS ProductStyleCode
                           , COALESCE(NULLIF(ID.INVENTBATCHID, ''), 'N/A')    AS TrackingBatchCode
                           , COALESCE(NULLIF(ID.INVENTSERIALID, ''), 'N/A')   AS TrackingSerialCode
                           , VPST.WORKERPURCHASER                             AS PurchaserCode
                           , VPSJ.REQUESTER                                   AS PurchaseRequesterCode
                           , VPSJ.ORDERACCOUNT                                AS VendorAccountCode
                           , VPSJ.INVOICEACCOUNT                              AS InvoiceAccountCode
                           , VPSJ.DELIVERYPOSTALADDRESS                       AS DeliveryAddressCode
                           , VPST.DATAAREAID                                  AS CompanyCode
                           , VPST.ITEMID                                      AS ItemCode
                           , VPST.CURRENCYCODE_W                              AS CurrencyCode
                           , VPST.DEFAULTDIMENSION                            AS FinancialDimensionCode
                           , VPSJ.PURCHASETYPE                                AS OrderTypeCode
                           , PT.PURCHPOOLID                                   AS PurchasePoolCode
                           , VPSJ.DLVMODE                                     AS DeliveryModeCode
                           , VPSJ.DLVTERM                                     AS DeliveryTermsCode
                           , PT.PAYMMODE                                      AS PaymentModeCode
                           , PT.PAYMENT                                       AS PaymentTermsCode
                           , VPST.ORIGPURCHID                                 AS PurchaseCode
                           , VPST.PACKINGSLIPID                               AS PurchasePackingSlip
                           , VPST.PURCHUNIT                                   AS PurchaseUnitCode
                           , VPST.ORDERED                                     AS OrderedQuantity
                           , VPST.REMAIN                                      AS RemainingQuantity
                           , VPST.QTY                                         AS DeliveredQuantity
                           , VPST.InventQty                                   AS InventoryQuantity
                           , VPST.RemainInvent                                AS InventoryQuantityOpen
                           , VPST.VALUEMST                                    AS ValueMST
             FROM            stage_ax.VENDPACKINGSLIPTRANS AS VPST
            INNER JOIN       stage_ax.VENDPACKINGSLIPJOUR  VPSJ ON VPST.PACKINGSLIPID      = VPSJ.PACKINGSLIPID
                                                               AND VPST.ORIGPURCHID        = VPSJ.PURCHID
                                                               AND VPST.DELIVERYDATE       = VPSJ.DELIVERYDATE
                                                               AND VPST.data_connection_id = VPSJ.data_connection_id
                                                               AND VPST.company_id         = VPSJ.company_id
             LEFT OUTER JOIN stage_ax.INVENTDIM            ID ON VPST.INVENTDIMID          = ID.INVENTDIMID
                                                             AND VPST.DATAAREAID           = ID.DATAAREAID
                                                             AND ID.company_id             = VPST.company_id
                                                             AND ID.data_connection_id     = VPST.data_connection_id
             LEFT JOIN       stage_ax.PURCHTABLE           PT ON VPSJ.PURCHID              = PT.PURCHID
                                                             AND VPSJ.data_connection_id   = PT.data_connection_id
                                                             AND VPSJ.company_id           = PT.company_id
                                                             AND VPSJ.PARTITION            = PT.PARTITION
            WHERE
             /** FULL LOAD **/
                             ( @load_type          = 0
                           AND VPST.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                        = 1
                           AND @execution_flag                   = 'N'
                           AND VPST.execution_flag               = @execution_flag
                           AND VPST.execution_timestamp          > @last_processed_timestamp)
               OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( VPST.execution_flag           = @execution_flag
                             AND VPST.execution_timestamp      > @last_processed_timestamp)
                            OR ( ID.execution_flag            = @execution_flag
                             AND ID.execution_timestamp       > @last_processed_timestamp)
                            OR ( PT.execution_flag          = @execution_flag
                             AND PT.execution_timestamp     > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                        = 1
                           AND @execution_flag                   = 'D'
                           AND VPST.execution_flag               = @execution_flag
                           AND VPST.execution_timestamp          > @last_processed_timestamp)) ;
GO

EXEC dbo.drop_object @object = N'fact.PurchasePackingView', @type = N'V' ;
GO
CREATE VIEW fact.PurchasePackingView
AS
  SELECT      StageID                = PP.StageID
            , ExecutionFlag          = PP.ExecutionFlag
            , PP.PurchasePackingTransRecID
            , PP.PurchasePackingTransPartition
            , PP.CompanyID
            , PP.DataConnectionID

            /***Dimensions*/
            -- Dates
            , DocumentDateID         = COALESCE(cd.DateID, 0)
            , ShippingExpectedDateID = COALESCE(srd.DateID, 0)
            , DeliveryDateID         = COALESCE(dd.DateID, 0)

            -- inventdimension
            , WMSlocationID          = COALESCE(wl.WMSLocationID, 0)
            , LocationID             = COALESCE(l.LocationID, 0)
            , ProductID              = COALESCE(p.ProductID, 0)
            , TrackingID             = COALESCE(t.TrackingID, 0)

            -- Employee
            , PurchaserID            = COALESCE(sTkr.EmployeeID, 0)
            , PurchaseRequesterID    = COALESCE(req.EmployeeID, 0)

            --Vendor
            , VendorAccountID        = COALESCE(cAcc.VendorID, 0)
            , InvoiceAccountID       = COALESCE(iAcc.VendorID, 0)

            -- Address
            , DeliveryAddressID      = COALESCE(dAdr.AddressID, 0)

            -- Item
            , ItemID                 = COALESCE(i.ItemID, 0)

            -- Currency
            , CurrencyID             = COALESCE(curr.CurrencyID, 0)

            -- Financialdimension
            , FinancialDimensionsID  = COALESCE(fd.FinancialDimensionsID, 0)
            , PurchaseTypeID         = COALESCE(sot.PurchaseTypeID, 0)
            , PurchasePoolID         = COALESCE(sp.PurchasePoolID, 0)
            , ModeOfDeliveryID       = COALESCE(modlv.ModeOfDeliveryID, 0)
            , DeliveryTermsID        = COALESCE(dt.DeliveryTermsID, 0)
            , MethodOfPaymentID      = COALESCE(mop.MethodOfPaymentID, 0)
            , PaymentTermsID         = COALESCE(pt.PaymentTermsID, 0)
            , PurchaseOrderID        = COALESCE(so.PurchaseOrderID, 0)
            , PurchaseUnitID         = COALESCE(uom.UnitOfMeasureID, 0)
            , PurchasePackingSlipID  = COALESCE(sps.PurchasePackingSlipID, 0)

            /***Measures***/
            , OrderedQuantity        = PP.OrderedQuantity
            , RemainingQuantity      = PP.RemainingQuantity
            , DeliveredQuantity      = PP.DeliveredQuantity
            , InventoryQuantity      = PP.InventoryQuantity
            , InventoryQuantityOpen  = PP.InventoryQuantityOpen
            , ValueMST               = PP.ValueMST

    --SELECT *
    FROM      delta.PurchasePacking   AS PP
    LEFT JOIN dim.Date                AS cd ON cd.Date                     = PP.DocumentDate
    LEFT JOIN dim.Date                AS srd ON srd.Date                   = PP.ShippingExpectedDate
    LEFT JOIN dim.Date                AS dd ON dd.Date                     = PP.DeliveryDate
    LEFT JOIN dim.WMSLocation         AS wl ON wl.WMSLocationCode          = PP.WMSlocationCode
                                           AND wl.LocationCode             = PP.LocationCode
                                           AND wl.Partition                = PP.PurchasePackingTransPartition
                                           AND wl.DataConnectionID         = PP.DataConnectionID
                                           AND wl.CompanyID                = PP.CompanyID
    LEFT JOIN dim.Location            AS l ON l.LocationCode               = PP.LocationCode
                                          AND l.Partition                  = PP.PurchasePackingTransPartition
                                          AND l.DataConnectionID           = PP.DataConnectionID
                                          AND l.CompanyID                  = PP.CompanyID
    LEFT JOIN dim.Product             AS p ON p.InventSize                 = PP.ProductInventSizeCode
                                          AND p.InventColor                = PP.ProductColorCode
                                          AND p.InventStyle                = PP.ProductStyleCode
                                          AND p.InventConfiguration        = PP.ProductConfigCode
                                          AND p.Partition                  = PP.PurchasePackingTransPartition
                                          AND p.DataConnectionID           = PP.DataConnectionID
                                          AND p.CompanyID                  = PP.CompanyID
    LEFT JOIN dim.Tracking            AS t ON t.InventBatchCode            = PP.TrackingBatchCode
                                          AND t.InventSerialCode           = PP.TrackingSerialCode
                                          AND t.Partition                  = PP.PurchasePackingTransPartition
                                          AND t.CompanyID                  = PP.CompanyID
                                          AND p.DataConnectionID           = PP.DataConnectionID
                                          AND p.CompanyID                  = PP.CompanyID
    LEFT JOIN dim.Vendor              AS cAcc ON cAcc.VendorAccountNo      = PP.VendorAccountCode
                                             AND cAcc.Partition            = PP.PurchasePackingTransPartition
                                             AND cAcc.DataConnectionID     = PP.DataConnectionID
                                             AND cAcc.CompanyID            = PP.CompanyID
    LEFT JOIN dim.Vendor              AS iAcc ON iAcc.VendorAccountNo      = PP.InvoiceAccountCode
                                             AND iAcc.Partition            = PP.PurchasePackingTransPartition
                                             AND iAcc.DataConnectionID     = PP.DataConnectionID
                                             AND iAcc.CompanyID            = PP.CompanyID
    LEFT JOIN dim.Address             AS dAdr ON dAdr.AddressRecID         = PP.DeliveryAddressCode
                                             AND dAdr.Partition            = PP.PurchasePackingTransPartition
                                             AND dAdr.DataConnectionID     = PP.DataConnectionID
    LEFT JOIN dim.Address             AS iAdr ON iAdr.AddressRecID         = PP.DeliveryAddressCode
                                             AND iAdr.Partition            = PP.PurchasePackingTransPartition
                                             AND iAdr.DataConnectionID     = PP.DataConnectionID
    LEFT JOIN dim.Item                AS i ON i.ItemNumber                 = PP.ItemCode
                                          AND i.Partition                  = PP.PurchasePackingTransPartition
                                          AND i.DataConnectionID           = PP.DataConnectionID
                                          AND i.CompanyID                  = PP.CompanyID
    LEFT JOIN dim.FinancialDimensions AS fd ON fd.DefaultDimension         = PP.FinancialDimensionCode
                                           AND fd.Partition                = PP.PurchasePackingTransPartition
                                           AND fd.DataConnectionID         = PP.DataConnectionID
    LEFT JOIN dim.ModeOfDelivery      AS modlv ON modlv.ModeOfDeliveryCode = PP.DeliveryModeCode
                                              AND modlv.Partition          = PP.PurchasePackingTransPartition
                                              AND modlv.DataConnectionID   = PP.DataConnectionID
                                              AND modlv.CompanyID          = PP.CompanyID
    LEFT JOIN dim.DeliveryTerms       AS dt ON dt.DeliveryTermsCode        = PP.DeliveryTermsCode
                                           AND dt.Partition                = PP.PurchasePackingTransPartition
                                           AND dt.DataConnectionID         = PP.DataConnectionID
                                           AND dt.CompanyID                = PP.CompanyID
    LEFT JOIN dim.PurchaseOrder       AS so ON so.PurchaseOrder            = PP.PurchaseCode
                                           AND so.Partition                = PP.PurchasePackingTransPartition
                                           AND so.DataConnectionID         = PP.DataConnectionID
                                           AND so.CompanyID                = PP.CompanyID
    LEFT JOIN dim.PurchaseType        AS sot ON sot.PurchaseTypeCode       = PP.OrderTypeCode
                                            AND sot.DataConnectionID       = PP.DataConnectionID
    LEFT JOIN dim.PurchasePackingSlip AS sps ON sps.PurchasePackingSlip    = PP.PurchasePackingSlip
                                            AND sps.CompanyID              = PP.CompanyID
                                            AND sps.Partition              = PP.PurchasePackingTransPartition
                                            AND sps.DataConnectionID       = PP.DataConnectionID
    LEFT JOIN dim.Employee            AS sTkr ON sTkr.EmployeeRecID        = PP.PurchaserCode
                                             AND sTkr.Partition            = PP.PurchasePackingTransPartition
                                             AND sTkr.DataConnectionID     = PP.DataConnectionID
    LEFT JOIN dim.Employee            AS req ON req.EmployeeRecID          = PP.PurchaseRequesterCode
                                            AND req.Partition              = PP.PurchasePackingTransPartition
                                            AND req.DataConnectionID       = PP.DataConnectionID
    LEFT JOIN dim.Currency            AS curr ON curr.Code                 = PP.CurrencyCode
                                             AND curr.Partition            = PP.PurchasePackingTransPartition
                                             AND curr.DataConnectionID     = PP.DataConnectionID
    LEFT JOIN dim.UnitOfMeasure       AS uom ON uom.UnitOfMeasureCode      = PP.PurchaseUnitCode
                                            AND uom.Partition              = PP.PurchasePackingTransPartition
                                            AND uom.DataConnectionID       = PP.DataConnectionID
    LEFT JOIN dim.PurchasePool        sp ON PP.PurchasePoolCode            = sp.PurchasePoolCode
                                        AND sp.Partition                   = PP.PurchasePackingTransPartition
                                        AND PP.DataConnectionID            = sp.DataConnectionID
                                        AND PP.CompanyID                   = sp.CompanyID
    LEFT JOIN dim.MethodOfPayment     mop ON mop.MethodOfPaymentCode       = PP.PaymentModeCode
                                         AND mop.Partition                 = PP.PurchasePackingTransPartition
                                         AND mop.DataConnectionID          = PP.DataConnectionID
                                         AND mop.CompanyID                 = PP.CompanyID
    LEFT JOIN dim.PaymentTerms        AS pt ON pt.PaymentTermsCode         = PP.PaymentTermsCode
                                           AND pt.Partition                = PP.PurchasePackingTransPartition
                                           AND pt.DataConnectionID         = PP.DataConnectionID
                                           AND pt.CompanyID                = PP.CompanyID ;
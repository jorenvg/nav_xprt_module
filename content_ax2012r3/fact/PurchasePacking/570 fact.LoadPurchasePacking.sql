EXEC dbo.drop_object @object = N'fact.LoadPurchasePacking' -- nvarchar(128)
                   , @type = N'P'                          -- nchar(2)
                   , @debug = 0 ;                          -- int
GO

CREATE PROCEDURE fact.LoadPurchasePacking
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @table_name SYSNAME = N'PurchasePacking' ;
  DECLARE @delta_table_identifier SYSNAME = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier SYSNAME = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sp_executeSQL @SQL
                   , @parmas
                   , @Delta_Inserts = @Delta_Inserts OUTPUT
                   , @Delta_Updates = @Delta_Updates OUTPUT
                   , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.PurchasePacking ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO [fact].[PurchasePacking] (
      StageID
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , PurchasePackingTransRecID
    , PurchasePackingTransPartition
    , DocumentDateID
    , ShippingExpectedDateID
    , DeliveryDateID
    , WMSlocationID
    , LocationID
    , ProductID
    , TrackingID
    , PurchaserID
    , PurchaseRequesterID
    , VendorAccountID
    , InvoiceAccountID
    , DeliveryAddressID
    , ItemID
    , CurrencyID
    , FinancialDimensionsID
    , PurchaseTypeID
    , PurchasePoolID
    , ModeOfDeliveryID
    , DeliveryTermsID
    , MethodOfPaymentID
    , PaymentTermsID
    , PurchaseOrderID
    , PurchaseUnitID
    , PurchasePackingSlipID
    , OrderedQuantity
    , RemainingQuantity
    , DeliveredQuantity
    , InventoryQuantity
    , InventoryQuantityOpen
    , ValueMST
    )
    SELECT StageID
         , CompanyID
         , DataConnectionID
         , @component_execution_id
         , PurchasePackingTransRecID
         , PurchasePackingTransPartition
         , DocumentDateID
         , ShippingExpectedDateID
         , DeliveryDateID
         , WMSlocationID
         , LocationID
         , ProductID
         , TrackingID
         , PurchaserID
         , PurchaseRequesterID
         , VendorAccountID
         , InvoiceAccountID
         , DeliveryAddressID
         , ItemID
         , CurrencyID
         , FinancialDimensionsID
         , PurchaseTypeID
         , PurchasePoolID
         , ModeOfDeliveryID
         , DeliveryTermsID
         , MethodOfPaymentID
         , PaymentTermsID
         , PurchaseOrderID
         , PurchaseUnitID
         , PurchasePackingSlipID
         , OrderedQuantity
         , RemainingQuantity
         , DeliveredQuantity
         , InventoryQuantity
         , InventoryQuantityOpen
         , ValueMST
      FROM [fact].[PurchasePackingView] ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      [fact].[PurchasePacking]
         SET      CompanyID = V.CompanyID
                , DataConnectionID = V.DataConnectionID
                , ComponentExecutionID = @component_execution_id
                , PurchasePackingTransRecID = V.PurchasePackingTransRecID
                , PurchasePackingTransPartition = V.PurchasePackingTransPartition
                , DocumentDateID = V.DocumentDateID
                , ShippingExpectedDateID = V.ShippingExpectedDateID
                , DeliveryDateID = V.DeliveryDateID
                , WMSlocationID = V.WMSlocationID
                , LocationID = V.LocationID
                , ProductID = V.ProductID
                , TrackingID = V.TrackingID
                , PurchaserID = V.PurchaserID
                , PurchaseRequesterID = V.PurchaseRequesterID
                , VendorAccountID = V.VendorAccountID
                , InvoiceAccountID = V.InvoiceAccountID
                , DeliveryAddressID = V.DeliveryAddressID
                , ItemID = V.ItemID
                , CurrencyID = V.CurrencyID
                , FinancialDimensionsID = V.FinancialDimensionsID
                , PurchaseTypeID = V.PurchaseTypeID
                , PurchasePoolID = V.PurchasePoolID
                , ModeOfDeliveryID = V.ModeOfDeliveryID
                , DeliveryTermsID = V.DeliveryTermsID
                , MethodOfPaymentID = V.MethodOfPaymentID
                , PaymentTermsID = V.PaymentTermsID
                , PurchaseOrderID = V.PurchaseOrderID
                , PurchaseUnitID = V.PurchaseUnitID
                , PurchasePackingSlipID = V.PurchasePackingSlipID
                , OrderedQuantity = V.OrderedQuantity
                , RemainingQuantity = V.RemainingQuantity
                , DeliveredQuantity = V.DeliveredQuantity
                , InventoryQuantity = V.InventoryQuantity
                , InventoryQuantityOpen = V.InventoryQuantityOpen
                , ValueMST = V.ValueMST
        FROM      [fact].[PurchasePacking]     F
       INNER JOIN [fact].[PurchasePackingView] AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO [fact].[PurchasePacking] (
        StageID
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , PurchasePackingTransRecID
      , PurchasePackingTransPartition
      , DocumentDateID
      , ShippingExpectedDateID
      , DeliveryDateID
      , WMSlocationID
      , LocationID
      , ProductID
      , TrackingID
      , PurchaserID
      , PurchaseRequesterID
      , VendorAccountID
      , InvoiceAccountID
      , DeliveryAddressID
      , ItemID
      , CurrencyID
      , FinancialDimensionsID
      , PurchaseTypeID
      , PurchasePoolID
      , ModeOfDeliveryID
      , DeliveryTermsID
      , MethodOfPaymentID
      , PaymentTermsID
      , PurchaseOrderID
      , PurchaseUnitID
      , PurchasePackingSlipID
      , OrderedQuantity
      , RemainingQuantity
      , DeliveredQuantity
      , InventoryQuantity
      , InventoryQuantityOpen
      , ValueMST
      )
      SELECT StageID
           , CompanyID
           , DataConnectionID
           , @component_execution_id
           , PurchasePackingTransRecID
           , PurchasePackingTransPartition
           , DocumentDateID
           , ShippingExpectedDateID
           , DeliveryDateID
           , WMSlocationID
           , LocationID
           , ProductID
           , TrackingID
           , PurchaserID
           , PurchaseRequesterID
           , VendorAccountID
           , InvoiceAccountID
           , DeliveryAddressID
           , ItemID
           , CurrencyID
           , FinancialDimensionsID
           , PurchaseTypeID
           , PurchasePoolID
           , ModeOfDeliveryID
           , DeliveryTermsID
           , MethodOfPaymentID
           , PaymentTermsID
           , PurchaseOrderID
           , PurchaseUnitID
           , PurchasePackingSlipID
           , OrderedQuantity
           , RemainingQuantity
           , DeliveredQuantity
           , InventoryQuantity
           , InventoryQuantityOpen
           , ValueMST
        FROM [fact].[PurchasePackingView] V
       WHERE ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.PurchasePacking F WHERE F.StageID = V.StageID) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM [fact].[PurchasePacking]
       WHERE EXISTS ( SELECT 1
                        FROM [fact].[PurchasePackingView] V
                       WHERE ExecutionFlag = 'D'
                         AND V.StageID     = [fact].[PurchasePacking].StageID) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
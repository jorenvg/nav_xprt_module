EXEC dbo.drop_object @object = N'fact.LoadPurchaseOrder' -- nvarchar(128)
                   , @type = N'P'                        -- nchar(2)
                   , @debug = 0 ;                        -- int
GO

CREATE PROCEDURE fact.LoadPurchaseOrder
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @table_name sysname = N'PurchaseOrder' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.PurchaseOrder ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO fact.PurchaseOrder (
      StageID
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , InventTransCode
    , DataAreaCode
    , Partition
    , CreatedDate
    , CreatedDateID
    , ShippingRequestedDateID
    , ShippingConfirmedDateID
    , PurchaseLineTypeID
    , WMSlocationID
    , LocationID
    , ProductID
    , TrackingID
    , PurchasePlacerID
    , RequesterID
    , VendorAccountID
    , InvoiceAccountID
    , DeliveryAddressID
    , ItemID
    , CurrencyTransactionID
    , CurrencyReportID
    , FinancialDimensionsID
    , PurchaseStatusID
    , PurchaseTypeID
    , PurchasePoolID
    , ModeOfDeliveryID
    , DeliveryTermsID
    , DeliveryTypeID
    , MethodOfPaymentID
    , PaymentTermsID
    , PurchaseOrderID
    , PurchaseUnitID
    , PurchaseQuantity
    , PurchasePrice
    , PurchasePrice_RCY
    , Discount
    , Discount_RCY
    , DiscountPercent
    , MultilineDiscount
    , MultilineDiscount_RCY
    , MultilineDiscountPercent
    , PurchaseCharges
    , PurchaseCharges_RCY
    , NetAmount
    , NetAmount_RCY
    , InventoryQuantity
    , InventoryQuantityOpen
    , PurchaseQuantityOpen
    , AmountInclTaxExclDiscount
    )
    SELECT StageID
         , CompanyID
         , DataConnectionID
         , @component_execution_id
         , InventTransCode
         , DataAreaCode
         , Partition
         , CreatedDate
         , CreatedDateID
         , ShippingRequestedDateID
         , ShippingConfirmedDateID
         , PurchaseLineTypeID
         , WMSlocationID
         , LocationID
         , ProductID
         , TrackingID
         , PurchasePlacerID
         , RequesterID
         , VendorAccountID
         , InvoiceAccountID
         , DeliveryAddressID
         , ItemID
         , CurrencyTransactionID
         , CurrencyReportID
         , FinancialDimensionsID
         , PurchaseStatusID
         , PurchaseTypeID
         , PurchasePoolID
         , ModeOfDeliveryID
         , DeliveryTermsID
         , DeliveryTypeID
         , MethodOfPaymentID
         , PaymentTermsID
         , PurchaseOrderID
         , PurchaseUnitID
         , PurchaseQuantity
         , PurchasePrice
         , PurchasePrice_RCY
         , Discount
         , Discount_RCY
         , DiscountPercent
         , MultilineDiscount
         , MultilineDiscount_RCY
         , MultilineDiscountPercent
         , PurchaseCharges
         , PurchaseCharges_RCY
         , NetAmount
         , NetAmount_RCY
         , InventoryQuantity
         , InventoryQuantityOpen
         , PurchaseQuantityOpen
         , AmountInclTaxExclDiscount
      FROM fact.PurchaseOrderView ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.PurchaseOrder
         SET      CompanyID = V.CompanyID
                , DataConnectionID = V.DataConnectionID
                , ComponentExecutionID = @component_execution_id
                , InventTransCode = V.InventTransCode
                , DataAreaCode = V.DataAreaCode
                , Partition = V.Partition
                , CreatedDate = V.CreatedDate
                , CreatedDateID = V.CreatedDateID
                , ShippingRequestedDateID = V.ShippingRequestedDateID
                , ShippingConfirmedDateID = V.ShippingConfirmedDateID
                , PurchaseLineTypeID = V.PurchaseLineTypeID
                , WMSlocationID = V.WMSlocationID
                , LocationID = V.LocationID
                , ProductID = V.ProductID
                , TrackingID = V.TrackingID
                , PurchasePlacerID = V.PurchasePlacerID
                , RequesterID = V.RequesterID
                , VendorAccountID = V.VendorAccountID
                , InvoiceAccountID = V.InvoiceAccountID
                , DeliveryAddressID = V.DeliveryAddressID
                , ItemID = V.ItemID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , FinancialDimensionsID = V.FinancialDimensionsID
                , PurchaseStatusID = V.PurchaseStatusID
                , PurchaseTypeID = V.PurchaseTypeID
                , PurchasePoolID = V.PurchasePoolID
                , ModeOfDeliveryID = V.ModeOfDeliveryID
                , DeliveryTermsID = V.DeliveryTermsID
                , DeliveryTypeID = V.DeliveryTypeID
                , MethodOfPaymentID = V.MethodOfPaymentID
                , PaymentTermsID = V.PaymentTermsID
                , PurchaseOrderID = V.PurchaseOrderID
                , PurchaseUnitID = V.PurchaseUnitID
                , PurchaseQuantity = V.PurchaseQuantity
                , PurchasePrice = V.PurchasePrice
                , PurchasePrice_RCY = V.PurchasePrice_RCY
                , Discount = V.Discount
                , Discount_RCY = V.Discount_RCY
                , DiscountPercent = V.DiscountPercent
                , MultilineDiscount = V.MultilineDiscount
                , MultilineDiscount_RCY = V.MultilineDiscount_RCY
                , MultilineDiscountPercent = V.MultilineDiscountPercent
                , PurchaseCharges = V.PurchaseCharges
                , PurchaseCharges_RCY = V.PurchaseCharges_RCY
                , NetAmount = V.NetAmount
                , NetAmount_RCY = V.NetAmount_RCY
                , InventoryQuantity = V.InventoryQuantity
                , InventoryQuantityOpen = V.InventoryQuantityOpen
                , PurchaseQuantityOpen = V.PurchaseQuantityOpen
                , AmountInclTaxExclDiscount = V.AmountInclTaxExclDiscount
        FROM      fact.PurchaseOrder     F
       INNER JOIN fact.PurchaseOrderView AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.PurchaseOrder (
        StageID
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , InventTransCode
      , DataAreaCode
      , Partition
      , CreatedDate
      , CreatedDateID
      , ShippingRequestedDateID
      , ShippingConfirmedDateID
      , PurchaseLineTypeID
      , WMSlocationID
      , LocationID
      , ProductID
      , TrackingID
      , PurchasePlacerID
      , RequesterID
      , VendorAccountID
      , InvoiceAccountID
      , DeliveryAddressID
      , ItemID
      , CurrencyTransactionID
      , CurrencyReportID
      , FinancialDimensionsID
      , PurchaseStatusID
      , PurchaseTypeID
      , PurchasePoolID
      , ModeOfDeliveryID
      , DeliveryTermsID
      , DeliveryTypeID
      , MethodOfPaymentID
      , PaymentTermsID
      , PurchaseOrderID
      , PurchaseUnitID
      , PurchaseQuantity
      , PurchasePrice
      , PurchasePrice_RCY
      , Discount
      , Discount_RCY
      , DiscountPercent
      , MultilineDiscount
      , MultilineDiscount_RCY
      , MultilineDiscountPercent
      , PurchaseCharges
      , PurchaseCharges_RCY
      , NetAmount
      , NetAmount_RCY
      , InventoryQuantity
      , InventoryQuantityOpen
      , PurchaseQuantityOpen
      , AmountInclTaxExclDiscount
      )
      SELECT V.StageID
           , V.CompanyID
           , V.DataConnectionID
           , @component_execution_id
           , V.InventTransCode
           , V.DataAreaCode
           , V.Partition
           , V.CreatedDate
           , V.CreatedDateID
           , V.ShippingRequestedDateID
           , V.ShippingConfirmedDateID
           , V.PurchaseLineTypeID
           , V.WMSlocationID
           , V.LocationID
           , V.ProductID
           , V.TrackingID
           , V.PurchasePlacerID
           , V.RequesterID
           , V.VendorAccountID
           , V.InvoiceAccountID
           , V.DeliveryAddressID
           , V.ItemID
           , V.CurrencyTransactionID
           , V.CurrencyReportID
           , V.FinancialDimensionsID
           , V.PurchaseStatusID
           , V.PurchaseTypeID
           , V.PurchasePoolID
           , V.ModeOfDeliveryID
           , V.DeliveryTermsID
           , V.DeliveryTypeID
           , V.MethodOfPaymentID
           , V.PaymentTermsID
           , V.PurchaseOrderID
           , V.PurchaseUnitID
           , V.PurchaseQuantity
           , V.PurchasePrice
           , V.PurchasePrice_RCY
           , V.Discount
           , V.Discount_RCY
           , V.DiscountPercent
           , V.MultilineDiscount
           , V.MultilineDiscount_RCY
           , V.MultilineDiscountPercent
           , V.PurchaseCharges
           , V.PurchaseCharges_RCY
           , V.NetAmount
           , V.NetAmount_RCY
           , V.InventoryQuantity
           , V.InventoryQuantityOpen
           , V.PurchaseQuantityOpen
           , V.AmountInclTaxExclDiscount
        FROM fact.PurchaseOrderView V
       WHERE V.ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.PurchaseOrder F WHERE F.StageID = V.StageID) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.PurchaseOrder
       WHERE EXISTS ( SELECT 1
                        FROM fact.PurchaseOrderView V
                       WHERE V.ExecutionFlag = 'D'
                         AND V.StageID       = [fact].[PurchaseOrder].StageID) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
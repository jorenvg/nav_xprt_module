EXEC dbo.drop_object 'fact.PurchaseOrder', 'T' ;
GO
CREATE TABLE [fact].[PurchaseOrder]
(
  StageID                    BIGINT            NOT NULL
, [CompanyID]                [INT]             NOT NULL
, [DataConnectionID]         [INT]             NOT NULL
, [ComponentExecutionID]     INT               NOT NULL
, [InventTransCode]          [NVARCHAR](20)    NULL
, [DataAreaCode]             [NVARCHAR](4)     NULL
, [Partition]                [BIGINT]          NULL
, [CreatedDate]              [DATE]            NULL
, [CreatedDateID]            [INT]             NULL
, [ShippingRequestedDateID]  [INT]             NULL
, [ShippingConfirmedDateID]  [INT]             NULL
, [PurchaseLineTypeID]       [INT]             NULL
, [WMSlocationID]            [INT]             NULL
, [LocationID]               [INT]             NULL
, [ProductID]                [INT]             NULL
, [TrackingID]               [INT]             NULL
, [PurchasePlacerID]         [INT]             NULL
, [RequesterID]              [INT]             NULL
, [VendorAccountID]          [INT]             NULL
, [InvoiceAccountID]         [INT]             NULL
, [DeliveryAddressID]        [INT]             NULL
, [ItemID]                   [INT]             NULL
, CurrencyTransactionID      INT               NULL
, CurrencyReportID           INT               NULL
, [FinancialDimensionsID]    [INT]             NULL
, [PurchaseStatusID]         [INT]             NULL
, [PurchaseTypeID]           [INT]             NULL
, [PurchasePoolID]           [INT]             NULL
, [ModeOfDeliveryID]         [INT]             NULL
, [DeliveryTermsID]          [INT]             NULL
, [DeliveryTypeID]           [INT]             NULL
, [MethodOfPaymentID]        [INT]             NULL
, [PaymentTermsID]           [INT]             NULL
, [PurchaseOrderID]          [INT]             NULL
, [PurchaseUnitID]           [INT]             NULL
, [PurchaseQuantity]         DECIMAL(19,4)     NULL
, [PurchasePrice]            DECIMAL(19,4)     NULL
, [PurchasePrice_RCY]        DECIMAL(19,4)     NULL
, [Discount]                 DECIMAL(19,4)     NULL
, [Discount_RCY]             DECIMAL(19,4)     NULL
, [DiscountPercent]          DECIMAL(19,4)     NULL
, [MultilineDiscount]        DECIMAL(19,4)     NULL
, [MultilineDiscount_RCY]    DECIMAL(19,4)     NULL
, [MultilineDiscountPercent] DECIMAL(19,4)     NULL
, [PurchaseCharges]          DECIMAL(19,4)     NULL
, [PurchaseCharges_RCY]      DECIMAL(19,4)     NULL
, [NetAmount]                DECIMAL(19,4)     NULL
, [NetAmount_RCY]            DECIMAL(19,4)     NULL
, [InventoryQuantity]        DECIMAL(19,4)     NULL
, [InventoryQuantityOpen]    DECIMAL(19,4)     NULL
, [PurchaseQuantityOpen]     DECIMAL(19,4)     NULL
, AmountInclTaxExclDiscount  DECIMAL(19,4)     NULL
) ;                         

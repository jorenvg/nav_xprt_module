EXEC dbo.drop_object @object = N'fact.LoadProductionOrder' -- nvarchar(128)
                   , @type = N'P'                          -- nchar(2)
                   , @debug = 0 ;                          -- int
GO

CREATE PROCEDURE fact.LoadProductionOrder
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  DECLARE @table_name sysname = N'ProductionOrder' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;
  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.ProductionOrder ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;
    INSERT INTO fact.ProductionOrder (
      StageId
    , ExecutionFlag
    , ComponentExecutionID
    , Partition
    , DataConnectionID
    , RecID
    , CompanyID
    -- Dates				
    , CreatedDateID
    , EstimatedDateID
    , ScheduledDateID
    , StartedDateID
    , ReportedAsFinishedDateID
    , EndedDateID
    , DeliveryDateID
    --Production			
    , ProductionOrderID
    , ItemID
    -- InventDim				
    , ProductID
    , TrackingID
    , LocationID
    , WMSlocationID
    --Measures		
    , IsDelayed
    , IsOnTime
    , NumberOfDaysDelayed
    , LeadtimeInMinutes
    , PlannedLeadtimeInMinutes
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionID
         , Partition
         , DataConnectionID
         , RecID
         , CompanyID
         -- Dates				
         , CreatedDateID
         , EstimatedDateID
         , ScheduledDateID
         , StartedDateID
         , ReportedAsFinishedDateID
         , EndedDateID
         , DeliveryDateID
         --Production			
         , ProductionOrderID
         , ItemID
         -- InventDim				
         , ProductID
         , TrackingID
         , LocationID
         , WMSlocationID
         --Measures		
         , IsDelayed
         , IsOnTime
         , NumberOfDaysDelayed
         , LeadtimeInMinutes
         , PlannedLeadtimeInMinutes
      FROM fact.ProductionOrderView ;
    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.ProductionOrder
         SET      [StageId] = V.[StageId]
                , [ExecutionFlag] = V.[ExecutionFlag]
                , [ComponentExecutionID] = V.[ComponentExecutionID]
                , [Partition] = V.[Partition]
                , [DataConnectionID] = V.[DataConnectionID]
                , [RecID] = V.[RecID]
                , [CompanyID] = V.[CompanyID]
                , [CreatedDateID] = V.[CreatedDateID]
                , [EstimatedDateID] = V.[EstimatedDateID]
                , [ScheduledDateID] = V.[ScheduledDateID]
                , [StartedDateID] = V.[StartedDateID]
                , [ReportedAsFinishedDateID] = V.[ReportedAsFinishedDateID]
                , [EndedDateID] = V.[EndedDateID]
                , [DeliveryDateID] = V.[DeliveryDateID]
                , [ProductionOrderID] = V.[ProductionOrderID]
                , [ItemID] = V.[ItemID]
                , [ProductID] = V.[ProductID]
                , [TrackingID] = V.[TrackingID]
                , [LocationID] = V.[LocationID]
                , [WMSlocationID] = V.[WMSlocationID]
                , [IsDelayed] = V.[IsDelayed]
                , [IsOnTime] = V.[IsOnTime]
                , [NumberOfDaysDelayed] = V.[NumberOfDaysDelayed]
                , [LeadtimeInMinutes] = V.[LeadtimeInMinutes]
                , [PlannedLeadtimeInMinutes] = V.[PlannedLeadtimeInMinutes]
        FROM      fact.ProductionOrder     AS F
       INNER JOIN fact.ProductionOrderView AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U' ;
      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.ProductionOrder (
        [StageId]
      , [ExecutionFlag]
      , [ComponentExecutionID]
      , [Partition]
      , [DataConnectionID]
      , [RecID]
      , [CompanyID]
      , [CreatedDateID]
      , [EstimatedDateID]
      , [ScheduledDateID]
      , [StartedDateID]
      , [ReportedAsFinishedDateID]
      , [EndedDateID]
      , [DeliveryDateID]
      , [ProductionOrderID]
      , [ItemID]
      , [ProductID]
      , [TrackingID]
      , [LocationID]
      , [WMSlocationID]
      , [IsDelayed]
      , [IsOnTime]
      , [NumberOfDaysDelayed]
      , [LeadtimeInMinutes]
      , [PlannedLeadtimeInMinutes]
      )
      SELECT V.[StageId]
           , V.[ExecutionFlag]
           , V.[ComponentExecutionID]
           , V.[Partition]
           , V.[DataConnectionID]
           , V.[RecID]
           , V.[CompanyID]
           , V.[CreatedDateID]
           , V.[EstimatedDateID]
           , V.[ScheduledDateID]
           , V.[StartedDateID]
           , V.[ReportedAsFinishedDateID]
           , V.[EndedDateID]
           , V.[DeliveryDateID]
           , V.[ProductionOrderID]
           , V.[ItemID]
           , V.[ProductID]
           , V.[TrackingID]
           , V.[LocationID]
           , V.[WMSlocationID]
           , V.[IsDelayed]
           , V.[IsOnTime]
           , V.[NumberOfDaysDelayed]
           , V.[LeadtimeInMinutes]
           , V.[PlannedLeadtimeInMinutes]
        FROM fact.ProductionOrderView AS V
       WHERE V.ExecutionFlag = 'N'
         AND NOT EXISTS (SELECT 1 FROM fact.ProductionOrder F WHERE F.StageID = V.StageID) ;
      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.ProductionOrder
       WHERE EXISTS ( SELECT 1
                        FROM fact.ProductionOrderView
                       WHERE ExecutionFlag                    = 'D'
                         AND [fact].[ProductionOrder].StageID = StageID) ;
      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
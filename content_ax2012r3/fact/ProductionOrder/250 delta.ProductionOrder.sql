EXEC dbo.drop_object 'delta.ProductionOrder', 'T' ;
GO
CREATE TABLE [delta].[ProductionOrder]
(
  StageId                    BIGINT        NOT NULL
, ExecutionFlag              NCHAR(40)     NULL
, ComponentExecutionId       INT           NOT NULL
, [Partition]                [BIGINT]      NULL
, [DataConnectionID]         [INT]         NULL
, [RecId]                    [BIGINT]      NULL
, [CompanyId]                [INT]         NULL
, [CreatedDate]              DATETIME      NULL
, [EstimatedDate]            DATETIME      NULL
, [ScheduledDate]            DATETIME      NULL
, [StartedDate]              DATETIME      NULL
, [ReportAsFinishedDate]     DATETIME      NULL
, [EndedDate]                DATETIME      NULL
, [DeliveryDate]             DATETIME      NULL
, [ProductionOrder]          NVARCHAR(512) NULL
, [Item]                     NVARCHAR(512) NULL
, [ProductConfigCode]        NVARCHAR(512) NULL
, [ProductColorCode]         NVARCHAR(512) NULL
, [ProductInventSizeCode]    NVARCHAR(512) NULL
, [ProductStyleCode]         NVARCHAR(512) NULL
, [TrackingBatchCode]        NVARCHAR(512) NULL
, [TrackingSerialCode]       NVARCHAR(512) NULL
, [LocationCode]             NVARCHAR(512) NULL
, [WMSlocationCode]          NVARCHAR(100) NULL
, [IsDelayed]                [BIT]         NULL
, [IsOnTime]                 [BIT]         NULL
, [NumberOfDaysDelayed]      [INT]         NULL
, [LeadtimeInMinutes]        [BIGINT]      NULL
, [PlannedLeadtimeInMinutes] [BIGINT]      NULL
) ;


EXEC dbo.drop_object @object = N'fact.ExchangeRatesInvalidDateView' -- nvarchar(128)
                   , @type = N'V'                                   -- nchar(2)
                   , @debug = 0 ;

GO

CREATE VIEW fact.ExchangeRatesInvalidDateView
AS
  SELECT CrossRate      = 1
       , FromCurrencyID = c.CurrencyID
       , ToCurrencyID   = c.CurrencyID
       , DateID         = 0
       , c.Partition
       , RecId          = 0
       , c.DataConnectionID
    FROM dim.Currency AS c
   WHERE NOT EXISTS ( SELECT 1
                        FROM fact.ExchangeRatesView AS erv
                       WHERE erv.DateID         = 0
                         AND erv.FromCurrencyID = c.CurrencyID
                         AND erv.ToCurrencyID   = c.CurrencyID) ;
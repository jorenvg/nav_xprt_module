EXEC dbo.drop_object 'delta.CustomerTransactionTransFunction', 'F' ;
GO
CREATE FUNCTION delta.CustomerTransactionTransFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            CT.stage_id                                                             AS StageId
                           , @execution_flag                                                         AS ExecutionFlag
                           , CT.DATA_CONNECTION_ID                                                   AS DataConnectionID
                           , CT.COMPANY_ID                                                           AS CompanyID
                           , @component_execution_id                                                 AS ComponentExecutionID
                           , CT.RECID                                                                AS CustomerTransactionRecID
                           , CT.PARTITION                                                            AS CustomerTransactionPartition
                           , 'Transaction'                                                           AS TransactionSource
                           , CASE WHEN CT.INVOICE IS NOT NULL AND CT.INVOICE <> '' THEN 1 ELSE 0 END AS IsInvoice
                           , CASE WHEN CT.AMOUNTMST < 0 THEN 1 ELSE 0 END                            AS IsCredit
                           , CT.TRANSTYPE                                                            AS TransType
                           , CT.VOUCHER                                                              AS Voucher
                           , CT.INVOICE                                                              AS SalesInvoice
                           , CT.TRANSDATE                                                            AS InvoiceDate
                           , CT.TRANSDATE                                                            AS PostingDate
                           , CT.DUEDATE                                                              AS DueDate
                           , CT.LASTSETTLEDATE                                                       AS LastSettleDate
                           , CT.Closed                                                               AS ClosedDate
                           , CT.ACCOUNTNUM                                                           AS InvoiceAccount
                           , CT.ORDERACCOUNT                                                         AS OrderAccount
                           , CT.DEFAULTDIMENSION                                                     AS DefaultDimension
                           , CT.APPROVER                                                             AS Approver
                           , CT.CurrencyCode                                                         AS CurrencyCode
                           , CT.AMOUNTCUR                                                            AS Amount
                           , CT.AMOUNTCUR * ex.CrossRate                                             AS Amount_RCY
                           , CT.AMOUNTMST                                                            AS AmountMST
             FROM            stage_ax.CUSTTRANS       AS CT
             LEFT OUTER JOIN reporting_exchange_rates AS ex ON ex.FromCurrencyCode = CT.CURRENCYCODE
                                                           AND ex.DataConnectionID = CT.data_connection_id
                                                           AND CT.TRANSDATE BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE
             /** FULL LOAD **/
                             ( @load_type        = 0
                           AND CT.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'N'
                           AND CT.execution_flag               = @execution_flag
                           AND CT.execution_timestamp          > @last_processed_timestamp)
               OR
               /** INCREMENTAL, UPDATED RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'U'
                           AND (( CT.execution_flag            = @execution_flag
                              AND CT.execution_timestamp       > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                      = 1
                           AND @execution_flag                 = 'D'
                           AND CT.execution_flag               = @execution_flag
                           AND CT.execution_timestamp          > @last_processed_timestamp)) ;

GO
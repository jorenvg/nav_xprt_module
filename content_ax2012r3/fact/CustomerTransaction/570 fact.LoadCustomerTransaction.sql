EXEC dbo.drop_object @object = N'fact.LoadCustomerTransaction' -- nvarchar(128)
                   , @type = N'P'                              -- nchar(2)
                   , @debug = 0 ;                              -- int
GO

CREATE PROCEDURE fact.LoadCustomerTransaction
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  DECLARE @table_name sysname = N'CustomerTransaction' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  /****************************************************************************************************
      Functionality:  Determine the rowcounts per execution flag
  
      Date            Changed by      Ticket/Change       Omschrijving
     
  *****************************************************************************************************/
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + 'Trans' + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + 'Trans' + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + 'Trans' + ' WHERE ExecutionFlag = ''D'';

	SELECT @Delta_Inserts += count(*) FROM ' + @delta_table_identifier + 'Settlement' + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates += count(*) FROM ' + @delta_table_identifier + 'Settlement' + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes += count(*) FROM ' + @delta_table_identifier + 'Settlement' + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;

  /****************************************************************************************************
      Functionality:  Full load the fact table, by truncating and inserted all non deleted records
  
      Date            Changed by      Ticket/Change       Omschrijving
     
  *****************************************************************************************************/
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.CustomerTransaction ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO fact.CustomerTransaction (
      CustomerTransactionRecId
    , CustomerTransactionPartition
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , TransactionSource
    , IsInvoiceID
    , IsCreditID
    , PostingDateID
    , DueDateID
    , LastSettleDateID
    , ClosedDateID
    , ApproverID
    , CurrencyTransactionID
    , CurrencyReportID
    , LedgerTransactionTypeID
    , SalesInvoiceID
    , InvoiceAccountID
    , OrderAccountID
    , VoucherID
    , FinancialDimensionsID
    , Amount
    , Amount_RCY
    , AmountMST
    , StageID
    )
    SELECT CustomerTransactionRecId
         , CustomerTransactionPartition
         , CompanyID
         , DataConnectionID
         , @component_execution_id
         , TransactionSource
         , IsInvoiceID
         , IsCreditID
         , PostingDateID
         , DueDateID
         , LastSettleDateID
         , ClosedDateID
         , ApproverID
         , CurrencyTransactionID
         , CurrencyReportID
         , LedgerTransactionTypeID
         , SalesInvoiceID
         , InvoiceAccountID
         , OrderAccountID
         , VoucherID
         , FinancialDimensionsID
         , Amount
         , Amount_RCY
         , AmountMST
         , StageID
      FROM fact.CustomerTransactionTransView ;

    SELECT @inserted = @@ROWCOUNT ;

    INSERT INTO fact.CustomerTransaction (
      CustomerTransactionRecId
    , CustomerTransactionPartition
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , TransactionSource
    , IsInvoiceID
    , IsCreditID
    , PostingDateID
    , DueDateID
    , LastSettleDateID
    , ClosedDateID
    , ApproverID
    , CurrencyTransactionID
    , CurrencyReportID
    , LedgerTransactionTypeID
    , SalesInvoiceID
    , InvoiceAccountID
    , OrderAccountID
    , VoucherID
    , FinancialDimensionsID
    , Amount
    , Amount_RCY
    , AmountMST
    , StageID
    )
    SELECT CustomerTransactionRecId
         , CustomerTransactionPartition
         , CompanyID
         , DataConnectionID
         , @component_execution_id
         , TransactionSource
         , IsInvoiceID
         , IsCreditID
         , PostingDateID
         , DueDateID
         , LastSettleDateID
         , ClosedDateID
         , ApproverID
         , CurrencyTransactionID
         , CurrencyReportID
         , LedgerTransactionTypeID
         , SalesInvoiceID
         , InvoiceAccountID
         , OrderAccountID
         , VoucherID
         , FinancialDimensionsID
         , Amount
         , Amount_RCY
         , AmountMST
         , StageID
      FROM fact.CustomerTransactionSettlementView ;

    SELECT @inserted += @@ROWCOUNT ;
  END ;

  /****************************************************************************************************
      Functionality:  Loading all incremental records by updating updated records, insert new records, and deleting deleted records.
  
      Date            Changed by      Ticket/Change       Omschrijving
     
  *****************************************************************************************************/

  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.CustomerTransaction
         SET      CustomerTransactionRecId = V.CustomerTransactionRecId
                , CustomerTransactionPartition = V.CustomerTransactionPartition
                , CompanyID = V.CompanyID
                , DataConnectionID = V.DataConnectionID
                , ComponentExecutionID = @component_execution_id
                , TransactionSource = V.TransactionSource
                , IsInvoiceID = V.IsInvoiceID
                , IsCreditID = V.IsCreditID
                , PostingDateID = V.PostingDateID
                , DueDateID = V.DueDateID
                , LastSettleDateID = V.LastSettleDateID
                , ClosedDateID = V.ClosedDateID
                , ApproverID = V.ApproverID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , LedgerTransactionTypeID = V.LedgerTransactionTypeID
                , SalesInvoiceID = V.SalesInvoiceID
                , InvoiceAccountID = V.InvoiceAccountID
                , OrderAccountID = V.OrderAccountID
                , VoucherID = V.VoucherID
                , FinancialDimensionsID = V.FinancialDimensionsID
                , Amount = V.Amount
                , Amount_RCY = V.Amount_RCY
                , AmountMST = V.AmountMST
        FROM      fact.CustomerTransaction          AS F
       INNER JOIN fact.CustomerTransactionTransView AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U'
         AND      F.TransactionSource  = V.TransactionSource ;

      SELECT @updated = @@ROWCOUNT ;

      UPDATE      fact.CustomerTransaction
         SET      CustomerTransactionRecId = V.CustomerTransactionRecId
                , CustomerTransactionPartition = V.CustomerTransactionPartition
                , CompanyID = V.CompanyID
                , DataConnectionID = V.DataConnectionID
                , ComponentExecutionID = @component_execution_id
                , TransactionSource = V.TransactionSource
                , IsInvoiceID = V.IsInvoiceID
                , IsCreditID = V.IsCreditID
                , PostingDateID = V.PostingDateID
                , DueDateID = V.DueDateID
                , LastSettleDateID = V.LastSettleDateID
                , ClosedDateID = V.ClosedDateID
                , ApproverID = V.ApproverID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , LedgerTransactionTypeID = V.LedgerTransactionTypeID
                , SalesInvoiceID = V.SalesInvoiceID
                , InvoiceAccountID = V.InvoiceAccountID
                , OrderAccountID = V.OrderAccountID
                , VoucherID = V.VoucherID
                , FinancialDimensionsID = V.FinancialDimensionsID
                , Amount = V.Amount
                , Amount_RCY = V.Amount_RCY
                , AmountMST = V.AmountMST
        FROM      fact.CustomerTransaction               AS F
       INNER JOIN fact.CustomerTransactionSettlementView AS V ON F.StageID = V.StageID
       WHERE      V.ExecutionFlag = 'U'
         AND      F.TransactionSource  = V.TransactionSource ;

      SELECT @updated += @@ROWCOUNT ;


    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.CustomerTransaction (
        CustomerTransactionRecId
      , CustomerTransactionPartition
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , TransactionSource
      , IsInvoiceID
      , IsCreditID
      , PostingDateID
      , DueDateID
      , LastSettleDateID
      , ClosedDateID
      , ApproverID
      , CurrencyTransactionID
      , CurrencyReportID
      , LedgerTransactionTypeID
      , SalesInvoiceID
      , InvoiceAccountID
      , OrderAccountID
      , VoucherID
      , FinancialDimensionsID
      , Amount
      , Amount_RCY
      , AmountMST
      , StageID
      )
      SELECT V.CustomerTransactionRecId
           , V.CustomerTransactionPartition
           , V.CompanyID
           , V.DataConnectionID
           , @component_execution_id
           , V.TransactionSource
           , V.IsInvoiceID
           , V.IsCreditID
           , V.PostingDateID
           , V.DueDateID
           , V.LastSettleDateID
           , V.ClosedDateID
           , V.ApproverID
           , V.CurrencyTransactionID
           , V.CurrencyReportID
           , V.LedgerTransactionTypeID
           , V.SalesInvoiceID
           , V.InvoiceAccountID
           , V.OrderAccountID
           , V.VoucherID
           , V.FinancialDimensionsID
           , V.Amount
           , V.Amount_RCY
           , V.AmountMST
           , V.StageID
        FROM fact.CustomerTransactionTransView AS V
       WHERE V.ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS ( SELECT 1
                            FROM fact.CustomerTransaction AS F
                           WHERE F.StageID           = V.StageID
                             AND F.TransactionSource = V.TransactionSource) ;

      SELECT @inserted = @@ROWCOUNT ;

      INSERT INTO fact.CustomerTransaction (
        CustomerTransactionRecId
      , CustomerTransactionPartition
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , TransactionSource
      , IsInvoiceID
      , IsCreditID
      , PostingDateID
      , DueDateID
      , LastSettleDateID
      , ClosedDateID
      , ApproverID
      , CurrencyTransactionID
      , CurrencyReportID
      , LedgerTransactionTypeID
      , SalesInvoiceID
      , InvoiceAccountID
      , OrderAccountID
      , VoucherID
      , FinancialDimensionsID
      , Amount
      , Amount_RCY
      , AmountMST
      , StageID
      )
      SELECT V.CustomerTransactionRecId
           , V.CustomerTransactionPartition
           , V.CompanyID
           , V.DataConnectionID
           , @component_execution_id
           , V.TransactionSource
           , V.IsInvoiceID
           , V.IsCreditID
           , V.PostingDateID
           , V.DueDateID
           , V.LastSettleDateID
           , V.ClosedDateID
           , V.ApproverID
           , V.CurrencyTransactionID
           , V.CurrencyReportID
           , V.LedgerTransactionTypeID
           , V.SalesInvoiceID
           , V.InvoiceAccountID
           , V.OrderAccountID
           , V.VoucherID
           , V.FinancialDimensionsID
           , V.Amount
           , V.Amount_RCY
           , V.AmountMST
           , V.StageID
        FROM fact.CustomerTransactionSettlementView AS V
       WHERE V.ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS ( SELECT 1
                            FROM fact.CustomerTransaction AS F
                           WHERE F.StageID           = V.StageID
                             AND F.TransactionSource = V.TransactionSource) ;

      SELECT @inserted += @@ROWCOUNT ;

    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.CustomerTransaction
       WHERE EXISTS ( SELECT 1
                        FROM fact.CustomerTransactionTransView AS V
                       WHERE V.ExecutionFlag     = 'D'
                         AND V.StageID           = V.StageID
                         AND V.TransactionSource = V.TransactionSource) ;

      SELECT @deleted = @@ROWCOUNT ;

      DELETE FROM fact.CustomerTransaction
       WHERE EXISTS ( SELECT 1
                        FROM fact.CustomerTransactionSettlementView AS V
                       WHERE V.ExecutionFlag   = 'D'
                         AND V.StageID         = StageID
                         AND TransactionSource = V.TransactionSource) ;

      SELECT @deleted += @@ROWCOUNT ;

    END ;
  END ;
END ;
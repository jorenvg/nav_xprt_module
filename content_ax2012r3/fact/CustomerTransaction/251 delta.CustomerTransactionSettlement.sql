EXEC dbo.drop_object 'delta.CustomerTransactionSettlement', 'T' ;
GO
CREATE TABLE [delta].[CustomerTransactionSettlement]
(
  StageId                        BIGINT            NOT NULL
, ExecutionFlag                  NCHAR(1)          NOT NULL
, DataConnectionID               INT               NOT NULL
, CompanyID                      INT               NOT NULL
, ComponentExecutionID           INT               NOT NULL
, [CustomerTransactionRecId]     [BIGINT]          NULL
, [CustomerTransactionPartition] BIGINT            NULL
, TransactionSource              NVARCHAR(50)      NULL
, IsInvoice                      INT               NULL
, IsCredit                       INT               NULL
, [PostingDate]                  [DATETIME]        NULL
, [DueDate]                      [DATETIME]        NULL
, [ClosedDate]                   [DATETIME]        NULL
, [LastSettleDate]               [DATETIME]        NULL
, [Voucher]                      [NVARCHAR](100)   NULL
, [TransType]                    [NVARCHAR](100)   NULL
, [SalesInvoice]                 [NVARCHAR](100)   NULL
, [InvoiceDate]                  [DATETIME]        NULL
, [InvoiceAccount]               [NVARCHAR](100)   NULL
, [OrderAccount]                 [NVARCHAR](100)   NULL
, [DefaultDimension]             [NVARCHAR](100)   NULL
, [Approver]                     [NVARCHAR](100)   NULL
, CurrencyCode                   NVARCHAR(50)      NULL
, [Amount]                       DECIMAL(19, 4)    NULL
, [Amount_RCY]                   DECIMAL(19, 4)    NULL
, [AmountMST]                    DECIMAL(19, 4)    NULL
) ;
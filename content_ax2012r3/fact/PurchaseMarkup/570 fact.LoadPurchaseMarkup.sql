EXEC dbo.drop_object @object = N'fact.LoadPurchaseMarkup' -- nvarchar(128)
                   , @type = N'P'                         -- nchar(2)
                   , @debug = 0 ;                         -- int
GO

CREATE PROCEDURE fact.LoadPurchaseMarkup
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @table_name sysname = N'PurchaseMarkup' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  --rowcounts per ExecutionFlag
  DECLARE @Delta_Inserts INT
        , @Delta_Updates INT
        , @Delta_Deletes INT ;
  DECLARE @parmas NVARCHAR(MAX) = N'@Delta_Inserts INT OUTPUT,@Delta_Updates INT OUTPUT,@Delta_Deletes INT OUTPUT' ;
  DECLARE @SQL NVARCHAR(MAX) = N'
	SELECT @Delta_Inserts = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''N'';
	SELECT @Delta_Updates = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''U'';
	SELECT @Delta_Deletes = count(*) FROM ' + @delta_table_identifier + ' WHERE ExecutionFlag = ''D'';
	' ;
  EXEC sys.sp_executesql @SQL
                       , @parmas
                       , @Delta_Inserts = @Delta_Inserts OUTPUT
                       , @Delta_Updates = @Delta_Updates OUTPUT
                       , @Delta_Deletes = @Delta_Deletes OUTPUT ;


  --Full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM fact.PurchaseMarkup ;
    EXECUTE dbo.truncate_table @fact_table_identifier ;

    INSERT INTO fact.PurchaseMarkup (
      StageId
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , PurchaseMarkupRecID
    , PurchaseMarkupPartition
    , TransactionDateID
    , DocumentDateID
    , DeliveryDateID
    , DeliveryAddressID
    , VendorAccountID
    , InvoiceAccountID
    , PurchasePlacerID
    , PurchaseRequesterID
    , VoucherID
    , DeliveryTermID
    , ModeOfDeliveryID
    , PaymentTermsID
    , MethodOfPaymentID
    , PurchasePoolID
    , CurrencyTransactionID
    , CurrencyReportID
    , PurchaseMarkupOriginID
    , MarkupTypeID
    , PurchaseOrderID
    , PurchaseInvoiceID
    , PurchasePackingSlipID
    , MarkUpCalculatedAmount
    , MarkUpCalculatedAmount_RCY
    , MarkUpPostedAmount
    , MarkUpPostedAmount_RCY
    , MarkupTaxAmount
    , MarkupTaxAmount_RCY
    )
    SELECT StageId
         , CompanyID
         , DataConnectionID
         , @component_execution_id
         , PurchaseMarkupRecID
         , PurchaseMarkupPartition
         , TransactionDateID
         , DocumentDateID
         , DeliveryDateID
         , DeliveryAddressID
         , VendorAccountID
         , InvoiceAccountID
         , PurchasePlacerID
         , PurchaseRequesterID
         , VoucherID
         , DeliveryTermID
         , ModeOfDeliveryID
         , PaymentTermsId
         , MethodOfPaymentID
         , PurchasePoolID
         , CurrencyTransactionID
         , CurrencyReportID
         , PurchaseMarkupOriginID
         , MarkupTypeID
         , PurchaseOrderID
         , PurchaseInvoiceID
         , PurchasePackingSlipID
         , MarkUpCalculatedAmount
         , MarkUpCalculatedAmount_RCY
         , MarkUpPostedAmount
         , MarkUpPostedAmount_RCY
         , MarkupTaxAmount
         , MarkupTaxAmount_RCY
      FROM fact.PurchaseMarkupView ;

    SELECT @inserted = @@ROWCOUNT ;
  END ;
  IF @load_type = 1
  BEGIN
    --Incremental load
    -- update
    IF @Delta_Updates <> 0
    BEGIN
      UPDATE      fact.PurchaseMarkup
         SET      CompanyID = V.CompanyID
                , DataConnectionID = V.DataConnectionID
                , ComponentExecutionID = @component_execution_id
                , PurchaseMarkupRecID = V.PurchaseMarkupRecID
                , PurchaseMarkupPartition = V.PurchaseMarkupPartition
                , TransactionDateID = V.TransactionDateID
                , DocumentDateID = V.DocumentDateID
                , DeliveryDateID = V.DeliveryDateID
                , DeliveryAddressID = V.DeliveryAddressID
                , VendorAccountID = V.VendorAccountID
                , InvoiceAccountID = V.InvoiceAccountID
                , PurchasePlacerID = V.PurchasePlacerID
                , PurchaseRequesterID = V.PurchaseRequesterID
                , VoucherID = V.VoucherID
                , DeliveryTermID = V.DeliveryTermID
                , ModeOfDeliveryID = V.ModeOfDeliveryID
                , PaymentTermsID = V.PaymentTermsId
                , MethodOfPaymentID = V.MethodOfPaymentID
                , PurchasePoolID = V.PurchasePoolID
                , CurrencyTransactionID = V.CurrencyTransactionID
                , CurrencyReportID = V.CurrencyReportID
                , PurchaseMarkupOriginID = V.PurchaseMarkupOriginID
                , MarkupTypeID = V.MarkupTypeID
                , PurchaseOrderID = V.PurchaseOrderID
                , PurchaseInvoiceID = V.PurchaseInvoiceID
                , PurchasePackingSlipID = V.PurchasePackingSlipID
                , MarkUpCalculatedAmount = V.MarkUpCalculatedAmount
                , MarkUpCalculatedAmount_RCY = V.MarkUpCalculatedAmount_RCY
                , MarkUpPostedAmount = V.MarkUpPostedAmount
                , MarkUpPostedAmount_RCY = V.MarkUpPostedAmount_RCY
                , MarkupTaxAmount = V.MarkupTaxAmount
                , MarkupTaxAmount_RCY = V.MarkupTaxAmount_RCY
        FROM      fact.PurchaseMarkup     F
       INNER JOIN fact.PurchaseMarkupView AS V ON F.StageId = V.StageId
       WHERE      V.ExecutionFlag = 'U' ;

      SELECT @updated = @@ROWCOUNT ;
    END ;
    -- insert
    IF @Delta_Inserts <> 0
    BEGIN
      INSERT INTO fact.PurchaseMarkup (
        StageId
      , CompanyID
      , DataConnectionID
      , ComponentExecutionID
      , PurchaseMarkupRecID
      , PurchaseMarkupPartition
      , TransactionDateID
      , DocumentDateID
      , DeliveryDateID
      , DeliveryAddressID
      , VendorAccountID
      , InvoiceAccountID
      , PurchasePlacerID
      , PurchaseRequesterID
      , VoucherID
      , DeliveryTermID
      , ModeOfDeliveryID
      , PaymentTermsID
      , MethodOfPaymentID
      , PurchasePoolID
      , CurrencyTransactionID
      , CurrencyReportID
      , PurchaseMarkupOriginID
      , MarkupTypeID
      , PurchaseOrderID
      , PurchaseInvoiceID
      , PurchasePackingSlipID
      , MarkUpCalculatedAmount
      , MarkUpCalculatedAmount_RCY
      , MarkUpPostedAmount
      , MarkUpPostedAmount_RCY
      , MarkupTaxAmount
      , MarkupTaxAmount_RCY
      )
      SELECT V.StageId
           , V.CompanyID
           , V.DataConnectionID
           , @component_execution_id
           , V.PurchaseMarkupRecID
           , V.PurchaseMarkupPartition
           , V.TransactionDateID
           , V.DocumentDateID
           , V.DeliveryDateID
           , V.DeliveryAddressID
           , V.VendorAccountID
           , V.InvoiceAccountID
           , V.PurchasePlacerID
           , V.PurchaseRequesterID
           , V.VoucherID
           , V.DeliveryTermID
           , V.ModeOfDeliveryID
           , V.PaymentTermsId
           , V.MethodOfPaymentID
           , V.PurchasePoolID
           , V.CurrencyTransactionID
           , V.CurrencyReportID
           , V.PurchaseMarkupOriginID
           , V.MarkupTypeID
           , V.PurchaseOrderID
           , V.PurchaseInvoiceID
           , V.PurchasePackingSlipID
           , V.MarkUpCalculatedAmount
           , V.MarkUpCalculatedAmount_RCY
           , V.MarkUpPostedAmount
           , V.MarkUpPostedAmount_RCY
           , V.MarkupTaxAmount
           , V.MarkupTaxAmount_RCY
        FROM fact.PurchaseMarkupView V
       WHERE V.ExecutionFlag IN ( 'N', 'U' )
         AND NOT EXISTS (SELECT 1 FROM fact.PurchaseMarkup F WHERE F.StageId = V.StageId) ;

      SELECT @inserted = @@ROWCOUNT ;
    END ;
    -- delete
    IF @Delta_Deletes <> 0
    BEGIN
      DELETE FROM fact.PurchaseMarkup
       WHERE EXISTS ( SELECT 1
                        FROM fact.PurchaseMarkupView V
                       WHERE V.ExecutionFlag = 'D'
                         AND V.StageId       = [fact].[PurchaseMarkup].StageId) ;

      SELECT @deleted = @@ROWCOUNT ;
    END ;
  END ;
END ;
EXEC dbo.drop_object @object = N'fact.PurchaseMarkupView', @type = N'V' ;
GO
CREATE VIEW fact.PurchaseMarkupView
AS
  SELECT            pm.StageId                              AS StageId
                  , pm.ExecutionFlag                        AS ExecutionFlag

                  /*** BK ***/
                  , pm.PurchaseMarkupRecID
                  , pm.PurchaseMarkupPartition
                  , pm.CompanyID
                  , pm.DataConnectionID
                  /*** Dimensions ***/
                  , COALESCE(td.DateID, 0)                  AS TransactionDateID
                  , COALESCE(dd.DateID, 0)                  AS [DocumentDateID]
                  , COALESCE(sd.DateID, 0)                  AS [DeliveryDateID]
                  , COALESCE(dadr.AddressID, 0)             AS [DeliveryAddressID]
                  , COALESCE(vAcc.VendorID, 0)              AS [VendorAccountID]
                  , COALESCE(iAcc.VendorID, 0)              AS [InvoiceAccountID]
                  , COALESCE(ppEmp.EmployeeID, 0)           AS [PurchasePlacerID]
                  , COALESCE(prEmp.EmployeeID, 0)           AS [PurchaseRequesterID]
                  , COALESCE(jv.VoucherID, 0)               AS [VoucherID]
                  , COALESCE(dt.DeliveryTermsID, 0)         AS [DeliveryTermID]
                  , COALESCE(modlv.ModeOfDeliveryID, 0)     AS ModeOfDeliveryID
                  , COALESCE(pt.PaymentTermsID, 0)          AS PaymentTermsId
                  , COALESCE(mop.MethodOfPaymentID, 0)      AS MethodOfPaymentID
                  , COALESCE(pp.PurchasePoolID, 0)          AS [PurchasePoolID]
                  , COALESCE(cy.CurrencyID, 0)              AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)             AS CurrencyReportID
                  , COALESCE(pmo.PurchaseMarkupOriginID, 0) AS PurchaseMarkupOriginID
                  , COALESCE(pmt.MarkupTypeID, 0)           AS MarkupTypeID
                  , COALESCE(so.PurchaseOrderID, 0)         AS PurchaseOrderID
                  , COALESCE(si.PurchaseInvoiceID, 0)       AS PurchaseInvoiceID
                  , COALESCE(sps.PurchasePackingSlipID, 0)  AS PurchasePackingSlipID
                  /*** Measures ***/
                  , pm.MarkUpCalculatedAmount
                  , pm.MarkUpCalculatedAmount_RCY
                  , pm.MarkUpPostedAmount
                  , pm.MarkUpPostedAmount_RCY
                  , pm.MarkupTaxAmount
                  , pm.MarkupTaxAmount_RCY
    FROM            delta.PurchaseMarkup     AS pm
    LEFT JOIN       dim.Date                 AS td ON td.Date                     = pm.TransactionDate
    LEFT JOIN       dim.Date                 AS dd ON dd.Date                     = pm.DocumentDate
    LEFT JOIN       dim.Date                 AS sd ON sd.Date                     = pm.DeliveryDate
    LEFT JOIN       dim.Currency             AS cy ON cy.Code                     = pm.CurrencyCode
                                                  AND cy.Partition                = pm.PurchaseMarkupPartition
                                                  AND cy.DataConnectionID         = pm.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport       AS rcy ON rcy.Partition              = pm.PurchaseMarkupPartition
    LEFT JOIN       dim.PurchaseMarkupOrigin AS pmo ON pmo.OriginTableCode        = pm.OriginTableCode
                                                   AND pmo.OriginTable            = pm.OriginTable
                                                   AND pmo.DataConnectionID       = pm.DataConnectionID
    LEFT JOIN       dim.PurchaseOrder        AS so ON so.PurchaseOrder            = pm.PurchaseOrderCode
                                                  AND so.DataConnectionID         = pm.DataConnectionID
                                                  AND so.CompanyID                = pm.CompanyID
                                                  AND so.Partition                = pm.PurchaseMarkupPartition
    LEFT JOIN       dim.PurchaseInvoice      AS si ON si.PurchaseInvoice          = pm.PurchaseInvoice
                                                  AND si.CompanyID                = pm.CompanyID
                                                  AND si.Partition                = pm.PurchaseMarkupPartition
                                                  AND si.DataConnectionID         = pm.DataConnectionID
    LEFT JOIN       dim.PurchasePackingSlip  AS sps ON sps.PurchasePackingSlip    = pm.PurchasePackingSlipCode
                                                   AND sps.Partition              = pm.PurchaseMarkupPartition
                                                   AND sps.CompanyID              = pm.CompanyID
                                                   AND sps.DataConnectionID       = pm.DataConnectionID
    LEFT JOIN       dim.MarkupType           AS pmt ON pmt.ChargesModuleCode      = pm.ModuleType
                                                   AND pmt.MarkupCode             = pm.MarkupCode
                                                   AND pmt.PARTITION              = pm.PurchaseMarkupPartition
                                                   AND pmt.DataConnectionID       = pm.DataConnectionID
                                                   AND pmt.CompanyID              = pm.CompanyID
    LEFT JOIN       dim.Address              AS dadr ON dadr.AddressRecID         = pm.DeliveryAddressCode
                                                    AND dadr.DataConnectionID     = pm.DataConnectionID
                                                    AND dadr.Partition            = pm.PurchaseMarkupPartition
    LEFT JOIN       dim.Vendor               AS vAcc ON vAcc.VendorAccountNo      = pm.VendorAccountCode
                                                    AND vAcc.Partition            = pm.PurchaseMarkupPartition
                                                    AND vAcc.DataConnectionID     = pm.DataConnectionID
                                                    AND vAcc.CompanyID            = pm.CompanyID
    LEFT JOIN       dim.Vendor               AS iAcc ON iAcc.VendorAccountNo      = pm.InvoiceAccountCode
                                                    AND iAcc.Partition            = pm.PurchaseMarkupPartition
                                                    AND iAcc.DataConnectionID     = pm.DataConnectionID
                                                    AND iAcc.CompanyID            = pm.CompanyID
    LEFT JOIN       dim.Employee             AS ppEmp ON ppEmp.EmployeeRecID      = pm.PurchasePlacerCode
                                                     AND ppEmp.Partition          = pm.PurchaseMarkupPartition
                                                     AND ppEmp.DataConnectionID   = pm.DataConnectionID
    LEFT JOIN       dim.Employee             AS prEmp ON prEmp.EmployeeRecID      = pm.PurchaseRequesterCode
                                                     AND prEmp.Partition          = pm.PurchaseMarkupPartition
                                                     AND prEmp.DataConnectionID   = pm.DataConnectionID
    LEFT JOIN       dim.Voucher              AS jv ON jv.Voucher                  = pm.VoucherCode
                                                  AND jv.CompanyID                = pm.CompanyID
                                                  AND jv.Partition                = pm.PurchaseMarkupPartition
                                                  AND jv.DataConnectionID         = pm.DataConnectionID
    LEFT JOIN       dim.DeliveryTerms        AS dt ON dt.DeliveryTermsCode        = pm.DeliveryTermCode
                                                  AND dt.Partition                = pm.PurchaseMarkupPartition
                                                  AND dt.DataConnectionID         = pm.DataConnectionID
                                                  AND dt.CompanyID                = pm.CompanyID
    LEFT JOIN       dim.ModeOfDelivery       AS modlv ON modlv.ModeOfDeliveryCode = pm.DeliveryModeCode
                                                     AND modlv.Partition          = pm.PurchaseMarkupPartition
                                                     AND modlv.DataConnectionID   = pm.DataConnectionID
                                                     AND modlv.CompanyID          = pm.CompanyID
    LEFT JOIN       dim.PaymentTerms         AS pt ON pt.PaymentTermsCode         = pm.PaymentTermCode
                                                  AND pt.Partition                = pm.PurchaseMarkupPartition
                                                  AND pt.DataConnectionID         = pm.DataConnectionID
                                                  AND pt.CompanyID                = pm.CompanyID
    LEFT JOIN       dim.MethodOfPayment      AS mop ON mop.MethodOfPaymentCode    = pm.PaymentModeCode
                                                   AND mop.Partition              = pm.PurchaseMarkupPartition
                                                   AND mop.DataConnectionID       = pm.DataConnectionID
                                                   AND mop.CompanyID              = pm.CompanyID
    LEFT JOIN       dim.PurchasePool         AS pp ON pp.PurchasePoolCode         = pm.PurchasePoolCode
                                                  AND pp.Partition                = pm.PurchaseMarkupPartition
                                                  AND pp.DataConnectionID         = pm.DataConnectionID
                                                  AND pp.CompanyID                = pm.CompanyID ;

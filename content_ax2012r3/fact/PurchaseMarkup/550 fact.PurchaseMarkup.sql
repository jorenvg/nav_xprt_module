EXEC dbo.drop_object 'fact.PurchaseMarkup', 'T' ;
GO
CREATE TABLE [fact].[PurchaseMarkup]
(
  StageId                      BIGINT            NOT NULL
, [CompanyID]                  [INT]             NOT NULL
, [DataConnectionID]           [INT]             NOT NULL
, [ComponentExecutionID]       INT               NOT NULL
, [PurchaseMarkupRecID]        [BIGINT]          NOT NULL
, [PurchaseMarkupPartition]    [BIGINT]          NOT NULL
, [TransactionDateID]          [INT]             NOT NULL
, [DocumentDateID]             [INT]             NOT NULL
, [DeliveryDateID]             [INT]             NOT NULL
, [DeliveryAddressID]          [INT]             NOT NULL
, [VendorAccountID]            [INT]             NOT NULL
, [InvoiceAccountID]           [INT]             NOT NULL
, [PurchasePlacerID]           [INT]             NOT NULL
, [PurchaseRequesterID]        [INT]             NOT NULL
, [VoucherID]                  [INT]             NOT NULL
, [DeliveryTermID]             [INT]             NOT NULL
, ModeOfDeliveryID             [INT]             NOT NULL
, PaymentTermsID               [INT]             NOT NULL
, MethodOfPaymentID            [INT]             NOT NULL
, [PurchasePoolID]             [INT]             NOT NULL
, CurrencyTransactionID        INT               NOT NULL
, CurrencyReportID             INT               NOT NULL
, [PurchaseMarkupOriginID]     [INT]             NOT NULL
, [MarkupTypeID]               [INT]             NOT NULL
, [PurchaseOrderID]            [INT]             NOT NULL
, [PurchaseInvoiceID]          [INT]             NOT NULL
, [PurchasePackingSlipID]      [INT]             NOT NULL
, [MarkUpCalculatedAmount]     DECIMAL(19,4)     NULL
, [MarkUpCalculatedAmount_RCY] DECIMAL(19,4)     NULL
, [MarkUpPostedAmount]         DECIMAL(19,4)     NULL
, [MarkUpPostedAmount_RCY]     DECIMAL(19,4)     NULL
, [MarkupTaxAmount]            DECIMAL(19,4)     NULL
, [MarkupTaxAmount_RCY]        DECIMAL(19,4)     NULL
) ;

EXEC dbo.drop_object 'delta.SalesQuotation', 'T' ;
GO
CREATE TABLE [delta].[SalesQuotation]
(
  [StageId]                   BIGINT            NOT NULL
, [ExecutionFlag]             NCHAR(1)          NOT NULL
, [InventTransCode]           [NVARCHAR](100)   NULL
, [DataAreaCode]              [NVARCHAR](10)    NULL
, [Partition]                 [BIGINT]          NULL
, [CompanyID]                 [INT]             NOT NULL
, [DataConnectionID]          [INT]             NOT NULL
, [ComponentExecutionID]      INT               NOT NULL
, [CreatedDate]               [DATE]            NULL
, [ShippingRequestedDate]     [DATE]            NULL
, [QuotationExpirationDate]   [DATE]            NULL
, [QuotationDocumentSendDate] [DATE]            NULL
, [QuotationDocumentDueDate]  [DATE]            NULL
, [SalesTakerCode]            [BIGINT]          NULL
, [SalesResponsibleCode]      [BIGINT]          NULL
, [CustomerAccountCode]       [NVARCHAR](100)   NULL
, [CompanyCode]               [NVARCHAR](10)    NULL
, [ItemCode]                  [NVARCHAR](100)   NULL
, [ProjectCategoryCode]       [NVARCHAR](100)   NULL
, [CurrencyCode]              [NVARCHAR](10)    NULL
, [FinancialDimensionCode]    [BIGINT]          NULL
, [DeliveryAddressCode]       [BIGINT]          NULL
, [QuotationStatusCode]       [NVARCHAR](100)   NULL
, [QuotationTypeCode]         [NVARCHAR](100)   NULL
, [SalesOriginCode]           [NVARCHAR](100)   NULL
, [SalesPoolCode]             [NVARCHAR](100)   NULL
, [SalesGroupCode]            [NVARCHAR](100)   NULL
, [DeliveryModeCode]          [NVARCHAR](100)   NULL
, [DeliveryTermsCode]         [NVARCHAR](100)   NULL
, [DeliveryTypeCode]          [NVARCHAR](100)   NULL
, [PaymentModeCode]           [NVARCHAR](100)   NULL
, [PaymentTermsCode]          [NVARCHAR](100)   NULL
, [QuotationCode]             [NVARCHAR](100)   NULL
, [SalesUnitCode]             [NVARCHAR](100)   NULL
, [SalesTaxGroupCode]         [NVARCHAR](100)   NULL
, [SalesTaxItemGroupCode]     [NVARCHAR](100)   NULL
, [SalesQuantity]             DECIMAL(19,4)     NULL
, [PriceUnit]                 DECIMAL(19,4)     NULL
, [PriceUnit_RCY]             DECIMAL(19,4)     NULL
, [CostPrice]                 DECIMAL(19,4)     NULL
, [CostPrice_RCY]             DECIMAL(19,4)     NULL
, [QuotationPrice]            DECIMAL(19,4)     NULL
, [QuotationPrice_RCY]        DECIMAL(19,4)     NULL
, [Discount]                  DECIMAL(19,4)     NULL
, [Discount_RCY]              DECIMAL(19,4)     NULL
, [DiscountPercent]           DECIMAL(19,4)     NULL
, [MultilineDiscount]         DECIMAL(19,4)     NULL
, [MultilineDiscount_RCY]     DECIMAL(19,4)     NULL
, [MultilineDiscountPercent]  DECIMAL(19,4)     NULL
, [SalesCharges]              DECIMAL(19,4)     NULL
, [SalesCharges_RCY]          DECIMAL(19,4)     NULL
, [QuotationAmount]           DECIMAL(19,4)     NULL
, [QuotationAmount_RCY]       DECIMAL(19,4)     NULL
) ;
/****************************************************************************************************
    Functionality:  Loads the delta for VendorTransactionSettlement, either incrementally or with a full load based on the @load_type.
			Returns the @inserted, @updated, @deleted records that have been processed on the delta table.
    Created by:          Date:           	
    Date            Changed by      Ticket/Change       Description

*****************************************************************************************************/
EXEC dbo.Drop_Object @object = N'delta.LoadFactVendorTransactionSettlement' -- nvarchar(128)
                   , @type = N'P'                                           -- nchar(2)
                   , @debug = 0 ;                                           -- int
GO

CREATE PROCEDURE delta.LoadFactVendorTransactionSettlement
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @execution_flag NCHAR(1) = N'' ;
  DECLARE @delta_table_name sysname = N'VendorTransactionSettlement' ;
  DECLARE @fact_table_name sysname = N'VendorTransaction' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @delta_table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @fact_table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  DECLARE @last_processed_timestamp BINARY(8) = (SELECT meta.get_last_processed_timestamp(@fact_table_identifier)) ;

  --truncate delta table
  EXECUTE dbo.truncate_table @delta_table_identifier ;

  IF @load_type = 1 --incremental
  BEGIN
    /****************************************************************************************************
  Functionality:  Incrementally inserts all new records for the VendorTransactionSettlement

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'N' ;

    INSERT INTO delta.VendorTransactionSettlement (
      StageId
    , ExecutionFlag
    , DataConnectionID
    , CompanyID
    , ComponentExecutionID
    , VendorTransactionRecID
    , VendorTransactionPartition
    , TransactionSource
    , IsInvoice
    , IsCredit
    , TransType
    , Voucher
    , PurchaseInvoice
    , InvoiceDate
    , PostingDate
    , DueDate
    , LastSettleDate
    , ClosedDate
    , InvoiceAccount
    , DefaultDimension
    , Approver
    , CurrencyCode
    , Amount
    , Amount_RCY
    , AmountMST
    )
    SELECT StageId
         , ExecutionFlag
         , DataConnectionID
         , CompanyID
         , ComponentExecutionID
         , VendorTransactionRecID
         , VendorTransactionPartition
         , TransactionSource
         , IsInvoice
         , IsCredit
         , TransType
         , Voucher
         , PurchaseInvoice
         , InvoiceDate
         , PostingDate
         , DueDate
         , LastSettleDate
         , ClosedDate
         , InvoiceAccount
         , DefaultDimension
         , Approver
         , CurrencyCode
         , Amount
         , Amount_RCY
         , AmountMST
      FROM delta.VendorTransactionSettlementFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SET @Inserted = @@RowCount ;
    /****************************************************************************************************
  Functionality:  Incrementally inserts all updated records for the VendorTransactionSettlement

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'U' ;
    INSERT INTO delta.VendorTransactionSettlement (
      StageId
    , ExecutionFlag
    , DataConnectionID
    , CompanyID
    , ComponentExecutionID
    , VendorTransactionRecID
    , VendorTransactionPartition
    , TransactionSource
    , IsInvoice
    , IsCredit
    , TransType
    , Voucher
    , PurchaseInvoice
    , InvoiceDate
    , PostingDate
    , DueDate
    , LastSettleDate
    , ClosedDate
    , InvoiceAccount
    , DefaultDimension
    , Approver
    , CurrencyCode
    , Amount
    , Amount_RCY
    , AmountMST
    )
    SELECT StageId
         , ExecutionFlag
         , DataConnectionID
         , CompanyID
         , ComponentExecutionID
         , VendorTransactionRecID
         , VendorTransactionPartition
         , TransactionSource
         , IsInvoice
         , IsCredit
         , TransType
         , Voucher
         , PurchaseInvoice
         , InvoiceDate
         , PostingDate
         , DueDate
         , LastSettleDate
         , ClosedDate
         , InvoiceAccount
         , DefaultDimension
         , Approver
         , CurrencyCode
         , Amount
         , Amount_RCY
         , AmountMST
      FROM delta.VendorTransactionSettlementFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SET @Inserted = @@RowCount ;
    /****************************************************************************************************
  Functionality:  Incrementally inserts all deleted records for VendorTransactionSettlement

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'D' ;
    INSERT INTO delta.VendorTransactionSettlement (
      StageId
    , ExecutionFlag
    , DataConnectionID
    , CompanyID
    , ComponentExecutionID
    , VendorTransactionRecID
    , VendorTransactionPartition
    , TransactionSource
    , IsInvoice
    , IsCredit
    , TransType
    , Voucher
    , PurchaseInvoice
    , InvoiceDate
    , PostingDate
    , DueDate
    , LastSettleDate
    , ClosedDate
    , InvoiceAccount
    , DefaultDimension
    , Approver
    , CurrencyCode
    , Amount
    , Amount_RCY
    , AmountMST
    )
    SELECT StageId
         , ExecutionFlag
         , DataConnectionID
         , CompanyID
         , ComponentExecutionID
         , VendorTransactionRecID
         , VendorTransactionPartition
         , TransactionSource
         , IsInvoice
         , IsCredit
         , TransType
         , Voucher
         , PurchaseInvoice
         , InvoiceDate
         , PostingDate
         , DueDate
         , LastSettleDate
         , ClosedDate
         , InvoiceAccount
         , DefaultDimension
         , Approver
         , CurrencyCode
         , Amount
         , Amount_RCY
         , AmountMST
      FROM delta.VendorTransactionSettlementFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SET @Inserted = @@RowCount ;
  END ;
  /****************************************************************************************************
    Functionality:  During a full load with insert all records that have not been deleted. 

    Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/

  IF @load_type = 0 --full load
  BEGIN
    SET @deleted = 0 ;
    SET @updated = 0 ;
    SET @execution_flag = N'N' ;

    INSERT INTO delta.VendorTransactionSettlement (
      StageId
    , ExecutionFlag
    , DataConnectionID
    , CompanyID
    , ComponentExecutionID
    , VendorTransactionRecID
    , VendorTransactionPartition
    , TransactionSource
    , IsInvoice
    , IsCredit
    , TransType
    , Voucher
    , PurchaseInvoice
    , InvoiceDate
    , PostingDate
    , DueDate
    , LastSettleDate
    , ClosedDate
    , InvoiceAccount
    , DefaultDimension
    , Approver
    , CurrencyCode
    , Amount
    , Amount_RCY
    , AmountMST
    )
    SELECT StageId
         , ExecutionFlag
         , DataConnectionID
         , CompanyID
         , ComponentExecutionID
         , VendorTransactionRecID
         , VendorTransactionPartition
         , TransactionSource
         , IsInvoice
         , IsCredit
         , TransType
         , Voucher
         , PurchaseInvoice
         , InvoiceDate
         , PostingDate
         , DueDate
         , LastSettleDate
         , ClosedDate
         , InvoiceAccount
         , DefaultDimension
         , Approver
         , CurrencyCode
         , Amount
         , Amount_RCY
         , AmountMST
      FROM delta.VendorTransactionSettlementFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SET @Inserted = @@RowCount ;
  END ;
END ;

EXEC dbo.drop_object 'fact.VendorTransaction', 'T' ;
GO
CREATE TABLE [fact].[VendorTransaction]
(
  StageID                    BIGINT            NOT NULL
, VendorTransactionRecId     [BIGINT]          NOT NULL
, VendorTransactionPartition BIGINT            NOT NULL
, [CompanyID]                [INT]             NOT NULL
, [DataConnectionID]         [INT]             NOT NULL
, [ComponentExecutionID]     INT               NOT NULL
, TransactionSource          [NVARCHAR](100)   NOT NULL
, IsInvoiceID                [INT]             NOT NULL
, IsCreditID                 [INT]             NOT NULL
, PostingDateID              [INT]             NOT NULL
, DueDateID                  [INT]             NOT NULL
, LastSettleDateID           [INT]             NOT NULL
, ClosedDateID               [INT]             NOT NULL
, ApproverID                 [INT]             NOT NULL
, CurrencyTransactionID      INT               NOT NULL
, CurrencyReportID           INT               NOT NULL
, LedgerTransactionTypeID    [INT]             NOT NULL
, PurchaseInvoiceID          [INT]             NOT NULL
, InvoiceAccountID           [INT]             NOT NULL
, VoucherID                  [INT]             NOT NULL
, FinancialDimensionsID      [INT]             NOT NULL
, Amount                     DECIMAL(19,4)     NULL
, Amount_RCY                 DECIMAL(19,4)     NULL
, AmountMST                  DECIMAL(19,4)     NULL
) ;






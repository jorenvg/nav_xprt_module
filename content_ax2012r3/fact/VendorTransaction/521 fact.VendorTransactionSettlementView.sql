EXEC dbo.drop_object @object = N'fact.VendorTransactionSettlementView', @type = N'V' ;
GO
CREATE VIEW fact.VendorTransactionSettlementView
AS
  SELECT            VTS.StageId                              AS StageID
                  , VTS.ExecutionFlag                        AS ExecutionFlag
                  /***BK***/
                  , VTS.VendorTransactionRecId               AS VendorTransactionRecId
                  , VTS.VendorTransactionPartition           AS VendorTransactionPartition
                  , VTS.CompanyID                            AS CompanyID
                  , VTS.DataConnectionID                     AS DataConnectionID
                  , VTS.TransactionSource                    AS TransactionSource

                  /***Dimensions*/
                  , COALESCE(YN.YesNoID, 0)                  AS IsInvoiceID
                  , COALESCE(YN2.YesNoID, 0)                 AS IsCreditID
                  --Dates
                  , COALESCE(DP.DateID, 0)                   AS PostingDateID
                  , COALESCE(DD.DateID, 0)                   AS DueDateID
                  , COALESCE(DS.DateID, 0)                   AS LastSettleDateID
                  , COALESCE(DC.DateID, 0)                   AS ClosedDateID
                  , COALESCE(APP.EmployeeID, 0)              AS ApproverID
                  , COALESCE(cy.CurrencyID, 0)               AS CurrencyTransactionID
                  , COALESCE(rcy.CurrencyID, 0)              AS CurrencyReportID
                  , COALESCE(LTT.LedgerTransactionTypeID, 0) AS LedgerTransactionTypeID
                  , COALESCE(SI.PurchaseInvoiceID, 0)        AS PurchaseInvoiceID
                  , COALESCE(CI.VendorID, 0)                 AS InvoiceAccountID
                  , COALESCE(VC.VoucherID, 0)                AS VoucherID
                  , COALESCE(FD.FinancialDimensionsID, 0)    AS FinancialDimensionsID

                  /***Measures*/
                  , VTS.Amount                               AS Amount
                  , VTS.Amount_RCY                           AS Amount_RCY
                  , VTS.AmountMST                            AS AmountMST
    FROM            delta.VendorTransactionSettlement AS VTS
    LEFT JOIN       dim.Date                          AS DP ON DP.Date                        = VTS.PostingDate
    LEFT JOIN       dim.Date                          AS DD ON DD.Date                        = VTS.DueDate
    LEFT JOIN       dim.Date                          AS DS ON DS.Date                        = VTS.LastSettleDate
    LEFT JOIN       dim.Date                          AS DC ON DC.Date                        = VTS.ClosedDate
    LEFT JOIN       dim.Currency                      AS cy ON cy.Code                        = VTS.CurrencyCode
                                                           AND cy.Partition                   = VTS.VendorTransactionPartition
                                                           AND cy.DataConnectionID            = VTS.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport                AS rcy ON rcy.Partition                 = VTS.VendorTransactionPartition
    LEFT JOIN       dim.Employee                      AS APP ON APP.EmployeeRecID             = VTS.Approver
                                                            AND APP.Partition                 = VTS.VendorTransactionPartition
                                                            AND APP.DataConnectionID          = VTS.DataConnectionID
    LEFT JOIN       dim.LedgerTransactionType         AS LTT ON LTT.LedgerTransactionTypeCode = VTS.TransType
                                                            AND LTT.DataConnectionID          = VTS.DataConnectionID
    LEFT JOIN       dim.PurchaseInvoice               AS SI ON SI.PurchaseInvoice             = VTS.PurchaseInvoice
                                                           AND SI.Partition                   = VTS.VendorTransactionPartition
                                                           AND SI.DataConnectionID            = VTS.DataConnectionID
                                                           AND SI.CompanyID                   = VTS.CompanyID
    LEFT JOIN       dim.Vendor                        AS CI ON CI.VendorAccountNo             = VTS.InvoiceAccount
                                                           AND CI.Partition                   = VTS.VendorTransactionPartition
                                                           AND CI.CompanyID                   = VTS.CompanyID
                                                           AND CI.DataConnectionID            = VTS.DataConnectionID
    LEFT JOIN       dim.Voucher                       AS VC ON VC.Voucher                     = VTS.Voucher
                                                           AND VC.Partition                   = VTS.VendorTransactionPartition
                                                           AND VC.CompanyID                   = VTS.CompanyID
                                                           AND VC.DataConnectionID            = VTS.DataConnectionID
    LEFT JOIN       dim.FinancialDimensions           AS FD ON FD.DefaultDimension            = VTS.DefaultDimension
                                                           AND FD.Partition                   = VTS.VendorTransactionPartition
                                                           AND FD.DataConnectionID            = VTS.DataConnectionID
    LEFT JOIN       dim.YesNo                         AS YN ON YN.YesNoCode                   = VTS.IsInvoice
    LEFT JOIN       dim.YesNo                         AS YN2 ON YN2.YesNoCode                 = VTS.IsCredit ;


GO


/****************************************************************************************************
  Functionality: Selects the changed vendor settlements for use of the vendor transactions fact table.
  Created by:    JvL	Date:	2018/11/20
  Date 		    Changed by 	Ticket/Change 	Description
  2018/11/20 	JvL 		    DEV-1396 		    changed Voucher to include the OFFSETTRANSVOUCHER instead of using the voucher of the transaction
                                          removed the filter on transtype <> 9 (ExchAdjustment) as this prevented settlements from being included.
*****************************************************************************************************/

EXEC dbo.drop_object 'delta.VendorTransactionSettlementFunction', 'F' ;
GO
CREATE FUNCTION delta.VendorTransactionSettlementFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT      ct.stage_id                                  AS StageId
                     , @execution_flag                              AS ExecutionFlag
                     , ct.DATA_CONNECTION_ID                        AS DataConnectionID
                     , ct.COMPANY_ID                                AS CompanyID
                     , @component_execution_id                      AS ComponentExecutionID
                     , ct.RECID                                     AS VendorTransactionRecID
                     , ct.PARTITION                                 AS VendorTransactionPartition
                     , 'Settlement'                                 AS TransactionSource
                     , 0                                            AS IsInvoice
                     , CASE WHEN ct.AMOUNTMST < 0 THEN 1 ELSE 0 END AS IsCredit
                     , ct.TRANSTYPE                                 AS TransType
                     , cs.OFFSETTRANSVOUCHER                        AS Voucher
                     , ct.INVOICE                                   AS PurchaseInvoice
                     , ct.TRANSDATE                                 AS InvoiceDate
                     , CS.TRANSDATE                                 AS PostingDate
                     , ct.DUEDATE                                   AS DueDate
                     , ct.LASTSETTLEDATE                            AS LastSettleDate
                     , ct.Closed                                    AS ClosedDate
                     , ct.ACCOUNTNUM                                AS InvoiceAccount
                     , CS.DEFAULTDIMENSION                          AS DefaultDimension
                     , ct.APPROVER                                  AS Approver
                     , ct.CurrencyCode                              AS CurrencyCode
                     , CS.SETTLEAMOUNTCUR * -1                      AS Amount
                     , CS.SETTLEAMOUNTCUR * ex.CrossRate * -1       AS Amount_RCY
                     , CS.SETTLEAMOUNTMST * -1                      AS AmountMST
             FROM      stage_ax.VENDSETTLEMENT  AS CS
            INNER JOIN stage_ax.VENDTRANS       AS ct ON CS.TRANSRECID         = ct.recid
                                                     AND CS.data_connection_id = ct.data_connection_id
             LEFT JOIN reporting_exchange_rates ex ON ex.FromCurrencyCode      = ct.CURRENCYCODE
                                                  AND ex.DataConnectionID      = ct.data_connection_id
                                                  AND CS.TRANSDATE BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE
             /** FULL LOAD **/
                       ( @load_type         = 0
                     AND CS.execution_flag  <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                       ( @load_type                 = 1
                     AND @execution_flag            = 'N'
                     AND CS.execution_flag          = @execution_flag
                     AND CS.execution_timestamp     > @last_processed_timestamp)
               OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( CS.execution_flag           = @execution_flag
                             AND CS.execution_timestamp      > @last_processed_timestamp)
                            OR ( CT.execution_flag            = @execution_flag
                             AND CT.execution_timestamp       > @last_processed_timestamp)))
                      OR
                      /** INCREMENTAL, DELETED RECORDS **/
                         ( @load_type                  = 1
                       AND @execution_flag             = 'D'
                       AND CS.execution_flag           = @execution_flag
                       AND CS.execution_timestamp      > @last_processed_timestamp));
GO

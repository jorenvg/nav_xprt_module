EXEC dbo.drop_object 'fact.SalesForecast', 'T' ;
GO
CREATE TABLE [fact].[SalesForecast]
(
  StageID                      BIGINT            NOT NULL
, [ComponentExecutionID]       INT               NOT NULL
, [CompanyID]                  [INT]             NOT NULL
, [DataConnectionID]           [INT]             NOT NULL
, [SalesForecastRecID]         [BIGINT]          NOT NULL
, [SalesForecastPartition]     [BIGINT]          NOT NULL
, [FinancialDimensionsID]      [INT]             NOT NULL
, [ForecastStartDateID]        [INT]             NOT NULL
, [ForecastEndDateID]          [INT]             NOT NULL
, [ItemID]                     [INT]             NOT NULL
, [ForecastModelID]            [INT]             NOT NULL
, [CustomerAccountID]          [INT]             NOT NULL
, CurrencyTransactionID        INT               NOT NULL
, CurrencyReportID             INT               NOT NULL
, [WMSlocationID]              [INT]             NOT NULL
, [LocationID]                 [INT]             NOT NULL
, [ProductID]                  [INT]             NOT NULL
, [TrackingID]                 [INT]             NOT NULL
, [ForecastInventoryQuantity]  DECIMAL(19,4)     NULL
, [ForecastSalesQuantity]      DECIMAL(19,4)     NULL
, [ForecastDiscountPercent]    DECIMAL(19,4)     NULL
, [ForecastSalesUnit]          [NVARCHAR](100)   NULL
, [ForecastAmount]             DECIMAL(19,4)     NULL
, [ForecastAmount_RCY]         DECIMAL(19,4)     NULL
, [ForecastCostPrice]          DECIMAL(19,4)     NULL
, [ForecastCostPrice_RCY]      DECIMAL(19,4)     NULL
, [ForecastSalesPrice]         DECIMAL(19,4)     NULL
, [ForecastSalesPrice_RCY]     DECIMAL(19,4)     NULL
, [ForecastDiscountAmount]     DECIMAL(19,4)     NULL
, [ForecastDiscountAmount_RCY] DECIMAL(19,4)     NULL
, [ForecastPriceUnit]          DECIMAL(19,4)     NULL
, [ForecastPriceUnit_RCY]      DECIMAL(19,4)     NULL
) ;

/****************************************************************************************************
    Functionality:  Loads the delta for SalesForecast, either incrementally or with a full load based on the @load_type.
			Returns the @inserted, @updated, @deleted records that have been processed on the delta table.
    Created by:          Date:           	
    Date            Changed by      Ticket/Change       Description

*****************************************************************************************************/
EXEC dbo.Drop_Object @object = N'delta.LoadFactSalesForecast' -- nvarchar(128)
                   , @type = N'P'                             -- nchar(2)
                   , @debug = 0 ;                             -- int
GO

CREATE PROCEDURE delta.LoadFactSalesForecast
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1 -- 0 = fullload, 1= incremental-grain 
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET NOCOUNT ON ;
  DECLARE @execution_flag NCHAR(1) = N'' ;
  DECLARE @table_name sysname = N'SalesForecast' ;
  DECLARE @delta_table_identifier sysname = N'delta.' + @table_name ;
  DECLARE @fact_table_identifier sysname = N'fact.' + @table_name ;

  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  DECLARE @last_processed_timestamp BINARY(8) = (SELECT meta.get_last_processed_timestamp(@fact_table_identifier)) ;

  --truncate delta table
  EXECUTE dbo.truncate_table @delta_table_identifier ;

  IF @load_type = 1 --incremental
  BEGIN
    /****************************************************************************************************
  Functionality:  Incrementally inserts all new records for the SalesForecast

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'N' ;

    INSERT INTO delta.SalesForecast (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , DataConnectionID
    , CompanyId
    , SalesForecastRecId
    , SalesForecastPartition
    , DefaultDimensionCode
    , ForecastStartDate
    , ForecastEndDate
    , ItemCode
    , ForecastModelCode
    , ProjectCode
    , CustomerAccountCode
    , CurrencyCode
    , ForecastComment
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , ForecastInventoryQuantity
    , ForecastSalesQuantity
    , ForecastDiscountPercent
    , ForecastSalesUnit
    , ForecastAmount
    , ForecastAmount_RCY
    , ForecastCostPrice
    , ForecastCostPrice_RCY
    , ForecastSalesPrice
    , ForecastSalesPrice_RCY
    , ForecastDiscountAmount
    , ForecastDiscountAmount_RCY
    , ForecastPriceUnit
    , ForecastPriceUnit_RCY
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , DataConnectionID
         , CompanyId
         , SalesForecastRecId
         , SalesForecastPartition
         , DefaultDimensionCode
         , ForecastStartDate
         , ForecastEndDate
         , ItemCode
         , ForecastModelCode
         , ProjectCode
         , CustomerAccountCode
         , CurrencyCode
         , ForecastComment
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , ForecastInventoryQuantity
         , ForecastSalesQuantity
         , ForecastDiscountPercent
         , ForecastSalesUnit
         , ForecastAmount
         , ForecastAmount_RCY
         , ForecastCostPrice
         , ForecastCostPrice_RCY
         , ForecastSalesPrice
         , ForecastSalesPrice_RCY
         , ForecastDiscountAmount
         , ForecastDiscountAmount_RCY
         , ForecastPriceUnit
         , ForecastPriceUnit_RCY
      FROM delta.SalesForecastFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @Inserted = @@RowCount ;
    /****************************************************************************************************
  Functionality:  Incrementally inserts all updated records for the SalesForecast

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'U' ;

    INSERT INTO delta.SalesForecast (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , DataConnectionID
    , CompanyId
    , SalesForecastRecId
    , SalesForecastPartition
    , DefaultDimensionCode
    , ForecastStartDate
    , ForecastEndDate
    , ItemCode
    , ForecastModelCode
    , ProjectCode
    , CustomerAccountCode
    , CurrencyCode
    , ForecastComment
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , ForecastInventoryQuantity
    , ForecastSalesQuantity
    , ForecastDiscountPercent
    , ForecastSalesUnit
    , ForecastAmount
    , ForecastAmount_RCY
    , ForecastCostPrice
    , ForecastCostPrice_RCY
    , ForecastSalesPrice
    , ForecastSalesPrice_RCY
    , ForecastDiscountAmount
    , ForecastDiscountAmount_RCY
    , ForecastPriceUnit
    , ForecastPriceUnit_RCY
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , DataConnectionID
         , CompanyId
         , SalesForecastRecId
         , SalesForecastPartition
         , DefaultDimensionCode
         , ForecastStartDate
         , ForecastEndDate
         , ItemCode
         , ForecastModelCode
         , ProjectCode
         , CustomerAccountCode
         , CurrencyCode
         , ForecastComment
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , ForecastInventoryQuantity
         , ForecastSalesQuantity
         , ForecastDiscountPercent
         , ForecastSalesUnit
         , ForecastAmount
         , ForecastAmount_RCY
         , ForecastCostPrice
         , ForecastCostPrice_RCY
         , ForecastSalesPrice
         , ForecastSalesPrice_RCY
         , ForecastDiscountAmount
         , ForecastDiscountAmount_RCY
         , ForecastPriceUnit
         , ForecastPriceUnit_RCY
      FROM delta.SalesForecastFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @Inserted = @@RowCount ;
    /****************************************************************************************************
  Functionality:  Incrementally inserts all deleted records for SalesForecast

  Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
    SET @execution_flag = N'D' ;

    INSERT INTO delta.SalesForecast (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , DataConnectionID
    , CompanyId
    , SalesForecastRecId
    , SalesForecastPartition
    , DefaultDimensionCode
    , ForecastStartDate
    , ForecastEndDate
    , ItemCode
    , ForecastModelCode
    , ProjectCode
    , CustomerAccountCode
    , CurrencyCode
    , ForecastComment
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , ForecastInventoryQuantity
    , ForecastSalesQuantity
    , ForecastDiscountPercent
    , ForecastSalesUnit
    , ForecastAmount
    , ForecastAmount_RCY
    , ForecastCostPrice
    , ForecastCostPrice_RCY
    , ForecastSalesPrice
    , ForecastSalesPrice_RCY
    , ForecastDiscountAmount
    , ForecastDiscountAmount_RCY
    , ForecastPriceUnit
    , ForecastPriceUnit_RCY
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , DataConnectionID
         , CompanyId
         , SalesForecastRecId
         , SalesForecastPartition
         , DefaultDimensionCode
         , ForecastStartDate
         , ForecastEndDate
         , ItemCode
         , ForecastModelCode
         , ProjectCode
         , CustomerAccountCode
         , CurrencyCode
         , ForecastComment
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , ForecastInventoryQuantity
         , ForecastSalesQuantity
         , ForecastDiscountPercent
         , ForecastSalesUnit
         , ForecastAmount
         , ForecastAmount_RCY
         , ForecastCostPrice
         , ForecastCostPrice_RCY
         , ForecastSalesPrice
         , ForecastSalesPrice_RCY
         , ForecastDiscountAmount
         , ForecastDiscountAmount_RCY
         , ForecastPriceUnit
         , ForecastPriceUnit_RCY
      FROM delta.SalesForecastFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @Inserted = @@RowCount ;
  END ;
  /****************************************************************************************************
    Functionality:  During a full load with insert all records that have not been deleted. 

    Date            Changed by      Ticket/Change       Description
*****************************************************************************************************/
  IF @load_type = 0 --full load
  BEGIN
    SET @deleted = 0 ;
    SET @updated = 0 ;
    SET @execution_flag = N'N' ;

    INSERT INTO delta.SalesForecast (
      StageId
    , ExecutionFlag
    , ComponentExecutionId
    , DataConnectionID
    , CompanyId
    , SalesForecastRecId
    , SalesForecastPartition
    , DefaultDimensionCode
    , ForecastStartDate
    , ForecastEndDate
    , ItemCode
    , ForecastModelCode
    , ProjectCode
    , CustomerAccountCode
    , CurrencyCode
    , ForecastComment
    , WMSlocationCode
    , LocationCode
    , ProductConfigCode
    , ProductColorCode
    , ProductInventSizeCode
    , ProductStyleCode
    , TrackingBatchCode
    , TrackingSerialCode
    , ForecastInventoryQuantity
    , ForecastSalesQuantity
    , ForecastDiscountPercent
    , ForecastSalesUnit
    , ForecastAmount
    , ForecastAmount_RCY
    , ForecastCostPrice
    , ForecastCostPrice_RCY
    , ForecastSalesPrice
    , ForecastSalesPrice_RCY
    , ForecastDiscountAmount
    , ForecastDiscountAmount_RCY
    , ForecastPriceUnit
    , ForecastPriceUnit_RCY
    )
    SELECT StageId
         , ExecutionFlag
         , ComponentExecutionId
         , DataConnectionID
         , CompanyId
         , SalesForecastRecId
         , SalesForecastPartition
         , DefaultDimensionCode
         , ForecastStartDate
         , ForecastEndDate
         , ItemCode
         , ForecastModelCode
         , ProjectCode
         , CustomerAccountCode
         , CurrencyCode
         , ForecastComment
         , WMSlocationCode
         , LocationCode
         , ProductConfigCode
         , ProductColorCode
         , ProductInventSizeCode
         , ProductStyleCode
         , TrackingBatchCode
         , TrackingSerialCode
         , ForecastInventoryQuantity
         , ForecastSalesQuantity
         , ForecastDiscountPercent
         , ForecastSalesUnit
         , ForecastAmount
         , ForecastAmount_RCY
         , ForecastCostPrice
         , ForecastCostPrice_RCY
         , ForecastSalesPrice
         , ForecastSalesPrice_RCY
         , ForecastDiscountAmount
         , ForecastDiscountAmount_RCY
         , ForecastPriceUnit
         , ForecastPriceUnit_RCY
      FROM delta.SalesForecastFunction(@last_processed_timestamp, @execution_flag, @load_type, @component_execution_id) ;
    SELECT @Inserted = @@RowCount ;
  END ;
END ;
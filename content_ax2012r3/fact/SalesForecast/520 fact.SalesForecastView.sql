EXEC dbo.drop_object @object = N'fact.SalesForecastView', @type = N'V' ;
GO
CREATE VIEW fact.SalesForecastView
AS
  SELECT            StageID                    = fsf.StageID
                  , ExecutionFlag              = fsf.ExecutionFlag
                  --	/***BK***/
                  , SalesForecastRecID         = fsf.SalesForecastRecID
                  , SalesForecastPartition     = fsf.SalesForecastPartition
                  , CompanyID                  = fsf.CompanyID
                  , DataConnectionID           = fsf.DataConnectionID
                  --	 Dimensions
                  , FinancialDimensionsID      = COALESCE(fd.FinancialDimensionsID, 0)
                  , ForecastStartDateID        = COALESCE(sd.DateID, 0)
                  , ForecastEndDateID          = COALESCE(ed.DateID, 0)
                  , ItemID                     = COALESCE(i.ItemID, 0)
                  , ForecastModelID            = COALESCE(fm.ForecastModelID, 0)
                  --,[ProjectCode]
                  , CustomerAccountID          = COALESCE(cAcc.CustomerID, 0)
                  , CurrencyTransactionID      = COALESCE(cy.CurrencyID, 0)
                  , CurrencyReportID           = COALESCE(rcy.CurrencyID, 0)
                  --,[ForecastComment]
                  , WMSlocationID              = COALESCE(wl.WMSLocationID, 0)
                  , LocationID                 = COALESCE(l.LocationID, 0)
                  , ProductID                  = COALESCE(p.ProductID, 0)
                  , TrackingID                 = COALESCE(t.TrackingID, 0)
                  --	Measures
                  , ForecastInventoryQuantity  = fsf.ForecastInventoryQuantity
                  , ForecastSalesQuantity      = fsf.ForecastSalesQuantity
                  , ForecastDiscountPercent    = fsf.ForecastDiscountPercent
                  , ForecastSalesUnit          = fsf.ForecastSalesUnit
                  , ForecastAmount             = fsf.ForecastAmount
                  , ForecastAmount_RCY         = fsf.ForecastAmount_RCY
                  , ForecastCostPrice          = fsf.ForecastCostPrice
                  , ForecastCostPrice_RCY      = fsf.ForecastCostPrice_RCY
                  , ForecastSalesPrice         = fsf.ForecastSalesPrice
                  , ForecastSalesPrice_RCY     = fsf.ForecastSalesPrice_RCY
                  , ForecastDiscountAmount     = fsf.ForecastDiscountAmount
                  , ForecastDiscountAmount_RCY = fsf.ForecastDiscountAmount_RCY
                  , ForecastPriceUnit          = fsf.ForecastPriceUnit
                  , ForecastPriceUnit_RCY      = fsf.ForecastPriceUnit_RCY
    FROM            delta.SalesForecast     AS fsf
    LEFT JOIN       dim.FinancialDimensions AS fd ON fd.DefaultDimension      = fsf.DefaultDimensionCode
                                                 AND fd.Partition             = fsf.SalesForecastPartition
                                                 AND fd.DataConnectionID      = fsf.DataConnectionID
    LEFT JOIN       dim.Date                AS sd ON sd.Date                  = fsf.ForecastStartDate
    LEFT JOIN       dim.Date                AS ed ON ed.Date                  = fsf.ForecastEndDate
    LEFT JOIN       dim.Item                AS i ON i.ItemNumber              = fsf.ItemCode
                                                AND i.Partition               = fsf.SalesForecastPartition
                                                AND i.DataConnectionID        = fsf.DataConnectionID
                                                AND i.CompanyID               = fsf.CompanyID
    LEFT JOIN       dim.ForecastModel       AS fm ON fm.ForecastModelCode     = fsf.ForecastModelCode
                                                 AND fm.Partition             = fsf.SalesForecastPartition
                                                 AND fm.DataConnectionID      = fsf.DataConnectionID
                                                 AND fm.CompanyID             = fsf.CompanyID
    LEFT JOIN       dim.Customer            AS cAcc ON cAcc.CustomerAccountNo = fsf.CustomerAccountCode
                                                   AND cAcc.Partition         = fsf.SalesForecastPartition
                                                   AND cAcc.DataConnectionID  = fsf.DataConnectionID
                                                   AND cAcc.CompanyID         = fsf.CompanyID
    LEFT JOIN       dim.Currency            AS cy ON cy.Code                  = fsf.CurrencyCode
                                                 AND cy.Partition             = fsf.SalesForecastPartition
                                                 AND cy.DataConnectionID      = fsf.DataConnectionID
    LEFT OUTER JOIN dim.CurrencyReport      rcy ON fsf.SalesForecastPartition = rcy.Partition
    LEFT JOIN       dim.WMSLocation         AS wl ON wl.WMSLocationCode       = fsf.WMSlocationCode
                                                 AND wl.LocationCode          = fsf.LocationCode
                                                 AND wl.Partition             = fsf.SalesForecastPartition
                                                 AND wl.DataConnectionID      = fsf.DataConnectionID
                                                 AND wl.CompanyID             = fsf.CompanyID
    LEFT JOIN       dim.Location            AS l ON l.LocationCode            = fsf.LocationCode
                                                AND l.Partition               = fsf.SalesForecastPartition
                                                AND l.DataConnectionID        = fsf.DataConnectionID
                                                AND l.CompanyID               = fsf.CompanyID
    LEFT JOIN       dim.Product             AS p ON p.InventSize              = fsf.ProductInventSizeCode
                                                AND p.InventColor             = fsf.ProductColorCode
                                                AND p.InventStyle             = fsf.ProductStyleCode
                                                AND p.InventConfiguration     = fsf.ProductConfigCode
                                                AND p.Partition               = fsf.SalesForecastPartition
                                                AND p.DataConnectionID        = fsf.DataConnectionID
                                                AND p.CompanyID               = fsf.CompanyID
    LEFT JOIN       dim.Tracking            AS t ON t.InventBatchCode         = fsf.TrackingBatchCode
                                                AND t.InventSerialCode        = fsf.TrackingSerialCode
                                                AND t.Partition               = fsf.SalesForecastPartition
                                                AND t.DataConnectionID        = fsf.DataConnectionID
                                                AND t.CompanyID               = fsf.CompanyID ;




GO


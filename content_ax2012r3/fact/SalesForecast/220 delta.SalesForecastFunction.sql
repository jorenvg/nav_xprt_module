EXEC dbo.drop_object 'delta.SalesForecastFunction', 'F' ;
GO
CREATE FUNCTION delta.SalesForecastFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT      FCS.stage_id                                     AS StageID
                     , @execution_flag                                  AS ExecutionFlag
                     , @component_execution_id                          AS ComponentExecutionId
                     , FCS.data_connection_id                           AS DataConnectionID
                     , FCS.company_id                                   AS CompanyId
                     , FCS.RECID                                        AS SalesForecastRecId
                     , FCS.PARTITION                                    AS SalesForecastPartition
                     , FCS.DEFAULTDIMENSION                             AS DefaultDimensionCode
                     , FCS.STARTDATE                                    AS ForecastStartDate
                     , FCS.ENDDATE                                      AS ForecastEndDate
                     , FCS.ITEMID                                       AS ItemCode
                     , FCS.MODELID                                      AS ForecastModelCode
                     , FCS.PROJID                                       AS ProjectCode
                     , FCS.CUSTACCOUNTID                                AS CustomerAccountCode
                     , FCS.CURRENCY                                     AS CurrencyCode
                     , FCS.COMMENT_                                     AS ForecastComment
                     , COALESCE(NULLIF(ID.WMSLOCATIONID, ''), 'N/A')    AS WMSlocationCode
                     , COALESCE(NULLIF(ID.INVENTLOCATIONID, ''), 'N/A') AS LocationCode
                     , COALESCE(NULLIF(ID.CONFIGID, ''), 'N/A')         AS ProductConfigCode
                     , COALESCE(NULLIF(ID.INVENTCOLORID, ''), 'N/A')    AS ProductColorCode
                     , COALESCE(NULLIF(ID.INVENTSIZEID, ''), 'N/A')     AS ProductInventSizeCode
                     , COALESCE(NULLIF(ID.INVENTSTYLEID, ''), 'N/A')    AS ProductStyleCode
                     , COALESCE(NULLIF(ID.INVENTBATCHID, ''), 'N/A')    AS TrackingBatchCode
                     , COALESCE(NULLIF(ID.INVENTSERIALID, ''), 'N/A')   AS TrackingSerialCode
                     , FCS.INVENTQTY                                    AS ForecastInventoryQuantity
                     , FCS.SALESQTY                                     AS ForecastSalesQuantity
                     , FCS.DISCPERCENT                                  AS ForecastDiscountPercent
                     , FCS.SALESUNITID                                  AS ForecastSalesUnit
                     , FCS.AMOUNT                                       AS ForecastAmount
                     , FCS.AMOUNT * ex.CrossRate                        AS ForecastAmount_RCY
                     , FCS.COSTPRICE                                    AS ForecastCostPrice
                     , FCS.COSTPRICE * ex.CrossRate                     AS ForecastCostPrice_RCY
                     , FCS.SALESPRICE                                   AS ForecastSalesPrice
                     , FCS.SALESPRICE * ex.CrossRate                    AS ForecastSalesPrice_RCY
                     , FCS.DISCAMOUNT                                   AS ForecastDiscountAmount
                     , FCS.DISCAMOUNT * ex.CrossRate                    AS ForecastDiscountAmount_RCY
                     , FCS.PRICEUNIT                                    AS ForecastPriceUnit
                     , FCS.PRICEUNIT * ex.CrossRate                     AS ForecastPriceUnit_RCY
             FROM      stage_ax.FORECASTSALES   AS FCS
             LEFT JOIN stage_ax.INVENTDIM       ID ON FCS.INVENTDIMID        = ID.INVENTDIMID
                                                  AND FCS.DATAAREAID         = ID.DATAAREAID
                                                  AND FCS.PARTITION          = ID.PARTITION
                                                  AND FCS.data_connection_id = ID.data_connection_id
                                                  AND FCS.company_id         = ID.company_id
             LEFT JOIN reporting_exchange_rates ex ON FCS.CURRENCY           = ex.FromCurrencyCode
                                                  AND FCS.data_connection_id = ex.DataConnectionID
                                                  AND FCS.STARTDATE BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE      FCS.ACTIVE            = 1
              AND

              /** FULL LOAD **/
                       ( @load_type                   = 0
                     AND FCS.execution_flag           <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                       ( @load_type                  = 1
                     AND @execution_flag             = 'N'
                     AND FCS.execution_flag          = @execution_flag
                     AND FCS.execution_timestamp     > @last_processed_timestamp)
               OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( FCS.execution_flag           = @execution_flag
                             AND FCS.execution_timestamp      > @last_processed_timestamp)
                            OR ( ID.execution_flag            = @execution_flag
                             AND ID.execution_timestamp       > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                       ( @load_type                  = 1
                     AND @execution_flag             = 'D'
                     AND FCS.execution_flag          = @execution_flag
                     AND FCS.execution_timestamp     > @last_processed_timestamp)) ;
GO

EXEC dbo.drop_object 'delta.SalesForecast', 'T' ;
GO
CREATE TABLE [delta].[SalesForecast]
(
  StageId                      BIGINT            NOT NULL
, ExecutionFlag                NCHAR(1)          NOT NULL
, ComponentExecutionId         INT               NOT NULL
, [DataConnectionID]           [INT]             NOT NULL
, [CompanyId]                  [INT]             NOT NULL
, [SalesForecastRecId]         [BIGINT]          NULL
, [SalesForecastPartition]     [BIGINT]          NULL
, [DefaultDimensionCode]       [BIGINT]          NULL
, [ForecastStartDate]          [DATETIME]        NULL
, [ForecastEndDate]            [DATETIME]        NULL
, [ItemCode]                   [NVARCHAR](100)   NULL
, [ForecastModelCode]          [NVARCHAR](100)   NULL
, [ProjectCode]                [NVARCHAR](100)   NULL
, [CustomerAccountCode]        [NVARCHAR](100)   NULL
, [CurrencyCode]               [NVARCHAR](10)    NULL
, [ForecastComment]            [NVARCHAR](255)   NULL
, [WMSlocationCode]            [NVARCHAR](100)   NULL
, [LocationCode]               [NVARCHAR](100)   NULL
, [ProductConfigCode]          [NVARCHAR](100)   NULL
, [ProductColorCode]           [NVARCHAR](100)   NULL
, [ProductInventSizeCode]      [NVARCHAR](100)   NULL
, [ProductStyleCode]           [NVARCHAR](100)   NULL
, [TrackingBatchCode]          [NVARCHAR](100)   NULL
, [TrackingSerialCode]         [NVARCHAR](100)   NULL
, [ForecastInventoryQuantity]  DECIMAL(19,4)     NULL
, [ForecastSalesQuantity]      DECIMAL(19,4)     NULL
, [ForecastDiscountPercent]    DECIMAL(19,4)     NULL
, [ForecastSalesUnit]          [NVARCHAR](100)   NULL
, [ForecastAmount]             DECIMAL(19,4)     NULL
, [ForecastAmount_RCY]         DECIMAL(19,4)     NULL
, [ForecastCostPrice]          DECIMAL(19,4)     NULL
, [ForecastCostPrice_RCY]      DECIMAL(19,4)     NULL
, [ForecastSalesPrice]         DECIMAL(19,4)     NULL
, [ForecastSalesPrice_RCY]     DECIMAL(19,4)     NULL
, [ForecastDiscountAmount]     DECIMAL(19,4)     NULL
, [ForecastDiscountAmount_RCY] DECIMAL(19,4)     NULL
, [ForecastPriceUnit]          DECIMAL(19,4)     NULL
, [ForecastPriceUnit_RCY]      DECIMAL(19,4)     NULL
) ;
EXEC dbo.drop_object 'delta.SalesPackingFunction', 'F' ;
GO
CREATE FUNCTION delta.SalesPackingFunction
(
  @last_processed_timestamp BINARY(8)
, @execution_flag           NCHAR(1) = 'N'
, @load_type                BIT      = 0
, @component_execution_id   INT      = NULL
)
RETURNS TABLE
AS
  RETURN ( WITH reporting_exchange_rates AS (
             SELECT er.CrossRate
                  , er.ExchangeRate1
                  , er.ExchangeRate2
                  , er.ExchangeRateType
                  , er.FromCurrencyCode
                  , er.ToCurrencyCode
                  , er.ValidFrom
                  , er.ValidTo
                  , er.Partition
                  , er.DataConnectionID
               FROM help.ExchangeRates    AS er
               JOIN meta.current_instance AS ci ON ci.reporting_currency_code = er.ToCurrencyCode
           )
           SELECT            CPST.stage_id                                                AS StageId
                           , @execution_flag                                              AS ExecutionFlag
                           , @component_execution_id                                      AS ComponentExecutionId
                           , CPST.RECID                                                   AS SalesPackingTransRecId
                           , CPST.PARTITION                                               AS SalesPackingTransPartition
                           , CPST.company_id                                              AS CompanyID
                           , CPST.data_connection_id                                      AS DataConnectionID
                           , CAST(CPSJ.DOCUMENTDATE AS DATE)                              AS DocumentDate
                           , ST.RECEIPTDATEREQUESTED                                      AS ReceiptRequestedDate
                           , CPST.SALESLINESHIPPINGDATEREQUESTED                          AS ShippingRequestedDate
                           , CPST.SALESLINESHIPPINGDATECONFIRMED                          AS ShippingConfirmedDate
                           , ST.RECEIPTDATECONFIRMED                                      AS ReceiptConfirmedDate
                           , CPST.DELIVERYDATE                                            AS DeliveryDate
                           , COALESCE(NULLIF(ID.WMSLOCATIONID, ''), 'N/A')                AS WMSlocationCode
                           , COALESCE(NULLIF(ID.INVENTLOCATIONID, ''), 'N/A')             AS LocationCode
                           , COALESCE(NULLIF(ID.CONFIGID, ''), 'N/A')                     AS ProductConfigCode
                           , COALESCE(NULLIF(ID.INVENTCOLORID, ''), 'N/A')                AS ProductColorCode
                           , COALESCE(NULLIF(ID.INVENTSIZEID, ''), 'N/A')                 AS ProductInventSizeCode
                           , COALESCE(NULLIF(ID.INVENTSTYLEID, ''), 'N/A')                AS ProductStyleCode
                           , COALESCE(NULLIF(ID.INVENTBATCHID, ''), 'N/A')                AS TrackingBatchCode
                           , COALESCE(NULLIF(ID.INVENTSERIALID, ''), 'N/A')               AS TrackingSerialCode
                           , CPSJ.WORKERSALESTAKER                                        AS SalesTakerCode
                           , ST.WORKERSALESRESPONSIBLE                                    AS SalesResponsibleCode
                           , CPSJ.ORDERACCOUNT                                            AS CustomerAccountCode
                           , CPSJ.INVOICEACCOUNT                                          AS InvoiceAccountCode
                           , CPST.DELIVERYPOSTALADDRESS                                   AS DeliveryAddressCode
                           , CPSJ.INVOICEPOSTALADDRESS                                    AS InvoiceAddressCode
                           , CPST.DATAAREAID                                              AS CompanyCode
                           , CPST.ITEMID                                                  AS ItemCode
                           , CPST.CURRENCYCODE                                            AS CurrencyCode
                           , CPST.DEFAULTDIMENSION                                        AS FinancialDimensionCode
                           , CPSJ.SALESTYPE                                               AS OrderTypeCode
                           , ST.SALESORIGINID                                             AS SalesOriginCode
                           , ST.SALESPOOLID                                               AS SalesPoolCode
                           , CPSJ.DLVMODE                                                 AS DeliveryModeCode
                           , COALESCE(NULLIF(CPST.DLVTERM, ''), NULLIF(CPSJ.DLVTERM, '')) AS DeliveryTermsCode
                           , ST.PAYMMODE                                                  AS PaymentModeCode
                           , ST.PAYMENT                                                   AS PaymentTermsCode
                           , CPST.SALESID                                                 AS SalesCode
                           , CPST.SALESGROUP                                              AS SalesGroupCode
                           , CPST.PACKINGSLIPID                                           AS SalesPackingSlip
                           , CPST.SALESUNIT                                               AS SalesUnitCode
                           , CPSJ.LEDGERVOUCHER                                           AS VoucherCode
                           , CPST.ORDERED                                                 AS OrderedQuantity
                           , CPST.REMAIN                                                  AS RemainingQuantity
                           , CPST.QTY                                                     AS DeliveredQuantity
                           , CPST.InventQty                                               AS InventoryQuantity
                           , CPST.RemainInvent                                            AS InventoryQuantityOpen
                           , CPST.AMOUNTCUR                                               AS Value
                           , CPST.AMOUNTCUR * ex.CrossRate                                AS Value_RCY
                           , CPST.VALUEMST                                                AS ValueMST
             FROM            stage_ax.CUSTPACKINGSLIPTRANS AS CPST
            INNER JOIN       stage_ax.CUSTPACKINGSLIPJOUR  CPSJ ON CPST.PACKINGSLIPID      = CPSJ.PACKINGSLIPID
                                                               AND CPST.SALESID            = CPSJ.SALESID
                                                               AND CPST.DELIVERYDATE       = CPSJ.DELIVERYDATE
                                                               AND CPST.data_connection_id = CPSJ.data_connection_id
                                                               AND CPST.company_id         = CPSJ.company_id
                                                               AND CPST.PARTITION          = CPSJ.PARTITION
             LEFT OUTER JOIN stage_ax.INVENTDIM            ID ON CPST.INVENTDIMID          = ID.INVENTDIMID
                                                             AND CPST.DATAAREAID           = ID.DATAAREAID
                                                             AND ID.company_id             = CPST.company_id
                                                             AND CPST.PARTITION            = ID.PARTITION
                                                             AND CPST.data_connection_id   = ID.data_connection_id
             LEFT JOIN       stage_ax.SALESTABLE           ST ON CPST.SALESID              = ST.SALESID
                                                             AND CPST.data_connection_id   = ST.data_connection_id
                                                             AND CPST.company_id           = ST.company_id
                                                             AND CPST.PARTITION            = ST.PARTITION
             LEFT JOIN       reporting_exchange_rates      ex ON ex.FromCurrencyCode       = CPST.CURRENCYCODE
                                                             AND ex.DataConnectionID       = CPST.data_connection_id
                                                             AND CPSJ.DOCUMENTDATE BETWEEN ex.ValidFrom AND ex.ValidTo
            WHERE
             /** FULL LOAD **/
                             ( @load_type          = 0
                           AND CPST.execution_flag <> 'D')
               OR
               /** INCREMENTAL, NEW RECORDS **/
                             ( @load_type                        = 1
                           AND @execution_flag                   = 'N'
                           AND CPST.execution_flag               = @execution_flag
                           AND CPST.execution_timestamp          > @last_processed_timestamp)
               OR
             /** INCREMENTAL, UPDATED RECORDS **/
                           ( @load_type                       = 1
                         AND @execution_flag                  = 'U'
                         AND ( ( CPST.execution_flag           = @execution_flag
                             AND CPST.execution_timestamp      > @last_processed_timestamp)
                            OR ( ID.execution_flag            = @execution_flag
                             AND ID.execution_timestamp       > @last_processed_timestamp)
                            OR ( ST.execution_flag          = @execution_flag
                             AND ST.execution_timestamp     > @last_processed_timestamp)))
               OR
               /** INCREMENTAL, DELETED RECORDS **/
                             ( @load_type                        = 1
                           AND @execution_flag                   = 'D'
                           AND CPST.execution_flag               = @execution_flag
                           AND CPST.execution_timestamp          > @last_processed_timestamp)) ;
GO

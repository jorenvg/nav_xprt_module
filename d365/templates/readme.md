# Entity lists

The list of entities can be found in these files:

* HIL_BI_ENTITIES.txt - entities that need to be exported for each company
* HIL_BI_SHARED_ENTITIES.txt - entities that are shared, i.e. need to be exported only once
* HIL_BI_MUSTAD_ENTITIES.txt - custom entities for specific client

TODO DEV-1315: The names in the files are the current names of the entities. These still need to be fixed.

|Current name                             |Correct name                       |
|-|-|
|HIL_DimAttributeAssetGroupEntity         |HIL_DimAttributeAssetGroup         |
|HIL_DimAttributeHCMWorkerEntity          |HIL_DimAttributeHCMWorker          |
|HIL_DimAttributeWrkCtrTableEntity        |HIL_DimAttributeWrkCtrTable        |
|HIL_PartitionsEntity                     |HIL_Partitions                     |
|HIL_WHSLoadTableVendPackingSlipJour      |HIL_WHSLoadTableCustPackingSlipJour|
|HIL_WHSLoadTableVendPackingSlipJourEntity|HIL_WHSLoadTableVendPackingSlipJour|
|HIL_DimensionHierarchyLevel              |HIL_DimensionHierarchyLevel        |
|||
|HIL_CommissionSalesGroupEntity           |HIL_CommissionSalesGroup           |
|HIL_PayTerm                              |HIL_PaymTerm                       |
|HIL_PdsRebateAgreementEntity*            |HIL_PdsRebateAgreement             |
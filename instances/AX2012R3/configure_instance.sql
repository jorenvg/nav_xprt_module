﻿SET NOCOUNT ON;


/***DECLARATIONS**********/
DECLARE @instance_id INT , @instance_name NVARCHAR(100) , @msg NVARCHAR(255), @instance_execution_id INT
DECLARE @connection_id INT 
DECLARE @db_name NVARCHAR(50), @connection_name NVARCHAR(50), @connection_type NVARCHAR(20)
DECLARE @server NVARCHAR(50) , @instance  NVARCHAR(50) , @instance_ssas NVARCHAR(50)
DECLARE @is_sql_auth BIT , @username NVARCHAR(20), @password NVARCHAR(20)
-- COMPANIES
DECLARE @comp_code NVARCHAR(20)
DECLARE @company_id INT 
-- FINANCIAL DIMENSIONS

/***END DECLARATIONS**********/

SELECT @instance_name = 'AX2012R3'

/***INSTANCE**********/
SET @msg = 'Creating Instance ' + @instance_name
PRINT @msg
EXEC meta.add_instance @instance_name =@instance_name, -- nvarchar(100)
  @environment = N'test', -- nvarchar(20)
  @date_start_year = 2002,
  @date_offset_years = 0,
  @instance_id = @instance_id OUTPUT -- int
/*** END INSTANCE **********/

/**AUDITING****/
EXEC [audit].log_instance_start @instance_id = @instance_id, -- int
  @instance_execution_id = @instance_execution_id OUTPUT -- int

/*** DESTINATION CONNECTIONS ********************************/
IF SERVERPROPERTY('IsLocalDb') = 1
	SELECT @server = 'np:\\.\pipe',
	@instance = CONVERT(NVARCHAR(128), SERVERPROPERTY('InstanceName')) + '\tsql\query'
ELSE
	SELECT @server = CAST(SERVERPROPERTY('ComputerNamePhysicalNetBIOS') AS NVARCHAR(50)),
	@instance = CONVERT(NVARCHAR(128), SERVERPROPERTY('InstanceName')) 

SET @instance_ssas = 'SSAS_MULTI_2016'

-- Birds SSAS
SELECT @db_name = 'Birds', @connection_name = @instance_name + '_SSAS', @connection_type = 'Multidimensional'
SET @msg = 'Adding Destination Connection ' + @connection_name
PRINT @msg
EXEC meta.add_destination_data_connection @instance_id = @instance_id, -- int
  @connection_type = @connection_type, -- nchar(20)
  @connection_name = @connection_name, -- nvarchar(50)
  @server = @server, -- nvarchar(50)
  @instance = @instance_ssas, -- nvarchar(50)
  @database = @db_name, -- nvarchar(50)
  @is_sql_authentication = 0, -- bit
  @data_connection_id = @connection_id OUTPUT  -- int
  ,@instance_execution_id = @instance_execution_id

/*** Add ServiceAccounts ***/
-- Do not deploy service accounts on LocalDB and Azure -- not nessesary
IF SERVERPROPERTY('IsLocalDb') <> 1
-- TODO Add check for azure with SERVERPROPERTY('Edition') = 'SQL Azure'
-- https://msdn.microsoft.com/en-us/library/ms174396.aspx
BEGIN
	PRINT 'Adding service accounts'
  EXEC meta.add_data_connection_service_account @data_connection_id = @connection_id, @instance_execution_id = @instance_execution_id
END
/***  ServiceAccounts ***/

/*** END DESTINATION CONNECTIONS ********************************/
/*** SOURCE CONNECTIONS ********************************/
  SELECT @connection_name = 'AX2012R3', @db_name = 'AX2012R3', @server = '192.168.36.53', @instance = NULL,
	 @is_sql_auth = 1, @username = 'birds_user', @password = 'sqlpass', @connection_type = 'AX2012R3'

  SET @msg = 'Adding Source Connection ' + @connection_name
  PRINT @msg

  EXEC meta.add_data_connection @instance_id = @instance_id, -- int
    @connection_type = @connection_type, -- nvarchar(20)
    @connection_name = @connection_name, -- nvarchar(50)
    @server = @server, -- nvarchar(50)
    @instance = @instance, -- nvarchar(50)
    @port = NULL, -- int
    @database = @db_name, -- nvarchar(50)
    @is_sql_authentication = @is_sql_auth, -- bit
    @username = @username, -- nvarchar(50)
    @password = @password, -- nvarchar(50)
    @data_connection_id = @connection_id OUTPUT -- int
    ,@instance_execution_id = @instance_execution_id
  
/*** END SOURCE CONNECTIONS ********************************/


/*** DEPLOY LINKED SERVERS ***/
  EXEC meta.deploy_all_linked_servers @instance_id = @instance_id, -- int
  @debug = 0, -- bit
  @instance_execution_id = @instance_execution_id -- int

/*** DEPLOY LINKED SERVERS ***/

/*** FINANCIAL DIMENSION **/
-- TODO adding financial dimensions to multiple datasources ***/
  EXEC meta.add_all_financial_dimensions @data_connection_id = @connection_id, -- int
    @debug = 0, -- bit
    @refresh = 1 -- bit
    ,@instance_execution_id = @instance_execution_id
/*** FINANCIAL DIMENSION **/

/****COMPANIES**********************************************************/
  SELECT @comp_code = 'usmf'
  SET @msg = 'Adding Company ' + @comp_code + ' to the connection ' + @connection_name
  PRINT @msg

  EXEC meta.add_company @data_connection_id = @connection_id, -- int
    @company_code = @comp_code, -- nvarchar(20)
    @company_id = @company_id -- int
    ,@instance_execution_id = @instance_execution_id

  SELECT @comp_code = 'usp2'
  SET @msg = 'Adding Company ' + @comp_code + ' to the connection ' + @connection_name
  PRINT @msg
  EXEC meta.add_company @data_connection_id = @connection_id, -- int
    @company_code = @comp_code, -- nvarchar(20)
    @company_id = @company_id -- int
    ,@instance_execution_id = @instance_execution_id

  SELECT @comp_code = 'uspi'
  SET @msg = 'Adding Company ' + @comp_code + ' to the connection ' + @connection_name
  PRINT @msg
    EXEC meta.add_company @data_connection_id = @connection_id, -- int
    @company_code = @comp_code, -- nvarchar(20)
    @company_id = @company_id -- int
    ,@instance_execution_id = @instance_execution_id

  SELECT @comp_code = 'usrt'
  SET @msg = 'Adding Company ' + @comp_code + ' to the connection ' + @connection_name
  PRINT @msg
  EXEC meta.add_company @data_connection_id = @connection_id, -- int
    @company_code = @comp_code, -- nvarchar(20)
    @company_id = @company_id -- int
    ,@instance_execution_id = @instance_execution_id


-- VIRTUAL COMPANIES
-- todo multiple datasources...
EXEC meta.add_all_virtual_companies @data_connection_id = @connection_id, -- int
    @debug = 0, -- bit
    @instance_execution_id = @instance_execution_id -- int

/**** END COMPANIES **********************************************************/  

/*** END SOURCE CONNECTIONS ********************************/


/**AUDITING*************************/
EXEC audit.log_instance_stop @instance_execution_id = @instance_execution_id, -- int
	  @status = N'S' -- nchar(2)


PRINT '---------------------------------------------------'

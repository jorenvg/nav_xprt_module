--uncomment to run in ssms
--EXEC meta.remove_all_metadata @debug = 0
--EXEC dbo.drop_all_external_datasources

SET NOCOUNT ON;

/***DECLARATIONS**********/
DECLARE @instance_id INT , @instance_name NVARCHAR(100) , @msg NVARCHAR(255), @instance_execution_id INT
DECLARE @connection_id INT
DECLARE @db_name NVARCHAR(50), @connection_name NVARCHAR(50), @connection_type NVARCHAR(20)
DECLARE @server NVARCHAR(50) , @instance  NVARCHAR(50) , @instance_ssas NVARCHAR(50)
DECLARE @is_sql_auth BIT , @username NVARCHAR(20), @password NVARCHAR(20)
-- COMPANIES
DECLARE @comp_code NVARCHAR(20)
DECLARE @company_id INT
-- FINANCIAL DIMENSIONS
 
/***END DECLARATIONS**********/
 
SELECT @instance_name = 'AX7'

/***INSTANCE**********/
SET @msg = 'Creating Instance ' + @instance_name
PRINT @msg
EXEC meta.add_instance @instance_name =@instance_name, -- nvarchar(100)
  @environment = N'test', -- nvarchar(20)
  @date_start_year = 2002,
  @date_offset_years = 0,
  @instance_id = @instance_id OUTPUT -- int
/*** END INSTANCE **********/
 
/**AUDITING****/
EXEC [audit].log_instance_start @instance_id = @instance_id, -- int
  @instance_execution_id = @instance_execution_id OUTPUT -- int
 
/*** END DESTINATION CONNECTIONS ********************************/
/*** SOURCE CONNECTIONS ********************************/
  SELECT @connection_name = 'AX7', @db_name = 'birds_d365_export', @server = 'hillstar-poc.database.windows.net', @instance = NULL,
       @is_sql_auth = 1, @username = 'birds_user', @password = 'gBShBVuZkVBJvZtyjN2t', @connection_type = 'AX7_Entities'
 
  EXEC dbo.drop_external_datasource_and_dependent_objects @connection_name

  SET @msg = 'Adding Source Connection ' + @connection_name
  PRINT @msg
 
  EXEC meta.add_data_connection @instance_id = @instance_id, -- int
    @connection_type = @connection_type, -- nvarchar(20)
    @connection_name = @connection_name, -- nvarchar(50)
    @server = @server, -- nvarchar(50)
    @instance = @instance, -- nvarchar(50)
    @port = NULL, -- int
    @database = @db_name, -- nvarchar(50)
    @is_sql_authentication = @is_sql_auth, -- bit
    @username = @username, -- nvarchar(50)
    @password = @password, -- nvarchar(50)
    @data_connection_id = @connection_id OUTPUT -- int
    ,@instance_execution_id = @instance_execution_id
 
/*** END SOURCE CONNECTIONS ********************************/

EXEC meta.[deploy_all_source_data_connection_objects]
       @instance_id = @instance_id
       , @debug = 0
       , @instance_execution_id  = @instance_execution_id
 
--  /*** FINANCIAL DIMENSION **/
-- EXEC meta.add_all_financial_dimensions @data_connection_id = @connection_id, -- int
--     @debug = 0, -- bit
--     @refresh = 1 -- bit
--     ,@instance_execution_id = @instance_execution_id
-- /*** FINANCIAL DIMENSION **/

-- /****COMPANIES**********************************************************/
  SELECT @comp_code = 'ussi'
  SET @msg = 'Adding Company ' + @comp_code + ' to the connection ' + @connection_name
  PRINT @msg

  EXEC meta.add_company @data_connection_id = @connection_id, -- int
    @company_code = @comp_code, -- nvarchar(20)
    @company_id = @company_id -- int
    ,@instance_execution_id = @instance_execution_id

--   SELECT @comp_code = 'usp2'
--   SET @msg = 'Adding Company ' + @comp_code + ' to the connection ' + @connection_name
--   PRINT @msg
--   EXEC meta.add_company @data_connection_id = @connection_id, -- int
--     @company_code = @comp_code, -- nvarchar(20)
--     @company_id = @company_id -- int
--     ,@instance_execution_id = @instance_execution_id

--   SELECT @comp_code = 'uspi'
--   SET @msg = 'Adding Company ' + @comp_code + ' to the connection ' + @connection_name
--   PRINT @msg
--     EXEC meta.add_company @data_connection_id = @connection_id, -- int
--     @company_code = @comp_code, -- nvarchar(20)
--     @company_id = @company_id -- int
--     ,@instance_execution_id = @instance_execution_id

--   SELECT @comp_code = 'usrt'
--   SET @msg = 'Adding Company ' + @comp_code + ' to the connection ' + @connection_name
--   PRINT @msg
--   EXEC meta.add_company @data_connection_id = @connection_id, -- int
--     @company_code = @comp_code, -- nvarchar(20)
--     @company_id = @company_id -- int
--     ,@instance_execution_id = @instance_execution_id

-- -- VIRTUAL COMPANIES
-- -- todo multiple datasources...
-- EXEC meta.add_all_virtual_companies @data_connection_id = @connection_id, -- int
--     @debug = 0, -- bit
--     @instance_execution_id = @instance_execution_id -- int

/**** END COMPANIES **********************************************************/  

 

EXEC [audit].log_instance_stop @instance_execution_id = @instance_execution_id, @status = 'S'

/*


DECLARE @component_id INT;
EXEC meta.add_dwh_component @instance_id = @instance_id, -- int
  @category = N'dim', -- nvarchar(10)
  @name = N'dimTest', -- nvarchar(50)
  @db_object = N'dim.tests', -- nvarchar(128)
  @type = N'T', -- nvarchar(2)
  @component_id = @component_id OUTPUT, -- int
  @instance_execution_id = 0 -- int
 
 
 
DECLARE @table_columns AS dbo.[table_column_metadata]
INSERT INTO @table_columns
(table_name, column_name, data_type, max_length, numeric_precision, numeric_scale, nullable )
VALUES
-- TABLE dbo.CUSTTABLE
       -- System columns
              ('dbo.CUSTTABLE', 'PARTITION','bigint',-1,19,0,0)
              ,('dbo.CUSTTABLE', 'RECID','bigint',-1,19,0,0)
              ,('dbo.CUSTTABLE', 'RECVERSION','int',-1,10,0,0)
              ,('dbo.CUSTTABLE', 'MODIFIEDDATETIME','datetime',-1,NULL,NULL,0)
       -- Company columns
              ,('dbo.CUSTTABLE', 'DATAAREAID','nvarchar',4,NULL,NULL,0)
       -- Other columns
              ,('dbo.CUSTTABLE', 'MAINCONTACTWORKER','bigint',-1,19,0,0)
-- TABLE dbo.DIRPARTYTABLE
       -- System columns
              ,('dbo.DIRPARTYTABLE', 'PARTITION','bigint',-1,19,0,0)
              ,('dbo.DIRPARTYTABLE', 'RECID','bigint',-1,19,0,0)
              ,('dbo.DIRPARTYTABLE', 'RECVERSION','int',-1,10,0,0)
              ,('dbo.DIRPARTYTABLE', 'MODIFIEDDATETIME','datetime',-1,NULL,NULL,0)
       -- Other columns
              ,('dbo.DIRPARTYTABLE', 'NAME','nvarchar',100,NULL,NULL,0)
-- TABLE dbo.HCMWORKER
       -- System columns
              ,('dbo.HCMWORKER', 'PARTITION','bigint',-1,19,0,0)
              ,('dbo.HCMWORKER', 'RECID','bigint',-1,19,0,0)
              ,('dbo.HCMWORKER', 'RECVERSION','int',-1,10,0,0)
              ,('dbo.HCMWORKER', 'MODIFIEDDATETIME','datetime',-1,NULL,NULL,0)
       -- Other columns
              ,('dbo.HCMWORKER', 'PERSON','bigint',-1,19,0,0)
              ,('dbo.HCMWORKER', 'PERSONNELNUMBER','nvarchar',25,NULL,NULL,0)
 
EXEC meta.add_all_stage_table_columns @data_connection_id = @connection_id
       , @component_category = 'dim'
       , @component_name = 'dimTest'
       , @table_columns = @table_columns
       , @debug = 0
 
 EXEC dbo.deploy_stagingarea
 	
 EXEC [repl].[process]

 EXEC [stage].[process]

 SELECT * FROM stage_ax.CUSTTABLE

*/